/**
* @file kdtree.cpp
* @brief K-dimensional search tree implementation and related data structs and algorithms
* @author Aurel Neic
* @version
* @date 2018-07-28
*/
#include "io_utils.h"
#include "kdtree.h"
#include "rand_generator.h"

rand_generator_state mt_rand_gen;

void generate_bbox(const mt_vector<tri_elem> & tri, bbox & box)
{
  vec3r & min = box.bounds[0];
  vec3r & max = box.bounds[1];

  size_t nele = tri.size();
  min.x = tri[0].v0.x, min.y = tri[0].v0.y, min.z = tri[0].v0.z;
  max.x = tri[0].v0.x, max.y = tri[0].v0.y, max.z = tri[0].v0.z;

  for(size_t i=0; i<nele; i++) {
    mt_real x = tri[i].v0.x, y = tri[i].v0.y, z = tri[i].v0.z;
    if(min.x > x) min.x = x;
    if(min.y > y) min.y = y;
    if(min.z > z) min.z = z;
    if(max.x < x) max.x = x;
    if(max.y < y) max.y = y;
    if(max.z < z) max.z = z;

    x = tri[i].v1.x, y = tri[i].v1.y, z = tri[i].v1.z;
    if(min.x > x) min.x = x;
    if(min.y > y) min.y = y;
    if(min.z > z) min.z = z;
    if(max.x < x) max.x = x;
    if(max.y < y) max.y = y;
    if(max.z < z) max.z = z;

    x = tri[i].v2.x, y = tri[i].v2.y, z = tri[i].v2.z;
    if(min.x > x) min.x = x;
    if(min.y > y) min.y = y;
    if(min.z > z) min.z = z;
    if(max.x < x) max.x = x;
    if(max.y < y) max.y = y;
    if(max.z < z) max.z = z;
  }
}

bool bbox_is_hit_1(const ray & r, const bbox & box, const mt_real scale_min, const mt_real scale_max)
{
  const vec3r ray_orig = r.origin();
  const vec3r ray_dir  = r.direction();

  const vec3r & min = box.bounds[0];
  const vec3r & max = box.bounds[1];

  mt_real tmin, tmax, tymin, tymax, tzmin, tzmax;
  mt_real divx = 1.0f / ray_dir.x,
        divy = 1.0f / ray_dir.y,
        divz = 1.0f / ray_dir.z;

  if (r.direction().x >= 0) {
    tmin = (min.x - ray_orig.x) * divx;
    tmax = (max.x - ray_orig.x) * divx;
  }
  else {
    tmin = (max.x - ray_orig.x) * divx;
    tmax = (min.x - ray_orig.x) * divx;
  }

  if (r.direction().y >= 0) {
    tymin = (min.y - ray_orig.y) * divy;
    tymax = (max.y - ray_orig.y) * divy;
  }
  else {
    tymin = (max.y - ray_orig.y) * divy;
    tymax = (min.y - ray_orig.y) * divy;
  }

  if ( (tmin > tymax) || (tymin > tmax) )
    return false;

  if (tymin > tmin) tmin = tymin;
  if (tymax < tmax) tmax = tymax;

  if (r.direction().z >= 0) {
    tzmin = (min.z - ray_orig.z) * divz;
    tzmax = (max.z - ray_orig.z) * divz;
  }
  else {
    tzmin = (max.z - ray_orig.z) * divz;
    tzmax = (min.z - ray_orig.z) * divz;
  }

  if ( (tmin > tzmax) || (tzmin > tmax) )
    return false;

  if (tzmin > tmin) tmin = tzmin;
  if (tzmax < tmax) tmax = tzmax;

  return ( (tmin < scale_max) && (tmax > scale_min) );
}

bool bbox_is_hit_2(const ray & r, const bbox & box, const mt_real scale_min, const mt_real scale_max)
{
  const vec3r ray_orig = r.origin();
  const vec3r ray_dir  = r.direction();

  mt_real divx = 1.0 / ray_dir.x;
  mt_real divy = 1.0 / ray_dir.y;
  mt_real divz = 1.0 / ray_dir.z;

  const vec3r & min = box.bounds[0];
  const vec3r & max = box.bounds[1];

  mt_real t1 = (min.x - ray_orig.x)*divx;
  mt_real t2 = (max.x - ray_orig.x)*divx;
  mt_real t3 = (min.y - ray_orig.y)*divy;
  mt_real t4 = (max.y - ray_orig.y)*divy;
  mt_real t5 = (min.z - ray_orig.z)*divz;
  mt_real t6 = (max.z - ray_orig.z)*divz;

  mt_real tmin = KDMAX(KDMAX(KDMIN(t1, t2), KDMIN(t3, t4)), KDMIN(t5, t6));
  mt_real tmax = KDMIN(KDMIN(KDMAX(t1, t2), KDMAX(t3, t4)), KDMAX(t5, t6));

  // if tmax < 0, ray is intersecting AABB, but the whole AABB is behind us
  if (tmax < 0) {
    // *t = tmax;
    return false;
  }

  // if tmin > tmax, ray doesn't intersect AABB
  if (tmin > tmax) {
    // *t = tmax;
    return false;
  }

  // *t = tmin;
  // return true;
  return ( (tmin < scale_max) && (tmax > scale_min) );
}

bool bbox_is_hit_3(const ray & r, const bbox & box, const mt_real scale_min, const mt_real scale_max)
{
  mt_real tmin, tmax, tymin, tymax, tzmin, tzmax;
  const vec3r & r_orig = r.origin(), & r_idir = r.iB;

  tmin  = (box.bounds[r.sign[0]    ].x - r_orig.x) * r_idir.x;
  tmax  = (box.bounds[1 - r.sign[0]].x - r_orig.x) * r_idir.x;
  tymin = (box.bounds[r.sign[1]    ].y - r_orig.y) * r_idir.y;
  tymax = (box.bounds[1 - r.sign[1]].y - r_orig.y) * r_idir.y;

  if ( (tmin > tymax) || (tymin > tmax) )
    return false;

  if (tymin > tmin) tmin = tymin;
  if (tymax < tmax) tmax = tymax;

  tzmin = (box.bounds[r.sign[2]    ].z - r_orig.z) * r_idir.z;
  tzmax = (box.bounds[1 - r.sign[2]].z - r_orig.z) * r_idir.z;

  if ( (tmin > tzmax) || (tzmin > tmax) )
    return false;

  if (tzmin > tmin) tmin = tzmin;
  if (tzmax < tmax) tmax = tzmax;

  return ( (tmin < scale_max) && (tmax > scale_min) );
}

bool ray_triangle_intersect(const ray & r,
                            const tri_elem & tri,
                            mt_real &t, mt_real &u, mt_real &v)
{
  const vec3r & orig = r.origin();
  const vec3r & dir  = r.direction();
  vec3r qvec;
  // find vectors for two edges sharing v0
  const vec3r edge1 = tri.v1 - tri.v0;
  const vec3r edge2 = tri.v2 - tri.v0;

  const mt_real testlen = EPS_MB*edge1.length();

  // begin calculating determinant - also used to calculate u parameter
  const vec3r pvec = dir.crossProd(edge2);
  // if determinant is near zero, ray lies in plane of triangle
  const mt_real det = edge1.scaProd(pvec);

  if (det > testlen) {
    // calculate distance from v0 to ray origin
    const vec3r tvec = orig - tri.v0;
    // calculate u parameter and test bounds
    u = tvec.scaProd(pvec);
    if (u < 0.0 || u > det)
      return false;

    // prepare to test v parameter
    qvec = tvec.crossProd(edge1);
    // calculate v parameter and test bounds
    v = dir.scaProd(qvec);
    if (v < 0.0 || u + v > det)
      return false;
  }
  else if(det < -testlen) {
    // calculate distance from vert0 to ray origin
    const vec3r tvec = orig - tri.v0;
    // calculate u parameter and test bounds
    u = tvec.scaProd(pvec);
    if (u > 0.0 || u < det)
      return false;
    // prepare to test V parameter
    qvec = tvec.crossProd(edge1);
    // calculate v parameter and test bounds
    v = dir.scaProd(qvec);
    if (v > 0.0 || u + v < det)
      return false;
  }
  else {
      return false;  // ray is parallel to the plane of the triangle
  }

  const mt_real inv_det = 1.0 / det;
  // calculate t, ray intersects triangle
  t = edge2.scaProd(qvec) * inv_det;
  u *= inv_det;
  v *= inv_det;
  return true;
}

kdtree::kdtree(int t) : init_size(1000), last_nod_idx(0), items_per_leaf(t)
{
  nodes.resize(init_size);
  boxes.resize(init_size);
}

kdtree::~kdtree()
{
  if(tris.size())
    for(int i=0; i<last_nod_idx; i++)
      if(tris[i]) delete tris[i];

  if(verts.size())
    for(int i=0; i<last_nod_idx; i++)
      if(verts[i]) delete verts[i];
}

void kdtree::build_tree(const mt_vector<tri_elem> & triangles)
{
  tris.resize(init_size, NULL);

  mt_vector<tri_elem>* tri = new mt_vector<tri_elem>(triangles);
  build(tri, -1, 0);
}

void kdtree::build_tree(const mt_meshdata & surfmesh)
{
  tris.resize(init_size, NULL);

  mt_vector<tri_elem>* triangles = new mt_vector<tri_elem>();
  triangles->reserve(surfmesh.e2n_cnt.size());
  tri_elem t;
  const mt_real* xyz = surfmesh.xyz.data();

  if(surfmesh.etype.size()) {
    for(size_t i=0, dsp=0; i<surfmesh.e2n_cnt.size(); i++)
    {
      if (surfmesh.etype[i] == Tri) {
        t.v0.get(surfmesh.xyz.data() + surfmesh.e2n_con[dsp+0]*3);
        t.v1.get(surfmesh.xyz.data() + surfmesh.e2n_con[dsp+1]*3);
        t.v2.get(surfmesh.xyz.data() + surfmesh.e2n_con[dsp+2]*3);
        t.eidx = i;
        triangles->push_back(t);
      } else if (surfmesh.etype[i] == Quad){
        t.v0.get(surfmesh.xyz.data() + surfmesh.e2n_con[dsp+0]*3);
        t.v1.get(surfmesh.xyz.data() + surfmesh.e2n_con[dsp+1]*3);
        t.v2.get(surfmesh.xyz.data() + surfmesh.e2n_con[dsp+2]*3);
        t.eidx = i;
        triangles->push_back(t);

        t.v0.get(surfmesh.xyz.data() + surfmesh.e2n_con[dsp+2]*3);
        t.v1.get(surfmesh.xyz.data() + surfmesh.e2n_con[dsp+3]*3);
        t.v2.get(surfmesh.xyz.data() + surfmesh.e2n_con[dsp+0]*3);
        t.eidx = i;
        triangles->push_back(t);
      }
      dsp += surfmesh.e2n_cnt[i];
    }
  } else {
    for(size_t i=0, dsp=0; i<surfmesh.e2n_cnt.size(); i++) {
      if(surfmesh.e2n_cnt[i] == 3) {
        t.v0.get(xyz + surfmesh.e2n_con[dsp+0]*3);
        t.v1.get(xyz + surfmesh.e2n_con[dsp+1]*3);
        t.v2.get(xyz + surfmesh.e2n_con[dsp+2]*3);
        t.eidx = i;
        triangles->push_back(t);
      } else if(surfmesh.e2n_cnt[i] == 4) {
        t.v0.get(xyz + surfmesh.e2n_con[dsp+0]*3);
        t.v1.get(xyz + surfmesh.e2n_con[dsp+1]*3);
        t.v2.get(xyz + surfmesh.e2n_con[dsp+2]*3);
        t.eidx = i;
        triangles->push_back(t);

        t.v0.get(xyz + surfmesh.e2n_con[dsp+2]*3);
        t.v1.get(xyz + surfmesh.e2n_con[dsp+3]*3);
        t.v2.get(xyz + surfmesh.e2n_con[dsp+0]*3);
        t.eidx = i;
        triangles->push_back(t);
      }
      dsp += surfmesh.e2n_cnt[i];
    }
  }

  build(triangles, -1, 0);
}

void kdtree::build_vertex_tree(const mt_vector<mt_real> & xyz, const mt_real bbox_scale)
{
  array_to_points(xyz, _xyz);
  verts.resize(init_size, NULL);

  mt_vector<int>* vertices = new mt_vector<int>(xyz.size() / 3);

  for(size_t i=0; i<vertices->size(); i++)
    (*vertices)[i] = i;

  bbox startbox;
  generate_bbox(_xyz, *vertices, startbox);
  cartesian_csys::bbox_scale_centered(startbox, bbox_scale);

  build(vertices, startbox, -1);
}

void kdtree::build_vertex_tree(const mt_vector<mt_idx_t> & nod, const mt_vector<mt_real> & xyz, const mt_real bbox_scale)
{
  _xyz.resize(nod.size());

  for(size_t i=0; i<_xyz.size(); i++)
    _xyz[i].get(xyz.data() + nod[i]*3);

  verts.resize(init_size, NULL);
  mt_vector<int>* vertices = new mt_vector<int>(_xyz.size());

  for(size_t i=0; i<vertices->size(); i++)
    (*vertices)[i] = i;

  bbox startbox;
  generate_bbox(_xyz, *vertices, startbox);
  cartesian_csys::bbox_scale_centered(startbox, bbox_scale);

  build(vertices, startbox, -1);
}

void kdtree::sort_median(const mt_vector<tri_elem> & tri, const bboxAxis split_axis, mt_vector<tri_elem> & left, mt_vector<tri_elem> & right) const
{
  const size_t sz = tri.size();
  mt_vector<mt_mixed_tuple<mt_real,int>> med_coords(sz);

  switch(split_axis) {
    case X:
      for(size_t i=0; i < sz; i++) {
        vec3r ctr = (tri[i].v0 + tri[i].v1 + tri[i].v2) / 3.0;
        med_coords[i] = {ctr.x, (int)i};
      }
      break;
    case Y:
      for(size_t i=0; i < sz; i++) {
        vec3r ctr = (tri[i].v0 + tri[i].v1 + tri[i].v2) / 3.0;
        med_coords[i] = {ctr.y, (int)i};
      }
      break;
    case Z:
      for(size_t i=0; i < sz; i++) {
        vec3r ctr = (tri[i].v0 + tri[i].v1 + tri[i].v2) / 3.0;
        med_coords[i] = {ctr.z, (int)i};
      }
      break;
    default: break;
  }

  size_t med = med_coords.size() / 2;
  std::sort(med_coords.begin(), med_coords.end());

  left .reserve_and_clear(med);
  right.reserve_and_clear(med);

  for(size_t i=0; i<med; i++)
    left.push_back(tri[med_coords[i].v2]);

  for(size_t i=med; i<sz; i++)
    right.push_back(tri[med_coords[i].v2]);
}


int kdtree::build(mt_vector<tri_elem>* current_tris, int up, int depth)
{
  int n        = add_node();
  nodes[n].up  = up;
  int num_poly = current_tris->size();

  if (num_poly == 0) {
    assert(0);
#ifdef KDTREE_VERB
    printf("%d: stopping empty. does this even happen? \n", depth);
#endif
    return n;
  }

  generate_bbox(*current_tris, boxes[n]);
  bbox & box = boxes[n];

  if (num_poly <= items_per_leaf) {
    assert(num_poly > 0);

    tris[n] = current_tris;

#ifdef KDTREE_VERB
    printf("%d: stopping with %d poly. success.\n", depth, (int) num_poly);
#endif
    return n;
  }

  // printf("%d: bbox: min (%lf, %lf, %lf), max (%lf, %lf, %lf)\n", depth,
  // node->bbox->start.x, node->bbox->start.y, node->bbox->start.z,
  // node->bbox->end.x, node->bbox->end.y, node->bbox->end.z);

  mt_vector<tri_elem>* tris_left  = new mt_vector<tri_elem>();
  mt_vector<tri_elem>* tris_right = new mt_vector<tri_elem>();
  bboxAxis axis       = get_longest_axis(box);
  nodes[n].split_axis = axis;

  sort_median(*current_tris, axis, *tris_left, *tris_right);

  bool empty_side = false;
  if ( (tris_left ->size() == 0 && tris_right->size() > 0) ||
      (tris_right->size() == 0 && tris_left ->size() > 0) )
    empty_side = true;

  if (empty_side == false) {
#ifdef KDTREE_VERB
    printf("%d: splitting %d -> ( %d , %d )\n", depth, num_poly, int(tris_left->size()),
        int(tris_right->size()));
    fflush(stdout);
#endif
    //Recurse down both left and right sides
    int lidx = build(tris_left, n, depth + 1);
    int ridx = build(tris_right, n, depth + 1);
    nodes[n].left  = lidx;
    nodes[n].right = ridx;
    // this is not a leaf node -> we can delete the current_tris
    delete current_tris;
  }
  else {
    //Stop here
    tris[n] = current_tris;

#ifdef KDTREE_VERB
    printf("%d: stopping with %d polys because of bad splitting (one side empty). \n", depth, num_poly);
    fflush(stdout);
#endif
  }

  return n;
}

mt_real kdtree::get_median(const mt_vector<int> & vert, const bboxAxis split_axis) const
{
  const size_t sz = vert.size();
  mt_vector<mt_real> med_coords(sz);

  switch(split_axis) {
    case X:
      for(size_t i=0; i < sz; i++)
        med_coords[i] = _xyz[vert[i]].x;
      break;
    case Y:
      for(size_t i=0; i < sz; i++)
        med_coords[i] = _xyz[vert[i]].y;
      break;
    case Z:
      for(size_t i=0; i < sz; i++)
        med_coords[i] = _xyz[vert[i]].z;
      break;
    default: break;
  }

  size_t med = med_coords.size() / 2;
  // we use std::nth_element to get the sorted value at median index without sorting
  // the whole sequence
  std::nth_element(med_coords.begin(), med_coords.begin() + med, med_coords.end());

  return med_coords[med];
}


int kdtree::build(mt_vector<int>* current_vert, const bbox & box, const int up)
{
  int n         = add_node();
  int num_verts = current_vert->size();
  nodes[n].up   = up;
  boxes[n]      = box;

  if (num_verts == 0) {
    printf("kdtree::vertex_build: stopping empty. This should not happen!\n");
    assert(0);
    return n;
  }

  if (num_verts <= items_per_leaf) {
#ifdef KDTREE_VERB
    printf("kdtree::vertex_build: stopping with %d verts. success.\n", (int) num_verts);
#endif
    verts[n] = current_vert;
    return n;
  }

  mt_vector<int>* verts_left  = new mt_vector<int>();
  mt_vector<int>* verts_right = new mt_vector<int>();
  verts_left->reserve(num_verts / 2);
  verts_right->reserve(num_verts / 2);

  bbox auxbox;
  generate_bbox(_xyz, *current_vert, auxbox);

  bool unbalanced = false;
  mt_real median = 0.0;

  bboxAxis axis = cartesian_csys::get_longest_axis(auxbox);
  nodes[n].split_axis = axis;

  median = get_median(*current_vert, axis);
  verts_left->resize(0);
  verts_right->resize(0);

  const mt_vector<int> & v = *(current_vert);
  for (int i = 0; i < num_verts; i++) {
    const vec3r & p = _xyz[v[i]];

    if (((axis == X) && (median < p.x)) ||
        ((axis == Y) && (median < p.y)) ||
        ((axis == Z) && (median < p.z)))
    {
      verts_right->push_back(v[i]);
    }
    else {
      verts_left->push_back(v[i]);
    }
  }

  #if 0
  mt_real ls = verts_left ->size(), rs = verts_right->size();
  unbalanced = (fabs(ls - rs) / num_verts) > 0.6f;
  #else
  unbalanced = (verts_left ->size() == 0 && verts_right->size() > 0) ||
               (verts_right->size() == 0 && verts_left ->size() > 0);
  #endif

  // if we have an unbalanced kdtree node, we want to redo the partitioning for the
  // next, biggest axis. For that, we zero out the current axis bounds, so that the
  // next call to get_longest_axis will give us the next longest
  if(unbalanced) {
    vec3r midPoint = (auxbox.bounds[0] + auxbox.bounds[1]) * mt_real(0.5);

    switch(axis) {
      case X: median = midPoint.x; break;
      case Y: median = midPoint.y; break;
      case Z: median = midPoint.z; break;
      default: break;
    }

    verts_left->resize(0);
    verts_right->resize(0);

    for (int i = 0; i < num_verts; i++) {
      const vec3r & p = _xyz[v[i]];

      if (((axis == X) && (median < p.x)) ||
          ((axis == Y) && (median < p.y)) ||
          ((axis == Z) && (median < p.z)))
      {
        verts_right->push_back(v[i]);
      }
      else {
        verts_left->push_back(v[i]);
      }
    }

    unbalanced = (verts_left ->size() == 0 && verts_right->size() > 0) ||
                 (verts_right->size() == 0 && verts_left ->size() > 0);

  }

  if (unbalanced == false) {
    // compute bboxes for children
    bbox left_split = box, right_split = box;
    switch(nodes[n].split_axis) {
      case X:
        left_split.bounds[1].x = median; right_split.bounds[0].x = median;
        break;
      case Y:
        left_split.bounds[1].y = median; right_split.bounds[0].y = median;
        break;
      case Z:
        left_split.bounds[1].z = median; right_split.bounds[0].z = median;
        break;
      case UNSET: break;
    }

    //Recurse down both left and right sides
    int lidx = build(verts_left, left_split, n);
    int ridx = build(verts_right, right_split, n);
    nodes[n].left  = lidx;
    nodes[n].right = ridx;
    // this is not a leaf node -> we can delete the current_vert
    delete current_vert;
  }
  else {
    //Stop here
    verts[n] = current_vert;
    nodes[n].split_axis = UNSET;

#ifdef KDTREE_VERB
    if(num_verts > items_per_leaf * 2) {
      printf("Warning! Node %d: stopping with %d vertices because of unbalanced splitting.\n", n, num_verts);
      generate_bbox(_xyz, *current_vert, auxbox);
      axis = cartesian_csys::get_longest_axis(auxbox);
      printf("box: (%f %f %f) - (%f %f %f)\n", auxbox.bounds[0].x, auxbox.bounds[0].y, auxbox.bounds[0].z,
                                               auxbox.bounds[1].x, auxbox.bounds[1].y, auxbox.bounds[1].z);
      printf("longest axis:");
      switch(axis) {
        case X: printf("X\n"); break;
        case Y: printf("Y\n"); break;
        case Z: printf("Z\n"); break;
        case UNSET: break;
      }
    }
    printf("Warning! Node %d: stopping with %d vertices because of unbalanced splitting.\n", n, num_verts);
    fflush(stdout);
#endif
  }

  return n;
}


int kdtree::add_node()
{
  int ret = last_nod_idx;
  last_nod_idx++;

  if(last_nod_idx == int(nodes.size()))
  {
    nodes.resize(last_nod_idx*2);
    boxes.resize(last_nod_idx*2);
    if(tris.size())
      tris.resize (last_nod_idx*2, NULL);
    if(verts.size())
      verts.resize (last_nod_idx*2, NULL);
  }

  return ret;
}

bool kdtree::intersect_ray(const int nod_idx, const ray & r, const mt_real scale_min,
                           mt_real & closest_so_far, tri_elem & hit_ele, vec3r & hit_pos) const
{
  const bbox & box       = boxes[nod_idx];
  const kdtree::node & n = nodes[nod_idx];

  if (bbox_is_hit_3(r, box, scale_min, closest_so_far)) {
    bool hasHit = false;
    if (tris[nod_idx] == NULL) {
      // current node is not a leaf yet -> recurse down both sides
      bool hitLeft  = intersect_ray(n.left,  r, scale_min, closest_so_far, hit_ele, hit_pos);
      bool hitRight = intersect_ray(n.right, r, scale_min, closest_so_far, hit_ele, hit_pos);

      return hitLeft || hitRight;
    }
    else {
      // current node is a leaf -> check all polys
      const mt_vector<tri_elem> & tr = *(tris[nod_idx]);
      int num_tri = tr.size();

      for (int i = 0; i < num_tri; i++)
      {
        mt_real u, v, scale;
        bool did_hit = ray_triangle_intersect(r, tr[i], scale, u, v);
        if (did_hit && scale > scale_min && scale < closest_so_far) {
          hasHit = true;
          hit_ele = tr[i];
          hit_pos = r.point_at_param(scale);
          closest_so_far = scale;
        }
      }
      return hasHit;
    }
  }
  else
    return false;
}

bool kdtree::closest_intersect(const ray & r, const mt_real scale_min, const mt_real scale_max,
                               tri_elem & hit_ele, vec3r & hit_pos) const
{
  mt_real closest_so_far = scale_max;
  bool ret = intersect_ray(0, r, scale_min, closest_so_far, hit_ele, hit_pos);
  return ret;
}

bool kdtree::find_enclosing_leaf(const int nod_idx, const vec3r ref, int & leaf_idx) const
{
  const bbox & box       = boxes[nod_idx];
  const kdtree::node & n = nodes[nod_idx];

  if (cartesian_csys::vert_in_bbox(ref, box)) {
    if (verts[nod_idx] == NULL) {
      // current node is not a leaf yet -> recurse down both sides
      bool in_left  = find_enclosing_leaf(n.left,  ref, leaf_idx);
      bool in_right = find_enclosing_leaf(n.right, ref, leaf_idx);

      if(!(in_left || in_right)) {
        fprintf(stderr, "%s error: KDtree node %d: Vertex is inside me but not inside my sons!\n",
                __func__, nod_idx);
      }
      return in_left || in_right;
    }
    else {
      leaf_idx = nod_idx;
      return true;
    }
  }
  else
    return false;
}

/*
 * Note: Since we decombose the volume without holes / overlap, there should be only one box
 *       where the ref point is inside, or none.
 *
 */
bool kdtree::find_enclosing_box(const int nod_idx, const vec3r ref, int & box_idx) const
{
  const bbox & box       = boxes[nod_idx];
  const kdtree::node & n = nodes[nod_idx];

  if (cartesian_csys::vert_in_bbox(ref, box)) {
    box_idx = nod_idx;

    if (n.left > -1) {
      bool in_left  = find_enclosing_box(n.left,  ref, box_idx);
      bool in_right = find_enclosing_box(n.right, ref, box_idx);

      return in_left || in_right;
    } else
      return true;
  } else
    return false;
}

void kdtree::closest_vertex(const int nod_idx, const vec3r ref, int & idx, mt_real & len2, bool exclude_same) const
{
  const kdtree::node & n = nodes[nod_idx];

  if (verts[nod_idx] == NULL) {
    // current node is not a leaf yet -> recurse down both sides
    closest_vertex(n.left,  ref, idx, len2, exclude_same);
    closest_vertex(n.right, ref, idx, len2, exclude_same);
  }
  else {
    // current node is a leaf -> check all verts
    const mt_vector<int> & v = *verts[nod_idx];
    int num_verts = v.size();

    for (int i = 0; i < num_verts; i++) {
      const vec3r p = _xyz[v[i]];
      mt_real dist2 = (ref - p).length2();

      if (dist2 < len2) {
        if(!(exclude_same && dist2 == 0)) {
          len2 = dist2;
          idx = v[i];
        }
      }
    }
  }
}

void kdtree::clear_vertices()
{
  _xyz.resize(0);

  for(size_t i=0; i<verts.size(); i++)
    if(verts[i] != NULL) verts[i]->resize(0);
}

void kdtree::insert_vertices(const mt_vector<mt_real> & xyz)
{
  mt_vector<vec3r> newcoords;
  array_to_points(xyz, newcoords);

  mt_vector<int> newinds(newcoords.size());

  size_t old_numverts = _xyz.size();
  _xyz.append(newcoords.begin(), newcoords.end());

  for(size_t i=0; i<newinds.size(); i++)
    newinds[i] = old_numverts + i;

  for(size_t i = 0; i < newcoords.size(); i++) {
    int idx   = newinds[i];
    vec3r & v = newcoords[i];

    int leaf_idx;
    bool is_inside = find_enclosing_leaf(0, v, leaf_idx);

    if(is_inside) {
      verts[leaf_idx]->push_back(idx);
    }
    else {
      fprintf(stderr, "%s warning: Could not insert vertex since it is outside of kdtree bounding box!\n",
              __func__);
    }
  }
}

void kdtree::balance_stats(int & min_per_leaf, int & max_per_leaf, mt_real & avrg_per_leaf)
{
  min_per_leaf = 100000;
  max_per_leaf = 0;
  avrg_per_leaf = 0.0;
  mt_real numleafs = 0.0;

  if(verts.size() > 0) {
    for(size_t i=0; i<verts.size(); i++) {
      if(verts[i] != NULL) {
        int sz = verts[i]->size();
        if(min_per_leaf > sz) min_per_leaf = sz;
        if(max_per_leaf < sz) max_per_leaf = sz;
        avrg_per_leaf += sz;
        numleafs += 1.0f;
      }
    }
    avrg_per_leaf /= numleafs;
  }
  else {
    for(size_t i=0; i<tris.size(); i++) {
      if(tris[i] != NULL) {
        int sz = tris[i]->size();
        if(min_per_leaf > sz) min_per_leaf = sz;
        if(max_per_leaf < sz) max_per_leaf = sz;
        avrg_per_leaf += sz;
        numleafs += 1.0f;
      }
    }
    avrg_per_leaf /= numleafs;
  }
}

void kdtree::closest_vertex_in_region(const int nod_idx, const vec3r ref, const bbox & reg,
                                      int & idx, mt_real & len2, bool exclude_same) const
{
  const bbox & box       = boxes[nod_idx];
  const kdtree::node & n = nodes[nod_idx];

  if(cartesian_csys::bboxes_intersect(reg, box)) {
    if (verts[nod_idx] == NULL) {
      // current node is not a leaf yet -> recurse down both sides
      closest_vertex_in_region(n.left,  ref, reg, idx, len2, exclude_same);
      closest_vertex_in_region(n.right, ref, reg, idx, len2, exclude_same);
    }
    else {
      // current node is a leaf -> check all verts
      const mt_vector<int> & v = *verts[nod_idx];
      int num_verts = v.size();

      for (int i = 0; i < num_verts; i++) {
        const vec3r p = _xyz[v[i]];
        mt_real dist2 = cartesian_csys::distance2(ref, p);

        if (dist2 < len2) {
          if(!(exclude_same && dist2 == 0)) {
            len2 = dist2;
            idx = v[i];
          }
        }
      }
    }
  }
}

bool kdtree::closest_vertex(const vec3r ref, int & idx, vec3r & closest, mt_real & len2, bool exclude_same) const
{
  if(verts.size() == 0) {
    fprintf(stderr, "%s error: This function needs the kdtree to be filled with\n"
                    "vertices (not triangles)! Aborting!\n", __func__);
    exit(1);
  }

  #if 0
  struct timeval t1, t2;
  gettimeofday(&t1, NULL);
  #endif

  int leaf_idx = -1;
  len2 = FLT_MAX;

  bool is_inside = find_enclosing_leaf(0, ref, leaf_idx);

  if(is_inside) {
    // printf("Found enclosing leaf (%d)\n", leaf_idx);
    closest_vertex(leaf_idx, ref, idx, len2, exclude_same);

    vec3r cur = _xyz[idx];
    mt_real rad = (ref - cur).length();
    bbox search_region = cartesian_csys::bbox_from_sphere(ref, rad);

    // we search upwards for a node that encloses the search region,
    // if we find one other than our current leaf node, we have to redo
    // the search from there
    int enc_idx = enclosing_node_upwards(leaf_idx, search_region);

    if(enc_idx != leaf_idx) {
      // printf("need to verify, starting from leaf(%d)\n", enc_idx);
      closest_vertex_in_region(enc_idx, ref, search_region, idx, len2, exclude_same);
    }
  } else {
    // printf("didnt find enclosing leaf\n");
    int box_idx = -1;
    is_inside = find_enclosing_box(0, ref, box_idx);
    if(is_inside) {
      // printf("found enclosing box %d\n", box_idx);
      // at least we find a box where the vertex is inside, we can start from there
      // to search over all points
      closest_vertex(box_idx, ref, idx, len2, exclude_same);
    } else {
      // printf("doing full traversal :( ..\n");
      // worst case: we search over all points
      closest_vertex(0, ref, idx, len2, exclude_same);
    }
  }

  closest = _xyz[idx];

  #if 0
  gettimeofday(&t2, NULL);
  printf("KDtree vertex lookup in %g sec\n", timediff_sec(t1, t2));
  #endif

  return is_inside;
}

void kdtree::k_closest_vertices(const int k, const vec3r ref, mt_vector<mt_mixed_tuple<mt_real, int>> & vtx) const
{
  if(verts.size() == 0) {
    fprintf(stderr, "%s error: This function needs the kdtree to be filled with\n"
        "vertices (not triangles)! Aborting!\n", __func__);
    exit(EXIT_FAILURE);
  }

  if(size_t(k) > _xyz.size()) {
    fprintf(stderr, "%s error: k is larger than number of vertices in kdtree: k = %d ! Aborting!\n",
            __func__, k);
    exit(EXIT_FAILURE);
  }

  #if 0
  struct timeval t1, t2;
  gettimeofday(&t1, NULL);
  #endif

    // we set the current size to 0, since we want to start populating vtx anew in each call
  vtx.resize(0);
  // we reserve k*2 many items to have good performance when using vtx.push_back()
  vtx.reserve(k*2);

  // we first look at the vertices of the leaf that includes the reference point.
  int leaf_idx = -1;
  bool is_inside = find_enclosing_leaf(0, ref, leaf_idx);
  if(is_inside == false) {
    // should the reference point be outside the bounding box, we first search the
    // closest vertex and then pick the leaf of that vertex
    int closest_vert_idx;
    mt_real closest_vert_len2;
    closest_vertex(0, ref, closest_vert_idx, closest_vert_len2, false);
    find_enclosing_leaf(0, _xyz[closest_vert_idx], leaf_idx);
  }

  // we take the k closest vertices in this leaf. this gives us a good starting point for
  // our search volume
  for(const int v : *verts[leaf_idx])
    vtx.push_back({cartesian_csys::distance2(_xyz[v], ref), v});

  std::sort(vtx.begin(), vtx.end());
  if(vtx.size() > size_t(k)) vtx.resize(k);

  // the starting radius is the distance of the furthest away point from vtx.
  // In case we don't have enough vertices we take the upper node bbox
  mt_real current_rad;

  if(vtx.size() > size_t(k * 0.5))
   current_rad = std::sqrt(vtx[vtx.size()-1].v1);
  else {
   vec3r ctr;
   cartesian_csys::sphere_from_bbox(boxes[nodes[leaf_idx].up], current_rad, ctr);
  }

  bbox search_region = cartesian_csys::bbox_from_sphere(ref, current_rad);

  // if we havent found enough vertices in leaf node, or the search region is not a
  // subbox of our leaf (i.e. it also intersects other leafs), we need to do a volume search
  if(vtx.size() < size_t(k) || cartesian_csys::is_subbox(search_region, boxes[leaf_idx]) == false)
  {
    int start_idx = enclosing_node_upwards(leaf_idx, search_region);
    do {
      // we reset vtx so that we get unique values
      vtx.resize(0);

      vertices_in_sphere(start_idx, ref, current_rad, search_region, vtx);

      if(vtx.size() < size_t(k)) {
        current_rad *= 2.0;
        search_region = cartesian_csys::bbox_from_sphere(ref, current_rad);
        start_idx = enclosing_node_upwards(start_idx, search_region);
      }
    } while(vtx.size() < size_t(k));

    std::sort(vtx.begin(), vtx.end());
    vtx.resize(k);
  }

  if(vtx.size() != (size_t)(k)) {
   fprintf(stderr, "ERROR in %s: couldn't find %d neighbors! The final %zd neighbors are: \n",
           __func__, k, vtx.size());

   for(size_t i=0; i < vtx.size(); i++)
    fprintf(stderr, "(%g, %d) ", vtx[i].v1, vtx[i].v2);

   fprintf(stderr, "\n");
   exit(EXIT_FAILURE);
  }

  #if 0
  gettimeofday(&t2, NULL);
  printf("KDtree vertex lookup in %g sec\n", timediff_sec(t1, t2));
  #endif
}

void kdtree::vertices_in_sphere(const int nidx, const vec3r ref, const mt_real rad,
                                const bbox & search_region, mt_vector<mt_mixed_tuple<mt_real,int> > & vtx) const
{
  const bbox & box       = boxes[nidx];
  const kdtree::node & n = nodes[nidx];

  if(cartesian_csys::bboxes_intersect(search_region, box)) {
    if (verts[nidx] == NULL) {
      // current node is not a leaf yet -> recurse down both sides
      vertices_in_sphere(n.left,  ref, rad, search_region, vtx);
      vertices_in_sphere(n.right, ref, rad, search_region, vtx);
    }
    else {
      // current node is a leaf -> check all verts
      const mt_vector<int> & v = *(verts[nidx]);
      int num_verts = v.size();

      for (int i = 0; i < num_verts; i++) {
        const vec3r p = _xyz[v[i]];
        mt_real dist2 = cartesian_csys::distance2(ref, p);

        if (dist2 <= rad * rad)
          vtx.push_back({dist2, v[i]});
      }
    }
  }
}

void kdtree::vertices_in_sphere(const vec3r ref, const mt_real rad, mt_vector<mt_mixed_tuple<mt_real,int> > & vtx) const
{
  if(verts.size() == 0) {
    fprintf(stderr, "%s error: This function needs the kdtree to be filled with\n"
                    "vertices (not triangles)! Aborting!\n", __func__);
    exit(1);
  }

  // we set the current size to 0, since we want to start populating vtx anew in each call
  vtx.resize(0);
  // for performance reasons, we dont want to start at size 1
  vtx.reserve(128);

  bbox search_region = cartesian_csys::bbox_from_sphere(ref, rad);
  vertices_in_sphere(0, ref, rad, search_region, vtx);

  std::sort(vtx.begin(), vtx.end());
}

void kdtree::count_ray_intersects(const int nod_idx, const ray & r, const mt_real scale_min,
                                  const mt_real scale_max, int & count) const
{
  const bbox & box       = boxes[nod_idx];
  const kdtree::node & n = nodes[nod_idx];

  if (bbox_is_hit_3(r, box, scale_min, scale_max)) {
    if (tris[nod_idx] == NULL) {
      // current node is not a leaf yet -> recurse down both sides
      count_ray_intersects(n.left,  r, scale_min, scale_max, count);
      count_ray_intersects(n.right, r, scale_min, scale_max, count);
    }
    else
    {
      // current node is a leaf -> check all polys
      const mt_vector<tri_elem> & tr = *(tris[nod_idx]);
      int num_tri = tr.size();

      for (int i = 0; i < num_tri; i++)
      {
        mt_real u, v, scale;
        bool did_hit = ray_triangle_intersect(r, tr[i], scale, u, v);
        if (did_hit && scale > scale_min && scale < scale_max)
          count++;
      }
    }
  }
}

int kdtree::count_intersects(const ray & r, const mt_real scale_min, const mt_real scale_max) const
{
  int cnt = 0;
  count_ray_intersects(0, r, scale_min, scale_max, cnt);
  return cnt;
}

int kdtree::enclosing_node_upwards(const int start_idx, const bbox & ref_box) const
{
  int curidx = start_idx;
  if(curidx < 0) curidx = 0;

  const bbox* testbox = &boxes[curidx];

  while(curidx > 0 && cartesian_csys::is_subbox(ref_box, *testbox) == false) {
    curidx = nodes[curidx].up;
    if(curidx < 0) curidx = 0;

    testbox = &boxes[curidx];
  }

  return curidx;
}


int kdtree::enclosing_node_downwards(const int start_idx, const bbox & ref_box) const
{
  int curidx = start_idx, nextidx = start_idx;

  do {
    curidx = nextidx;
    // check if we have children
    if(nodes[curidx].left > 0) {
      if(cartesian_csys::is_subbox(ref_box, boxes[nodes[curidx].left]))        // check left
        nextidx = nodes[curidx].left;
      else if(cartesian_csys::is_subbox(ref_box, boxes[nodes[curidx].right]))  // check right
        nextidx = nodes[curidx].right;
      else
        nextidx = curidx;
    }
    else
      nextidx = curidx;
  } while(nextidx != curidx);

  return curidx;
}

vec3r get_closest_tri_point(const tri_elem & tri, const vec3r & ref)
{
  const vec3r & a = tri.v0;
  const vec3r & b = tri.v1;
  const vec3r & c = tri.v2;

  // from: https://github.com/embree/embree/blob/master/tutorials/common/math/closest_point.h

  const vec3r ab = b - a;
  const vec3r ac = c - a;
  const vec3r ap = ref - a;

  const float d1 = ab.scaProd(ap);
  const float d2 = ac.scaProd(ap);
  if (d1 <= 0.f && d2 <= 0.f) return a; //#1

  const vec3r bp = ref - b;
  const float d3 = ab.scaProd(bp);
  const float d4 = ac.scaProd(bp);
  if (d3 >= 0.f && d4 <= d3) return b; //#2

  const vec3r cp = ref - c;
  const float d5 = ab.scaProd(cp);
  const float d6 = ac.scaProd(cp);
  if (d6 >= 0.f && d5 <= d6) return c; //#3

  const float vc = d1 * d4 - d3 * d2;
  if (vc <= 0.f && d1 >= 0.f && d3 <= 0.f)
  {
      const float v = d1 / (d1 - d3);
      return a + ab * v; //#4
  }

  const float vb = d5 * d2 - d1 * d6;
  if (vb <= 0.f && d2 >= 0.f && d6 <= 0.f)
  {
      const float v = d2 / (d2 - d6);
      return a + ac * v; //#5
  }

  const float va = d3 * d6 - d5 * d4;
  if (va <= 0.f && (d4 - d3) >= 0.f && (d5 - d6) >= 0.f)
  {
      const float v = (d4 - d3) / ((d4 - d3) + (d5 - d6));
      return b + (c - b) * v; //#6
  }

  const float denom = 1.f / (va + vb + vc);
  const float v = vb * denom;
  const float w = vc * denom;
  return a + ab * v + ac * w; //#0
}

mt_real closest_bbox_dist(const bbox & box, const vec3r & p)
{
  vec3r cp;
  cp.x = clamp<mt_real>(p.x, box.bounds[0].x, box.bounds[1].x);
  cp.y = clamp<mt_real>(p.y, box.bounds[0].y, box.bounds[1].y);
  cp.z = clamp<mt_real>(p.z, box.bounds[0].z, box.bounds[1].z);

  mt_real mindist = (cp - p).length2();
  return mindist;
}

bool bbox_sph_intersect(const bbox & b, const vec3r ctr, const mt_real rad)
{
  mt_real dist2 = closest_bbox_dist(b, ctr);
  return dist2 < (rad * rad);
}

bool kdtree::search_closest_tri_downward(const mt_idx_t nod_idx, const vec3r & pos, tri_elem & e, vec3r & closest) const
{
  const node & n                = nodes[nod_idx];
  const mt_vector<tri_elem>* tr = tris[nod_idx];

  if (tr == NULL) {
    // current node is not a leaf yet -> recurse down both sides
    mt_real dist_left  = closest_bbox_dist(boxes[n.left] , pos);
    mt_real dist_right = closest_bbox_dist(boxes[n.right], pos);

    bool found = false;
    if(dist_left < dist_right)
      found = search_closest_tri_downward(n.left,  pos, e, closest);
    else
      found = search_closest_tri_downward(n.right, pos, e, closest);

    return found;
  } else {
    mt_real mindist = FLT_MAX;
    tri_elem minele;

    for (const tri_elem & tri : *tr) {
      vec3r test = get_closest_tri_point(tri, pos);
      mt_real dist = (test - pos).length2();
      if(mindist > dist) {
        closest = test;
        mindist = dist;
        minele = tri;
      }
    }

    e = minele;
    return true;
  }
}

bool kdtree::search_closest_tri_in_sphere(const mt_idx_t nod_idx, const vec3r & pos, const vec3r & sph_ctr, const mt_real sph_rad, tri_elem & e, vec3r & closest) const
{
  const node & n = nodes[nod_idx];
  const bbox & b = boxes[nod_idx];
  const mt_vector<tri_elem>* tr = tris[nod_idx];

  if (bbox_sph_intersect(b, sph_ctr, sph_rad)) {
    if (tr == NULL) {
      // current node is not a leaf yet -> recurse down both sides
      bool found_left  = search_closest_tri_in_sphere(n.left,  pos, sph_ctr, sph_rad, e, closest);
      bool found_right = search_closest_tri_in_sphere(n.right, pos, sph_ctr, sph_rad, e, closest);

      return found_left || found_right;
    } else {
      mt_real oldmindist = (closest - pos).length2();
      mt_real mindist = oldmindist;
      bool found_new = false;

      for (const tri_elem & tri : *tr) {
        vec3r test = get_closest_tri_point(tri, pos);
        mt_real dist = (test - pos).length2();

        if (mindist > dist) {
          mindist = dist;
          e = tri;
          closest = test;
          found_new = true;
        }
      }

      return found_new;
    }
  } else
    return false;
}

bool kdtree::closest_triangle(const vec3r & pos, tri_elem & e, vec3r & closest, bool exact) const
{
  bool found = search_closest_tri_downward(0, pos, e, closest);

  if(found && exact) {
    mt_real dist = (closest - pos).length();
    search_closest_tri_in_sphere(0, pos, pos, dist, e, closest);
  }

  return found;
}

vec3r random_point_in_unit_sphere_fast()
{
  rand_generator & rnd = GET_RND();

  vec3r v;
  mt_real l = 0.0;
  do {
    v.x = rnd.get_rand() * 2. - 1.;
    v.y = rnd.get_rand() * 2. - 1.;
    v.z = rnd.get_rand() * 2. - 1.;
    l = v.length2();
  } while(l > 1.0 || !l);

  return v;
}

bool inside_closed_surface(const kdtree & tree, const vec3r & pos)
{
  if(!cartesian_csys::vert_in_bbox(pos, tree.boxes[0]))
    return false;

  mt_real testcnt = 0.0;
  const int init_tests = 3, full_tests = 8;

  for(int i=0; i < init_tests; i++) {
    int num_intersect = tree.count_intersects(ray(pos, random_point_in_unit_sphere_fast()), 0.0, FLT_MAX);
    if(num_intersect % 2) testcnt += 1.0;
  }

  if((testcnt != mt_real(init_tests)) && (testcnt != 0)) {
    // the decision is not clear, we sample more and decide statistically
    const int num_tests = full_tests - init_tests;

    for(int i=0; i<num_tests; i++) {
      int num_intersect = tree.count_intersects(ray(pos, random_point_in_unit_sphere_fast()), 0.0, FLT_MAX);
      if(num_intersect % 2) testcnt += 1.0;
    }

    testcnt /= mt_real(full_tests);
    return testcnt > 0.5f;
  }
  else {
    // the decision is clear
    return testcnt == mt_real(init_tests);
  }
}


static vec3r random_point_on_unit_sphere()
{
  rand_generator & rnd = GET_RND();
  const mt_real theta = rnd.get_rand()*MT_PI;
  const mt_real phi = rnd.get_rand()*MT_PI*2.0;
  const mt_real x = std::sin(theta)*std::cos(phi);
  const mt_real y = std::sin(theta)*std::sin(phi);
  const mt_real z = std::cos(theta);
  return {x, y, z};
}


mt_real inside_closed_surface(const kdtree & tree, const vec3r & pos, const size_t num_tests)
{
  if (!((cartesian_csys::vert_in_bbox(pos, tree.boxes[0])) && (num_tests > 0)))
    return false;

  size_t inside_cnt = 0;
  for (size_t i=0; i<num_tests; i++) {
    const vec3r dvec = random_point_on_unit_sphere();
    const int num_intersect = tree.count_intersects(ray(pos, dvec), 0.0, DBL_MAX);
    if (num_intersect % 2) inside_cnt++;
  }
  return static_cast<mt_real>(inside_cnt)/static_cast<mt_real>(num_tests);
}


static inline bool intersect_in_eps(const kdtree & tree, const vec3r & pos, const vec3r & dir, const mt_real eps)
{
  ray r(pos, dir);
  tri_elem hit_buff;
  vec3r hit_pos;

  if(tree.closest_intersect(r, -1e100, 1e100, hit_buff, hit_pos)) {
    vec3r d = hit_pos - pos;
    return (d.length2() < eps);
  }

  return false;
}

bool on_surface(const kdtree & tree, const vec3r & pos, const vec3r & nrml, const mt_real eps)
{
  vec3r n = nrml;
  const mt_real eps2 = eps * eps;

  bool test = intersect_in_eps(tree, pos, n, eps2);
  if(test) {
    return true;
  } else {
    n *= mt_real(-1.0);
    test = intersect_in_eps(tree, pos, n, eps2);
    return test;
  }
}


