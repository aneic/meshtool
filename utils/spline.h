#ifndef SPLINE_HPP
#define SPLINE_HPP

#include <cstdio>
#include <cstdlib>
#include <limits>
#include <stdexcept>
#include <algorithm>
#include <cmath>
#include <iostream>
#include <sstream>
#include <limits>
#include <memory>


#include "mt_modes_base.h"

inline mt_real b3_spline(const mt_real x)
{
    mt_real absx = std::abs(x);
    if (absx < 1)
    {
        mt_real y = 2 - absx;
        mt_real z = 1 - absx;
        return (1. / 6.) *(y*y*y - 4*z*z*z);
    }
    if (absx < 2)
    {
        mt_real y = 2 - absx;
        return (1. / 6.) * y*y*y;
    }
    return (mt_real)(0);
}

inline mt_real b3_spline_prime(const mt_real x)
{
    if (x < 0)
    {
        return -b3_spline_prime(-x);
    }

    if (x < 1)
    {
        return x*(1.5*x - 2);
    }
    if (x < 2)
    {
        return -0.5*(2 - x)*(2 - x);
    }
    return (mt_real)(0.0);
}

// spline interpolation
class spline
{
private:
    mt_vector<mt_real> m_beta;
    mt_real m_h_inv;
    mt_real m_a;
    mt_real m_avg;
public:
    // set default boundary condition to be zero curvature at both ends
    spline() : m_beta(), m_h_inv(0.0), m_a(0.0), m_avg(0.0)
    {
        ;
    }
    template<class InputIterator>
    spline(InputIterator s, InputIterator e, const mt_real left_endpoint,
            const mt_real step_size, const mt_real left_endpoint_derivative = std::numeric_limits<mt_real>::quiet_NaN(),
            const mt_real right_endpoint_derivative = std::numeric_limits<mt_real>::quiet_NaN());

    mt_real operator() (const mt_real x) const;
    mt_real prime(const mt_real x) const;
};


template<class InputIterator>
spline::spline(InputIterator s, InputIterator e, const mt_real left_endpoint,
            const mt_real step_size, const mt_real left_endpoint_derivative,
            const mt_real right_endpoint_derivative) : m_a(left_endpoint), m_avg(0)
{
  const mt_real third = 1. / 3.;
  size_t length = std::distance(s,e);
  if (length < 5)
  {
      if (std::isnan(left_endpoint_derivative) || std::isnan(right_endpoint_derivative))
      {
          fprintf(stderr, "ERROR in %s: Interpolation using a cubic b spline with derivatives estimated at the endpoints requires at least 5 points.\n", __func__);
          exit(EXIT_FAILURE);
      }
      if (length < 3)
      {
          fprintf(stderr, "ERROR in %s: Interpolation using a cubic b spline requires at least 3 points.\n", __func__);
          exit(EXIT_FAILURE);
      }
  }
  if (std::isnan(left_endpoint))
  {
      fprintf(stderr, "ERROR in %s: Left endpoint is NAN; this is disallowed.\n", __func__);
      exit(EXIT_FAILURE);
  }
  if (left_endpoint + length*step_size >= (std::numeric_limits<mt_real>::max)())
  {
      fprintf(stderr, "ERROR in %s: Right endpoint overflows the maximum representable number of the specified precision.\n", __func__);
      exit(EXIT_FAILURE);
  }
  if (step_size <= 0)
  {
      fprintf(stderr, "ERROR in %s: The step size must be strictly > 0.\n", __func__);
      exit(EXIT_FAILURE);
  }

  m_h_inv = 1 / step_size;
  // Following Kress's notation, s'(a) = a1, s'(b) = b1
  mt_real a1 = left_endpoint_derivative;
  // See the finite-difference table on Wikipedia for reference on how
  // to construct high-order estimates for one-sided derivatives:
  // https://en.wikipedia.org/wiki/Finite_difference_coefficient#Forward_and_backward_finite_difference
  // Here, we estimate then to O(h^4), as that is the maximum accuracy we could obtain from this method.
  if (std::isnan(a1))
  {
      // For simple functions (linear, quadratic, so on)
      // almost all the error comes from derivative estimation.
      // This does pairwise summation which gives us another digit of accuracy over naive summation.
      mt_real t0 = 4*(s[1] + third*s[3]);
      mt_real t1 = -(25*third*s[0] + s[4])/4  - 3*s[2];
      a1 = m_h_inv*(t0 + t1);
  }
  mt_real b1 = right_endpoint_derivative;
  if (std::isnan(b1))
  {
      size_t n = length - 1;
      mt_real t0 = 4*(s[n-3] + third*s[n - 1]);
      mt_real t1 = -(25*third*s[n - 4] + s[n])/4  - 3*s[n - 2];
      b1 = m_h_inv*(t0 + t1);
  }
  // s(x) = \sum \alpha_i B_{3}( (x- x_i - a)/h )
  // Of course we must reindex from Kress's notation, since he uses negative indices which make C++ unhappy.
  m_beta.assign(length + 2, std::numeric_limits<mt_real>::quiet_NaN());
  // Since the splines have compact support, they decay to zero very fast outside the endpoints.
  // This is often very annoying; we'd like to evaluate the interpolant a little bit outside the
  // boundary [a,b] without massive error.
  // A simple way to deal with this is just to subtract the DC component off the signal, so we need the average.
  // This algorithm for computing the average is recommended in
  // http://www.heikohoffmann.de/htmlthesis/node134.html
  mt_real t = 1;
  for (size_t i = 0; i < length; ++i)
  {
      if (std::isnan(s[i]))
      {
          fprintf(stderr, "ERROR in %s: This function you are trying to interpolate is a nan at index %zd\n", __func__, i);
          exit(EXIT_FAILURE);
      }
      m_avg += (s[i] - m_avg) / t;
      t += 1;
  }
  // Now we must solve an almost-tridiagonal system, which requires O(N) operations.
  // There are, in fact 5 diagonals, but they only differ from zero on the first and last row,
  // so we can patch up the tridiagonal row reduction algorithm to deal with two special rows.
  // See Kress, equations 8.41
  // The the "tridiagonal" matrix is:
  // 1  0 -1
  // 1  4  1
  //    1  4  1
  //       1  4  1
  //          ....
  //          1  4  1
  //          1  0 -1
  // Numerical estimate indicate that as N->Infinity, cond(A) -> 6.9, so this matrix is good.
  mt_vector<mt_real> rhs(length + 2, std::numeric_limits<mt_real>::quiet_NaN());
  mt_vector<mt_real> super_diagonal(length + 2, std::numeric_limits<mt_real>::quiet_NaN());

  rhs[0] = -2*step_size*a1;
  rhs[rhs.size() - 1] = -2*step_size*b1;

  super_diagonal[0] = 0;

  for(size_t i = 1; i < rhs.size() - 1; ++i)
  {
      rhs[i] = 6*(s[i - 1] - m_avg);
      super_diagonal[i] = 1;
  }


  // One step of row reduction on the first row to patch up the 5-diagonal problem:
  // 1 0 -1 | r0
  // 1 4 1  | r1
  // mapsto:
  // 1 0 -1 | r0
  // 0 4 2  | r1 - r0
  // mapsto
  // 1 0 -1 | r0
  // 0 1 1/2| (r1 - r0)/4
  super_diagonal[1] = 0.5;
  rhs[1] = (rhs[1] - rhs[0])/4;

  // Now do a tridiagonal row reduction the standard way, until just before the last row:
  for (size_t i = 2; i < rhs.size() - 1; ++i)
  {
      mt_real diagonal = 4 - super_diagonal[i - 1];
      rhs[i] = (rhs[i] - rhs[i - 1])/diagonal;
      super_diagonal[i] /= diagonal;
  }

  // Now the last row, which is in the form
  // 1 sd[n-3] 0      | rhs[n-3]
  // 0  1     sd[n-2] | rhs[n-2]
  // 1  0     -1      | rhs[n-1]
  mt_real final_subdiag = -super_diagonal[rhs.size() - 3];
  rhs[rhs.size() - 1] = (rhs[rhs.size() - 1] - rhs[rhs.size() - 3])/final_subdiag;
  mt_real final_diag = -1/final_subdiag;
  // Now we're here:
  // 1 sd[n-3] 0         | rhs[n-3]
  // 0  1     sd[n-2]    | rhs[n-2]
  // 0  1     final_diag | (rhs[n-1] - rhs[n-3])/diag

  final_diag = final_diag - super_diagonal[rhs.size() - 2];
  rhs[rhs.size() - 1] = rhs[rhs.size() - 1] - rhs[rhs.size() - 2];


  // Back substitutions:
  m_beta[rhs.size() - 1] = rhs[rhs.size() - 1]/final_diag;
  for(size_t i = rhs.size() - 2; i > 0; --i)
  {
      m_beta[i] = rhs[i] - super_diagonal[i]*m_beta[i + 1];
  }
  m_beta[0] = m_beta[2] + rhs[0];
}

inline mt_real spline::operator()(const mt_real x) const
{
  // See Kress, 8.40: Since B3 has compact support, we don't have to sum over all terms,
  // just the (at most 5) whose support overlaps the argument.
  mt_real z = m_avg;
  mt_real t = m_h_inv*(x - m_a) + 1;

  size_t k_min = (size_t) (std::max)(static_cast<long>(0), static_cast<long>(std::trunc(std::ceil(t - 2))));
  size_t k_max = (size_t) (std::max)((std::min)(static_cast<long>(m_beta.size() - 1), static_cast<long>(std::trunc(std::floor(t + 2)))), (long) 0);
  for (size_t k = k_min; k <= k_max; ++k)
  {
      z += m_beta[k]*b3_spline(t - k);
  }
  return z;
}

inline mt_real spline::prime(const mt_real x) const
{
    mt_real z = 0;
    mt_real t = m_h_inv*(x - m_a) + 1;

    size_t k_min = (size_t) (std::max)(static_cast<long>(0), static_cast<long>(std::trunc(std::ceil(t - 2))));
    size_t k_max = (size_t) (std::min)(static_cast<long>(m_beta.size() - 1), static_cast<long>(std::trunc(std::floor(t + 2))));

    for (size_t k = k_min; k <= k_max; ++k)
    {
        z += m_beta[k]*b3_spline_prime(t - k);
    }
    return z*m_h_inv;
}


template<class RandomAccessContainer>
class cubic_hermite_spline {
public:

    cubic_hermite_spline(const RandomAccessContainer & x, RandomAccessContainer && y, RandomAccessContainer dydx)
     : x_(x), y_{std::move(y)}, dydx_{std::move(dydx)}
    {
        using std::abs;
        using std::isnan;
        if (x_.size() != y_.size())
        {
            throw std::domain_error("There must be the same number of ordinates as abscissas.");
        }
        if (x_.size() != dydx_.size())
        {
            throw std::domain_error("There must be the same number of ordinates as derivative values.");
        }
        if (x_.size() < 2)
        {
            throw std::domain_error("Must be at least two data points.");
        }
        mt_real x0 = x_[0];
        for (size_t i = 1; i < x_.size(); ++i)
        {
            mt_real x1 = x_[i];
            if (x1 <= x0)
            {
                std::ostringstream oss;
                oss.precision(std::numeric_limits<mt_real>::digits10+3);
                oss << "Abscissas must be listed in strictly increasing order x0 < x1 < ... < x_{n-1}, ";
                oss << "but at x[" << i - 1 << "] = " << x0 << ", and x[" << i << "] = " << x1 << ".\n";
                throw std::domain_error(oss.str());
            }
            x0 = x1;
        }
    }

    mt_real operator()(mt_real x) const
    {
        if  (x < x_[0] || x > x_[x_.size() -1] )
        {
            std::ostringstream oss;
            oss.precision(std::numeric_limits<mt_real>::digits10+3);
            oss << "Requested abscissa x = " << x << ", which is outside of allowed range ["
                << x_[0] << ", " << x_[x_.size() -1] << "]";
            throw std::domain_error(oss.str());
        }
        // We need t := (x-x_k)/(x_{k+1}-x_k) \in [0,1) for this to work.
        // Sadly this neccessitates this loathesome check, otherwise we get t = 1 at x = xf.
        if (x == x_[x_.size() -1])
        {
            return y_[y_.size()-1];
        }

        auto it = std::upper_bound(x_.begin(), x_.end(), x);
        auto i = std::distance(x_.begin(), it) -1;
        mt_real x0 = *(it-1);
        mt_real x1 = *it;
        mt_real y0 = y_[i];
        mt_real y1 = y_[i+1];
        mt_real s0 = dydx_[i];
        mt_real s1 = dydx_[i+1];
        mt_real dx = (x1-x0);
        mt_real t = (x-x0)/dx;

        // See the section 'Representations' in the page
        // https://en.wikipedia.org/wiki/Cubic_Hermite_spline
        mt_real y = (1-t)*(1-t)*(y0*(1+2*t) + s0*(x-x0))
              + t*t*(y1*(3-2*t) + dx*s1*(t-1));
        return y;
    }

    mt_real prime(mt_real x) const
    {
        if  (x < x_[0] || x > x_[x_.size()-1])
        {
            std::ostringstream oss;
            oss.precision(std::numeric_limits<mt_real>::digits10+3);
            oss << "Requested abscissa x = " << x << ", which is outside of allowed range ["
                << x_[0] << ", " << x_[x_.size()-1] << "]";
            throw std::domain_error(oss.str());
        }
        if (x == x_[x_.size()-1])
        {
            return dydx_[dydx_.size()-1];
        }
        auto it = std::upper_bound(x_.begin(), x_.end(), x);
        auto i = std::distance(x_.begin(), it) -1;
        mt_real x0 = *(it-1);
        mt_real x1 = *it;
        mt_real y0 = y_[i];
        mt_real y1 = y_[i+1];
        mt_real s0 = dydx_[i];
        mt_real s1 = dydx_[i+1];
        mt_real dx = (x1-x0);

        mt_real d1 = (y1 - y0 - s0*dx)/(dx*dx);
        mt_real d2 = (s1 - s0)/(2*dx);
        mt_real c2 = 3*d1 - 2*d2;
        mt_real c3 = 2*(d2 - d1)/dx;
        return s0 + 2*c2*(x-x0) + 3*c3*(x-x0)*(x-x0);
    }


    friend std::ostream& operator<<(std::ostream & os, const cubic_hermite_spline & m)
    {
        os << "(x,y,y') = {";
        for (size_t i = 0; i < m.x_.size() - 1; ++i)
        {
            os << "(" << m.x_[i] << ", " << m.y_[i] << ", " << m.dydx_[i] << "),  ";
        }
        auto n = m.x_.size()-1;
        os << "(" << m.x_[n] << ", " << m.y_[n] << ", " << m.dydx_[n] << ")}";
        return os;
    }

    size_t size() const
    {
        return static_cast<size_t>(x_.size());
    }

    int64_t bytes() const
    {
        return 3*x_.size()*sizeof(mt_real) + 3*sizeof(x_);
    }

    std::pair<mt_real, mt_real> domain() const
    {
        return {x_[0], x_[x_.size()-1]};
    }

    RandomAccessContainer x_;
    RandomAccessContainer y_;
    RandomAccessContainer dydx_;
};

template<class RandomAccessContainer>
class pchip {
public:
    pchip(const RandomAccessContainer & x, RandomAccessContainer && y,
          mt_real left_endpoint_derivative = std::numeric_limits<mt_real>::quiet_NaN(),
          mt_real right_endpoint_derivative = std::numeric_limits<mt_real>::quiet_NaN())
    {
        using std::isnan;
        if (x.size() < 4)
        {
            throw std::domain_error("Must be at least four data points.");
        }
        RandomAccessContainer s(x.size(), std::numeric_limits<mt_real>::quiet_NaN());
        if (isnan(left_endpoint_derivative))
        {
            // O(h) finite difference derivative:
            // This, I believe, is the only derivative guaranteed to be monotonic:
            s[0] = (y[1]-y[0])/(x[1]-x[0]);
        }
        else
        {
            s[0] = left_endpoint_derivative;
        }

        for (decltype(s.size()) k = 1; k < s.size()-1; ++k) {
            mt_real hkm1 = x[k] - x[k-1];
            mt_real dkm1 = (y[k] - y[k-1])/hkm1;

            mt_real hk = x[k+1] - x[k];
            mt_real dk = (y[k+1] - y[k])/hk;
            mt_real w1 = 2*hk + hkm1;
            mt_real w2 = hk + 2*hkm1;
            if ( (dk > 0 && dkm1 < 0) || (dk < 0 && dkm1 > 0) || dk == 0 || dkm1 == 0)
            {
                s[k] = 0;
            }
            else
            {
                s[k] = (w1+w2)/(w1/dkm1 + w2/dk);
            }

        }
        // Quadratic extrapolation at the other end:
        auto n = s.size();
        if (isnan(right_endpoint_derivative))
        {
            s[n-1] = (y[n-1]-y[n-2])/(x[n-1] - x[n-2]);
        }
        else
        {
            s[n-1] = right_endpoint_derivative;
        }
        impl_ = std::make_shared<cubic_hermite_spline<RandomAccessContainer>>(x, std::move(y), std::move(s));
    }

    mt_real operator()(mt_real x) const {
        return impl_->operator()(x);
    }

    mt_real prime(mt_real x) const {
        return impl_->prime(x);
    }

    friend std::ostream& operator<<(std::ostream & os, const pchip & m)
    {
        os << *m.impl_;
        return os;
    }

private:
    std::shared_ptr<cubic_hermite_spline<RandomAccessContainer>> impl_;
};



#endif /* SPLINE_HPP */
