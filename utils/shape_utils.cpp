#include "shape_utils.hpp"
#include "mesh_utils.h"

const vec3r v000(0.0, 0.0, 0.0);
const vec3r v100(1.0, 0.0, 0.0);
const vec3r v010(0.0, 1.0, 0.0);
const vec3r v110(1.0, 1.0, 0.0);
const vec3r v001(0.0, 0.0, 1.0);
const vec3r v101(1.0, 0.0, 1.0);
const vec3r v011(0.0, 1.0, 1.0);
const vec3r v111(1.0, 1.0, 1.0);

mt_real distance_triangle(const vec3r &p, const vec3r &v0, const vec3r &v1, const vec3r &v2, bool &behind)
{
  vec3r proj(p);

  const vec3r u0(v1-v0), u1(v2-v0), u2(v2-v1); // AB, AC, BC
  const vec3r x0(p-v0), x1(p-v1), x2(p-v2);    // AX, BX, CX

  vec3r w0, w1, w2;
  w0 = u0; 
  w0.normalize();
  w1 = u1-w0*(w0.scaProd(u1)/w0.length2()); 
  w1.normalize();
  w2 = w0.crossProd(w1);
  w2.normalize();

  const mt_real a0 = w0.scaProd(x0);
  const mt_real a1 = w1.scaProd(x0);
  const mt_real a2 = w2.scaProd(x0);
  behind = (a2 < 0.0);

  const vec3r plane_p(v0+w0*a0+w1*a1);

  const mt_real n0 = u0.length2(); // ||AB||^2
  const mt_real n1 = u1.length2(); // ||AC||^2
  const mt_real n2 = u2.length2(); // ||BC||^2

  const mt_real d0 =  u0.scaProd(x0); // AB.AX
  const mt_real d1 =  u1.scaProd(x0); // AC.AX
  const mt_real d2 = -u0.scaProd(x1); // BA.BX = -AB.BX
  const mt_real d3 =  u2.scaProd(x1); // BC.BX
  const mt_real d4 = -u1.scaProd(x2); // CA.CX = -AC.BX
  const mt_real d5 = -u2.scaProd(x2); // CB.CX = -BC.BX

  //const mt_real d6 =  x1.scaProd(u2.crossProd(u0).crossProd(u0)); // [(BCxBA)xBA].BX =  BX.[(BCxAB)xAB]
  //const mt_real d7 = -x2.scaProd(u2.crossProd(u1).crossProd(u1)); // [(CBxCA)xCA].CX = -CX.[(BCxAC)xAC]
  //const mt_real d8 = -x2.scaProd(u1.crossProd(u2).crossProd(u2)); // [(CAxBC)xBC].CX = -CX.[(ACxBC)xBC]

  const mt_real d6 =  x0.scaProd(u0.crossProd(w2));
  const mt_real d7 = -x0.scaProd(u1.crossProd(w2));
  const mt_real d8 =  x1.scaProd(u2.crossProd(w2));

  if ((d0 <= 0.0) && (d1 <= 0.0)) proj = v0;
  else if ((d2 <= 0.0) && (d3 <= 0.0)) proj = v1;
  else if ((d4 <= 0.0) && (d5 <= 0.0)) proj = v2;
  else if ((d0  > 0.0) && (d2  > 0.0) && (d6 >= 0.0)) proj = v0+u0*(d0/n0);
  else if ((d1  > 0.0) && (d4  > 0.0) && (d7 >= 0.0)) proj = v0+u1*(d1/n1);
  else if ((d3  > 0.0) && (d5  > 0.0) && (d8 >= 0.0)) proj = v1+u2*(d3/n2);
  else proj = plane_p;

  return (p-proj).length();
}

mt_real distance_parallelogram(const vec3r &p, const vec3r &v0, const vec3r &v1, const vec3r &v2, bool &behind)
{
  vec3r proj(p);

  const vec3r u0(v1-v0), u1(v2-v0); // AB, AC, BD=AC, CD=AB
  const vec3r x0(p-v0), x1(p-v1), x2(p-v2), x3(p+v0-v1-v2);  // AX, BX, CX, DX

  vec3r w0, w1, w2;
  w0 = u0; 
  w0.normalize();
  w1 = u1-w0*(w0.scaProd(u1)/w0.length2()); 
  w1.normalize();
  w2 = w0.crossProd(w1);
  w2.normalize();

  const mt_real a0 = w0.scaProd(x0);
  const mt_real a1 = w1.scaProd(x0);
  const mt_real a2 = w2.scaProd(x0);
  behind = (a2 < 0.0);

  const vec3r plane_p(v0+w0*a0+w1*a1);

  const mt_real n0 = u0.length2(); // ||AB||^2
  const mt_real n1 = u1.length2(); // ||AC||^2

  const mt_real d0 =  u0.scaProd(x0); // AB.AX
  const mt_real d1 =  u1.scaProd(x0); // AC.AX
  const mt_real d2 = -u0.scaProd(x1); // BA.BX = -AB.BX
  const mt_real d3 =  u1.scaProd(x1); // BD.BX =  AC.BX

  const mt_real d4 = -u1.scaProd(x2); // CA.CX = -AC.CX
  const mt_real d5 =  u0.scaProd(x2); // CD.CX =  AB.CX 
  const mt_real d6 = -u1.scaProd(x3); // DB.DX = -BD.DX = -AC.DX
  const mt_real d7 = -u0.scaProd(x3); // DC.DX = -CD.DX = -AB.DX

  const mt_real d8  =  x0.scaProd(u0.crossProd(w2));
  const mt_real d9  = -x0.scaProd(u1.crossProd(w2));
  const mt_real d10 =  x1.scaProd(u1.crossProd(w2));
  const mt_real d11 = -x2.scaProd(u0.crossProd(w2));

  if ((d0 <= 0.0) && (d1 <= 0.0)) proj = v0;
  else if ((d2 <= 0.0) && (d3 <= 0.0)) proj = v1;
  else if ((d4 <= 0.0) && (d5 <= 0.0)) proj = v2;
  else if ((d6 <= 0.0) && (d7 <= 0.0)) proj = v1+v2-v0;
  else if ((d0  > 0.0) && (d2  > 0.0) && (d8  >= 0.0)) proj = v0+u0*(d0/n0);
  else if ((d1  > 0.0) && (d4  > 0.0) && (d9  >= 0.0)) proj = v0+u1*(d1/n1);
  else if ((d3  > 0.0) && (d6  > 0.0) && (d10 >= 0.0)) proj = v1+u1*(d3/n1);
  else if ((d5  > 0.0) && (d7  > 0.0) && (d11 >= 0.0)) proj = v2+u0*(d5/n0);
  else proj = plane_p;

  return (p-proj).length();
}

vec3r data_gradient(const vec3r* lgrad,
                    const mt_vector<vec3r>   & vtx,
                    const mt_vector<mt_real> & data)
{
  // data gradient on reference element
  vec3r func_lgrad;

  // transformation matrix (DF)^(-T)
  mat3x3r tmat;

  // we take the number of vertex points from the vtx array
  size_t N = vtx.size();

  for (size_t i=0; i<N; i++) {
    tmat += outerProd(lgrad[i], vtx[i]);
    func_lgrad += lgrad[i]*data[i];
  }

  invert_mat3x3(tmat);
  return tmat.dotProd(func_lgrad);
}

vec3r data_gradient_2D(const vec3r* lgrad,
                       const mt_vector<vec3r>   & vtx,
                       const mt_vector<mt_real> & data)
{
  // data gradient on reference element
  vec3r func_lgrad;

  // transformation matrix (DF)^(-T)
  mat3x3r tmat;

  // we take the number of vertex points from the vtx array
  size_t N = vtx.size();

  for (size_t i=0; i<N; i++) {
    tmat += outerProd(lgrad[i], vtx[i]);
    func_lgrad += lgrad[i]*data[i];
  }

  invert_mat2x2(tmat);
  return tmat.dotProd(func_lgrad);
}

inline void transform_pts_2D(elem_t type, mt_vector<vec3r> & vtx, vec3r & x, vec3r & y, vec3r & z)
{
  switch(type) {
    case Line: {
      vec3r p0 = vtx[0], p1 = vtx[1];

      vtx[0] = {0, 0, 0};
      vtx[1] = {(p1 - p0).length(), 0, 0};
      break;
    }

    case Tri: {
      vec3r p0 = vtx[0], p1 = vtx[1], p2 = vtx[2];
      vec3r p01 = p1 - p0, p02 = p2 - p0;

      x = unit_vector(p01);
      z = unit_vector(p01.crossProd(p02));
      y = x.crossProd(z);

      vtx[0] = {0, 0, 0};
      vtx[1] = {p01.length(), 0, 0};
      vtx[2] = {p02.scaProd(x), p02.scaProd(y), 0};
      break;
    }

    case Quad: {
      vec3r p0 = vtx[0], p1 = vtx[1], p2 = vtx[2], p3 = vtx[3];
      vec3r p01 = p1 - p0, p02 = p2 - p0, p03 = p3 - p0;

      x = unit_vector(p01);
      z = unit_vector(p01.crossProd(p02));
      y = x.crossProd(z);

      vtx[0] = {0, 0, 0};
      vtx[1] = {p01.length(), 0, 0};
      vtx[2] = {p02.scaProd(x), p02.scaProd(y), 0};
      vtx[3] = {p03.scaProd(x), p03.scaProd(y), 0};
      break;
    }

    default: break;
  }
}

void compute_gradient(const mt_meshdata& mesh,
                      const MT_USET<mt_tag_t> & comp_tags,
                      const mt_vector<mt_real> & data,
                      const bool nodal_input, const bool nodal_output,
                      mt_vector<vec3r> & grad,
                      mt_vector<mt_real> & mag)
{
  mt_vector<mt_real> emag, ndata, *_mag = NULL;
  const mt_vector<mt_real> *_data = NULL;
  mt_vector<vec3r>   egrad, *_grad = NULL;

  // the core algorithm computes gradients of a nodal function into the
  // element centers. is we want different input / output representation
  // we need to interpolate
  if(nodal_input) {
    _data = &data;
  } else {
    elemData_to_nodeData(mesh, data, ndata);
    _data = &ndata;
  }

  if(nodal_output) {
    _grad = &egrad;
    _mag  = &emag;
  } else {
    _grad = &grad;
    _mag  = &mag;
  }

  size_t numele = mesh.e2n_cnt.size();
  _grad->assign(numele);
  _mag->assign(numele);
  const bool have_comp_tags = comp_tags.size() > 0;

  #ifdef OPENMP
  #pragma omp parallel
  #endif
  {
    // vertex and data buffer
    mt_vector<vec3r> vtx(8);
    mt_vector<mt_real> val(8);
    reference_grad rg;

    #ifdef OPENMP
    #pragma omp for schedule(guided)
    #endif
    for(size_t i=0; i<numele; i++)
    {
      const mt_idx_t ctag = mesh.etags[i];
      bool skip_elem = have_comp_tags && comp_tags.count(ctag) == 0;
      if(!skip_elem) {
        const mt_idx_t* con = mesh.e2n_con.data() + mesh.e2n_dsp[i];
        const mt_real* xyz = mesh.xyz.data();
        vec3r dgrad;

        int cnt = mesh.e2n_cnt[i];
        vtx.resize(cnt);
        val.resize(cnt);
        for(int j=0; j < cnt; j++) {
          vtx[j].get(xyz + 3*con[j]);
          val[j] = (*_data)[con[j]];
        }

        switch(mesh.etype[i]) {
          case Tetra:
            dgrad = data_gradient(rg.lgrad_tet, vtx, val);
            break;

          case Prism:
            dgrad = data_gradient(rg.lgrad_pri, vtx, val);
            break;

          case Pyramid:
            dgrad = data_gradient(rg.lgrad_pyr, vtx, val);
            break;

          case Hexa:
            dgrad = data_gradient(rg.lgrad_hex, vtx, val);
            break;

          case Tri: {
            vec3r x, y, z;
            transform_pts_2D(Tri, vtx, x, y, z);
            dgrad = data_gradient_2D(rg.lgrad_tri, vtx, val);
            dgrad = x * dgrad.x + y * dgrad.y + z * dgrad.z;
            break;
          }

          case Quad: {
            vec3r x, y, z;
            transform_pts_2D(Quad, vtx, x, y, z);
            dgrad = data_gradient_2D(rg.lgrad_quad, vtx, val);
            dgrad = x * dgrad.x + y * dgrad.y + z * dgrad.z;
            break;
          }

          default: break;
        }

        (*_grad)[i] = dgrad;
        (*_mag)[i]  = dgrad.length();
      }
    }
  }

  if(nodal_output) {
    elemData_to_nodeData(mesh, egrad, grad);
    elemData_to_nodeData(mesh, emag,  mag);
  }
}

void compute_current_density(const mt_meshdata & mesh,
                             const mt_vector<mat3x3r> & cond,
                             const mt_vector<mt_real> & phie,
                             mt_vector<vec3r> & J_field)
{
  mt_vector<vec3r> grad;
  mt_vector<mt_real> gradmag;
  MT_USET<mt_tag_t> tags;

  compute_gradient(mesh, tags, phie , true, false, grad, gradmag);
  J_field.resize(cond.size());

#ifdef OPENMP
  #pragma omp parallel for schedule(guided)
#endif
  for(size_t eidx=0; eidx < cond.size(); eidx++)
    J_field[eidx] = cond[eidx] * grad[eidx];
}


