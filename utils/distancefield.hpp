#include <math.h>
#include <iostream>

#ifndef DISTANCEFIELD_H
#define DISTANCEFIELD_H

#define EK_EPS 1e-16
// #define EK_HIGHPREC

#define DIST_ACC 1e-6
#define EK_COEFSZ 16 // the size of the coefficients block, at least 14 is needed
#define EK_INF 1e100

#ifdef EK_HIGHPREC
#define ek_double long double
#define ek_sqrt sqrtl
#else
#define ek_double double
#define ek_sqrt sqrt
#endif


//#define EK_PRINT_STATS

template<class S> inline
S dot(S* a, S* b)
{
  S ret = a[0]*b[0] + a[1]*b[1] + a[2]*b[2];
  return ret;
}

template<class T, class S, class V>
class distfield_generator
{
public:
  struct initdata
  {
    mt_vector<T> nod;  ///< Activation nodes.
    mt_vector<S> val;  ///< Activation times.
  };

private:
  // e2n: graph connecting elements to nodes
  // n2e: graph connecting nodes to elements, thus e2n transposed
  // n2n: graph connecting nodes with nodes
  const mt_meshdata &  _mesh;
  const mt_vector<S> & _xyz;

  // initial nodes and their respective value
  const initdata & _init;

  size_t _numNodes;
  size_t _numElems;

  V _initval;
  bool _is_3d;

  mt_vector<T> _act_nod;
  size_t       _act_idx;

  mt_vector<bool> _fin_nod_map;
  mt_vector<bool> _act_nod_map;

  mt_vector<T>         _nod_upd;
  mt_vector<ek_double> _sca_coeff;

  void get_base(const T* con, const int size, const T v, T* base)
  {
    for(int i=0, widx=0; i<size; i++){
      if(con[i] != v)
        base[widx++] = con[i];
    }
  }

  inline void nodal_expand(mt_vector<V> & phi, mt_vector<V> & phi_old, const T v, const V vval)
  {
    bool active;
    mt_idx_t start = _mesh.n2n_dsp[v], stop = start + _mesh.n2n_cnt[v];

    for(int j = start; j < stop; j++) {
      mt_idx_t nidx = _mesh.n2n_con[j];

      if( (nidx != v) && (_fin_nod_map[nidx] == false) ) {
        #ifdef OPENMP
        #pragma omp atomic read
        #endif
        active = _act_nod_map[nidx];

        if( !active ) {
          V nval = phi_old[nidx];

          if(nval > vval) {
            V newval = solve_nbh(phi_old, nidx);
            if( (nval - newval) > (DIST_ACC * (1 + newval)) )
            {
              #ifdef OPENMP
              #pragma omp atomic write
              #endif
              phi[nidx] = newval;

              _nod_upd[nidx]++;

              #ifdef OPENMP
              #pragma omp atomic write
              #endif
              _act_nod_map[nidx] = true;
            }
          }
        }
      }
    }
  }

  // solve line for v2
  inline V solve_line(const V phi1, const S* x1, const S* x2)
  {
     // we solve travel time from x1 to x2
    ek_double e12[3];
    e12[0] = x2[0] - x1[0]; e12[1] = x2[1] - x1[1]; e12[2] = x2[2] - x1[2];

    V sol = phi1 + sqrt(dot(e12, e12));
    return sol;
  }

  // The solve_triangle version computing the products on-the-fly
  inline V solve_triangle(const V f1, const V f2, const S* x1, const S* x2, const S* x3)
  {
    // phi
    V f12 = f2 - f1, f12_abs = fabs(f12);

    bool f12_bad = (f12_abs < EK_EPS) || (f12_abs == EK_INF);
    if(f12_bad)
      return EK_INF;

    // compute edges
    ek_double e12[3], e23[3];

    e12[0] = x2[0] - x1[0]; e12[1] = x2[1] - x1[1]; e12[2] = x2[2] - x1[2];
    e23[0] = x3[0] - x2[0]; e23[1] = x3[1] - x2[1]; e23[2] = x3[2] - x2[2];

    ek_double a = dot(e23, e23);
    ek_double b = dot(e23, e12);
    ek_double c = dot(e12, e12);

    ek_double sqrt_val, p1, p2, l1, l2;
    sqrt_val = -b*b*c + a*c*c + (b*b - a*c)*f12*f12;
    if(sqrt_val < 0)
      return EK_INF;

    sqrt_val = ek_sqrt(sqrt_val);
    p1 = b*f12*f12 - b*c;
    p2 = c*f12*f12 - c*c;

    l1 = -( p1 - sqrt_val*f12 ) / p2;
    l2 = -( p1 + sqrt_val*f12 ) / p2;

    V ret1 = EK_INF, ret2 = EK_INF;

    if( (l1 > 0) && (l1 < 1) )
      ret1 = l1 * f1 + (1-l1) * f2 + ek_sqrt(a + 2*b*l1 + l1*l1*c);

    if( (l2 > 0) && (l2 < 1) )
      ret2 = l2 * f1 + (1-l2) * f2 + ek_sqrt(a + 2*b*l2 + l2*l2*c);

    V minsol = ret1;
    if(minsol > ret2) minsol = ret2;

    return minsol;
  }

  V solve_tetra(V phi1, V phi2, V phi3, const S* x1, const S* x2, const S* x3, const S* x4)
  {
    // phi
    V phi23 = phi3 - phi2, phi23_abs = fabs(phi23);
    V phi13 = phi3 - phi1, phi13_abs = fabs(phi13);

    bool phi23_bad = (phi23_abs < EK_EPS) || (phi23_abs == EK_INF);
    bool phi13_bad = (phi13_abs < EK_EPS) || (phi13_abs == EK_INF);

    if(phi23_bad || phi13_bad)
      return V(EK_INF);

    S e13[3], e23[3], e34[3];
    e13[0] = x3[0] - x1[0]; e13[1] = x3[1] - x1[1]; e13[2] = x3[2] - x1[2];
    e23[0] = x3[0] - x2[0]; e23[1] = x3[1] - x2[1]; e23[2] = x3[2] - x2[2];
    e34[0] = x4[0] - x3[0]; e34[1] = x4[1] - x3[1]; e34[2] = x4[2] - x3[2];

    ek_double a, b, c, d, e, f;
    a = dot(e34, e34);
    b = dot(e34, e13);
    c = dot(e13, e13);
    d = dot(e23, e13);
    e = dot(e23, e34);
    f = dot(e23, e23);


    // solve quadratic system
    ek_double sqrt_val, p1, p2, l2_1, l2_2, l1_1, l1_2;

    sqrt_val = -e*e*c*d*d + a*d*d*d*d - (b*b*c - a*c*c)*f*f + ((b*b - a*c)*f*f -
               (e*e*c - a*d*d - 2*(e*c - b*d)*e)*f)*phi13*phi13 - 2*(e*e*c*d -
               2*e*b*d*d + a*d*d*d + (b*b - a*c)*d*f)*phi13*phi23 + (e*e*c*c -
               2*e*b*c*d + a*c*d*d + (b*b*c - a*c*c)*f)*phi23*phi23 + 2*(e*c*d*d -
               b*d*d*d)*e + (e*e*c*c + (b*b - 2*a*c)*d*d - 2*(e*c*c - b*c*d)*e)*f;

    if(sqrt_val < 0)
      return V(EK_INF);

    sqrt_val = ek_sqrt(sqrt_val);

    p1 = e*c*d*d - b*d*d*d + (e*c - b*d)*f*phi13*phi13 - 2*(e*c*d - b*d*d)*phi13*phi23 +
           (e*c*c - b*c*d)*phi23*phi23 - (e*c*c - b*c*d)*f;

    p2 = (d*d*d*d - 2*c*d*d*f + c*c*f*f + (d*d*f - c*f*f)*phi13*phi13 - 2*(d*d*d - c*d*f)*phi13*phi23
         + (c*d*d - c*c*f)*phi23*phi23);

    l2_1 = (p1 + sqrt_val*(d*phi13 - c*phi23)) / p2;
    l2_2 = (p1 - sqrt_val*(d*phi13 - c*phi23)) / p2;

    l1_1 = -((f*l2_1 + e)*phi13 - (d*l2_1 + b)*phi23)/(d*phi13 - c*phi23);
    l1_2 = -((f*l2_2 + e)*phi13 - (d*l2_2 + b)*phi23)/(d*phi13 - c*phi23);

    // compute phi4
    V phi4_1 = EK_INF, phi4_2 = EK_INF;

    V l3 = 1 - l1_1 - l2_1;
    if( (l1_1 > 0. && l1_1 < 1.) && (l2_1 > 0. && l2_1 < 1.) && l3 > 0 )
    {
      phi4_1 = l1_1 * phi1 + l2_1 * phi2 + (1-l1_1-l2_1)*phi3 +
               ek_sqrt(a + 2*l1_1*b + l1_1*l1_1*c + 2*l1_1*l2_1*d + 2*l2_1*e + l2_1*l2_1*f);
    }
    l3 = 1 - l1_2 - l2_2;
    if( (l1_2 > 0. && l1_2 < 1.) && (l2_2 > 0. && l2_2 < 1.) && l3 > 0 )
    {
      phi4_2 = l1_2 * phi1 + l2_2 * phi2 + (1-l1_2-l2_2)*phi3 +
               ek_sqrt(a + 2*l1_2*b + l1_2*l1_2*c + 2*l1_2*l2_2*d + 2*l2_2*e + l2_2*l2_2*f);
    }

    V minsol = phi4_1;
    if(minsol > phi4_2) minsol = phi4_2;

    return minsol;
  }

  inline V solve_2D(T* base, const mt_vector<V> & phi, const T eidx, const T v)
  {
    const T* tri = _mesh.e2n_con.data() + _mesh.e2n_dsp[eidx];
    get_base(tri, 3, v, base);

    T v1 = base[0], v2 = base[1];
    S x[9];
    x[0*3+0] = _xyz[v1*3+0];
    x[0*3+1] = _xyz[v1*3+1];
    x[0*3+2] = _xyz[v1*3+2];

    x[1*3+0] = _xyz[v2*3+0];
    x[1*3+1] = _xyz[v2*3+1];
    x[1*3+2] = _xyz[v2*3+2];

    x[2*3+0] = _xyz[v*3+0];
    x[2*3+1] = _xyz[v*3+1];
    x[2*3+2] = _xyz[v*3+2];

    V phi1, phi2;
    #ifdef OPENMP
    #pragma omp atomic read
    #endif
    phi1 = phi[v1];
    #ifdef OPENMP
    #pragma omp atomic read
    #endif
    phi2 = phi[v2];

    if( (phi1 == EK_INF) && (phi2 == EK_INF) )
      return EK_INF;

    // solve 2D
    V sol = solve_triangle(phi1, phi2, x, x+3, x+6);
    if (sol == EK_INF) {
      // solve 1D
      V sol1, sol2;

      sol1 = solve_line(phi1, x,   x+6);
      sol2 = solve_line(phi2, x+3, x+6);

      sol = sol1;
      if(sol > sol2) sol = sol2;
    }
    return sol;
  }

  // get 1D minimal phi4 at v4 for one tetra defined by v1 to v4
  V get_min_1D(const V phi1, const V phi2, const V phi3, const S* x)
  {
    V sol1, sol2, sol3, minsol;

    sol1 = solve_line(phi1, x,   x+9);
    sol2 = solve_line(phi2, x+3, x+9);
    sol3 = solve_line(phi3, x+6, x+9);
    minsol = sol1;
    if(minsol > sol2) minsol = sol2;
    if(minsol > sol3) minsol = sol3;

    return minsol;
  }

  // get 2D minimal phi4 at v4 for one tetra defined by v1 to v4
  V get_min_2D(const V phi1, const V phi2, const V phi3, const S* x)
  {
    V sol1, sol2, sol3, minsol;

    sol1 = solve_triangle(phi1, phi2, x,   x+3, x+9);
    sol2 = solve_triangle(phi2, phi3, x+3, x+6, x+9);
    sol3 = solve_triangle(phi1, phi3, x,   x+6, x+9);
    minsol = sol1;
    if(minsol > sol2) minsol = sol2;
    if(minsol > sol3) minsol = sol3;

    return minsol;
  }

  // solve tetra with index eidx, for vertex with index v
  inline V solve_3D(T* base, const mt_vector<V> & phi, const T eidx, const T v)
  {
    const T* tet = _mesh.e2n_con.data() + _mesh.e2n_dsp[eidx];
    get_base(tet, 4, v, base);

    const T v1 = base[0], v2 = base[1], v3 = base[2];
    S x[12];
    x[0*3+0] = _xyz[v1*3+0];
    x[0*3+1] = _xyz[v1*3+1];
    x[0*3+2] = _xyz[v1*3+2];

    x[1*3+0] = _xyz[v2*3+0];
    x[1*3+1] = _xyz[v2*3+1];
    x[1*3+2] = _xyz[v2*3+2];

    x[2*3+0] = _xyz[v3*3+0];
    x[2*3+1] = _xyz[v3*3+1];
    x[2*3+2] = _xyz[v3*3+2];

    x[3*3+0] = _xyz[v*3+0];
    x[3*3+1] = _xyz[v*3+1];
    x[3*3+2] = _xyz[v*3+2];

    V phi1, phi2, phi3;
    phi1 = phi[v1];
    phi2 = phi[v2];
    phi3 = phi[v3];

    if( (phi1 == EK_INF) && (phi2 == EK_INF) && (phi3 == EK_INF) )
      return EK_INF;

    // solve 3D
    V sol = solve_tetra(phi1, phi2, phi3, x, x+3, x+6, x+9);
    if(sol == EK_INF) {
      // solve 2D
      sol = get_min_2D(phi1, phi2, phi3, x);
      if (sol == EK_INF) {
        // solve 1D
        sol = get_min_1D(phi1, phi2, phi3, x);
      }
    }

    return sol;
  }


  // solve for vertex v
  // this involves computing 3D-1D solutions for all elements connected to v,
  // then taking the minimal one
  inline V solve_nbh(const mt_vector<V> & phi, const T v)
  {
    T base[3];
    V sol = EK_INF, minsol = EK_INF;

    // iterate over elements
    T start = _mesh.n2e_dsp[v], end = start + _mesh.n2e_cnt[v];

    for(T i=start, sidx=0; i<end; i++, sidx++)
    {
      T eidx  = _mesh.n2e_con[i];

      if      (_is_3d)                   sol = solve_3D(base, phi, eidx, v);
      else if (_mesh.e2n_cnt[eidx] == 3) sol = solve_2D(base, phi, eidx, v);
      else if (_mesh.e2n_cnt[eidx] == 2) {
        const mt_idx_t* con = _mesh.e2n_con.data() + _mesh.e2n_dsp[eidx];
        mt_idx_t v_start, v_end;
        if(con[0] == v) {
          v_end   = v;
          v_start = con[1];
        } else {
          v_end   = v;
          v_start = con[0];
        }

        mt_real phi_start = phi[v_start];
        sol = solve_line(phi_start, _mesh.xyz.data() + v_start*3, _mesh.xyz.data() + v_end*3);
      }

      if(minsol > sol) minsol = sol;
    }

    return minsol;
  }

public:
  distfield_generator(const mt_meshdata & mesh,
                      const mt_vector<S> & xyz,
                 const struct initdata & init):
                 _mesh(mesh),
                 _xyz(xyz),
                 _init(init),
                 _numNodes(xyz.size() / 3),
                 _numElems(mesh.e2n_cnt.size())
  {
    _fin_nod_map.resize(_numNodes, false);
    _act_nod_map.resize(_numNodes, false);

    _nod_upd.resize(_numNodes, 0);
    _act_nod.resize(_numNodes);

    _initval = EK_INF;

    _is_3d = mesh.etype[0] == Tetra;
  }

  inline void operator()(mt_vector<V> & phi, V UR_val)
  {
    phi.assign(_numNodes, _initval);
    mt_vector<V> phi_old;

    // initialize active nodes and phi
    for(size_t i=0; i<_init.nod.size(); i++)
    {
      T nidx = _init.nod[i];
      _fin_nod_map[nidx] = true;
      phi[nidx] = _init.val[i];
    }
    for(const T nidx : _init.nod) {
      mt_idx_t start = _mesh.n2n_dsp[nidx], end = start + _mesh.n2n_cnt[nidx];
      for(int j=start; j<end; j++) {
        T idx = _mesh.n2n_con[j];
        if(_fin_nod_map[idx] == false)
          _act_nod_map[idx] = true;
      }
    }

    // set up active list
    _act_idx = 0;
    for(size_t i=0; i<_numNodes; i++)
      if(_act_nod_map[i]) _act_nod[_act_idx++] = i;

    // solve for active nodes ---------------------------------------------
    size_t loopidx = 0;
    size_t numConv;

    phi_old = phi;

    while( _act_idx > 0 )
    {
      loopidx ++;
      numConv = 0;

      // if( !(loopidx % 10) )
      //   std::cout << "Iter: " << loopidx << ", active list size: " << _act_idx << ", "
      //             << ((float)_act_idx) / _numNodes * 100 << " % of all nodes" << std::endl;

      size_t asize = _act_idx;
      // compute phi
      #ifdef OPENMP
      #pragma omp parallel for schedule(guided)
      #endif
      for(size_t i=0; i<asize; i++)
      {
        T actidx = _act_nod[i];
        V oldval, newval;

        oldval = phi_old[actidx];
        newval = solve_nbh(phi_old, actidx);

        if(newval < oldval) {
          phi[actidx] = newval;
          _nod_upd[actidx]++;
        }
        else
          newval = oldval;


        if( fabs(newval - oldval) < (DIST_ACC * (1 + (newval + oldval)*0.5)) )
        {
          nodal_expand(phi, phi_old, actidx, newval);
          _act_nod_map[actidx] = false;
          numConv++;
        }
      }

      phi_old = phi;

      _act_idx = 0;
      for(size_t i=0; i<_numNodes; i++)
        if(_act_nod_map[i]) _act_nod[_act_idx++] = i;
    }

    // set unreachable vertices to UR_val
    for(V & s : phi)
      if(s == _initval) s = UR_val;

  }

  inline void operator()(mt_vector<V> & phi,
                         const mt_vector<T> & snod,
                         const V UR_val,
                         bool set_all = false)
  {
    phi.assign(_numNodes, _initval);
    mt_vector<V> phi_old;

    // initialize active nodes and phi
    for(size_t i=0; i<_init.nod.size(); i++)
    {
      T nidx = _init.nod[i];
      _fin_nod_map[nidx] = true;
      phi[nidx] = _init.val[i];
    }
    for(const T nidx : _init.nod) {
      mt_idx_t start = _mesh.n2n_dsp[nidx], end = start + _mesh.n2n_cnt[nidx];
      for(int j=start; j<end; j++) {
        T idx = _mesh.n2n_con[j];
        if(_fin_nod_map[idx] == false)
          _act_nod_map[idx] = true;
      }
    }

    // set up active list
    _act_idx = 0;
    for(mt_idx_t n : snod)
      if(_act_nod_map[n]) _act_nod[_act_idx++] = n;

    // solve for active nodes ---------------------------------------------
    size_t loopidx = 0;
    size_t numConv;

    phi_old = phi;

    while( _act_idx > 0 )
    {
      loopidx ++;
      numConv = 0;

      size_t asize = _act_idx;
      // compute phi
      #ifdef OPENMP
      #pragma omp parallel for schedule(guided)
      #endif
      for(size_t i=0; i<asize; i++)
      {
        T actidx = _act_nod[i];
        V oldval, newval;

        oldval = phi_old[actidx];
        newval = solve_nbh(phi_old, actidx);

        if(newval < oldval) {
          phi[actidx] = newval;
          _nod_upd[actidx]++;
        }
        else
          newval = oldval;


        if( fabs(newval - oldval) < (DIST_ACC * (1 + (newval + oldval)*0.5)) )
        {
          nodal_expand(phi, phi_old, actidx, newval);
          _act_nod_map[actidx] = false;
          numConv++;
        }
      }

      phi_old = phi;

      _act_idx = 0;
      for(mt_idx_t n : snod)
        if(_act_nod_map[n]) _act_nod[_act_idx++] = n;
    }

    if(!set_all) {
      for(mt_idx_t n : snod)
        if(phi[n] == _initval) phi[n] = UR_val;
    } else {
      for(V & s : phi)
        if(s == _initval) s = UR_val;
    }
  }


  inline const mt_vector<T> & get_upd() const
  {
    return _nod_upd;
  }

};

typedef distfield_generator<mt_idx_t,mt_real,mt_real> dfgen;



inline void compute_rel_distfield(const mt_meshdata & mesh,
                                  const mt_vector<mt_idx_t> & snod,
                                  const mt_vector<mt_idx_t> & start_nod,
                                  const mt_vector<mt_idx_t> & end_nod,
                                  mt_vector<mt_real> & field)
{
  distfield_generator<mt_idx_t,mt_real,mt_real>::initdata init;
  init.nod.assign(start_nod.begin(), start_nod.end());
  init.val.assign(start_nod.size(), mt_real(0.0));

  mt_vector<mt_real> start_sol, end_sol;
  {
    distfield_generator<mt_idx_t,mt_real,mt_real> df(mesh, mesh.xyz, init);

    if(snod.size()) df(start_sol, snod, -1.0);
    else            df(start_sol, -1.0);
  }

  init.nod.assign(end_nod.begin(), end_nod.end());
  init.val.assign(end_nod.size(), mt_real(0.0));
  {
    distfield_generator<mt_idx_t,mt_real,mt_real> df(mesh, mesh.xyz, init);

    if(snod.size()) df(end_sol, snod, -1.0);
    else            df(end_sol, -1.0);
  }

  field.resize(start_sol.size());
  for(mt_idx_t n : snod) {
    if(start_sol[n] > -1.0) {
      mt_real d = start_sol[n] + end_sol[n];
      field[n] = d > 1e-12 ? start_sol[n] / d : 0.0;
    } else {
      field[n] = 1.0;
    }
  }
}

#endif


