/**
* @file mesh_refining.cpp
* @brief Mesh refining classes.
* @author Aurel Neic
* @version
* @date 2017-08-07
*/


#include "mt_utils_base.h"
#include "topology_utils.h"
#include "mesh_utils.h"
#include "mesh_refining.h"
#include "mesh_smoothing.h"
#include "mesh_quality.h"

void mt_edge_splitter::split_line(const mt_idx_t eidx,
    const mt_idx_t ele_start,
    const mt_idx_t con_start,
    const mt_tuple<mt_idx_t> & edge,
    const mt_idx_t nv)
{
  mt_meshdata & mesh = _edge_manager._mesh;
  short numfib = mesh.lon.size() == mesh.e2n_cnt.size()*6 ? 6 : 3;
  bool have_lon = mesh.lon.size() > 0;

  mt_idx_t v1 = mesh.e2n_con[mesh.e2n_dsp[eidx] + 0],
  v2 = mesh.e2n_con[mesh.e2n_dsp[eidx] + 1];

  // generate connectivities
  mesh.e2n_con[con_start + 0] = v1;
  mesh.e2n_con[con_start + 1] = nv;
  mesh.e2n_con[con_start + 2] = nv;
  mesh.e2n_con[con_start + 3] = v2;

  // copy element data
  mesh.e2n_cnt[ele_start + 0] = 2;
  mesh.e2n_cnt[ele_start + 1] = 2;
  mesh.etype[ele_start + 0] = Line;
  mesh.etype[ele_start + 1] = Line;
  mesh.etags[ele_start + 0] = mesh.etags[eidx];
  mesh.etags[ele_start + 1] = mesh.etags[eidx];

  if(have_lon) {
    memcpy(mesh.lon.data() + ele_start*numfib,     mesh.lon.data() + eidx*numfib, numfib*sizeof(mt_fib_t));
    memcpy(mesh.lon.data() + (ele_start+1)*numfib, mesh.lon.data() + eidx*numfib, numfib*sizeof(mt_fib_t));
  }
}

// Split a triangle into 2 triangles along a given edge.
void mt_edge_splitter::split_tri(const mt_idx_t eidx,
    const mt_idx_t ele_start,
    const mt_idx_t con_start,
    const mt_tuple<mt_idx_t> & edge,
    const mt_idx_t nv)
{
  mt_meshdata & mesh = _edge_manager._mesh;
  short numfib = mesh.lon.size() == mesh.e2n_cnt.size()*6 ? 6 : 3;
  bool have_lon = mesh.lon.size() > 0;

  mt_idx_t b1 = edge.v1, b2 = edge.v2, v;
  mt_idx_t v1 = mesh.e2n_con[mesh.e2n_dsp[eidx] + 0],
  v2 = mesh.e2n_con[mesh.e2n_dsp[eidx] + 1],
  v3 = mesh.e2n_con[mesh.e2n_dsp[eidx] + 2];

  if(v1 != b1 && v1 != b2) {
    v  = v1;
    b1 = v2, b2 = v3;
  } else if(v2 != b1 && v2 != b2) {
    v = v2;
    b1 = v3, b2 = v1;
  } else {
    v = v3;
    b1 = v1, b2 = v2;
  }

  /*
   * the new triangles are (b1, nv, v) and (b2, v, nv)
   *     v
   *    .    .
   *   .       .
   *  .           .
   * b1 . .  *nv . .b2
   */
  // generate connectivities
  mesh.e2n_con[con_start + 0] = b1;
  mesh.e2n_con[con_start + 1] = nv;
  mesh.e2n_con[con_start + 2] = v;
  mesh.e2n_con[con_start + 3] = b2;
  mesh.e2n_con[con_start + 4] = v;
  mesh.e2n_con[con_start + 5] = nv;

  // copy element data
  mesh.e2n_cnt[ele_start + 0] = 3;
  mesh.e2n_cnt[ele_start + 1] = 3;
  mesh.etype[ele_start + 0] = Tri;
  mesh.etype[ele_start + 1] = Tri;
  mesh.etags[ele_start + 0] = mesh.etags[eidx];
  mesh.etags[ele_start + 1] = mesh.etags[eidx];

  if(have_lon) {
    memcpy(mesh.lon.data() + ele_start*numfib,     mesh.lon.data() + eidx*numfib, numfib*sizeof(mt_fib_t));
    memcpy(mesh.lon.data() + (ele_start+1)*numfib, mesh.lon.data() + eidx*numfib, numfib*sizeof(mt_fib_t));
  }
}


void mt_edge_splitter::split_tet(const mt_idx_t eidx,
    const mt_idx_t ele_start,
    const mt_idx_t con_start,
    const mt_tuple<mt_idx_t> & edge,
    const mt_idx_t nv)
{
  mt_meshdata & mesh = _edge_manager._mesh;
  short numfib = mesh.lon.size() == mesh.e2n_cnt.size()*6 ? 6 : 3;
  bool have_lon = mesh.lon.size() > 0;

  /*
   * the new tetrahedra are (va, b1, vb, nv) and (va, nv, vb, b2)
   *     b2
   *    .  .  .
   *   .    .    .
   *  .     *nv    .
   * va . -  . -  - .vb
   *      .   .   .
   *         . b1
   */

  mt_idx_t b1 = edge.v1, b2 = edge.v2, va, vb;
  mt_idx_t v1 = mesh.e2n_con[mesh.e2n_dsp[eidx] + 0],
  v2 = mesh.e2n_con[mesh.e2n_dsp[eidx] + 1],
  v3 = mesh.e2n_con[mesh.e2n_dsp[eidx] + 2],
  v4 = mesh.e2n_con[mesh.e2n_dsp[eidx] + 3];

  if     (v1 != b1 && v1 != b2) va = v1;
  else if(v2 != b1 && v2 != b2) va = v2;
  else if(v3 != b1 && v3 != b2) va = v3;
  else                          va = v4;

  if     (v1 != b1 && v1 != b2 && v1 != va) vb = v1;
  else if(v2 != b1 && v2 != b2 && v2 != va) vb = v2;
  else if(v3 != b1 && v3 != b2 && v3 != va) vb = v3;
  else                                      vb = v4;

  // generate connectivities
  mesh.e2n_con[con_start + 0] = va;
  mesh.e2n_con[con_start + 1] = b1;
  mesh.e2n_con[con_start + 2] = vb;
  mesh.e2n_con[con_start + 3] = nv;
  mesh.e2n_con[con_start + 4] = va;
  mesh.e2n_con[con_start + 5] = nv;
  mesh.e2n_con[con_start + 6] = vb;
  mesh.e2n_con[con_start + 7] = b2;

  // copy element data
  mesh.e2n_cnt[ele_start + 0] = 4;
  mesh.e2n_cnt[ele_start + 1] = 4;
  mesh.etype[ele_start + 0] = Tetra;
  mesh.etype[ele_start + 1] = Tetra;
  mesh.etags[ele_start + 0] = mesh.etags[eidx];
  mesh.etags[ele_start + 1] = mesh.etags[eidx];

  if(have_lon) {
    memcpy(mesh.lon.data() + ele_start*numfib,     mesh.lon.data() + eidx*numfib, numfib*sizeof(mt_fib_t));
    memcpy(mesh.lon.data() + (ele_start+1)*numfib, mesh.lon.data() + eidx*numfib, numfib*sizeof(mt_fib_t));
  }
}

void mt_edge_splitter::split_iter(mt_vector<mt_mixed_triple<mt_idx_t,mt_idx_t,float>> & split_edge,
                                  MT_USET<mt_idx_t> & split_elem)
{
  mt_meshdata                          & mesh     = _edge_manager._mesh;
  MT_MAP<mt_tuple<mt_idx_t>, mt_idx_t> & edge_map = _edge_manager._edge_map;

  ++_inner_iter;
  short nlon = (mesh.lon.size() / 6) == mesh.e2n_cnt.size() ? 6 : 3;
  bool have_lon = mesh.lon.size() > 0;
  bool have_etags = mesh.etags.size() > 0;
  bool have_etype = mesh.etype.size() > 0;

  // prepare datastructs =============================================
  mt_idx_t mesh_xyz_idx      = mesh.xyz.size() / 3;
  mt_idx_t mesh_e2n_cnt_idx  = mesh.e2n_cnt.size();
  mt_idx_t mesh_e2n_con_idx  = mesh.e2n_con.size();

  size_t new_nelem = 0, new_ncon = 0;

  for(mt_idx_t eidx : split_elem)
  {
    switch(mesh.etype[eidx])
    {
      case Line:  new_nelem += 2; new_ncon += 4; break;
      case Tri:   new_nelem += 2; new_ncon += 6; break;
      case Tetra: new_nelem += 2; new_ncon += 8; break;
      default:
               std::cerr << "split_edges_iter: Error, element type not yet supported. Aborting!" << std::endl;
               return;
    }
  }

  mesh_resize_elemdata(mesh, mesh.e2n_cnt.size() + new_nelem, mesh.e2n_con.size() + new_ncon);
  mesh.xyz.resize(mesh.xyz.size() + split_edge.size()*3);

  MT_USET<mt_idx_t> edge_elems;

  // start splitting =================================================
  for(auto & it : split_edge)
  {
    // first split edge ----------------------------------------------
    mt_tuple<mt_idx_t> tupbuff;
    mt_idx_t v1 = it.v1, v2 = it.v2, new_v = mesh_xyz_idx++;

    // add new vertex
    mt_point<mt_real> p1(mesh.xyz.data() + v1*3), p2(mesh.xyz.data() + v2*3);
    mt_point<mt_real> np = p1 + (p2 - p1) * mt_real(it.v3);
    mesh.xyz[new_v*3+0] = np.x;
    mesh.xyz[new_v*3+1] = np.y;
    mesh.xyz[new_v*3+2] = np.z;

    // old edge
    sortTuple(v1, v2, tupbuff.v1, tupbuff.v2);

    // now split elements connected to the edge ----------------------
    edge_elems.clear();
    elements_with_edge(mesh, v1, v2, edge_elems);

    for(mt_idx_t eidx : edge_elems)
    {
      switch(mesh.etype[eidx])
      {
        case Line:
          split_line(eidx, mesh_e2n_cnt_idx, mesh_e2n_con_idx, tupbuff, new_v);

          add_edges_line(mesh.e2n_con.data() + mesh_e2n_con_idx + 0, _edgeidx, edge_map);
          add_edges_line(mesh.e2n_con.data() + mesh_e2n_con_idx + 2, _edgeidx, edge_map);

          mesh_e2n_cnt_idx += 2;
          mesh_e2n_con_idx += 4;
          break;

        case Tri:
          split_tri(eidx, mesh_e2n_cnt_idx, mesh_e2n_con_idx, tupbuff, new_v);

          add_edges_tri(mesh.e2n_con.data() + mesh_e2n_con_idx + 0, _edgeidx, edge_map);
          add_edges_tri(mesh.e2n_con.data() + mesh_e2n_con_idx + 3, _edgeidx, edge_map);

          mesh_e2n_cnt_idx += 2;
          mesh_e2n_con_idx += 6;
          break;

        case Tetra:
          split_tet(eidx, mesh_e2n_cnt_idx, mesh_e2n_con_idx, tupbuff, new_v);

          add_edges_tet(mesh.e2n_con.data() + mesh_e2n_con_idx + 0, _edgeidx, edge_map);
          add_edges_tet(mesh.e2n_con.data() + mesh_e2n_con_idx + 4, _edgeidx, edge_map);

          mesh_e2n_cnt_idx += 2;
          mesh_e2n_con_idx += 8;
          break;

        default:
          std::cerr << "split_edges_iter: Error, element type not yet supported. Aborting!" << std::endl;
          return;
      }
    }
    edge_map.erase(tupbuff);

    if(_prg) _prg->next();
  }

  // remove split elements from mesh
  size_t widx=0;
  size_t e_ridx=0, e_widx=0;
  size_t numelems = mesh.e2n_cnt.size();
  for(size_t i=0; i<numelems; i++)
  {
    if( split_elem.count(i) == 0 )
    {
      // mesh element data
      mesh.e2n_cnt[widx] = mesh.e2n_cnt[i];

      if(have_etags)
        mesh.etags[widx] = mesh.etags[i];
      if(have_etype)
        mesh.etype[widx] = mesh.etype[i];

      if(have_lon) {
        for(int j=0; j<nlon; j++)
          mesh.lon[widx*nlon + j] = mesh.lon[i*nlon + j];
      }

      widx++;

      // mesh connectivity
      for(int j=0; j<mesh.e2n_cnt[i]; j++)
        mesh.e2n_con[e_widx++] = mesh.e2n_con[e_ridx++];
    }
    else {
      e_ridx   += mesh.e2n_cnt[i];
    }
  }

  mesh.e2n_cnt.resize(widx);
  if(have_etags)
    mesh.etags.resize(widx);
  if(have_etype)
    mesh.etype.resize(widx);
  if(have_lon)
    mesh.lon.resize(widx*nlon);
  mesh.e2n_con.resize(e_widx);

  bucket_sort_offset(mesh.e2n_cnt, mesh.e2n_dsp);
  transpose_connectivity_par(mesh.e2n_cnt, mesh.e2n_dsp, mesh.e2n_con, mesh.n2e_cnt, mesh.n2e_dsp, mesh.n2e_con, false);
}

void mt_edge_splitter::operator()(mt_vector<edgeele> & edge_set,
                                  PROGRESS<size_t>* inp_prg)
{
  mt_meshdata & mesh = _edge_manager._mesh;
  _prg = inp_prg;

  mt_vector<mt_mixed_triple<mt_idx_t,mt_idx_t,float> > sel_edge;
  MT_USET<mt_idx_t> split_elem;
  mt_vector<bool> rem_edge;

  edge_len_dsc comp;
  std::sort(edge_set.begin(), edge_set.end(), comp);
  std::unique(edge_set.begin(), edge_set.end());

  MT_USET<mt_idx_t> edge_elems;

  while(edge_set.size() > 0)
  {
    split_elem.clear();
    sel_edge.reserve_and_clear(edge_set.size());
    rem_edge.assign(edge_set.size(), false);

    // iterate over edges in descending length order.
    for(size_t it=0; it<edge_set.size(); it++)
    {
      edgeele & eit = edge_set[it];
      bool can_split = true;

      edge_elems.clear();
      elements_with_edge(mesh, eit.v1, eit.v2, edge_elems);

      for(mt_idx_t eidx : edge_elems) {
        if(split_elem.count(eidx)) {
          can_split = false;
          break;
        }
      }

      if(can_split) {
        sel_edge.push_back({eit.v1, eit.v2, eit.split_at});

        for(mt_idx_t eidx : edge_elems)
          split_elem.insert(eidx);

        rem_edge[it] = true;
      }
    }

    size_t widx=0;
    for(size_t ridx=0; ridx < rem_edge.size(); ridx++) {
      if(!rem_edge[ridx])
        edge_set[widx++] = edge_set[ridx];
    }
    edge_set.resize(widx);

    split_iter(sel_edge, split_elem);
  }
  if(_prg) _prg->finish();

#if 0
  if( _inner_iter > RECOMP_THR ) {
    std::cout << "Recomputing node and edge indices.. " << std::endl;
    _inner_iter = 0;
    _edge_manager.refresh_edge_data(true);
    _edgeidx = _edge_manager.edges().size();
  }
#endif
}

void mt_edge_collapser::collapse_iter(MT_MAP<mt_idx_t, mt_mixed_tuple<mt_idx_t, mt_point<mt_real> > > & nodmap,
                                      MT_USET<mt_idx_t> & rem_elem)
{
  mt_meshdata & mesh = _edge_manager._mesh;

  short nlon = (mesh.lon.size() / 6) == mesh.e2n_cnt.size() ? 6 : 3;
  bool have_lon = mesh.lon.size() > 0;
  bool have_etags = mesh.etags.size() > 0;
  bool have_etype = mesh.etype.size() > 0;

  // loop over elements, and update connectivites
  for(size_t i=0; i<mesh.e2n_con.size(); i++) {
    mt_idx_t c = mesh.e2n_con[i];
    if(nodmap.count(c)) {
      mt_idx_t idx = nodmap[c].v1;
      mt_point<mt_real> p = nodmap[c].v2;

      mesh.e2n_con[i] = idx;
      mesh.xyz[idx*3+0] = p.x;
      mesh.xyz[idx*3+1] = p.y;
      mesh.xyz[idx*3+2] = p.z;
    }
  }

  // remove elements from mesh and e2g
  size_t widx=0;
  size_t e_ridx=0, e_widx=0;
  size_t numelems = mesh.e2n_cnt.size();
  for(size_t i=0; i<numelems; i++)
  {
    if( rem_elem.count(i) == 0 )
    {
      // mesh element data
      mesh.e2n_cnt[widx] = mesh.e2n_cnt[i];
      if(have_etags)
        mesh.etags[widx] = mesh.etags[i];
      if(have_etype)
        mesh.etype[widx] = mesh.etype[i];
      if(have_lon) {
        for(int j=0; j<nlon; j++)
          mesh.lon[widx*nlon + j] = mesh.lon[i*nlon + j];
      }

      widx++;

      // mesh connectivity
      for(int j=0; j<mesh.e2n_cnt[i]; j++)
        mesh.e2n_con[e_widx++] = mesh.e2n_con[e_ridx++];

    }
    else {
      e_ridx   += mesh.e2n_cnt[i];
    }
  }
  mesh.e2n_cnt.resize(widx);
  if(have_etype)
    mesh.etags.resize(widx);
  if(have_etype)
    mesh.etype.resize(widx);
  if(have_lon)
    mesh.lon.resize(widx*nlon);

  mesh.e2n_con.resize(e_widx);
  bucket_sort_offset(mesh.e2n_cnt, mesh.e2n_dsp);
  transpose_connectivity_par(mesh.e2n_cnt, mesh.e2n_dsp, mesh.e2n_con, mesh.n2e_cnt, mesh.n2e_dsp, mesh.n2e_con, false);
}

bool mt_edge_collapser::check_bad_tet(const MT_USET<mt_idx_t> & edgeelems,
          const mt_vector<bool> & nodmask,
          const MT_MAP<mt_idx_t, mt_mixed_tuple<mt_idx_t, mt_point<mt_real> > > & nodmap)
{
  mt_meshdata & mesh = _edge_manager._mesh;
  mt_idx_t  con[4] = {0, 1, 2, 3};
  mt_real xyz[12];

  bool bad = false;

  for(const mt_idx_t & eidx : edgeelems) {
    // we create an auxiliary element holding the remapped connectivity
    mt_idx_t estart = mesh.e2n_dsp[eidx];
    for(size_t i=0; i<4; i++) {
      mt_idx_t c = mesh.e2n_con[estart+i];
      if(nodmask[c]) {
        const mt_point<mt_real> & p = nodmap.find(c)->second.v2;
        xyz[i*3+0] = p.x;
        xyz[i*3+1] = p.y;
        xyz[i*3+2] = p.z;
      }
      else {
        xyz[i*3+0] = mesh.xyz[c*3+0];
        xyz[i*3+1] = mesh.xyz[c*3+1];
        xyz[i*3+2] = mesh.xyz[c*3+2];
      }
    }

    double qual = QMETRIC(con, xyz);
    if(has_bad_vol(Tetra, con, xyz, mt_real(0.2)) || qual > RESAMPLE_QUAL_THR) {
      bad = true;
      break;
    }
  }
  return bad;
}

bool mt_edge_collapser::check_bad_tri(const MT_USET<mt_idx_t> & edgeelems,
          MT_MAP<mt_idx_t, mt_mixed_tuple<mt_idx_t, mt_point<mt_real> > > & nodmap)
{
  mt_meshdata & mesh = _edge_manager._mesh;
  mt_idx_t  con[3] = {0, 1, 2};
  mt_real xyz[9];
  bool bad = false;

  for(const mt_idx_t & eidx : edgeelems) {
    // we create an auxiliary element holding the remapped connectivity
    mt_idx_t estart = mesh.e2n_dsp[eidx];
    for(size_t i=0; i<3; i++) {
      mt_idx_t c = mesh.e2n_con[estart+i];
      if(nodmap.count(c)) {
        const mt_point<mt_real> & p = nodmap[c].v2;
        xyz[i*3+0] = p.x;
        xyz[i*3+1] = p.y;
        xyz[i*3+2] = p.z;
      }
      else {
        xyz[i*3+0] = mesh.xyz[c*3+0];
        xyz[i*3+1] = mesh.xyz[c*3+1];
        xyz[i*3+2] = mesh.xyz[c*3+2];
      }
    }

    double qual = tri_qmetric_surface(con, xyz);
    if(has_bad_vol(Tri, con, xyz, mt_real(0.2)) || qual > RESAMPLE_QUAL_THR) {
      bad = true;
      break;
    }
  }

  return bad;
}

void mt_edge_collapser::operator()(std::set<edgeele, edge_len_asc> & edge_set,
                                   const mt_vector<bool> & surf_nodes,
                                   const mt_vector<mt_real> & surf_nrml,
                                   const mt_vector<bool> & fixed_surf_nodes,
                                   const mt_real surf_corr,
                                   PROGRESS<size_t>* prg)
{
  mt_meshdata & mesh = _edge_manager._mesh;

  MT_USET<mt_idx_t> rem_elem, edge_elem, attached_elems;
  MT_MAP<mt_idx_t, mt_mixed_tuple<mt_idx_t, mt_point<mt_real> > > nodmap;
  mt_vector<bool> nodmask(mesh.xyz.size() / 3, false);

  auto eit = edge_set.begin();
  size_t clp_cnt = 0;

  // iterate over edges in descending length order.
  while(eit != edge_set.end())
  {
    const mt_idx_t v1 = eit->v1, v2 = eit->v2;

    bool mod_v1 = nodmask[v1], mod_v2 = nodmask[v2];
    bool sel = false, move_vtx = false, abort = false;

    mt_idx_t rem_nod, keep_nod;

    if(!(mod_v1 || mod_v2))
    {
      bool v1_surf  = surf_nodes[v1],  v2_surf  = surf_nodes[v2];
      bool v1_fixed = fixed_surf_nodes[v1], v2_fixed = fixed_surf_nodes[v2];
      rem_nod = v1, keep_nod = v2;

      if(v1_surf) {
        if(v2_surf) {
          // both nodes are surface nodes
          mt_point<mt_real> n1(surf_nrml.data()+v1*3), n2(surf_nrml.data()+v2*3);
          if( (surf_corr == 0.0) || (n1.scaProd(n2) > surf_corr) ) {
            // both node normals correlate sufficiently
            if(v1_fixed || v2_fixed) {
              if(!v1_fixed) // v1 can be removed -> no vertex moving
                sel = true;
              if(!v2_fixed) { // v2 can be removed -> swap and no moving
                rem_nod = v2, keep_nod = v1;
                sel = true;
              }
            }
            else { // no vtx is fixed -> no problem
              sel = true;
              move_vtx = true;
            }
          }
        }
        else {
          // v2 is not on a surface -> we swap rem and keep nodes
          rem_nod = v2, keep_nod = v1;
          sel = true;
        }
      }
      else {
        sel = true;
        if(!v2_surf) move_vtx = true;
      }
    }

    if(sel) {
      mt_mixed_tuple<mt_idx_t, mt_point<mt_real> > tb;
      tb.v1 = keep_nod;
      tb.v2 = mt_point<mt_real>(mesh.xyz.data() + keep_nod*3);
      nodmap[rem_nod] = tb;
      nodmask[rem_nod] = true;
      nodmap[keep_nod] = tb;
      nodmask[keep_nod] = true;

      mt_point<mt_real> & rem_pt = nodmap[rem_nod].v2;
      mt_point<mt_real> & keep_pt = nodmap[keep_nod].v2;

      // add elements connected to collapsed edge for removal
      edge_elem.clear();
      attached_elems.clear();

      elements_with_edge(mesh, eit->v1, eit->v2, edge_elem);

      mt_idx_t start = mesh.n2e_dsp[rem_nod], stop = start + mesh.n2e_cnt[rem_nod];
      for(mt_idx_t i = start; i<stop; i++) {
        mt_idx_t e = mesh.n2e_con[i];
        if(edge_elem.count(e) == 0) attached_elems.insert(e);
      }
      start = mesh.n2e_dsp[keep_nod], stop = start + mesh.n2e_cnt[keep_nod];
      for(mt_idx_t i = start; i<stop; i++) {
        mt_idx_t e = mesh.n2e_con[i];
        if(edge_elem.count(e) == 0) attached_elems.insert(e);
      }

      // TODO: factor this into function
      if(move_vtx) {
        // set keep_nod coord to center between rem_nod and keep_nod
        mt_point<mt_real> p1(mesh.xyz.data()+rem_nod*3), p2(mesh.xyz.data()+keep_nod*3);
        mt_point<mt_real> c = (p1 + p2) * mt_real(0.5);
        keep_pt = c, rem_pt = c;
        // check if collapsing to center yields bad elems
        bool bad = check_bad_tet(attached_elems, nodmask, nodmap);

        if(bad) {
          c = p1 + ((p2 - p1)*mt_real(0.25));
          keep_pt = c, rem_pt = c;
          bad = check_bad_tet(attached_elems, nodmask, nodmap);
        }
        if(bad) {
          c = p1 + ((p2 - p1)*mt_real(0.75));
          keep_pt = c, rem_pt = c;
          bad = check_bad_tet(attached_elems, nodmask, nodmap);
        }
        if(bad) abort = true;
      }
      else
        abort = check_bad_tet(attached_elems, nodmask, nodmap);

      if(abort) {
        nodmap.erase(keep_nod);
        nodmap.erase(rem_nod);
        nodmask[rem_nod] = false;
        nodmask[keep_nod] = false;
      }
      else {
        // the selected edge can be removed
        auto rem_it = eit++;
        edge_set.erase(rem_it);

        for(mt_idx_t i : edge_elem) rem_elem.insert(i);

        clp_cnt++;
      }
    }

    if(abort || (!sel)) ++eit;
  }
  if(prg) prg->next();

  // apply node remapping to mesh
  collapse_iter(nodmap, rem_elem);
  if(prg) prg->next();

  // recompute edges
  _edge_manager.refresh_edge_data(false);
  if(prg) prg->next();

  // recompute surface normals on the fly
  #if 0
  for(size_t nidx=0; nidx < mesh.n2e_cnt.size(); nidx++) {
    if(mesh.n2e_cnt[nidx] > 0 && surf_nodes[nidx]) {
      MT_USET<mt_idx_t> nset, eset, dset;
      nset.insert(nidx);
      nodeSet_to_elemSet(mesh, nset, eset);
      elemSet_to_nodeSet(mesh, eset, nset);
      nset.erase(nidx);

      for(auto n : nset)
        if(!surf_nodes[n]) dset.insert(n);
      for(auto n : dset) nset.erase(n);
    }
  }
  #endif
  if(prg) prg->finish();
}

void mt_edge_collapser::operator()(std::set<edgeele, edge_len_asc> & edge_set,
                                   const mt_vector<mt_real> & surf_nrml,
                                   const mt_vector<bool> & fixed_surf_nodes,
                                   const mt_real surf_corr,
                                   PROGRESS<size_t>* prg)
{
  mt_meshdata & mesh = _edge_manager._mesh;

  MT_USET<mt_idx_t> rem_elem, edge_elem, attached_elems;
  MT_MAP<mt_idx_t, mt_mixed_tuple<mt_idx_t, mt_point<mt_real> > > nodmap;

  auto eit = edge_set.begin();
  size_t clp_cnt = 0;

  // iterate over edges in descending length order.
  while(eit != edge_set.end())
  {
    const mt_idx_t v1 = eit->v1, v2 = eit->v2;

    bool mod_v1  = nodmap.count(v1)  > 0, mod_v2  = nodmap.count(v2) > 0;
    bool sel = false, move_vtx = false, abort = false;

    mt_idx_t rem_nod, keep_nod;

    if(!(mod_v1 || mod_v2))
    {
      bool v1_fixed = fixed_surf_nodes[v1], v2_fixed = fixed_surf_nodes[v2];
      rem_nod = v1, keep_nod = v2;

      // both nodes are surface nodes
      mt_point<mt_real> n1(surf_nrml.data()+v1*3), n2(surf_nrml.data()+v2*3);
      if( (surf_corr == 0.0) || (n1.scaProd(n2) > surf_corr) ) {
        // both node normals correlate sufficiently
        if(v1_fixed || v2_fixed) {
          if(this->collapse_along_fixed && v1_fixed == v2_fixed) {
            sel = true;
            move_vtx = true;
          } else {
            if(!v1_fixed) // v1 can be removed -> no vertex moving
              sel = true;
            if(!v2_fixed) { // v2 can be removed -> swap and no moving
              rem_nod = v2, keep_nod = v1;
              sel = true;
            }
          }
        } else { // no vtx is fixed -> no problem
          sel = true;
          move_vtx = true;
        }
      }
    }

    if(sel) {
      mt_mixed_tuple<mt_idx_t, mt_point<mt_real> > tb;
      tb.v1 = keep_nod;
      tb.v2 = mt_point<mt_real>(mesh.xyz.data() + keep_nod*3);
      nodmap[rem_nod] = tb;
      nodmap[keep_nod] = tb;

      mt_point<mt_real> & rem_pt = nodmap[rem_nod].v2;
      mt_point<mt_real> & keep_pt = nodmap[keep_nod].v2;

      // add elements connected to collapsed edge for removal
      edge_elem.clear();
      attached_elems.clear();

      elements_with_edge(mesh, eit->v1, eit->v2, edge_elem);

      mt_idx_t start = mesh.n2e_dsp[rem_nod], stop = start + mesh.n2e_cnt[rem_nod];
      for(mt_idx_t i = start; i<stop; i++) {
        mt_idx_t e = mesh.n2e_con[i];
        if(edge_elem.count(e) == 0) attached_elems.insert(e);
      }
      start = mesh.n2e_dsp[keep_nod], stop = start + mesh.n2e_cnt[keep_nod];
      for(mt_idx_t i = start; i<stop; i++) {
        mt_idx_t e = mesh.n2e_con[i];
        if(edge_elem.count(e) == 0) attached_elems.insert(e);
      }

      // TODO: factor this into function
      if(move_vtx) {
        // set keep_nod coord to center between rem_nod and keep_nod
        mt_point<mt_real> p1(mesh.xyz.data()+rem_nod*3), p2(mesh.xyz.data()+keep_nod*3);
        mt_point<mt_real> c = (p1 + p2) * mt_real(0.5);
        keep_pt = c, rem_pt = c;
        // check if collapsing to center yields bad elems
        bool bad = check_bad_tri(attached_elems, nodmap);

        if(bad) {
          c = p1 + ((p2 - p1)*mt_real(0.25));
          keep_pt = c, rem_pt = c;
          bad = check_bad_tri(attached_elems, nodmap);
        }
        if(bad) {
          c = p1 + ((p2 - p1)*mt_real(0.75));
          keep_pt = c, rem_pt = c;
          bad = check_bad_tri(attached_elems, nodmap);
        }
        if(bad) {
          c = p1;
          keep_pt = c, rem_pt = c;
          bad = check_bad_tri(attached_elems, nodmap);
        }
        if(bad) {
          c = p2;
          keep_pt = c, rem_pt = c;
          bad = check_bad_tri(attached_elems, nodmap);
        }
        if(bad) abort = true;
      }
      else
        abort = check_bad_tri(attached_elems, nodmap);

      if(abort) {
        nodmap.erase(keep_nod);
        nodmap.erase(rem_nod);
      }
      else {
        // the selected edge can be removed
        auto rem_it = eit++;
        edge_set.erase(rem_it);

        for(mt_idx_t i : edge_elem) rem_elem.insert(i);

        clp_cnt++;
      }
    }

    if(abort || (!sel)) ++eit;
  }
  if(prg) prg->next();

  // apply node remapping to mesh
  collapse_iter(nodmap, rem_elem);
  if(prg) prg->next();

  // recompute edges
  _edge_manager.refresh_edge_data(false);
  if(prg) prg->finish();
}


bool non_uniform_splitting(mt_meshdata & mesh,
                           const MT_USET<mt_tag_t> & tags,
                           mt_edge_splitter & splitter,
                           mt_edge_manager & edge_manager,
                           const mt_vector<bool> & fixed_nod,
                           const mt_real max)
{
  if(mesh.e2n_cnt.size() == 0)
    return false;

  timeval t1, t2;
  size_t iter = 0, nsel = 0;
  char msg[1024];

  bool having_fixed_nodes = fixed_nod.size();
  bool updated_mesh = false;
  bool verbose = false;

  edge_len_asc ascending;
  mt_vector<edgeele> sel_edges;
  const MT_MAP<mt_tuple<mt_idx_t>,mt_idx_t> & edges = edge_manager.edges();
  float nsel_to_total = 0.0, nsel_to_total_thres = 0.1;

  // main splitting loop
  do {
    sel_edges.resize(0);

#ifdef OPENMP
    #pragma omp parallel
#endif
    {
      MT_USET<edgeele>   sel_edges_set;
      mt_vector<edgeele> elem_edge_lengths;

#ifdef OPENMP
      #pragma omp for schedule(dynamic, 16)
#endif
      for(size_t eidx=0; eidx < mesh.e2n_cnt.size(); eidx++)
      {
        if(tags.count(mesh.etags[eidx])) {
          add_edges_lengths(mesh, eidx, elem_edge_lengths);
          edgeele longest = *std::max_element(elem_edge_lengths.begin(), elem_edge_lengths.end(), ascending);

          bool edge_is_fixed = false;
          if(having_fixed_nodes) {
            bool v1_fixed = fixed_nod[longest.v1],
                 v2_fixed = fixed_nod[longest.v2];
            // we cannot touch edges going to the fixed surface. first, because sometimes vertices
            // switch and we might brake the logic, second for elem quality reasons
            edge_is_fixed = v1_fixed || v2_fixed;
          }

          if(!edge_is_fixed && longest.length > max)
            sel_edges_set.insert(longest);
        }
      }

#ifdef OPENMP
      #pragma omp critical
#endif
      {
        sel_edges.push_back(sel_edges_set.begin(), sel_edges_set.end());
      }
    }

    for(auto & e : sel_edges) {
      e.split_at = 0.5f;
      auto it = edges.find({e.v1, e.v2});
      e.idx = it->second;
    }

    nsel = sel_edges.size();
    nsel_to_total = float(nsel) / float(edges.size()) * 100.0;

    iter++;
    if(nsel_to_total > nsel_to_total_thres) {
      PROGRESS<size_t>* prog = NULL;

      if(verbose) {
        sprintf(msg, "Iter %zu: splitting %zu edges, %.4f %% of total %zu ",
                iter, nsel, nsel_to_total, edges.size());
        prog = new PROGRESS<size_t>(nsel, msg);
      }

      gettimeofday(&t1, NULL);
      splitter(sel_edges, prog);
      gettimeofday(&t2, NULL);

      if(verbose) {
        std::cout << "Done in " << (float)timediff_sec(t1, t2) << " sec" << std::endl;
        delete prog;
      }
      updated_mesh = true;
    }
  } while(nsel > 0 && nsel_to_total > nsel_to_total_thres);

  return updated_mesh;
}

bool uniform_splitting(mt_meshdata & mesh,
                       const MT_USET<mt_tag_t> & tags,
                       mt_edge_manager & edge_manager,
                       const mt_real max)
{
  if(mesh.e2n_cnt.size() == 0)
    return false;

  timeval t1, t2;
  size_t iter = 0, nsel = 0;
  bool updated_mesh = false;

  // main splitting loop
  do {
    std::set<edgeele, edge_len_dsc> sel_edges;
    // this is the set of edges for all tags
    const MT_MAP<mt_tuple<mt_idx_t>,mt_idx_t> & edges = edge_manager.edges();
    mt_vector<edgeele> elem_edge_lengths;
    mt_real avrg_len = 0.0, num_added = 0.0;

    // first we determine if the average edge length of the specified tag region is
    // significantly larger than the specified max length
    for(size_t eidx=0; eidx < mesh.e2n_cnt.size(); eidx++)
    {
      if(tags.count(mesh.etags[eidx]))
        add_edges_lengths(mesh, eidx, elem_edge_lengths);
    }

    for(const edgeele & e : elem_edge_lengths) {
      avrg_len  += e.length;
      num_added += 1.0;
    }

    if(num_added)
      avrg_len /= num_added;

    if(avrg_len * 0.6 > max) {
      iter++;
      gettimeofday(&t1, NULL);
      printf("Iter %zu: Uniform refining .. \n", iter);

      refine_uniform(mesh, edges);
      bucket_sort_offset(mesh.e2n_cnt, mesh.e2n_dsp);
      edge_manager.refresh_edge_data(false);

      gettimeofday(&t2, NULL);
      std::cout << "Done in " << (float)timediff_sec(t1, t2) << " sec" << std::endl;
      updated_mesh = true;
    }
  }
  while(nsel > 0);

  return updated_mesh;
}

/// update the mesh with a collapsing iter
bool surf_collapse_update(mt_meshdata & mesh,
                          const MT_USET<mt_tag_t> & tags,
                          mt_edge_manager & edge_manager,
                          mt_edge_collapser & collapser,
                          const mt_real min,
                          const mt_real scorr,
                          const bool fix_bnd,
                          const bool dosmooth,
                          const float sharp_edge)
{
  if(mesh.e2n_cnt.size() == 0)
    return false;

  struct timeval t1, t2;
  bool did_update_mesh = false;
  size_t nsel = 0, nsel_old = 0;
  size_t iter = 0;
  char msg[1024];

  bool verbose = false;

  mt_vector<mt_real> surf_nrml;
  compute_nodal_surface_normals(mesh, mesh.xyz, surf_nrml);

#if 0
  write_vector_ascii(surf_nrml, "normals.vec", 3);
#endif

  // we keep the edges connected to only one element as fixed, 
  // since they define the boundary of a non-closed surface
  mt_vector<bool> fixed_nod;
  fixed_nod.assign(mesh.n2e_cnt.size(), false);

  MT_USET<mt_idx_t> ele;

  if(fix_bnd) {
    for(auto e : edge_manager.edges()) {
      ele.clear();
      elements_with_edge(mesh, e.first.v1, e.first.v2, ele);

      if(ele.size() == 1) {
        fixed_nod[e.first.v1] = true;
        fixed_nod[e.first.v2] = true;
      }
    }
  }

  edge_len_asc asc_comparator;
  std::set< edgeele, edge_len_asc> sel_edges;
  const MT_MAP<mt_tuple<mt_idx_t>, mt_idx_t> & edges = edge_manager.edges();
  float nsel_to_total = 0.0, nsel_to_total_thres = 0.1;

  // main collapsing loop
  do {
    sel_edges.clear();

#ifdef OPENMP
    #pragma omp parallel
#endif
    {
      MT_USET<edgeele>   sel_edges_set;
      mt_vector<edgeele> elem_edge_lengths;

#ifdef OPENMP
      #pragma omp for schedule(dynamic, 16)
#endif
      for(size_t eidx=0; eidx<mesh.e2n_cnt.size(); eidx++) {
        if(tags.count(mesh.etags[eidx])) {
          add_edges_lengths(mesh, eidx, elem_edge_lengths);
          edgeele shortest = *std::min_element(elem_edge_lengths.begin(), elem_edge_lengths.end(), asc_comparator);
          if(shortest.length < min)
            sel_edges_set.insert(shortest);
        }
      }

      for(auto & e : sel_edges_set) {
        e.split_at = 0.5f;
        auto it = edges.find({e.v1, e.v2});
        e.idx = it->second;
      }

#ifdef OPENMP
      #pragma omp critical
#endif
      {
        sel_edges.insert(sel_edges_set.begin(), sel_edges_set.end());
      }
    }

    nsel     = sel_edges.size();
    nsel_old = nsel;
    iter++;
    nsel_to_total = float(nsel) / float(edges.size()) * 100.0;

    if(nsel_to_total > nsel_to_total_thres) {

      PROGRESS<size_t>* prog = NULL;
      if(verbose && nsel_to_total > nsel_to_total_thres) {
        sprintf(msg, "Iter %zu: collapse %zu edges (%.4f %%)", iter, nsel, nsel_to_total);
        prog = new PROGRESS<size_t>(4, msg);
      }

      gettimeofday(&t1, NULL);

      collapser(sel_edges, surf_nrml, fixed_nod, scorr, prog);
      nsel = sel_edges.size();
      size_t diff = nsel_old - nsel;

      gettimeofday(&t2, NULL);

      if(verbose && nsel_to_total > nsel_to_total_thres) {
        printf("Collapsed %zu (%.2f %%) in %.2f sec\n", diff, float(diff) / float(nsel_old) * 100.0f,
               (float)timediff_sec(t1, t2));
        delete prog;
      }

      if(diff > 0) {
        did_update_mesh = true;
        if(mesh.e2n_cnt.size())
          compute_nodal_surface_normals(mesh, mesh.xyz, surf_nrml);
      }
    }
  } while(nsel_to_total > nsel_to_total_thres && nsel_old > nsel);

  if(did_update_mesh) {
    // for checking if elements are insisde out we need the e2n_graph refreshed
    mesh.e2n_dsp.resize(mesh.e2n_cnt.size());
    bucket_sort_offset(mesh.e2n_cnt, mesh.e2n_dsp);
    correct_insideOut_tri(mesh, surf_nrml);

    // now we refresh edges and remap the points
    edge_manager.refresh_edge_data(true);

    // for simplicity we always recompte the connectivity as it might be needed in
    // splitting or smoothing runs
    compute_full_mesh_connectivity(mesh, false);

    if(dosmooth)
      surface_smooth(mesh, ITER_BASE, SMTH_BASE, sharp_edge, 0, false, false);
  }

  return did_update_mesh;
}

/// update the mesh with a splitting iter
bool surf_split_update(mt_meshdata & mesh,
                       const MT_USET<mt_tag_t> & tags,
                       mt_edge_manager & edge_manager,
                       mt_edge_splitter & splitter,
                       const mt_real max,
                       const bool unif_splt,
                       const bool dosmooth,
                       const float sharp_edge)
{
  // std::cout << "\n *** Splitting step *** \n" << std::endl;

  mt_meshdata origmesh = mesh;

  // right now I dont see why we would need to fix the surface boundary during refinement, 
  // so the feature is off.
  mt_vector<bool> boundary_nodes;

  bool splt_update;

  if(unif_splt)
    splt_update = uniform_splitting(mesh, tags, edge_manager, max);
  else
    splt_update = non_uniform_splitting(mesh, tags, splitter, edge_manager, boundary_nodes, max);

  if(splt_update) {
    // finish splitting
    edge_manager.refresh_edge_data(true);
    compute_full_mesh_connectivity(mesh, false);

    if(dosmooth)
      surface_smooth(mesh, ITER_BASE, SMTH_BASE, sharp_edge, 0, false, false);
  }

  return splt_update;
}

void resample_surface(mt_meshdata & mesh,
                      const mt_real min,
                      const mt_real max,
                      const MT_USET<mt_tag_t> & tags,
                      const mt_real surf_corr,
                      const bool fix_bnd,
                      const bool uniform_splitting,
                      const bool dosmooth,
                      const float sharp_edge)
{
  mt_edge_manager edge_manager(mesh);
  mt_edge_splitter splitter(edge_manager);
  mt_edge_collapser collapser(edge_manager);

  bool did_update_mesh;
  int  iter = 0, maxiter = 10;
  do {
    did_update_mesh = false;
    iter++;

    // collapsing run ========================================================================
    if(min > 0) {
      did_update_mesh = surf_collapse_update(mesh, tags, edge_manager, collapser, min, surf_corr, fix_bnd, dosmooth, sharp_edge);
    }

    // splitting run ========================================================================
    if(max > 0) {
      bool split_update = surf_split_update(mesh, tags, edge_manager, splitter, max, uniform_splitting, dosmooth, sharp_edge);
      did_update_mesh = did_update_mesh || split_update;
    }
  } while(did_update_mesh && iter < maxiter);
}

void update_surface_data(mt_meshdata & mesh,
                         const MT_USET<mt_tag_t> & tags,
                         const bool fix_bnd,
                         mt_vector<bool> & surf_nod,
                         mt_vector<bool> & boundary_node,
                         mt_vector<mt_real> & surf_nrml)
{
  if(mesh.e2n_cnt.size() == 0)
    return;

  mt_meshdata surfmesh;
  compute_surface(mesh, tags, surfmesh, true);

  if(surfmesh.e2n_cnt.size() == 0)
    return;

  compute_full_mesh_connectivity(surfmesh);

  surf_nod.assign(mesh.n2e_cnt.size(), false);
  if(fix_bnd) boundary_node.assign(mesh.n2e_cnt.size(), false);

  for(auto c : surfmesh.e2n_con) {
    surf_nod[c] = true;
    if(fix_bnd) boundary_node[c] = true;
  }

  surfmesh.xyz.assign(mesh.xyz.size(), mesh.xyz.data(), false);
  compute_nodal_surface_normals(surfmesh, mesh.xyz, surf_nrml);
  surfmesh.xyz.assign(0, NULL, false);
}

void resample_mesh(mt_meshdata & mesh,
                   const mt_real min,
                   const mt_real max,
                   const mt_vector<MT_USET<mt_tag_t>> & alltags,
                   const MT_USET<mt_tag_t> & tags,
                   const mt_real surf_corr,
                   const bool fix_bnd,
                   const bool do_conv,
                   bool unif_split,
                   const bool dosmooth,
                   const float sharp_edge)
{
  struct timeval t1, t2;

  mt_edge_manager edge_manager(mesh);
  mt_edge_splitter splitter(edge_manager);
  mt_edge_collapser collapser(edge_manager);
  float smth_base = SMTH_BASE, smth_change = SMTH_CHANGE, edge_ang = sharp_edge;
  float iter_base = ITER_BASE;

  // splitting run ========================================================================
  if(max > 0) {
    mt_vector<bool> boundary_node;
    if(fix_bnd) {
      if(unif_split) {
        fprintf(stderr, "Warning: Boundary fixing and uniform refinement exclude each other!\n"
            "Turning off uniform refinement!\n");
        unif_split = false;
      }

      mt_meshdata surfmesh;
      compute_surface(mesh, tags, surfmesh, false);

      // if fix_bnd is set, we insert the surface nodes of the geometric surface into
      // the boundary_node mask, which is tested in the splitting phase
      boundary_node.resize(mesh.n2e_cnt.size(), false);
      for(const mt_idx_t & nidx : surfmesh.e2n_con) boundary_node[nidx] = true;
    }

    bool splt_update;

    if(unif_split)
      splt_update = uniform_splitting(mesh, tags, edge_manager, max);
    else
      splt_update = non_uniform_splitting(mesh, tags, splitter, edge_manager, boundary_node, max);

    if(splt_update) {
      // finish splitting
      correct_insideOut(mesh);
      edge_manager.refresh_edge_data(true);
      compute_full_mesh_connectivity(mesh, false);
      if (dosmooth)
        volumetric_smooth_from_tags(mesh, alltags, iter_base, smth_base, 1, edge_ang, RESAMPLE_QUAL_THR, false, false);
    }
  }

  // collapsing run ========================================================================
  if(min > 0) {
    size_t iter = 0;
    size_t nsel = 0, nsel_old = 0, nsel_init = 0;
    char msg[1024];

    // compute geometric surface
    std::cout << "Computing surface and surface normals .." << std::endl;
    gettimeofday(&t1, NULL);
    mt_vector<bool> surf_nod;
    mt_vector<mt_real> surf_nrml;
    mt_vector<bool> fixed_nod,       // nodes that we have to collapse towards
                    boundary_node;   // nodes that cannot take part in collapsing

    // compute surface normals and fixed nods if fix_bnd == 1
    update_surface_data(mesh, tags, fix_bnd, surf_nod, boundary_node, surf_nrml);

    {
      mt_meshdata surfmesh, linemesh;
      unified_surface_from_tags(mesh, alltags, surfmesh, NULL);
      compute_full_mesh_connectivity(surfmesh, false);
      compute_line_interfaces(mesh, surfmesh, 0.0, false, linemesh);
      fixed_nod.assign(mesh.n2e_cnt.size(), false);
      for(auto c : linemesh.e2n_con) fixed_nod[c] = true;
    }

    gettimeofday(&t2, NULL);
    std::cout << "Done in " << (float)timediff_sec(t1, t2) << " sec" << std::endl;

    edge_len_asc asc_comparator;
    std::set<edgeele, edge_len_asc> sel_edges;
    bool collapse_update = false;

    // if we have do_conv == false, we want to limit the number of iterations and
    // the number of smoothing passes we want to allow to save time and not change
    // the mesh too much
    const size_t max_passes = 8;
    const size_t max_iter   = dosmooth ? max_passes * ITER_THRS : 10000;
    // we use this iteration variables in case we have do_conv == true. Then we
    // want to only stop after ITER_THRS iterations with zero improvement. the reasoning
    // is that we want to wait for a smoothing step to occur and then retry collapsing
    // before we call it a day.
    size_t max_zero_iter = dosmooth ? ITER_THRS : 1, zero_iter = 0;

    // we store here the ratio between selected edges and total edge count
    double diff_to_nsel;
    bool iter_control_continue = false;
    long int diff = 0;
    const MT_MAP<mt_tuple<mt_idx_t>, mt_idx_t> & edges = edge_manager.edges();

    // main collapsing loop
    do {
      sel_edges.clear();

#ifdef OPENMP
      #pragma omp parallel
#endif
      {
        MT_USET<edgeele>   sel_edges_set;
        mt_vector<edgeele> elem_edge_lengths;

#ifdef OPENMP
        #pragma omp for schedule(dynamic, 16)
#endif
        for(size_t eidx=0; eidx<mesh.e2n_cnt.size(); eidx++) {
          if(tags.count(mesh.etags[eidx])) {
            add_edges_lengths(mesh, eidx, elem_edge_lengths);

            edgeele shortest = *std::min_element(elem_edge_lengths.begin(), elem_edge_lengths.end(), asc_comparator);
            bool edge_is_fixed = false;
            if(fix_bnd) {
              bool v1_fixed = boundary_node[shortest.v1], v2_fixed = boundary_node[shortest.v2];
              edge_is_fixed = v1_fixed || v2_fixed;
            }

            if(!edge_is_fixed && shortest.length < min)
              sel_edges_set.insert(shortest);
          }
        }

        for(auto & e : sel_edges_set) {
          e.split_at = 0.5f;
          auto it = edges.find({e.v1, e.v2});
          e.idx = it->second;
        }

#ifdef OPENMP
        #pragma omp critical
#endif
        {
          sel_edges.insert(sel_edges_set.begin(), sel_edges_set.end());
        }
      }

      nsel_old = nsel = sel_edges.size();
      iter++;
      diff_to_nsel = 0.0;

      if(nsel) {
        // store the number of initially selected edges
        if(nsel_init == 0) nsel_init = nsel;

        double nsel_to_total = double(nsel) / double(edges.size()) * 100.0;

        PROGRESS<size_t>* prog = NULL;
        if(nsel_to_total > 0.5) {
          sprintf(msg, "Iter %zu: collapse %zu edges (%.4lf %%)", iter, nsel, nsel_to_total);
          prog = new PROGRESS<size_t>(4, msg);
        }

        gettimeofday(&t1, NULL);

        // call collapser to do collapsing work
        collapser(sel_edges, surf_nod, surf_nrml, fixed_nod, surf_corr, prog);
        nsel = sel_edges.size();
        diff = nsel_old - nsel;
        diff_to_nsel = double(diff) / double(nsel_init);

        gettimeofday(&t2, NULL);
        if(nsel_to_total > 0.5) {
          printf("Collapsed %ld (%.2f %%) in %.2f sec\n", diff, float(diff) / float(nsel_old) * 100.0f,
                 (float)timediff_sec(t1, t2));
          delete prog;
        }

        // if we collapsed some edges we mark that work has been done
        if(diff > 0) collapse_update = true;

        // if post-smoothing is turned on, we do a smoothing step every ITER_THRS iterations
        // we reduce smoothing strength every time we smoothed to reduce volumetric
        // shrinking
        if(dosmooth && iter % ITER_THRS == 0) {
          compute_full_mesh_connectivity(mesh);
          volumetric_smooth_from_tags(mesh, alltags, iter_base, smth_base, 1, edge_ang, RESAMPLE_QUAL_THR, false, false);
          update_surface_data(mesh, tags, fix_bnd, surf_nod, boundary_node, surf_nrml);
          zero_iter = 0;

          if(do_conv == false) {
            smth_base *= smth_change;
            iter_base *= smth_change;
          }
        }
      }

      // in this flag we set all iteration control logic: if do_conv is true, we
      // iterate unter no more collapses could be made. if it is false, we stop earlier
      // to save iterations
      if(do_conv) {
        if(diff == 0) zero_iter++;
        iter_control_continue = nsel > 0 && zero_iter < max_zero_iter;
      }
      else
        iter_control_continue = nsel > 0 && diff_to_nsel > 1e-3 && iter < max_iter;
    }
    while(iter_control_continue);

    if(collapse_update) {
      // finish collapsing
      correct_insideOut(mesh);
      edge_manager.refresh_edge_data(true);
      compute_full_mesh_connectivity(mesh);
    }
  }
}

void surface_intersect_splitting(mt_meshdata & mesh,
                                 mt_edge_splitter & splitter,
                                 mt_edge_manager & edge_manager,
                                 const kdtree & tree,
                                 const MT_USET<mt_idx_t> & fixed_nod,
                                 const float prec,
                                 const bool final_split)
{
  timeval t1, t2;
  size_t iter = 0, max_iter = 20, nsel_merge = 0;
  char msg[1024];

  float max_len = prec * 1.5f; max_len = max_len * max_len;
  bool having_fixed_nodes = fixed_nod.size();

  kdtree fix_tree(16);
  if(having_fixed_nodes) {
    mt_vector<vec3r> fixed_pts; fixed_pts.reserve(fixed_nod.size());

    for(mt_idx_t v : fixed_nod)
      fixed_pts.push_back(vec3r(mesh.xyz.data() + v*3));

    fix_tree.build_vertex_tree(fixed_pts);
  }

  mt_vector<edgeele> sel_edges;

  mt_vector<edgeele> elem_edge_lengths;
  edge_len_asc ascending;

  const MT_MAP<mt_tuple<mt_idx_t>,mt_idx_t> & edges = edge_manager.edges();

  do {
    sel_edges.resize(0);
    sel_edges.reserve(edges.size() / 4);

    MT_USET<mt_idx_t> sel_elem, sel_nod;
    // mark elements connected to edges that intersect the surface for splitting
    for(auto eit = edges.begin(); eit != edges.end(); ++eit)
    {
      mt_idx_t v1 = eit->first.v1, v2 = eit->first.v2;

      vec3f p1(mesh.xyz.data() + v1*3), p2(mesh.xyz.data() + v2*3);
      vec3f e = p2 - p1;

      tri_elem hit_tri;
      vec3r    hit_pos;
      int did_intersec = tree.closest_intersect(ray(p1, e), 0.05, 0.95, hit_tri, hit_pos);
      mt_real edge_len2 = e.length2();

      if(did_intersec && edge_len2 > max_len)
        elements_with_edge(mesh, v1, v2, sel_elem);
    }

    // extend elements to split by one layer in order to reduce element anisotropy
    if(iter < 4) {
      elemSet_to_nodeSet(mesh, sel_elem, sel_nod);
      nodeSet_to_elemSet(mesh, sel_nod, sel_elem);
    }

    // split selected elements along their longest edge
    for(const mt_idx_t eidx : sel_elem)
    {
      add_edges_lengths(mesh, eidx, elem_edge_lengths);

      edgeele longest = *std::max_element(elem_edge_lengths.begin(), elem_edge_lengths.end(), ascending);

      bool edge_is_fixed = false;
      if(having_fixed_nodes) {
        mt_idx_t v1 = longest.v1, v2 = longest.v2;
        vec3r p1(mesh.xyz.data() + v1*3), p2(mesh.xyz.data() + v2*3);

        int idx; vec3r cl; mt_real len2;
        fix_tree.closest_vertex(p1, idx, cl, len2, false);
        bool v1_fixed = sqrt(len2) < (prec * 1e-6);

        fix_tree.closest_vertex(p2, idx, cl, len2, false);
        bool v2_fixed = sqrt(len2) < (prec * 1e-6);

        // we cannot touch edges going to the fixed surface. first, because sometimes vertices
        // switch and we might brake the logic, second for elem quality reasons
        edge_is_fixed = v1_fixed || v2_fixed;
      }

      if(!edge_is_fixed) {
        longest.split_at = 0.5f;

        auto it = edges.find({longest.v1, longest.v2});
        longest.idx = it->second;

        sel_edges.push_back(longest);
      }
    }

    // do splitting
    nsel_merge = sel_edges.size();
    iter++;

    if(nsel_merge) {
      sprintf(msg, "Iter %zu: splitting %zu edges, %.4f %% of total %zu ",
              iter, nsel_merge, float(nsel_merge)/edges.size()*100.0f, edges.size());
      PROGRESS<size_t> prog(nsel_merge, msg);
      gettimeofday(&t1, NULL);

      splitter(sel_edges, &prog);
      transpose_connectivity(mesh.e2n_cnt, mesh.e2n_con, mesh.n2e_cnt, mesh.n2e_con);
      mesh.n2e_dsp.resize(mesh.n2e_cnt.size());
      bucket_sort_offset(mesh.n2e_cnt, mesh.n2e_dsp);

      gettimeofday(&t2, NULL);
      std::cout << "Done in " << (float)timediff_sec(t1, t2) << " sec" << std::endl;
    }
  }
  while(nsel_merge > 0 && iter < max_iter);

  // We maybe want to have a last pass were we do something smart to improve the
  // surface smoothness of the sampled mesh.
  if(final_split) {
    sel_edges.resize(0);
    for(auto eit = edges.begin(); eit != edges.end(); ++eit)
    {
      mt_idx_t v1 = eit->first.v1, v2 = eit->first.v2;
      mt_idx_t edge_idx = eit->second;

      vec3r p1(mesh.xyz.data() + v1*3), p2(mesh.xyz.data() + v2*3);
      vec3r e = p2 - p1;

      tri_elem hit_tri;
      vec3r    hit_pos;
      int did_intersec = tree.closest_intersect(ray(p1, e), 0.0, 1.0, hit_tri, hit_pos);

      if(did_intersec) {
        mt_real hit_dist = (hit_pos - p1).length();
        mt_real split_at = hit_dist / e.length();

        if(split_at < 0.05) // for intersection close to p1 we move p1
        {
          p1 = hit_pos;
          p1.set(mesh.xyz.data() + v1*3);
        }
        else if(split_at > 0.95)  // for intersection close to p2 we move p2
        {
          p2 = hit_pos;
          p2.set(mesh.xyz.data() + v2*3);
        }
        else if(split_at > 0.2 || split_at < 0.8) // for intersection towards the center we split
          sel_edges.push_back({v1, v2, edge_idx, e.length2(), float(split_at)});
      }
    }

    nsel_merge = sel_edges.size();
    if(nsel_merge) {
      sprintf(msg, "Final merge split: splitting %zu edges, %.4f %% of total %zu ",
              nsel_merge, float(nsel_merge) / edges.size(), edges.size());
      PROGRESS<size_t> prog(nsel_merge, msg);
      gettimeofday(&t1, NULL);

      splitter(sel_edges, &prog);

      gettimeofday(&t2, NULL);
      std::cout << "Done in " << (float)timediff_sec(t1, t2) << " sec" << std::endl;
    }
  }

  // finish splitting
  correct_insideOut(mesh);
  edge_manager.refresh_edge_data(true);
}

void surface_distance_splitting(mt_meshdata & mesh,
                                mt_edge_splitter & splitter,
                                mt_edge_manager & edge_manager,
                                const kdtree & bdry_tree,
                                const kdtree & tree,
                                const mt_mask & bdry_mask,
                                const float bdry_res,
                                const float final_res)
{
  timeval t1, t2;
  size_t iter = 0, max_iter = 20, nsel_merge = 0;
  char msg[1024];

  mt_vector<edgeele> sel_edges;
  edge_len_asc ascending;
  const MT_MAP<mt_tuple<mt_idx_t>,mt_idx_t> & edges = edge_manager.edges();

  do {
    sel_edges.resize(0);

#ifdef OPENMP
    #pragma omp parallel
#endif
    {
      MT_USET<edgeele>   sel_edges_set;
      mt_vector<edgeele> elem_edge_lengths;

#ifdef OPENMP
      #pragma omp for schedule(dynamic, 16)
#endif
      for(size_t eidx=0; eidx < mesh.e2n_cnt.size(); eidx++)
      {
        vec3r ctr = barycenter(mesh.e2n_cnt[eidx],
                               mesh.e2n_con.data() + mesh.e2n_dsp[eidx],
                               mesh.xyz.data());

        mt_real dist_from_start, dist_to_end;

        int idx; vec3r cl;
        tree     .closest_vertex(ctr, idx, cl, dist_from_start, false);
        bdry_tree.closest_vertex(ctr, idx, cl, dist_to_end, false);

        dist_from_start = sqrt(dist_from_start);
        dist_to_end     = sqrt(dist_to_end);

        // element distance between boundary and split surf between 0 and 1
        mt_real elem_dist = dist_from_start / (dist_from_start + dist_to_end);
        mt_real target_res = lerp(elem_dist, final_res, bdry_res);

        add_edges_lengths(mesh, eidx, elem_edge_lengths);
        edgeele longest = *std::max_element(elem_edge_lengths.begin(), elem_edge_lengths.end(), ascending);
        mt_real edge_len = sqrt(longest.length);

        if(edge_len * 0.75 > target_res) {
          bool edge_is_fixed = false;
          mt_idx_t v1 = longest.v1, v2 = longest.v2;
          bool v1_fixed = bdry_mask.count(v1);
          bool v2_fixed = bdry_mask.count(v2);
          // we cannot touch edges going to the boundary surface. first, because sometimes vertices
          // switch and we might brake the logic, second for elem quality reasons
          edge_is_fixed = v1_fixed || v2_fixed;

          if(!edge_is_fixed)
            sel_edges_set.insert(longest);
        }
      }

#ifdef OPENMP
      #pragma omp critical
#endif
      {
        sel_edges.push_back(sel_edges_set.begin(), sel_edges_set.end());
      }
    }

    bool inconsistent_edges = false;

    for(auto & e : sel_edges) {
      e.split_at = 0.5f;
      auto it = edges.find({e.v1, e.v2});
      if(it == edges.end())
        inconsistent_edges = true;
      else
        e.idx = it->second;
    }

    if(inconsistent_edges) {
      fprintf(stderr, "%s error: Stopping due to inconsisten edges!\n", __func__);
      sel_edges.resize(0);
    }

    // do splitting
    nsel_merge = sel_edges.size();
    iter++;

    if(nsel_merge) {
      sprintf(msg, "Iter %zu: splitting %zu edges, %.4f %% of total %zu ",
              iter, nsel_merge, float(nsel_merge)/edges.size()*100.0f, edges.size());
      PROGRESS<size_t> prog(nsel_merge, msg);
      gettimeofday(&t1, NULL);

      splitter(sel_edges, &prog);

      gettimeofday(&t2, NULL);
      std::cout << "Done in " << (float)timediff_sec(t1, t2) << " sec" << std::endl;
    }
  }
  while(nsel_merge > 0 && iter < max_iter);

  // finish splitting
  correct_insideOut(mesh);
  edge_manager.refresh_edge_data(true);
}

bool cable_is_line(const mt_pscable & cab, mt_real thr)
{
  size_t num_inp_pts = cab.pts.size() / 3;
  mt_point<mt_real> s(cab.pts.data()), e(cab.pts.data() + (num_inp_pts-1)*3);
  mt_point<mt_real> d = e - s; d.normalize();

  for(size_t i=0; i<num_inp_pts; i++)
  {
    mt_point<mt_real> p = mt_point<mt_real>(cab.pts.data() + i*3) - s;
    p.normalize();
    mt_real sca = p.scaProd(d);
    if(sca < thr) return false;
  }

  return true;
}

void get_cable_displacements(const mt_pscable & cab, mt_vector<mt_real> & dsp)
{
  dsp.resize(cab.pts.size() / 3);
  dsp[0] = 0.0;

  const mt_real* xyz = cab.pts.data();
  vec3r p0(xyz), p1;
  xyz += 3;

  for(size_t i=1; i<cab.pts.size()/3; i++) {
    p1.get(xyz);
    dsp[i] = dsp[i-1] + (p1 - p0).length();

    p0 = p1;
    xyz += 3;
  }
}

void resample_purk_cable(mt_pscable & cab,
                         const mt_vector<mt_real> & dsp,
                         mt_vector<mt_real> & xyzbuff,
                         const int numint)
{
  size_t num_inp_pts = cab.pts.size() / 3;
  mt_real len = dsp[dsp.size()-1];

  // buffer old points for sampling the intervals
  xyzbuff = cab.pts;
  mt_point<mt_real> s(xyzbuff.data()), e(xyzbuff.data() + (num_inp_pts-1)*3);
  int numpts = numint + 1;

  cab.pts.resize(numpts*3);

  // start point
  cab.pts[0] = s.x;
  cab.pts[1] = s.y;
  cab.pts[2] = s.z;

  int inp_segm_idx = 0;
  mt_real dx = len / numint;

  // intermediate points
  for(int iidx = 1; iidx < numpts - 1; iidx++)
  {
    mt_real next_outp_len = iidx * dx;
    while(inp_segm_idx < int(dsp.size()-1) && next_outp_len > dsp[inp_segm_idx]) inp_segm_idx++;
    int v0 = inp_segm_idx-1, v1 = inp_segm_idx;

    vec3r p0(xyzbuff.data() + v0*3), p1(xyzbuff.data() + v1*3);
    vec3r p = lerp(dsp[v0], dsp[v1], next_outp_len, p0, p1);

    cab.pts[iidx*3+0] = p.x;
    cab.pts[iidx*3+1] = p.y;
    cab.pts[iidx*3+2] = p.z;
  }

  // end point
  cab.pts[(numpts-1)*3+0] = e.x;
  cab.pts[(numpts-1)*3+1] = e.y;
  cab.pts[(numpts-1)*3+2] = e.z;
}


void resample_purkinje(struct mt_psdata & ps, mt_vector<int> & cables, float size)
{
  // cable segment displacements
  mt_vector<mt_real> dsp;
  mt_vector<mt_real> xyz; // here we store the input point coordinates when resampling

  if(cables.size() == 0)
    range<int>(0, ps.cables.size(), cables);

  for(int cidx : cables)
  {
    struct mt_pscable & ccab = ps.cables[cidx];

    get_cable_displacements(ccab, dsp);

    // determine segment length and number of required intervals
    mt_real len = dsp[dsp.size()-1];
    int numint = int(len / size) + 1;

    resample_purk_cable(ccab, dsp, xyz, numint);
  }
}

mt_real avrg_purk_resolution(struct mt_psdata & ps, mt_vector<int> & eval_cab)
{
  mt_real avrg_size = 0.0;
  size_t  intervals = 0;

  // cable segment displacements
  mt_vector<mt_real> dsp, cnt;

  for(int cidx : eval_cab)
  {
    struct mt_pscable & ccab = ps.cables[cidx];

    get_cable_displacements(ccab, dsp);
    cnt_from_dsp(dsp, cnt);

    for(mt_real c : cnt) {
      avrg_size += c;
      intervals++;
    }
  }

  if(intervals) avrg_size /= intervals;

  return avrg_size;
}

void minimize_purkinje(struct mt_psdata & ps)
{
  for(size_t cidx = 0; cidx < ps.cables.size(); cidx++)
  {
    struct mt_pscable & ccab = ps.cables[cidx];

    // jump over cables that are not a geometric line
    bool isLine = cable_is_line(ccab, mt_real(0.9));
    if(!isLine) continue;

    size_t num_inp_pts = ccab.pts.size() / 3;
    mt_point<mt_real> s(ccab.pts.data()), e(ccab.pts.data() + (num_inp_pts-1)*3);

    // we have only 2 points
    ccab.pts.resize(6);

    // start point
    ccab.pts[0] = s.x;
    ccab.pts[1] = s.y;
    ccab.pts[2] = s.z;

    // end point
    ccab.pts[3] = e.x;
    ccab.pts[4] = e.y;
    ccab.pts[5] = e.z;
  }
}
