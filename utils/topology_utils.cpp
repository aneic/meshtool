/**
* @file topology_utils.cpp
* @brief Topology manipulation utility functions.
* @author Aurel Neic
* @version
* @date 2017-08-16
*/

#include "mt_utils_base.h"
#include "io_utils.h"
#include "mesh_utils.h"
#include "mesh_smoothing.h"
#include "mesh_quality.h"
#include "topology_utils.h"
#include "dense_mat.hpp"


void elements_in_selection(const mt_meshdata & mesh, mt_vector<mt_idx_t> & found_elem,
                           const MT_USET<mt_tag_t> & visible_tags, const MT_USET<mt_idx_t> & sel_idx)
{
  MT_USET<mt_idx_t> nodset, eleset;
  nodset.insert(sel_idx.begin(), sel_idx.end());
  nodeSet_to_elemSet(mesh, nodset, eleset);

  mt_vector<mt_idx_t> check_eidx;
  check_eidx.assign(eleset.begin(), eleset.end());

  found_elem.reserve(1000);

  #ifdef OPENMP
  #pragma omp parallel
  #endif
  {
    mt_vector<mt_idx_t> loc_found_elem;
    loc_found_elem.reserve(1000);

    #ifdef OPENMP
    #pragma omp for schedule(guided)
    #endif
    for(size_t i=0; i<check_eidx.size(); i++) {
      mt_idx_t eidx = check_eidx[i];

      mt_idx_t start = mesh.e2n_dsp[eidx], stop = start + mesh.e2n_cnt[eidx], scnt = 0;
      for(mt_idx_t j=start; j<stop; j++) {
        mt_idx_t c = mesh.e2n_con[j];
        if(sel_idx.count(c))
          scnt++;
      }
      if(scnt == mesh.e2n_cnt[eidx] && (visible_tags.size() == 0 || visible_tags.count(mesh.etags[eidx])))
        loc_found_elem.push_back(eidx);
    }

    #ifdef OPENMP
    #pragma omp critical
    #endif
    {
      for(mt_idx_t & idx : loc_found_elem)
        found_elem.push_back(idx);
    }
  }
}

void compute_full_mesh_connectivity(mt_meshdata & mesh, bool verbose)
{
  // mt_timer full_time, trsp_time, n2n_time;
  // full_time.start();

  PROGRESS<short>* prog = NULL;

  if(verbose)
    prog = new PROGRESS<short>(3, "Computing connectivity graphs: ");

  bucket_sort_offset(mesh.e2n_cnt, mesh.e2n_dsp);
  if(prog) prog->next();

  // trsp_time.start();
  transpose_connectivity_par(mesh.e2n_cnt, mesh.e2n_dsp, mesh.e2n_con,
                             mesh.n2e_cnt, mesh.n2e_dsp, mesh.n2e_con, false);
  // trsp_time.stop();
  if(prog) prog->next();

  // n2n_time.start();
  compute_n2n_par(mesh.e2n_cnt, mesh.e2n_dsp, mesh.e2n_con,
                  mesh.n2e_cnt, mesh.n2e_dsp, mesh.n2e_con,
                  mesh.n2n_cnt, mesh.n2n_dsp, mesh.n2n_con);
  // n2n_time.stop();
  if(prog) prog->next();

  // full_time.stop();

  if(prog) {
    prog->finish();
    delete prog;
  }

  // printf("connectivity timings: %.3lf full, %.3lf transposing, %.3lf mat-multiply\n",
  //         full_time.get_time_sec(), trsp_time.get_time_sec(), n2n_time.get_time_sec());
}

void compute_full_mesh_connectivity(mt_meshdata & mesh, std::string basename, bool verbose)
{
  std::string filename = basename + FULL_CON_EXT;
  bool have_read_con = false;

  if(file_exists(filename)) {
    read_full_mesh_connectivity(mesh, filename);
    have_read_con = true;

    // do checks
    if(mesh.n2e_cnt.size() != (mesh.xyz.size() / 3))
      have_read_con = false;

    if(mesh.n2n_cnt.size() != (mesh.xyz.size() / 3))
      have_read_con = false;

    mt_idx_t e2n_edges = bucket_sort_size(mesh.e2n_cnt);
    mt_idx_t n2e_edges = bucket_sort_size(mesh.n2e_cnt);

    if(e2n_edges != n2e_edges)
      have_read_con = false;

    // notify user that data is inconsistent
    if(have_read_con == false)
      fprintf(stderr, "%s warning: connectivity read from %s is inconsistent! Recomputing it!\n",
              __func__, filename.c_str());
  }

  if(have_read_con == false) {
    compute_full_mesh_connectivity(mesh, verbose);
    write_full_mesh_connectivity(mesh, filename);
  }
}

void elements_with_edge(const mt_meshdata & mesh, mt_idx_t v0, mt_idx_t v1, MT_USET<mt_idx_t> & elemset)
{
  mt_idx_t estart = mesh.n2e_dsp[v0], estop = estart + mesh.n2e_cnt[v0];
  for(mt_idx_t i=estart; i<estop; i++)
  {
    mt_idx_t eidx = mesh.n2e_con[i];
    mt_idx_t nstart = mesh.e2n_dsp[eidx], nstop = nstart + mesh.e2n_cnt[eidx];
    for(mt_idx_t j=nstart; j<nstop; j++)
    {
      mt_idx_t nidx = mesh.e2n_con[j];
      if(nidx == v1) {
        elemset.insert(eidx);
        break;
      }
    }
  }
}

void elements_with_face(const mt_meshdata & mesh, mt_idx_t v0, mt_idx_t v1, mt_idx_t v2,
                        MT_USET<mt_idx_t> & elemset)
{
  // iterate over all elements connected to v0 and look for (v1, v2)
  mt_idx_t estart = mesh.n2e_dsp[v0], estop = estart + mesh.n2e_cnt[v0];
  for(mt_idx_t i=estart; i<estop; i++)
  {
    mt_idx_t eidx = mesh.n2e_con[i];
    mt_idx_t nstart = mesh.e2n_dsp[eidx], nsize = mesh.e2n_cnt[eidx];
    const mt_idx_t* con = mesh.e2n_con.data() + nstart;
    short pos1 = -1, pos2 = -1;
    for(mt_idx_t j=0; j<nsize; j++)
    {
      mt_idx_t nidx = con[j];
      if(nidx == v1) pos1 = j;
      else if(nidx == v2) pos2 = j;
    }
    // (v1, v2) found -> insert eidx
    if( (pos1 > -1) && (pos2 > -1) )
      elemset.insert(eidx);
  }
}

mt_idx_t get_opposite_element(const mt_idx_t* con,
                              const mt_meshdata & vol,
                              const mt_idx_t our_eidx)
{
  mt_idx_t v0 = con[0], v1 = con[1], v2 = con[2];

  MT_USET<mt_idx_t> ele;
  elements_with_face(vol, v0, v1, v2, ele);

  if(ele.size() > 1) {
    for(mt_idx_t e : ele)
      if(e != our_eidx)
        return e;
  }

  return our_eidx;
}


void tet_refine_centerPoint(mt_meshdata & mesh, const mt_vector<mt_idx_t> & ref_elem)
{
  size_t old_nelem = mesh.e2n_cnt.size();
  size_t old_npts  = mesh.xyz.size() / 3;

  short lonsize = mesh.lon.size() == old_nelem * 6 ? 6 : 3;

  size_t nelem = old_nelem + ref_elem.size() * 4;
  mesh_resize_elemdata(mesh, nelem, nelem*4);
  mesh.xyz.resize(old_npts*3 + ref_elem.size()*3);

  PROGRESS<size_t> prog(ref_elem.size(), "Center-Point refining progress: ");

  size_t write_pidx = old_npts;
  size_t write_eidx = old_nelem;

  for(auto it = ref_elem.begin(); it != ref_elem.end(); ++it)
  {
    prog.next();
    mt_idx_t eidx = *it;
    mt_idx_t v0 = mesh.e2n_con[eidx*4+0];
    mt_idx_t v1 = mesh.e2n_con[eidx*4+1];
    mt_idx_t v2 = mesh.e2n_con[eidx*4+2];
    mt_idx_t v3 = mesh.e2n_con[eidx*4+3];

    // computing center point
    mt_point<mt_real> cp(0, 0, 0);
    cp += mt_point<mt_real>(mesh.xyz.data()+v0*3);
    cp += mt_point<mt_real>(mesh.xyz.data()+v1*3);
    cp += mt_point<mt_real>(mesh.xyz.data()+v2*3);
    cp += mt_point<mt_real>(mesh.xyz.data()+v3*3);
    cp /= 4.0;

    // write new point
    mesh.xyz[write_pidx*3+0] = cp.x;
    mesh.xyz[write_pidx*3+1] = cp.y;
    mesh.xyz[write_pidx*3+2] = cp.z;

    // write new elems
    mt_fib_t* read_lon = mesh.lon.data() + eidx*lonsize;

    mesh.e2n_cnt[write_eidx] = 4;
    mesh.etags  [write_eidx] = mesh.etags[eidx];
    mesh.etype  [write_eidx] = Tetra;
    mesh.e2n_con[write_eidx*4+0] = v0;
    mesh.e2n_con[write_eidx*4+1] = v1;
    mesh.e2n_con[write_eidx*4+2] = v2;
    mesh.e2n_con[write_eidx*4+3] = write_pidx;
    memcpy(mesh.lon.data() + write_eidx*lonsize, read_lon, lonsize*sizeof(mt_fib_t));
    write_eidx++;

    mesh.e2n_cnt[write_eidx] = 4;
    mesh.etags  [write_eidx] = mesh.etags[eidx];
    mesh.etype  [write_eidx] = Tetra;
    mesh.e2n_con[write_eidx*4+0] = v1;
    mesh.e2n_con[write_eidx*4+1] = v3;
    mesh.e2n_con[write_eidx*4+2] = v2;
    mesh.e2n_con[write_eidx*4+3] = write_pidx;
    memcpy(mesh.lon.data() + write_eidx*lonsize, read_lon, lonsize*sizeof(mt_fib_t));
    write_eidx++;

    mesh.e2n_cnt[write_eidx] = 4;
    mesh.etags  [write_eidx] = mesh.etags[eidx];
    mesh.etype  [write_eidx] = Tetra;
    mesh.e2n_con[write_eidx*4+0] = v0;
    mesh.e2n_con[write_eidx*4+1] = v2;
    mesh.e2n_con[write_eidx*4+2] = v3;
    mesh.e2n_con[write_eidx*4+3] = write_pidx;
    memcpy(mesh.lon.data() + write_eidx*lonsize, read_lon, lonsize*sizeof(mt_fib_t));
    write_eidx++;

    mesh.e2n_cnt[write_eidx] = 4;
    mesh.etags  [write_eidx] = mesh.etags[eidx];
    mesh.etype  [write_eidx] = Tetra;
    mesh.e2n_con[write_eidx*4+0] = v0;
    mesh.e2n_con[write_eidx*4+1] = v3;
    mesh.e2n_con[write_eidx*4+2] = v1;
    mesh.e2n_con[write_eidx*4+3] = write_pidx;
    memcpy(mesh.lon.data() + write_eidx*lonsize, read_lon, lonsize*sizeof(mt_fib_t));
    write_eidx++;

    write_pidx++;
  }
  prog.finish();

  mt_vector<bool> keep(mesh.e2n_cnt.size(), true);

  for(auto it = ref_elem.begin(); it != ref_elem.end(); ++it)
    keep[*it] = false;

  mt_vector<mt_idx_t> nod, eidx;
  restrict_meshdata(keep, mesh, nod, eidx);
}


void refine_uniform(mt_meshdata & mesh,
                    const MT_MAP<mt_tuple<mt_idx_t>, mt_idx_t> & edge_map)
{
  mt_meshdata outmesh;

  size_t num_elem     = mesh.e2n_cnt.size();
  size_t num_elem_out = 0;
  int numfib = mesh.lon.size() == num_elem* 6 ? 6 : 3;

  for(size_t i=0; i<num_elem; i++) {
    if(mesh.etype[i] == Tri)        num_elem_out += 4;
    else if(mesh.etype[i] == Tetra) num_elem_out += 8;
    else {
      check_elem_type(mesh.etype[i], Tetra, "refine_uniform");
    }
  }

  outmesh.e2n_cnt.resize(num_elem_out);
  outmesh.etype.resize(num_elem_out);
  outmesh.etags.resize(num_elem_out);
  outmesh.lon.resize(num_elem_out*numfib);

  size_t widx = 0;
  // copy fibers and tags
  for(size_t i=0; i<num_elem; i++) {
    if(mesh.etype[i] == Tri) {
      for(int j=0; j<4; j++) {
        outmesh.etype[widx+j]   = Tri;
        outmesh.etags[widx+j]   = mesh.etags[i];
        outmesh.e2n_cnt[widx+j] = 3;
        memcpy(outmesh.lon.data()+(widx+j)*numfib, mesh.lon.data()+i*numfib, numfib*sizeof(mt_fib_t));
      }
      widx += 4;
    }
    else {
      for(int j=0; j<8; j++) {
        outmesh.etype[widx+j]   = Tetra;
        outmesh.etags[widx+j]   = mesh.etags[i];
        outmesh.e2n_cnt[widx+j] = 4;
        memcpy(outmesh.lon.data()+(widx+j)*numfib, mesh.lon.data()+i*numfib, numfib*sizeof(mt_fib_t));
      }
      widx += 8;
    }
  }

  // the edges are also indexing our additional vertices
  size_t num_pts     = mesh.xyz.size() / 3;
  size_t num_pts_out = num_pts + edge_map.size();
  outmesh.xyz.resize(num_pts_out*3);

  mt_real* xyz = mesh.xyz.data(), *outxyz = outmesh.xyz.data();
  // copy coordinates
  {
    memcpy(outxyz, xyz, mesh.xyz.size()*sizeof(mt_real));
    outxyz += mesh.xyz.size();

    for(auto it : edge_map) {
      vec3r p1(xyz + it.first.v1*3);
      vec3r p2(xyz + it.first.v2*3);
      vec3r np = (p1 + p2) * mt_real(0.5);

      mt_idx_t edge_idx = it.second;
      outxyz[edge_idx*3+0] = np.x;
      outxyz[edge_idx*3+1] = np.y;
      outxyz[edge_idx*3+2] = np.z;
    }
  }

  size_t outcon_size = bucket_sort_size(outmesh.e2n_cnt);
  outmesh.e2n_con.resize(outcon_size);

  mt_idx_t *con = mesh.e2n_con.data(), *outcon = outmesh.e2n_con.data();
  mt_tuple<mt_idx_t> buff[64]; // unneeded buff to fit API

  for(size_t i=0; i<num_elem; i++) {
    if(mesh.etype[i] == Tri) {
      mt_idx_t edges[3], v1 = con[0], v2 = con[1], v3 = con[2];
      get_edges_tri(con, edge_map, edges, buff);
      con += 3;

      // a=(1,2) b=(2,3) c=(1,3)
      mt_idx_t a = num_pts + edges[0], b = num_pts + edges[1], c = num_pts + edges[2];

      outcon[0] =v1,  outcon[1] = a, outcon[2] = c; outcon += 3;
      outcon[0] = a,  outcon[1] = b, outcon[2] = c; outcon += 3;
      outcon[0] = a,  outcon[1] =v2, outcon[2] = b; outcon += 3;
      outcon[0] = c,  outcon[1] = b, outcon[2] =v3; outcon += 3;
    }
    else {
      mt_idx_t edges[6], v1 = con[0], v2 = con[1], v3 = con[2], v4 = con[3];
      get_edges_tet(con, edge_map, edges, buff);

      con += 4;
      // a=(1,2) b=(1,4) c=(2,4) d=(2,3) e=(3,4) f=(1,3)
      mt_idx_t a = num_pts + edges[0], b = num_pts + edges[3], c = num_pts + edges[4],
             d = num_pts + edges[1], e = num_pts + edges[5], f = num_pts + edges[2];

      outcon[0] =v1,  outcon[1] = a, outcon[2] = f, outcon[3] = b; outcon += 4;
      outcon[0] = a,  outcon[1] =v2, outcon[2] = d, outcon[3] = c; outcon += 4;
      outcon[0] = d,  outcon[1] =v3, outcon[2] = f, outcon[3] = e; outcon += 4;
      outcon[0] = a,  outcon[1] = d, outcon[2] = f, outcon[3] = c; outcon += 4;
      outcon[0] = c,  outcon[1] = d, outcon[2] = e, outcon[3] = f; outcon += 4;
      outcon[0] = a,  outcon[1] = c, outcon[2] = b, outcon[3] = f; outcon += 4;
      outcon[0] = f,  outcon[1] = e, outcon[2] = b, outcon[3] = c; outcon += 4;
      outcon[0] = b,  outcon[1] = c, outcon[2] = e, outcon[3] =v4; outcon += 4;
    }
  }

  mesh = outmesh;
}

void refine_uniform(mt_meshdata & mesh)
{
  mt_meshdata outmesh;
  MT_MAP<mt_tuple<mt_idx_t>,mt_idx_t> edge_map;

  size_t num_elem     = mesh.e2n_cnt.size();
  size_t num_elem_out = 0;
  int numfib = mesh.lon.size() == num_elem* 6 ? 6 : 3;

  mt_idx_t* con = mesh.e2n_con.data(), *outcon = NULL;
  size_t edge_idx = 0;

  for(size_t i=0; i<num_elem; i++) {
    if(mesh.etype[i] == Tri) {
      num_elem_out += 4;
      add_edges_tri(con, edge_idx, edge_map);
      con += 3;
    }
    else if(mesh.etype[i] == Tetra) {
      num_elem_out += 8;
      add_edges_tet(con, edge_idx, edge_map);
      con += 4;
    }
    else {
      check_elem_type(mesh.etype[i], Tetra, "refine_uniform");
    }
  }

  outmesh.e2n_cnt.resize(num_elem_out);
  outmesh.etype.resize(num_elem_out);
  outmesh.etags.resize(num_elem_out);
  outmesh.lon.resize(num_elem_out*numfib);

  size_t widx = 0;
  // copy fibers and tags
  for(size_t i=0; i<num_elem; i++) {
    if(mesh.etype[i] == Tri) {
      for(int j=0; j<4; j++) {
        outmesh.etype[widx+j]   = Tri;
        outmesh.etags[widx+j]   = mesh.etags[i];
        outmesh.e2n_cnt[widx+j] = 3;
        memcpy(outmesh.lon.data()+(widx+j)*numfib, mesh.lon.data()+i*numfib, numfib*sizeof(mt_fib_t));
      }
      widx += 4;
    }
    else {
      for(int j=0; j<8; j++) {
        outmesh.etype[widx+j]   = Tetra;
        outmesh.etags[widx+j]   = mesh.etags[i];
        outmesh.e2n_cnt[widx+j] = 4;
        memcpy(outmesh.lon.data()+(widx+j)*numfib, mesh.lon.data()+i*numfib, numfib*sizeof(mt_fib_t));
      }
      widx += 8;
    }
  }

  // the edges are also indexing our additional vertices
  size_t num_pts     = mesh.xyz.size() / 3;
  size_t num_pts_out = num_pts + edge_map.size();
  outmesh.xyz.resize(num_pts_out*3);

  mt_real* xyz = mesh.xyz.data(), *outxyz = outmesh.xyz.data();
  // copy coordinates
  {
    memcpy(outxyz, xyz, mesh.xyz.size()*sizeof(mt_real));
    outxyz += mesh.xyz.size();

    for(auto it : edge_map) {
      vec3r p1(xyz + it.first.v1*3);
      vec3r p2(xyz + it.first.v2*3);
      vec3r np = (p1 + p2) * mt_real(0.5);

      mt_idx_t e_idx = it.second;
      outxyz[e_idx*3+0] = np.x;
      outxyz[e_idx*3+1] = np.y;
      outxyz[e_idx*3+2] = np.z;
    }
  }

  size_t outcon_size = bucket_sort_size(outmesh.e2n_cnt);
  outmesh.e2n_con.resize(outcon_size);

  con = mesh.e2n_con.data(), outcon = outmesh.e2n_con.data();
  mt_tuple<mt_idx_t> buff[6]; // unneeded buff to fit API

  for(size_t i=0; i<num_elem; i++) {
    if(mesh.etype[i] == Tri) {
      mt_idx_t edges[3], v1 = con[0], v2 = con[1], v3 = con[2];
      get_edges_tri(con, edge_map, edges, buff);
      con += 3;

      // a=(1,2) b=(2,3) c=(1,3)
      mt_idx_t a = num_pts + edges[0], b = num_pts + edges[1], c = num_pts + edges[2];

      outcon[0] =v1,  outcon[1] = a, outcon[2] = c; outcon += 3;
      outcon[0] = a,  outcon[1] = b, outcon[2] = c; outcon += 3;
      outcon[0] = a,  outcon[1] =v2, outcon[2] = b; outcon += 3;
      outcon[0] = c,  outcon[1] = b, outcon[2] =v3; outcon += 3;
    }
    else {
      mt_idx_t edges[6], v1 = con[0], v2 = con[1], v3 = con[2], v4 = con[3];
      get_edges_tet(con, edge_map, edges, buff);

      con += 4;
      // a=(1,2) b=(1,4) c=(2,4) d=(2,3) e=(3,4) f=(1,3)
      mt_idx_t a = num_pts + edges[0], b = num_pts + edges[3], c = num_pts + edges[4],
             d = num_pts + edges[1], e = num_pts + edges[5], f = num_pts + edges[2];

      outcon[0] =v1,  outcon[1] = a, outcon[2] = f, outcon[3] = b; outcon += 4;
      outcon[0] = a,  outcon[1] =v2, outcon[2] = d, outcon[3] = c; outcon += 4;
      outcon[0] = d,  outcon[1] =v3, outcon[2] = f, outcon[3] = e; outcon += 4;
      outcon[0] = a,  outcon[1] = d, outcon[2] = f, outcon[3] = c; outcon += 4;
      outcon[0] = c,  outcon[1] = d, outcon[2] = e, outcon[3] = f; outcon += 4;
      outcon[0] = a,  outcon[1] = c, outcon[2] = b, outcon[3] = f; outcon += 4;
      outcon[0] = f,  outcon[1] = e, outcon[2] = b, outcon[3] = c; outcon += 4;
      outcon[0] = b,  outcon[1] = c, outcon[2] = e, outcon[3] =v4; outcon += 4;
    }
  }

  mesh.swap(outmesh);
}

void add_edge(const mt_idx_t v1,
              const mt_idx_t v2,
              mt_tuple<mt_idx_t> & tupbuff,
              size_t & edgeidx,
              mt_idx_t* ele2edge_con,
              MT_MAP<mt_tuple<mt_idx_t>, mt_idx_t> & edge_map)
{
  sortTuple(v1, v2, tupbuff.v1, tupbuff.v2);
  auto it = edge_map.find(tupbuff);

  if(it != edge_map.end())
    *ele2edge_con = it->second;
  else {
    edge_map[tupbuff] = mt_idx_t(edgeidx);
    *ele2edge_con = edgeidx;
    edgeidx++;
  }
}
void add_edge(const mt_idx_t v1,
              const mt_idx_t v2,
              mt_tuple<mt_idx_t> & tupbuff,
              size_t & edgeidx,
              MT_MAP<mt_tuple<mt_idx_t>, mt_idx_t> & edge_map)
{
  sortTuple(v1, v2, tupbuff.v1, tupbuff.v2);
  auto it = edge_map.find(tupbuff);

  if(it == edge_map.end())
    edge_map[tupbuff] = mt_idx_t(edgeidx++);
}

void add_face(const mt_idx_t v1,
              const mt_idx_t v2,
              const mt_idx_t v3,
              mt_triple<mt_idx_t> & tripbuff,
              size_t & faceidx,
              mt_idx_t* ele2face_con,
              MT_MAP<mt_triple<mt_idx_t>, mt_idx_t> & face_map)
{
  sortTriple(v1, v2, v3, tripbuff.v1, tripbuff.v2, tripbuff.v3);
  auto it = face_map.find(tripbuff);

  if(it != face_map.end())
    *ele2face_con = it->second;
  else {
    face_map[tripbuff] = mt_idx_t(faceidx);
    *ele2face_con = faceidx;
    faceidx++;
  }
}

mt_idx_t get_edge(const mt_idx_t v1,
                const mt_idx_t v2,
                mt_tuple<mt_idx_t> & tupbuff,
                const MT_MAP<mt_tuple<mt_idx_t>, mt_idx_t> & edge_map)
{
  sortTuple(v1, v2, tupbuff.v1, tupbuff.v2);
  auto it = edge_map.find(tupbuff);

  if(it != edge_map.end()) return it->second;
  else                     return -1;
}

void add_edges_line(const mt_idx_t* mesh_con,
                    mt_idx_t* ele2edge_con,
                    size_t & edgeidx,
                    MT_MAP<mt_tuple<mt_idx_t>, mt_idx_t> & edge_map)
{
  struct mt_tuple<mt_idx_t> tupbuff;

  mt_idx_t v1 = mesh_con[0];
  mt_idx_t v2 = mesh_con[1];

  add_edge(v1, v2, tupbuff, edgeidx, ele2edge_con+0, edge_map);
}
void add_edges_line(const mt_idx_t* mesh_con,
                    size_t & edgeidx,
                    MT_MAP<mt_tuple<mt_idx_t>, mt_idx_t> & edge_map)
{
  struct mt_tuple<mt_idx_t> tupbuff;

  mt_idx_t v1 = mesh_con[0];
  mt_idx_t v2 = mesh_con[1];

  add_edge(v1, v2, tupbuff, edgeidx, edge_map);
}

void add_edges_tri(const mt_idx_t* mesh_con,
                   mt_idx_t* ele2edge_con,
                   size_t & edgeidx,
                   MT_MAP<mt_tuple<mt_idx_t>, mt_idx_t> & edge_map)
{
  struct mt_tuple<mt_idx_t> tupbuff;

  mt_idx_t v1 = mesh_con[0];
  mt_idx_t v2 = mesh_con[1];
  mt_idx_t v3 = mesh_con[2];

  // (1,2)
  add_edge(v1, v2, tupbuff, edgeidx, ele2edge_con+0, edge_map);
  // (2,3)
  add_edge(v2, v3, tupbuff, edgeidx, ele2edge_con+1, edge_map);
  // (3,1)
  add_edge(v3, v1, tupbuff, edgeidx, ele2edge_con+2, edge_map);
}
void add_edges_tri(const mt_idx_t* mesh_con,
                   size_t & edgeidx,
                   MT_MAP<mt_tuple<mt_idx_t>, mt_idx_t> & edge_map)
{
  struct mt_tuple<mt_idx_t> tupbuff;

  mt_idx_t v1 = mesh_con[0];
  mt_idx_t v2 = mesh_con[1];
  mt_idx_t v3 = mesh_con[2];

  // (1,2)
  add_edge(v1, v2, tupbuff, edgeidx, edge_map);
  // (2,3)
  add_edge(v2, v3, tupbuff, edgeidx, edge_map);
  // (3,1)
  add_edge(v3, v1, tupbuff, edgeidx, edge_map);
}

void add_edges_quad(const mt_idx_t* mesh_con,
                    mt_idx_t* ele2edge_con,
                    size_t & edgeidx,
                    MT_MAP<mt_tuple<mt_idx_t>, mt_idx_t> & edge_map)
{
  struct mt_tuple<mt_idx_t> tupbuff;

  mt_idx_t v1 = mesh_con[0];
  mt_idx_t v2 = mesh_con[1];
  mt_idx_t v3 = mesh_con[2];
  mt_idx_t v4 = mesh_con[3];

  // (1,2)
  add_edge(v1, v2, tupbuff, edgeidx, ele2edge_con+0, edge_map);
  // (2,3)
  add_edge(v2, v3, tupbuff, edgeidx, ele2edge_con+1, edge_map);
  // (3,4)
  add_edge(v3, v4, tupbuff, edgeidx, ele2edge_con+2, edge_map);
  // (4,1)
  add_edge(v4, v1, tupbuff, edgeidx, ele2edge_con+3, edge_map);
}
void add_edges_quad(const mt_idx_t* mesh_con,
                    size_t & edgeidx,
                    MT_MAP<mt_tuple<mt_idx_t>, mt_idx_t> & edge_map)
{
  struct mt_tuple<mt_idx_t> tupbuff;

  mt_idx_t v1 = mesh_con[0];
  mt_idx_t v2 = mesh_con[1];
  mt_idx_t v3 = mesh_con[2];
  mt_idx_t v4 = mesh_con[3];

  // (1,2)
  add_edge(v1, v2, tupbuff, edgeidx, edge_map);
  // (2,3)
  add_edge(v2, v3, tupbuff, edgeidx, edge_map);
  // (3,4)
  add_edge(v3, v4, tupbuff, edgeidx, edge_map);
  // (4,1)
  add_edge(v4, v1, tupbuff, edgeidx, edge_map);
}

void add_edges_tet(const mt_idx_t* mesh_con,
                   mt_idx_t* ele2edge_con,
                   size_t & edgeidx,
                   MT_MAP<mt_tuple<mt_idx_t>, mt_idx_t> & edge_map)
{
  struct mt_tuple<mt_idx_t> tupbuff;

  mt_idx_t v1 = mesh_con[0];
  mt_idx_t v2 = mesh_con[1];
  mt_idx_t v3 = mesh_con[2];
  mt_idx_t v4 = mesh_con[3];

  // (1,2)
  add_edge(v1, v2, tupbuff, edgeidx, ele2edge_con+0, edge_map);
  // (2,3)
  add_edge(v2, v3, tupbuff, edgeidx, ele2edge_con+1, edge_map);
  // (3,1)
  add_edge(v3, v1, tupbuff, edgeidx, ele2edge_con+2, edge_map);
  // (1,4)
  add_edge(v1, v4, tupbuff, edgeidx, ele2edge_con+3, edge_map);
  // (2,4)
  add_edge(v2, v4, tupbuff, edgeidx, ele2edge_con+4, edge_map);
  // (3,4)
  add_edge(v3, v4, tupbuff, edgeidx, ele2edge_con+5, edge_map);
}
void add_edges_tet(const mt_idx_t* mesh_con,
                   size_t & edgeidx,
                   MT_MAP<mt_tuple<mt_idx_t>, mt_idx_t> & edge_map)
{
  struct mt_tuple<mt_idx_t> tupbuff;

  mt_idx_t v1 = mesh_con[0];
  mt_idx_t v2 = mesh_con[1];
  mt_idx_t v3 = mesh_con[2];
  mt_idx_t v4 = mesh_con[3];

  // (1,2)
  add_edge(v1, v2, tupbuff, edgeidx, edge_map);
  // (2,3)
  add_edge(v2, v3, tupbuff, edgeidx, edge_map);
  // (3,1)
  add_edge(v3, v1, tupbuff, edgeidx, edge_map);
  // (1,4)
  add_edge(v1, v4, tupbuff, edgeidx, edge_map);
  // (2,4)
  add_edge(v2, v4, tupbuff, edgeidx, edge_map);
  // (3,4)
  add_edge(v3, v4, tupbuff, edgeidx, edge_map);
}
static void add_edges_pyr(const mt_idx_t* mesh_con,
                   mt_idx_t* ele2edge_con,
                   size_t & edgeidx,
                   MT_MAP<mt_tuple<mt_idx_t>, mt_idx_t> & edge_map)
{
  struct mt_tuple<mt_idx_t> tupbuff;

  mt_idx_t v1 = mesh_con[0];
  mt_idx_t v2 = mesh_con[1];
  mt_idx_t v3 = mesh_con[2];
  mt_idx_t v4 = mesh_con[3];
  mt_idx_t v5 = mesh_con[4];

  // (1,2)
  add_edge(v1, v2, tupbuff, edgeidx, ele2edge_con+0, edge_map);
  // (2,3)
  add_edge(v2, v3, tupbuff, edgeidx, ele2edge_con+1, edge_map);
  // (3,4)
  add_edge(v3, v4, tupbuff, edgeidx, ele2edge_con+2, edge_map);
  // (4,1)
  add_edge(v4, v1, tupbuff, edgeidx, ele2edge_con+3, edge_map);

  // (1,5)
  add_edge(v1, v5, tupbuff, edgeidx, ele2edge_con+4, edge_map);
  // (2,5)
  add_edge(v2, v5, tupbuff, edgeidx, ele2edge_con+5, edge_map);
  // (3,5)
  add_edge(v3, v5, tupbuff, edgeidx, ele2edge_con+6, edge_map);
  // (4,5)
  add_edge(v4, v5, tupbuff, edgeidx, ele2edge_con+7, edge_map);
}
static void add_edges_pyr(const mt_idx_t* mesh_con,
                   size_t & edgeidx,
                   MT_MAP<mt_tuple<mt_idx_t>, mt_idx_t> & edge_map)
{
  struct mt_tuple<mt_idx_t> tupbuff;

  mt_idx_t v1 = mesh_con[0];
  mt_idx_t v2 = mesh_con[1];
  mt_idx_t v3 = mesh_con[2];
  mt_idx_t v4 = mesh_con[3];
  mt_idx_t v5 = mesh_con[4];

  // (1,2)
  add_edge(v1, v2, tupbuff, edgeidx, edge_map);
  // (2,3)
  add_edge(v2, v3, tupbuff, edgeidx, edge_map);
  // (3,4)
  add_edge(v3, v4, tupbuff, edgeidx, edge_map);
  // (4,1)
  add_edge(v4, v1, tupbuff, edgeidx, edge_map);

  // (1,5)
  add_edge(v1, v5, tupbuff, edgeidx, edge_map);
  // (2,5)
  add_edge(v2, v5, tupbuff, edgeidx, edge_map);
  // (3,5)
  add_edge(v3, v5, tupbuff, edgeidx, edge_map);
  // (4,5)
  add_edge(v4, v5, tupbuff, edgeidx, edge_map);
}
static void add_edges_prism(const mt_idx_t* mesh_con,
                   mt_idx_t* ele2edge_con,
                   size_t & edgeidx,
                   MT_MAP<mt_tuple<mt_idx_t>, mt_idx_t> & edge_map)
{
  struct mt_tuple<mt_idx_t> tupbuff;

  mt_idx_t v1 = mesh_con[0];
  mt_idx_t v2 = mesh_con[1];
  mt_idx_t v3 = mesh_con[2];
  mt_idx_t v4 = mesh_con[3];
  mt_idx_t v5 = mesh_con[4];
  mt_idx_t v6 = mesh_con[5];

  // (1,2)
  add_edge(v1, v2, tupbuff, edgeidx, ele2edge_con+0, edge_map);
  // (2,3)
  add_edge(v2, v3, tupbuff, edgeidx, ele2edge_con+1, edge_map);
  // (3,1)
  add_edge(v3, v1, tupbuff, edgeidx, ele2edge_con+2, edge_map);

  // (4,5)
  add_edge(v4, v5, tupbuff, edgeidx, ele2edge_con+3, edge_map);
  // (5,6)
  add_edge(v5, v6, tupbuff, edgeidx, ele2edge_con+4, edge_map);
  // (6,4)
  add_edge(v6, v4, tupbuff, edgeidx, ele2edge_con+5, edge_map);

  // (1,4)
  add_edge(v1, v4, tupbuff, edgeidx, ele2edge_con+6, edge_map);
  // (2,6)
  add_edge(v2, v6, tupbuff, edgeidx, ele2edge_con+7, edge_map);
  // (3,5)
  add_edge(v3, v5, tupbuff, edgeidx, ele2edge_con+8, edge_map);
}
static void add_edges_prism(const mt_idx_t* mesh_con,
                   size_t & edgeidx,
                   MT_MAP<mt_tuple<mt_idx_t>, mt_idx_t> & edge_map)
{
  struct mt_tuple<mt_idx_t> tupbuff;

  mt_idx_t v1 = mesh_con[0];
  mt_idx_t v2 = mesh_con[1];
  mt_idx_t v3 = mesh_con[2];
  mt_idx_t v4 = mesh_con[3];
  mt_idx_t v5 = mesh_con[4];
  mt_idx_t v6 = mesh_con[5];

  // (1,2)
  add_edge(v1, v2, tupbuff, edgeidx, edge_map);
  // (2,3)
  add_edge(v2, v3, tupbuff, edgeidx, edge_map);
  // (3,1)
  add_edge(v3, v1, tupbuff, edgeidx, edge_map);

  // (4,5)
  add_edge(v4, v5, tupbuff, edgeidx, edge_map);
  // (5,6)
  add_edge(v5, v6, tupbuff, edgeidx, edge_map);
  // (6,4)
  add_edge(v6, v4, tupbuff, edgeidx, edge_map);

  // (1,4)
  add_edge(v1, v4, tupbuff, edgeidx, edge_map);
  // (2,6)
  add_edge(v2, v6, tupbuff, edgeidx, edge_map);
  // (3,5)
  add_edge(v3, v5, tupbuff, edgeidx, edge_map);
}
static void add_edges_hex(const mt_idx_t* mesh_con,
                   mt_idx_t* ele2edge_con,
                   size_t & edgeidx,
                   MT_MAP<mt_tuple<mt_idx_t>, mt_idx_t> & edge_map)
{
  struct mt_tuple<mt_idx_t> tupbuff;

  mt_idx_t v1 = mesh_con[0];
  mt_idx_t v2 = mesh_con[1];
  mt_idx_t v3 = mesh_con[2];
  mt_idx_t v4 = mesh_con[3];
  mt_idx_t v5 = mesh_con[4];
  mt_idx_t v6 = mesh_con[5];
  mt_idx_t v7 = mesh_con[6];
  mt_idx_t v8 = mesh_con[7];

  // (1,2)
  add_edge(v1, v2, tupbuff, edgeidx, ele2edge_con+0, edge_map);
  // (2,3)
  add_edge(v2, v3, tupbuff, edgeidx, ele2edge_con+1, edge_map);
  // (3,4)
  add_edge(v3, v4, tupbuff, edgeidx, ele2edge_con+2, edge_map);
  // (4,1)
  add_edge(v4, v1, tupbuff, edgeidx, ele2edge_con+3, edge_map);

  // (5,6)
  add_edge(v5, v6, tupbuff, edgeidx, ele2edge_con+4, edge_map);
  // (6,7)
  add_edge(v6, v7, tupbuff, edgeidx, ele2edge_con+5, edge_map);
  // (7,8)
  add_edge(v7, v8, tupbuff, edgeidx, ele2edge_con+6, edge_map);
  // (8,5)
  add_edge(v8, v5, tupbuff, edgeidx, ele2edge_con+7, edge_map);

  // (1,5)
  add_edge(v1, v5, tupbuff, edgeidx, ele2edge_con+8, edge_map);
  // (2,8)
  add_edge(v2, v8, tupbuff, edgeidx, ele2edge_con+9, edge_map);
  // (3,7)
  add_edge(v3, v7, tupbuff, edgeidx, ele2edge_con+10, edge_map);
  // (4,6)
  add_edge(v4, v6, tupbuff, edgeidx, ele2edge_con+11, edge_map);
}
static void add_edges_hex(const mt_idx_t* mesh_con,
                   size_t & edgeidx,
                   MT_MAP<mt_tuple<mt_idx_t>, mt_idx_t> & edge_map)
{
  struct mt_tuple<mt_idx_t> tupbuff;

  mt_idx_t v1 = mesh_con[0];
  mt_idx_t v2 = mesh_con[1];
  mt_idx_t v3 = mesh_con[2];
  mt_idx_t v4 = mesh_con[3];
  mt_idx_t v5 = mesh_con[4];
  mt_idx_t v6 = mesh_con[5];
  mt_idx_t v7 = mesh_con[6];
  mt_idx_t v8 = mesh_con[7];

  // (1,2)
  add_edge(v1, v2, tupbuff, edgeidx, edge_map);
  // (2,3)
  add_edge(v2, v3, tupbuff, edgeidx, edge_map);
  // (3,4)
  add_edge(v3, v4, tupbuff, edgeidx, edge_map);
  // (4,1)
  add_edge(v4, v1, tupbuff, edgeidx, edge_map);

  // (5,6)
  add_edge(v5, v6, tupbuff, edgeidx, edge_map);
  // (6,7)
  add_edge(v6, v7, tupbuff, edgeidx, edge_map);
  // (7,8)
  add_edge(v7, v8, tupbuff, edgeidx, edge_map);
  // (8,5)
  add_edge(v8, v5, tupbuff, edgeidx, edge_map);

  // (1,5)
  add_edge(v1, v5, tupbuff, edgeidx, edge_map);
  // (2,8)
  add_edge(v2, v8, tupbuff, edgeidx, edge_map);
  // (3,7)
  add_edge(v3, v7, tupbuff, edgeidx, edge_map);
  // (4,6)
  add_edge(v4, v6, tupbuff, edgeidx, edge_map);
}

void get_edges_line(const mt_idx_t* mesh_con,
                    const MT_MAP<mt_tuple<mt_idx_t>, mt_idx_t> & edge_map,
                    mt_idx_t* edge_idx, mt_tuple<mt_idx_t>* edge)
{
  mt_idx_t v1 = mesh_con[0];
  mt_idx_t v2 = mesh_con[1];

  // (1,2)
  edge_idx[0] = get_edge(v1, v2, edge[0], edge_map);
}

void get_edges_line(const mt_idx_t* mesh_con,
                    mt_tuple<mt_idx_t>* edge)
{
  mt_idx_t v1 = mesh_con[0];
  mt_idx_t v2 = mesh_con[1];

  sortTuple(v1, v2, edge[0].v1, edge[0].v2);
}

void get_edges_tri(const mt_idx_t* mesh_con,
                   const MT_MAP<mt_tuple<mt_idx_t>, mt_idx_t> & edge_map,
                   mt_idx_t* edge_idx, mt_tuple<mt_idx_t>* edge)
{
  mt_idx_t v1 = mesh_con[0];
  mt_idx_t v2 = mesh_con[1];
  mt_idx_t v3 = mesh_con[2];

  // (1,2)
  edge_idx[0] = get_edge(v1, v2, edge[0], edge_map);
  // (2,3)
  edge_idx[1] = get_edge(v2, v3, edge[1], edge_map);
  // (3,1)
  edge_idx[2] = get_edge(v3, v1, edge[2], edge_map);
}

void get_edges_tri(const mt_idx_t* mesh_con,
                   mt_tuple<mt_idx_t>* edge)
{
  mt_idx_t v1 = mesh_con[0];
  mt_idx_t v2 = mesh_con[1];
  mt_idx_t v3 = mesh_con[2];

  // (1,2)
  sortTuple(v1, v2, edge[0].v1, edge[0].v2);
  // (2,3)
  sortTuple(v2, v3, edge[1].v1, edge[1].v2);
  // (3,1)
  sortTuple(v3, v1, edge[2].v1, edge[2].v2);
}

void get_edges_quad(const mt_idx_t* mesh_con,
                    const MT_MAP<mt_tuple<mt_idx_t>, mt_idx_t> & edge_map,
                    mt_idx_t* edge_idx, mt_tuple<mt_idx_t>* edge)
{
  mt_idx_t v1 = mesh_con[0];
  mt_idx_t v2 = mesh_con[1];
  mt_idx_t v3 = mesh_con[2];
  mt_idx_t v4 = mesh_con[3];

  // (1,2)
  edge_idx[0] = get_edge(v1, v2, edge[0], edge_map);
  // (2,3)
  edge_idx[1] = get_edge(v2, v3, edge[1], edge_map);
  // (3,4)
  edge_idx[2] = get_edge(v3, v4, edge[2], edge_map);
  // (4,1)
  edge_idx[3] = get_edge(v4, v1, edge[3], edge_map);
}

void get_edges_quad(const mt_idx_t* mesh_con,
                   mt_tuple<mt_idx_t>* edge)
{
  mt_idx_t v1 = mesh_con[0];
  mt_idx_t v2 = mesh_con[1];
  mt_idx_t v3 = mesh_con[2];
  mt_idx_t v4 = mesh_con[3];

  // (1,2)
  sortTuple(v1, v2, edge[0].v1, edge[0].v2);
  // (2,3)
  sortTuple(v2, v3, edge[1].v1, edge[1].v2);
  // (3,4)
  sortTuple(v3, v4, edge[2].v1, edge[2].v2);
  // (4,1)
  sortTuple(v4, v1, edge[3].v1, edge[3].v2);
}

void get_edges_tet(const mt_idx_t* mesh_con,
                   const MT_MAP<mt_tuple<mt_idx_t>, mt_idx_t> & edge_map,
                   mt_idx_t* edge_idx, mt_tuple<mt_idx_t>* edge)
{
  mt_idx_t v1 = mesh_con[0];
  mt_idx_t v2 = mesh_con[1];
  mt_idx_t v3 = mesh_con[2];
  mt_idx_t v4 = mesh_con[3];

  // (1,2)
  edge_idx[0] = get_edge(v1, v2, edge[0], edge_map);
  // (2,3)
  edge_idx[1] = get_edge(v2, v3, edge[1], edge_map);
  // (3,1)
  edge_idx[2] = get_edge(v3, v1, edge[2], edge_map);
  // (1,4)
  edge_idx[3] = get_edge(v1, v4, edge[3], edge_map);
  // (2,4)
  edge_idx[4] = get_edge(v2, v4, edge[4], edge_map);
  // (3,4)
  edge_idx[5] = get_edge(v3, v4, edge[5], edge_map);
}

void get_edges_tet(const mt_idx_t* mesh_con, mt_tuple<mt_idx_t>* edge)
{
  mt_idx_t v1 = mesh_con[0];
  mt_idx_t v2 = mesh_con[1];
  mt_idx_t v3 = mesh_con[2];
  mt_idx_t v4 = mesh_con[3];

  // (1,2)
  sortTuple(v1, v2, edge[0].v1, edge[0].v2);
  // (2,3)
  sortTuple(v2, v3, edge[1].v1, edge[1].v2);
  // (3,1)
  sortTuple(v3, v1, edge[2].v1, edge[2].v2);
  // (1,4)
  sortTuple(v1, v4, edge[3].v1, edge[3].v2);
  // (2,4)
  sortTuple(v2, v4, edge[4].v1, edge[4].v2);
  // (3,4)
  sortTuple(v3, v4, edge[5].v1, edge[5].v2);
}

void get_edges_pyr(const mt_idx_t* mesh_con, mt_tuple<mt_idx_t>* edge)
{
  mt_idx_t v1 = mesh_con[0];
  mt_idx_t v2 = mesh_con[1];
  mt_idx_t v3 = mesh_con[2];
  mt_idx_t v4 = mesh_con[3];
  mt_idx_t v5 = mesh_con[4];

  // (1,2)
  sortTuple(v1, v2, edge[0].v1, edge[0].v2);
  // (2,3)
  sortTuple(v2, v3, edge[1].v1, edge[1].v2);
  // (3,4)
  sortTuple(v3, v4, edge[2].v1, edge[2].v2);
  // (4,1)
  sortTuple(v4, v1, edge[3].v1, edge[3].v2);

  // (1,5)
  sortTuple(v1, v5, edge[4].v1, edge[4].v2);
  // (2,5)
  sortTuple(v2, v5, edge[5].v1, edge[5].v2);
  // (3,5)
  sortTuple(v3, v5, edge[6].v1, edge[6].v2);
  // (4,5)
  sortTuple(v4, v5, edge[7].v1, edge[7].v2);
}

void get_edges_prism(const mt_idx_t* mesh_con, mt_tuple<mt_idx_t>* edge)
{
  mt_idx_t v1 = mesh_con[0];
  mt_idx_t v2 = mesh_con[1];
  mt_idx_t v3 = mesh_con[2];
  mt_idx_t v4 = mesh_con[3];
  mt_idx_t v5 = mesh_con[4];
  mt_idx_t v6 = mesh_con[5];

  // (1,2)
  sortTuple(v1, v2, edge[0].v1, edge[0].v2);
  // (2,3)
  sortTuple(v2, v3, edge[1].v1, edge[1].v2);
  // (3,1)
  sortTuple(v3, v1, edge[2].v1, edge[2].v2);

  // (4,5)
  sortTuple(v4, v5, edge[3].v1, edge[3].v2);
  // (5,6)
  sortTuple(v5, v6, edge[4].v1, edge[4].v2);
  // (6,4)
  sortTuple(v6, v4, edge[5].v1, edge[5].v2);

  // (1,4)
  sortTuple(v1, v4, edge[6].v1, edge[6].v2);
  // (2,6)
  sortTuple(v2, v6, edge[7].v1, edge[7].v2);
  // (3,5)
  sortTuple(v3, v5, edge[8].v1, edge[8].v2);
}

void get_edges_hex(const mt_idx_t* mesh_con, mt_tuple<mt_idx_t>* edge)
{
  mt_idx_t v1 = mesh_con[0];
  mt_idx_t v2 = mesh_con[1];
  mt_idx_t v3 = mesh_con[2];
  mt_idx_t v4 = mesh_con[3];
  mt_idx_t v5 = mesh_con[4];
  mt_idx_t v6 = mesh_con[5];
  mt_idx_t v7 = mesh_con[6];
  mt_idx_t v8 = mesh_con[7];

  // (1,2)
  sortTuple(v1, v2, edge[0].v1, edge[0].v2);
  // (2,3)
  sortTuple(v2, v3, edge[1].v1, edge[1].v2);
  // (3,4)
  sortTuple(v3, v4, edge[2].v1, edge[2].v2);
  // (4,1)
  sortTuple(v4, v1, edge[3].v1, edge[3].v2);

  // (5,6)
  sortTuple(v5, v6, edge[4].v1, edge[4].v2);
  // (6,7)
  sortTuple(v6, v7, edge[5].v1, edge[5].v2);
  // (7,8)
  sortTuple(v7, v8, edge[6].v1, edge[6].v2);
  // (8,5)
  sortTuple(v8, v5, edge[7].v1, edge[7].v2);

  // (1,5)
  sortTuple(v1, v5, edge[8].v1, edge[8].v2);
  // (2,8)
  sortTuple(v2, v8, edge[9].v1, edge[9].v2);
  // (3,7)
  sortTuple(v3, v7, edge[10].v1, edge[10].v2);
  // (4,6)
  sortTuple(v4, v6, edge[11].v1, edge[11].v2);
}

int get_edges(const mt_meshdata & mesh, const MT_MAP<mt_tuple<mt_idx_t>, mt_idx_t> & edge_map,
              const mt_idx_t eidx, mt_vector<mt_idx_t> & edge_indices, mt_vector<mt_tuple<mt_idx_t>> & edges)
{
  const mt_idx_t* con = mesh.e2n_con.data() + mesh.e2n_dsp[eidx];

  size_t oldsize = edges.size();
  int numedges;

  switch(mesh.etype[eidx]) {
    case Tri:
      numedges = 3;
      edge_indices.resize(oldsize + numedges);
      edges.resize(oldsize + numedges);
      get_edges_tri (con, edge_map, edge_indices.data() + oldsize, edges.data() + oldsize);
      break;

    case Quad:
      numedges = 4;
      edge_indices.resize(oldsize + numedges);
      edges.resize(oldsize + numedges);
      get_edges_quad(con, edge_map, edge_indices.data() + oldsize, edges.data() + oldsize);
      break;

    case Tetra:
      numedges = 6;
      edge_indices.resize(oldsize + numedges);
      edges.resize(oldsize + numedges);
      get_edges_tet (con, edge_map, edge_indices.data() + oldsize, edges.data() + oldsize);
      break;

    default:
      fprintf(stderr, "%s error: (#0) Unimplemented element type (%d), elemidx %ld! Aborting!\n", __func__, (int)mesh.etype[eidx], (long)eidx);
      exit(1);
  }

  return numedges;
}

int get_edges(const mt_meshdata & mesh, const mt_idx_t eidx, mt_vector<mt_tuple<mt_idx_t>> & edges)
{
  const mt_idx_t* con = mesh.e2n_con.data() + mesh.e2n_dsp[eidx];

  size_t oldsize = edges.size();
  int numedges;

  switch(mesh.etype[eidx]) {
    case Tri:
      numedges = 3;
      edges.resize(oldsize + numedges);
      get_edges_tri (con, edges.data() + oldsize);
      break;

    case Quad:
      numedges = 4;
      edges.resize(oldsize + numedges);
      get_edges_quad(con, edges.data() + oldsize);
      break;

    case Tetra:
      numedges = 6;
      edges.resize(oldsize + numedges);
      get_edges_tet (con, edges.data() + oldsize);
      break;

    case Pyramid:
      numedges = 8;
      edges.resize(oldsize + numedges);
      get_edges_pyr(con, edges.data() + oldsize);
      break;

    case Prism:
      numedges = 9;
      edges.resize(oldsize + numedges);
      get_edges_prism(con, edges.data() + oldsize);
      break;

    case Hexa:
      numedges = 12;
      edges.resize(oldsize + numedges);
      get_edges_hex(con, edges.data() + oldsize);
      break;

    case Line:
      numedges = 1;
      edges.resize(oldsize + numedges);
      get_edges_line(con, edges.data() + oldsize);
      break;

    default:
      fprintf(stderr, "%s error: (#1) Unimplemented element type (%d), elemidx %ld ! Aborting!\n", __func__, (int)mesh.etype[eidx], (long)eidx);
      exit(1);
  }

  return numedges;
}

void add_faces_tri(const mt_idx_t* mesh_con,
                   mt_idx_t* ele2face_con,
                   size_t & faceidx,
                   MT_MAP<mt_triple<mt_idx_t>, mt_idx_t> & face_map)
{
  struct mt_triple<mt_idx_t> tripbuff;

  mt_idx_t v1 = mesh_con[0];
  mt_idx_t v2 = mesh_con[1];
  mt_idx_t v3 = mesh_con[2];

  add_face(v1, v2, v3, tripbuff, faceidx, ele2face_con, face_map);
}

void add_faces_tri(const mt_idx_t* mesh_con,
                   mt_vector<mt_idx_t> & face_con)
{
  struct mt_triple<mt_idx_t> buff;

  mt_idx_t v1 = mesh_con[0];
  mt_idx_t v2 = mesh_con[1];
  mt_idx_t v3 = mesh_con[2];

  sortTriple(v1, v2, v3, buff.v1, buff.v2, buff.v3);
  face_con.push_back(buff.v1); face_con.push_back(buff.v2); face_con.push_back(buff.v3);
}

void add_faces_tet(const mt_idx_t* mesh_con,
                   mt_idx_t* ele2face_con,
                   size_t & faceidx,
                   MT_MAP<mt_triple<mt_idx_t>, mt_idx_t> & face_map)
{
  struct mt_triple<mt_idx_t> tripbuff;

  mt_idx_t v1 = mesh_con[0];
  mt_idx_t v2 = mesh_con[1];
  mt_idx_t v3 = mesh_con[2];
  mt_idx_t v4 = mesh_con[3];

  // (1,2,3)
  add_face(v1, v2, v3, tripbuff, faceidx, ele2face_con+0, face_map);
  // (2,3,4)
  add_face(v2, v3, v4, tripbuff, faceidx, ele2face_con+1, face_map);
  // (1,3,4)
  add_face(v1, v3, v4, tripbuff, faceidx, ele2face_con+2, face_map);
  // (1,2,4)
  add_face(v1, v2, v4, tripbuff, faceidx, ele2face_con+3, face_map);
}

void add_faces_tet(const mt_idx_t* mesh_con,
                   mt_vector<mt_idx_t> & face_con)
{
  struct mt_triple<mt_idx_t> buff;

  mt_idx_t v1 = mesh_con[0];
  mt_idx_t v2 = mesh_con[1];
  mt_idx_t v3 = mesh_con[2];
  mt_idx_t v4 = mesh_con[3];

  // (1,2,3)
  sortTriple(v1, v2, v3, buff.v1, buff.v2, buff.v3);
  face_con.push_back(buff.v1); face_con.push_back(buff.v2); face_con.push_back(buff.v3);
  // (2,3,4)
  sortTriple(v2, v3, v4, buff.v1, buff.v2, buff.v3);
  face_con.push_back(buff.v1); face_con.push_back(buff.v2); face_con.push_back(buff.v3);
  // (1,3,4)
  sortTriple(v1, v3, v4, buff.v1, buff.v2, buff.v3);
  face_con.push_back(buff.v1); face_con.push_back(buff.v2); face_con.push_back(buff.v3);
  // (1,2,4)
  sortTriple(v1, v2, v4, buff.v1, buff.v2, buff.v3);
  face_con.push_back(buff.v1); face_con.push_back(buff.v2); face_con.push_back(buff.v3);
}

void compute_edges(const mt_meshdata & mesh,
                   mt_mapping<mt_idx_t> & ele2edge,
                   MT_MAP<mt_tuple<mt_idx_t>, mt_idx_t> & edge_map)
{
  assert(mesh.etype.size() == mesh.e2n_cnt.size());

  ele2edge.fwd_cnt.assign(mesh.e2n_cnt.size(), mt_cnt_t(0));
  ele2edge.fwd_dsp.assign(mesh.e2n_cnt.size());
  size_t numedges = 0;

  for(size_t i=0; i<mesh.e2n_cnt.size(); i++)
  {
    mt_idx_t n = -1;
    switch(mesh.etype[i])
    {
      case Line:  n = 1; break;
      case Tri:   n = 3; break;
      case Quad:  n = 4; break;
      case Tetra: n = 6; break;
      default:
        std::cerr << "compute_edges: Error, element type not yet supported. Aborting!"
                  << std::endl;
        exit(1);
    }
    numedges += n;
    ele2edge.fwd_cnt[i] = n;
  }
  bucket_sort_offset(ele2edge.fwd_cnt, ele2edge.fwd_dsp);
  ele2edge.fwd_con.resize(numedges);
  size_t edgeidx=0;

  // loop over elements and construct edge_map and ele2edge map in one go
  for(size_t i=0; i<mesh.e2n_cnt.size(); i++)
  {
    const mt_idx_t* mesh_con = mesh.e2n_con.data() + mesh.e2n_dsp[i];
    mt_idx_t* ele2edge_con = ele2edge.fwd_con.data() + ele2edge.fwd_dsp[i];

    switch(mesh.etype[i])
    {
      case Line:
        add_edges_line(mesh_con, ele2edge_con, edgeidx, edge_map);
        break;

      case Tri:
        add_edges_tri(mesh_con, ele2edge_con, edgeidx, edge_map);
        break;

      case Quad:
        add_edges_quad(mesh_con, ele2edge_con, edgeidx, edge_map);
        break;

      case Tetra:
        add_edges_tet(mesh_con, ele2edge_con, edgeidx, edge_map);
        break;

      case Pyramid:
        add_edges_pyr(mesh_con, ele2edge_con, edgeidx, edge_map);
        break;

      case Prism:
        add_edges_prism(mesh_con, ele2edge_con, edgeidx, edge_map);
        break;

      case Hexa:
        add_edges_hex(mesh_con, ele2edge_con, edgeidx, edge_map);
        break;

      default:
        std::cerr << "compute_edges: Error, element type not yet supported. Aborting!"
                  << std::endl;
        return;
    }
  }
}

void compute_edges(const mt_meshdata & mesh,
                   MT_MAP<mt_tuple<mt_idx_t>, mt_idx_t> & edge_map)
{
  assert(mesh.etype.size() == mesh.e2n_cnt.size());
  size_t edgeidx=0;

  // loop over elements and construct edge_map and ele2edge map in one go
  for(size_t i=0; i<mesh.e2n_cnt.size(); i++)
  {
    const mt_idx_t* mesh_con = mesh.e2n_con.data() + mesh.e2n_dsp[i];

    switch(mesh.etype[i])
    {
      case Line:
        add_edges_line(mesh_con, edgeidx, edge_map);
        break;

      case Tri:
        add_edges_tri(mesh_con, edgeidx, edge_map);
        break;

      case Quad:
        add_edges_quad(mesh_con, edgeidx, edge_map);
        break;

      case Tetra:
        add_edges_tet(mesh_con, edgeidx, edge_map);
        break;

      case Pyramid:
        add_edges_pyr(mesh_con, edgeidx, edge_map);
        break;

      case Prism:
        add_edges_prism(mesh_con, edgeidx, edge_map);
        break;

      case Hexa:
        add_edges_hex(mesh_con, edgeidx, edge_map);
        break;

      default:
        std::cerr << "compute_edges: Error, element type not yet supported. Aborting!" << std::endl;
        return;
    }
  }
}

void compute_edge2elem(const mt_meshdata & surf,
                       MT_MAP<mt_tuple<mt_idx_t>, mt_vector<mt_idx_t>> & edge2elem_map)
{
  assert((surf.etype.size() == surf.e2n_cnt.size()) && ((surf.treg[(int)Tri]+surf.treg[(int)Quad]) == surf.etype.size()));

  mt_idx_t v0, v1, v2, v3;
  mt_tuple<mt_idx_t> edge;

  #define ADD_EDGE(I,V0,V1) {edge={(V0),(V1)};if(edge.v1>edge.v2)std::swap(edge.v1,edge.v2); \
  auto it=edge2elem_map.find(edge);if(it!=edge2elem_map.end())it->second.push_back(static_cast<mt_idx_t>(I)); \
  else edge2elem_map[edge]=mt_vector<mt_idx_t>({static_cast<mt_idx_t>(I)});}

  // loop over elements and construct edge_map and ele2edge map in one go
  for(size_t i=0; i<surf.e2n_cnt.size(); i++)
  {
    const mt_idx_t* mesh_con = surf.e2n_con.data() + surf.e2n_dsp[i];

    switch(surf.etype[i])
    {
      case Tri: {
        v0 = mesh_con[0];
        v1 = mesh_con[1];
        v2 = mesh_con[2];
        ADD_EDGE(i,v0,v1);
        ADD_EDGE(i,v1,v2);
        ADD_EDGE(i,v2,v0);
      } break;

      case Quad: {
        v0 = mesh_con[0];
        v1 = mesh_con[1];
        v2 = mesh_con[2];
        v3 = mesh_con[3];
        ADD_EDGE(i,v0,v1);
        ADD_EDGE(i,v1,v2);
        ADD_EDGE(i,v2,v3);
        ADD_EDGE(i,v3,v0);
      } break;

      default:
        std::cerr << "compute_edges: Error, element type not yet supported. Aborting!" << std::endl;
        return;
    }
  }

  #undef ADD_EDGE
}

void compute_faces(const mt_meshdata & mesh,
                   mt_mapping<mt_idx_t> & ele2face,
                   MT_MAP<mt_triple<mt_idx_t>, mt_idx_t> & face_map)
{
  assert(mesh.etype.size() == mesh.e2n_cnt.size());

  ele2face.fwd_cnt.assign(mesh.e2n_cnt.size(), mt_cnt_t(0));
  ele2face.fwd_dsp.assign(mesh.e2n_cnt.size());
  size_t numfaces = 0;

  for(size_t i=0; i<mesh.e2n_cnt.size(); i++)
  {
    mt_idx_t n = -1;
    switch(mesh.etype[i])
    {
      case Tri:   n = 1; break;
      case Tetra: n = 4; break;
      default:
        std::cerr << "compute_faces: Error, element type not yet supported. Aborting!"
                  << std::endl;
        exit(1);
    }
    numfaces += n;
    ele2face.fwd_cnt[i] = n;
  }
  bucket_sort_offset(ele2face.fwd_cnt, ele2face.fwd_dsp);
  ele2face.fwd_con.resize(numfaces);
  size_t faceidx=0;

  // loop over elements and construct face_map and ele2face map in one go
  for(size_t i=0; i<mesh.e2n_cnt.size(); i++)
  {
    const mt_idx_t* mesh_con = mesh.e2n_con.data() + mesh.e2n_dsp[i];
    mt_idx_t* ele2face_con = ele2face.fwd_con.data() + ele2face.fwd_dsp[i];

    switch(mesh.etype[i])
    {
      case Tri:
        add_faces_tri(mesh_con, ele2face_con, faceidx, face_map);
        break;

      case Tetra:
        add_faces_tet(mesh_con, ele2face_con, faceidx, face_map);
        break;

      default:
        std::cerr << "compute_faces: Error, element type not yet supported. Aborting!"
                  << std::endl;
        exit(1);
    }
  }
}

void get_faces(const mt_meshdata & mesh,
               const size_t eidx,
               mt_vector<mt_idx_t> & con)
{
  switch(mesh.etype[eidx]) {
    case Tri:
      add_faces_tri(mesh.e2n_con.data() + mesh.e2n_dsp[eidx], con);
      break;

    case Tetra:
      add_faces_tet(mesh.e2n_con.data() + mesh.e2n_dsp[eidx], con);
      break;

    default:
        std::cerr << "compute_faces: Error, element type not yet supported. Aborting!" << std::endl;
        exit(1);
  }
}

void edgemesh_from_edgemap(const MT_MAP<mt_tuple<mt_idx_t>, mt_idx_t> & edgemap,
                           mt_meshdata & edgemesh)
{
  edgemesh.etype.assign(edgemap.size(), Line);
  edgemesh.e2n_cnt.assign(edgemap.size(), 2);
  edgemesh.e2n_con.resize(edgemap.size()*2);

  size_t edge_idx=0;
  for(auto it = edgemap.begin(); it != edgemap.end(); ++it)
  {
    edgemesh.e2n_con[edge_idx*2+0] = it->first.v1;
    edgemesh.e2n_con[edge_idx*2+1] = it->first.v2;
    edge_idx++;
  }
}

void compute_line_interfaces(const mt_meshdata & mesh,
                             const mt_meshdata & surfmesh,
                             const mt_real edge_ang,
                             const bool edges_on_geom_surf,
                             mt_meshdata & linemesh)
{
  MT_MAP<mt_tuple<mt_idx_t>, mt_idx_t> sel_edge_map; // edges we are interested in

  // add sharp edges to selection
  if(edge_ang > 0) {
    MT_USET<mt_idx_t> sharp_vtx;
    MT_MAP<mt_tuple<mt_idx_t>, mt_idx_t> edge_map;

    if(edges_on_geom_surf) {
      // compute surface of whole geometry
      mt_meshdata geo_surf;
      compute_surface(mesh, geo_surf, true);
      compute_full_mesh_connectivity(geo_surf);

      // identify sharp edges on geomery surface
      mt_vector<mt_idx_t> vtx = geo_surf.e2n_con;
      binary_sort(vtx); unique_resize(vtx);
      identify_sharp_edge_nodes(geo_surf, mesh.xyz, vtx, sharp_vtx, edge_ang);

      mt_mapping<mt_idx_t> ele2edge;
      compute_edges(geo_surf, ele2edge, edge_map);
    }
    else {
      mt_vector<mt_idx_t> vtx = surfmesh.e2n_con;
      binary_sort(vtx); unique_resize(vtx);
      identify_sharp_edge_nodes(surfmesh, mesh.xyz, vtx, sharp_vtx, edge_ang);

      mt_mapping<mt_idx_t> ele2edge;
      compute_edges(surfmesh, ele2edge, edge_map);
    }

    for(auto it = edge_map.begin(); it != edge_map.end(); ++it)
      if(sharp_vtx.count(it->first.v1) > 0 && sharp_vtx.count(it->first.v2) > 0)
        sel_edge_map.insert(*it);
  }

  // add surface interfaces
  {
    // compute edges of surface
    mt_mapping<mt_idx_t> ele2edge;
    MT_MAP<mt_tuple<mt_idx_t>, mt_idx_t> edge_map;     // all edges of the surface mesh

    compute_edges(surfmesh, ele2edge, edge_map);
    ele2edge.transpose();

    // add interfaces between surfaces to selection
    for(auto it = edge_map.begin(); it != edge_map.end(); ++it){
      mt_idx_t edge_idx = it->second;
      if(ele2edge.bwd_cnt[edge_idx] != 2)
        sel_edge_map.insert(*it);
    }
  }
  edgemesh_from_edgemap(sel_edge_map, linemesh);

  #if 0
  linemesh.xyz = mesh.xyz;
  linemesh.etags.assign(linemesh.e2n_cnt.size(), 1);
  linemesh.etype.assign(linemesh.e2n_cnt.size(), Line);
  write_mesh_selected(linemesh, "vtk", "debug_linemesh");
  #endif
}

void compute_line_interfaces(const mt_meshdata & surfmesh,
                             const mt_vector<mt_real> & xyz,
                             const mt_vector<MT_USET<mt_tag_t>> & stags,
                             const mt_real edge_ang,
                             mt_meshdata & linemesh)
{
  MT_MAP<mt_tuple<mt_idx_t>, mt_idx_t> sel_edge_map; // edges we are interested in

  MT_MAP<mt_tuple<mt_idx_t>, mt_idx_t> edge_map;
  mt_mapping<mt_idx_t> ele2edge;
  compute_edges(surfmesh, ele2edge, edge_map);
  ele2edge.transpose(); ele2edge.setup_dsp();

  // add sharp edges to selection
  if(edge_ang > 0) {
    MT_USET<mt_idx_t> sharp_vtx;

    mt_vector<mt_idx_t> vtx = surfmesh.e2n_con;
    binary_sort(vtx); unique_resize(vtx);
    identify_sharp_edge_nodes(surfmesh, xyz, vtx, sharp_vtx, edge_ang);

    for(auto it = edge_map.begin(); it != edge_map.end(); ++it)
      if(sharp_vtx.count(it->first.v1) > 0 && sharp_vtx.count(it->first.v2) > 0)
        sel_edge_map.insert(*it);
  }

  auto get_stags_idx = [&stags](mt_tag_t tag)
  {
    mt_idx_t ret = -1;

    for(mt_idx_t i=0; i < mt_idx_t(stags.size()); i++)
      if(stags[i].count(tag)) {
        ret = i;
        break;
      }

    return ret;
  };

  // add interfaces between surfaces to selection
  for(auto it = edge_map.begin(); it != edge_map.end(); ++it){
    mt_idx_t edge_idx = it->second;

    if(ele2edge.bwd_cnt[edge_idx] > 2)
      sel_edge_map.insert(*it);
    else {
      mt_idx_t start = ele2edge.bwd_dsp[edge_idx], stop = start + ele2edge.bwd_cnt[edge_idx];
      mt_idx_t elem_idx = ele2edge.bwd_con[start];
      mt_idx_t stag_idx = get_stags_idx(surfmesh.etags[elem_idx]);

      for(mt_idx_t i = start + 1; i < stop; i++) {
        elem_idx = ele2edge.bwd_con[i];
        if(stag_idx != get_stags_idx(surfmesh.etags[elem_idx])) {
          sel_edge_map.insert(*it);
          break;
        }
      }
    }
  }

  edgemesh_from_edgemap(sel_edge_map, linemesh);

  #if 0
  linemesh.xyz = mesh.xyz;
  linemesh.etags.assign(linemesh.e2n_cnt.size(), 1);
  linemesh.etype.assign(linemesh.e2n_cnt.size(), Line);
  write_mesh_selected(linemesh, "vtk", "debug_linemesh");
  #endif
}

void linear_search_vtx(const mt_vector<mt_real> & xyz,
                       const mt_point<mt_real> & ref_pt,
                       mt_idx_t & idx, mt_real & dist)
{
  size_t nnodes = xyz.size() / 3;

  mt_point<mt_real> e;
  mt_real      mindist = INFINITY;
  size_t minidx  = 0;

  for(size_t i=0; i < nnodes; i++)
  {
    e = ref_pt - mt_point<mt_real>(xyz.data() + i*3);
    mt_real d = e.length2();

    if(mindist > d) {
      mindist = d;
      minidx = i;
    }
  }

  idx  = minidx;
  dist = mindist;
}

void linear_search_vtx(const mt_vector<mt_real> & xyz,
                       const MT_USET<mt_idx_t> & nod_set,
                       const mt_point<mt_real> & ref_pt,
                       mt_idx_t & idx, mt_real & dist)
{
  mt_point<mt_real> e;
  mt_real      mindist = INFINITY;
  size_t minidx  = 0;

  for(auto it = nod_set.begin(); it != nod_set.end(); ++it)
  {
    e = ref_pt - mt_point<mt_real>(xyz.data() + (*it)*3);
    mt_real d = e.length2();

    if(mindist > d) {
      mindist = d;
      minidx = *it;
    }
  }

  idx  = minidx;
  dist = mindist;
}

void neighbour_closest_to_vtx(const mt_meshdata & mesh,
                              const mt_point<mt_real> & ref_pt,
                              const mt_idx_t nidx,
                              mt_idx_t & end_idx,
                              mt_real & end_dist)
{
  // we create a set of (distance, vtx) tuples of all neighbours of the nidx vertex
  // thus, the closest vertex can be retreived trivially

  mt_idx_t nstart = mesh.n2n_dsp[nidx], nstop = nstart + mesh.n2n_cnt[nidx];
  mt_vector<mt_mixed_tuple<mt_real, mt_idx_t>> neighbours; // the neighbours sorted by distance
  neighbours.reserve(32);

  for(mt_idx_t i=nstart; i<nstop; i++) {
    mt_idx_t tidx = mesh.n2n_con[i];
    if( tidx != nidx) {
      mt_point<mt_real> e = mt_point<mt_real>(mesh.xyz.data() + tidx*3) - ref_pt;
      neighbours.push_back({e.length2(), tidx});
    }
  }

  // the closest vertex is sorted first
  if(neighbours.size() > 0) {
    auto closest = std::min_element(neighbours.begin(), neighbours.end());
    end_dist = closest->v1;
    end_idx  = closest->v2;
  }
  else {
    std::cerr << "Warning: Node " << nidx << " has no neighbours!" << std::endl;
    linear_search_vtx(mesh.xyz, ref_pt, end_idx, end_dist);
  }
}

void find_closest_vtx(const mt_meshdata & mesh,
                      const mt_point<mt_real> & ref_pt,
                      const mt_idx_t start_idx,
                      mt_idx_t & end_idx,
                      mt_real & end_dist)
{
  mt_idx_t last_idx  = start_idx, curr_idx = start_idx;
  mt_real last_dist = INFINITY, curr_dist = INFINITY;

  // we iterate over the neighbourhoods of vertices until we have the one with the
  // closest distance.
  do {
    last_dist = curr_dist;
    last_idx  = curr_idx;
    neighbour_closest_to_vtx(mesh, ref_pt, last_idx, curr_idx, curr_dist);
  } while (curr_dist < last_dist);

  end_idx  = last_idx;
  end_dist = last_dist;
}


void compute_correspondance(const mt_vector<mt_real> & xyz1,
                            const mt_vector<mt_real> & xyz2,
                            mt_vector<mt_idx_t> & corr,
                            mt_vector<mt_real> & corr_dist)
{
  size_t nnodes = xyz1.size() / 3;
  corr     .assign(nnodes, -1);
  corr_dist.assign(nnodes, 0.0f);

  PROGRESS<size_t> progress(nnodes, "Correspondance progress: ");

  kdtree vert_tree(100);
  vert_tree.build_vertex_tree(xyz2);

  int outside = 0;

  #ifdef OPENMP
  #pragma omp parallel for schedule(guided) reduction(+:outside)
  #endif
  for(size_t i = 0; i < corr.size(); i++) {
    vec3r ref(xyz1.data() + i*3);
    int nidx = -1;
    vec3r   closest_point;
    mt_real len2;

    bool inside = vert_tree.closest_vertex(ref, nidx, closest_point, len2);
    if(!inside) outside++;

    corr[i] = nidx;
    corr_dist[i] = len2;

    #ifdef OPENMP
    #pragma omp critical
    #endif
    {progress.next();}
  }

  progress.finish();

  if(outside) {
    fprintf(stderr, "%s warning: %d of %d queried vertices were not inside bbox of the used kdtree!\n",
            __func__, outside, (int)corr.size());
  }

}

void traverse_nodal_connectivity(const mt_vector<mt_cnt_t> & n2n_cnt,
                           const mt_vector<mt_idx_t> & n2n_dsp,
                           const mt_vector<mt_idx_t> & n2n_con,
                           const MT_USET<mt_idx_t> & blocked,
                           MT_USET<mt_idx_t> & reached)
{
  MT_USET<mt_idx_t> cur_reached, next_reached;
  cur_reached.insert(reached.begin(), reached.end());

  size_t nreached = cur_reached.size(), last_nreached = 0;

  while(last_nreached < nreached)
  {
    last_nreached = nreached;

    for(mt_idx_t & nidx : cur_reached)
    {
      if(blocked.count(nidx) == 0)
      {
        mt_idx_t start = n2n_dsp[nidx], stop = start + n2n_cnt[nidx];
        for(mt_idx_t j = start; j<stop; j++)
        {
          mt_idx_t cidx = n2n_con[j];
          if(!reached.count(cidx)) {
            reached.insert(cidx);
            next_reached.insert(cidx);
            nreached++;
          }
        }
      }
    }

    cur_reached.clear();
    cur_reached.insert(next_reached.begin(), next_reached.end());
    next_reached.clear();
  }
}

void nodal_selection_boundary(const mt_meshdata & mesh,
                              const MT_USET<mt_idx_t> & sel,
                              MT_USET<mt_idx_t> & bdry)
{
  mt_idx_t max_n = mesh.n2n_dsp.size();

  for(mt_idx_t n : sel) {
    if(n >= 0 && n < max_n) {
      mt_idx_t start = mesh.n2n_dsp[n], stop = start + mesh.n2n_cnt[n];
      for(mt_idx_t j=start; j<stop; j++) {
        mt_idx_t c = mesh.n2n_con[j];
        if(sel.count(c) == 0) {
          bdry.insert(n);
          break;
        }
      }
    }
  }
}

void nodal_selection_boundary(const mt_meshdata & mesh,
                              const MT_USET<mt_idx_t> & sel,
                              mt_vector<mt_idx_t> & bdry)
{
  bdry.resize(0); bdry.reserve(256);
  mt_idx_t max_n = mesh.n2n_dsp.size();

  for(mt_idx_t n : sel) {
    if(n >= 0 && n < max_n) {
      mt_idx_t start = mesh.n2n_dsp[n], stop = start + mesh.n2n_cnt[n];
      for(mt_idx_t j=start; j<stop; j++) {
        mt_idx_t c = mesh.n2n_con[j];
        if(sel.count(c) == 0) {
          bdry.push_back(n);
          break;
        }
      }
    }
  }
}

void traverse_surfelem_connectivity(const mt_meshdata & surfmesh,
                                    const mt_mapping<mt_idx_t> & ele2edge,
                                    const MT_USET<mt_idx_t> & block_edges,
                                    mt_vector<bool> & reached)
{
  check_same_size(surfmesh.e2n_cnt, reached, __func__);
  size_t nelem = surfmesh.e2n_cnt.size();
  MT_USET<mt_idx_t> cur_reached, next_reached;

  for(size_t i=0; i < nelem; i++)
    if(reached[i]) cur_reached.insert(i);

  size_t nreached = cur_reached.size(), last_nreached = 0;

  while(last_nreached < nreached)
  {
    last_nreached = nreached;

    // sweep over connectivity and try to reach new nodes
    for(mt_idx_t eidx : cur_reached)
    {
      // we look at edges of reached element
      mt_idx_t start = ele2edge.fwd_dsp[eidx], stop = start + ele2edge.fwd_cnt[eidx];
      for(mt_idx_t j = start; j < stop; j++) {
        mt_idx_t edge_idx = ele2edge.fwd_con[j];

        // check if current edge is sharp
        if(block_edges.count(edge_idx) == 0) {
          // if edge is not sharp, we iterate over connected elements and mark them as reached
          mt_idx_t estart = ele2edge.bwd_dsp[edge_idx],
                 estop  = estart + ele2edge.bwd_cnt[edge_idx];

          for(mt_idx_t k = estart; k < estop; k++) {
            mt_idx_t reached_eidx = ele2edge.bwd_con[k];
            if(mt_idx_t(eidx) != reached_eidx && reached[reached_eidx] == false) {
              reached[reached_eidx] = true;
              next_reached.insert(reached_eidx);
              nreached++;
            }
          }
        }
      }
    }

    cur_reached.clear();
    cur_reached.insert(next_reached.begin(), next_reached.end());
    next_reached.clear();
  }
}

void traverse_surfelem_connectivity(const mt_meshdata & surfmesh,
                                    const mt_mapping<mt_idx_t> & ele2edge,
                                    const mt_vector<mt_real> & elem_normals,
                                    const vec3r ref_nrml,
                                    mt_real thr,
                                    mt_vector<bool> & reached)
{
  check_same_size(surfmesh.e2n_cnt, reached, __func__);
  size_t nelem = surfmesh.e2n_cnt.size();

  // thr is assumed in degrees, we convert it back to a length
  thr = cos(thr * (MT_PI/ 180.0));

  MT_USET<mt_idx_t> cur_reached, next_reached;

  for(size_t i=0; i < nelem; i++)
    if(reached[i]) cur_reached.insert(i);

  size_t nreached = cur_reached.size(), last_nreached = 0;

  while(last_nreached < nreached)
  {
    last_nreached = nreached;

    // sweep over connectivity and try to reach new nodes
    for(mt_idx_t eidx : cur_reached)
    {
      // we look at edges of reached element
      mt_idx_t start = ele2edge.fwd_dsp[eidx], stop = start + ele2edge.fwd_cnt[eidx];
      for(mt_idx_t j = start; j < stop; j++) {
        mt_idx_t edge_idx = ele2edge.fwd_con[j];

        // we iterate over connected elements and check angle
        mt_idx_t estart = ele2edge.bwd_dsp[edge_idx],
        estop  = estart + ele2edge.bwd_cnt[edge_idx];

        for(mt_idx_t k = estart; k < estop; k++) {
          mt_idx_t reached_eidx = ele2edge.bwd_con[k];
          if(mt_idx_t(eidx) != reached_eidx && reached[reached_eidx] == false)
          {
            vec3r n(elem_normals.data() + reached_eidx*3);
            if(n.scaProd(ref_nrml) > thr) {
              reached[reached_eidx] = true;
              next_reached.insert(reached_eidx);
              nreached++;
            }
          }
        }
      }
    }

    cur_reached.clear();
    cur_reached.insert(next_reached.begin(), next_reached.end());
    next_reached.clear();
  }
}

void traverse_nodal_connectivity(const mt_vector<mt_cnt_t> & n2n_cnt,
                           const mt_vector<mt_idx_t> & n2n_dsp,
                           const mt_vector<mt_idx_t> & n2n_con,
                           const mt_vector<mt_real> & xyz,
                           mt_point<mt_real> & ref,
                           const mt_real rad,
                           MT_USET<mt_idx_t> & reached)
{
  const mt_real squared_rad = rad*rad;
  MT_USET<mt_idx_t> cur_reached, next_reached;
  cur_reached.insert(reached.begin(), reached.end());

  size_t nreached = cur_reached.size(), last_nreached = 0;

  while(last_nreached < nreached)
  {
    last_nreached = nreached;

    for(mt_idx_t & nidx : cur_reached)
    {
      mt_idx_t start = n2n_dsp[nidx], stop = start + n2n_cnt[nidx];
      for(mt_idx_t j = start; j<stop; j++)
      {
        mt_idx_t cidx = n2n_con[j];
        mt_point<mt_real> e = ref - mt_point<mt_real>(xyz.data() + 3*cidx);
        if( !reached.count(cidx) && (e.length2() < squared_rad) ) {
          reached.insert(cidx);
          next_reached.insert(cidx);
          nreached++;
        }
      }
    }

    cur_reached.clear();
    cur_reached.insert(next_reached.begin(), next_reached.end());
    next_reached.clear();
  }
}


void traverse_nodal_connectivity(const mt_vector<mt_cnt_t> & n2n_cnt,
                           const mt_vector<mt_idx_t> & n2n_dsp,
                           const mt_vector<mt_idx_t> & n2n_con,
                           const mt_vector<mt_real> & xyz,
                           const std::function<bool(const vec3r&, const mt_idx_t)>& select,
                           MT_USET<mt_idx_t> & reached)
{
  MT_USET<mt_idx_t> cur_reached, next_reached;
  cur_reached.insert(reached.begin(), reached.end());

  size_t nreached = cur_reached.size(), last_nreached = 0;

  while(last_nreached < nreached)
  {
    last_nreached = nreached;

    for(mt_idx_t & nidx : cur_reached)
    {
      mt_idx_t start = n2n_dsp[nidx], stop = start + n2n_cnt[nidx];
      for(mt_idx_t j = start; j<stop; j++)
      {
        mt_idx_t cidx = n2n_con[j];
        const mt_point<mt_real> pnt(xyz.data() + 3*cidx);
        if ((!reached.count(cidx)) && (select(pnt, cidx))) {
          reached.insert(cidx);
          next_reached.insert(cidx);
          nreached++;
        }
      }
    }

    cur_reached.clear();
    cur_reached.insert(next_reached.begin(), next_reached.end());
    next_reached.clear();
  }
}

void nodal_connectivity_decomposition(const mt_meshdata & mesh,
                                      mt_vector<mt_meshdata> & parts,
                                      mt_vector<mt_vector<mt_idx_t> > & parts_eidx)
{
  check_condition(mesh.n2n_dsp.size() > 0 && mesh.e2n_dsp.size() > 0,
                  "mesh n2n connectivity set up", __func__);

  size_t nelem = mesh.e2n_cnt.size();
  size_t nnode = mesh.n2e_cnt.size();
  size_t initidx, iter = 0;
  MT_USET<mt_idx_t> reached;
  mt_vector<mt_idx_t> nodal_part(nnode, -1);
  bool finished = false;

  // the vertices in the mesh
  mt_vector<mt_idx_t> vtx(mesh.e2n_con);
  binary_sort(vtx); unique_resize(vtx);
  initidx = vtx[0];

  while(!finished) {
    reached.clear();
    mt_meshdata       & meshpart  = parts     .push_back(mt_meshdata());
    mt_vector<mt_idx_t> & extr_eidx = parts_eidx.push_back(mt_vector<mt_idx_t>());

    extr_eidx.reserve(nelem / 2);
    reached.insert(initidx);

    traverse_nodal_connectivity(mesh.n2n_cnt, mesh.n2n_dsp, mesh.n2n_con, reached);

    for(size_t eidx = 0; eidx < mesh.e2n_cnt.size(); eidx++) {
      mt_idx_t start = mesh.e2n_dsp[eidx], stop = start + mesh.e2n_cnt[eidx];
      mt_idx_t numreached = 0;

      for(mt_idx_t i = start; i < stop; i++) {
        mt_idx_t nidx = mesh.e2n_con[i];
        if(reached.count(nidx)) numreached++;
      }

      if(numreached == mesh.e2n_cnt[eidx])
        extr_eidx.push_back(eidx);
    }

    extract_mesh(extr_eidx, mesh, meshpart);

    for(auto v : vtx)
      if(reached.count(v)) nodal_part[v] = iter;

    // we finish if all nodes in the mesh have been partitioned
    finished = true;
    for(auto v : vtx) {
      if(nodal_part[v] == -1) {
        initidx = v;
        finished = false;
        break;
      }
    }

    iter++;
  }
}

void surf_connectivity_decomposition(const mt_meshdata & surfmesh,
                                     const mt_mapping<mt_idx_t> & ele2edge,
                                     const MT_USET<mt_idx_t> & block_edges,
                                     mt_vector<mt_meshdata> & parts,
                                     mt_vector<mt_vector<mt_idx_t> > & parts_eidx)
{
  size_t nelem = surfmesh.e2n_cnt.size();
  size_t initidx = 0, iter = 0;
  mt_vector<bool> reached;
  mt_vector<mt_idx_t> part(nelem, -1);
  bool finished = false;

  while(!finished) {
    reached.assign(nelem, false);
    mt_meshdata       & meshpart  = parts     .push_back(mt_meshdata());
    mt_vector<mt_idx_t> & extr_eidx = parts_eidx.push_back(mt_vector<mt_idx_t>());

    extr_eidx.reserve(nelem / 2);
    reached[initidx] = true;
    traverse_surfelem_connectivity(surfmesh, ele2edge, block_edges, reached);

    for(size_t i=0; i<nelem; i++) {
      if(reached[i]) {
        extr_eidx.push_back(i);
        part[i] = iter;
      }
    }

    extract_mesh(extr_eidx, surfmesh, meshpart);

    // we finish if all nodes in the mesh have been partitioned
    finished = true;

    for(size_t i=0; i<nelem; i++) {
      if(part[i] == -1) {
        initidx = i;
        finished = false;
        break;
      }
    }

    iter++;
  }
}

bool tet_fix_face_orientation(mt_meshdata & mesh, mt_idx_t eidx, short face_idx)
{
  mt_idx_t* nod = mesh.e2n_con.data() + mesh.e2n_dsp[eidx];
  mt_idx_t n1 = nod[0], n2 = nod[1], n3 = nod[2], n4 = nod[3];

  vec3r ele_ctr = barycenter(4, nod, mesh.xyz.data());
  vec3r c, n;

  switch(face_idx) {
    default:
      fprintf(stderr, "%s error: Tet face index not in range [1,4]!\n", __func__);
      exit(EXIT_FAILURE);

    case 1:
      // (2,3,1)
      n = triangle_normal(n2, n3, n1, mesh.xyz);
      c = triangle_centerpoint(n2, n3, n1, mesh.xyz);
      break;

    case 2:
      // (1,4,2)
      n = triangle_normal     (n1, n4, n2, mesh.xyz);
      c = triangle_centerpoint(n1, n4, n2, mesh.xyz);
      break;

    case 3:
      // (2,4,3)
      n = triangle_normal     (n2, n4, n3, mesh.xyz);
      c = triangle_centerpoint(n2, n4, n3, mesh.xyz);
      break;

    case 4:
      // (1,3,4)
      n = triangle_normal     (n1, n3, n4, mesh.xyz);
      c = triangle_centerpoint(n1, n3, n4, mesh.xyz);
      break;
  }

  vec3r v = unit_vector(ele_ctr - c);
  mt_real sca = n.scaProd(v);

  if(sca > 0) {
    switch(face_idx) {
      default:
        fprintf(stderr, "%s error: Tet face index not in range [1,4]!\n", __func__);
        exit(EXIT_FAILURE);

      case 1:
        // (2,3,1)
        nod[1] = n1,
        nod[2] = n3,
        nod[0] = n2;
        break;

      case 2:
        // (1,4,2)
        nod[0] = n2,
        nod[3] = n4,
        nod[1] = n1;
        break;

      case 3:
        // (2,4,3)
        nod[1] = n3,
        nod[3] = n4,
        nod[2] = n2;
        break;

      case 4:
        // (1,3,4)
        nod[0] = n4,
        nod[2] = n3,
        nod[3] = n1;
        break;
    }
    return true;
  }

  return false;
}

bool correct_insideOut(mt_meshdata & mesh)
{
  size_t nelem = mesh.e2n_cnt.size();

  if(mesh.e2n_dsp.size() != nelem) {
    mesh.e2n_dsp.resize(mesh.e2n_cnt.size());
    bucket_sort_offset(mesh.e2n_cnt, mesh.e2n_dsp);
  }

  bool did_corr = false;
  size_t corr = 0, corr_old = 0, corr_pass = 0, max = 5;

  do {
    corr_old = corr;
    corr_pass++;
    for(size_t i=0; i<nelem; i++) {
      if(mesh.etype[i] == Tetra) {
        #if 1
        const mt_idx_t edsp = mesh.e2n_dsp[i];
        const mt_idx_t v0 = mesh.e2n_con[edsp+0];
        const mt_idx_t v1 = mesh.e2n_con[edsp+1];
        const mt_idx_t v2 = mesh.e2n_con[edsp+2];
        const mt_idx_t v3 = mesh.e2n_con[edsp+3];

        mt_point<mt_real> p0(mesh.xyz.data() + v0*3);
        mt_point<mt_real> p1(mesh.xyz.data() + v1*3);
        mt_point<mt_real> p2(mesh.xyz.data() + v2*3);
        mt_point<mt_real> p3(mesh.xyz.data() + v3*3);

        const mt_real vol = signed_tet_volume(p0,p1,p2,p3);

        if(vol < 0) {
          mesh.e2n_con[edsp+0] = v2;
          mesh.e2n_con[edsp+1] = v1;
          mesh.e2n_con[edsp+2] = v0;
          mesh.e2n_con[edsp+3] = v3;
          corr++;
        }
        #else
        // in this branch we fix the faces individually. I have not come across a case
        // where it was better than the one-time check with signed_tet_volume(). -Aurel
        bool fix = false;
        fix |= tet_fix_face_orientation(mesh, i, 1);
        fix |= tet_fix_face_orientation(mesh, i, 2);
        fix |= tet_fix_face_orientation(mesh, i, 3);
        fix |= tet_fix_face_orientation(mesh, i, 4);

        if(fix) corr++;
        #endif
      }
    }

    if(corr) did_corr = true;

    std::cout << "Pass " << corr_pass << ": Corrected " << corr - corr_old << " inside-out elements." << std::endl;
  } while( (corr > corr_old) && (corr_pass < max) );

  return did_corr;
}

void correct_insideOut_tri(mt_meshdata & mesh, const mt_vector<mt_real> & surf_nrml)
{
  size_t nelem = mesh.e2n_cnt.size();
  size_t corr = 0, corr_old = 0, corr_pass = 0, max = 5;

  do {
    corr_old = corr;
    corr_pass++;
    for(size_t i=0; i<nelem; i++) {
      assert(mesh.etype[i] == Tri);

      const mt_idx_t v0 = mesh.e2n_con[i*3+0];
      const mt_idx_t v1 = mesh.e2n_con[i*3+1];
      const mt_idx_t v2 = mesh.e2n_con[i*3+2];

      mt_point<mt_real> p0(mesh.xyz.data() + v0*3);
      mt_point<mt_real> p1(mesh.xyz.data() + v1*3);
      mt_point<mt_real> p2(mesh.xyz.data() + v2*3);
      mt_point<mt_real> n0(surf_nrml.data() + v0*3);
      mt_point<mt_real> n1(surf_nrml.data() + v1*3);
      mt_point<mt_real> n2(surf_nrml.data() + v2*3);

      mt_point<mt_real> e01 = p1 - p0;
      mt_point<mt_real> e02 = p2 - p0;

      mt_point<mt_real> n  = e01.crossProd(e02) * mt_real(-1);
      mt_point<mt_real> na = (n0 + n1 + n2) / mt_real(3.0);
      mt_real sca = n.scaProd(na);

      if(sca < 0) {
        mesh.e2n_con[i*3+0] = v2;
        mesh.e2n_con[i*3+1] = v1;
        mesh.e2n_con[i*3+2] = v0;
        corr++;
      }
    }

    // std::cout << "Pass " << corr_pass << ": Corrected " << corr - corr_old <<
    //             " inside-out elements." << std::endl;
  } while( (corr > corr_old) && (corr_pass < max) );
}

void correct_duplicate_elements(mt_meshdata & mesh, bool verbose)
{
  MT_USET<mt_quadruple<mt_idx_t> > tet_elems;
  MT_USET<mt_triple<mt_idx_t> > tri_elems;
  mt_vector<bool> keep(mesh.e2n_cnt.size(), false);
  mt_vector<mt_idx_t> elems, qbuff(4);
  size_t dupl = 0;

  for(size_t eidx = 0; eidx < mesh.e2n_cnt.size(); eidx++)
  {
    mt_idx_t estart = mesh.e2n_dsp[eidx];
    switch(mesh.etype[eidx]) {
      case Tetra:
      {
        qbuff.assign(mesh.e2n_con.data() + estart, mesh.e2n_con.data() + estart+4);
        binary_sort(qbuff);
        mt_quadruple<mt_idx_t> q = {qbuff[0], qbuff[1], qbuff[2], qbuff[3]};
        auto it = tet_elems.find(q);
        if(it == tet_elems.end()) {
          tet_elems.insert(q);
          keep[eidx] = true;
        }
        else dupl++;
        break;
      }
      case Tri:
      {
        mt_triple<mt_idx_t> q;
        sortTriple(mesh.e2n_con[estart+0], mesh.e2n_con[estart+1], mesh.e2n_con[estart+2],
                   q.v1, q.v2, q.v3);
        auto it = tri_elems.find(q);
        if(it == tri_elems.end()) {
          tri_elems.insert(q);
          keep[eidx] = true;
        }
        else dupl++;
        break;
      }
      default: break;
    }
  }

  if(dupl) {
    if(verbose) printf("Removing %zd element duplicates..\n", dupl);
    restrict_meshdata(keep, mesh, elems);
  }
}

bool correct_duplicate_vertices(mt_meshdata & mesh, bool verbose)
{
  int scale_factor;
  MT_MAP<mt_triple<mt_idx_t>,mt_idx_t> coords;
  int did_correct = 0;
  mt_triple<mt_idx_t> t;

  if(mesh.e2n_dsp.size() == 0)
    bucket_sort_offset(mesh.e2n_cnt, mesh.e2n_dsp);

  generate_coord_map(mesh, coords, scale_factor);

  for(size_t eidx = 0, ridx = 0; eidx < mesh.e2n_cnt.size(); eidx++) {
    for(mt_idx_t i = 0; i < mesh.e2n_cnt[eidx]; i++, ridx++) {
      mt_idx_t v = mesh.e2n_con[ridx];
      t.v1 = mt_idx_t(mesh.xyz[v*3+0]*scale_factor);
      t.v2 = mt_idx_t(mesh.xyz[v*3+1]*scale_factor);
      t.v3 = mt_idx_t(mesh.xyz[v*3+2]*scale_factor);

      auto it = coords.find(t);
      if(it != coords.end()) {
        mt_idx_t v2 = it->second;
        if(v != v2) {
          if(verbose) {
            printf("Duplicate vertex: %ld (%g, %g, %g) == %ld (%g, %g, %g)\n",
                   (long) v,  mesh.xyz[v*3+0],  mesh.xyz[v*3+1],  mesh.xyz[v*3+2],
                   (long) v2, mesh.xyz[v2*3+0], mesh.xyz[v2*3+1], mesh.xyz[v2*3+2]);
          }
          mesh.e2n_con[ridx] = v2;
          did_correct++;
        }
      }
      else {
        fprintf(stderr, "%s error: coord map inconsistent with mesh. Aborting!\n", __func__);
        exit(1);
      }
    }
  }

  // if we remaped nodes, we do a re-indexing
  if(did_correct > 0) {
    if(verbose) printf("Correcting %d duplicate nodes..\n", did_correct);
    reindex_nodes(mesh, verbose);
  }

  // if we remaped nodes, we also need to check if we have now ill-defined elements
  if(did_correct > 0) {
    MT_USET<mt_idx_t> elemnodes;
    mt_vector<bool> keep(mesh.e2n_cnt.size(), true);
    int badelem = 0;

    for(size_t eidx = 0, ridx = 0; eidx < mesh.e2n_cnt.size(); eidx++) {
      elemnodes.clear();
      for(mt_idx_t i = 0; i < mesh.e2n_cnt[eidx]; i++, ridx++)
        elemnodes.insert(mesh.e2n_con[ridx]);

      if(elemnodes.size() != size_t(mesh.e2n_cnt[eidx])) {
        if(verbose) {
          printf("Ill defined element %ld: ", (long int) eidx);
          for(mt_idx_t i = mesh.e2n_dsp[eidx], j = 0; j<mesh.e2n_cnt[eidx]; i++, j++)
            printf("%ld ", (long int) mesh.e2n_con[i]);
          printf("\n");
        }
        badelem++;
        keep[eidx] = false;
      }
    }

    if(badelem) {
      mt_vector<mt_idx_t> elems;
      if(verbose) printf("Removing %d ill-defined elements\n", badelem);
      restrict_meshdata(keep, mesh, elems, verbose);
    }
  }

  return did_correct > 0;
}

bool correct_duplicate_vertices(mt_meshdata & mesh, const mt_real eps, bool verbose)
{
  mt_vector<vec3r> used_coords;
  mt_vector<mt_idx_t> nod(mesh.e2n_con);
  binary_sort(nod); unique_resize(nod);

  int did_correct = 0;
  MT_MAP<mt_idx_t, mt_idx_t> old2new;

  used_coords.resize(nod.size());

  for(size_t i=0; i<nod.size(); i++)
    used_coords[i].get(mesh.xyz.data() + nod[i]*3);

  kdtree tree(10);
  tree.build_vertex_tree(used_coords);

  for(size_t i=0; i<nod.size(); i++) {
    if(old2new.count(nod[i]) == 0) {
      vec3r ref_pt(mesh.xyz.data() + nod[i]*3);

      mt_vector<mt_mixed_tuple<mt_real,int>> close_vtx;
      tree.vertices_in_sphere(ref_pt, eps, close_vtx);

      if(close_vtx.size() > 1) {
        std::sort(close_vtx.begin(), close_vtx.end());
        mt_idx_t new_vtx_ind = nod[close_vtx[0].v2];

        for(size_t j=0; j<close_vtx.size(); j++)
          old2new[nod[close_vtx[j].v2]] = new_vtx_ind;
      }
    }
  }

  if(old2new.size()) {
    for(size_t i=0; i<mesh.e2n_con.size(); i++) {
      mt_idx_t c = mesh.e2n_con[i];
      if(old2new.count(c)) {
        mesh.e2n_con[i] = old2new[c];
        did_correct++;
      }
    }
  }

  // if we remaped nodes, we do a re-indexing
  if(did_correct > 0) {
    if(verbose) printf("Correcting %d duplicate nodes..\n", did_correct);
    reindex_nodes(mesh, verbose);
  }

  // if we remaped nodes, we also need to check if we have now ill-defined elements
  if(did_correct > 0) {
    MT_USET<mt_idx_t> elemnodes;
    mt_vector<bool> keep(mesh.e2n_cnt.size(), true);
    int badelem = 0;

    for(size_t eidx = 0, ridx = 0; eidx < mesh.e2n_cnt.size(); eidx++) {
      elemnodes.clear();
      for(mt_idx_t i = 0; i < mesh.e2n_cnt[eidx]; i++, ridx++)
        elemnodes.insert(mesh.e2n_con[ridx]);

      if(elemnodes.size() != size_t(mesh.e2n_cnt[eidx])) {
        if(verbose) {
          printf("Ill defined element %ld: ", (long int) eidx);
          for(mt_idx_t i = mesh.e2n_dsp[eidx], j = 0; j<mesh.e2n_cnt[eidx]; i++, j++)
            printf("%ld ", (long int) mesh.e2n_con[i]);
          printf("\n");
        }
        badelem++;
        keep[eidx] = false;
      }
    }

    if(badelem) {
      mt_vector<mt_idx_t> elems;
      if(verbose) printf("Removing %d ill-defined elements\n", badelem);
      restrict_meshdata(keep, mesh, elems, verbose);
    }
  }

  return did_correct > 0;
}

bool correct_dangling_tri_elems(mt_meshdata & mesh, const int num_cnnx)
{
  bool is_tri = mesh_is_trisurf(mesh);
  if(!is_tri) {
    fprintf(stderr, "%s error: mesh is not a triangle surface!\n", __func__);
    return false;
  }

  mt_vector<mt_cnt_t> e2e_cnt;
  mt_vector<mt_idx_t> e2e_con, e2e_ele;

  multiply_connectivities(mesh.e2n_cnt, mesh.e2n_con, mesh.n2e_cnt, mesh.n2e_con, e2e_cnt, e2e_con, e2e_ele);
  restrict_connectivity(e2e_cnt, e2e_con, e2e_ele, mt_idx_t(2));

  mt_vector<bool> keep(mesh.e2n_cnt.size(), true);
  size_t found = 0;

  for(size_t i=0; i < e2e_cnt.size(); i++) {
    if(e2e_cnt[i] < (num_cnnx + 1)) {
      found++;
      keep[i] = false;
    }
  }

  if(found) {
    printf("%s: removing %zd dangling triangles\n", __func__, found);
    mt_vector<mt_idx_t> eidx_out;
    restrict_meshdata(keep, mesh, eidx_out, false);
  }

  return bool(found);
}

bool correct_dangling_tri_elems(mt_meshdata & mesh, const MT_USET<mt_tuple<mt_idx_t>> & open_edges)
{
  if(!open_edges.size())
    return false;

  bool is_tri = mesh_is_trisurf(mesh);
  if(!is_tri) {
    fprintf(stderr, "%s error: mesh is not a triangle surface!\n", __func__);
    return false;
  }

  mt_vector<bool> keep(mesh.e2n_cnt.size(), true);
  MT_USET<mt_idx_t> elem_idx;

  for(const mt_tuple<mt_idx_t> & t : open_edges)
    elements_with_edge(mesh, t.v1, t.v2, elem_idx);

  for(mt_idx_t e : elem_idx) keep[e] = false;

  printf("%s: removing %zd dangling triangles\n", __func__, elem_idx.size());

  mt_vector<mt_idx_t> eidx_out;
  restrict_meshdata(keep, mesh, eidx_out, false);

  return true;
}

bool has_bad_vol(const elem_t type,
                 const mt_idx_t* con,
                 const mt_real* xyz,
                 const mt_real bad_thr)
{
  bool ret = false;

  switch(type) {
    case Tri:
    {
      const mt_idx_t v0 = con[0], v1 = con[1], v2 = con[2];

      vec3r p0(xyz + v0*3);
      vec3r p1(xyz + v1*3);
      vec3r p2(xyz + v2*3);

      vec3r base_ctr = (p0 + p1) / mt_real(2.0);
      vec3r e01 = unit_vector(p1 - p0);
      vec3r e02 = unit_vector(p2 - p0);

      vec3r n  = unit_vector(e02 - (e01 * e01.scaProd(e02)));
      vec3r c2 = unit_vector(p2 - base_ctr);
      mt_real sca = n.scaProd(c2);

      if(sca < bad_thr)
        ret = true;

      break;
    }

    case Tetra:
    {
      const mt_idx_t v0 = con[0], v1 = con[1], v2 = con[2], v3 = con[3];

      vec3r p0(xyz + v0*3);
      vec3r p1(xyz + v1*3);
      vec3r p2(xyz + v2*3);
      vec3r p3(xyz + v3*3);

      vec3r base_ctr = (p0 + p1 + p2) / mt_real(3.0);
      vec3r e01 = unit_vector(p1 - p0);
      vec3r e02 = unit_vector(p2 - p0);
      vec3r c3  = unit_vector(p3 - base_ctr);

      // we dont really check the volume, but rather if the "top point vector"
      // e03 has a significant component on the normal vector
      vec3r n = e01.crossProd(e02);
      mt_real sca = n.scaProd(c3);

      if(sca < bad_thr)
        ret = true;

      break;
    }

    default: break;
  }

  return ret;
}

void tet_get_angles(const mt_idx_t* con,
                    const mt_vector<mt_real> & xyz,
                    mt_vector<mt_real> & angles)
{
  const mt_idx_t v0 = con[0];
  const mt_idx_t v1 = con[1];
  const mt_idx_t v2 = con[2];
  const mt_idx_t v3 = con[3];

  mt_point<mt_real> p0(xyz.data()+v0*3);
  mt_point<mt_real> p1(xyz.data()+v1*3);
  mt_point<mt_real> p2(xyz.data()+v2*3);
  mt_point<mt_real> p3(xyz.data()+v3*3);

  mt_point<mt_real> e01 = p1 - p0; e01.normalize();
  mt_point<mt_real> e02 = p2 - p0; e02.normalize();
  // mt_point<mt_real> e12 = p2 - p1; e12.normalize();

  mt_point<mt_real> e03 = p3 - p0; e03.normalize();
  mt_point<mt_real> e13 = p3 - p1; e13.normalize();
  mt_point<mt_real> e23 = p3 - p2; e23.normalize();

  mt_point<mt_real> n = e01.crossProd(e02); n.normalize();

  // mt_real a = acos(e01.scaProd(e02)) * (180.0 / MT_PI),
  //   b = 180.0 - acos(e01.scaProd(e12)) * (180.0 / MT_PI),
  //   c = 180.0 - a - b;

  // angles.insert( a );
  // angles.insert( b );
  // angles.insert( c );

  mt_real a, b, c;
  a = acos(n.scaProd(e03)) * (180.0 / MT_PI);
  a = a < 90.0 ? a : 180.0 - a;
  b = acos(n.scaProd(e13)) * (180.0 / MT_PI);
  b = b < 90.0 ? b : 180.0 - b;
  c = acos(n.scaProd(e23)) * (180.0 / MT_PI);
  c = c < 90.0 ? c : 180.0 - c;

  angles.push_back( a );
  angles.push_back( b );
  angles.push_back( c );
}

void write_graph(const mt_vector<mt_idx_t> & cnt,
                 const mt_vector<mt_idx_t> & con,
                 const char* filename)
{
  size_t ncon = con.size();
  mt_vector<int>    lcnt(cnt.size());
  mt_vector<int>    lcol(ncon);
  mt_vector<double> lele(ncon);

  int row_size = htobe(int(lcnt.size()));
  int col_max = *std::max_element(lcol.begin(), lcol.end());
  int col_size = htobe(int(col_max + 2));

  for(size_t i=0; i<cnt.size(); i++)
    lcnt[i] = htobe(int(cnt[i]));

  for(size_t i=0; i<ncon; i++) {
    lcol[i] = htobe(int(con[i] + 1));
    lele[i] = htobe(double(1));
  }

  int petscMatIndex = htobe(int(1211216));
  int nele = htobe(int(ncon));

  FILE* fd = fopen(filename, MT_FOPEN_WRITE);
  if(!fd) {
    treat_file_open_error(filename);
  } else {
    fwrite(&petscMatIndex, sizeof(int)   , 1         , fd);
    fwrite(&row_size     , sizeof(int)   , 1         , fd); // m
    fwrite(&col_size     , sizeof(int)   , 1         , fd); // n
    fwrite(&nele         , sizeof(int)   , 1         , fd); // nz
    fwrite(lcnt.data()   , sizeof(int)   , lcnt.size(), fd); // nnz .. nonzeros per row
    fwrite(lcol.data()   , sizeof(int)   , lcol.size(), fd); // j   .. column indices
    fwrite(lele.data()   , sizeof(double), lele.size(), fd); // s   .. element data

    fclose(fd);
  }
}


void mt_shmem_partitioner::compute_partitions(const mt_meshdata & mesh,
      const int npart)
{
  assert(mesh.n2n_cnt.size() > 0);
  size_t nnodes = mesh.n2n_cnt.size();
  size_t psize = (nnodes) / npart;

  part.resize(npart);
  itf.assign(nnodes, false);

  // loop over partitions
  for(int part_idx=0; part_idx < npart; part_idx++)
  {
    // define partition start and end node indices
    mt_idx_t part_start = part_idx*psize;
    mt_idx_t part_end   = part_idx < npart - 1 ? part_start + psize : nnodes;
    // loop over nodes of current partition
    for(mt_idx_t nidx = part_start; nidx < part_end; nidx++) {
      // add nodes to partition set
      part[part_idx].insert(nidx);
      mt_idx_t row_start = mesh.n2n_dsp[nidx], row_end = row_start + mesh.n2n_cnt[nidx];
      for(mt_idx_t j = row_start; j<row_end; j++)
      {
        mt_idx_t cidx = mesh.n2n_con[j];
        // mark connected nodes outside of the partition interval as interface nodes
        if(cidx < part_start || cidx > part_end)
          itf[cidx] = true;
      }
    }
  }
}

void enclosing_element(const mt_meshdata & mesh,
                       const MT_USET<mt_idx_t> & nod,
                       const mt_point<mt_real> & pt,
                       const mt_real edge_len,
                       mt_idx_t & elem,
                       vec3r & bary)
{
  elem = -1;

  for(const mt_idx_t & nidx : nod) {
    mt_idx_t eidx_start = mesh.n2e_dsp[nidx],
           eidx_stop  = eidx_start + mesh.n2e_cnt[nidx];

    for(mt_idx_t i=eidx_start; i<eidx_stop; i++)
    {
      const mt_idx_t eidx = mesh.n2e_con[i];
      const mt_real eps = edge_len * 1e-6;
#if 0
      if(barycentric_coordinate(mesh, eidx, pt, bary, eps)) {
#else
      int ret = barycentric_coordinate(mesh, eidx, pt, bary, eps);
      if(ret == 0 && valid_barycentric_coord(mesh.etype[eidx], bary)) {
#endif
        elem = eidx;
        return;
      }
    }
  }
}

void expand_nodeset(const mt_meshdata & mesh,
                    MT_USET<mt_idx_t> & nod)
{
  MT_USET<mt_idx_t> oldnod = nod;
  nod.clear();

  for(mt_idx_t nidx : oldnod) {
    if(nidx < mt_idx_t(mesh.n2n_dsp.size())) {
      mt_idx_t start = mesh.n2n_dsp[nidx], stop  = start + mesh.n2n_cnt[nidx];
      for(mt_idx_t i=start; i<stop; i++) nod.insert(mesh.n2n_con[i]);
    }
  }
}

void shrink_nodeset(const mt_meshdata & mesh,
                    MT_USET<mt_idx_t> & nod)
{
  MT_USET<mt_idx_t> bdry;
  nodal_selection_boundary(mesh, nod, bdry);

  for(mt_idx_t nidx : bdry)
    nod.erase(nidx);
}

void expand_in_radius(const mt_meshdata & mesh,
                      const mt_point<mt_real> ref,
                      const mt_real squared_rad,
                      mt_vector<mt_idx_t> & nod)
{
  size_t old_nodsize;
  MT_USET<mt_idx_t> nodset;
  nodset.insert(nod.begin(), nod.end());

  do {
    old_nodsize = nod.size();
    for(auto nidx : nod) {
      mt_idx_t start = mesh.n2n_dsp[nidx], stop = start + mesh.n2n_cnt[nidx];

      for(mt_idx_t i=start; i<stop; i++) {
        const mt_idx_t cidx = mesh.n2n_con[i];
        mt_point<mt_real> e = mt_point<mt_real>(mesh.xyz.data() + cidx*3) - ref;
        if(e.length2() < squared_rad) nodset.insert(cidx);
      }
    }

    nod.assign(nodset.begin(), nodset.end());
  } while(old_nodsize < nod.size());
}

void element_edges_stats(const elem_t type, const mt_idx_t* con, const mt_real* xyz,
                           mt_real & min_edge, mt_real & max_edge, mt_real & avg_edge)
{
  mt_real num_edges = 0;
  min_edge = 1e200, max_edge = 0, avg_edge = 0;

  switch(type) {
    case Prism:
    {
      const mt_idx_t v1 = con[0], v2 = con[1], v3 = con[2], v4 = con[3], v5 = con[4], v6 = con[5];
      mt_real edge;

      const mt_real v1x = xyz[v1*3+0], v1y = xyz[v1*3+1], v1z = xyz[v1*3+2];
      const mt_real v2x = xyz[v2*3+0], v2y = xyz[v2*3+1], v2z = xyz[v2*3+2];
      const mt_real v3x = xyz[v3*3+0], v3y = xyz[v3*3+1], v3z = xyz[v3*3+2];
      const mt_real v4x = xyz[v4*3+0], v4y = xyz[v4*3+1], v4z = xyz[v4*3+2];
      const mt_real v5x = xyz[v5*3+0], v5y = xyz[v5*3+1], v5z = xyz[v5*3+2];
      const mt_real v6x = xyz[v6*3+0], v6y = xyz[v6*3+1], v6z = xyz[v6*3+2];

      // (3,1)
      edge = std::sqrt(POW2(v3x-v1x) + POW2(v3y-v1y) + POW2(v3z-v1z));
      if(min_edge > edge) min_edge = edge;
      if(max_edge < edge) max_edge = edge;
      avg_edge += edge;

      // (3,2)
      edge = std::sqrt(POW2(v3x-v2x) + POW2(v3y-v2y) + POW2(v3z-v2z));
      if(min_edge > edge) min_edge = edge;
      if(max_edge < edge) max_edge = edge;
      avg_edge += edge;

      // (2,1)
      edge = std::sqrt(POW2(v2x-v1x) + POW2(v2y-v1y) + POW2(v2z-v1z));
      if(min_edge > edge) min_edge = edge;
      if(max_edge < edge) max_edge = edge;
      avg_edge += edge;

      // (5,4)
      edge = std::sqrt(POW2(v5x-v4x) + POW2(v5y-v4y) + POW2(v5z-v4z));
      if(min_edge > edge) min_edge = edge;
      if(max_edge < edge) max_edge = edge;
      avg_edge += edge;

      // (6,5)
      edge = std::sqrt(POW2(v5x-v6x) + POW2(v5y-v6y) + POW2(v5z-v6z));
      edge = std::sqrt(POW2(v5x-v6x) + POW2(v5y-v6y) + POW2(v5z-v6z));
      if(min_edge > edge) min_edge = edge;
      if(max_edge < edge) max_edge = edge;
      avg_edge += edge;

      // (6,4)
      edge = std::sqrt(POW2(v4x-v6x) + POW2(v4y-v6y) + POW2(v4z-v6z));
      if(min_edge > edge) min_edge = edge;
      if(max_edge < edge) max_edge = edge;
      avg_edge += edge;

      // (1,4)
      edge = std::sqrt(POW2(v1x-v4x) + POW2(v1y-v4y) + POW2(v1z-v4z));
      if(min_edge > edge) min_edge = edge;
      if(max_edge < edge) max_edge = edge;
      avg_edge += edge;

      // (3,5)
      edge = std::sqrt(POW2(v3x-v5x) + POW2(v3y-v5y) + POW2(v3z-v5z));
      if(min_edge > edge) min_edge = edge;
      if(max_edge < edge) max_edge = edge;
      avg_edge += edge;

      // (2,6)
      edge = std::sqrt(POW2(v2x-v6x) + POW2(v2y-v6y) + POW2(v2z-v6z));
      if(min_edge > edge) min_edge = edge;
      if(max_edge < edge) max_edge = edge;
      avg_edge += edge;

      num_edges = 9.0;
      break;
    }
    case Hexa:
    {
      const mt_idx_t v1 = con[0], v2 = con[1], v3 = con[2], v4 = con[3], v5 = con[4], v6 = con[5], v7 = con[6], v8 = con[7];
      mt_real edge;

      const mt_real v1x = xyz[v1*3+0], v1y = xyz[v1*3+1], v1z = xyz[v1*3+2];
      const mt_real v2x = xyz[v2*3+0], v2y = xyz[v2*3+1], v2z = xyz[v2*3+2];
      const mt_real v3x = xyz[v3*3+0], v3y = xyz[v3*3+1], v3z = xyz[v3*3+2];
      const mt_real v4x = xyz[v4*3+0], v4y = xyz[v4*3+1], v4z = xyz[v4*3+2];
      const mt_real v5x = xyz[v5*3+0], v5y = xyz[v5*3+1], v5z = xyz[v5*3+2];
      const mt_real v6x = xyz[v6*3+0], v6y = xyz[v6*3+1], v6z = xyz[v6*3+2];
      const mt_real v7x = xyz[v7*3+0], v7y = xyz[v7*3+1], v7z = xyz[v7*3+2];
      const mt_real v8x = xyz[v8*3+0], v8y = xyz[v8*3+1], v8z = xyz[v8*3+2];

      // (1,2)
      edge = std::sqrt(POW2(v2x-v1x) + POW2(v2y-v1y) + POW2(v2z-v1z));
      if(min_edge > edge) min_edge = edge;
      if(max_edge < edge) max_edge = edge;
      avg_edge += edge;

      // (3,2)
      edge = std::sqrt(POW2(v3x-v2x) + POW2(v3y-v2y) + POW2(v3z-v2z));
      if(min_edge > edge) min_edge = edge;
      if(max_edge < edge) max_edge = edge;
      avg_edge += edge;

      // (3,4)
      edge = std::sqrt(POW2(v3x-v4x) + POW2(v3y-v4y) + POW2(v3z-v4z));
      if(min_edge > edge) min_edge = edge;
      if(max_edge < edge) max_edge = edge;
      avg_edge += edge;

      // (1,4)
      edge = std::sqrt(POW2(v1x-v4x) + POW2(v1y-v4y) + POW2(v1z-v4z));
      if(min_edge > edge) min_edge = edge;
      if(max_edge < edge) max_edge = edge;
      avg_edge += edge;

      // (5,8)
      edge = std::sqrt(POW2(v5x-v8x) + POW2(v5y-v8y) + POW2(v5z-v8z));
      if(min_edge > edge) min_edge = edge;
      if(max_edge < edge) max_edge = edge;
      avg_edge += edge;

      // (8,7)
      edge = std::sqrt(POW2(v8x-v7x) + POW2(v8y-v7y) + POW2(v8z-v7z));
      if(min_edge > edge) min_edge = edge;
      if(max_edge < edge) max_edge = edge;
      avg_edge += edge;

      // (7,6)
      edge = std::sqrt(POW2(v7x-v6x) + POW2(v7y-v6y) + POW2(v7z-v6z));
      if(min_edge > edge) min_edge = edge;
      if(max_edge < edge) max_edge = edge;
      if(max_edge < edge) max_edge = edge;
      avg_edge += edge;

      // (5,6)
      edge = std::sqrt(POW2(v5x-v6x) + POW2(v5y-v6y) + POW2(v5z-v6z));
      if(min_edge > edge) min_edge = edge;
      if(max_edge < edge) max_edge = edge;
      avg_edge += edge;

      // (5,1)
      edge = std::sqrt(POW2(v5x-v1x) + POW2(v5y-v1y) + POW2(v5z-v1z));
      if(min_edge > edge) min_edge = edge;
      if(max_edge < edge) max_edge = edge;
      avg_edge += edge;

      // (8,2)
      edge = std::sqrt(POW2(v8x-v2x) + POW2(v8y-v2y) + POW2(v8z-v2z));
      if(min_edge > edge) min_edge = edge;
      if(max_edge < edge) max_edge = edge;
      avg_edge += edge;

      // (7,3)
      edge = std::sqrt(POW2(v7x-v3x) + POW2(v7y-v3y) + POW2(v7z-v3z));
      if(min_edge > edge) min_edge = edge;
      if(max_edge < edge) max_edge = edge;
      avg_edge += edge;

      // (4,6)
      edge = std::sqrt(POW2(v4x-v6x) + POW2(v4y-v6y) + POW2(v4z-v6z));
      if(min_edge > edge) min_edge = edge;
      if(max_edge < edge) max_edge = edge;
      avg_edge += edge;

      num_edges = 12.0;
      break;
    }
    case Tetra:
    {
      const mt_idx_t v1 = con[0], v2 = con[1], v3 = con[2], v4 = con[3];
      mt_real edge;

      const mt_real v1x = xyz[v1*3+0], v1y = xyz[v1*3+1], v1z = xyz[v1*3+2];
      const mt_real v2x = xyz[v2*3+0], v2y = xyz[v2*3+1], v2z = xyz[v2*3+2];
      const mt_real v3x = xyz[v3*3+0], v3y = xyz[v3*3+1], v3z = xyz[v3*3+2];
      const mt_real v4x = xyz[v4*3+0], v4y = xyz[v4*3+1], v4z = xyz[v4*3+2];

      // (1,2)
      edge = sqrt(POW2(v1x - v2x) + POW2(v1y - v2y) + POW2(v1z - v2z));
      if(min_edge > edge) min_edge = edge;
      if(max_edge < edge) max_edge = edge;
      avg_edge += edge;
      // (1,3)
      edge = sqrt(POW2(v1x - v3x) + POW2(v1y - v3y) + POW2(v1z - v3z));
      if(min_edge > edge) min_edge = edge;
      if(max_edge < edge) max_edge = edge;
      avg_edge += edge;
      // (2,3)
      edge = sqrt(POW2(v2x - v3x) + POW2(v2y - v3y) + POW2(v2z - v3z));
      if(min_edge > edge) min_edge = edge;
      if(max_edge < edge) max_edge = edge;
      avg_edge += edge;
      // (2,4)
      edge = sqrt(POW2(v2x - v4x) + POW2(v2y - v4y) + POW2(v2z - v4z));
      if(min_edge > edge) min_edge = edge;
      if(max_edge < edge) max_edge = edge;
      avg_edge += edge;
      // (1,4)
      edge = sqrt(POW2(v1x - v4x) + POW2(v1y - v4y) + POW2(v1z - v4z));
      if(min_edge > edge) min_edge = edge;
      if(max_edge < edge) max_edge = edge;
      avg_edge += edge;
      // (3,4)
      edge += sqrt(POW2(v3x - v4x) + POW2(v3y - v4y) + POW2(v3z - v4z));
      if(min_edge > edge) min_edge = edge;
      if(max_edge < edge) max_edge = edge;
      avg_edge += edge;

      num_edges = 6.0;
      break;
    }

    case Tri:
    {
      const mt_idx_t v1 = con[0], v2 = con[1], v3 = con[2];
      mt_real edge;

      const mt_real v1x = xyz[v1*3+0], v1y = xyz[v1*3+1], v1z = xyz[v1*3+2];
      const mt_real v2x = xyz[v2*3+0], v2y = xyz[v2*3+1], v2z = xyz[v2*3+2];
      const mt_real v3x = xyz[v3*3+0], v3y = xyz[v3*3+1], v3z = xyz[v3*3+2];

      // (1,2)
      edge = sqrt(POW2(v1x - v2x) + POW2(v1y - v2y) + POW2(v1z - v2z));
      if(min_edge > edge) min_edge = edge;
      if(max_edge < edge) max_edge = edge;
      avg_edge += edge;
      // (1,3)
      edge = sqrt(POW2(v1x - v3x) + POW2(v1y - v3y) + POW2(v1z - v3z));
      if(min_edge > edge) min_edge = edge;
      if(max_edge < edge) max_edge = edge;
      avg_edge += edge;
      // (2,3)
      edge = sqrt(POW2(v2x - v3x) + POW2(v2y - v3y) + POW2(v2z - v3z));
      if(min_edge > edge) min_edge = edge;
      if(max_edge < edge) max_edge = edge;
      avg_edge += edge;

      num_edges = 3.0;
      break;
    }

    case Line:
    {
      const mt_idx_t v1 = con[0], v2 = con[1];
      mt_real edge;

      const mt_real v1x = xyz[v1*3+0], v1y = xyz[v1*3+1], v1z = xyz[v1*3+2];
      const mt_real v2x = xyz[v2*3+0], v2y = xyz[v2*3+1], v2z = xyz[v2*3+2];

      // (1,2)
      edge = sqrt(POW2(v1x - v2x) + POW2(v1y - v2y) + POW2(v1z - v2z));
      if(min_edge > edge) min_edge = edge;
      if(max_edge < edge) max_edge = edge;
      avg_edge += edge;

      num_edges = 1.0;
      break;
    }

    default:
      std::cerr << "element_edges_stats error: element not yet implemented. Aborting!" << std::endl;
      exit(EXIT_FAILURE);
  }

  avg_edge /= num_edges;
}

mt_real avrg_edgelength_estimate(const mt_meshdata & mesh, bool full)
{
  if(mesh.e2n_cnt.size() == 0) return 0.0;

  if(mesh.e2n_dsp.size() == 0)
    mesh.refresh_dsp();

  MT_USET<mt_tuple<mt_idx_t>> edge_set;

  if(full) {
    #ifdef OPENMP
    #pragma omp parallel
    #endif
    {
      mt_vector<mt_tuple<mt_idx_t>> edges;
      MT_USET<mt_tuple<mt_idx_t>>   loc_set;

      #ifdef OPENMP
      #pragma omp for schedule(guided)
      #endif
      for(size_t eidx = 0; eidx < mesh.e2n_cnt.size(); eidx++) {
        edges.resize(0); get_edges(mesh, eidx, edges);

        for(auto & it : edges)
          loc_set.insert(it);
      }

      #ifdef OPENMP
      #pragma omp critical
      #endif
      {
        edge_set.insert(loc_set.begin(), loc_set.end());
      }
    }
  }
  else {
    const int numsamples = 10;
    mt_vector<mt_tuple<mt_idx_t>> edges;

    for(int s=0; s < numsamples; s++) {
      mt_idx_t eidx = mt_idx_t(drand48() * (mesh.e2n_cnt.size() - 1));
      edges.resize(0); get_edges(mesh, eidx, edges);

      for(auto & it : edges)
        edge_set.insert(it);
    }
  }

  mt_real tavg = 0.0, nvals = 0.0;
  for(auto & it : edge_set) {
    vec3r p1(mesh.xyz.data() + it.v1*3);
    vec3r p2(mesh.xyz.data() + it.v2*3);

    tavg += (p1 - p2).length();
    nvals += 1.0;
  }

  tavg /= nvals;
  return tavg;
}

mt_real avrg_edgelength_estimate(const mt_meshdata & mesh, const mt_vector<mt_idx_t> & eidx)
{
  if(mesh.e2n_dsp.size() == 0) mesh.refresh_dsp();

  MT_USET<mt_tuple<mt_idx_t>> edge_set;

  #ifdef OPENMP
  #pragma omp parallel
  #endif
  {
    mt_vector<mt_tuple<mt_idx_t>> edges;
    MT_USET<mt_tuple<mt_idx_t>>   loc_set;

    #ifdef OPENMP
    #pragma omp for schedule(guided)
    #endif
    for(size_t i = 0; i < eidx.size(); i++) {
      edges.resize(0); get_edges(mesh, eidx[i], edges);

      for(auto & it : edges)
        loc_set.insert(it);
    }

    #ifdef OPENMP
    #pragma omp critical
    #endif
    {
      edge_set.insert(loc_set.begin(), loc_set.end());
    }
  }

  mt_real tavg = 0.0, nvals = 0.0;
  for(auto & it : edge_set) {
    vec3r p1(mesh.xyz.data() + it.v1*3);
    vec3r p2(mesh.xyz.data() + it.v2*3);

    tavg += (p1 - p2).length();
    nvals += 1.0;
  }

  tavg /= nvals;
  return tavg;
}

mt_real avrg_edgelength_estimate(const mt_meshdata & mesh, const MT_USET<mt_tag_t> & tags)
{
  if(mesh.e2n_dsp.size() == 0) mesh.refresh_dsp();

  MT_USET<mt_tuple<mt_idx_t>> edge_set;

  #ifdef OPENMP
  #pragma omp parallel
  #endif
  {
    mt_vector<mt_tuple<mt_idx_t>> edges;
    MT_USET<mt_tuple<mt_idx_t>>   loc_set;

    #ifdef OPENMP
    #pragma omp for schedule(guided)
    #endif
    for(size_t eidx = 0; eidx < mesh.e2n_cnt.size(); eidx++) {
      if(tags.count(mesh.etags[eidx])) {
        edges.resize(0); get_edges(mesh, eidx, edges);

        for(auto & it : edges)
          loc_set.insert(it);
      }
    }

    #ifdef OPENMP
    #pragma omp critical
    #endif
    {
      edge_set.insert(loc_set.begin(), loc_set.end());
    }
  }

  mt_real tavg = 0.0, nvals = 0.0;
  for(auto & it : edge_set) {
    vec3r p1(mesh.xyz.data() + it.v1*3);
    vec3r p2(mesh.xyz.data() + it.v2*3);

    tavg += (p1 - p2).length();
    nvals += 1.0;
  }

  tavg /= nvals;
  return tavg;
}

mt_real min_edgelength_estimate(const mt_meshdata & mesh, bool full, bool nonzeromin)
{
  if(mesh.e2n_dsp.size() == 0)
    mesh.refresh_dsp();

  mt_real tmin = 1e100;
  //GET THE NON-ZERO MIN!
  if(full) {
    #ifdef OPENMP
    #pragma omp parallel for schedule(guided) reduction(min:tmin)
    #endif
    for(size_t eidx = 0; eidx < mesh.e2n_cnt.size(); eidx++) {
      mt_real avg, min, max;
      element_edges_stats(mesh.etype[eidx], mesh.e2n_con.data() + mesh.e2n_dsp[eidx],
          mesh.xyz.data(), min, max, avg);
      if(nonzeromin) {
        if(tmin > min && min > 0.) {
          tmin = min;
        }
      } else {
        if(tmin > min) {
          tmin = min;
        }
      }
    }
  }
  else {
    const int numsamples = 10;

    for(int s=0; s < numsamples; s++) {
      mt_idx_t eidx = mt_idx_t(drand48() * (mesh.e2n_cnt.size() - 1));
      mt_real avg, min, max;
      element_edges_stats(mesh.etype[eidx], mesh.e2n_con.data() + mesh.e2n_dsp[eidx],
          mesh.xyz.data(), min, max, avg);
      if(tmin > min) tmin = min;
    }
  }

  return tmin;
}


void generate_surfmesh_sizing_field(const mt_meshdata & surfmesh, mt_vector<mt_real> & sizes)
{
  if(surfmesh.e2n_dsp.size() == 0)
    surfmesh.refresh_dsp();

  const size_t numpnts = surfmesh.xyz.size() / 3;
  const size_t numnodes = surfmesh.n2n_cnt.size();
  const mt_real * xyz = surfmesh.xyz.data();

  sizes.resize(numpnts);
  sizes.zero();

  #ifdef OPENMP
  #pragma omp parallel for schedule(guided)
  #endif
  for(size_t i=0; i < numnodes; i++) {
    vec3r np(xyz + i*3);
    mt_real local_edge_avg = 0.;
    mt_real numadd = 0.0;
    mt_idx_t start = surfmesh.n2n_dsp[i], stop = start + surfmesh.n2n_cnt[i];
    for(mt_idx_t k=start; k < stop; k++) {
      mt_idx_t cidx = surfmesh.n2n_con[k];
      vec3r p(xyz + cidx*3);

      mt_real edgelen = (np - p).length();
      local_edge_avg += edgelen;
      numadd += 1.0;
    }
    numadd -= 1.0;
    if(numadd > 0)
      local_edge_avg /= numadd;
    sizes[i] = local_edge_avg;
  }
}

void generate_surfmesh_sizing_field(const mt_meshdata & surfmesh,
                                    mt_vector<mt_idx_t> & surf_nod,
                                    mt_vector<mt_real> & sizes)
{
  if(surfmesh.e2n_dsp.size() == 0)
    surfmesh.refresh_dsp();

  const size_t numnodes = surf_nod.size();
  const mt_real * xyz = surfmesh.xyz.data();

  sizes.assign(surfmesh.xyz.size() / 3, 0.0);

  #ifdef OPENMP
  #pragma omp parallel for schedule(guided)
  #endif
  for(size_t i=0; i < numnodes; i++) {
    mt_idx_t nidx = surf_nod[i];
    vec3r np(xyz + nidx*3);
    mt_real local_edge_avg = 0.;
    mt_real numadd = 0.0;
    mt_idx_t start = surfmesh.n2n_dsp[nidx], stop = start + surfmesh.n2n_cnt[nidx];
    for(mt_idx_t k=start; k < stop; k++) {
      mt_idx_t cidx = surfmesh.n2n_con[k];
      vec3r p(xyz + cidx*3);

      mt_real edgelen = (np - p).length();
      local_edge_avg += edgelen;
      numadd += 1.0;
    }
    numadd -= 1.0;
    if(numadd > 0)
      local_edge_avg /= numadd;

    sizes[nidx] = local_edge_avg;
  }
}


void identify_sharp_edge_nodes(const mt_meshdata & surfmesh,
                               const mt_vector<mt_real> & xyz,
                               const mt_vector<mt_idx_t> & vtxlist,
                               MT_USET<mt_idx_t> & edge_vtx,
                               mt_real thr)
{
  if(surfmesh.e2n_dsp.size() == 0)
    surfmesh.refresh_dsp();

  // edge_thr is assumed in degrees, we convert it back to a length
  thr = cos(thr * (MT_PI/ 180.0));

  for(size_t idx = 0; idx < vtxlist.size(); idx++)
  {
    mt_idx_t nidx = vtxlist[idx];
    mt_point<mt_real> nref(0, 0, 0);
    bool sharp = false;

    mt_idx_t start = surfmesh.n2e_dsp[nidx], end = start + surfmesh.n2e_cnt[nidx];
    for(mt_idx_t i=start; i<end; i++)
    {
      mt_idx_t cele = surfmesh.n2e_con[i], dsp = surfmesh.e2n_dsp[cele];
      mt_idx_t v1 = surfmesh.e2n_con[dsp+0], v2 = surfmesh.e2n_con[dsp+1], v3 = surfmesh.e2n_con[dsp+2];
      mt_point<mt_real> n = triangle_normal(v1, v2, v3, xyz);

      if(nref.length2() > 0) {
        // a refrerence normal has been already set. we can compute a current normal
        // and do the angle check
        mt_real pro = nref.scaProd(n);
        if(fabs(pro) < thr) {
          sharp = true;
          break;
        }
      }
      else {
        // the refrerence edge has not been set. we set it now
        nref = n;
      }
    }

    if(sharp) {
      edge_vtx.insert(nidx);

      // uncomment this if you want to add the neighbours of a sharp edge to the
      // sharp edges set
      #if 0
      start = surfmesh.n2n_dsp[nidx];
      end = start + surfmesh.n2n_cnt[nidx];

      for(int i=start; i<end; i++)
        edge_vtx.insert(surfmesh.n2n_con[i]);
      #endif
    }
  }
}

void identify_sharp_edges(const mt_meshdata & surfmesh,
                          const mt_vector<mt_real> & xyz,
                          const mt_mapping<mt_idx_t> & ele2edge,
                          const MT_MAP<mt_tuple<mt_idx_t>, mt_idx_t> & edges,
                          mt_real thr,
                          MT_USET<mt_idx_t> & sharp_edges)
{
  if(surfmesh.e2n_dsp.size() == 0)
    surfmesh.refresh_dsp();

  // edge_thr is assumed in degrees, we convert it back to a length
  thr = cos(thr * (MT_PI/ 180.0));

  for(auto eit = edges.begin(); eit != edges.end(); ++eit)
  {
    mt_idx_t edge_idx = eit->second;
    vec3r nref(0, 0, 0);
    bool sharp = false;

    mt_idx_t start = ele2edge.bwd_dsp[edge_idx], end = start + ele2edge.bwd_cnt[edge_idx];
    for(mt_idx_t i=start; i<end; i++)
    {
      mt_idx_t eidx = ele2edge.bwd_con[i], elem_dsp = surfmesh.e2n_dsp[eidx];

      mt_idx_t v1 = surfmesh.e2n_con[elem_dsp+0], v2 = surfmesh.e2n_con[elem_dsp+1],
             v3 = surfmesh.e2n_con[elem_dsp+2];
      vec3r  n  = triangle_normal(v1, v2, v3, xyz);

      if(nref.length2() > 0) {
        // a refrerence normal has been already set. we can compute a current normal
        // and do the angle check
        mt_real pro = nref.scaProd(n);
        if(fabs(pro) < thr) {
          sharp = true;
          break;
        }
      }
      else {
        // the refrerence edge has not been set. we set it now
        nref = n;
      }
    }

    if(sharp)
      sharp_edges.insert(edge_idx);
  }
}

void identify_sharp_edges(const mt_meshdata & surfmesh,
                          const mt_vector<mt_real> & xyz,
                          MT_USET<mt_idx_t> & nod,
                          mt_real thr,
                          MT_USET<mt_tuple<mt_idx_t>> & sharp_edges)
{
  if(surfmesh.e2n_dsp.size() == 0)
    surfmesh.refresh_dsp();

  if(nod.size() == 0)
    nod.insert(surfmesh.e2n_con.begin(), surfmesh.e2n_con.end());

  // edge_thr is assumed in degrees, we convert it back to a length
  thr = cos(thr * (MT_PI/ 180.0));

  MT_USET<mt_idx_t> ele;
  nodeSet_to_elemSet(surfmesh, nod, ele);

  mt_vector<mt_tuple<mt_idx_t>> cur_edges;
  MT_USET<mt_tuple<mt_idx_t>> edges;

  for(mt_idx_t eidx : ele) {
    cur_edges.resize(0);
    get_edges(surfmesh, eidx, cur_edges);

    edges.insert(cur_edges.begin(), cur_edges.end());
  }

  for(mt_tuple<mt_idx_t> & it : edges) {
    vec3r nref(0, 0, 0);
    bool sharp = false;

    ele.clear();
    elements_with_edge(surfmesh, it.v1, it.v2, ele);

    if(ele.size()) {
      for(mt_idx_t eidx : ele)
      {
        mt_idx_t elem_dsp = surfmesh.e2n_dsp[eidx];

        mt_idx_t v1 = surfmesh.e2n_con[elem_dsp+0], v2 = surfmesh.e2n_con[elem_dsp+1],
                 v3 = surfmesh.e2n_con[elem_dsp+2];
        vec3r  n  = triangle_normal(v1, v2, v3, xyz);

        if(nref.length2() > 0) {
          // a refrerence normal has been already set. we can compute a current normal
          // and do the angle check
          mt_real pro = nref.scaProd(n);
          if(fabs(pro) < thr) {
            sharp = true;
            break;
          }
        } else {
          // the refrerence edge has not been set. we set it now
          nref = n;
        }
      }
    }

    if(sharp)
      sharp_edges.insert(it);
  }
}

void identify_surface_border_nodes(const mt_meshdata & surfmesh, MT_USET<mt_idx_t> & border)
{
  mt_mapping<mt_idx_t> ele2edge;
  MT_MAP<mt_tuple<mt_idx_t>, mt_idx_t> edges;
  compute_edges(surfmesh, ele2edge, edges);
  ele2edge.transpose();

  for(auto it = edges.begin(); it != edges.end(); ++it) {
    mt_idx_t edge_idx = it->second;
    if(ele2edge.bwd_cnt[edge_idx] == 1) {
      // edge connected to only one element -> its a border edge of an open surface
      border.insert(it->first.v1);
      border.insert(it->first.v2);
    }
  }
}

void identify_surface_border(const mt_meshdata & surfmesh, mt_meshdata & border)
{
  mt_mapping<mt_idx_t> ele2edge;
  MT_MAP<mt_tuple<mt_idx_t>, mt_idx_t> edges, border_edges;

  compute_edges(surfmesh, ele2edge, edges);
  ele2edge.transpose();

  for(auto it = edges.begin(); it != edges.end(); ++it) {
    mt_idx_t edge_idx = it->second;
    if(ele2edge.bwd_cnt[edge_idx] == 1)
      border_edges.insert(*it);
  }

  border.e2n_cnt.assign(border_edges.size(), mt_idx_t(2));
  border.etype.assign(border_edges.size(), Line);
  border.e2n_con.resize(border_edges.size() * 2);

  size_t widx = 0;
  for(auto & it : border_edges) {
    border.e2n_con[widx*2+0] = it.first.v1;
    border.e2n_con[widx*2+1] = it.first.v2;
    widx++;
  }

  border.xyz = surfmesh.xyz;
}


void add_interface_to_splitlist(const mt_meshdata & mesh,
                                const mt_meshgraph & mgA,
                                const mt_meshgraph & mgB,
                                mt_idx_t & Nidx,
                                mt_vector<split_item> & splitlist)
{
  mt_vector<mt_idx_t> nodA(mgA.e2n_con), nodB(mgB.e2n_con);
  binary_sort(nodA); unique_resize(nodA);
  binary_sort(nodB); unique_resize(nodB);

  MT_USET<mt_idx_t> eidxA;
  eidxA.insert(mgA.eidx.begin(), mgA.eidx.end());

  int rsize = nodA.size() > nodB.size() ? nodA.size() : nodB.size();
  mt_vector<mt_idx_t> ifc(rsize);

  // compute set of interface nodes via set intersection
  {
    mt_idx_t* e = std::set_intersection(nodA.begin(), nodA.end(),
        nodB.begin(), nodB.end(), ifc.begin());
    ifc.resize(e - ifc.begin());
  }

  splitlist.reserve(splitlist.size() + ifc.size());
  split_item itm;

  // iterate over interface
  for(mt_idx_t nidx : ifc) {
    mt_idx_t start = mesh.n2e_dsp[nidx], end = start + mesh.n2e_cnt[nidx];
    // iterate over elements connected to interface node nidx
    for(int k=start; k < end; k++) {
      mt_idx_t eidx = mesh.n2e_con[k];
      // if element tag is in tagsA insert it into the final splitlist
      if(eidxA.count(eidx)) {
        itm.elemIdx = eidx;
        itm.oldIdx  = nidx;
        itm.newIdx  = Nidx;
        splitlist.push_back(itm);
      }
    }
    Nidx++;
  }
}

void add_surface_to_splitlist(const mt_meshdata & mesh,
                              const mt_meshdata & surf,
                              const MT_USET<mt_idx_t> & vol_eidx,
                              mt_idx_t & Nidx,
                              mt_vector<split_item> & splitlist)
{
  mt_vector<mt_idx_t> ifc(surf.e2n_con);
  binary_sort(ifc); unique_resize(ifc);

  split_item itm;

  for(mt_idx_t nidx : ifc) {
    mt_idx_t start = mesh.n2e_dsp[nidx], end = start + mesh.n2e_cnt[nidx];

    for(int k=start; k < end; k++) {
      mt_idx_t eidx = mesh.n2e_con[k];

      if(vol_eidx.count(eidx)) {
        itm.elemIdx = eidx;
        itm.oldIdx  = nidx;
        itm.newIdx  = Nidx;
        splitlist.push_back(itm);
      }
    }
    Nidx++;
  }
}

void restrict_n2n_to_subset(const mt_vector<mt_idx_t> & n2n_cnt,
                            const mt_vector<mt_idx_t> & n2n_dsp,
                            const mt_vector<mt_idx_t> & n2n_con,
                            const MT_USET<mt_idx_t> & subset,
                            mt_vector<mt_idx_t> & on2n_cnt,
                            mt_vector<mt_idx_t> & on2n_dsp,
                            mt_vector<mt_idx_t> & on2n_con)
{
  on2n_cnt.assign(n2n_cnt.size(), mt_idx_t(0));

  for(mt_idx_t n : subset) {
    mt_idx_t start = n2n_dsp[n], stop = start + n2n_cnt[n];
    for(mt_idx_t j=start; j<stop; j++) {
      mt_idx_t c = n2n_con[j];
      if(subset.count(c))
        on2n_cnt[n]++;
    }
  }

  bucket_sort_offset(on2n_cnt, on2n_dsp);
  size_t con_sum = bucket_sort_size(on2n_cnt);
  on2n_con.resize(con_sum);

  mt_vector<mt_idx_t> write_pos(on2n_dsp);

  for(mt_idx_t n : subset) {
    mt_idx_t start = n2n_dsp[n], stop = start + n2n_cnt[n];
    for(mt_idx_t j=start; j<stop; j++) {
      mt_idx_t c = n2n_con[j];
      if(subset.count(c))
        on2n_con[write_pos[n]++] = c;
    }
  }
}

/// compute permutation of surfaces so that we have an outer-to-inner surface order
void get_surface_outer_to_inner_perm(const mt_vector<kdtree*> & surface_trees, mt_vector<int> & perm)
{
  mt_vector<surfbox_elem> surface_boxes(surface_trees.size());
  for(size_t i=0; i<surface_trees.size(); i++)
  {
    surface_boxes[i].tree  = surface_trees[i];
    surface_boxes[i].sidx = i;
  }

  std::sort(surface_boxes.begin(), surface_boxes.end(), bounding_to_nonbounding{});

  perm.resize(surface_boxes.size());
  for(size_t i=0; i<surface_trees.size(); i++)
    perm[i] = surface_boxes[i].sidx;
}

bool mesh_is_trisurf(const mt_meshdata & mesh)
{
  return (has_surface_elems(mesh) && !has_volumetric_elems(mesh));
}

mt_idx_t get_quadface_ctr_idx(const mt_idx_t v0,
                            const mt_idx_t v1,
                            const mt_idx_t v2,
                            const mt_idx_t v3,
                            const mt_real* in_xyz,
                            mt_meshdata & out,
                            MT_MAP<mt_quadruple<mt_idx_t>,mt_idx_t> & quad2idx)
{
  mt_vector<mt_idx_t> face_indices(4);
  vec3r ctr;
  ctr += vec3r(in_xyz + v0*3); ctr += vec3r(in_xyz + v1*3);
  ctr += vec3r(in_xyz + v2*3); ctr += vec3r(in_xyz + v3*3);
  ctr /= mt_real(4);

  face_indices[0] = v0; face_indices[1] = v1; face_indices[2] = v2; face_indices[3] = v3;
  binary_sort(face_indices);

  mt_quadruple<mt_idx_t> qe;
  qe.v1 = face_indices[0], qe.v2 = face_indices[1];
  qe.v3 = face_indices[2], qe.v4 = face_indices[3];

  mt_idx_t ctr_idx = -1;
  if(quad2idx.count(qe)) {
    ctr_idx = quad2idx[qe];
  } else {
    ctr_idx = out.xyz.size() / 3;
    quad2idx[qe] = ctr_idx;
    out.xyz.push_back(ctr.x); out.xyz.push_back(ctr.y); out.xyz.push_back(ctr.z);
  }

  return ctr_idx;
}

void add_face_pyr(const mt_idx_t v0, const mt_idx_t v1, const mt_idx_t v2, const mt_idx_t v3, const mt_idx_t c,
                  const mt_real* in_xyz,
                  const mt_idx_t tag,
                  const short nfib, const mt_fib_t* lon,
                  mt_meshdata & out,
                  MT_MAP<mt_quadruple<mt_idx_t>,mt_idx_t> & quad2idx)
{
  mt_idx_t fc_idx = get_quadface_ctr_idx(v0, v1, v2, v3, in_xyz, out, quad2idx);
  mt_idx_t con[4];

  // sub-tetraeder consisting of edge (0,1) center point of the pyramids base (fc) and the pyramids tip (4)
  con[0] = v0, con[1] = v1, con[2] = fc_idx, con[3] = c;
  mesh_add_elem(out, Tetra, con, tag, nfib, lon);

  // sub-tet: (1,2) (fc) (4)
  con[0] = v1, con[1] = v2, con[2] = fc_idx, con[3] = c;
  mesh_add_elem(out, Tetra, con, tag, nfib, lon);

  // sub-tet: (2,3) (fc) (4)
  con[0] = v2, con[1] = v3, con[2] = fc_idx, con[3] = c;
  mesh_add_elem(out, Tetra, con, tag, nfib, lon);

  // sub-tet: (3,0) (fc) (4)
  con[0] = v3, con[1] = v0, con[2] = fc_idx, con[3] = c;
  mesh_add_elem(out, Tetra, con, tag, nfib, lon);
}

void pyr_to_tet(const mt_meshdata & in, mt_meshdata & out, const mt_idx_t in_eidx,
                MT_MAP<mt_quadruple<mt_idx_t>,mt_idx_t> & quad2idx)
{
  const mt_real* xyz = in.xyz.data();
  const mt_idx_t*  in_con = in.e2n_con.data() + in.e2n_dsp[in_eidx];

  short nfib = in.e2n_cnt.size() * 6 == in.lon.size() ? 6 : 3;
  const mt_idx_t tag = in.etags[in_eidx];
  const mt_fib_t *lon = in.lon.data() + in_eidx * nfib;

  mt_idx_t v0, v1, v2, v3, v4;
  v0 = in_con[0], v1 = in_con[1], v2 = in_con[2], v3 = in_con[3], v4 = in_con[4];

  add_face_pyr(v0, v1, v2, v3, v4, xyz, tag, nfib, lon, out, quad2idx);
}

void hex_to_tet(const mt_meshdata & in, mt_meshdata & out, const mt_idx_t in_eidx,
                MT_MAP<mt_quadruple<mt_idx_t>,mt_idx_t> & quad2idx)
{
  const mt_real* xyz = in.xyz.data();
  const mt_idx_t*  in_con = in.e2n_con.data() + in.e2n_dsp[in_eidx];

  short nfib = in.e2n_cnt.size() * 6 == in.lon.size() ? 6 : 3;
  const mt_idx_t tag = in.etags[in_eidx];
  const mt_fib_t *lon = in.lon.data() + in_eidx * nfib;

  mt_idx_t v0 = in_con[0], v1 = in_con[1], v2 = in_con[2], v3 = in_con[3];
  mt_idx_t v4 = in_con[4], v5 = in_con[5], v6 = in_con[6], v7 = in_con[7];

  vec3r ctr;
  ctr += vec3r(xyz + v0*3); ctr += vec3r(xyz + v1*3);
  ctr += vec3r(xyz + v2*3); ctr += vec3r(xyz + v3*3);
  ctr += vec3r(xyz + v4*3); ctr += vec3r(xyz + v5*3);
  ctr += vec3r(xyz + v6*3); ctr += vec3r(xyz + v7*3);
  ctr /= mt_real(8);

  mt_idx_t ctr_idx = out.xyz.size() / 3;
  out.xyz.push_back(ctr.x); out.xyz.push_back(ctr.y); out.xyz.push_back(ctr.z);

  // face (0,1,2,3)
  add_face_pyr(v0, v1, v2, v3, ctr_idx, xyz, tag, nfib, lon, out, quad2idx);

  // face (0,4,7,1)
  add_face_pyr(v0, v4, v7, v1, ctr_idx, xyz, tag, nfib, lon, out, quad2idx);

  // face (1,7,6,2)
  add_face_pyr(v1, v7, v6, v2, ctr_idx, xyz, tag, nfib, lon, out, quad2idx);

  // face (2,6,5,3)
  add_face_pyr(v2, v6, v5, v3, ctr_idx, xyz, tag, nfib, lon, out, quad2idx);

  // face (3,5,4,0)
  add_face_pyr(v3, v5, v4, v0, ctr_idx, xyz, tag, nfib, lon, out, quad2idx);

  // face (4,5,6,7)
  add_face_pyr(v4, v5, v6, v7, ctr_idx, xyz, tag, nfib, lon, out, quad2idx);
}

void prism_to_tet(const mt_meshdata & in, mt_meshdata & out, const mt_idx_t in_eidx,
                  MT_MAP<mt_quadruple<mt_idx_t>,mt_idx_t> & quad2idx)
{
  const mt_real* xyz = in.xyz.data();
  const mt_idx_t*  in_con = in.e2n_con.data() + in.e2n_dsp[in_eidx];

  short nfib = in.e2n_cnt.size() * 6 == in.lon.size() ? 6 : 3;
  const mt_idx_t tag = in.etags[in_eidx];
  const mt_fib_t *lon = in.lon.data() + in_eidx * nfib;

  mt_idx_t v0 = in_con[0], v1 = in_con[1], v2 = in_con[2];
  mt_idx_t v3 = in_con[3], v4 = in_con[4], v5 = in_con[5];

  vec3r ctr;
  ctr += vec3r(xyz + v0*3); ctr += vec3r(xyz + v1*3);
  ctr += vec3r(xyz + v2*3); ctr += vec3r(xyz + v3*3);
  ctr += vec3r(xyz + v4*3); ctr += vec3r(xyz + v5*3);
  ctr /= mt_real(6);

  mt_idx_t ctr_idx = out.xyz.size() / 3;
  out.xyz.push_back(ctr.x); out.xyz.push_back(ctr.y); out.xyz.push_back(ctr.z);

  // face (0,3,5,1)
  add_face_pyr(v0, v3, v5, v1, ctr_idx, xyz, tag, nfib, lon, out, quad2idx);

  // face (2,1,5,4)
  add_face_pyr(v2, v1, v5, v4, ctr_idx, xyz, tag, nfib, lon, out, quad2idx);

  // face (0,2,4,3)
  add_face_pyr(v0, v2, v4, v3, ctr_idx, xyz, tag, nfib, lon, out, quad2idx);

  mt_idx_t con[4];
  con[3] = ctr_idx;

  // face (0,1,2)
  con[0] = v0, con[1] = v1, con[2] = v2;
  mesh_add_elem(out, Tetra, con, tag, nfib, lon);

  // face (3,4,5)
  con[0] = v3, con[1] = v4, con[2] = v5;
  mesh_add_elem(out, Tetra, con, tag, nfib, lon);
}

void tetrahedralize_mesh(const mt_meshdata & in, mt_meshdata & out)
{
  MT_MAP<mt_quadruple<mt_idx_t>,mt_idx_t> quad2idx;
  out.xyz = in.xyz;

  for(size_t e_in = 0; e_in < in.e2n_cnt.size(); e_in++) {
    switch(in.etype[e_in]) {
      default:
      case Tetra: {
        short nfib = in.e2n_cnt.size() * 6 == in.lon.size() ? 6 : 3;
        const mt_idx_t*  con = in.e2n_con.data() + in.e2n_dsp[e_in];
        const mt_idx_t   tag = in.etags[e_in];
        const mt_fib_t*  lon = in.lon.data() + e_in * nfib;
        mesh_add_elem(out, in.etype[e_in], con, tag, nfib, lon);
        break;
      }

      case Pyramid:
        pyr_to_tet(in, out, e_in, quad2idx);
        break;

      case Hexa:
        hex_to_tet(in, out, e_in, quad2idx);
        break;

      case Prism:
        prism_to_tet(in, out, e_in, quad2idx);
        break;
    }
  }
}

int tetrahedralize_hex_mesh(const mt_meshdata & in, mt_meshdata & out)
{
  if(!mesh_is_hexmesh(in)) return -2;
  
  const size_t nelemi = in.e2n_cnt.size();
  const size_t nelemo = 6*nelemi;
  const size_t fibdim = (in.lon.size()/(3*nelemi)); // 0, 1 or 2
  
  // assign points
  out.xyz = in.xyz;
  // assign cnt & type in e2n-graph
  out.e2n_cnt.assign(nelemo, 4);
  out.etype.assign(nelemo, Tetra);
  // reserve memory for remaining 
  // arrays in e2n-graph
  out.e2n_con.resize(4*nelemo); 
  out.e2n_dsp.resize(nelemo); 
  // reserve memory for element tags
  // and fibers
  out.etags.resize(nelemo); 
  out.lon.resize(3*fibdim*nelemo); 

  // iterate over all elements
  for (size_t i=0; i<nelemi; i++) {
    const mt_idx_t dsp = in.e2n_dsp[i];
    const mt_idx_t v0 = in.e2n_con[dsp+0];
    const mt_idx_t v1 = in.e2n_con[dsp+1];
    const mt_idx_t v2 = in.e2n_con[dsp+2];
    const mt_idx_t v3 = in.e2n_con[dsp+3];
    const mt_idx_t v4 = in.e2n_con[dsp+4];
    const mt_idx_t v5 = in.e2n_con[dsp+5];
    const mt_idx_t v6 = in.e2n_con[dsp+6];
    const mt_idx_t v7 = in.e2n_con[dsp+7];
    const mt_tag_t etag = in.etags[i];     

    #define ADD_FIBERS(I) if(fibdim==1){out.lon[3*(6*i+(I))+0]=in.lon[3*i+0];out.lon[3*(6*i+(I))+1]=in.lon[3*i+1]; \
    out.lon[3*(6*i+(I))+2]=in.lon[3*i+2];}else if(fibdim==2){out.lon[6*(6*i+(I))+0]=in.lon[6*i+0]; \
    out.lon[6*(6*i+(I))+1]=in.lon[6*i+1];out.lon[6*(6*i+(I))+2]=in.lon[6*i+2];out.lon[6*(6*i+(I))+3]=in.lon[6*i+3]; \
    out.lon[6*(6*i+(I))+4]=in.lon[6*i+4];out.lon[6*(6*i+(I))+5]=in.lon[6*i+5]; }

    #define ADD_TET(I,A,B,C,D) {out.etags[6*i+(I)]=etag;out.e2n_dsp[6*i+(I)]=24*i+4*(I); \
    out.e2n_con[4*(6*i+(I))+0]=(A);out.e2n_con[4*(6*i+(I))+1]=(B);out.e2n_con[4*(6*i+(I))+2]=(C); \
    out.e2n_con[4*(6*i+(I))+3]=(D);ADD_FIBERS(I);}

    ADD_TET(0,v4,v6,v7,v2);
    ADD_TET(1,v4,v7,v1,v2);
    ADD_TET(2,v4,v1,v0,v2);
    ADD_TET(3,v4,v3,v2,v0);
    ADD_TET(4,v5,v3,v2,v4);
    ADD_TET(5,v5,v2,v6,v4);

    #undef ADD_TET
    #undef ADD_FIBERS
  }
  return 0;
}

void get_nlevel_graph_laplace(const mt_meshdata & mesh, const int n,
                              mt_vector<mt_cnt_t> & cnt, mt_vector<mt_idx_t> & con)
{
  cnt.assign(mesh.n2n_cnt.size(), mt_cnt_t(0));
  con.resize(0); con.reserve(cnt.size() * 16);
  MT_USET<mt_idx_t> N;

  for(size_t i=0; i<cnt.size(); i++) {
    if(mesh.n2n_cnt[i]) {
      N.clear();
      N.insert(mt_idx_t(i));
      for(int j=0; j<n; j++)
        expand_nodeset(mesh, N);

      cnt[i] = N.size();
      for(mt_idx_t & nn : N) con.push_back(nn);
    }
  }
}

void do_retag_iter(mt_meshdata & mesh, int ncon, int nneigh)
{
  struct timeval t1, t2;

  // get unique tag list
  mt_vector<mt_tag_t> tags = mesh.etags;
  binary_sort(tags); unique_resize(tags);
  mt_idx_t update = 0;

  do {
    update = 0;
    MT_USET<mt_idx_t> bad_elems;
    PROGRESS<size_t> prg(tags.size(), "Identifying isolated elements: ");

    gettimeofday(&t1, NULL);
    for(size_t i=0; i<tags.size(); i++)
    {
      mt_idx_t ctag = tags[i];
      MT_USET<mt_tag_t> tagset; tagset.insert(ctag);
      mt_meshgraph mg;
      extract_tagged_meshgraph(tagset, mesh, mg);
      mt_vector<mt_cnt_t> & e2n_cnt = mg.e2n_cnt;
      mt_vector<mt_idx_t> & e2n_con = mg.e2n_con;
      mt_vector<mt_cnt_t> n2e_cnt, e2e_cnt;
      mt_vector<mt_idx_t> e2n_dsp, n2e_dsp, n2e_con, e2e_con, e2e_mlt;

      bucket_sort_offset(e2n_cnt, e2n_dsp);
      transpose_connectivity_par(e2n_cnt, e2n_dsp, e2n_con, n2e_cnt, n2e_dsp, n2e_con);

      multiply_connectivities(e2n_cnt, e2n_con, n2e_cnt, n2e_con, e2e_cnt, e2e_con, e2e_mlt);
      restrict_connectivity<mt_cnt_t,mt_idx_t>(e2e_cnt, e2e_con, e2e_mlt, ncon);

      for(size_t eidx = 0; eidx < e2e_cnt.size(); eidx++)
      {
        if( (e2e_cnt[eidx] - 1) < nneigh )
          bad_elems.insert(mg.eidx[eidx]);
      }
      prg.next();
    }
    prg.finish();
    gettimeofday(&t2, NULL);
    std::cout << "Done in " << (float)timediff_sec(t1, t2) << " sec" << std::endl;

    std::cout << "Found " << bad_elems.size() << " isolated elements." << std::endl;
    if(bad_elems.size()) {
      for(auto eidx : bad_elems)
      {
        mt_idx_t* con = mesh.e2n_con.data() + mesh.e2n_dsp[eidx];
        MT_USET<mt_idx_t> nset, eset;
        nset.insert(con, con + mesh.e2n_cnt[eidx]);
        nodeSet_to_elemSet(mesh, nset, eset);
        eset.erase(eidx);

        MT_MAP<mt_idx_t, mt_idx_t> tagmults;
        for(auto n : eset)
        {
          mt_idx_t ctag = mesh.etags[n];
          mt_idx_t mult = tagmults[ctag];
          tagmults[ctag] = ++mult;
        }

        mt_idx_t maxmult = 0, maxtag = -1;
        for(auto mit = tagmults.begin(); mit != tagmults.end(); ++mit) {
          if(maxmult < mit->second) {
            maxmult = mit->second;
            maxtag = mit->first;
          }
        }
        if(maxtag > -1 && maxtag != mesh.etags[eidx]) {
          mesh.etags[eidx] = maxtag;
          update++;
        }
      }
      gettimeofday(&t2, NULL);
      std::cout << "Updated " << update << " tags in " << (float)timediff_sec(t1, t2) << " sec" << std::endl;
    }
  } while(update);
}

void smooth_tags_iter(mt_meshdata & mesh,
                      mt_vector<mt_idx_t> & eidx,
                      int rad)
{
#if 0
  MT_USET<mt_idx_t> eidx_set;
  eidx_set.insert(eidx.begin(), eidx.end());

  for(mt_idx_t e : eidx)
  {
    mt_idx_t* con = mesh.e2n_con.data() + mesh.e2n_dsp[e];
    MT_USET<mt_idx_t> nset, eset;

    nset.insert(con, con + mesh.e2n_cnt[e]);
    nodeSet_to_elemSet(mesh, nset, eset);
    eset.erase(e);

    MT_MAP<mt_idx_t, mt_idx_t> tagmults;
    for(auto n : eset) {
      if(eidx_set.count(n)) {
        mt_idx_t ctag   = mesh.etags[n];
        tagmults[ctag] += 1;
      }
    }

    mt_idx_t maxmult = 0, maxtag = -1;
    for(auto mit = tagmults.begin(); mit != tagmults.end(); ++mit) {
      if(maxmult < mit->second) {
        maxmult = mit->second;
        maxtag = mit->first;
      }
    }
    if(maxtag > -1 && maxtag != mesh.etags[e]) {
      mesh.etags[e] = maxtag;
    }
  }
#else
  mt_vector<mt_tag_t> tags_srt;
  MT_USET<mt_idx_t> nset, eset;

  mt_idx_t max_elem = *std::max_element(eidx.begin(), eidx.end());
  mt_mask  smooth_dom(max_elem+1);
  for(mt_idx_t e : eidx) smooth_dom.insert(e);

  for(mt_idx_t e : eidx)
  {
    mt_idx_t* con = mesh.e2n_con.data() + mesh.e2n_dsp[e];

    nset.clear(); eset.clear();
    nset.insert(con, con + mesh.e2n_cnt[e]);

    for(int r = 1; r < rad; r++)
      expand_nodeset(mesh, nset);

    // this is like a nodeSet_to_elemSet, but we only include the elems that
    // are present in smooth_dom
    for(mt_idx_t n : nset) {
      mt_idx_t start = mesh.n2e_dsp[n], stop = start + mesh.n2e_cnt[n];
      for(mt_idx_t j=start; j<stop; j++) {
        mt_idx_t ce = mesh.n2e_con[j];
        if(smooth_dom.count(ce)) eset.insert(ce);
      }
    }

    tags_srt.reserve_and_clear(eset.size());
    for(mt_idx_t ee : eset) tags_srt.push_back(mesh.etags[ee]);
    std::sort(tags_srt.begin(), tags_srt.end());

    mesh.etags[e] = tags_srt[tags_srt.size()/2];
  }
#endif
}

void smooth_tags(mt_meshdata & mesh, mt_vector<mt_idx_t> & eidx, int niter, int rad)
{
  if(eidx.size() == 0) range<mt_idx_t>(0, mesh.e2n_cnt.size(), eidx);

  for(int i=0; i<niter; i++)
    smooth_tags_iter(mesh, eidx, rad);
}

void check_non_mainfold(mt_meshdata & mesh,
                        mt_vector<mt_tuple<mt_idx_t>> & edges,
                        mt_vector<mt_idx_t> & eidx)
{
  MT_MAP<mt_tuple<mt_idx_t>, mt_idx_t> edge_map;
  mt_mapping<mt_idx_t> ele2edge;

  compute_edges(mesh, ele2edge, edge_map);
  ele2edge.transpose(); ele2edge.setup_dsp();

  MT_USET<mt_idx_t> sel_edges;

  for(size_t edge = 0; edge < ele2edge.bwd_cnt.size(); edge++) {
    if(ele2edge.bwd_cnt[edge] > 2)
      sel_edges.insert(edge);
  }

  for(auto & it : edge_map) {
    mt_tuple<mt_idx_t> & t = it.first;
    mt_idx_t & edge     = it.second;

    if(sel_edges.count(edge)) {
      edges.push_back(t);

      mt_idx_t start = ele2edge.bwd_dsp[edge], stop = start + 1;
      for(mt_idx_t j=start; j<stop; j++)
        eidx.push_back(ele2edge.bwd_con[j]);
    }
  }
}

void check_collapsed_surf(mt_meshdata & trimesh, MT_USET<mt_idx_t> & nod)
{
  mt_vector<mt_real> snrml;
  compute_element_surface_normals(trimesh, trimesh.xyz, snrml);

  for(size_t i=0; i<trimesh.n2e_cnt.size(); i++) {
    if(trimesh.n2e_cnt[i] > 1) {
      mt_idx_t start = trimesh.n2e_dsp[i], stop = start + trimesh.n2e_cnt[i];

      vec3r n0(snrml.data() + trimesh.n2e_con[start]*3);

      for(mt_idx_t j=start+1; j<stop; j++) {
        mt_idx_t e = trimesh.n2e_con[j];
        vec3r n(snrml.data() + e*3);

        if(n.scaProd(n0) < -0.3) {
          nod.insert(i);
          break;
        }
      }
    }
  }
}

void close_trimesh(mt_meshdata & mesh)
{
  MT_MAP<mt_tuple<mt_idx_t>, mt_idx_t> edge_map;
  mt_mapping<mt_idx_t> ele2edge;

  compute_edges(mesh, ele2edge, edge_map);
  ele2edge.transpose(); ele2edge.setup_dsp();

  mt_vector<mt_tuple<mt_idx_t>> open_edges;
  for (auto it = edge_map.begin(); it != edge_map.end(); ++it) {
    mt_idx_t edge_idx = it->second;
    if (ele2edge.bwd_cnt[edge_idx] == 1)
      open_edges.push_back(it->first);
  }

  auto linemesh_ctr = [] (const mt_vector<mt_real> & xyz,
                          const mt_meshdata & segm,
                          vec3r & ctr)
  {
    ctr = {0, 0, 0};
    int cnt = 0;

    for (size_t i=0; i<segm.e2n_con.size()/2; i++) {
      mt_idx_t v1 = segm.e2n_con[i*2+0],
               v2 = segm.e2n_con[i*2+1];

      ctr += vec3r(xyz.data() + v1 * 3);
      ctr += vec3r(xyz.data() + v2 * 3);
      cnt += 2;
    }

    ctr /= mt_real(cnt);
  };

  auto close_segment = [&edge_map, &ele2edge](const mt_vector<mt_idx_t> & segm_con,
                          const vec3r & ctr,
                          mt_meshdata & md)
  {
    size_t num_old_pts = md.xyz.size() / 3;
    size_t num_old_ele = md.e2n_cnt.size();
    int num_lon = md.lon.size() == num_old_ele * 6 ? 6 : 3;

    md.xyz.push_back(ctr.x);
    md.xyz.push_back(ctr.y);
    md.xyz.push_back(ctr.z);

    mt_idx_t new_elem_idx = num_old_ele;

    for (size_t i=0; i<segm_con.size()/2; i++) {
      mt_idx_t v1 = segm_con[i*2+0],
               v2 = segm_con[i*2+1];

      md.e2n_con.push_back(v1);
      md.e2n_con.push_back(v2);
      md.e2n_con.push_back(num_old_pts);

      mt_tuple<mt_idx_t> t  = {v1 < v2 ? v1 : v2, v1 < v2 ? v2 : v1};
      mt_idx_t edge_idx     = edge_map.find(t)->second;
      mt_idx_t old_elem_idx = ele2edge.bwd_con[ele2edge.bwd_dsp[edge_idx]];

      md.etags.push_back(md.etags[old_elem_idx]);
      md.etype.push_back(Tri);
      md.e2n_cnt.push_back(3);

      for(int j=0; j<num_lon; j++)
        md.lon.push_back(md.lon[old_elem_idx*num_lon + j]);

      new_elem_idx++;
    }
  };

  mt_meshdata all_openings;
  for(auto & t : open_edges) {
    all_openings.e2n_con.push_back(t.v1);
    all_openings.e2n_con.push_back(t.v2);
  }

  all_openings.e2n_cnt.assign(all_openings.e2n_con.size() / 2, 2);
  all_openings.etype.assign(all_openings.e2n_cnt.size(), Line);

  compute_full_mesh_connectivity(all_openings);

  mt_vector<mt_meshdata> openings;
  mt_vector<mt_vector<mt_idx_t>> parts_eidx;

  nodal_connectivity_decomposition(all_openings, openings, parts_eidx);

  for(const mt_meshdata & m : openings) {
    vec3r ctr;
    linemesh_ctr(mesh.xyz, m, ctr);
    close_segment(m.e2n_con, ctr, mesh);
  }
}

