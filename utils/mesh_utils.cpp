/**
* @file mesh_utils.cpp
* @brief General mesh utility funcions
* @author Aurel Neic
* @version
* @date 2017-08-16
*/

#ifdef OPENMP
#include <omp.h>
#endif

#include "mt_utils.h"
#include "topology_utils.h"
#include "dense_mat.hpp"


/// the global progress manager variable
progress_manager mt_prg_mng;

void reindex_nodes(mt_meshdata & mesh,
                   mt_vector<mt_idx_t> & nod_out,
                   bool verbose)
{
  mt_vector<mt_idx_t> & con = mesh.e2n_con;
  mt_vector<mt_real> & xyz = mesh.xyz;

  PROGRESS<short>* prg = NULL;
  if(verbose)
    prg = new PROGRESS<short>(5, "Reindexing progress: ");

  // create nod_out
  nod_out.assign(con.begin(), con.end());

  if(prg) prg->next();

  binary_sort(nod_out);
  unique_resize(nod_out);

  if(prg) prg->next();

  // extract new list of points
  mt_vector<mt_real> old_xyz(xyz);
  for(size_t i=0; i<nod_out.size(); i++)
  {
    mt_idx_t n = nod_out[i];
    xyz[i*3+0] = old_xyz[n*3+0];
    xyz[i*3+1] = old_xyz[n*3+1];
    xyz[i*3+2] = old_xyz[n*3+2];
  }
  xyz.resize(nod_out.size()*3);

  if(prg) prg->next();

  // remap con to indices in nod_out -------------------------------------
  map_glob2loc(nod_out, con);

  if(prg) prg->next();

  // reallocate
  xyz.shrink_to_fit();

  if(verbose) {
    prg->finish();
    delete prg;
  }
}

void reindex_nodes(mt_meshdata & mesh, bool verbose)
{
  mt_vector<mt_idx_t> & con = mesh.e2n_con;
  mt_vector<mt_real> & xyz = mesh.xyz;

  PROGRESS<short>* prg = NULL;
  if(verbose)
    prg = new PROGRESS<short>(6, "Reindexing progress: ");

  // create nod_out
  MT_MAP<mt_idx_t,mt_idx_t> g2l;
  {
    for(const mt_idx_t & c : con)
      g2l[c] = 0;

    if(prg) prg->next();
    g2l.sort();

    if(prg) prg->next();
    auto it = g2l.begin();
    for(size_t i=0; i < g2l.size(); ++i, ++it)
      it->second = i;
  }

  if(prg) prg->next();

  // extract new list of points
  mt_vector<mt_real> old_xyz(xyz);
  for(auto & m : g2l)
  {
    mt_idx_t glb = m.first;
    mt_idx_t loc = m.second;
    xyz[loc*3+0] = old_xyz[glb*3+0];
    xyz[loc*3+1] = old_xyz[glb*3+1];
    xyz[loc*3+2] = old_xyz[glb*3+2];
  }
  xyz.resize(g2l.size()*3);

  if(prg) prg->next();

  // remap con to indices in nod_out -------------------------------------
  map_glob2loc(g2l, con);

  if(prg) prg->next();

  // reallocate
  xyz.shrink_to_fit();

  if(verbose) {
    prg->finish();
    delete prg;
  }
}


void restrict_elements(const mt_vector<bool> & keep,
                       mt_meshdata & mesh,
                       mt_vector<mt_idx_t> & eidx_out,
                       bool verbose)
{
  mt_vector<mt_cnt_t> & cnt = mesh.e2n_cnt;
  mt_vector<mt_idx_t> & con = mesh.e2n_con;
  bool have_tag  = mesh.etags.size() == cnt.size();
  bool have_type = mesh.etype.size() == cnt.size();
  bool have_lon  = mesh.lon.size() == cnt.size() * 3 || mesh.lon.size() == cnt.size() * 6;

  short nlon = (mesh.lon.size() / 6) == mesh.e2n_cnt.size() ? 6 : 3;

  size_t widx=0;
  size_t e_ridx=0, e_widx=0;
  size_t numelems = cnt.size();
  eidx_out.resize(numelems);

  PROGRESS<size_t>* prg = NULL;

  if(verbose)
    prg = new PROGRESS<size_t>(numelems, "Mesh elements restriction: ");

  for(size_t i=0; i<numelems; i++)
  {
    if( keep[i] )
    {
      // element index
      eidx_out[widx] = i;
      // element size
      cnt[widx] = cnt[i];

      if(have_tag)  mesh.etags[widx] = mesh.etags[i];
      if(have_type) mesh.etype[widx] = mesh.etype[i];
      // fibers
      if(have_lon)
        for(int j=0; j<nlon; j++) mesh.lon[widx*nlon + j] = mesh.lon[i*nlon + j];

      widx++;

      // element connectivity
      for(int j=0; j<cnt[i]; j++)
        con[e_widx++] = con[e_ridx++];

    }
    else
      e_ridx += cnt[i];

    if(prg) prg->next();
  }
  eidx_out.resize(widx);

  if(have_tag) {
    mesh.etags.resize(widx);
    mesh.etags.shrink_to_fit();
  }

  if(have_type) {
    mesh.etype.resize(widx);
    mesh.etype.shrink_to_fit();
  }

  if(have_lon)
    mesh.lon.resize(widx*nlon);

  // reallocate
  eidx_out.shrink_to_fit();
  cnt.resize(widx);   cnt.shrink_to_fit();
  con.resize(e_widx); con.shrink_to_fit();

  if(verbose) {
    prg->finish();
    delete prg;
  }
}

void extract_tagged_meshgraph(const MT_USET<mt_tag_t> & tags,
                              const mt_meshdata & mesh,
                              mt_meshgraph & graph)
{
  size_t numelem = mesh.e2n_cnt.size();
  size_t num_extr_elem = 0, num_extr_entr = 0;

  for(size_t i=0; i<numelem; i++) {
    if(tags.count(mesh.etags[i])) {
      num_extr_elem++;
      num_extr_entr += mesh.e2n_cnt[i];
    }
  }

  graph.e2n_cnt.resize(num_extr_elem);
  graph.etype  .resize(num_extr_elem);
  graph.e2n_con.resize(num_extr_entr);
  graph.eidx.resize(num_extr_elem);

  for(size_t ridx_cnt=0, ridx_con=0, widx_cnt=0, widx_con=0; ridx_cnt<numelem; ridx_cnt++) {
    if(tags.count(mesh.etags[ridx_cnt])) {
      graph.e2n_cnt[widx_cnt] = mesh.e2n_cnt[ridx_cnt];
      graph.etype[widx_cnt]   = mesh.etype[ridx_cnt];
      graph.eidx[widx_cnt]    = ridx_cnt;

      for(mt_idx_t j=0; j<graph.e2n_cnt[widx_cnt]; j++)
        graph.e2n_con[widx_con++] = mesh.e2n_con[ridx_con++];

      widx_cnt++;
    }
    else
      ridx_con += mesh.e2n_cnt[ridx_cnt];
  }
}

void extract_meshgraph(const mt_vector<mt_idx_t> & eidx,
                       const mt_meshdata & mesh,
                       mt_meshgraph & graph)
{
  assert(mesh.e2n_dsp.size() > 0);

  size_t num_extr_elem = eidx.size(), num_extr_entr = 0;

  for(const mt_idx_t & e : eidx)
    num_extr_entr += mesh.e2n_cnt[e];

  graph.e2n_cnt.resize(num_extr_elem);
  graph.e2n_con.resize(num_extr_entr);
  graph.eidx.resize(num_extr_elem);
  graph.etype.resize(num_extr_elem);

  size_t widx_cnt = 0, widx_con = 0;
  for(const mt_idx_t & ridx : eidx)
  {
    graph.e2n_cnt[widx_cnt] = mesh.e2n_cnt[ridx];
    graph.etype  [widx_cnt] = mesh.etype[ridx];
    graph.eidx   [widx_cnt] = ridx;

    mt_idx_t ridx_con = mesh.e2n_dsp[ridx];
    for(mt_idx_t j=0; j<graph.e2n_cnt[widx_cnt]; j++)
      graph.e2n_con[widx_con++] = mesh.e2n_con[ridx_con++];

    widx_cnt++;
  }
}

void extract_elements(const mt_vector<mt_idx_t> & eidx,
                      const mt_meshdata & mesh,
                      mt_meshdata & outmesh)
{
  if(mesh.e2n_dsp.size() != mesh.e2n_cnt.size())
    mesh.refresh_dsp();

  size_t num_extr_elem = eidx.size(), num_extr_entr = 0;
  const short numfibvals = mesh.e2n_cnt.size() * 6 == mesh.lon.size() ? 6 : 3;

  for(const mt_idx_t & e : eidx)
    num_extr_entr += mesh.e2n_cnt[e];

  outmesh.e2n_cnt.resize(num_extr_elem);
  outmesh.e2n_con.resize(num_extr_entr);

  const bool have_type = mesh.etype.size() > 0;
  const bool have_tags = mesh.etags.size() > 0;
  const bool have_fibs = mesh.lon.size() > 0;

  if(have_type) outmesh.etype.resize(num_extr_elem);
  if(have_tags) outmesh.etags.resize(num_extr_elem, 0);
  if(have_fibs) outmesh.lon.resize(num_extr_elem * numfibvals, 0.0);

  size_t widx_cnt = 0, widx_con = 0;
  for(const mt_idx_t & ridx : eidx)
  {
    outmesh.e2n_cnt[widx_cnt] = mesh.e2n_cnt[ridx];

    if(have_type) outmesh.etype[widx_cnt] = mesh.etype[ridx];
    if(have_tags) outmesh.etags[widx_cnt] = mesh.etags[ridx];

    mt_idx_t ridx_con = mesh.e2n_dsp[ridx];
    for(mt_idx_t j=0; j<outmesh.e2n_cnt[widx_cnt]; j++)
      outmesh.e2n_con[widx_con++] = mesh.e2n_con[ridx_con++];

    if(have_fibs)
      for(short j=0; j<numfibvals; j++)
        outmesh.lon[widx_cnt*numfibvals + j] = mesh.lon[ridx*numfibvals + j];

    widx_cnt++;
  }
}

void extract_mesh(const mt_vector<mt_idx_t> & eidx,
                  const mt_meshdata & mesh,
                  mt_meshdata & outmesh)
{
  extract_elements(eidx, mesh, outmesh);
  outmesh.xyz = mesh.xyz;
}

void decompose_mesh(const mt_vector<mt_idx_t> & eidx,
                    mt_meshdata & mesh,
                    mt_meshdata & extr_mesh,
                    bool reindex)
{
  extract_elements(eidx, mesh, extr_mesh);
  extr_mesh.xyz = mesh.xyz;

  mt_vector<bool> keep(mesh.e2n_cnt.size(), true);
  for(auto v : eidx) keep[v] = false;

  mt_vector<mt_idx_t> eidx_out;
  restrict_elements(keep, mesh, eidx_out, false);

  if(reindex) {
    reindex_nodes(mesh, false);
    reindex_nodes(extr_mesh, false);
  }
}

void insert_etags(struct mt_meshdata & mesh, const mt_vector<mt_tag_t> & etags, const mt_vector<mt_idx_t> & eidx)
{
  for(size_t i=0; i<eidx.size(); i++) mesh.etags[eidx[i]] = etags[i];
}

void insert_fibers(struct mt_meshdata & mesh, const mt_vector<mt_fib_t> & lon, const mt_vector<mt_idx_t> & eidx)
{
  bool twolon = (mesh.lon.size() / 6) == mesh.e2n_cnt.size() ? true : false;

  if(twolon) {
    for(size_t i=0; i<eidx.size(); i++) {
      const mt_fib_t* read = lon.data() + i*6;
      mt_fib_t* write = mesh.lon.data() + eidx[i]*6;
      write[0] = read[0];
      write[1] = read[1];
      write[2] = read[2];
      write[3] = read[3];
      write[4] = read[4];
      write[5] = read[5];
    }
  }
  else {
    for(size_t i=0; i<eidx.size(); i++) {
      const mt_fib_t* read = lon.data() + i*3;
      mt_fib_t* write = mesh.lon.data() + eidx[i]*3;
      write[0] = read[0];
      write[1] = read[1];
      write[2] = read[2];
    }
  }
}

void insert_points(struct mt_meshdata & mesh, const mt_vector<mt_real> & xyz, const mt_vector<mt_idx_t> & nod)
{
  for(size_t i=0; i<nod.size(); i++) {
    const mt_real* read = xyz.data() + i*3;
    mt_real* write = mesh.xyz.data() + nod[i]*3;
    write[0] = read[0];
    write[1] = read[1];
    write[2] = read[2];
  }
}

void insert_surf_tri(mt_idx_t n1, mt_idx_t n2, mt_idx_t n3, size_t eidx,
                     mt_triple<mt_idx_t> & surf,
                     tri_sele & sele,
                     MT_MAP<mt_triple<mt_idx_t>, tri_sele> & surfmap)
{
  sele.v1 = n1, sele.v2 = n2, sele.v3 = n3; sele.eidx = eidx;
  sortTriple(n1, n2, n3, surf.v1, surf.v2, surf.v3);

  auto it = surfmap.find(surf);
  if(it != surfmap.end()) surfmap.erase(it);
  else surfmap[surf] = sele;
}

void insert_surf_quad(mt_idx_t n1, mt_idx_t n2, mt_idx_t n3, mt_idx_t n4, size_t eidx,
                      mt_quadruple<mt_idx_t> & surf,
                      quad_sele & sele,
                      MT_MAP<mt_quadruple<mt_idx_t>, quad_sele> & surfmap)
{
  sele.v1 = n1, sele.v2 = n2, sele.v3 = n3, sele.v4 = n4, sele.eidx = eidx;
  sortQuadruple(n1, n2, n3, n4, surf.v1, surf.v2, surf.v3, surf.v4);

  auto it = surfmap.find(surf);
  if(it != surfmap.end()) surfmap.erase(it);
  else surfmap[surf] = sele;
}

void insert_surf_tet(const mt_idx_t* nod,
                     const size_t eidx,
                     MT_MAP<mt_triple<mt_idx_t>, tri_sele> & surfmap)
{
  // surfaces are defined according to carpmanual in *clockwise* order
  // surfaces are (2,3,1) , (1,4,2) , (2,4,3) , (1,3,4)
  struct mt_triple<mt_idx_t> surf;
  struct tri_sele  sele;

  mt_idx_t n1 = nod[0], n2 = nod[1], n3 = nod[2], n4 = nod[3];

  insert_surf_tri(n2, n3, n1, eidx, surf, sele, surfmap);
  insert_surf_tri(n1, n4, n2, eidx, surf, sele, surfmap);
  insert_surf_tri(n2, n4, n3, eidx, surf, sele, surfmap);
  insert_surf_tri(n1, n3, n4, eidx, surf, sele, surfmap);
}

void insert_surf_pyr(const mt_idx_t* nod, const size_t eidx,
                     MT_MAP<mt_triple<mt_idx_t>, tri_sele> & surfmap,
                     MT_MAP<mt_quadruple<mt_idx_t>, quad_sele> & qsurfmap)
{
  // surfaces are defined according to carpmanual in *clockwise* order
  // surfaces are (1,5,2) , (2,5,3) , (3,5,4) , (4,5,1) , (1,2,3,4)
  struct mt_triple<mt_idx_t>    surf; struct mt_quadruple<mt_idx_t> qsurf;
  struct tri_sele  sele; struct quad_sele qsele;

  mt_idx_t n1 = nod[0], n2 = nod[1], n3 = nod[2], n4 = nod[3], n5 = nod[4];

  insert_surf_tri(n1, n5, n2, eidx, surf, sele, surfmap);
  insert_surf_tri(n2, n5, n3, eidx, surf, sele, surfmap);
  insert_surf_tri(n3, n5, n4, eidx, surf, sele, surfmap);
  insert_surf_tri(n4, n5, n1, eidx, surf, sele, surfmap);
  insert_surf_quad(n1, n2, n3, n4, eidx, qsurf, qsele, qsurfmap);
}

void insert_surf_pri(const mt_idx_t* nod, const size_t eidx,
                     MT_MAP<mt_triple<mt_idx_t>, tri_sele> & surfmap,
                     MT_MAP<mt_quadruple<mt_idx_t>, quad_sele> & qsurfmap)
{
  struct mt_triple<mt_idx_t> surf;
  struct mt_quadruple<mt_idx_t> qsurf;
  struct tri_sele  sele;
  struct quad_sele qsele;

  mt_idx_t n1 = nod[0], n2 = nod[1], n3 = nod[2], n4 = nod[3], n5 = nod[4], n6 = nod[5];

  // surfaces are defined according to carpmanual in *clockwise* order
  // surfaces are (1,2,3) , (4,5,6) , (1,4,6,2) , (3,2,6,5) , (1,3,5,4)
  insert_surf_tri(n1, n2, n3, eidx, surf, sele, surfmap);
  insert_surf_tri(n4, n5, n6, eidx, surf, sele, surfmap);
  insert_surf_quad(n1, n4, n6, n2, eidx, qsurf, qsele, qsurfmap);
  insert_surf_quad(n3, n2, n6, n5, eidx, qsurf, qsele, qsurfmap);
  insert_surf_quad(n1, n3, n5, n4, eidx, qsurf, qsele, qsurfmap);
}

void insert_surf_hex(const mt_idx_t* nod, const size_t eidx,
                     MT_MAP<mt_quadruple<mt_idx_t>, quad_sele> & surfmap)
{
  // surfaces are defined according to carpmanual in *clockwise* order
  // surfaces are (1,2,3,4) , (3,2,8,7) , (4,3,7,6) , (1,4,6,5) , (2,1,5,8), (6,7,8,5)
  struct mt_quadruple<mt_idx_t> qsurf;
  struct quad_sele qsele;

  mt_idx_t n1 = nod[0], n2 = nod[1], n3 = nod[2], n4 = nod[3],
         n5 = nod[4], n6 = nod[5], n7 = nod[6], n8 = nod[7];

  insert_surf_quad(n1, n2, n3, n4, eidx, qsurf, qsele, surfmap);
  insert_surf_quad(n3, n2, n8, n7, eidx, qsurf, qsele, surfmap);
  insert_surf_quad(n4, n3, n7, n6, eidx, qsurf, qsele, surfmap);
  insert_surf_quad(n1, n4, n6, n5, eidx, qsurf, qsele, surfmap);
  insert_surf_quad(n2, n1, n5, n8, eidx, qsurf, qsele, surfmap);
  insert_surf_quad(n6, n7, n8, n5, eidx, qsurf, qsele, surfmap);
}

void compute_surface_seq(const mt_meshdata & mesh,
                         const mt_vector<mt_idx_t> & elems,
                         MT_MAP<mt_triple<mt_idx_t>, tri_sele>     & tri_surf,
                         MT_MAP<mt_quadruple<mt_idx_t>, quad_sele> & quad_surf)
{
  size_t nele = mesh.e2n_cnt.size();

  mt_vector<mt_idx_t> dsp(nele);
  bucket_sort_offset(mesh.e2n_cnt, dsp);
  bool unsupported_elements = false;

  for(const mt_idx_t eidx : elems)
  {
    const mt_idx_t * nod = mesh.e2n_con.data() + dsp[eidx];
    switch(mesh.etype[eidx]) {
      case Tri: {
        mt_triple<mt_idx_t> tri;
        tri_sele trie;
        insert_surf_tri(nod[0], nod[1], nod[2], eidx, tri, trie, tri_surf);
        break;
      }
      case Quad: {
        mt_quadruple<mt_idx_t> qd;
        quad_sele qde;
        insert_surf_quad(nod[0], nod[1], nod[2], nod[3], eidx, qd, qde, quad_surf);
        break;
      }
      case Tetra:
        insert_surf_tet(nod, eidx, tri_surf);
        break;
      case Pyramid:
        insert_surf_pyr(nod, eidx, tri_surf, quad_surf);
        break;
      case Prism:
        insert_surf_pri(nod, eidx, tri_surf, quad_surf);
        break;
      case Hexa:
        insert_surf_hex(nod, eidx, quad_surf);
        break;

      default:
        unsupported_elements = true;
        break;
    }
  }

  if(unsupported_elements)
    fprintf(stderr, "%s warning: encountered unsupported element types during the surface computation!\n", __func__);
}

void compute_surface_parallel(const mt_meshdata & mesh,
                              const mt_vector<mt_idx_t> & elems,
                              MT_MAP<mt_triple<mt_idx_t>, tri_sele>     & tri_surf,
                              MT_MAP<mt_quadruple<mt_idx_t>, quad_sele> & quad_surf)
{
  size_t nele = mesh.e2n_cnt.size();
  mt_vector<mt_idx_t> dsp(nele);
  bucket_sort_offset(mesh.e2n_cnt, dsp);

  /*
   * The parallelization idea is to split the mesh along its longest axis into
   * nthreads number of blocks. Each thread computes the surface of its own block.
   * In the end, the threads merge their local surfaces into the final surface.
   *
   */

  bbox box;
  generate_bbox(mesh.xyz, box);
  bool unsupported_elements = false;

  #ifdef OPENMP
  #pragma omp parallel
  #endif
  {
    bboxAxis longest_axis     = get_longest_axis(box);
    mt_real  longest_axis_len = get_longest_axis_length(box);

    #ifdef OPENMP
    int tid = omp_get_thread_num();
    int nthreads = omp_get_num_threads();
    #else
    int tid = 0;
    int nthreads = 1;
    #endif

    mt_real blocksize = (longest_axis_len / nthreads) * 1.01;

    MT_MAP<mt_triple<mt_idx_t>, tri_sele>     loc_tri_surf;
    MT_MAP<mt_quadruple<mt_idx_t>, quad_sele> loc_quad_surf;

    for(size_t i=0; i < elems.size(); i++)
    {
      const mt_idx_t eidx = elems[i];
      const mt_idx_t * nod = mesh.e2n_con.data() + dsp[eidx];
      // vec3r ctr = barycenter(mesh.e2n_cnt[eidx], nod, mesh.xyz.data());
      vec3r ctr(mesh.xyz.data() + nod[0]*3);

      mt_real coord;
      switch(longest_axis) {
        default: break;
        case X: coord = ctr.x - box.bounds[0].x; break;
        case Y: coord = ctr.y - box.bounds[0].y; break;
        case Z: coord = ctr.z - box.bounds[0].z; break;
      }

      int associated_thread = coord / blocksize;

      if(associated_thread == tid) {
        switch(mesh.etype[eidx]) {
          case Tri: {
            mt_triple<mt_idx_t> tri;
            tri_sele trie;
            insert_surf_tri(nod[0], nod[1], nod[2], eidx, tri, trie, loc_tri_surf);
            break;
          }
          case Quad: {
            mt_quadruple<mt_idx_t> qd;
            quad_sele qde;
            insert_surf_quad(nod[0], nod[1], nod[2], nod[3], eidx, qd, qde, loc_quad_surf);
            break;
          }
          case Tetra:
            insert_surf_tet(nod, eidx, loc_tri_surf);
            break;
          case Pyramid:
            insert_surf_pyr(nod, eidx, loc_tri_surf, loc_quad_surf);
            break;
          case Prism:
            insert_surf_pri(nod, eidx, loc_tri_surf, loc_quad_surf);
            break;
          case Hexa:
            insert_surf_hex(nod, eidx, loc_quad_surf);
            break;

          default:
            unsupported_elements = true;
            break;
        }
      }
    }

    #ifdef OPENMP
    #pragma omp critical
    #endif
    {
      for(auto it = loc_tri_surf.begin(); it != loc_tri_surf.end(); ++it)
      {
        auto fit = tri_surf.find(it->first);
        if(fit != tri_surf.end())
          tri_surf.erase(fit);
        else
          tri_surf.insert(*it);
      }

      for(auto it = loc_quad_surf.begin(); it != loc_quad_surf.end(); ++it)
      {
        auto fit = quad_surf.find(it->first);
        if(fit != quad_surf.end())
          quad_surf.erase(fit);
        else
          quad_surf.insert(*it);
      }
    }
  }

  if(unsupported_elements)
    fprintf(stderr, "%s warning: encountered unsupported element types during the surface computation!\n", __func__);
}

void compute_all_surfaces_parallel(const mt_meshdata & mesh, tag_surf_map & tagsurf)
{
  auto get_surfmap_tri = [](MT_MAP<mt_tag_t, MT_MAP<mt_triple<mt_idx_t>, tri_sele>*> & ts_tri,
                            mt_tag_t tag)
  {
    MT_MAP<mt_triple<mt_idx_t>, tri_sele>* ret = NULL;
    auto it = ts_tri.find(tag);

    if(it != ts_tri.end())
      ret = it->second;
    else {
      ret = new MT_MAP<mt_triple<mt_idx_t>, tri_sele>();
      ts_tri[tag] = ret;
    }

    return ret;
  };

  auto get_surfmap_quad = [](MT_MAP<mt_tag_t, MT_MAP<mt_quadruple<mt_idx_t>, quad_sele>*> & ts_quad,
                            mt_tag_t tag)
  {
    MT_MAP<mt_quadruple<mt_idx_t>, quad_sele>* ret = NULL;
    auto it = ts_quad.find(tag);

    if(it != ts_quad.end())
      ret = it->second;
    else {
      ret = new MT_MAP<mt_quadruple<mt_idx_t>, quad_sele>();
      ts_quad[tag] = ret;
    }

    return ret;
  };

  tagsurf.clear();

  size_t nele = mesh.e2n_cnt.size();
  if(mesh.e2n_dsp.size() != mesh.e2n_cnt.size())
    mesh.refresh_dsp();

  /*
   * The parallelization idea is to split the mesh along its longest axis into
   * nthreads number of blocks. Each thread computes the surface of its own block.
   * In the end, the threads merge their local surfaces into the final surface.
   *
   */

  bbox box;
  generate_bbox(mesh.xyz, box);
  bool unsupported_elements = false;

  #ifdef OPENMP
  #pragma omp parallel
  #endif
  {
    bboxAxis longest_axis     = get_longest_axis(box);
    mt_real  longest_axis_len = get_longest_axis_length(box);

    #ifdef OPENMP
    int tid = omp_get_thread_num();
    int nthreads = omp_get_num_threads();
    #else
    int tid = 0;
    int nthreads = 1;
    #endif

    mt_real blocksize = (longest_axis_len / nthreads) * 1.01;

    MT_MAP<mt_tag_t, MT_MAP<mt_triple<mt_idx_t>, tri_sele>*>     loc_tag_surf_tri;
    MT_MAP<mt_tag_t, MT_MAP<mt_quadruple<mt_idx_t>, quad_sele>*> loc_tag_surf_quad;

    for(size_t eidx=0; eidx < nele; eidx++)
    {
      const mt_idx_t * nod = mesh.e2n_con.data() + mesh.e2n_dsp[eidx];
      // vec3r ctr = barycenter(mesh.e2n_cnt[eidx], nod, mesh.xyz.data());
      vec3r ctr(mesh.xyz.data() + nod[0]*3);

      mt_real coord;
      switch(longest_axis) {
        default: break;
        case X: coord = ctr.x - box.bounds[0].x; break;
        case Y: coord = ctr.y - box.bounds[0].y; break;
        case Z: coord = ctr.z - box.bounds[0].z; break;
      }

      int associated_thread = coord / blocksize;

      if(associated_thread == tid) {
        MT_MAP<mt_triple<mt_idx_t>, tri_sele>    & loc_tri_surf  = *get_surfmap_tri (loc_tag_surf_tri,  mesh.etags[eidx]);
        MT_MAP<mt_quadruple<mt_idx_t>, quad_sele>& loc_quad_surf = *get_surfmap_quad(loc_tag_surf_quad, mesh.etags[eidx]);

        switch(mesh.etype[eidx]) {
          case Tri: {
            mt_triple<mt_idx_t> tri;
            tri_sele trie;
            insert_surf_tri(nod[0], nod[1], nod[2], eidx, tri, trie, loc_tri_surf);
            break;
          }
          case Quad: {
            mt_quadruple<mt_idx_t> qd;
            quad_sele qde;
            insert_surf_quad(nod[0], nod[1], nod[2], nod[3], eidx, qd, qde, loc_quad_surf);
            break;
          }
          case Tetra:
            insert_surf_tet(nod, eidx, loc_tri_surf);
            break;
          case Pyramid:
            insert_surf_pyr(nod, eidx, loc_tri_surf, loc_quad_surf);
            break;
          case Prism:
            insert_surf_pri(nod, eidx, loc_tri_surf, loc_quad_surf);
            break;
          case Hexa:
            insert_surf_hex(nod, eidx, loc_quad_surf);
            break;

          default:
            unsupported_elements = true;
            break;
        }
      }
    }

    #ifdef OPENMP
    #pragma omp critical
    #endif
    {
      for(auto & tit : loc_tag_surf_tri) {
        MT_MAP<mt_triple<mt_idx_t>, tri_sele> & tri_surf     = *get_surfmap_tri(tagsurf.tri, tit.first);
        MT_MAP<mt_triple<mt_idx_t>, tri_sele> & loc_tri_surf = *tit.second;

        for(auto it = loc_tri_surf.begin(); it != loc_tri_surf.end(); ++it)
        {
          auto fit = tri_surf.find(it->first);
          if(fit != tri_surf.end())
            tri_surf.erase(fit);
          else
            tri_surf.insert(*it);
        }
      }

      for(auto & tit : loc_tag_surf_quad) {
        MT_MAP<mt_quadruple<mt_idx_t>, quad_sele> & quad_surf     = *get_surfmap_quad(tagsurf.quad, tit.first);
        MT_MAP<mt_quadruple<mt_idx_t>, quad_sele> & loc_quad_surf = *tit.second;

        for(auto it = loc_quad_surf.begin(); it != loc_quad_surf.end(); ++it) {
          auto fit = quad_surf.find(it->first);
          if(fit != quad_surf.end())
            quad_surf.erase(fit);
          else
            quad_surf.insert(*it);
        }
      }
    }

    for(auto & tit : loc_tag_surf_tri)
      delete tit.second;
    for(auto & tit : loc_tag_surf_quad)
      delete tit.second;
  }

  if(unsupported_elements)
    fprintf(stderr, "%s warning: encountered unsupported element types during the surface computation!\n", __func__);
}

void update_surfaces_parallel(const mt_meshdata & mesh, const MT_USET<mt_tag_t> & tags, tag_surf_map & tagsurf)
{
  auto get_surfmap_tri = [](MT_MAP<mt_tag_t, MT_MAP<mt_triple<mt_idx_t>, tri_sele>*> & ts_tri,
                            mt_tag_t tag)
  {
    MT_MAP<mt_triple<mt_idx_t>, tri_sele>* ret = NULL;
    auto it = ts_tri.find(tag);

    if(it != ts_tri.end())
      ret = it->second;
    else {
      ret = new MT_MAP<mt_triple<mt_idx_t>, tri_sele>();
      ts_tri[tag] = ret;
    }

    return ret;
  };

  auto get_surfmap_quad = [](MT_MAP<mt_tag_t, MT_MAP<mt_quadruple<mt_idx_t>, quad_sele>*> & ts_quad,
                            mt_tag_t tag)
  {
    MT_MAP<mt_quadruple<mt_idx_t>, quad_sele>* ret = NULL;
    auto it = ts_quad.find(tag);

    if(it != ts_quad.end())
      ret = it->second;
    else {
      ret = new MT_MAP<mt_quadruple<mt_idx_t>, quad_sele>();
      ts_quad[tag] = ret;
    }

    return ret;
  };

  tagsurf.clear(tags);

  size_t nele = mesh.e2n_cnt.size();
  if(mesh.e2n_dsp.size() != mesh.e2n_cnt.size())
    mesh.refresh_dsp();

  bbox box;
  generate_bbox(mesh.xyz, box);
  bool unsupported_elements = false;

  #ifdef OPENMP
  #pragma omp parallel
  #endif
  {
    bboxAxis longest_axis     = get_longest_axis(box);
    mt_real  longest_axis_len = get_longest_axis_length(box);

    #ifdef OPENMP
    int tid = omp_get_thread_num();
    int nthreads = omp_get_num_threads();
    #else
    int tid = 0;
    int nthreads = 1;
    #endif

    mt_real blocksize = (longest_axis_len / nthreads) * 1.01;

    MT_MAP<mt_tag_t, MT_MAP<mt_triple<mt_idx_t>, tri_sele>*>     loc_tag_surf_tri;
    MT_MAP<mt_tag_t, MT_MAP<mt_quadruple<mt_idx_t>, quad_sele>*> loc_tag_surf_quad;

    for(size_t eidx=0; eidx < nele; eidx++)
    {
      if(tags.count(mesh.etags[eidx]) == 0) continue;

      const mt_idx_t * nod = mesh.e2n_con.data() + mesh.e2n_dsp[eidx];
      // vec3r ctr = barycenter(mesh.e2n_cnt[eidx], nod, mesh.xyz.data());
      vec3r ctr(mesh.xyz.data() + nod[0]*3);

      mt_real coord;
      switch(longest_axis) {
        default: break;
        case X: coord = ctr.x - box.bounds[0].x; break;
        case Y: coord = ctr.y - box.bounds[0].y; break;
        case Z: coord = ctr.z - box.bounds[0].z; break;
      }

      int associated_thread = coord / blocksize;

      if(associated_thread == tid) {
        MT_MAP<mt_triple<mt_idx_t>, tri_sele>    & loc_tri_surf  = *get_surfmap_tri (loc_tag_surf_tri,  mesh.etags[eidx]);
        MT_MAP<mt_quadruple<mt_idx_t>, quad_sele>& loc_quad_surf = *get_surfmap_quad(loc_tag_surf_quad, mesh.etags[eidx]);

        switch(mesh.etype[eidx]) {
          case Tri: {
            mt_triple<mt_idx_t> tri;
            tri_sele trie;
            insert_surf_tri(nod[0], nod[1], nod[2], eidx, tri, trie, loc_tri_surf);
            break;
          }
          case Quad: {
            mt_quadruple<mt_idx_t> qd;
            quad_sele qde;
            insert_surf_quad(nod[0], nod[1], nod[2], nod[3], eidx, qd, qde, loc_quad_surf);
            break;
          }
          case Tetra:
            insert_surf_tet(nod, eidx, loc_tri_surf);
            break;
          case Pyramid:
            insert_surf_pyr(nod, eidx, loc_tri_surf, loc_quad_surf);
            break;
          case Prism:
            insert_surf_pri(nod, eidx, loc_tri_surf, loc_quad_surf);
            break;
          case Hexa:
            insert_surf_hex(nod, eidx, loc_quad_surf);
            break;

          default:
            unsupported_elements = true;
            break;
        }
      }
    }

    #ifdef OPENMP
    #pragma omp critical
    #endif
    {
      for(auto & tit : loc_tag_surf_tri) {
        MT_MAP<mt_triple<mt_idx_t>, tri_sele> & tri_surf     = *get_surfmap_tri(tagsurf.tri, tit.first);
        MT_MAP<mt_triple<mt_idx_t>, tri_sele> & loc_tri_surf = *tit.second;

        for(auto it = loc_tri_surf.begin(); it != loc_tri_surf.end(); ++it)
        {
          auto fit = tri_surf.find(it->first);
          if(fit != tri_surf.end())
            tri_surf.erase(fit);
          else
            tri_surf.insert(*it);
        }
      }

      for(auto & tit : loc_tag_surf_quad) {
        MT_MAP<mt_quadruple<mt_idx_t>, quad_sele> & quad_surf     = *get_surfmap_quad(tagsurf.quad, tit.first);
        MT_MAP<mt_quadruple<mt_idx_t>, quad_sele> & loc_quad_surf = *tit.second;

        for(auto it = loc_quad_surf.begin(); it != loc_quad_surf.end(); ++it) {
          auto fit = quad_surf.find(it->first);
          if(fit != quad_surf.end())
            quad_surf.erase(fit);
          else
            quad_surf.insert(*it);
        }
      }
    }

    for(auto & tit : loc_tag_surf_tri)
      delete tit.second;
    for(auto & tit : loc_tag_surf_quad)
      delete tit.second;
  }

  if(unsupported_elements)
    fprintf(stderr, "%s warning: encountered unsupported element types during the surface computation!\n", __func__);
}

void compute_surface(const mt_meshdata & mesh,
                     const MT_USET<mt_tag_t> & tags,
                     MT_MAP<mt_triple<mt_idx_t>, tri_sele>     & tri_surf,
                     MT_MAP<mt_quadruple<mt_idx_t>, quad_sele> & quad_surf)
{
  mt_vector<mt_idx_t> elems;
  elems.reserve(mesh.etags.size());

  for(size_t eidx = 0; eidx < mesh.e2n_cnt.size(); eidx++) {
    if(tags.count(mesh.etags[eidx]))
      elems.push_back(mt_idx_t(eidx));
  }

#ifdef OPENMP
  compute_surface_parallel(mesh, elems, tri_surf, quad_surf);
#else
  compute_surface_seq(mesh, elems, tri_surf, quad_surf);
#endif
}

void compute_surface(const mt_meshdata & mesh, const tag_surf_map & tagsurf,
                     const MT_USET<mt_tag_t> & tags, mt_meshdata & surface,
                     mt_vector<mt_idx_t> & elem_orig)
{
  auto get_surfmap_tri = [](const MT_MAP<mt_tag_t, MT_MAP<mt_triple<mt_idx_t>, tri_sele>*> & ts_tri,
                            mt_tag_t tag)
  {
    MT_MAP<mt_triple<mt_idx_t>, tri_sele>* ret = NULL;
    auto it = ts_tri.find(tag);

    if(it != ts_tri.end())
      ret = it->second;

    return ret;
  };

  auto get_surfmap_quad = [](const MT_MAP<mt_tag_t, MT_MAP<mt_quadruple<mt_idx_t>, quad_sele>*> & ts_quad,
                            mt_tag_t tag)
  {
    MT_MAP<mt_quadruple<mt_idx_t>, quad_sele>* ret = NULL;
    auto it = ts_quad.find(tag);

    if(it != ts_quad.end())
      ret = it->second;

    return ret;
  };

  MT_MAP<mt_triple<mt_idx_t>, tri_sele>     tri_surf;
  MT_MAP<mt_quadruple<mt_idx_t>, quad_sele> quad_surf;

  for(mt_tag_t t : tags) {
    MT_MAP<mt_triple<mt_idx_t>, tri_sele>     & ts = *get_surfmap_tri (tagsurf.tri,  t);
    MT_MAP<mt_quadruple<mt_idx_t>, quad_sele> & qs = *get_surfmap_quad(tagsurf.quad, t);

    for(auto & tit : ts) {
      auto fit = tri_surf.find(tit.first);
      if(fit != tri_surf.end())
        tri_surf.erase(fit);
      else
        tri_surf.insert(tit);
    }

    for(auto & tit : qs) {
      auto fit = quad_surf.find(tit.first);
      if(fit != quad_surf.end())
        quad_surf.erase(fit);
      else
        quad_surf.insert(tit);
    }
  }

  // we compute a purely triangular surface
  surfmap_to_vector(tri_surf, quad_surf, surface.e2n_con, elem_orig);
  size_t num_tri = surface.e2n_con.size() / 3;
  surface.e2n_cnt.assign(num_tri, 3);
  surface.etype  .assign(num_tri, Tri);
}

void compute_surface(const mt_meshdata & mesh,
                     const MT_USET<mt_tag_t> & tags,
                     mt_meshdata & surfmesh,
                     const bool full_mesh)
{
  MT_MAP<mt_triple<mt_idx_t>, tri_sele>  tri_surf;
  MT_MAP<mt_quadruple<mt_idx_t>, quad_sele> quad_surf;
  mt_vector<mt_idx_t> elem_orig;

  compute_surface(mesh, tags, tri_surf, quad_surf);

  if(full_mesh)
    surfmap_to_surfmesh(tri_surf, quad_surf, surfmesh, elem_orig);
  else
    surfmap_to_vector(tri_surf, quad_surf, surfmesh.e2n_con, elem_orig);
}

void compute_surface(const mt_meshdata & mesh,
                     const mt_vector<mt_idx_t> & elems,
                     MT_MAP<mt_triple<mt_idx_t>, tri_sele>     & tri_surf,
                     MT_MAP<mt_quadruple<mt_idx_t>, quad_sele> & quad_surf)
{
#ifdef OPENMP
  compute_surface_parallel(mesh, elems, tri_surf, quad_surf);
#else
  compute_surface_seq(mesh, elems, tri_surf, quad_surf);
#endif
}

void compute_surface(const mt_meshdata & mesh,
                     const mt_vector<mt_idx_t> & elems,
                     mt_meshdata & surfmesh,
                     const bool full_mesh,
                     mt_vector<mt_idx_t>* elem_orig)
{
  MT_MAP<mt_triple<mt_idx_t>, tri_sele>  tri_surf;
  MT_MAP<mt_quadruple<mt_idx_t>, quad_sele> quad_surf;

  mt_vector<mt_idx_t> loc_eorig;
  mt_vector<mt_idx_t> & eorig = elem_orig ? *elem_orig : loc_eorig;

  compute_surface(mesh, elems, tri_surf, quad_surf);

  if(full_mesh)
    surfmap_to_surfmesh(tri_surf, quad_surf, surfmesh, eorig);
  else
    surfmap_to_vector(tri_surf, quad_surf, surfmesh.e2n_con, eorig);
}

void surfmap_to_vector(const MT_MAP<struct mt_triple<mt_idx_t>, struct tri_sele> & tri,
                       const MT_MAP<struct mt_quadruple<mt_idx_t>, struct quad_sele> & quad,
                       mt_vector<mt_idx_t> & surf_con,
                       mt_vector<mt_idx_t> & elem_origin)
{
   typename MT_MAP<struct mt_triple<mt_idx_t>, struct tri_sele>::const_iterator tri_it = tri.begin();
   typename MT_MAP<struct mt_quadruple<mt_idx_t>, struct quad_sele>::const_iterator quad_it = quad.begin();

   surf_con.resize(tri.size()*3 + quad.size()*6);
   elem_origin.resize(tri.size() + quad.size()*2);

   mt_idx_t sidx = 0, eidx = 0;

   while(tri_it != tri.end()) {
     const struct tri_sele& val = tri_it->second;
     surf_con[sidx+0] = val.v1;
     surf_con[sidx+1] = val.v2;
     surf_con[sidx+2] = val.v3;

     elem_origin[eidx++] = tri_it->second.eidx;

     sidx+=3;
     ++tri_it;
   }
   while(quad_it != quad.end()) {
     const struct quad_sele& val = quad_it->second;
     surf_con[sidx+0] = val.v1;
     surf_con[sidx+1] = val.v2;
     surf_con[sidx+2] = val.v3;

     elem_origin[eidx++] = quad_it->second.eidx;

     surf_con[sidx+3] = val.v1;
     surf_con[sidx+4] = val.v3;
     surf_con[sidx+5] = val.v4;

     elem_origin[eidx++] = quad_it->second.eidx;

     sidx+=6;
     ++quad_it;
   }
}

void surfmap_to_surfmesh(const MT_MAP<struct mt_triple<mt_idx_t>, struct tri_sele> & tri,
                         const MT_MAP<struct mt_quadruple<mt_idx_t>, struct quad_sele> & quad,
                         mt_meshdata & surf_mesh,
                         mt_vector<mt_idx_t> & elem_origin)
{
  surf_mesh.e2n_cnt.assign(tri.size(), 3);
  surf_mesh.etype  .assign(tri.size(), Tri);
  surf_mesh.etags  .assign(tri.size(), mt_tag_t(0));

  surf_mesh.e2n_cnt.resize(tri.size() + quad.size(), 4);
  surf_mesh.etype  .resize(tri.size() + quad.size(), Quad);
  surf_mesh.etags  .resize(tri.size() + quad.size(), mt_idx_t(0));

  elem_origin      .resize(surf_mesh.e2n_cnt.size());
  surf_mesh.e2n_con.resize(tri.size()*3 + quad.size()*4);

  auto tri_it = tri.begin();
  for(size_t k=0; k<tri.size(); k++) {
    surf_mesh.e2n_con[k*3+0] = tri_it->second.v1;
    surf_mesh.e2n_con[k*3+1] = tri_it->second.v2;
    surf_mesh.e2n_con[k*3+2] = tri_it->second.v3;

    elem_origin[k] = tri_it->second.eidx;

    ++tri_it;
  }

  auto quad_it = quad.begin();
  for(size_t k=0, dsp = tri.size()*3; k<quad.size(); k++) {
    surf_mesh.e2n_con[dsp + k*4+0] = quad_it->second.v1;
    surf_mesh.e2n_con[dsp + k*4+1] = quad_it->second.v2;
    surf_mesh.e2n_con[dsp + k*4+2] = quad_it->second.v3;
    surf_mesh.e2n_con[dsp + k*4+3] = quad_it->second.v4;

    elem_origin[tri.size()+k] = quad_it->second.eidx;

    ++quad_it;
  }
}

void vector_to_surfmap(const mt_vector<mt_idx_t> & surf_con,
                       MT_MAP<struct mt_triple<mt_idx_t>, struct tri_sele> & trimap)
{
  size_t num_tri = surf_con.size() / 3;
  mt_triple<mt_idx_t> stri;
  tri_sele       tri;

  for(size_t i=0; i<num_tri; i++) {
    tri.v1 = surf_con[i*3+0];
    tri.v2 = surf_con[i*3+1];
    tri.v3 = surf_con[i*3+2];
    tri.eidx = i;

    sortTriple(tri.v1, tri.v2, tri.v3, stri.v1, stri.v2, stri.v3);
    trimap[stri] = tri;
  }
}

void surfmesh_to_surfmap(const mt_meshdata & surfmesh,
                         const mt_vector<mt_idx_t> & eidx,
                         MT_MAP<mt_triple<mt_idx_t>, tri_sele> & tri,
                         MT_MAP<mt_quadruple<mt_idx_t>, quad_sele> & quad)
{
  size_t numele = surfmesh.e2n_cnt.size();
  mt_triple<mt_idx_t> stri; mt_quadruple<mt_idx_t> squad;
  tri_sele tri_ele; quad_sele quad_ele;

  check_nonzero(surfmesh.etype.size(), __func__);
  const mt_idx_t* nod = surfmesh.e2n_con.data();

  for(size_t i=0; i<numele; i++)
  {
    switch(surfmesh.etype[i]) {
      case Tri: {
        insert_surf_tri(nod[0], nod[1], nod[2], eidx[i], stri, tri_ele, tri);
        break;
      }

      case Quad: {
        insert_surf_quad(nod[0], nod[1], nod[2], nod[3], eidx[i], squad, quad_ele, quad);
        break;
      }

      default:
        fprintf(stderr, "%s error: Non-surface element!\n", __func__);
        exit(1);
    }

    nod += surfmesh.e2n_cnt[i];
  }
}


void surface_difference(MT_MAP<mt_triple<mt_idx_t>, tri_sele> & lhs_tri,
                        MT_MAP<mt_quadruple<mt_idx_t>, quad_sele> & lhs_quad,
                        const MT_MAP<mt_triple<mt_idx_t>, tri_sele> & rhs_tri,
                        const MT_MAP<mt_quadruple<mt_idx_t>, quad_sele> & rhs_quad)
{
   typename MT_MAP<mt_triple<mt_idx_t>, tri_sele>::const_iterator tri_it = rhs_tri.begin();
   typename MT_MAP<mt_quadruple<mt_idx_t>, quad_sele>::const_iterator quad_it = rhs_quad.begin();

   // remove elements of rhs from lhs
   while(tri_it != rhs_tri.end()) {
     typename MT_MAP<mt_triple<mt_idx_t>, tri_sele>::iterator it = lhs_tri.find(tri_it->first);
     if(it != lhs_tri.end()) lhs_tri.erase(it);
     ++tri_it;
   }
   while(quad_it != rhs_quad.end()) {
     typename MT_MAP<mt_quadruple<mt_idx_t>, quad_sele>::iterator it = lhs_quad.find(quad_it->first);
     if(it != lhs_quad.end()) lhs_quad.erase(it);
     ++quad_it;
   }
}

void surface_intersection(MT_MAP<mt_triple<mt_idx_t>, tri_sele> & lhs_tri,
                          MT_MAP<mt_quadruple<mt_idx_t>, quad_sele> & lhs_quad,
                          const MT_MAP<mt_triple<mt_idx_t>, tri_sele> & rhs_tri,
                          const MT_MAP<mt_quadruple<mt_idx_t>, quad_sele> & rhs_quad)
{
   typename MT_MAP<mt_triple<mt_idx_t>, tri_sele>::iterator tri_it = lhs_tri.begin();
   typename MT_MAP<mt_quadruple<mt_idx_t>, quad_sele>::iterator quad_it = lhs_quad.begin();

   // iterate over lhs and remove elements which do not also exist in rhs
   while(tri_it != lhs_tri.end()) {
     if(rhs_tri.count(tri_it->first) == 0) {
       typename MT_MAP<mt_triple<mt_idx_t>, tri_sele>::iterator it = tri_it;
       ++tri_it;
       lhs_tri.erase(it);
     }
     else ++tri_it;
   }
   while(quad_it != lhs_quad.end()) {
     if(rhs_quad.count(quad_it->first) == 0) {
       typename MT_MAP<mt_quadruple<mt_idx_t>, quad_sele>::iterator it = quad_it;
       ++quad_it;
       lhs_quad.erase(it);
     }
     else ++quad_it;
   }
}

void surface_union(MT_MAP<mt_triple<mt_idx_t>, tri_sele> & lhs_tri,
                   MT_MAP<mt_quadruple<mt_idx_t>, quad_sele> & lhs_quad,
                   const MT_MAP<mt_triple<mt_idx_t>, tri_sele> & rhs_tri,
                   const MT_MAP<mt_quadruple<mt_idx_t>, quad_sele> & rhs_quad)
{
  typename MT_MAP<mt_triple<mt_idx_t>, tri_sele>::const_iterator     tri_it  = rhs_tri.begin();
  typename MT_MAP<mt_quadruple<mt_idx_t>, quad_sele>::const_iterator quad_it = rhs_quad.begin();

  // iterate over rhs and add its elements to lhs
  while(tri_it != rhs_tri.end()) {
    lhs_tri.insert(*tri_it);
    ++tri_it;
  }

  while(quad_it != rhs_quad.end()) {
    lhs_quad.insert(*quad_it);
    ++quad_it;
  }
}

void generate_nbc_data(const mt_meshdata & mesh,
                       const mt_meshdata & surf,
                       const mt_vector<mt_idx_t> & elem_orig,
                       struct nbc_data & nbc)
{
  size_t ssize = surf.e2n_cnt.size();
  nbc.eidx.resize(ssize); nbc.sp_vtx.resize(ssize); nbc.tag.resize(ssize);

  for(size_t i = 0; i<ssize; i++) {
    mt_idx_t mesh_eidx = elem_orig[i];

    const mt_idx_t* s = surf.e2n_con.data() + surf.e2n_dsp[i];
    const mt_idx_t* e = mesh.e2n_con.data() + mesh.e2n_dsp[mesh_eidx];

    MT_USET<mt_idx_t> si;
    si.insert(s, s + surf.e2n_cnt[i]);

    mt_idx_t v = -1;
    for(mt_idx_t j = 0; j < mesh.e2n_cnt[mesh_eidx]; j++)
      if(si.count(e[j]) == 0) {
        v = e[j];
        break;
      }

    // now we can write the nbc data
    nbc.sp_vtx[i] = v;
    nbc.eidx[i]   = mesh_eidx;
    nbc.tag[i]    = mesh.etags[mesh_eidx];
  }
}

void unified_surface_from_tags(const mt_meshdata & mesh,
                               const mt_vector<MT_USET<mt_tag_t>> & tags,
                               mt_meshdata & surfmesh,
                               MT_USET<mt_idx_t> * vtx_set)
{
  MT_MAP<mt_triple<mt_idx_t>, tri_sele>     full_trimap;
  MT_MAP<mt_quadruple<mt_idx_t>, quad_sele> full_quadmap;

  for(size_t i=0; i<tags.size(); i++)
  {
    MT_MAP<mt_triple<mt_idx_t>, tri_sele>     cur_trimap;
    MT_MAP<mt_quadruple<mt_idx_t>, quad_sele> cur_quadmap;

    if(vtx_set != NULL) {
      mt_vector<mt_idx_t> elem_vec;
      MT_USET<mt_idx_t>   elem_set;

      for(size_t e=0; e<mesh.etags.size(); e++)
        if(tags[i].count(mesh.etags[e]))
          elem_set.insert(e);

      elemSet_to_nodeSet(mesh, elem_set, *vtx_set);

      elem_vec.assign(elem_set.begin(), elem_set.end());
      compute_surface(mesh, elem_vec, cur_trimap, cur_quadmap);
    }
    else {
      compute_surface(mesh, tags[i], cur_trimap, cur_quadmap);
    }

    surface_union(full_trimap, full_quadmap, cur_trimap, cur_quadmap);
  }
  mt_vector<mt_idx_t> elem_orig;
  surfmap_to_vector(full_trimap, full_quadmap, surfmesh.e2n_con, elem_orig);
  surfmesh.e2n_cnt.assign(surfmesh.e2n_con.size() / 3, 3);
  surfmesh.etype.assign(surfmesh.e2n_cnt.size(), Tri);

  surfmesh.etags.resize(surfmesh.etype.size());
  for(size_t i=0; i<elem_orig.size(); i++)
    surfmesh.etags[i] = mesh.etags[elem_orig[i]];

  #if 0
  surfmesh.xyz = mesh.xyz;
  write_mesh_selected(surfmesh, "vtk", "debug_surfmesh");
  #endif
}

void unified_surface_from_list(const std::string & list,
                               const char delimiter,
                               mt_meshdata & surfmesh)
{
  struct mt_triple<mt_idx_t> surf;
  struct tri_sele       sele;
  MT_MAP<mt_triple<mt_idx_t>, tri_sele>     trimap;
  MT_MAP<mt_quadruple<mt_idx_t>, quad_sele> quadmap;
  MT_MAP<mt_triple<mt_idx_t>, tri_sele>::iterator it;

  mt_vector<std::string> surfs;
  split_string(list, delimiter, surfs);

  for(size_t i=0; i<surfs.size(); i++)
  {
    fixBasename(surfs[i]);
    std::string curr_surf_file = surfs[i] + SURF_EXT;
    std::cout << "Reading surface: " << curr_surf_file << std::endl;

    mt_meshdata curr_surf;
    readElements(curr_surf, curr_surf_file);
    size_t nele = curr_surf.e2n_cnt.size();

    // generate unique surface
    for(size_t eidx=0; eidx < nele; eidx++)
    {
      mt_idx_t n1 = curr_surf.e2n_con[eidx*3+0];
      mt_idx_t n2 = curr_surf.e2n_con[eidx*3+1];
      mt_idx_t n3 = curr_surf.e2n_con[eidx*3+2];

      sele.v1 = n1, sele.v2 = n2, sele.v3 = n3; sele.eidx = eidx;
      sortTriple(n1, n2, n3, surf.v1, surf.v2, surf.v3);

      it = trimap.find(surf);
      if(it == trimap.end()) trimap[surf] = sele;
    }
  }
  mt_vector<mt_idx_t> elem_orig;
  surfmap_to_vector(trimap, quadmap, surfmesh.e2n_con, elem_orig);
  surfmesh.e2n_cnt.assign(surfmesh.e2n_con.size() / 3, 3);
  surfmesh.etype.assign(surfmesh.e2n_cnt.size(), Tri);
  surfmesh.etags.assign(surfmesh.e2n_cnt.size(), mt_tag_t(0));
}

void mesh_resize_elemdata(mt_meshdata & mesh, size_t size_elem, size_t size_con)
{
  bool twoFib = mesh.lon.size() == (mesh.e2n_cnt.size() * 6);

  mesh.e2n_cnt.resize(size_elem);
  mesh.e2n_con.resize(size_con);

  mesh.etype.resize(size_elem);
  mesh.etags.resize(size_elem);
  mesh.lon  .resize(twoFib ? size_elem * 6 : size_elem * 3);
}

void mesh_add_elem(mt_meshdata & mesh, const elem_t type, const mt_idx_t* con, const mt_tag_t tag)
{
  short numfibvals = 3;
  mt_idx_t numcon = -1;

  switch(type) {
    case Node:
      numcon = 1;
      break;

    case Line:
      numcon = 2;
      break;

    case Tri:
      numcon = 3;
      break;

    case Quad:
    case Tetra:
      numcon = 4;
      break;

    case Pyramid:
      numcon = 5;
      break;

    case Octa:
    case Prism:
      numcon = 6;
      break;

    case Hexa:
      numcon = 8;
      break;

    default: break;
  }

  mesh.e2n_cnt.push_back(numcon);
  mesh.etype.push_back(type);
  mesh.etags.push_back(tag);

  for(mt_idx_t i=0; i<numcon; i++)
    mesh.e2n_con.push_back(con[i]);

  for(short i=0; i<numfibvals; i++)
    mesh.lon.push_back(0.0);
}

void mesh_add_elem(mt_meshdata & mesh, const elem_t type, const mt_idx_t* con, const mt_tag_t tag, short nfib, const mt_fib_t* fib)
{
  mt_idx_t numcon = -1;

  switch(type) {
    case Node:
      numcon = 1;
      break;

    case Line:
      numcon = 2;
      break;

    case Tri:
      numcon = 3;
      break;

    case Quad:
    case Tetra:
      numcon = 4;
      break;

    case Pyramid:
      numcon = 5;
      break;

    case Octa:
    case Prism:
      numcon = 6;
      break;

    case Hexa:
      numcon = 8;
      break;

    default: break;
  }

  mesh.e2n_cnt.push_back(numcon);
  mesh.etype.push_back(type);
  mesh.etags.push_back(tag);

  for(mt_idx_t i=0; i<numcon; i++)
    mesh.e2n_con.push_back(con[i]);

  for(short i=0; i<nfib; i++)
    mesh.lon.push_back(fib[i]);
}

void surface_to_mesh(const mt_meshdata & mesh,
                     const mt_vector<mt_idx_t> & surf_con,
                     const nbc_data & nbc,
                     mt_meshdata & surfmesh)
{
  bool lon2 = mesh.lon.size() == mesh.e2n_cnt.size()*6;
  size_t nelem = surf_con.size() / 3;

  // make sure we have proper nbc data
  assert(nelem = nbc.eidx.size());

  // all elements are triangles
  surfmesh.e2n_cnt.assign(nelem, 3);
  surfmesh.etype.assign(nelem, Tri);
  surfmesh.etags.assign(nbc.tag.begin(), nbc.tag.end());

#if 0
  surfmesh.e2n_con.assign(surf_con.begin(), surf_con.end());
#else
  surfmesh.e2n_con.resize(surf_con.size());
  // flip surface connectivity to have outward facing normals
  for(size_t i=0; i<nelem; i++) {
    surfmesh.e2n_con[i*3+0] = surf_con[i*3+2];
    surfmesh.e2n_con[i*3+1] = surf_con[i*3+1];
    surfmesh.e2n_con[i*3+2] = surf_con[i*3+0];
  }
#endif

  mt_vector<mt_idx_t> nod(surf_con);
  binary_sort(nod); unique_resize(nod);
  nod.shrink_to_fit();

  // remap surface indices
  map_glob2loc(nod, surfmesh.e2n_con);

  // copy vertex coords
  size_t nnodes = nod.size();
  surfmesh.xyz.resize(nnodes*3);
  for(size_t i=0; i<nnodes; i++) {
    mt_idx_t n = nod[i];
    surfmesh.xyz[i*3+0] = mesh.xyz[n*3+0];
    surfmesh.xyz[i*3+1] = mesh.xyz[n*3+1];
    surfmesh.xyz[i*3+2] = mesh.xyz[n*3+2];
  }

  // copy fibers
  surfmesh.lon.resize( lon2 ? nelem*6 : nelem*3 );
  if(lon2) {
    for(size_t i=0; i<nelem; i++) {
      mt_idx_t eidx = nbc.eidx[i];
      surfmesh.lon[i*6+0] = mesh.lon[eidx*6+0];
      surfmesh.lon[i*6+1] = mesh.lon[eidx*6+1];
      surfmesh.lon[i*6+2] = mesh.lon[eidx*6+2];
      surfmesh.lon[i*6+3] = mesh.lon[eidx*6+3];
      surfmesh.lon[i*6+4] = mesh.lon[eidx*6+4];
      surfmesh.lon[i*6+5] = mesh.lon[eidx*6+5];
    }
  }
  else {
    for(size_t i=0; i<nelem; i++) {
      mt_idx_t eidx = nbc.eidx[i];
      surfmesh.lon[i*3+0] = mesh.lon[eidx*3+0];
      surfmesh.lon[i*3+1] = mesh.lon[eidx*3+1];
      surfmesh.lon[i*3+2] = mesh.lon[eidx*3+2];
    }
  }
}

void nbc_data_into_surface(const mt_meshdata & mesh, const nbc_data & nbc, mt_meshdata & surfmesh, bool flip)
{
  bool lon2 = mesh.lon.size() == mesh.e2n_cnt.size()*6;
  size_t nelem = surfmesh.e2n_cnt.size();

  // make sure we have proper nbc data
  assert(nelem == nbc.eidx.size());
  surfmesh.etags.assign(nbc.tag.begin(), nbc.tag.end());

  if(flip) {
    // flip surface connectivity to have outward facing normals in meshalyzer
    for(size_t i=0, ridx=0, widx=0; i<nelem; i++) {
      switch(surfmesh.e2n_cnt[i]) {
        case 3:
        {
          mt_idx_t v1 = surfmesh.e2n_con[ridx++];
          mt_idx_t v2 = surfmesh.e2n_con[ridx++];
          mt_idx_t v3 = surfmesh.e2n_con[ridx++];
          surfmesh.e2n_con[widx++] = v3;
          surfmesh.e2n_con[widx++] = v2;
          surfmesh.e2n_con[widx++] = v1;
          break;
        }

        case 4: {
          mt_idx_t v1 = surfmesh.e2n_con[ridx++];
          mt_idx_t v2 = surfmesh.e2n_con[ridx++];
          mt_idx_t v3 = surfmesh.e2n_con[ridx++];
          mt_idx_t v4 = surfmesh.e2n_con[ridx++];
          surfmesh.e2n_con[widx++] = v4;
          surfmesh.e2n_con[widx++] = v3;
          surfmesh.e2n_con[widx++] = v2;
          surfmesh.e2n_con[widx++] = v1;
          break;
        }
      }
    }
  }

  mt_vector<mt_idx_t> nod(surfmesh.e2n_con);
  binary_sort(nod); unique_resize(nod);
  nod.shrink_to_fit();

  // remap surface indices
  map_glob2loc(nod, surfmesh.e2n_con);

  // copy vertex coords
  size_t nnodes = nod.size();
  surfmesh.xyz.resize(nnodes*3);
  for(size_t i=0; i<nnodes; i++) {
    mt_idx_t n = nod[i];
    surfmesh.xyz[i*3+0] = mesh.xyz[n*3+0];
    surfmesh.xyz[i*3+1] = mesh.xyz[n*3+1];
    surfmesh.xyz[i*3+2] = mesh.xyz[n*3+2];
  }

  // copy fibers
  surfmesh.lon.resize( lon2 ? nelem*6 : nelem*3 );
  if(lon2) {
    for(size_t i=0; i<nelem; i++) {
      mt_idx_t eidx = nbc.eidx[i];
      surfmesh.lon[i*6+0] = mesh.lon[eidx*6+0];
      surfmesh.lon[i*6+1] = mesh.lon[eidx*6+1];
      surfmesh.lon[i*6+2] = mesh.lon[eidx*6+2];
      surfmesh.lon[i*6+3] = mesh.lon[eidx*6+3];
      surfmesh.lon[i*6+4] = mesh.lon[eidx*6+4];
      surfmesh.lon[i*6+5] = mesh.lon[eidx*6+5];
    }
  }
  else {
    for(size_t i=0; i<nelem; i++) {
      mt_idx_t eidx = nbc.eidx[i];
      surfmesh.lon[i*3+0] = mesh.lon[eidx*3+0];
      surfmesh.lon[i*3+1] = mesh.lon[eidx*3+1];
      surfmesh.lon[i*3+2] = mesh.lon[eidx*3+2];
    }
  }
}

void remove_nodes_from_surf(const MT_USET<mt_idx_t> & nodes,
                            mt_vector<mt_cnt_t> & surf_cnt,
                            mt_vector<mt_idx_t> & surf_con,
                            struct nbc_data & nbc)
{
  size_t nele = surf_cnt.size();
  size_t widx=0, widx_con=0;
  size_t ridx_con=0;

  for(size_t i=0; i<nele; i++) {
    switch(surf_cnt[i]) {
      case 3:
      {
        mt_idx_t vtx0 = surf_con[ridx_con++];
        mt_idx_t vtx1 = surf_con[ridx_con++];
        mt_idx_t vtx2 = surf_con[ridx_con++];

        bool ok0 = (nodes.count(vtx0) == 0);
        bool ok1 = (nodes.count(vtx1) == 0);
        bool ok2 = (nodes.count(vtx2) == 0);

        if(ok0 && ok1 && ok2) {
          surf_con[widx_con++] = vtx0;
          surf_con[widx_con++] = vtx1;
          surf_con[widx_con++] = vtx2;

          surf_cnt[widx]   = surf_cnt[i];

          nbc.eidx[widx]   = nbc.eidx[i];
          nbc.sp_vtx[widx] = nbc.sp_vtx[i];
          nbc.tag[widx]    = nbc.tag[i];

          widx++;
        }
        break;
      }

      case 4:
      {
        mt_idx_t vtx0 = surf_con[ridx_con++];
        mt_idx_t vtx1 = surf_con[ridx_con++];
        mt_idx_t vtx2 = surf_con[ridx_con++];
        mt_idx_t vtx3 = surf_con[ridx_con++];

        bool ok0 = (nodes.count(vtx0) == 0);
        bool ok1 = (nodes.count(vtx1) == 0);
        bool ok2 = (nodes.count(vtx2) == 0);
        bool ok3 = (nodes.count(vtx3) == 0);

        if(ok0 && ok1 && ok2 && ok3) {
          surf_con[widx_con++] = vtx0;
          surf_con[widx_con++] = vtx1;
          surf_con[widx_con++] = vtx2;
          surf_con[widx_con++] = vtx3;

          surf_cnt[widx]   = surf_cnt[i];

          nbc.eidx[widx]   = nbc.eidx[i];
          nbc.sp_vtx[widx] = nbc.sp_vtx[i];
          nbc.tag[widx]    = nbc.tag[i];

          widx++;
        }
        break;
      }

      default: break;
    }
  }

  surf_con.resize(widx_con);
  surf_cnt.resize(widx);

  nbc.eidx.resize(widx);
  nbc.sp_vtx.resize(widx);
  nbc.tag.resize(widx);
}


void remove_nodes_from_surf(const MT_USET<mt_idx_t> & nodes,
                            mt_vector<mt_cnt_t> & surf_cnt,
                            mt_vector<mt_idx_t> & surf_con)
{
  size_t nele = surf_cnt.size();
  size_t widx=0, widx_con=0;
  size_t ridx_con=0;

  for(size_t i=0; i<nele; i++) {
    switch(surf_cnt[i]) {
      case 3:
      {
        mt_idx_t vtx0 = surf_con[ridx_con++];
        mt_idx_t vtx1 = surf_con[ridx_con++];
        mt_idx_t vtx2 = surf_con[ridx_con++];

        bool ok0 = (nodes.count(vtx0) == 0);
        bool ok1 = (nodes.count(vtx1) == 0);
        bool ok2 = (nodes.count(vtx2) == 0);

        if(ok0 && ok1 && ok2) {
          surf_con[widx_con++] = vtx0;
          surf_con[widx_con++] = vtx1;
          surf_con[widx_con++] = vtx2;

          surf_cnt[widx]   = surf_cnt[i];

          widx++;
        }
        break;
      }

      case 4:
      {
        mt_idx_t vtx0 = surf_con[ridx_con++];
        mt_idx_t vtx1 = surf_con[ridx_con++];
        mt_idx_t vtx2 = surf_con[ridx_con++];
        mt_idx_t vtx3 = surf_con[ridx_con++];

        bool ok0 = (nodes.count(vtx0) == 0);
        bool ok1 = (nodes.count(vtx1) == 0);
        bool ok2 = (nodes.count(vtx2) == 0);
        bool ok3 = (nodes.count(vtx3) == 0);

        if(ok0 && ok1 && ok2 && ok3) {
          surf_con[widx_con++] = vtx0;
          surf_con[widx_con++] = vtx1;
          surf_con[widx_con++] = vtx2;
          surf_con[widx_con++] = vtx3;

          surf_cnt[widx]   = surf_cnt[i];

          widx++;
        }
        break;
      }

      default: break;
    }
  }

  surf_con.resize(widx_con);
  surf_cnt.resize(widx);
}

void remove_elems_from_surf(const mt_vector<bool> & keep,
                            mt_vector<mt_cnt_t> & surf_cnt,
                            mt_vector<mt_idx_t> & surf_con,
                            struct nbc_data & nbc)
{
  size_t nele = surf_cnt.size();
  size_t widx=0, widx_con=0;
  size_t ridx_con=0;

  for(size_t i=0; i<nele; i++) {
    switch(surf_cnt[i]) {
      case 3:
      {
        mt_idx_t vtx0 = surf_con[ridx_con++];
        mt_idx_t vtx1 = surf_con[ridx_con++];
        mt_idx_t vtx2 = surf_con[ridx_con++];

        if(keep[i]) {
          surf_con[widx_con++] = vtx0;
          surf_con[widx_con++] = vtx1;
          surf_con[widx_con++] = vtx2;

          surf_cnt[widx]   = surf_cnt[i];

          nbc.eidx[widx]   = nbc.eidx[i];
          nbc.sp_vtx[widx] = nbc.sp_vtx[i];
          nbc.tag[widx]    = nbc.tag[i];

          widx++;
        }
        break;
      }

      case 4:
      {
        mt_idx_t vtx0 = surf_con[ridx_con++];
        mt_idx_t vtx1 = surf_con[ridx_con++];
        mt_idx_t vtx2 = surf_con[ridx_con++];
        mt_idx_t vtx3 = surf_con[ridx_con++];

        if(keep[i]) {
          surf_con[widx_con++] = vtx0;
          surf_con[widx_con++] = vtx1;
          surf_con[widx_con++] = vtx2;
          surf_con[widx_con++] = vtx3;

          surf_cnt[widx]   = surf_cnt[i];

          nbc.eidx[widx]   = nbc.eidx[i];
          nbc.sp_vtx[widx] = nbc.sp_vtx[i];
          nbc.tag[widx]    = nbc.tag[i];

          widx++;
        }
        break;
      }

      default: break;
    }
  }

  surf_con.resize(widx_con);
  surf_cnt.resize(widx);

  nbc.eidx.resize(widx);
  nbc.sp_vtx.resize(widx);
  nbc.tag.resize(widx);
}




void compute_element_surface_normals(const mt_meshdata & surfmesh,
                                     const mt_vector<mt_real> & xyz,
                                     mt_vector<mt_real> & snrml)
{
  assert(surfmesh.e2n_dsp.size() > 0);

  size_t nele_surf = surfmesh.e2n_cnt.size();
  snrml.resize(nele_surf*3);

  for(size_t eidx = 0; eidx < nele_surf; eidx++)
  {
    mt_idx_t dsp = surfmesh.e2n_dsp[eidx];

    mt_idx_t v1 = surfmesh.e2n_con[dsp+0];
    mt_idx_t v2 = surfmesh.e2n_con[dsp+1];
    mt_idx_t v3 = surfmesh.e2n_con[dsp+2];

    // also for quads, we compute a triangle surface normal
    vec3r n = triangle_normal(v1, v2, v3, xyz);

    snrml[eidx*3+0] = n.x;
    snrml[eidx*3+1] = n.y;
    snrml[eidx*3+2] = n.z;
  }
}

void compute_nodal_surface_normals(const mt_meshdata & surfmesh,
                                   const mt_vector<mt_real> & xyz,
                                   mt_vector<mt_real> & snrml,
                                   const mt_real sign_scale)
{
  // full mesh connectivity needs to be set up
  std::string func = std::string(__func__) + " e2n_dsp:";
  check_nonzero(surfmesh.e2n_dsp.size(), func.c_str());
  func = std::string(__func__) + " n2e_cnt:";
  check_nonzero(surfmesh.n2e_cnt.size(), func.c_str());
  func = std::string(__func__) + " n2n_cnt:";
  check_nonzero(surfmesh.n2n_cnt.size(), func.c_str());

  size_t nele = surfmesh.e2n_cnt.size(), nnod = surfmesh.n2e_cnt.size(), full_nnod = xyz.size() / 3;
  mt_vector<vec3r> enrmls(nele), nnrmls(full_nnod);

  for(size_t eidx = 0; eidx < nele; eidx++)
  {
    mt_idx_t dsp = surfmesh.e2n_dsp[eidx];

    mt_idx_t v1 = surfmesh.e2n_con[dsp+0];
    mt_idx_t v2 = surfmesh.e2n_con[dsp+1];
    mt_idx_t v3 = surfmesh.e2n_con[dsp+2];

    // also for quads, we compute a triangle surface normal
    enrmls[eidx] = triangle_normal(v1, v2, v3, xyz);
  }

#if 0
  //Angle averaged vertex normals
  // according to "A Comparison of Algorithms for Vertex Normal Computation" this yields
  // smoother results than the default are weights
  #ifdef OPENMP
  #pragma omp parallel for schedule(guided)
  #endif
  for(size_t nidx = 0; nidx < nnod; nidx++)
  {
    if(surfmesh.n2e_cnt[nidx]) {
      vec3r avrg(0,0,0), temp;
      mt_vector<vec3r> edges(3);
      mt_idx_t start = surfmesh.n2e_dsp[nidx], stop = start + surfmesh.n2e_cnt[nidx];
      for(mt_idx_t i=start; i < stop; i++)
      {
        mt_idx_t eidx = surfmesh.n2e_con[i];
        const mt_idx_t* con = surfmesh.e2n_con.data() + surfmesh.e2n_dsp[eidx];
        vec3r refp(xyz.data() + 3 * nidx);

        // the algorithm picks the edges to the first two vertices that are not nidx of
        // each connected facet for computing the coefficients
        short widx = 0;
        for(short k=0; k < 3; k++) {
          if(con[k] != mt_idx_t(nidx)) {
            temp.get(xyz.data() + 3 * con[k]);
            edges[widx++] = temp;
          }
        }
        mt_real alpha = angle_between_line(refp, edges[0], edges[1]);
        avrg += enrmls[eidx] * alpha;
      }
      nnrmls[nidx] = avrg;
    }
  }
#else
  elemData_to_nodeData(surfmesh, enrmls, nnrmls);
#endif

  size_t num_zero = 0, num_fixed = 0;

  snrml.assign(full_nnod * 3, 0.0);
  for(size_t nidx = 0; nidx < nnod; nidx++)
  {
    if(surfmesh.n2e_cnt[nidx] > 0) {
      if(nnrmls[nidx].length2() == 0.0)
      {
        num_zero++;

        vec3r avrg(0,0,0);
        mt_idx_t start = surfmesh.n2e_dsp[nidx], stop = start + surfmesh.n2e_cnt[nidx];
        for(mt_idx_t i=start; i < stop; i++)
        {
          mt_idx_t eidx = surfmesh.n2e_con[i];
          avrg += enrmls[eidx];
        }

        if(avrg.length2() > 0) {
          // we take the averaged normal
          nnrmls[nidx] = avrg;
          num_fixed++;
        } else {
          // we take the normal of the first element
          nnrmls[nidx] = enrmls[surfmesh.n2e_con[start]];
        }
      }

      nnrmls[nidx] *= sign_scale;
      nnrmls[nidx].normalize();
      snrml[nidx*3+0] = nnrmls[nidx].x;
      snrml[nidx*3+1] = nnrmls[nidx].y;
      snrml[nidx*3+2] = nnrmls[nidx].z;
    }
  }

#if 0
  if(num_zero) {
    fprintf(stderr, "%s warning: %zu zero normals were computed. %zu were fixed by using homogenous weights.\n"
                    "Maybe the surface element orientation is not consistent.\n", __func__,
                    num_zero, num_fixed);
  }
#endif
}

void update_nodal_surface_normals(const mt_meshdata & surfmesh,
                                  const mt_vector<mt_real> & xyz,
                                  const MT_USET<mt_idx_t> & nod,
                                  mt_vector<mt_real> & snrml,
                                  const mt_real sign_scale)
{
  // full mesh connectivity needs to be set up
  check_nonzero(surfmesh.e2n_dsp.size(), __func__);
  check_nonzero(surfmesh.n2e_cnt.size(), __func__);

  for(mt_idx_t n : nod)
  {
    vec3r cur_nrml;

    mt_idx_t ele_start = surfmesh.n2e_dsp[n], ele_stop = ele_start + surfmesh.n2e_cnt[n];
    for(mt_idx_t i = ele_start; i<ele_stop; i++) {
      mt_idx_t eidx = surfmesh.n2e_con[i];
      mt_idx_t dsp = surfmesh.e2n_dsp[eidx];
      mt_idx_t v1 = surfmesh.e2n_con[dsp+0];
      mt_idx_t v2 = surfmesh.e2n_con[dsp+1];
      mt_idx_t v3 = surfmesh.e2n_con[dsp+2];

      // also for quads, we compute a triangle surface normal
      cur_nrml += triangle_normal(v1, v2, v3, xyz);
    }

    cur_nrml *= sign_scale;
    cur_nrml.normalize();
    cur_nrml.set(snrml.data() + n*3);
  }
}

bool apply_normal_orientation(mt_meshdata & surf, kdtree & surf_tree, mt_vector<mt_idx_t> & eidx, const bool inward)
{
  if(surf.e2n_dsp.size() == 0)
    bucket_sort_offset(surf.e2n_cnt, surf.e2n_dsp);

  mt_real edge_len_est = min_edgelength_estimate(surf, true, true);
  bool have_flipped = false;

  // if we pass in a empty eidx vector, then we will iterate over all surface elements
  bool using_elem_vec = eidx.size() > 0;
  size_t end = using_elem_vec ? eidx.size() : surf.e2n_cnt.size();

  for(size_t i=0; i<end; i++) {
    mt_idx_t ii = using_elem_vec ? eidx[i] : i;
    mt_idx_t* con = surf.e2n_con.data() + surf.e2n_dsp[ii];
    mt_idx_t v0 = con[0], v1 = con[1], v2 = con[2];

    vec3r n = triangle_normal(v0, v1, v2, surf.xyz);
    vec3r c = triangle_centerpoint(v0, v1, v2, surf.xyz);
    vec3r testpoint = c + (n * (0.01f * edge_len_est));

    bool is_inside = inside_closed_surface(surf_tree, testpoint);
    bool needs_flip = is_inside != inward;

    // if the current orientation does not match the desired one, we flip the normals
    if(needs_flip) {
      con[0] = v2;
      con[1] = v1;
      con[2] = v0;
      have_flipped = true;
    }
  }

  return have_flipped;
}

bool apply_normal_orientation(mt_meshdata & surf, const bool inward)
{
  if(surf.e2n_dsp.size() == 0)
    bucket_sort_offset(surf.e2n_cnt, surf.e2n_dsp);

  kdtree surf_tree(10);
  surf_tree.build_tree(surf);
  mt_vector<mt_idx_t> eidx;

  return apply_normal_orientation(surf, surf_tree, eidx, inward);
}

bool point_inside_surface(const mt_meshdata & surf, const kdtree & tree, vec3r & found_point)
{
  mt_real avg_edge_length = avrg_edgelength_estimate(surf);

  bool did_find = false;
  found_point = {0,0,0};
  int iter = 0, max_iter = 10;
  mt_idx_t numelem = surf.e2n_cnt.size();

  do {
    mt_idx_t eidx = mt_idx_t(drand48() * numelem) % numelem;
    mt_idx_t v1 = surf.e2n_con[eidx*3+0];
    mt_idx_t v2 = surf.e2n_con[eidx*3+1];
    mt_idx_t v3 = surf.e2n_con[eidx*3+2];
    vec3r inside_normal = triangle_normal(v1, v2, v3, surf.xyz) * mt_real(-1.0);
    vec3r centerpoint   = triangle_centerpoint(v1, v2, v3, surf.xyz);

    vec3r insidepoint = centerpoint + inside_normal * avg_edge_length;
    if(inside_closed_surface(tree, insidepoint)) {
      did_find = true;
      found_point = insidepoint;
    }
    else {
      inside_normal *= -1.0f;
      insidepoint = centerpoint + inside_normal * avg_edge_length;
      if(inside_closed_surface(tree, insidepoint)) {
        did_find = true;
        found_point = insidepoint;
      }
    }

    iter++;
  } while(did_find == false && iter < max_iter);

  return did_find;
}

void remove_bad_surface_edges(const mt_meshdata & mesh,
                              mt_meshdata & surfmesh)
{
  // full mesh connectivity needs to be set up
  assert(surfmesh.n2n_cnt.size() > 0 && surfmesh.n2e_cnt.size() > 0);

  mt_vector<mt_real> snrml;
  compute_nodal_surface_normals(surfmesh, mesh.xyz, snrml);

  mt_vector<mt_cnt_t> & cnt = surfmesh.n2n_cnt;
  mt_vector<mt_idx_t> & con = surfmesh.n2n_con;

  size_t ridx=0, widx=0;

  for(size_t i=0; i<cnt.size(); i++)
  {
    mt_idx_t c = 0;  // current count
    mt_point<mt_real> cn(snrml.data() + i*3);  // current normal

    for(mt_idx_t j=0; j<cnt[i]; j++)
    {
      mt_point<mt_real> tn(snrml.data() + con[ridx]*3);

      if( cn.scaProd(tn) > mt_real(0.0) )
      {
        c++;
        con[widx++] = con[ridx];
      }
      ridx++;
    }
    cnt[i] = c;
  }
  con.resize(widx);
  bucket_sort_offset(surfmesh.n2n_cnt, surfmesh.n2n_dsp);
}

void psdata_to_mesh(const mt_psdata & ps, mt_meshdata & psmesh)
{
  size_t tot_npt = 0;
  mt_vector<vec3r> pts;
  mt_vector<int> cabele_dsp(ps.cables.size()+1, 0);

  // determine number of ele and estimate number of points
  size_t tot_nele = 0;
  for(size_t i=0; i<ps.cables.size(); i++) {
    size_t npt = ps.cables[i].pts.size() / 3;
    tot_npt += npt;
    tot_nele += npt - 1;
    cabele_dsp[i+1] = tot_nele;
  }

  psmesh.e2n_cnt.assign(tot_nele, mt_cnt_t(2));
  psmesh.etype.assign(tot_nele, Line);
  psmesh.etags.resize(tot_nele);

  psmesh.e2n_con.assign(tot_nele*2, -1);
  pts.reserve(tot_npt);

  // we reset the element counter to use it as current elem index
  tot_nele = 0, tot_npt = 0;

  for (size_t i = 0; i < ps.cables.size(); i++) {
    const mt_pscable & ccab = ps.cables[i];

    size_t nele = ccab.pts.size() / 3 - 1;
    size_t cable_pt = 0;   // index in the current cable's points

    bool root = (ps.cables[i].par1 < 0 && ps.cables[i].par2 == -1);

    if(root) { //root cable
      psmesh.e2n_con[tot_nele*2 + 0] = tot_npt;
      pts.push_back(vec3r(ccab.pts.data() + cable_pt* 3));
      tot_npt++;
    }
    cable_pt++;

    for (size_t e = 0; e < nele; e++)
    {
      if(e < nele - 1) {
        psmesh.e2n_con[tot_nele*2 + 1] = tot_npt;
        pts.push_back(vec3r(ccab.pts.data() + cable_pt* 3));
        psmesh.e2n_con[(tot_nele+1)*2 + 0] = tot_npt;
        tot_npt++;
      }
      else {  // check for 2 cables joining as in a 2:1 or 2:2
        size_t j = i;
        if(ccab.br1 >= 0) {
          for(j=0; j<i; j++) {
            int ccab_last_ele = cabele_dsp[i+1] - 1;
            int cabj_last_ele = cabele_dsp[j+1] - 1;

            if(ps.cables[j].br1 == ccab.br1 || ps.cables[j].br2 == ccab.br1) {
              psmesh.e2n_con[ccab_last_ele*2+1] = psmesh.e2n_con[cabj_last_ele*2+1];
              break;
            }
            if( ccab.br2 >= 0 &&
                (ps.cables[j].br1 == ccab.br2 || ps.cables[j].br2 == ccab.br2)) {
              psmesh.e2n_con[ccab_last_ele*2+1] = psmesh.e2n_con[cabj_last_ele*2+1];
              break;
            }
          }
        }

        if(j==i) {                 // no 2:1 situation
          psmesh.e2n_con[tot_nele*2 + 1] = tot_npt;
          pts.push_back(vec3r(ccab.pts.data() + cable_pt* 3));
          tot_npt++;
        }
      }

      // element tag is cable index
      psmesh.etags[tot_nele] = i;

      cable_pt++;
      tot_nele++;
    }
  }

  // third pass - determine initial cable nodes since all final
  // nodes are determined, and recompute quantities for intial element
  for (size_t i = 0; i < ps.cables.size(); i++) {
    bool root = (ps.cables[i].par1 < 0 && ps.cables[i].par2 == -1);

    if(!root) {
      int cur_cab_first_ele = cabele_dsp[i];
      int parent_cab_last_ele = cabele_dsp[ps.cables[i].par1 + 1] - 1;

      psmesh.e2n_con[cur_cab_first_ele*2 + 0] = psmesh.e2n_con[parent_cab_last_ele*2 + 1];
    }
  }

  points_to_array(pts, psmesh.xyz);
}

void generate_coord_map(mt_meshdata & mesh,
                        MT_MAP<mt_triple<mt_idx_t>,mt_idx_t> & cmap,
                        int & scale_factor)
{
  // we have to take minimal edge lenght here in order to compute the correct scale factor
  // since the average can be enough oders of magnitude higher than the minimum for us to
  // loose coordinates
  // TODO: Elias
  mt_real edge_len = min_edgelength_estimate(mesh, true);

  // we treat edge_len == 0 with an error
  if(edge_len == 0.0) {
    fprintf(stderr, "%s WARNING: Zero minimum edge length detected!\nRetry edge length estimate with nonzero minimum!\n", __func__);
    // We can take the non-zero minimum, aka the first edge that has non zero length and base the estimate on that
    edge_len = min_edgelength_estimate(mesh, true, true);
  }

  int dp = get_dec_power_estimate(edge_len);
  int scale_power = 3;

  if(dp < 0)
    scale_power += -dp;

  scale_factor = pow(10.0, scale_power);

  for(size_t i=0; i<mesh.xyz.size() / 3; i++) {
    mt_triple<mt_idx_t> t;
    t.v1 = mt_idx_t(mesh.xyz[i*3+0]*scale_factor);
    t.v2 = mt_idx_t(mesh.xyz[i*3+1]*scale_factor);
    t.v3 = mt_idx_t(mesh.xyz[i*3+2]*scale_factor);
    cmap[t] = i;
  }
}

void mesh_union(mt_meshdata & mesh1, const mt_meshdata & mesh2, const bool error_on_empty_intf)
{
  if(mesh2.e2n_cnt.size() == 0) return;

  // we want to make sure that the dsp is always up to date
  bucket_sort_offset(mesh1.e2n_cnt, mesh1.e2n_dsp);

  short numfib2 = mesh2.e2n_cnt.size() * 6 == mesh2.lon.size() ? 2 : 1;

  short numfib1 = 0;
  if(mesh1.e2n_cnt.size())
    numfib1 = mesh1.e2n_cnt.size() * 6 == mesh1.lon.size() ? 2 : 1;
  else
    numfib1 = numfib2;

  // temporary buffer for mesh2 fibers, needed since we may have to resize it
  mt_vector<mt_fib_t> mesh2_lon(mesh2.lon);

  if(numfib1 != numfib2) {
    // one mesh has two fib directions, and the other has one direction.
    // we only need to expand the fibers of the mesh with one direction.
    if(numfib1 == 1) set_num_fibers(mesh1.e2n_cnt.size(), mesh1.lon, 2);
    else             set_num_fibers(mesh2.e2n_cnt.size(), mesh2_lon, 2);
  }

  MT_MAP<mt_idx_t,mt_idx_t> mesh2_to_mesh1_interf;

  // mesh1 may be empty, thus we skip this part if it is ..
  if(mesh1.xyz.size()) {
    kdtree m1tree(10);
    m1tree.build_vertex_tree(mesh1.xyz);

    mt_real eps = min_edgelength_estimate(mesh1, true, true) * 0.1;

    mt_vector<mt_mixed_tuple<mt_real,int>> close_vtx;

    for(size_t i=0; i<mesh2.xyz.size() / 3; i++) {
      vec3r ref_pt(mesh2.xyz.data() + i*3);

      close_vtx.resize(0);
      m1tree.vertices_in_sphere(ref_pt, eps, close_vtx);

      if(close_vtx.size()) {
        if(close_vtx.size() > 1)
          std::sort(close_vtx.begin(), close_vtx.end());

        mt_idx_t mesh1_idx = close_vtx[0].v2;
        mt_idx_t mesh2_idx = i;

        mesh2_to_mesh1_interf[mesh2_idx] = mesh1_idx;
      }
    }
  }

  std::cout << "Interface size: " << mesh2_to_mesh1_interf.size() << std::endl;
  if(error_on_empty_intf && mesh2_to_mesh1_interf.size() == 0 && mesh1.e2n_cnt.size() && mesh2.e2n_cnt.size()) {
    fprintf(stderr, "%s error: Empty mesh interface is not allowed! Aborting!\n", __func__);
    return;
  }

  // construct whole mapping vector for mesh2
  size_t nnod_msh1 = mesh1.xyz.size() / 3, nnod_msh2 = mesh2.xyz.size() / 3;
  mt_vector<mt_idx_t> mesh2_to_wholemesh(nnod_msh2);
  for(size_t n=0; n<nnod_msh2; n++) {
    if(mesh2_to_mesh1_interf.count(n)) // interface node
      mesh2_to_wholemesh[n] = mesh2_to_mesh1_interf[n];
    else  // other nodes
      mesh2_to_wholemesh[n] = nnod_msh1 + n;
  }

  mt_vector<mt_idx_t> mesh2_e2n_con = mesh2.e2n_con;
  for(size_t i=0; i<mesh2_e2n_con.size(); i++)
    mesh2_e2n_con[i] = mesh2_to_wholemesh[mesh2_e2n_con[i]];

  mesh1.e2n_cnt.append(mesh2.e2n_cnt.begin(), mesh2.e2n_cnt.end());
  mesh1.e2n_con.append(mesh2_e2n_con.begin(), mesh2_e2n_con.end());

  mesh1.etype.append(mesh2.etype.begin(), mesh2.etype.end());
  mesh1.etags.append(mesh2.etags.begin(), mesh2.etags.end());
  mesh1.lon.append(mesh2_lon.begin(), mesh2_lon.end());

  mesh1.xyz.append(mesh2.xyz.begin(), mesh2.xyz.end());

  // reindex to get rid of duplicated vertices
  reindex_nodes(mesh1, true);

  bucket_sort_offset(mesh1.e2n_cnt, mesh1.e2n_dsp);
  correct_duplicate_elements(mesh1);
}


/// function evaluating if nodes are inside a closed surface or not
void nodes_in_surface(const mt_meshdata & mesh,
                      const kdtree & tree,
                      mt_vector<bool> & in_surf)
{
  mt_vector<vec3r> points;
  array_to_points(mesh.xyz, points);
  in_surf.resize(points.size());

  #ifdef OPENMP
  #pragma omp parallel for schedule(guided)
  #endif
  for(size_t i=0; i<points.size(); i++) {
    if(tree.in_root_bbox(points[i]))
      in_surf[i] = inside_closed_surface(tree, points[i]);
    else
      in_surf[i] = false;
  }
}

void elems_in_surface(const mt_meshdata & mesh,
                      const kdtree & surface,
                      mt_vector<mt_idx_t> & elems)
{
  #ifdef OPENMP
  #pragma omp parallel
  #endif
  {
    mt_vector<mt_idx_t> loc_elems; loc_elems.reserve(mesh.e2n_cnt.size() / 6);

    #ifdef OPENMP
    #pragma omp for schedule(guided)
    #endif
    for(size_t eidx = 0; eidx < mesh.e2n_cnt.size(); eidx++)
    {
      vec3r p = barycenter(mesh, eidx);
      if(inside_closed_surface(surface, p))
        loc_elems.push_back(mt_idx_t(eidx));
    }

    #ifdef OPENMP
    #pragma omp critical
    #endif
    {
      elems.append(loc_elems.begin(), loc_elems.end());
    }
  }
}

void elems_on_surface(const mt_meshdata & mesh,
                      const kdtree & surface,
                      const mt_real eps,
                      mt_vector<mt_idx_t> & elems)
{
  check_condition(mesh.etype[0] == Tri, __func__, "mesh is a triangle mesh");

  #ifdef OPENMP
  #pragma omp parallel
  #endif
  {
    mt_vector<mt_idx_t> loc_elems; loc_elems.reserve(mesh.e2n_cnt.size() / 6);

    #ifdef OPENMP
    #pragma omp for schedule(guided)
    #endif
    for(size_t eidx = 0; eidx < mesh.e2n_cnt.size(); eidx++)
    {
      vec3r p = barycenter(mesh, eidx);

      const mt_idx_t* con = mesh.e2n_con.data() + mesh.e2n_dsp[eidx];
      vec3r n = triangle_normal(con[0], con[1], con[2], mesh.xyz);

      if(on_surface(surface, p, n, eps))
        loc_elems.push_back(mt_idx_t(eidx));
    }

    #ifdef OPENMP
    #pragma omp critical
    #endif
    {
      elems.append(loc_elems.begin(), loc_elems.end());
    }
  }
}


void sample_elem_tags(mt_meshdata & mesh, mt_mask & tag_found, const kdtree & tree, const mt_idx_t newtag,
                     const int sampling_type, const int grow)
{
  if(mesh.e2n_dsp.size() == 0 || mesh.n2e_cnt.size() == 0)
    compute_full_mesh_connectivity(mesh, false);

  MT_USET<mt_idx_t> insert_ele, insert_nod;

  switch(sampling_type)
  {
    default:
    case 0:
    {
      // this version samples elements with all nodes inside the surface
      mt_vector<bool> nod_in_surf;
      nodes_in_surface(mesh, tree, nod_in_surf);
      for(size_t eidx=0; eidx < mesh.e2n_cnt.size(); eidx++)
      {
        if(tag_found.count(eidx) == 0) {
          mt_idx_t start = mesh.e2n_dsp[eidx], stop = start + mesh.e2n_cnt[eidx];
          mt_idx_t ncon = 0;
          for(mt_idx_t j=start; j<stop; j++) {
            mt_idx_t nidx = mesh.e2n_con[j];
            if(nod_in_surf[nidx]) ncon++;
          }

          if(ncon == mesh.e2n_cnt[eidx])
            insert_ele.insert(eidx);
        }
      }
      break;
    }
    case 1:
#ifdef OPENMP
#pragma omp parallel
#endif
    {
      MT_USET<mt_idx_t> loc_ele;

#ifdef OPENMP
#pragma omp for schedule(guided)
#endif
      for(size_t eidx=0; eidx < mesh.e2n_cnt.size(); eidx++)
      {
        if(tag_found.count(eidx) == 0) {
          vec3r ctr = barycenter(mesh.e2n_cnt[eidx], mesh.e2n_con.data() + mesh.e2n_dsp[eidx], mesh.xyz.data());
          if(inside_closed_surface(tree, ctr))
            loc_ele.insert(eidx);
        }
      }

#ifdef OPENMP
#pragma omp critical
#endif
      {
        insert_ele.insert(loc_ele.begin(), loc_ele.end());
      }
    }
    break;

    case 2:
    {
      // this version samples elements with at lease one node inside the surface
      mt_vector<bool> nod_in_surf;
      nodes_in_surface(mesh, tree, nod_in_surf);
      for(size_t eidx=0; eidx < mesh.e2n_cnt.size(); eidx++)
      {
        if(tag_found.count(eidx) == 0) {
          mt_idx_t start = mesh.e2n_dsp[eidx], stop = start + mesh.e2n_cnt[eidx];
          mt_idx_t ncon = 0;
          for(mt_idx_t j=start; j<stop; j++) {
            mt_idx_t nidx = mesh.e2n_con[j];
            if(nod_in_surf[nidx]) ncon++;
          }

          if(ncon > 0)
            insert_ele.insert(eidx);
        }
      }
      break;
    }

    case 3:
    {
      mt_real eps = avrg_edgelength_estimate(mesh, true);

#ifdef OPENMP
#pragma omp parallel
#endif
      {
        MT_USET<mt_idx_t> loc_ele;

#ifdef OPENMP
#pragma omp parallel for schedule(guided)
#endif
        for(size_t eidx=0; eidx < mesh.e2n_cnt.size(); eidx++)
        {
          if(tag_found.count(eidx) == 0) {
            vec3r ctr = barycenter(mesh, eidx);

            const mt_idx_t* con = mesh.e2n_con.data() + mesh.e2n_dsp[eidx];
            vec3r n = triangle_normal(con[0], con[1], con[2], mesh.xyz);

            if(on_surface(tree, ctr, n, eps))
              loc_ele.insert(eidx);
          }
        }
#ifdef OPENMP
#pragma omp critical
#endif
        {
          insert_ele.insert(loc_ele.begin(), loc_ele.end());
        }
      }
      break;
    }
  }

  if(grow) {
    elemSet_to_nodeSet(mesh, insert_ele, insert_nod);

    for(int i=0; i<grow; i++)
      expand_nodeset(mesh, insert_nod);
    for(int i=0; i<grow; i++)
      shrink_nodeset(mesh, insert_nod);

    mt_vector<mt_idx_t> elem;
    MT_USET<mt_tag_t> empty;
    elements_in_selection(mesh, elem, empty, insert_nod);

    insert_ele.clear();
    insert_ele.insert(elem.begin(), elem.end());
  }

  for(mt_idx_t e : insert_ele) {
    mesh.etags[e] = newtag;
    tag_found.insert(e);
  }
}

void unified_surf_from_mixed_list(const mt_meshdata & mesh,
                                  const mt_vector<std::string> & list,
                                  MT_MAP<mt_triple<mt_idx_t>,    tri_sele>  & tri_surf,
                                  MT_MAP<mt_quadruple<mt_idx_t>, quad_sele> & quad_surf)
{
  MT_USET<mt_tag_t> tags;

  for(size_t i=0; i<list.size(); i++) {
    if(is_integer(list[i])) {
      tags.insert(atoi(list[i].c_str()));
    }
    else if(list[i] == "_all_") {
      tags.insert(mesh.etags.begin(), mesh.etags.end());
    }
    else {
      mt_meshdata curr_surf;
      mt_vector<mt_idx_t> eidx;

      read_surf_info(mesh, curr_surf, eidx, list[i]);
      surfmesh_to_surfmap(curr_surf, eidx, tri_surf, quad_surf);
    }
  }

  if(tags.size())
    compute_surface(mesh, tags, tri_surf, quad_surf);
}

void read_surf_info(const mt_meshdata & mesh, mt_meshdata & surf, mt_vector<mt_idx_t> & eidx,
                    std::string basefile)
{
  fixBasename(basefile);
  std::string surffile = basefile + SURF_EXT;
  std::string nbcfile  = basefile + NBC_EXT;
  std::string vtxfile     = basefile + SURF_EXT + VTX_EXT;
  std::string vtxfile_v2  = basefile + VTX_EXT;
  nbc_data nbc;
  bool vtx1 = false;

  if(file_exists(nbcfile)) {
    std::cout << "Reading nbc file: " << nbcfile << std::endl;
    read_nbc(nbc, surf.e2n_con, nbcfile);
    surf.e2n_cnt.assign(surf.e2n_con.size() / 3, mt_idx_t(3));
    surf.etype  .assign(surf.e2n_con.size() / 3, Tri);
    eidx = nbc.eidx;
  }
  else if(file_exists(surffile)) {
    check_nonzero(mesh.n2e_cnt.size(), __func__);

    std::cout << "Reading surface: " << surffile << std::endl;
    readElements(surf, surffile);
    bucket_sort_offset(surf.e2n_cnt, surf.e2n_dsp);

    size_t nelems = surf.e2n_cnt.size();
    eidx.resize(nelems);

    std::cout << "Recovering volumetric element indices .. " << std::endl;

    #ifdef OPENMP
    #pragma omp parallel for schedule(dynamic, 10)
    #endif
    for(size_t e = 0; e < nelems; e++) {
      MT_USET<mt_idx_t> elemset;
      mt_idx_t * con = surf.e2n_con.data() + surf.e2n_dsp[e];

      elements_with_face(mesh, con[0], con[1], con[2], elemset);

      switch(elemset.size()) {
        default: fprintf(stderr, "%s error: More than 2 elements seem to share a face! Aborting!\n", __func__);
                 exit(1);

        case 0: eidx[e] = 0; break;

        case 1: eidx[e] = *elemset.begin(); break;

        case 2: {
          auto it = elemset.begin();
          mt_idx_t e1 = *it; ++it; mt_idx_t e2 = *it;
          vec3r ctr1   = barycenter(mesh, e1);
          vec3r ctr_tr = triangle_centerpoint(con[0], con[1], con[2], mesh.xyz);
          vec3r n      = triangle_normal(con[0], con[1], con[2], mesh.xyz);

          if(n.scaProd(unit_vector(ctr1 - ctr_tr)) < 0.0)
            eidx[e] = e1;
          else
            eidx[e] = e2;

          break;
        }
      }
    }
  }
  else if((vtx1 = file_exists(vtxfile)) || file_exists(vtxfile_v2)) {
    mt_vector<mt_idx_t> vtx;
    if(vtx1) {
      std::cout << "Reading vertex file: " << vtxfile << std::endl;
      read_vtx(vtx, vtxfile);
    } else {
      std::cout << "Reading vertex file: " << vtxfile_v2 << std::endl;
      read_vtx(vtx, vtxfile_v2);
    }

    mt_mask vtxnod(mesh.xyz.size() / 3);
    vtxnod.insert(vtx.begin(), vtx.end());

    std::cout << "Generating surface data .. " << std::endl;

    mt_mapping<mt_idx_t> ele2face;
    MT_MAP<mt_triple<mt_idx_t>, mt_idx_t> face_map;
    MT_USET<mt_triple<mt_idx_t> > sel_faces;

    compute_faces(mesh, ele2face, face_map);

    for(auto it = face_map.begin(); it != face_map.end(); ++it)
    {
      const mt_triple<mt_idx_t> & trp = it->first;
      if(vtxnod.count(trp.v1) && vtxnod.count(trp.v2) && vtxnod.count(trp.v3))
        sel_faces.insert(trp);
    }

    size_t nelems = sel_faces.size();
    surf.e2n_cnt.assign(nelems, mt_idx_t(3));
    surf.etype  .assign(nelems, Tri);
    surf.e2n_con.resize(nelems*3);
    eidx.resize(nelems);

    size_t widx = 0;
    for(const mt_triple<mt_idx_t> & t : sel_faces) {
      surf.e2n_con[widx+0] = t.v1;
      surf.e2n_con[widx+1] = t.v2;
      surf.e2n_con[widx+2] = t.v3;
      widx += 3;
    }
    bucket_sort_offset(surf.e2n_cnt, surf.e2n_dsp);

    std::cout << "Recovering volumetric element indices .. " << std::endl;

    #ifdef OPENMP
    #pragma omp parallel for schedule(dynamic, 10)
    #endif
    for(size_t e = 0; e<nelems; e++) {
      MT_USET<mt_idx_t> elemset;
      mt_idx_t * con = surf.e2n_con.data() + surf.e2n_dsp[e];

      elements_with_face(mesh, con[0], con[1], con[2], elemset);

      switch(elemset.size()) {
        case 0:
          fprintf(stderr, "%s error: Surf element does not belong to mesh! Aborting!\n", __func__);
          exit(1);

        default:
        case 1: {
          eidx[e] = *elemset.begin();
          mt_idx_t v1 = con[0], v2 = con[1], v3 = con[2];

          vec3r ctr    = barycenter(mesh, eidx[e]);
          vec3r ctr_tr = triangle_centerpoint(v1, v2, v3, mesh.xyz);
          vec3r n      = triangle_normal(v1, v2, v3, mesh.xyz);

          if(n.scaProd(unit_vector(ctr - ctr_tr)) > 0.0) {
            con[0] = v3, con[1] = v2, con[2] = v1;
          }
          break;
        }
      }
    }
  } else {
    fprintf(stderr, "%s error: Neither .surf, .vtx nor .neubc file can be found! Aborting!\n", __func__);
    exit(1);
  }
}

void write_surf_info(const mt_meshdata & surface, const nbc_data* nbc,
                     const size_t numele, const size_t npts, std::string bname)
{
  fixBasename(bname);
  std::string surfname = bname + SURF_EXT;
  std::string vtxname  = bname + SURF_EXT + VTX_EXT;
  std::string nbcname  = bname + NBC_EXT;

  // generate unique nodelist from surface
  mt_vector<mt_idx_t> vtx;
  vtx.assign(surface.e2n_con.begin(), surface.e2n_con.end());
  binary_sort(vtx); unique_resize(vtx);

  std::cout << "Writing surface " << surfname << std::endl;
  write_surf(surface.e2n_cnt, surface.e2n_con, surfname);

  std::cout << "Writing vertices " << vtxname << std::endl;
  write_vtx(vtx, vtxname);

  if(nbc) {
    std::cout << "Writing neubc " << nbcname << std::endl;
    write_nbc(*nbc, surface.e2n_con, npts, numele, nbcname);
  }
}

void reindex_cuthill_mckee(const mt_vector<mt_cnt_t> & n2n_cnt,
                           const mt_vector<mt_idx_t> & n2n_dsp,
                           const mt_vector<mt_idx_t> & n2n_con,
                           const bool reverse,
                           MT_MAP<mt_idx_t, mt_idx_t> & old2new)
{
  /*
   * We use the Cuthill-McKee algorithm to generate a permutation
   * of the original indices. This permutation is then renumbered
   * either forward or reverse.
   *
   * The permutation is generated as following:
   * Initially, an arbitrary node is selected. It is inserted in the permutation
   * and in the imaginary set R (represented by a boolean vector).
   * Then we loop over each node in perm, adding all connected nodes, that are not yet
   * in R, in ascending order to perm and R.
   *
   */
  size_t nnod = n2n_cnt.size();

  mt_vector<bool>   inR(nnod, false);
  mt_vector<mt_idx_t> perm(nnod);
  perm.resize(1); perm[0] = 0; inR[0] = true;
  mt_idx_t pidx=0;

  while(perm.size() < nnod)
  {
    mt_idx_t nidx;
    if (pidx < mt_idx_t(perm.size())) nidx = perm[pidx++];
    else {
      int i=0;
      while(inR[i] == true) i++;
      nidx = i;
      std::cerr << "Warning: node " << nidx << " seems decoupled from mesh as it could not be reached by traversing the mesh edges!" << std::endl;
    }
    mt_idx_t start = n2n_dsp[nidx], stop = start + n2n_cnt[nidx];
    mt_vector<mt_idx_t> adj(stop - start);

    mt_idx_t widx=0;
    for(mt_idx_t j = start; j<stop; j++) {
      mt_idx_t cidx = n2n_con[j];
      if( ! inR[cidx] ) {
        adj[widx++] = cidx;
        inR[cidx] = true;
      }
    }

    adj.resize(widx);
    binary_sort(adj);
    perm.append(adj.begin(), adj.end());
  }

  if(!reverse) {
    // Cuthill-McKee
    for(size_t i=0; i<perm.size(); i++) old2new[perm[i]] = i;
  } else {
    // Reverse Cuthill-McKee
    for(size_t i=0, j=perm.size()-1; i<perm.size(); i++, j--) old2new[perm[j]] = i;
  }
}

void apply_split(mt_meshdata & mesh,
                 const mt_vector<split_item> & splitlist)
{
  MT_MAP<mt_idx_t,mt_idx_t> new2old;

  if(mesh.e2n_dsp.size() != mesh.e2n_cnt.size())
    bucket_sort_offset(mesh.e2n_cnt, mesh.e2n_dsp);

  mt_idx_t max_idx = 0;

  for(const split_item & se : splitlist) {
    mt_idx_t eidx = se.elemIdx;

    if(new2old.count(se.newIdx) == 0) {
      new2old[se.newIdx] = se.oldIdx;
      if(max_idx < se.newIdx) max_idx = se.newIdx;
    }

    const mt_idx_t start = mesh.e2n_dsp[eidx], stop = start + mesh.e2n_cnt[eidx];
    for(mt_idx_t j=start; j<stop; j++) {
      mt_idx_t c = mesh.e2n_con[j];
      if(c == se.oldIdx) {
        mesh.e2n_con[j] = se.newIdx;
        break;
      }
    }
  }

  mesh.xyz.resize((max_idx + 1) * 3);

  for(auto it=new2old.begin(); it != new2old.end(); ++it) {
    mt_idx_t newIdx = it->first, oldIdx = it->second;

    mesh.xyz[newIdx*3+0] = mesh.xyz[oldIdx*3+0];
    mesh.xyz[newIdx*3+1] = mesh.xyz[oldIdx*3+1];
    mesh.xyz[newIdx*3+2] = mesh.xyz[oldIdx*3+2];
  }
}

void undo_split(mt_meshdata & mesh,
                const mt_vector<split_item> & splitlist)
{
  MT_USET<mt_idx_t> remIdx;

  if(mesh.e2n_dsp.size() != mesh.e2n_cnt.size())
    bucket_sort_offset(mesh.e2n_cnt, mesh.e2n_dsp);

  for(const split_item & se : splitlist) {
    mt_idx_t eidx = se.elemIdx;
    remIdx.insert(se.newIdx);

    const mt_idx_t start = mesh.e2n_dsp[eidx], stop = start + mesh.e2n_cnt[eidx];
    for(mt_idx_t j=start; j<stop; j++) {
      mt_idx_t c = mesh.e2n_con[j];
      if(c == se.newIdx) {
        mesh.e2n_con[j] = se.oldIdx;
        break;
      }
    }
  }

  // WARNING: if we remove splits out-of-order we break the indexing! A more sophisticated
  // index mapping is required for safe undo of multiple splits in any order..
  size_t widx = 0;
  for(size_t ridx = 0; ridx < mesh.xyz.size() / 3; ridx++) {
    if(remIdx.count(ridx) == 0) {
      mesh.xyz[widx*3+0] = mesh.xyz[ridx*3+0];
      mesh.xyz[widx*3+1] = mesh.xyz[ridx*3+1];
      mesh.xyz[widx*3+2] = mesh.xyz[ridx*3+2];
      widx++;
    }
  }
  mesh.xyz.resize(widx*3);
}

mt_mixed_tuple<mt_idx_t, mt_real>
find_elem_idx(const vec3r &pnt, const mt_idx_t closest_node_idx,
              const mt_meshdata &mesh, std::size_t max_it)
{
  // LV (-1) or RV (1)
  assert((0 <= closest_node_idx) && (closest_node_idx < mt_idx_t(mesh.xyz.size() / 3)));

  const mt_real tol = 1.0e-8;

  MT_USET<mt_idx_t> node_idx_set;     // already considered nodes
  MT_USET<mt_idx_t> new_node_idx_set; // new nodes to consider
  MT_USET<mt_idx_t> elem_idx_set;     // already considered elements
  new_node_idx_set.insert(closest_node_idx);

  mt_idx_t elem_idx, elem_cnt, elem_dsp;
  mt_idx_t node_idx, node_dsp;

  std::array<mt_idx_t, 8> vtx_idx;  // vertex indices of element
  std::array<vec3r, 8> elem_vtx;  // element vertices (UVC or XYZ)

  mt_idx_t best_elem_idx;             // index of the element with "best" solution
  vec3r sol, best_sol;              // solution and best solution
  mt_real sol_dist, best_sol_dist;  // distance of solution and best solution

  std::size_t count;
  bool iterate;
  TetraShape tet;

  best_elem_idx = -1;
  best_sol_dist = std::numeric_limits<mt_real>::infinity();
  iterate = true;
  count = 0;

  MT_USET<mt_idx_t> tmp_node_idx_set;

  while ((iterate) && (count<max_it)) {
    count++;
    tmp_node_idx_set.clear();

    for (auto it=new_node_idx_set.begin(); it!=new_node_idx_set.end() && (iterate); it++)
    {
      node_idx = *it;

      // if new node index was already considered
      // continue with next node
      if (node_idx_set.count(node_idx))
        continue;
      else // add to considered nodes
        node_idx_set.insert(node_idx);

      // get attached elements
      elem_cnt = mesh.n2e_cnt[node_idx];
      elem_dsp = mesh.n2e_dsp[node_idx];

      for (mt_idx_t j=0; (j<elem_cnt) && (iterate); j++)
      {
        elem_idx = mesh.n2e_con[elem_dsp+j];

        // if new element was already considered
        // continue with next element
        if (elem_idx_set.count(elem_idx))
          continue;
        else // add to considered elements
          elem_idx_set.insert(elem_idx);

        node_dsp = mesh.e2n_dsp[elem_idx];

        // ###########################
        // TODO[MG]: generalize !!!!
        // ###########################

        vtx_idx[0] = mesh.e2n_con[node_dsp+0];
        vtx_idx[1] = mesh.e2n_con[node_dsp+1];
        vtx_idx[2] = mesh.e2n_con[node_dsp+2];
        vtx_idx[3] = mesh.e2n_con[node_dsp+3];

        elem_vtx[0].assign(mesh.xyz[3*vtx_idx[0]+0], mesh.xyz[3*vtx_idx[0]+1], mesh.xyz[3*vtx_idx[0]+2]);
        elem_vtx[1].assign(mesh.xyz[3*vtx_idx[1]+0], mesh.xyz[3*vtx_idx[1]+1], mesh.xyz[3*vtx_idx[1]+2]);
        elem_vtx[2].assign(mesh.xyz[3*vtx_idx[2]+0], mesh.xyz[3*vtx_idx[2]+1], mesh.xyz[3*vtx_idx[2]+2]);
        elem_vtx[3].assign(mesh.xyz[3*vtx_idx[3]+0], mesh.xyz[3*vtx_idx[3]+1], mesh.xyz[3*vtx_idx[3]+2]);

        tmp_node_idx_set.insert(vtx_idx[0]);
        tmp_node_idx_set.insert(vtx_idx[1]);
        tmp_node_idx_set.insert(vtx_idx[2]);
        tmp_node_idx_set.insert(vtx_idx[3]);

        sol.assign(0.0);
        tet.set_vertices(elem_vtx[0], elem_vtx[1], elem_vtx[2], elem_vtx[3]);
        const int error = tet.barycentric_coordinate(pnt, sol, tol, 1);

        if ((error == 0) || (error == 2)) {
          sol_dist = tet.barycentric_distance(sol);

          if (sol_dist < tol)
            iterate = false;

          if (sol_dist < best_sol_dist) {
            best_elem_idx = elem_idx;
            best_sol_dist = sol_dist;
            best_sol = sol;
          }
        }
      }
    }

    new_node_idx_set.clear();
    new_node_idx_set.insert(tmp_node_idx_set.begin(), tmp_node_idx_set.end());
  }

  return mt_mixed_tuple<mt_idx_t, mt_real>{best_elem_idx, best_sol_dist};
}

mt_idx_t find_xyz_coord(vec3r &pnt, const vec3r &uvc, const mt_idx_t closest_node_idx, const short ven_idx,
                      const mt_meshdata &mesh, const mt_vector<vec3r> &UVC_data,
                      const mt_vector<mt_real> &cart_xyz, const mt_mask & is_LV,
                      std::size_t max_it)
{
  // LV (-1) or RV (1)
  assert((ven_idx == -1) || (ven_idx == 1));

  const mt_real tol = 1.0e-8;

  MT_USET<mt_idx_t> node_idx_set;     // already considered nodes
  MT_USET<mt_idx_t> new_node_idx_set; // new nodes to consider
  MT_USET<mt_idx_t> elem_idx_set;     // already considered elements
  new_node_idx_set.insert(closest_node_idx);

  mt_idx_t elem_idx, elem_cnt, elem_dsp;
  mt_idx_t node_idx, node_dsp;

  std::array<mt_idx_t, 8> vtx_idx;  // vertex indices of element
  std::array<vec3r, 8> elem_vtx;  // element vertices (UVC or XYZ)

  mt_idx_t best_elem_idx;             // index of the element with "best" solution
  vec3r sol, best_sol;              // solution and best solution
  mt_real sol_dist, best_sol_dist;  // distance of solution and best solution

  std::size_t count;
  bool iterate;
  TetraShape tet;

  best_elem_idx = -1;
  best_sol_dist = std::numeric_limits<mt_real>::infinity();
  iterate = true;
  count = 0;

  MT_USET<mt_idx_t> tmp_node_idx_set;

  while ((iterate) && (count<max_it)) {
    count++;
    tmp_node_idx_set.clear();

    for (auto it=new_node_idx_set.begin(); it!=new_node_idx_set.end() && (iterate); it++)
    {
      node_idx = *it;

      // if new node index was already considered
      // continue with next node
      if (node_idx_set.count(node_idx))
        continue;
      else // add to considered nodes
        node_idx_set.insert(node_idx);

      // get attached elements
      elem_cnt = mesh.n2e_cnt[node_idx];
      elem_dsp = mesh.n2e_dsp[node_idx];

      for (mt_idx_t j=0; (j<elem_cnt) && (iterate); j++)
      {
        elem_idx = mesh.n2e_con[elem_dsp+j];

        // if new element was already considered
        // continue with next element
        if (elem_idx_set.count(elem_idx))
          continue;
        else // add to considered elements
          elem_idx_set.insert(elem_idx);

        node_dsp = mesh.e2n_dsp[elem_idx];

        // ###########################
        // TODO[MG]: generalize !!!!
        // ###########################

        vtx_idx[0] = mesh.e2n_con[node_dsp+0];
        vtx_idx[1] = mesh.e2n_con[node_dsp+1];
        vtx_idx[2] = mesh.e2n_con[node_dsp+2];
        vtx_idx[3] = mesh.e2n_con[node_dsp+3];

        if (ven_idx == -1) // We are in the LV
        {
          // exclude elements which are not pure LV elements
          if (is_LV.count(vtx_idx[0]) == 0 ||
              is_LV.count(vtx_idx[1]) == 0 ||
              is_LV.count(vtx_idx[2]) == 0 ||
              is_LV.count(vtx_idx[3]) == 0)
            continue;

          // get uvc-vertices
          elem_vtx[0] = UVC_data[vtx_idx[0]];
          elem_vtx[1] = UVC_data[vtx_idx[1]];
          elem_vtx[2] = UVC_data[vtx_idx[2]];
          elem_vtx[3] = UVC_data[vtx_idx[3]];

          // if the phi coordinate is negative and the
          // current element is in the transition region (-PI -> PI)
          // switch the sign of the phi component
          if ((uvc.y < 0.0) && (elem_vtx[0].y > (1.0-1.0e-6)*MT_PI)) elem_vtx[0].y *= -1.0;
          if ((uvc.y < 0.0) && (elem_vtx[1].y > (1.0-1.0e-6)*MT_PI)) elem_vtx[1].y *= -1.0;
          if ((uvc.y < 0.0) && (elem_vtx[2].y > (1.0-1.0e-6)*MT_PI)) elem_vtx[2].y *= -1.0;
          if ((uvc.y < 0.0) && (elem_vtx[3].y > (1.0-1.0e-6)*MT_PI)) elem_vtx[3].y *= -1.0;

          tmp_node_idx_set.insert(vtx_idx[0]);
          tmp_node_idx_set.insert(vtx_idx[1]);
          tmp_node_idx_set.insert(vtx_idx[2]);
          tmp_node_idx_set.insert(vtx_idx[3]);
        }
        else  // We are in the RV
        {
          // exclude pure LV element
          if (is_LV.count(vtx_idx[0]) &&
              is_LV.count(vtx_idx[1]) &&
              is_LV.count(vtx_idx[2]) &&
              is_LV.count(vtx_idx[3]))
            continue;

          // get uvc-vertices
          elem_vtx[0] = UVC_data[vtx_idx[0]];
          elem_vtx[1] = UVC_data[vtx_idx[1]];
          elem_vtx[2] = UVC_data[vtx_idx[2]];
          elem_vtx[3] = UVC_data[vtx_idx[3]];

          // if it is a LV node, set phi component to +- MT_PI*0.5
          // else add it to the search nodes
          if (is_LV.count(vtx_idx[0])) {
            if      (elem_vtx[0].y < 0.0) elem_vtx[0].y = -MT_PI*0.5;
            else if (elem_vtx[0].y > 0.0) elem_vtx[0].y = +MT_PI*0.5;
          }
          else
            tmp_node_idx_set.insert(vtx_idx[0]);

          if (is_LV.count(vtx_idx[1])) {
            if      (elem_vtx[1].y < 0.0) elem_vtx[1].y = -MT_PI*0.5;
            else if (elem_vtx[1].y > 0.0) elem_vtx[1].y = +MT_PI*0.5;
          }
          else
            tmp_node_idx_set.insert(vtx_idx[1]);

          if (is_LV.count(vtx_idx[2])) {
            if      (elem_vtx[2].y < 0.0) elem_vtx[2].y = -MT_PI*0.5;
            else if (elem_vtx[2].y > 0.0) elem_vtx[2].y = +MT_PI*0.5;
          }
          else
            tmp_node_idx_set.insert(vtx_idx[2]);

          if (is_LV.count(vtx_idx[3])) {
            if      (elem_vtx[3].y < 0.0) elem_vtx[3].y = -MT_PI*0.5;
            else if (elem_vtx[3].y > 0.0) elem_vtx[3].y = +MT_PI*0.5;
          }
          else
            tmp_node_idx_set.insert(vtx_idx[3]);
        }

        sol.assign(0.0);
        tet.set_vertices(elem_vtx[0], elem_vtx[1], elem_vtx[2], elem_vtx[3]);
        const int error = tet.barycentric_coordinate(uvc, sol, tol, 1);

        if ((error == 0) || (error == 2)) {
          sol_dist = tet.barycentric_distance(sol);

          if (sol_dist < tol)
            iterate = false;

          if (sol_dist < best_sol_dist) {
            best_elem_idx = elem_idx;
            best_sol_dist = sol_dist;
            best_sol = sol;
          }
        }
      }
    }

    new_node_idx_set.clear();
    new_node_idx_set.insert(tmp_node_idx_set.begin(), tmp_node_idx_set.end());
  }

  mt_idx_t ret_value = -2;
  if (best_elem_idx != -1)
  {
#if 0
    if (iterate)
      std::cout << "Approximate solution distance " << best_sol_dist << std::endl;
#endif
    node_dsp = mesh.e2n_dsp[best_elem_idx];

    vtx_idx[0] = mesh.e2n_con[node_dsp+0];
    vtx_idx[1] = mesh.e2n_con[node_dsp+1];
    vtx_idx[2] = mesh.e2n_con[node_dsp+2];
    vtx_idx[3] = mesh.e2n_con[node_dsp+3];

    elem_vtx[0].get(cart_xyz.data() + vtx_idx[0]*3);
    elem_vtx[1].get(cart_xyz.data() + vtx_idx[1]*3);
    elem_vtx[2].get(cart_xyz.data() + vtx_idx[2]*3);
    elem_vtx[3].get(cart_xyz.data() + vtx_idx[3]*3);

    tet.set_vertices(elem_vtx[0], elem_vtx[1], elem_vtx[2], elem_vtx[3]);
    pnt = tet.eval_shape(best_sol);

    ret_value = (iterate) ? -1 : 0;
  }
  return ret_value;
}

void get_xyz_from_UVCs(const mt_meshdata & mesh,
                       const mt_vector<vec3r> & UVCs_vec, const mt_mask & is_LV,
                       const mt_vector<mt_real> & coords, const mt_vector<bool> & coord_is_lv,
                       mt_vector<mt_idx_t> & vtxlist, mt_vector<vec3r> & ptslist, size_t max_it)
{
  if (!(max_it>0)) max_it = 1;
  size_t got_exact_coords = 0, got_approx_coords = 0, got_clamped_coords = 0;

  mt_vector<vec3r> UVCs_lv, UVCs_rv;
  mt_vector<int> lv_UVC_idx, rv_UVC_idx;

  UVCs_lv.reserve(UVCs_vec.size() / 2), UVCs_rv.reserve(UVCs_vec.size() / 2);
  lv_UVC_idx.reserve(UVCs_vec.size() / 2), rv_UVC_idx.reserve(UVCs_vec.size() / 2);

  for(size_t i=0; i < UVCs_vec.size(); i++) {
    if(is_LV.count(i)) {
      lv_UVC_idx.push_back(i);
      UVCs_lv.push_back(UVCs_vec[i]);
    } else {
      rv_UVC_idx.push_back(i);
      UVCs_rv.push_back(UVCs_vec[i]);
    }
  }

  // count how many lv / rv coord inputs
  mt_vector<int> lv_coord_idx, rv_coord_idx;
  lv_coord_idx.reserve(coord_is_lv.size() / 2), rv_coord_idx.reserve(coord_is_lv.size() / 2);

  for(size_t i=0; i < coord_is_lv.size(); i++)
    if(coord_is_lv[i]) lv_coord_idx.push_back(i);
    else               rv_coord_idx.push_back(i);

  const mt_real inf = std::numeric_limits<mt_real>::infinity();
  const vec3r pnt_inf(inf, inf, inf);

#if 0
  std::cout << "mesh UVCs consist of:" << std::endl;
  std::cout << UVCs_lv.size() << " LV coordinates" << std::endl;
  std::cout << UVCs_rv.size() << " RV coordinates" << std::endl;

  std::cout << "input consists of:" << std::endl;
  std::cout << lv_coord_idx.size() << " LV coordinates" << std::endl;
  std::cout << rv_coord_idx.size() << " RV coordinates" << std::endl;
#endif

  vtxlist.resize(coord_is_lv.size());
  ptslist.resize(coord_is_lv.size());

  //left ventricle
  if (UVCs_lv.size() > 0) {
    kdtree tree_lv(10);

    if(lv_coord_idx.size())
      tree_lv.build_vertex_tree(UVCs_lv);

    for(int i : lv_coord_idx)
    {
      vec3r ctr(coords.data() + i*3);
      vec3r dummy;
      mt_real dummy_len;
      int idx_lv = 0, idx = 0;

      // make sure the coordinate is within
      // the ranges
      vec3r uvc;
      uvc.x = clamp<mt_real>(ctr.x, 0.0, 1.0);      // rho
      uvc.y = clamp<mt_real>(ctr.y, -MT_PI, MT_PI); // phi
      uvc.z = clamp<mt_real>(ctr.z, 0.0, 1.0);      // z

#ifndef NDEBUG
      if ((uvc-ctr).length() > 0.01)
        std::cout << "Clamped LV UVC coordinate: ( "
                  << ctr.z << " " << ctr.x << " " << ctr.y << " ) -> ( "
                  << uvc.z << " " << uvc.x << " " << uvc.y << " )" << std::endl;
#endif

      //Closest vertex in uvc coordinate
      tree_lv.closest_vertex(uvc, idx_lv, dummy, dummy_len);
      idx = lv_UVC_idx[idx_lv];
      vtxlist[i] = idx;

      // find containing element
      ptslist[i].get(mesh.xyz.data() + idx*3);
      const mt_idx_t found = find_xyz_coord(dummy, uvc, idx, -1, mesh, UVCs_vec, mesh.xyz, is_LV, max_it);
      if ((found == 0) || (found == -1))
        ptslist[i] = dummy;

#if 0
      if (found == -1)       // approximate XYZ coordinate
        std::cout << "Only approximate XYZ coordinate found in LV, coord " << i << std::endl;
      else if (found == -2)  // no XYZ coordinate
        std::cout << "No XYZ coordinate found in LV, coord " << i << std::endl;
#endif
      switch(found) {
        case 0: got_exact_coords++; break;
        case -1: got_approx_coords++; break;
        case -2: got_clamped_coords++; break;
        default: break;
      }
    }
  } else {
    for(int i : lv_coord_idx) {
      vtxlist[i] = -1;
      ptslist[i] = pnt_inf;
    }
  }

  //right ventricle
  if (UVCs_rv.size() > 0) {
    kdtree tree_rv(10);

    if(rv_coord_idx.size())
      tree_rv.build_vertex_tree(UVCs_rv);

    for(int i : rv_coord_idx)
    {
      vec3r ctr(coords.data() + i*3);
      vec3r dummy;
      mt_real dummy_len;
      int idx_rv = 0, idx = 0;

      // make sure the coordinate is within
      // the ranges
      vec3r uvc;
      uvc.x = clamp<mt_real>(ctr.x, 0.0, 1.0);              // rho
      uvc.y = clamp<mt_real>(ctr.y, -MT_PI*0.5, MT_PI*0.5); // phi
      uvc.z = clamp<mt_real>(ctr.z, 0.0, 1.0);              // z

#ifndef NDEBUG
      if ((uvc-ctr).length() > 0.01)
        std::cout << "Clamped RV UVC coordinate: ( "
                  << ctr.z << " " << ctr.x << " " << ctr.y << " ) -> ( "
                  << uvc.z << " " << uvc.x << " " << uvc.y << " )" << std::endl;
#endif

      //Closest vertex in uvc coordinate
      tree_rv.closest_vertex(uvc, idx_rv, dummy, dummy_len);
      idx = rv_UVC_idx[idx_rv];
      vtxlist[i] = idx;

      // find containing element
      ptslist[i].get(mesh.xyz.data() + idx*3);
      const mt_idx_t found = find_xyz_coord(dummy, uvc, idx, +1, mesh, UVCs_vec, mesh.xyz, is_LV, max_it);
      if ((found == 0) || (found == -1))
        ptslist[i] = dummy;

#if 0
      if (found == -1)       // approximate XYZ coordinate
        std::cout << "Only approximate XYZ coordinate found in RV, coord " << i << std::endl;
      else if (found == -2)  // no XYZ coordinate
        std::cout << "No XYZ coordinate found in RV, coord " << i << std::endl;
#endif
      switch(found) {
        case 0: got_exact_coords++; break;
        case -1: got_approx_coords++; break;
        case -2: got_clamped_coords++; break;
        default: break;
      }
    }
  } else {
    for(int i : rv_coord_idx) {
      vtxlist[i] = -1;
      ptslist[i] = pnt_inf;
    }
  }

  printf("UVC -> XYZ: %zu exact, %zu approx, %zu clamped \n", got_exact_coords, got_approx_coords, got_clamped_coords);
}


mt_idx_t find_xyz_coord(vec3r &pnt, const vec3r &ucc, const mt_idx_t closest_node_idx, const short ven_idx,
                        const mt_meshdata &mesh, const mt_vector<vec3r> &UCCs_vec, const mt_vector<int8_t>& UCCs_chmbr,
                        const mt_vector<mt_real> &xyz, std::size_t max_it)
{
  // LA (-2) or LV (-1) or RV (1) or RA (2)
  assert((ven_idx == -2) || (ven_idx == -1) || (ven_idx == 1) || (ven_idx == 2));

  const mt_real tol = 1.0e-8;

  MT_USET<mt_idx_t> node_idx_set;     // already considered nodes
  MT_USET<mt_idx_t> new_node_idx_set; // new nodes to consider
  MT_USET<mt_idx_t> elem_idx_set;     // already considered elements
  new_node_idx_set.insert(closest_node_idx);

  mt_idx_t elem_idx, elem_cnt, elem_dsp;
  mt_idx_t node_idx, node_dsp;

  std::array<mt_idx_t, 8> vtx_idx;  // vertex indices of element
  std::array<vec3r, 8> elem_vtx;    // element vertices (UVC or XYZ)

  mt_idx_t best_elem_idx;           // index of the element with "best" solution
  vec3r sol, best_sol;              // solution and best solution
  mt_real sol_dist, best_sol_dist;  // distance of solution and best solution

  std::size_t count;
  bool iterate;
  TetraShape tet;

  best_elem_idx = -1;
  best_sol_dist = std::numeric_limits<mt_real>::infinity();
  iterate = true;
  count = 0;

  MT_USET<mt_idx_t> tmp_node_idx_set;

  while ((iterate) && (count<max_it)) {
    count++;
    tmp_node_idx_set.clear();

    for (auto it=new_node_idx_set.begin(); it!=new_node_idx_set.end() && (iterate); it++)
    {
      node_idx = *it;

      // if new node index was already considered
      // continue with next node
      if (node_idx_set.count(node_idx))
        continue;
      else // add to considered nodes
        node_idx_set.insert(node_idx);

      // get attached elements
      elem_cnt = mesh.n2e_cnt[node_idx];
      elem_dsp = mesh.n2e_dsp[node_idx];

      for (mt_idx_t j=0; (j<elem_cnt) && (iterate); j++)
      {
        elem_idx = mesh.n2e_con[elem_dsp+j];

        // if new element was already considered
        // continue with next element
        if (elem_idx_set.count(elem_idx))
          continue;
        else // add to considered elements
          elem_idx_set.insert(elem_idx);

        node_dsp = mesh.e2n_dsp[elem_idx];

        // ###########################
        // TODO[MG]: generalize !!!!
        // ###########################

        vtx_idx[0] = mesh.e2n_con[node_dsp+0];
        vtx_idx[1] = mesh.e2n_con[node_dsp+1];
        vtx_idx[2] = mesh.e2n_con[node_dsp+2];
        vtx_idx[3] = mesh.e2n_con[node_dsp+3];

        if (ven_idx == -1) // We are in the LV
        {
          // exclude elements which are not pure LV elements
          if ((UCCs_chmbr[vtx_idx[0]] != -1) ||
              (UCCs_chmbr[vtx_idx[1]] != -1) ||
              (UCCs_chmbr[vtx_idx[2]] != -1) ||
              (UCCs_chmbr[vtx_idx[3]] != -1))
            continue;

          // get uvc-vertices
          elem_vtx[0] = UCCs_vec[vtx_idx[0]];
          elem_vtx[1] = UCCs_vec[vtx_idx[1]];
          elem_vtx[2] = UCCs_vec[vtx_idx[2]];
          elem_vtx[3] = UCCs_vec[vtx_idx[3]];

          // if the phi coordinate is negative and the
          // current element is in the transition region (-PI -> PI)
          // switch the sign of the phi component
          if ((ucc.y < 0.0) && (elem_vtx[0].y > (1.0-1.0e-6)*MT_PI)) elem_vtx[0].y *= -1.0;
          if ((ucc.y < 0.0) && (elem_vtx[1].y > (1.0-1.0e-6)*MT_PI)) elem_vtx[1].y *= -1.0;
          if ((ucc.y < 0.0) && (elem_vtx[2].y > (1.0-1.0e-6)*MT_PI)) elem_vtx[2].y *= -1.0;
          if ((ucc.y < 0.0) && (elem_vtx[3].y > (1.0-1.0e-6)*MT_PI)) elem_vtx[3].y *= -1.0;

          tmp_node_idx_set.insert(vtx_idx[0]);
          tmp_node_idx_set.insert(vtx_idx[1]);
          tmp_node_idx_set.insert(vtx_idx[2]);
          tmp_node_idx_set.insert(vtx_idx[3]);
        }
        else if (ven_idx == 1)  // We are in the RV
        {
          // exclude elements which are not pure RV elements
          if ((UCCs_chmbr[vtx_idx[0]] != 1) ||
              (UCCs_chmbr[vtx_idx[1]] != 1) ||
              (UCCs_chmbr[vtx_idx[2]] != 1) ||
              (UCCs_chmbr[vtx_idx[3]] != 1))
            continue;

          // get uvc-vertices
          elem_vtx[0] = UCCs_vec[vtx_idx[0]];
          elem_vtx[1] = UCCs_vec[vtx_idx[1]];
          elem_vtx[2] = UCCs_vec[vtx_idx[2]];
          elem_vtx[3] = UCCs_vec[vtx_idx[3]];

          tmp_node_idx_set.insert(vtx_idx[0]);
          tmp_node_idx_set.insert(vtx_idx[1]);
          tmp_node_idx_set.insert(vtx_idx[2]);
          tmp_node_idx_set.insert(vtx_idx[3]);
        }
        else if (ven_idx == -2) // We are in the LA
        {
          // exclude elements which are not pure LA elements
          if ((UCCs_chmbr[vtx_idx[0]] != -2) ||
              (UCCs_chmbr[vtx_idx[1]] != -2) ||
              (UCCs_chmbr[vtx_idx[2]] != -2) ||
              (UCCs_chmbr[vtx_idx[3]] != -2))
            continue;

          // get uac-vertices
          elem_vtx[0] = UCCs_vec[vtx_idx[0]];
          elem_vtx[1] = UCCs_vec[vtx_idx[1]];
          elem_vtx[2] = UCCs_vec[vtx_idx[2]];
          elem_vtx[3] = UCCs_vec[vtx_idx[3]];

          tmp_node_idx_set.insert(vtx_idx[0]);
          tmp_node_idx_set.insert(vtx_idx[1]);
          tmp_node_idx_set.insert(vtx_idx[2]);
          tmp_node_idx_set.insert(vtx_idx[3]);
        }
        else if (ven_idx == 2) // We are in the RA
        {
          // exclude elements which are not pure RA elements
          if ((UCCs_chmbr[vtx_idx[0]] != 2) ||
              (UCCs_chmbr[vtx_idx[1]] != 2) ||
              (UCCs_chmbr[vtx_idx[2]] != 2) ||
              (UCCs_chmbr[vtx_idx[3]] != 2))
            continue;

          // get uac-vertices
          elem_vtx[0] = UCCs_vec[vtx_idx[0]];
          elem_vtx[1] = UCCs_vec[vtx_idx[1]];
          elem_vtx[2] = UCCs_vec[vtx_idx[2]];
          elem_vtx[3] = UCCs_vec[vtx_idx[3]];

          tmp_node_idx_set.insert(vtx_idx[0]);
          tmp_node_idx_set.insert(vtx_idx[1]);
          tmp_node_idx_set.insert(vtx_idx[2]);
          tmp_node_idx_set.insert(vtx_idx[3]);
        }

        sol.assign(0.0);
        tet.set_vertices(elem_vtx[0], elem_vtx[1], elem_vtx[2], elem_vtx[3]);
        const int error = tet.barycentric_coordinate(ucc, sol, tol, 1);

        if ((error == 0) || (error == 2)) {
          sol_dist = tet.barycentric_distance(sol);

          if (sol_dist < tol)
            iterate = false;

          if (sol_dist < best_sol_dist) {
            best_elem_idx = elem_idx;
            best_sol_dist = sol_dist;
            best_sol = sol;
          }
        }
      }
    }

    new_node_idx_set.clear();
    new_node_idx_set.insert(tmp_node_idx_set.begin(), tmp_node_idx_set.end());
  }

  mt_idx_t ret_value = -2;
  if (best_elem_idx != -1)
  {
#if 0
    if (iterate)
      std::cout << "Approximate solution distance " << best_sol_dist << std::endl;
#endif
    node_dsp = mesh.e2n_dsp[best_elem_idx];

    vtx_idx[0] = mesh.e2n_con[node_dsp+0];
    vtx_idx[1] = mesh.e2n_con[node_dsp+1];
    vtx_idx[2] = mesh.e2n_con[node_dsp+2];
    vtx_idx[3] = mesh.e2n_con[node_dsp+3];

    elem_vtx[0].get(xyz.data() + vtx_idx[0]*3);
    elem_vtx[1].get(xyz.data() + vtx_idx[1]*3);
    elem_vtx[2].get(xyz.data() + vtx_idx[2]*3);
    elem_vtx[3].get(xyz.data() + vtx_idx[3]*3);

    tet.set_vertices(elem_vtx[0], elem_vtx[1], elem_vtx[2], elem_vtx[3]);
    pnt = tet.eval_shape(best_sol);

    ret_value = (iterate) ? -1 : 0;
  }
  return ret_value;
}

void get_xyz_from_UCCs(const mt_meshdata & mesh,
                       const mt_vector<vec3r> & UCCs_vec, const mt_vector<int8_t> & UCCs_chmbr,
                       const mt_vector<mt_real> & coords, const mt_vector<int8_t> & coords_chmbr,
                       mt_vector<mt_idx_t> & vtxlist, mt_vector<vec3r> & ptslist, size_t max_it)
{
  if (!(max_it>0)) max_it = 1;
  size_t got_exact_coords = 0, got_approx_coords = 0, got_clamped_coords = 0;

  mt_vector<vec3r> UCCs_lv, UCCs_rv, UCCs_la, UCCs_ra;
  mt_vector<int> lv_UCC_idx, rv_UCC_idx, la_UCC_idx, ra_UCC_idx;

  UCCs_lv.reserve(UCCs_vec.size() / 2),
  lv_UCC_idx.reserve(UCCs_vec.size() / 2);

  UCCs_rv.reserve(UCCs_vec.size() / 2);
  rv_UCC_idx.reserve(UCCs_vec.size() / 2);

  UCCs_la.reserve(UCCs_vec.size() / 2),
  la_UCC_idx.reserve(UCCs_vec.size() / 2);

  UCCs_ra.reserve(UCCs_vec.size() / 2);
  ra_UCC_idx.reserve(UCCs_vec.size() / 2);

  for (size_t i=0; i < UCCs_vec.size(); i++) {
    if (UCCs_chmbr[i] == -2) {
      la_UCC_idx.push_back(i);
      UCCs_la.push_back(UCCs_vec[i]);
    } else if (UCCs_chmbr[i] == -1) {
      lv_UCC_idx.push_back(i);
      UCCs_lv.push_back(UCCs_vec[i]);
    } else if (UCCs_chmbr[i] == +1) {
      rv_UCC_idx.push_back(i);
      UCCs_rv.push_back(UCCs_vec[i]);
    } else if (UCCs_chmbr[i] == +2) {
      ra_UCC_idx.push_back(i);
      UCCs_ra.push_back(UCCs_vec[i]);
    }
  }

  // count how many la / lv / rv / ra coord inputs
  mt_vector<int> lv_coord_idx, rv_coord_idx, la_coord_idx, ra_coord_idx;
  lv_coord_idx.reserve(coords.size() / 2);
  rv_coord_idx.reserve(coords.size() / 2);
  la_coord_idx.reserve(coords.size() / 2);
  ra_coord_idx.reserve(coords.size() / 2);

  for (size_t i=0; i < coords_chmbr.size(); i++) {
    if (coords_chmbr[i] == -2) la_coord_idx.push_back(i);
    else if (coords_chmbr[i] == -1) lv_coord_idx.push_back(i);
    else if (coords_chmbr[i] == +1) rv_coord_idx.push_back(i);
    else if (coords_chmbr[i] == +2) ra_coord_idx.push_back(i);
  }

  const mt_real inf = std::numeric_limits<mt_real>::infinity();
  const vec3r pnt_inf(inf, inf, inf);

#if 0
  std::cout << "mesh UVCs consist of:" << std::endl;
  std::cout << UVCs_lv.size() << " LV coordinates" << std::endl;
  std::cout << UVCs_rv.size() << " RV coordinates" << std::endl;

  std::cout << "input consists of:" << std::endl;
  std::cout << lv_coord_idx.size() << " LV coordinates" << std::endl;
  std::cout << rv_coord_idx.size() << " RV coordinates" << std::endl;
#endif

  vtxlist.resize(coords_chmbr.size());
  ptslist.resize(coords_chmbr.size());

  auto do_ventr_mapping = [&](mt_vector<int> & coord_idx, mt_vector<vec3r> & UCCs, mt_vector<int> & UCC_idx, int chmb) {
    kdtree tree(10);
    tree.build_vertex_tree(UCCs);
    const mt_real phi_lim = MT_PI*((chmb==1)?0.5:1.0);

#ifdef OPENMP
#pragma omp parallel for schedule(guided) reduction(+: got_exact_coords, got_approx_coords, got_clamped_coords)
#endif
    for(size_t ii=0; ii < coord_idx.size(); ii++)
    {
      int i = coord_idx[ii];
      vec3r ctr(coords.data() + i*3);
      vec3r dummy;
      mt_real dummy_len;
      int idx = 0;

      // make sure the coordinate is within
      // the ranges
      vec3r uvc;
      uvc.x = clamp<mt_real>(ctr.x, 0.0, 1.0);          // rho
      uvc.y = clamp<mt_real>(ctr.y, -phi_lim, phi_lim); // phi
      uvc.z = clamp<mt_real>(ctr.z, 0.0, 1.0);          // z

      //Closest vertex in uvc coordinate
      tree.closest_vertex(uvc, idx, dummy, dummy_len);
      idx = UCC_idx[idx];
      vtxlist[i] = idx;

      // find containing element
      ptslist[i].get(mesh.xyz.data() + idx*3);
      const mt_idx_t found = find_xyz_coord(dummy, uvc, idx, chmb, mesh, UCCs_vec, UCCs_chmbr, mesh.xyz, max_it);
      if ((found == 0) || (found == -1))
        ptslist[i] = dummy;

      switch(found) {
        case 0: got_exact_coords++; break;
        case -1: got_approx_coords++; break;
        case -2: got_clamped_coords++; break;
        default: break;
      }
    }
  };

  auto do_atrial_mapping = [&](mt_vector<int> & coord_idx, mt_vector<vec3r> & UCCs, mt_vector<int> & UCC_idx, int chmb) {
    kdtree tree(10);
    tree.build_vertex_tree(UCCs);

#ifdef OPENMP
#pragma omp parallel for schedule(guided) reduction(+: got_exact_coords, got_approx_coords, got_clamped_coords)
#endif
    for(size_t ii=0; ii < coord_idx.size(); ii++)
    {
      int i = coord_idx[ii];
      vec3r ctr(coords.data() + i*3);
      vec3r dummy;
      mt_real dummy_len;
      int idx = 0;

      // make sure the coordinate is within
      // the ranges
      vec3r uac;
      uac.x = clamp<mt_real>(ctr.x, 0.0, 1.0);  // alpha
      uac.y = clamp<mt_real>(ctr.y, 0.0, 1.0);  // beta
      uac.z = clamp<mt_real>(ctr.z, 0.0, 1.0);  // gamma

      //Closest vertex in uvc coordinate
      tree.closest_vertex(uac, idx, dummy, dummy_len);
      idx = UCC_idx[idx];
      vtxlist[i] = idx;

      // find containing element
      ptslist[i].get(mesh.xyz.data() + idx*3);
      const mt_idx_t found = find_xyz_coord(dummy, uac, idx, chmb, mesh, UCCs_vec, UCCs_chmbr, mesh.xyz, max_it);
      if ((found == 0) || (found == -1))
        ptslist[i] = dummy;

      switch(found) {
        case 0: got_exact_coords++; break;
        case -1: got_approx_coords++; break;
        case -2: got_clamped_coords++; break;
        default: break;
      }
    }
  };


  //left ventricle
  if (UCCs_lv.size() > 0 && lv_coord_idx.size() > 0) {
    do_ventr_mapping(lv_coord_idx, UCCs_lv, lv_UCC_idx, -1);
  } else {
    for(int i : lv_coord_idx) {
      vtxlist[i] = -1;
      ptslist[i] = pnt_inf;
    }
  }

  //right ventricle
  if (UCCs_rv.size() > 0 && rv_coord_idx.size() > 0) {
    do_ventr_mapping(rv_coord_idx, UCCs_rv, rv_UCC_idx, 1);
  } else {
    for(int i : rv_coord_idx) {
      vtxlist[i] = -1;
      ptslist[i] = pnt_inf;
    }
  }

  //left atria
  if (UCCs_la.size() > 0 && la_coord_idx.size() > 0) {
    do_atrial_mapping(la_coord_idx, UCCs_la, la_UCC_idx, -2);
  } else {
    for(int i : la_coord_idx) {
      vtxlist[i] = -1;
      ptslist[i] = pnt_inf;
    }
  }

  //right atria
  if (UCCs_ra.size() > 0 && ra_coord_idx.size() > 0) {
    do_atrial_mapping(ra_coord_idx, UCCs_ra, ra_UCC_idx, 2);
  } else {
    for(int i : ra_coord_idx) {
      vtxlist[i] = -1;
      ptslist[i] = pnt_inf;
    }
  }

  printf("UCC -> XYZ: %zu exact, %zu approx, %zu clamped \n", got_exact_coords, got_approx_coords, got_clamped_coords);
}

bool check_split_operations(mt_vector<MT_USET<mt_tag_t>> & tagsA,
                            mt_vector<MT_USET<mt_tag_t>> & tagsB,
                            std::string* errstr)
{
  char strbuff[1024];
  size_t numsplits = tagsA.size();

  for(size_t sidx = 0; sidx < numsplits; sidx++) {
    auto & ref_a = tagsA[sidx];

    for(mt_tag_t ra : ref_a) {
      for(size_t cidx = 0; cidx < numsplits; cidx++) {
        if(cidx != sidx) {
          if(tagsA[cidx].count(ra)) {
            snprintf(strbuff, 1024, "Split error: %d is on left-hand-side in split %d and also in split %d. This is not allowed!", (int)ra, (int)sidx+1, (int)cidx+1);

            if(errstr) *errstr = strbuff;
            else {
              fprintf(stderr, "%s\n", strbuff);
            }

            return false;
          }
        }
      }
    }
  }

  for(size_t sidx = 0; sidx < numsplits; sidx++) {
    auto & ref_a = tagsA[sidx];
    auto & ref_b = tagsB[sidx];

    for(mt_tag_t rb : ref_b) {
      for(size_t cidx = 0; cidx < numsplits; cidx++) {
        if(cidx != sidx) {
          if(tagsA[cidx].count(rb)) {
            auto & cur_b = tagsB[cidx];

            for(mt_tag_t ra : cur_b) {
              if(ref_a.count(ra)) {
                snprintf(strbuff, 1024, "Split error: %d splits from %d in split %d, but %d also splits from %d in split %d. This is not allowed!",
                        (int)ra, (int)rb, (int)sidx+1, (int)rb, (int)ra, (int)cidx+1);

                if(errstr) *errstr = strbuff;
                else {
                  fprintf(stderr, "%s\n", strbuff);
                }


                return false;
              }
            }
          }
        }
      }
    }
  }

  return true;
}


bool parse_splitlist_operation(std::string op,
                               mt_vector<MT_USET<mt_tag_t>> & tagsA,
                               mt_vector<MT_USET<mt_tag_t>> & tagsB)
{
  mt_vector<std::string> splitlist;

  split_string(op, '/', splitlist);
  size_t numsplits = splitlist.size();

  tagsA.resize(numsplits);
  tagsB.resize(numsplits);

  // first load tag groups for all splits into tagsA and tagsB ----------------
  for(size_t i=0; i<numsplits; i++) {
    // std::cout << splitlist[i] << std::endl;
    mt_vector<std::string> tlist;
    split_string(splitlist[i], ':', tlist);
    if(tlist.size() == 2) {
      mt_vector<std::string> alist, blist;
      split_string(tlist[0], ',', alist);
      for(size_t j=0; j<alist.size(); j++) tagsA[i].insert(atoi(alist[j].c_str()));
      split_string(tlist[1], ',', blist);
      for(size_t j=0; j<blist.size(); j++) tagsB[i].insert(atoi(blist[j].c_str()));
    } else {
      std::cerr << "Split error: Operation " << splitlist[i] << " has bad syntax." << std::endl;
      return false;
    }
  }

  bool ret = check_split_operations(tagsA, tagsB);
  return ret;
}

void generate_splitlist(const mt_meshdata & mesh,
                        const mt_vector<MT_USET<mt_tag_t>> & tagsA,
                        const mt_vector<MT_USET<mt_tag_t>> & tagsB,
                        const mt_idx_t start_idx,
                        mt_vector<split_item> & list)
{
  assert(tagsA.size() == tagsB.size());
  size_t numsplits = tagsA.size();
  mt_idx_t Nidx = start_idx;

  list.resize(0);
  for(size_t i=0; i<numsplits; i++) {
    mt_meshgraph mgA, mgB;
    extract_tagged_meshgraph(tagsA[i], mesh, mgA);
    extract_tagged_meshgraph(tagsB[i], mesh, mgB);

    add_interface_to_splitlist(mesh, mgA, mgB, Nidx, list);
  }
}

/// set number of fiber directions (num_fib can be 1 or 2)
void set_num_fibers(const size_t nele, mt_vector<mt_fib_t> & lon, short new_num_fib)
{
  check_condition(new_num_fib == 1 || new_num_fib == 2, "new_num_fib either 1 or 2", __func__);

  short  old_num_fib = lon.size() == nele * 6 ? 2 : 1;

  if(new_num_fib < old_num_fib) {
    mt_vector<mt_fib_t> old_lon(lon);
    lon.resize(nele*3);

    for(size_t i=0; i<nele; i++) {
      lon[i*3+0] = old_lon[i*6+0];
      lon[i*3+1] = old_lon[i*6+1];
      lon[i*3+2] = old_lon[i*6+2];
    }
  } else if (new_num_fib > old_num_fib) {
    mt_vector<mt_fib_t> old_lon(lon);
    lon.assign(nele*6, mt_real(0));

    for(size_t i=0; i<nele; i++) {
      lon[i*6+0] = old_lon[i*3+0];
      lon[i*6+1] = old_lon[i*3+1];
      lon[i*6+2] = old_lon[i*3+2];
    }
  }
}
