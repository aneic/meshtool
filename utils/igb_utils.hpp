/**
* @file igb_utils.hpp
*
* @brief IGB format specific IO utils.
* @author Aurel Neic and Elias Karabelas
* @version
* @date 2017-08-04
*/

#ifndef _IGB_UTILS_H
#define _IGB_UTILS_H

#include <cstddef>
#include <stdio.h>    /* printf */
#include <stdlib.h>
#include <string.h>
#include <strings.h>  // needed for strcasecmp()
#include <assert.h>
#include <errno.h>

#include <iostream>
#include <cmath>
#include <limits>
#include <type_traits>
#include <algorithm>
#include <sstream>
#include <string>
#include <map>
#include "mt_vector.h"

// -------------- Bits de statut pour Header_Read et Header_Write ------ */
#define MOT_CLEF_INV 2
#define GRANDEUR_INV 4

/*
 * Types de trames
 */
#define MIN_TRAME 0
#define CC8    0
#define CC4    1
#define HEX   2
#define HEXEDGES  3
#define HEXBRIDGES  4
#define HEXLINES  5
#define HEX2    6
#define MAX_TRAME 6
#define NTRAMES   7

#define LF  0x0A
#define FF  0x0C
// #define CR  0x0D
#define MY_IGB_HEADERSIZE 1024
#define MLF  0x0A
#define MFF  0x0C
#define MCR  0x0D
#define NALLOC 100

#define     NUL   0
#define     INCONNU 0

#define     IGB_BIG_ENDIAN    666666666
#define     IGB_LITTLE_ENDIAN 777777777
#define     N_SYSTEMES  2

// -------------- Constantes diverses ---------------------------------- */

#define     MAXL      80  //  Longueur maximale d'une ligne d'entete */
#define     N_MAX_ITEMS     30  //  Nombre maximal d'items optionnels */
#define     L_MAX_ITEM      49  //  Longueur maximale pour un item
/* ------------------------ TYPES definition ------------------------------ */
#define     IGB_BYTE      1  /* -- byte ----------------------------------- */
#define     IGB_CHAR      2  /* -- Char ----------------------------------- */
#define     IGB_SHORT     3  /* -- short ---------------------------------- */
#define     IGB_LONG      4  /* -- long ----------------------------------- */
#define     IGB_FLOAT     5  /* -- float ---------------------------------- */
#define     IGB_DOUBLE    6  /* -- Double --------------------------------- */
#define     IGB_COMPLEX   7  /* -- 2 x float (real part, imaginary part) -- */
#define     IGB_D_COMPLEX 8  /* -- 2 x Double (real part, imaginary part) - */
#define     IGB_RGBA      9  /* -- 4 x byte (red, green, blue, alpha) ----- */
#define     IGB_STRUCTURE 10 /* -- Structure ------------------------------ */
#define     IGB_POINTER   11 /* -- void * --------------------------------- */
#define     IGB_LIST      12 /* -- List   --------------------------------- */
#define     IGB_INT       13 /* -- integer -------------------------------- */
#define     IGB_UINT      14 /* -- unsigned integer------------------------ */
#define     IGB_USHORT    15 /* -- unsigned short integer------------------ */
#define     IGB_VEC3_f    16 /* -- 3 X float ------------------------------ */
#define     IGB_VEC3_d    17 /* -- 3 X double ----------------------------- */
#define     IGB_VEC4_f    18 /* -- 4 X float ------------------------------ */
#define     IGB_VEC4_d    19 /* -- 4 X double ----------------------------- */
#define     IGB_VEC9_f    20 /* -- 9 X float  ----------------------------- */
#define     IGB_MIN_TYPE  1
#define     IGB_MAX_TYPE  20

/* define for endedness */
#define IGB_ENDIAN_VAL -1.24e5
#define IGB_LITTLE_END_REP 0,48,242,199

// error codes
#define ERR_EOF_IN_HEADER      1
#define ERR_LINE_TOO_LONG      2
#define ERR_UNPRINTABLE_CHAR   3
#define ERR_IGB_SYNTAX         4
#define ERR_UNDEFINED_X_Y_TYPE 5
#define ERR_SIZE_REDEFINED     6
#define ERR_SIZE_NOT_DEFINED   7
#define WARN_DIM_INCONSISTENT  256


/*
   Definition des types List, bytes, Char, Double, complex d_complex
   et rgba
   */
struct List
{
  long    nitems;
  char    *items;
};

typedef  unsigned char byte;

struct S_Complex
{
  float real, imag;
};

struct D_Complex
{
  double real, imag;
};

#ifndef _COMPLEX_DEFINED
struct complex
{
  float   reel ;
  float   imag ;
};

struct d_complex
{
  double  reel ;
  double  imag ;
};

#define _COMPLEX_DEFINED
#endif
union rgba {
  unsigned  long    l;
  byte    b[4];
};

/* Indice de chaque composante dans le vecteur b[] de l'union rgba */
#define RGBA_ROUGE 3
#define RGBA_VERT  2
#define RGBA_BLEU  1
#define RGBA_ALPHA 0

struct igb_header
{
  std::string filename;

  FILE*  fileptr = NULL;
  long   v_x, v_y, v_z, v_t ;                  //!< dimensions --------------------
  long   v_type ;                              //!< type arithmetique -------------
  long   v_taille ;                            //!< taille des pixels (type STRUCTURE)
  long   v_systeme;                           //!< big or little endian
  long   v_num ;                              //!< numero de la tranche ----------
  long   v_bin ;                              //!< nombre de couleurs ------------
  long   v_trame ;                            //!< trame(connectivite) -----------
  long   v_lut ;                               //!< nombre de bytes table couleurs
  long   v_comp ;                              //!< nombre de bytes table compres.
  float  v_epais ;                            //!< epaiseur d'une tranche --------
  float  v_org_x, v_org_y, v_org_z, v_org_t ; //!< coin sup gauche --------
  float  v_inc_x, v_inc_y, v_inc_z, v_inc_t ; //!< distance entre pixels -
  float  v_dim_x, v_dim_y, v_dim_z, v_dim_t ; //!< dimension totale -------
  //float *v_vect_z ;                           //!< coord z de chaque tranche -----
  char   v_unites_x[41], v_unites_y[41], v_unites_z[41], v_unites_t[41] ;
  //!< unites de mesure --------------
  char   v_unites[41] ;                       //!< unites de mesure pour les valeurs des pixels -----------
  float  v_facteur, v_zero ;                  //!< facteur d'echelle et valeur du zero -
  char   v_struct_desc[41] ;                  //!< description de la structure ---
  char   v_aut_name[41] ;                     //!< nom de l'auteur ---------------
  void*  v_transparent;                        //!< transparent value for data
  // boolean flags to indicate if a default value has been overridden
  bool   bool_x, bool_y, bool_z, bool_t;
  bool   bool_type;
  bool   bool_taille;
  bool   bool_num;
  bool   bool_bin;
  bool   bool_trame;
  bool   bool_lut;
  bool   bool_comp;
  bool   bool_epais;
  bool   bool_org_x, bool_org_y, bool_org_z, bool_org_t;
  bool   bool_inc_x, bool_inc_y, bool_inc_z, bool_inc_t;
  bool   bool_dim_x, bool_dim_y, bool_dim_z, bool_dim_t;
  bool   bool_unites_x, bool_unites_y, bool_unites_z, bool_unites_t;
  bool   bool_unites;
  bool   bool_facteur, bool_zero;
  bool   bool_struct_desc;
  bool   bool_aut_name;
  bool   bool_transparent;
  char   transstr[257];
  bool   igb_header_initialized;
};

inline void close_igb_fileptr(igb_header & igb)
{
  if(igb.fileptr) {
    fclose(igb.fileptr);
    igb.fileptr = NULL;
  }
}


//Check for almost equal of a type
template<class T> inline
typename std::enable_if<!std::numeric_limits<T>::is_integer, bool>::type
    almost_equal(T x, T y, int ulp = 4)
{
    // the machine epsilon has to be scaled to the magnitude of the values used
    // and multiplied by the desired precision in ULPs (units in the last place)
    return std::abs(x-y) <= std::numeric_limits<T>::epsilon() * std::abs(x+y) * ulp
        // unless the result is subnormal
        || std::abs(x-y) < std::numeric_limits<T>::min();
}

static const char* Header_type[IGB_MAX_TYPE+1] =
{
  "",
  "byte",
  "char",
  "short",
  "long",
  "float",
  "double",
  "complex",
  "double_complex",
  "rgba",
  "structure",
  "pointer",
  "list",
  "int",
  "uint",
  "ushort",
  "vec3f",
  "vec3d",
  "vec4f",
  "vec4d",
  "vec9f"
};

//* size of the stored data, not the variable type
static size_t Data_Size[IGB_MAX_TYPE+1] =
{
  0,
  sizeof(byte),
  sizeof(char),
  sizeof(short),
  sizeof(long),
  sizeof(float),
  sizeof(double),
  0,
  0,
  0,
  0,
  sizeof(void*),
  0,
  sizeof(int),
  sizeof(unsigned int),
  sizeof(unsigned short),
  3*sizeof(float),
  3*sizeof(double),
  4*sizeof(float),
  4*sizeof(double),
  9*sizeof(float)
};

/** the number of components for each data type */
static const long Num_Components[IGB_MAX_TYPE+1] =
{
  0, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 1, 0, 1, 1, 1, 3, 3, 4, 4, 9
};

static const long Header_Systeme_No[2] =
{
  IGB_BIG_ENDIAN,
  IGB_LITTLE_ENDIAN
};

static const char* Header_Systeme[2] =
{
  "big_endian",
  "little_endian"
};

//maximum allowed number of keys, at the moment 36
static const char* allowed_keys[] =
{
  "x", "y", "z", "t", "type", "systeme", "taille", "bin", "trame", "num", "comp", "lut", "dim_x", "dim_y",
  "dim_z", "dim_t", "inc_x", "inc_y", "inc_z", "inc_t", "org_x", "org_y", "org_z", "org_t", "unites_x", "unites_y",
  "unites_z", "unites_t", "epais", "unites", "facteur", "zero", "struct", "aut", "transparent"
};

//constant mapping for the keywords in the vector
static const long allowed_keys_in_numbers[] =
{
  0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34
};

inline void make_igb_consistent(igb_header & igb_head)
{
  if(igb_head.bool_dim_x && igb_head.bool_inc_x && igb_head.bool_x)
    if(!almost_equal(igb_head.v_dim_x, igb_head.v_inc_x * (float)((igb_head.v_x -1))))
    {
      fprintf(stderr, "Adjusting dim_x to make dimensions consistent\n");
      igb_head.v_dim_x = igb_head.v_inc_x * (float)(igb_head.v_x-1);
    }

  if(igb_head.bool_dim_y && igb_head.bool_inc_y && igb_head.bool_y)
    if(!almost_equal(igb_head.v_dim_y,igb_head.v_inc_y * (float)(igb_head.v_y -1)))
    {
      fprintf(stderr, "Adjusting dim_y to make dimensions consistent\n");
      igb_head.v_dim_y = igb_head.v_inc_y * (float)(igb_head.v_y-1);
    }

  if(igb_head.bool_dim_z && igb_head.bool_inc_z && igb_head.bool_z)
    if(!almost_equal(igb_head.v_dim_z,igb_head.v_inc_z * (float)(igb_head.v_z -1)))
    {
      fprintf(stderr, "Adjusting dim_z to make dimensions consistent\n");
      igb_head.v_dim_z = igb_head.v_inc_z * (float)(igb_head.v_z-1);
    }

  if(igb_head.bool_dim_t && igb_head.bool_inc_t && igb_head.bool_t)
    if(!almost_equal(igb_head.v_dim_t, igb_head.v_inc_t * (float)(igb_head.v_t -1)))
    {
      fprintf(stderr, "Adjusting dim_t to make dimensions consistent\n");
      igb_head.v_dim_t = igb_head.v_inc_t * (float)(igb_head.v_t-1);
    }
}

inline long igb_system_endian_code()
{
  float val=IGB_ENDIAN_VAL;
  // quick fix.
  char  le_val[] = { (char)0,(char)48,(char)242,(char)199 },*pval   = (char *)(&val);
  assert( sizeof(float) == 4 );
  if ( *pval == le_val[0] )
    return IGB_LITTLE_ENDIAN;
  else
    return IGB_BIG_ENDIAN;
}

inline void igb_treat_file_open_error(std::string file, int errnum){
    fprintf(stderr, "An IO error occured when opening file %s: %s\n\n",
            file.c_str(), strerror(errnum));
    exit(1);
}

inline long init_igb_header(const std::string filename, igb_header & igb_head)
{
  long ret = 0;
  //Check file extension

  if(!(filename.substr(filename.find_last_of(".") + 1) == "dynpt")) //no dynpt extension
  {
    //maybe igb extension
    if(!(filename.substr(filename.find_last_of(".") + 1) == "igb"))
    {
      ret = -1;
      fprintf(stderr, "Error in %s: Cannot read %s\n", __func__, filename.c_str());
      return ret;
    }
  }

  igb_head.filename = filename;
  igb_head.fileptr  = NULL;

  igb_head.v_x = igb_head.v_y = igb_head.v_type = 0 ;
  igb_head.v_systeme = igb_system_endian_code();
  igb_head.v_comp = igb_head.v_lut = igb_head.v_num = igb_head.v_bin = 0 ;
  igb_head.v_trame = CC8 ;
  igb_head.v_z = igb_head.v_t = 1 ;
  igb_head.v_epais = 0.0 ;
  igb_head.v_inc_x = igb_head.v_inc_y = igb_head.v_inc_z = igb_head.v_inc_t = 1.0 ;
  igb_head.v_org_x = igb_head.v_org_y = igb_head.v_org_z = 1;
  igb_head.v_org_t = 0.0 ;
  igb_head.v_unites_x[0] = igb_head.v_unites_x[40] = '\000' ;
  igb_head.v_unites_y[0] = igb_head.v_unites_y[40] = '\000' ;
  igb_head.v_unites_z[0] = igb_head.v_unites_z[40] = '\000' ;
  igb_head.v_unites_t[0] = igb_head.v_unites_t[40] = '\000' ;
  igb_head.v_unites[0] = igb_head.v_unites[40] = '\000' ;
  igb_head.v_facteur = 1.0 ;
  igb_head.v_zero = 0.0 ;
  igb_head.v_aut_name[0] = igb_head.v_aut_name[40] = '\000' ;
  igb_head.v_struct_desc[0] = igb_head.v_struct_desc[40] = '\000' ;
  igb_head.v_transparent = NULL;

  igb_head.bool_x = igb_head.bool_y = igb_head.bool_type = false;
  igb_head.bool_z = igb_head.bool_t = false;
  igb_head.bool_taille = false;
  igb_head.bool_num = false;
  igb_head.bool_bin = false;
  igb_head.bool_trame = false;
  igb_head.bool_lut = false;
  igb_head.bool_comp = false;
  igb_head.bool_epais = false;
  igb_head.bool_org_x = igb_head.bool_org_y = igb_head.bool_org_z = igb_head.bool_org_t = false;
  igb_head.bool_inc_x = igb_head.bool_inc_y = igb_head.bool_inc_z = igb_head.bool_inc_t = false;
  igb_head.bool_dim_x = igb_head.bool_dim_y = igb_head.bool_dim_z = igb_head.bool_dim_t = false;
  igb_head.bool_unites_x = igb_head.bool_unites_y = igb_head.bool_unites_z = igb_head.bool_unites_t = false;
  igb_head.bool_unites = false;
  igb_head.bool_facteur = igb_head.bool_zero = false;
  igb_head.bool_struct_desc = false;
  igb_head.bool_aut_name = false;
  igb_head.bool_transparent = false;
  igb_head.igb_header_initialized = true;

  return(ret);
}

inline void init_igb_header(const igb_header & hdr_template, const std::string filename, igb_header & igb_head)
{
  igb_head          = hdr_template;
  igb_head.filename = filename;
  igb_head.fileptr  = NULL;
}

inline void set_igb_header_datatype(const std::string datatype, igb_header & igb_head)
{
  long tn = IGB_MIN_TYPE;
  while( tn <= IGB_MAX_TYPE && datatype.compare(Header_type[tn]))
  {
    tn++;
    if(tn <= IGB_MAX_TYPE)
      igb_head.v_type = tn;
    else {
      fprintf(stderr, "ERROR in %s: Illegal data type \"%s\" specified for IGB header\n",__func__, datatype.c_str());
      exit(-1);
    }
    igb_head.bool_type = true;
  }
}

inline void set_igb_header_systeme(const std::string datatype, igb_header & igb_head)
{
  if(datatype.compare(Header_Systeme[0]) == 0) //BIG ENDIAN
    igb_head.v_systeme = Header_Systeme_No[0];
  else if (datatype.compare(Header_Systeme[1]) == 0) //LITTLE ENDIAN
    igb_head.v_systeme = Header_Systeme_No[1];
  else
  {
    fprintf(stderr, "ERROR in %s: Illegal system \"%s\" specified for IGB header\n", __func__, datatype.c_str());
    exit(-1);
  }
}

inline long read_igb_header(igb_header & igb_head)
{
  auto file_size = [] (FILE* fd)
  {
#ifdef __WIN32__
    off64_t oldpos = ftello64(fd);
    fseeko64(fd, 0L, SEEK_END);
    off64_t sz     = ftello64(fd);
    fseeko64(fd, oldpos, SEEK_SET);
#else
    size_t oldpos = ftell(fd);
    fseek(fd, 0L, SEEK_END);
    size_t sz = ftell(fd);
    fseek(fd, oldpos, SEEK_SET);
#endif

    return sz;
  };

  long ret = 0;
  if(!igb_head.igb_header_initialized)
  {
    fprintf(stderr, "ERROR in %s: igb header was not initialized!", __func__);
    ret = -1;
    return ret;
  }


  // open now the igb header and parse it
  char * header = new char[MY_IGB_HEADERSIZE+1];
  igb_head.fileptr = fopen(igb_head.filename.c_str(), "rb");

  if(igb_head.fileptr == NULL)
    igb_treat_file_open_error(igb_head.filename, errno);

  size_t chk = fread(header, sizeof(char), MY_IGB_HEADERSIZE, igb_head.fileptr);
  // we save the file size for later
  size_t igb_size = file_size(igb_head.fileptr);

  if(chk != MY_IGB_HEADERSIZE) {
    fprintf(stderr, "%s error: igb header size does not match! Aborting!", __func__);
    ret = -1;
    return ret;
  }

  header[MY_IGB_HEADERSIZE] = '\0';
  //parse the header file into a key value string map
  char* pch = strtok (header, " \r\n");
  std::map<std::string, std::string> token_key_list;
  while(pch != nullptr)
  {
    std::string token = std::string(pch);
    std::string key, value;
    std::string::size_type pos = token.find(':');
    if(token.npos != pos) {
      value = token.substr(pos +1);
      key = token.substr(0,pos);
    }
    token_key_list.insert(std::make_pair(key,value));
    pch = strtok(nullptr, " \r\n");
  }

  delete[] header;

  //now parse the map
  //loop over the allowed keys
  for(size_t i=0; i < sizeof(allowed_keys) / sizeof(char*); i++)
  {
    std::map<std::string, std::string>::iterator search = token_key_list.find(allowed_keys[i]);
    if(search != token_key_list.end()) //found the token i in the map
    {
      //parse the stuff
      switch(i)
      {
        case 0 : igb_head.v_x = (long)(std::stoi(search->second)); igb_head.bool_x = true; break;
        case 1 : igb_head.v_y = (long)(std::stoi(search->second)); igb_head.bool_y = true; break;
        case 2 : igb_head.v_z = (long)(std::stoi(search->second)); igb_head.bool_z = true; break;
        case 3 : igb_head.v_t = (long)(std::stoi(search->second)); igb_head.bool_t = true; break;
        case 4 : set_igb_header_datatype(search->second, igb_head); break;
        case 5 : set_igb_header_systeme(search->second, igb_head); break;
        case 6 : igb_head.v_taille = (long)(std::stoi(search->second)); igb_head.bool_taille = true; break;
        case 7 : igb_head.v_bin = (long)(std::stoi(search->second)); igb_head.bool_bin = true; break;
        case 8 :
                 {
                   if(search->second.compare("c8") == 0)
                     igb_head.v_trame = CC8;
                   else if(search->second.compare("c4") == 0)
                     igb_head.v_trame = CC4;
                   else if(search->second.compare("hex") == 0)
                     igb_head.v_trame = HEX;
                   else if(search->second.compare("hexedges") == 0)
                     igb_head.v_trame = HEXEDGES;
                   else if(search->second.compare("hexbridges") == 0)
                     igb_head.v_trame = HEXBRIDGES;
                   else if(search->second.compare("hexlines") == 0)
                     igb_head.v_trame = HEXLINES;
                   else if(search->second.compare("hex2") == 0)
                     igb_head.v_trame = HEX2;
                   igb_head.bool_trame = true;
                   break;
                 }
        case 9 : igb_head.v_num   = (long)(std::stoi(search->second)); igb_head.bool_num = true; break;
        case 10 : igb_head.v_comp = (long)(std::stoi(search->second)); igb_head.bool_comp = true; break;
        case 11 : igb_head.v_lut  = (long)(std::stoi(search->second)); igb_head.bool_lut = true; break;
        case 12 : igb_head.v_dim_x = std::stof(search->second); igb_head.bool_dim_x = true; break;
        case 13 : igb_head.v_dim_y = std::stof(search->second); igb_head.bool_dim_y = true; break;
        case 14 : igb_head.v_dim_z = std::stof(search->second); igb_head.bool_dim_z = true; break;
        case 15 : igb_head.v_dim_t = std::stof(search->second); igb_head.bool_dim_t = true; break;
        case 16 : igb_head.v_inc_x = std::stof(search->second); igb_head.bool_inc_x = true; break;
        case 17 : igb_head.v_inc_y = std::stof(search->second); igb_head.bool_inc_y = true; break;
        case 18 : igb_head.v_inc_z = std::stof(search->second); igb_head.bool_inc_z = true; break;
        case 19 : igb_head.v_inc_t = std::stof(search->second); igb_head.bool_inc_t = true; break;
        case 20 : igb_head.v_org_x = std::stof(search->second); igb_head.bool_org_x = true; break;
        case 21 : igb_head.v_org_y = std::stof(search->second); igb_head.bool_org_y = true; break;
        case 22 : igb_head.v_org_z = std::stof(search->second); igb_head.bool_org_z = true; break;
        case 23 : igb_head.v_org_t = std::stof(search->second); igb_head.bool_org_t = true; break;
        case 24 : strncpy(igb_head.v_unites_x, search->second.c_str(), 40); igb_head.bool_unites_x = true; break;
        case 25 : strncpy(igb_head.v_unites_y, search->second.c_str(), 40); igb_head.bool_unites_y = true; break;
        case 26 : strncpy(igb_head.v_unites_z, search->second.c_str(), 40); igb_head.bool_unites_z = true; break;
        case 27 : strncpy(igb_head.v_unites_t, search->second.c_str(), 40); igb_head.bool_unites_t = true; break;
        case 28 : igb_head.v_epais = std::stof(search->second); igb_head.bool_epais = true; break;
        case 29 : strncpy(igb_head.v_unites, search->second.c_str(), 40); igb_head.bool_unites = true; break;
        case 30 : igb_head.v_facteur = std::stof(search->second); igb_head.bool_facteur = true; break;
        case 31 : igb_head.v_zero = std::stof(search->second); igb_head.bool_zero = true; break;
        case 32 : strncpy(igb_head.v_struct_desc, search->second.c_str(), 40); igb_head.bool_struct_desc = true; break;
        case 33 : strncpy(igb_head.v_aut_name, search->second.c_str(), 40); igb_head.bool_aut_name = true; break;
        case 34 : strcpy(igb_head.transstr, search->second.c_str()); igb_head.bool_transparent = true; break;
        default:
                  {
                    fprintf(stderr, "ERROR in %s: Unrecognized Keyword \"%s\" found while parsing the IGB header!\n", __func__, search->first.c_str());
                    ret = -1;
                    break;
                  }
      }
    }
  }

  if(igb_head.bool_transparent)
  {
    if( strlen(igb_head.transstr) != 2 * Data_Size[igb_head.v_type])
      fprintf(stderr, "ATTENTION: ignoring invalid transparent value !\n");
    else
    {
      char s[3], *p, *v;
      s[2] = '\0';
      v = (char *)(igb_head.v_transparent = calloc( Data_Size[igb_head.v_type], 1 ));
      for (size_t i=0; i<Data_Size[igb_head.v_type]; i++ ) {
        s[0] = igb_head.transstr[i*2];
        s[1] = igb_head.transstr[i*2+1];
        v[i] = strtol( s, &p, 16 );
      }
    }
  }

  /* --- l'info x y et type est obligatoire --- */
  if ( !igb_head.bool_x || !igb_head.bool_y || !igb_head.bool_type ) {
    fprintf(stderr, "\nERROR in %s: x, y or type not defined\n", __func__) ;
    ret = -1;
    return(ret);
  }

  /* --- calcul des inc et dim --- */
  if ( igb_head.bool_dim_x ) {
    if ( igb_head.bool_inc_x ) {
      float dim_x = igb_head.v_inc_x * (float)(igb_head.v_x - 1) ;
      if ( !almost_equal(dim_x, igb_head.v_dim_x)) {
        fprintf(stderr, "\nATTENTION:\n") ;
        fprintf(stderr,
            "dimension missmatch: (x (%ld) - 1) * inc_x (%.3g) = %.3g, but dim_x (%.3g)\n",
            igb_head.v_x, igb_head.v_inc_x, dim_x, igb_head.v_dim_x) ;
        ret = -1 ;
       }
     }
    else {
      igb_head.v_inc_x = igb_head.v_dim_x / (float)(igb_head.v_x - 1) ;
      igb_head.bool_inc_x = true;
    }
  } else {
    igb_head.v_dim_x = (float)(igb_head.v_x - 1) * igb_head.v_inc_x ;
    if ( igb_head.bool_inc_x ) igb_head.bool_dim_x = true;
  }

  if ( igb_head.bool_dim_y ) {
    if ( igb_head.bool_inc_y ) {
      float dim_y = igb_head.v_inc_y * (float)(igb_head.v_y - 1);
      if (!almost_equal(dim_y, igb_head.v_dim_y)) {
        fprintf(stderr, "\nATTENTION:\n") ;
        fprintf(stderr,
            "dimension missmatch: (y (%ld) - 1) * inc_y (%.3g) = %.3g, but dim_y (%.3g)\n",
            igb_head.v_y, igb_head.v_inc_y, dim_y, igb_head.v_dim_y) ;
        ret = -1 ;
      }
    } else {
      igb_head.v_inc_y = igb_head.v_dim_y / (float)(igb_head.v_y - 1) ;
      igb_head.bool_inc_y = true;
    }
  } else {
    igb_head.v_dim_y = (float)(igb_head.v_y - 1) * igb_head.v_inc_y ;
    if ( igb_head.bool_inc_y ) igb_head.bool_dim_y = true;
  }

  if ( igb_head.bool_dim_z ) {
    if ( igb_head.bool_inc_z ) {
      float dim_z = igb_head.v_inc_z * (float)(igb_head.v_z - 1) ;
      if ( !almost_equal(dim_z, igb_head.v_dim_z)) {
        fprintf(stderr, "\nATTENTION:\n") ;
        fprintf(stderr,
            "dimension missmatch: (z (%ld) - 1) * inc_z (%.3g) = %.3g, but dim_z (%.3g)\n",
            igb_head.v_z, igb_head.v_inc_z, dim_z, igb_head.v_dim_z) ;
        ret = -1 ;
      }
    }
    else {
      igb_head.v_inc_z = igb_head.v_dim_z / (float)(igb_head.v_z - 1) ;
      igb_head.bool_inc_y = true;
    }
  }
  else {
    igb_head.v_dim_z = (float)(igb_head.v_z - 1) * igb_head.v_inc_z ;
    if ( igb_head.bool_inc_z ) igb_head.bool_dim_z = true;
  }

  // here we check if the file size matches up with the reported number of slices
  {
    int    num_comp       = Num_Components[igb_head.v_type];
    size_t byte_per_entry = Data_Size[igb_head.v_type] / num_comp;
    size_t nvals = igb_head.v_x * igb_head.v_y * igb_head.v_z * num_comp;

    size_t expected_igb_size = byte_per_entry*nvals*igb_head.v_t;
    if(igb_size < expected_igb_size) {
      int actual_v_t = igb_size / (byte_per_entry*nvals);
      fprintf(stderr, "%s warning: %s contains fewer time slices (%d) than reported in the header (%d)! Adjusting.\n",
              __func__, igb_head.filename.c_str(), actual_v_t, int(igb_head.v_t));

      igb_head.v_t     = actual_v_t;
      if(igb_head.bool_inc_t)
        igb_head.v_dim_t = igb_head.v_inc_t * (float)((igb_head.v_t-1));
    }
  }

  if (igb_head.bool_dim_t) {
    if (igb_head.bool_inc_t) {
      float dim_t = igb_head.v_inc_t * (float)((igb_head.v_t - 1)) ;
      if (!almost_equal(dim_t, igb_head.v_dim_t)) {
        fprintf(stderr, "\nATTENTION:\n") ;
        fprintf(stderr, 
            "dimension missmatch: (t (%ld) - 1) * inc_t (%.3g) = %.3g, but dim_t (%.3g)\n", 
            igb_head.v_t, igb_head.v_inc_t, dim_t, igb_head.v_dim_t) ;
        ret = -1;
      }
    }
    else {
      igb_head.v_inc_t = igb_head.v_dim_t / (float)((igb_head.v_t - 1)) ;
      igb_head.bool_inc_t = true;
    }
  }
  else {
    igb_head.v_dim_t = (float)((igb_head.v_t - 1)) * igb_head.v_inc_t ;
    if (igb_head.bool_inc_t)
      igb_head.bool_dim_t = true;
  }

  if ( igb_head.bool_taille ) {
    if (igb_head.v_type!=IGB_STRUCTURE) {
      fprintf(stderr, "\nERREUR taille redefinie pour type autre que structure\n") ;
      ret = -1;
    }
  }
  else {
    if (igb_head.v_type==IGB_STRUCTURE) {
      fprintf(stderr,
          "\nERREUR taille non definie pour type structure\n") ;
      ret = -1;
    }
    else
      igb_head.v_taille = Data_Size[igb_head.v_type];
  }
  return ret;
}

inline long write_igb_header(igb_header & igb_head)
{
  long ret = 0;
  igb_head.fileptr = fopen(igb_head.filename.c_str(), "wb");
  if(igb_head.fileptr == NULL) igb_treat_file_open_error(igb_head.filename, errno);

  if (igb_head.v_type<IGB_MIN_TYPE || igb_head.v_type>IGB_MAX_TYPE) {
    std::cerr<< "\nHeader_Write: unknown data type: "<< igb_head.v_type;
    return (-1);
  }

  //do again this fancy routine since the amount of points in the header may have changed
  /* --- l'info x y et type est obligatoire --- */
  if ( !igb_head.bool_x || !igb_head.bool_y || !igb_head.bool_type )
  {
    fprintf(stderr, "\nERROR in %s: x, y or type not defined\n", __func__) ;
    ret = -1;
    return(ret);
  }
  /* --- calcul des inc et dim --- */
  if ( igb_head.bool_dim_x ) {
    if ( igb_head.bool_inc_x )
    {
      float dim_x = igb_head.v_inc_x * (float)(igb_head.v_x) ;
      if ( !almost_equal(dim_x, igb_head.v_dim_x) )
      {
        fprintf(stderr, "\nATTENTION:\n") ;
        fprintf(stderr,
            "conflit entre x (%ld) * inc_x (%.3g) = %.3g et dim_x (%.3g)\n",
            igb_head.v_x, igb_head.v_inc_x, dim_x, igb_head.v_dim_x) ;
        ret = -1 ;
      }
    }
    else {
      igb_head.v_inc_x = igb_head.v_dim_x / (float)(igb_head.v_x) ;
      igb_head.bool_inc_x = true;
    }
  }
  else {
    igb_head.v_dim_x = (float)(igb_head.v_x) * igb_head.v_inc_x ;
    if ( igb_head.bool_inc_x ) igb_head.bool_dim_x = true;
  }

  if ( igb_head.bool_dim_y ) {
    if ( igb_head.bool_inc_y ) {
      float dim_y = igb_head.v_inc_y * (float)(igb_head.v_y) ;
      if ( !almost_equal(dim_y, igb_head.v_dim_y) )
      {
        fprintf(stderr, "\nATTENTION:\n") ;
        fprintf(stderr,
            "conflit entre y (%ld) * inc_y (%.3g) = %.3g et dim_y (%.3g)\n",
            igb_head.v_y, igb_head.v_inc_y, dim_y, igb_head.v_dim_y) ;
        ret = -1 ;
      }
    }
    else {
      igb_head.v_inc_y = igb_head.v_dim_y / (float)(igb_head.v_y) ;
      igb_head.bool_inc_y = true;
    }
  }
  else {
    igb_head.v_dim_y = (float)(igb_head.v_y) * igb_head.v_inc_y ;
    if ( igb_head.bool_inc_y ) igb_head.bool_dim_y = true;
  }

  if ( igb_head.bool_dim_z ) {
    if ( igb_head.bool_inc_z ) {
      float dim_z = igb_head.v_inc_z * (float)(igb_head.v_z) ;
      if ( !almost_equal(dim_z, igb_head.v_dim_z) )
      {
        fprintf(stderr, "\nATTENTION:\n") ;
        fprintf(stderr,
            "conflit entre z (%ld) * inc_z (%.3g) = %.3g et dim_z (%.3g)\n",
            igb_head.v_z, igb_head.v_inc_z, dim_z, igb_head.v_dim_z) ;
        ret = -1 ;
      }
    }
    else {
      igb_head.v_inc_z = igb_head.v_dim_z / (float)(igb_head.v_z) ;
      igb_head.bool_inc_y = true;
    }
  }
  else {
    igb_head.v_dim_z = (float)(igb_head.v_z) * igb_head.v_inc_z ;
    if ( igb_head.bool_inc_z ) igb_head.bool_dim_z = true;
  }


  if ( igb_head.bool_dim_t ) {
    if ( igb_head.bool_inc_t ) {
      float dim_t = igb_head.v_inc_t * (float)((igb_head.v_t-1)) ;
      if ( !almost_equal(dim_t, igb_head.v_dim_t) )
      {
        fprintf(stderr, "\nATTENTION:\n") ;
        fprintf(stderr,
            "conflit entre t (%ld) * inc_t (%.3g) = %.3g et dim_t (%.3g)\n",
            igb_head.v_t, igb_head.v_inc_t, dim_t, igb_head.v_dim_t) ;
        ret = -1;
      }
    }
    else {
      igb_head.v_inc_t = igb_head.v_dim_t / (float)((igb_head.v_t - 1)) ;
      igb_head.bool_inc_t = true;
    }
  }
  else {
    igb_head.v_dim_t = (float)((igb_head.v_t-1)) * igb_head.v_inc_t ;
    if ( igb_head.bool_inc_t )
      igb_head.bool_dim_t = true;
  }

  std::string type = Header_type[igb_head.v_type];

  if (igb_head.v_type==IGB_STRUCTURE && igb_head.v_taille<1) {
    std::cerr << "\nHeader_Write: taille invalide:" << igb_head.v_taille << std::endl;
    return (-1);
  }

  if (igb_head.v_trame<MIN_TRAME || igb_head.v_trame>MAX_TRAME) {
    fprintf(stderr, "\nHeader_Write: trame inconnue: %ld\n", igb_head.v_trame);
    return (-1);
  }

  make_igb_consistent(igb_head);

  // we will now only allow writing of big or little endian */
  const char* systeme=(igb_system_endian_code()==IGB_BIG_ENDIAN)?"big_endian":"little_endian";
  char ligne[1024];
  if (igb_head.bool_t) {
    if (igb_head.bool_z) {
      sprintf(ligne, "x:%ld y:%ld z:%ld t:%ld type:%s systeme:%s ",
          igb_head.v_x, igb_head.v_y, igb_head.v_z, igb_head.v_t, type.c_str(), systeme);
    } else {
      sprintf(ligne, "x:%ld y:%ld t:%ld type:%s systeme:%s ",
          igb_head.v_x, igb_head.v_y, igb_head.v_t, type.c_str(), systeme);
    }
  } else {
    if (igb_head.bool_z) {
      sprintf(ligne, "x:%ld y:%ld z:%ld type:%s systeme:%s ",
          igb_head.v_x, igb_head.v_y, igb_head.v_z, type.c_str(), systeme);
    } else {
      sprintf(ligne, "x:%ld y:%ld type:%s systeme:%s ",
          igb_head.v_x, igb_head.v_y, type.c_str(), systeme);
    }
  }

  int n_car = strlen(ligne);

  int n_lignes = 1;
  int n_items = 0;
  int l_item[N_MAX_ITEMS+1];
  char items[N_MAX_ITEMS+1][L_MAX_ITEM];
  /*
     Le mot-clef "taille" n'est ecrit que pour le type STRUCTURE mais il est
     obligatoire pour ce cas.
     */
  if (igb_head.v_type==IGB_STRUCTURE) {
    sprintf(&items[n_items][0], "taille:%ld ", igb_head.v_taille);
    l_item[n_items] = strlen(&items[n_items][0]);
    n_items++;
  }
  if (igb_head.bool_org_x) {
    sprintf(&items[n_items][0], "org_x:%f ", igb_head.v_org_x);
    l_item[n_items] = strlen(&items[n_items][0]);
    n_items++;
  }
  if (igb_head.bool_org_y) {
    sprintf(&items[n_items][0], "org_y:%f ", igb_head.v_org_y);
    l_item[n_items] = strlen(&items[n_items][0]);
    n_items++;
  }
  if (igb_head.bool_org_z) {
    sprintf(&items[n_items][0], "org_z:%f ", igb_head.v_org_z);
    l_item[n_items] = strlen(&items[n_items][0]);
    n_items++;
  }
  if (igb_head.bool_org_t) {
    sprintf(&items[n_items][0], "org_t:%f ", igb_head.v_org_t);
    l_item[n_items] = strlen(&items[n_items][0]);
    n_items++;
  }
  if (igb_head.bool_dim_x) {
    sprintf(&items[n_items][0], "dim_x:%f ", igb_head.v_dim_x);
    l_item[n_items] = strlen(&items[n_items][0]);
    n_items++;
  }
  if (igb_head.bool_inc_x) {
    sprintf(&items[n_items][0], "dim_x:%f ", igb_head.v_inc_x);
    l_item[n_items] = strlen(&items[n_items][0]);
    n_items++;
  }
  if (igb_head.bool_dim_y) {
    sprintf(&items[n_items][0], "dim_y:%f ", igb_head.v_dim_y);
    l_item[n_items] = strlen(&items[n_items][0]);
    n_items++;
  }
  if (igb_head.bool_inc_y) {
    sprintf(&items[n_items][0], "dim_y:%f ", igb_head.v_inc_y);
    l_item[n_items] = strlen(&items[n_items][0]);
    n_items++;
  }
  if (igb_head.bool_dim_z) {
    sprintf(&items[n_items][0], "dim_z:%f ", igb_head.v_dim_z);
    l_item[n_items] = strlen(&items[n_items][0]);
    n_items++;
  }
  if (igb_head.bool_inc_z) {
    sprintf(&items[n_items][0], "dim_z:%f ", igb_head.v_inc_z);
    l_item[n_items] = strlen(&items[n_items][0]);
    n_items++;
  }
  if (igb_head.bool_dim_t) {
    sprintf(&items[n_items][0], "dim_t:%f ", igb_head.v_dim_t);
    l_item[n_items] = strlen(&items[n_items][0]);
    n_items++;
  }
  if (igb_head.bool_inc_t) {
    sprintf(&items[n_items][0], "inc_t:%f ", igb_head.v_inc_t);
    l_item[n_items] = strlen(&items[n_items][0]);
    n_items++;
  }
  if (igb_head.bool_unites_x) {
    sprintf(&items[n_items][0], "unites_x:%.40s ", igb_head.v_unites_x);
    l_item[n_items] = strlen(&items[n_items][0]);
    n_items++;
  }
  if (igb_head.bool_unites_y) {
    sprintf(&items[n_items][0], "unites_y:%.40s ", igb_head.v_unites_y);
    l_item[n_items] = strlen(&items[n_items][0]);
    n_items++;
  }
  if (igb_head.bool_unites_z) {
    sprintf(&items[n_items][0], "unites_z:%.40s ", igb_head.v_unites_z);
    l_item[n_items] = strlen(&items[n_items][0]);
    n_items++;
  }
  if (igb_head.bool_unites_t) {
    sprintf(&items[n_items][0], "unites_t:%.40s ", igb_head.v_unites_t);
    l_item[n_items] = strlen(&items[n_items][0]);
    n_items++;
  }
  if (igb_head.bool_num) {
    sprintf(&items[n_items][0], "num:%ld ", igb_head.v_num);
    l_item[n_items] = strlen(&items[n_items][0]);
    n_items++;
  }
  if (igb_head.bool_bin) {
    sprintf(&items[n_items][0], "bin:%ld ", igb_head.v_bin);
    l_item[n_items] = strlen(&items[n_items][0]);
    n_items++;
  }
  if (igb_head.bool_trame) {
    switch (igb_head.v_trame) {
      case CC8:
        sprintf(&items[n_items][0], "trame:c8 ");
        break;
      case CC4:
        sprintf(&items[n_items][0], "trame:c4 ");
        break;
      case HEX:
        sprintf(&items[n_items][0], "trame:hex ");
        break;
      case HEXEDGES:
        sprintf(&items[n_items][0], "trame:hexedges ");
        break;
      case HEXBRIDGES:
        sprintf(&items[n_items][0], "trame:hexbridges ");
        break;
      case HEXLINES:
        sprintf(&items[n_items][0], "trame:hexlines ");
        break;
      case HEX2:
        sprintf(&items[n_items][0], "trame:hex2 ");
        break;
    }
    l_item[n_items] = strlen(&items[n_items][0]);
    n_items++;
  }
  if (igb_head.bool_lut) {
    sprintf(&items[n_items][0], "lut:%ld ", igb_head.v_lut);
    l_item[n_items] = strlen(&items[n_items][0]);
    n_items++;
  }
  if (igb_head.bool_comp) {
    sprintf(&items[n_items][0], "comp:%ld ", igb_head.v_comp);
    l_item[n_items] = strlen(&items[n_items][0]);
    n_items++;
  }
  if (igb_head.bool_epais) {
    sprintf(&items[n_items][0], "epais:%f ", igb_head.v_epais);
    l_item[n_items] = strlen(&items[n_items][0]);
    n_items++;
  }
  if (igb_head.bool_unites) {
    sprintf(&items[n_items][0], "unites:%.40s ", igb_head.v_unites);
    l_item[n_items] = strlen(&items[n_items][0]);
    n_items++;
  }
  if (igb_head.bool_facteur) {
    sprintf(&items[n_items][0], "facteur:%f ", igb_head.v_facteur);
    l_item[n_items] = strlen(&items[n_items][0]);
    n_items++;
  }
  if (igb_head.bool_zero) {
    sprintf(&items[n_items][0], "zero:%f ", igb_head.v_zero);
    l_item[n_items] = strlen(&items[n_items][0]);
    n_items++;
  }
  if ( igb_head.v_transparent != NULL ) {
    char *p=(char *)igb_head.v_transparent, value[MAXL];
    for (size_t a=0; a<Data_Size[igb_head.v_type]; a++ )
      sprintf( value+a*2, "%x", *(p++) );
    value[2*Data_Size[igb_head.v_type]] = '\0';
    sprintf(&items[n_items][0], "transparent:%s ", value );
    l_item[n_items] = strlen(&items[n_items][0]);
    n_items++;
  }
  if (igb_head.bool_struct_desc) {
    sprintf(&items[n_items][0], "struct:%.40s ", igb_head.v_struct_desc);
    l_item[n_items] = strlen(&items[n_items][0]);
    n_items++;
  }
  if (igb_head.bool_aut_name) {
    sprintf(&items[n_items][0], "aut:%.40s ", igb_head.v_aut_name);
    l_item[n_items] = strlen(&items[n_items][0]);
    n_items++;
  }

  int n_car_total = 0;
  /* Ecrit tous les items, sauf les commentaires */
  for (int i=0;i<n_items;i++) {
    if (n_car+l_item[i]<71) { /*  Ajoute a la ligne courante s'il reste de la place */
      strcat(ligne, &items[i][0]);
      n_car += l_item[i];
    } else {    /*  Sinon, ecrit cette ligne et commence-en une autre */
      ligne[n_car++] = '\r';
      ligne[n_car++] = '\n';
      ligne[n_car]   = '\000';
      n_car_total += n_car;
      if (fputs(ligne, igb_head.fileptr)==-1)
      {
        fprintf(stderr, "\nHeader_Write: Erreur a l'ecriture \n");
        perror("\n *** ");
        fprintf(stderr,  "\n");
        return (-1);
      }
      strcpy(ligne, &items[i][0]);
      n_car = l_item[i];
      n_lignes++;
    }
  }
  /*
     Termine la derniere ligne
     */
  ligne[n_car++] = '\r';
  ligne[n_car++] = '\n';
  ligne[n_car]   = '\000';
  n_car_total += n_car;
  if (fputs(ligne, igb_head.fileptr)==-1)
  {
    fprintf(stderr, "\nHeader_Write: Erreur a l'ecriture \n");
    perror("\n *** ");
    fprintf(stderr,  "\n");
    return (-1);
  }
  n_lignes++;

  /*
     Determine le nombre de caracteres et de lignes supplementaires
     necessaires
     */
  int n_blocs   = 1 + (n_car_total-1)/1024;
  int n_car_sup = n_blocs*1024 - n_car_total;
  int n_lig_sup;
  if (n_car_sup>0) {
    n_lig_sup = 1 + (n_car_sup-1)/72;
  } else {
    n_lig_sup = 0;
  }
  int n_car_dl  = 1 + (n_car_sup-1)%72;

  /*
     Complete l'entete a un multiple de 1024 caracteres
     */
  for (int i=0;i<70;i++) ligne[i] = ' ';
  ligne[70] = '\r';
  ligne[71] = '\n';
  ligne[72] = '\000';
  for (int i=0;i<n_lig_sup-1;i++) {
    if (fputs(ligne, igb_head.fileptr)==-1)
    {
      fprintf(stderr, "\nHeader_Write: Erreur a l'ecriture \n");
      perror("\n *** ");
      fprintf(stderr,  "\n");
      return(-1);
    }
  }

  /*
     La derniere ligne se termine par un saut de page (FF)
     */
  for (int i=0;i<n_car_dl-2;i++) ligne[i] = ' ';
  if (n_car_dl>2) ligne[n_car_dl-3] = '\r';
  if (n_car_dl>1) ligne[n_car_dl-2] = '\n';
  ligne[n_car_dl-1] = FF;
  ligne[n_car_dl] = '\000';
  if (fputs( ligne , igb_head.fileptr)==-1)
  {
    fprintf(stderr, "\nHeader_Write: Erreur a l'ecriture \n");
    perror("\n *** ");
    fprintf(stderr,  "\n");
    return (-1);
  }

  if (n_car_total>1024)
  {
    fprintf(stderr,
          "\nHeader_Write ATTENTION: etiquette de grandeur non-standard \n");
  }
  return ret;
}

inline bool igb_system_is_big_endian()
{
  int i = 1;
  char *p = (char *)&i;

  if (p[0] == 1) return false;
  else           return true;
}

/**
* @brief Set the dimensions (number of vals per axis) of the igb data
*
* @param x    x dimension.
* @param y    y dimension.
* @param z    z dimension.
* @param t    time dimension.
* @param igb  The igb header.
*/
inline void set_igb_header_dim(int x, int y, int z, int t, igb_header & igb)
{
  igb.v_x = x; igb.bool_x = true;
  igb.v_y = y; igb.bool_y = true;
  igb.v_z = z; igb.bool_z = true;
  igb.v_t = t; igb.bool_t = true;
}

/**
* @brief Set the units of the igb data.
*
* @param space_unit  Spatial unit.
* @param time_unit   Temporal unit.
* @param val_unit    Function value unit.
* @param igb         The igb header.
*/
inline void set_igb_header_units(const std::string space_unit,
                   const std::string time_unit,
                   const std::string val_unit,
                   igb_header & igb)
{
  sprintf(igb.v_unites, "%.40s", val_unit.c_str()); igb.bool_unites = true;

  if(igb.bool_x)
    sprintf(igb.v_unites_x, "%.40s", space_unit.c_str()), igb.bool_unites_x = true;
  if(igb.bool_y)
    sprintf(igb.v_unites_y, "%.40s", space_unit.c_str()), igb.bool_unites_y = true;
  if(igb.bool_z)
    sprintf(igb.v_unites_z, "%.40s", space_unit.c_str()), igb.bool_unites_z = true;
  if(igb.bool_t)
    sprintf(igb.v_unites_t, "%.40s", time_unit.c_str()), igb.bool_unites_t = true;
}

/**
* @brief Byte swapping func.
*
* @tparam T Data type.
* @param in Input val.
*
* @return Output val with swapped bytes.
*/
template<typename T> inline
T igb_byte_swap(T in)
{
  T out; // output buffer

  char*  inp  = ( char* ) (& in );
  char*  outp = ( char* ) (& out);
  size_t size = sizeof(T);

  switch(size) {
    case 4:
      outp[0] = inp[3];
      outp[1] = inp[2];
      outp[2] = inp[1];
      outp[3] = inp[0];
      break;

    case 2:
      outp[0] = inp[1];
      outp[1] = inp[0];
      break;

    case 8:
      outp[0] = inp[7], outp[1] = inp[6];
      outp[2] = inp[5], outp[3] = inp[4];
      outp[4] = inp[3], outp[5] = inp[2];
      outp[6] = inp[1], outp[7] = inp[0];
      break;

    default:
      for(size_t i=0; i<size; i++)
        outp[i] = inp[size-1-i];
  }
  return out;
}
template<class VEC>
void parse_igb_line(const long& v_type, VEC & data_vec, const char* rbuff, size_t count, const size_t& nvals, bool endian_match) {
  switch(v_type) {
    case IGB_BYTE:
    case IGB_CHAR:
      data_vec.assign(rbuff, rbuff+nvals);
      break;
    case IGB_SHORT:
      data_vec.assign((short*)rbuff, ((short*)rbuff)+nvals);
      break;
    case IGB_INT:
      data_vec.assign((int*)rbuff, ((int*)rbuff)+nvals);
      break;
    case IGB_LONG:
      data_vec.assign((long*)rbuff, ((long*)rbuff)+nvals);
      break;
    case IGB_FLOAT:
    case IGB_VEC3_f:
    case IGB_VEC4_f:
    case IGB_VEC9_f:
      data_vec.assign((float*)rbuff, ((float*)rbuff)+nvals);
      break;
    case IGB_DOUBLE:
    case IGB_VEC3_d:
    case IGB_VEC4_d:
      data_vec.assign((double*)rbuff, ((double*)rbuff)+nvals);
      break;
    default:
      fprintf(stderr, "Error: Data type reading not implemented! Aborting!\n");
      exit(EXIT_FAILURE);
  }

  if(!endian_match)
    for(size_t j=0; j<nvals; j++)
      data_vec[j] = igb_byte_swap(data_vec[j]);

  delete [] rbuff;

  if (count != nvals) {
    fprintf(stderr, "Warning in %s: Number of values read does not match given blocksize \n",__func__);
    fprintf(stderr, "               read:     %ld\n", long(count));
    fprintf(stderr, "               expected: %ld\n", long(nvals));

    data_vec.assign(nvals, 0.0);
  }
}
/**
* @brief Read a block of igb time-slices.
*
* Note that the function assumes that init_igb_header and read_igb_header have already
* been called!
*
* @tparam V Data type of the output vector
* @param data_vec    The vector we read into.
* @param igb         The igb header, already set up.
*
* @return number of read values.
*/
template<class VEC>
size_t read_igb_slice(VEC & data_vec, igb_header & igb)
{
  if( !(igb.igb_header_initialized && igb.fileptr) )
  {
    fprintf(stderr, "ERROR in %s: IGB header was not initialized!\n", __func__);
    exit(EXIT_FAILURE);
  }

  bool machine_big_endian = igb_system_is_big_endian();
  bool file_big_endian    = igb.v_systeme == IGB_BIG_ENDIAN;
  bool endian_match       = machine_big_endian == file_big_endian;

  int    num_comp       = Num_Components[igb.v_type];
  size_t byte_per_entry = Data_Size[igb.v_type] / num_comp;

  //Load data
  size_t nvals = igb.v_x * igb.v_y * igb.v_z * num_comp;
  char*  rbuff = new char[nvals*byte_per_entry];
  size_t count = fread(rbuff, byte_per_entry, nvals , igb.fileptr);

  parse_igb_line(igb.v_type, data_vec, rbuff, count, nvals, endian_match);
  return count;
}

template<class VEC>
size_t read_igb_trace(VEC & data_vec, igb_header & igb, size_t idx)
{
  if( !(igb.igb_header_initialized && igb.fileptr) )
  {
    fprintf(stderr, "ERROR in %s: IGB header was not initialized!\n", __func__);
    exit(EXIT_FAILURE);
  }

  bool machine_big_endian = igb_system_is_big_endian();
  bool file_big_endian    = igb.v_systeme == IGB_BIG_ENDIAN;
  bool endian_match       = machine_big_endian == file_big_endian;

  int    num_comp       = Num_Components[igb.v_type];
  size_t byte_per_entry = Data_Size[igb.v_type] / num_comp;

  //Load data
  size_t nvals = igb.v_x * igb.v_y * igb.v_z * num_comp;

  data_vec.resize(0);
  data_vec.reserve(nvals);
  char*  rbuff = new char[byte_per_entry];
  int count = 0;

  for(int slice = 0; slice < igb.v_t; slice++)
  {
#ifndef __WIN32__
    size_t offset = MY_IGB_HEADERSIZE + (byte_per_entry*nvals*slice) + (byte_per_entry*idx);
    fseek(igb.fileptr, offset, SEEK_SET);
#else
    off64_t offset = MY_IGB_HEADERSIZE + (byte_per_entry*nvals*slice) + (byte_per_entry*idx);
    fseeko64(igb.fileptr, offset, SEEK_SET);
#endif

    count += fread(rbuff, byte_per_entry, 1, igb.fileptr);
    switch(igb.v_type) {
      case IGB_BYTE:
      case IGB_CHAR:
        data_vec.push_back(*rbuff);
        break;
      case IGB_SHORT:
        data_vec.push_back(((short*)rbuff)[0]);
        break;
      case IGB_INT:
        data_vec.push_back(((int*)rbuff)[0]);
        break;
      case IGB_LONG:
        data_vec.push_back(((long*)rbuff)[0]);
        break;
      case IGB_FLOAT:
      case IGB_VEC3_f:
      case IGB_VEC4_f:
      case IGB_VEC9_f:
        data_vec.push_back(((float*)rbuff)[0]);
        break;
      case IGB_DOUBLE:
      case IGB_VEC3_d:
      case IGB_VEC4_d:
        data_vec.push_back(((double*)rbuff)[0]);
        break;
      default:
        fprintf(stderr, "Error: Data type reading not implemented! Aborting!\n");
        exit(EXIT_FAILURE);
    }
  }

  if(!endian_match)
    for(size_t j=0; j<data_vec.size(); j++)
      data_vec[j] = igb_byte_swap(data_vec[j]);

  delete [] rbuff;

  if (count != igb.v_t) {
    fprintf(stderr, "Warning in %s: Number of values read does not match the number of slices \n",__func__);
    fprintf(stderr, "               read:     %ld\n", long(count));
    fprintf(stderr, "               expected: %ld\n", long(igb.v_t));
  }

  return count;
}

/// go to a specific igb slice index
inline
void set_igb_slice(igb_header & igb, size_t slice_idx)
{
  if( !(igb.igb_header_initialized && igb.fileptr) )
  {
    fprintf(stderr, "ERROR in %s: IGB header was not initialized!\n", __func__);
    exit(EXIT_FAILURE);
  }

  size_t num_comp       = Num_Components[igb.v_type];
  size_t byte_per_entry = Data_Size[igb.v_type] / num_comp;
  size_t nvals = igb.v_x * igb.v_y * igb.v_z * num_comp;
#ifndef __WIN32__
  size_t offset = MY_IGB_HEADERSIZE + (byte_per_entry*nvals*slice_idx);
  fseek(igb.fileptr, offset, SEEK_SET);
#else
  off64_t offset = MY_IGB_HEADERSIZE + (byte_per_entry*nvals*slice_idx);
  fseeko64(igb.fileptr, offset, SEEK_SET);
#endif
}

/**
 * @brief read a igb slice corresponing to one time instance. The slice index is provided. 
 * 
 * @tparam V Data type.
 * @param data_vec       Vector storing the data.
 * @param igb            The igb header.
 * @param slice_idx      The slice index to read.
 * @return size_t        How many values have been read.
 */
template<class VEC> inline
size_t read_igb_slice(VEC & data_vec, igb_header & igb, size_t slice_idx)
{
  if( !(igb.igb_header_initialized && igb.fileptr) )
  {
    fprintf(stderr, "ERROR in %s: IGB header was not initialized!\n", __func__);
    exit(EXIT_FAILURE);
  }

  set_igb_slice(igb, slice_idx);
  return read_igb_slice(data_vec, igb);
}


/**
* @brief Read a block of igb time-slices.
*
* Note that the function assumes that init_igb_header and read_igb_header have already
* been called!
*
* @tparam VVEC       Vector of vectors of data type of the output
* @param data_vec    The vector we read into.
* @param blocksize   The number of time-slices we want to read.
* @param igb         The igb header, already set up.
*
* @return number of read values.
*/
template<class VVEC> inline
size_t read_igb_block(VVEC & data_vec, const int blocksize, igb_header & igb)
{
  size_t count = 0;

  //allocate vector
  data_vec.resize(blocksize);

  // Load data
  for(int i=0; i < blocksize; i++)
    count += read_igb_slice(data_vec[i], igb);

  return count;
}

/**
* @brief Read a full igb dataset (all time-slices).
**
* @tparam V Data type of the vector we read into.
* @param data_vec The vector we read into.
* @param igb      The igb header struct we read into.
* @param filename The filename of the igb data.
*
* @return 0 on success, -1 else.
*/
template<class VVEC> inline
int read_igb_data(VVEC & data_vec, igb_header & igb, const std::string & filename)
{
  init_igb_header(filename, igb);
  read_igb_header(igb);

  int ret = read_igb_block(data_vec, igb.v_t, igb);
  close_igb_fileptr(igb);

  return(ret);
}

/**
* @brief Write a block of igb time-slices.
*
* Note that the function assumes that init_igb_header and write_igb_header have already
* been called!
*
* @tparam VEC      vector of data type.
* @param data_vec  A vector of time slices.
* @param igb_head  The igb header
*
* @return 0 on success, -1 else.
*/
template<class VEC> inline
int write_igb_slice(const VEC & data_vec, igb_header & igb_head)
{
  if( !(igb_head.igb_header_initialized && igb_head.fileptr) )
  {
    fprintf(stderr, "ERROR in %s: IGB header was not initialized!\n", __func__);
    exit(EXIT_FAILURE);
  }

  size_t num_comp       = (size_t) Num_Components[igb_head.v_type];
  size_t byte_per_entry = (size_t) Data_Size[igb_head.v_type] / num_comp;
  size_t nvals          = (size_t) (igb_head.v_x * igb_head.v_y * igb_head.v_z * num_comp);
  size_t checksum       = (size_t) (data_vec.size());
  size_t count = 0;
  size_t vals_written = 0;

  assert(data_vec.size() == size_t(nvals));
  vals_written = 0;
  switch(igb_head.v_type) {
    case IGB_BYTE:
    case IGB_CHAR:
    {
      mt_vector<char> wbuff;
      wbuff.assign(data_vec.begin(), data_vec.end());
      const char* wp = (const char*) wbuff.data();
      vals_written =  fwrite(wp, byte_per_entry, nvals, igb_head.fileptr);
      count += vals_written;
      break;
    }
    case IGB_SHORT:
    {
      mt_vector<short> wbuff;
      wbuff.assign(data_vec.begin(), data_vec.end());
      const char* wp = (const char*) wbuff.data();
      vals_written =  fwrite(wp, byte_per_entry, nvals, igb_head.fileptr);
      count += vals_written;
      break;
    }
    case IGB_INT:
    {
      mt_vector<int> wbuff;
      wbuff.assign(data_vec.begin(), data_vec.end());
      const char* wp = (const char*) wbuff.data();
      vals_written =  fwrite(wp, byte_per_entry, nvals, igb_head.fileptr);
      count += vals_written;
      break;
    }
    case IGB_LONG:
    {
      mt_vector<long int> wbuff;
      wbuff.assign(data_vec.begin(), data_vec.end());
      const char* wp = (const char*) wbuff.data();
      vals_written =  fwrite(wp, byte_per_entry, nvals, igb_head.fileptr);
      count += vals_written;
      break;
    }
    case IGB_FLOAT:
    case IGB_VEC3_f:
    case IGB_VEC4_f:
    case IGB_VEC9_f:
    {
      mt_vector<float> wbuff;
      wbuff.assign(data_vec.begin(), data_vec.end());
      const char* wp = (const char*) wbuff.data();
      vals_written =  fwrite(wp, byte_per_entry, nvals, igb_head.fileptr);
      count += vals_written;
      break;
    }
    case IGB_DOUBLE:
    case IGB_VEC3_d:
    case IGB_VEC4_d:
    {
      mt_vector<double> wbuff;
      wbuff.assign(data_vec.begin(), data_vec.end());
      const char* wp = (const char*) wbuff.data();
      vals_written =  fwrite(wp, byte_per_entry, nvals, igb_head.fileptr);
      count += vals_written;
      break;
    }
    default:
      fprintf(stderr, "Error: Data type reading not implemented! Aborting!\n");
      exit(EXIT_FAILURE);
  }

  if(count != (size_t) (igb_head.v_x *  num_comp))
  {
    fprintf(stderr, "Warning in %s: Number of values written does not match blocksize\n",__func__);
    fprintf(stderr, "               written:  %zd\n", count);
    fprintf(stderr, "               expected: %zd\n", checksum);
  }

  return count;
}

/**
* @brief Write a block of igb time-slices.
*
* Note that the function assumes that init_igb_header and write_igb_header have already
* been called!
*
* @tparam VVEC     vector of vectors of data type.
* @param data_vec  A vector of time slices.
* @param igb_head  The igb header
*
* @return number of written values.
*/
template<class VVEC> inline
int write_igb_block(const VVEC & data_vec, igb_header & igb_head)
{
  size_t count = 0;

  for(size_t i=0; i < data_vec.size(); i++)
    count += write_igb_slice(data_vec[i], igb_head);

  return count;
}


/**
* @brief Write an igb dataset, all time-slices at once.
*
* Note that the function assumes that init_igb_header and write_igb_header have already
* been called!
*
* @tparam VVEC    vector of vectors of data type.
* @param data_vec A vector of time-slice data.
* @param igb      The igb header, already initialized.
*
* @return 0 on success, -1 else.
*/
template<class VVEC> inline
int write_igb_data(const VVEC & data_vec, igb_header & igb)
{
  assert(data_vec.size() == (size_t)igb.v_t);
  int ret = write_igb_block(data_vec, igb);
  close_igb_fileptr(igb);
  return ret;
}

#endif

