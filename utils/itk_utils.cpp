#include "itk_utils.h"

#ifdef MT_GZIP
#include <fcntl.h>
#include <zlib.h>
#endif

bool itk_image::read_file_vtk(const char* filename)
{
  FILE * fp;
  fp = fopen(filename, MT_FOPEN_READ);
  if (fp) {
    int datatype = 0; // 0 ... not set, 1 ... ascii, 2 ... binary

    char line[256], tmpstr[32];
    int readflag = 0;
    while (fgets(line, 256*sizeof(char), fp) && (!readflag))
    {
      if (strncmp(line, "ASCII", 5) == 0)
        datatype = 1;
      else if (strncmp(line, "BINARY", 6) == 0)
        datatype = 2;
      else if (strncmp(line, "DATASET", 7) == 0)
      {
        sscanf(line, "%*s %s", tmpstr);
        if (strncmp(tmpstr, "STRUCTURED_POINTS", 17) != 0) readflag = -1;
      }
      else if (strncmp(line, "DIMENSIONS", 10) == 0)
      {
        sscanf(line, "%*s %zu %zu %zu", &this->dim.v1, &this->dim.v2, &this->dim.v3);
        printf("[ITKIMAGE]: dimensions %zu x %zu x %zu ( %zu )\n", this->dim.v1, this->dim.v2, this->dim.v3, this->dim.v1*this->dim.v2*this->dim.v3);
      }
      else if (strncmp(line, "SPACING", 7) == 0)
      {
        sscanf(line, "%*s %lf %lf %lf", &this->pixspc.v1, &this->pixspc.v2, &this->pixspc.v3);
        printf("[ITKIMAGE]: spacing %lf x %lf x %lf\n", this->pixspc.v1, this->pixspc.v2, this->pixspc.v3);
      }
      else if (strncmp(line, "ORIGIN", 6) == 0)
      {
        sscanf(line, "%*s %lf %lf %lf", &this->orig.v1, &this->orig.v2, &this->orig.v3);
        printf("[ITKIMAGE]: origin %lf x %lf x %lf\n", this->orig.v1, this->orig.v2, this->orig.v3);
      }
      else if (strncmp(line, "POINT_DATA", 10) == 0)
      {
        sscanf(line, "%*s %zu", &this->npix);
        printf("[ITKIMAGE]: pointdata %zu\n", this->npix);
        char * retval = fgets(line, 256*sizeof(char), fp);
        if ((retval) && (strncmp(line, "SCALARS", 7) == 0))
        {
          char comptypestr[16];
          this->ncomp = 1;
          sscanf(line, "%*s %s %s %u", tmpstr, comptypestr, &this->ncomp);
          printf("[ITKIMAGE]: pointdata, name '%s', type '%s', length %u\n", tmpstr, comptypestr, this->ncomp);
          this->comptypeid = itk_get_datatype_id_vtk(comptypestr);
          this->compsz = itk_get_datatype_size(this->comptypeid);
          if (this->compsz != UINT_MAX)
          {
            this->pixsz = this->compsz*this->ncomp;
            this->datasz = this->npix*this->pixsz;
            printf("[ITKIMAGE]: pointdata, size %zu bytes\n", this->datasz);
            long int fpos = mt_ftell(fp);
            if (fgets(line, 256*sizeof(char), fp) && (strncmp(line, "LOOKUP_TABLE", 12) == 0))
              printf("[ITKIMAGE]: LOOKUP_TABLE found\n");
            else
            {
              printf("[ITKIMAGE]: no LOOKUP_TABLE found\n");
              fseek (fp, fpos, SEEK_SET);
            }
            if (datatype == 1)
            {
              this->databuff.resize(this->datasz);
              unsigned int i = 0;
              bool readerror = false;

              // ascii read data
              for (i = 0; (i < this->npix) && (!readerror); i++)
                if (itk_read_pixel_ascii(fp, this->ncomp, this->comptypeid, &this->databuff[i * this->pixsz]) != this->ncomp)
                  readerror = true;

              if (readerror) readflag = -5;
            }
            else if (datatype == 2)
            {
              // allocate data
              this->databuff.resize(this->datasz);

              // binary read data
              if (fread(this->databuff.data(), sizeof(char), this->datasz, fp) != this->datasz)
                readflag = -5;
              else
                byteswap_data();
            }
            else readflag = -4;
          }
          else readflag = -3;
        }
        else if ((retval) && (strncmp(line, "COLOR_SCALARS", 13) == 0))
        {
          this->ncomp = 1;
          sscanf(line, "%*s %s %u", tmpstr, &this->ncomp);
          printf("[ITKIMAGE]: pointdata, color_scalars, name '%s', length %u\n", tmpstr, this->ncomp);
          this->comptypeid = 1; // unsigned char
          this->compsz = 1;
          if (this->compsz != UINT_MAX)
          {
            this->pixsz = this->compsz*this->ncomp;
            this->datasz = this->npix*this->pixsz;
            printf("[ITKIMAGE]: pointdata, size %zu bytes\n", this->datasz);
            if (datatype == 1)
            {
              this->databuff.resize(this->datasz);
              unsigned int i = 0;
              float compval = 0.0;
              unsigned char disccompval = 0;
              bool readerror = false;
              for (i = 0; (i < this->npix*this->ncomp) && (!readerror); i++)
              {
                if (fscanf(fp, "%f", &compval) == 1)
                {
                  disccompval = (unsigned char) (compval*255.0);
                  this->databuff[i] = (char) disccompval;
                }
                else readerror = true;
              }
              if (readerror) readflag = -5;
            }
            else if (datatype == 2)
            {
              this->databuff.resize(this->datasz);
              if (fread(this->databuff.data(), sizeof(char), this->datasz, fp) != this->datasz)
                readflag = -5;
              else
                byteswap_data();
            }
            else readflag = -4;
          }
          else readflag = -3;
        }
        else readflag = -2;
      }
    }
    fclose(fp);

    if (readflag < 0)
    {
      switch (readflag)
      {
        case -1 : printf("[ITKIMAGE]: error while reading file, not supported data set !\n"); break;
        case -2 : printf("[ITKIMAGE]: error while reading file, no scalar point data found !\n"); break;
        case -3 : printf("[ITKIMAGE]: error while reading file, invalid pixel data type id %d !\n", this->comptypeid); break;
        case -4 : printf("[ITKIMAGE]: error while reading file, ascii format not supported yet !\n"); break;
        case -5 : printf("[ITKIMAGE]: error while reading file, pixel data size error !\n"); break;
      }
      return false;
    }
  }
  else {
    printf("[ITKIMAGE]: failed to open '%s' \n", filename);
    return false;
  }

  return true;
}

bool itk_image::read_file_nrrd(const char * filename)
{
  FILE * fp;
  fp = fopen(filename, "rb");
  if (fp)
  {
    int datatype = 0, filetype = 0; // 0 ... not set
    char line[256], tmpstr[32];
    int readflag = 0, imgdim = -1, spdim = -1, endian = -1;
    dcmo = DCM_AXES_UNKOWN;
    unsigned int headerflag = 0;
    bool headerdone = false;

    // read header
    while ((!headerdone) && (!readflag) && fgets(line, 256*sizeof(char), fp))
    {
      cstring_rstrip(line);
      if (line[0] && (line[0] != '#'))
      {
        if (strncmp(line, "NRRD0004", 8) == 0) {
          filetype = 4;
          printf("[ITKIMAGE]: NRRD type %d\n", filetype);
        }
        else if (strncmp(line, "NRRD0005", 8) == 0) {
          filetype = 5;
          printf("[ITKIMAGE]: NRRD type %d\n", filetype);
        }
        else if (strncmp(line, "dimension: ", 11) == 0)
        {
          sscanf(&line[11], "%d", &imgdim);
          printf("[ITKIMAGE]: dimension %u\n", imgdim);
          if (imgdim != 3) readflag = -1;
          else headerflag |= 1;
        }
        else if (strncmp(line, "space dimension: ", 17) == 0)
        {
          sscanf(&line[17], "%d", &spdim);
          printf("[ITKIMAGE]: space dimension %u\n", spdim);
          if (spdim != 3) readflag = -5;
          //else headerflag |= 2;
        }
        else if (strncmp(line, "space: ", 7) == 0)
        {
          sscanf(&line[7], "%s", tmpstr);
          spdim = itk_get_spacedimension_nrrd(tmpstr);
          printf("[ITKIMAGE]: space dimension %u ( '%s' )\n", spdim, tmpstr);
          if (spdim != 3) readflag = -5;
          //else headerflag |= 2;
          // also read out dicom axes orientation
          dcm_orient idcmo = itk_get_dicom_orientation_nrrd(tmpstr);
          printf("[ITKIMAGE]: anatomical axes orientation %u ( '%s' )\n", idcmo, tmpstr);
          this->dcmo = idcmo;
        }
        else if (strncmp(line, "type: ", 6) == 0)
        {
          strncpy(tmpstr, &line[6], 32);
          printf("[ITKIMAGE]: type '%s'\n", tmpstr);
          this->comptypeid = itk_get_datatype_id_nrrd(tmpstr);
          this->compsz = itk_get_datatype_size(this->comptypeid);
          if (this->compsz == UINT_MAX) readflag = -3;
          else headerflag |= 4;
        }
        else if (strncmp(line, "encoding: ", 10) == 0)
        {
          sscanf(&line[10], "%s", tmpstr);
          printf("[ITKIMAGE]: encoding '%s'\n", tmpstr);
          datatype = itk_get_filetype_id_nrrd(tmpstr);
          if (datatype == -1) readflag = -2;
          else headerflag |= 8;
        }
        else if (strncmp(line, "endian: ", 8) == 0)
        {
          sscanf(&line[8], "%s", tmpstr);
          printf("[ITKIMAGE]: endian '%s'\n", tmpstr);
          if (strncmp(tmpstr, "big", 3) == 0)         endian = BIG_ENDIAN;
          else if (strncmp(tmpstr, "little", 6) == 0) endian = LITTLE_ENDIAN;
          else readflag = -6;
        }
        else if (strncmp(line, "sizes: ",  7) == 0)
        {
          sscanf(&line[7], "%zu %zu %zu", &this->dim.v1, &this->dim.v2, &this->dim.v3);
          printf("[ITKIMAGE]: dimensions %zu x %zu x %zu ( %zu )\n", this->dim.v1, this->dim.v2, this->dim.v3, this->dim.v1*this->dim.v2*this->dim.v3);
          headerflag |= 16;
        }
        else if (strncmp(line, "space directions: ", 18) == 0)
        {
          double dir[9];
          sscanf(&line[18], "(%lf,%lf,%lf) (%lf,%lf,%lf) (%lf,%lf,%lf)", &dir[0], &dir[1], &dir[2], &dir[3], &dir[4], &dir[5], &dir[6], &dir[7], &dir[8]);
          this->pixspc.v1 = sqrt(dir[0]*dir[0]+dir[1]*dir[1]+dir[2]*dir[2]);
          this->pixspc.v2 = sqrt(dir[3]*dir[3]+dir[4]*dir[4]+dir[5]*dir[5]);
          this->pixspc.v3 = sqrt(dir[6]*dir[6]+dir[7]*dir[7]+dir[8]*dir[8]);
          printf("[ITKIMAGE]: spacing %lf x %lf x %lf\n", this->pixspc.v1, this->pixspc.v2, this->pixspc.v3);
        }
        else if (strncmp(line, "spacings: ", 10) == 0)
        {
          sscanf(&line[10], " %lf %lf %lf", &this->pixspc.v1, &this->pixspc.v2, &this->pixspc.v3);
          printf("[ITKIMAGE]: spacing %lf x %lf x %lf\n", this->pixspc.v1, this->pixspc.v2, this->pixspc.v3);
        }
        else if (strncmp(line, "space origin: ", 14) == 0)
        {
          sscanf(&line[14], "(%lf,%lf,%lf)", &this->orig.v1, &this->orig.v2, &this->orig.v3);
          printf("[ITKIMAGE]: origin %lf  %lf  %lf\n", this->orig.v1, this->orig.v2, this->orig.v3);
        }
        else if (strncmp(line, "axis mins: ", 11) == 0)
        {
          sscanf(&line[11], "%lf %lf %lf", &this->orig.v1, &this->orig.v2, &this->orig.v3);
          printf("[ITKIMAGE]: origin %lf  %lf  %lf   (from axismins)\n", this->orig.v1, this->orig.v2, this->orig.v3);
        }
        else if (strncmp(line, "axismins: ", 10) == 0)
        {
          sscanf(&line[10], "%lf %lf %lf", &this->orig.v1, &this->orig.v2, &this->orig.v3);
          printf("[ITKIMAGE]: origin %lf  %lf  %lf   (from axismins)\n", this->orig.v1, this->orig.v2, this->orig.v3);
        }
      }
      else if (line[0] && (line[0] == '#')) printf("[ITKIMAGE]: comment '%s'\n", &line[1]);
      else if (!line[0] && ((headerflag & (1|4|8|16)) == (1|4|8|16))) headerdone = true;
    }
    if (spdim == -1)
      spdim = imgdim;
    // read data
    if ((!readflag) && (headerdone))
    {
      printf("[ITKIMAGE]: reading data, type %d\n", datatype);
      this->ncomp = 1;
      this->npix = this->dim.v1*this->dim.v2*this->dim.v3;
      this->pixsz = this->compsz*this->ncomp;
      this->datasz = this->npix*this->pixsz;
      printf("[ITKIMAGE]: pixel size %u, data size %zu\n", this->pixsz, this->datasz);
      if (datatype == 0) // binary
      {
        this->databuff.resize(this->datasz);
        const auto SZE = fread(this->databuff.data(), sizeof(char), this->datasz, fp);
        if (SZE != this->datasz)
          readflag = -4;
        else if (endian != itk_get_endianness())
          byteswap_data();
      }
      else if (datatype == 1) // ascii
      {
        this->databuff.resize(this->datasz);
        bool readerror = false;
        // ascii read data
        for (size_t i = 0; (i < this->npix) && (!readerror); i++)
          if (itk_read_pixel_ascii(fp, this->ncomp, this->comptypeid, &this->databuff[i * this->pixsz]) != this->ncomp)
            readerror = true;
         if (readerror) readflag = -4;
      }
#ifdef MT_GZIP
      else if (datatype == 3) // gzip
      {
        // the following master piece of code was taken from
        // https://gitlab.kitware.com/vtk/vtk/-/blob/06b396b29ca95835ccf5cfd1cd726b7659de0d32/IO/Image/vtkNrrdReader.cxx#L873
        long int pos = mt_ftell(fp);

#ifdef __WIN32__
        // on Windows we have to set O_BINARY, on Linux the constant doesnt always exist.
        // this is very sad ..
        int flags = O_RDONLY | O_BINARY;
#else
        int flags = O_RDONLY;
#endif

        int fd = open(filename, flags);
        if (fd < 0)
          readflag = -8;
        else
        {
          lseek(fd, pos, SEEK_SET);
          gzFile gf = gzdopen(fd, "r");
          if (gf == NULL)
          {
            readflag = -8;
            close(fd);
          }
          else
          {
            this->databuff.resize(this->datasz);
            int rsize = gzread(gf, this->databuff.data(), this->datasz);
            if ((rsize < 0) || (static_cast<unsigned int>(rsize) != this->datasz))
              readflag = -8;
            gzclose(gf); // closes `fd`
          }
        }
      }
#endif
      else
        readflag = -7;
    }
    else puts("[ITKIMAGE]: error while reading header !");
    fclose(fp);

    if (readflag < 0)
    {
      switch (readflag)
      {
        case -1 : printf("[ITKIMAGE]: error while reading file, dimension == %d != 3 !\n", imgdim); break;
        case -2 : printf("[ITKIMAGE]: error while reading file, unknwon datatype !\n"); break;
        case -3 : printf("[ITKIMAGE]: error while reading file, invalid pixel data type id %d !\n", this->comptypeid); break;
        case -4 : printf("[ITKIMAGE]: error while reading file, pixel data size error !\n"); break;
        case -5 : printf("[ITKIMAGE]: error while reading file, space dimension == %d != 3 supported !\n", spdim); break;
        case -6 : printf("[ITKIMAGE]: error while reading file, unknown endian type !\n"); break;
        case -7 : printf("[ITKIMAGE]: error while reading file, datatype %d not supported!\n", datatype); break;
        case -8 : printf("[ITKIMAGE]: error while reading file, failed to read compressed data"); break;
      }
      return false;
    }
  }
  else {
    printf("[ITKIMAGE]: failed to open '%s' \n", filename);
    return false;
  }
  return true;
}

bool itk_image::write_file_vtk(const char * filename, int comp)
{
  FILE * fp;
  fp = fopen(filename, "wb");

  unsigned int ncomp_output = comp > -1 ? 1 : this->ncomp;

  if (fp) {
    char line[256];
    sprintf(line, "# vtk DataFile Version 3.0\n"
                  "GENERATED BY CFEL\n"
                  "BINARY\n"
                  "DATASET STRUCTURED_POINTS\n"
                  "DIMENSIONS %zu %zu %zu\n", this->dim.v1, this->dim.v2, this->dim.v3);
    fwrite(line, sizeof(char), strlen(line), fp);
    sprintf(line, "SPACING %.16e %.16e %.16e\n"
                  "ORIGIN %.16e %.16e %.16e\n", this->pixspc.v1, this->pixspc.v2, this->pixspc.v3,
                                                  this->orig.v1, this->orig.v2, this->orig.v3);
    fwrite(line, sizeof(char), strlen(line), fp);
    sprintf(line, "POINT_DATA %zu\n"
                  "SCALARS scalars %s %u\n"
                  "LOOKUP_TABLE default\n", this->npix, itk_get_datatype_str_vtk(this->comptypeid), ncomp_output);
    fwrite(line, sizeof(char), strlen(line), fp);

    byteswap_data();
    size_t SZE = 0, TGT_SZE = 0;

    // extract certain component ncomp
    if(comp > -1) {
      TGT_SZE = this->npix * this->compsz;
      mt_vector<char> export_buff(TGT_SZE);

      size_t read_offset = comp * this->compsz;
      for(size_t i=0; i<this->npix; i++)
        memcpy(export_buff.data() + i*this->compsz, this->databuff.data() + i*this->pixsz + read_offset, this->compsz);

      SZE = fwrite(export_buff.data(), sizeof(char), export_buff.size(), fp);
    } else {
      TGT_SZE = this->datasz;
      SZE = fwrite(this->databuff.data(), sizeof(char), this->datasz, fp);
    }

    byteswap_data();
    fclose(fp);
    printf("[ITKIMAGE]: written to '%s' \n", filename);
    return (SZE == TGT_SZE);
  } else {
    printf("[ITKIMAGE]: failed to open '%s' \n", filename);
    return false;
  }
}

bool itk_image::write_file_nrrd(const char * filename, unsigned int comp)
{
  FILE * fp = fopen(filename, "wb");

#ifdef MT_GZIP
  const char* encoding = "gzip";
#else
  const char* encoding = "raw";
#endif


  if (fp) {
    const int endian = itk_get_endianness();
    const char* endian_str = (endian == LITTLE_ENDIAN) ? "little" : "big";

    char line[256];
    sprintf(line, "NRRD0005\n"
                  "# GENERATED BY CFEL\n"
                  "dimension: 3\n"
                  "space dimension: 3\n"
                  "type: %s\n"
                  "encoding: %s\n"
                  "endian: %s\n",
                  itk_get_datatype_str_nrrd(this->comptypeid), encoding, endian_str);
    fwrite(line, sizeof(char), strlen(line), fp);
    sprintf(line, "sizes: %zu %zu %zu\n"
                  "space directions: (%lg,0,0) (0,%lg,0) (0,0,%lg)\n"
                  "space origin: (%lg,%lg,%lg)\n\n",
                  this->dim.v1, this->dim.v2, this->dim.v3,
                  this->pixspc.v1, this->pixspc.v2, this->pixspc.v3,
                  this->orig.v1, this->orig.v2, this->orig.v3);

    fwrite(line, sizeof(char), strlen(line), fp);

    size_t SZE = 0, TGT_SZE = 0;

    auto do_write = [&](mt_vector<char> & buffer) {
#ifdef MT_GZIP
      {
        fclose(fp);
        gzFile gf = gzopen(filename, "ab");
        const int rval = gzwrite(gf, buffer.data(), sizeof(char)*buffer.size());
        gzclose(gf);
        if (rval > 0)
          SZE = static_cast<size_t>(rval);
        else
          printf("[ITKIMAGE]: error %d while writing '%s' !\n", rval, filename);
      }
#else
      {
        SZE = fwrite(buffer.data(), sizeof(char), buffer.size(), fp);
        fclose(fp);
      }
#endif
    };

    if(this->ncomp > 1) {
      TGT_SZE = this->npix * this->compsz;

      mt_vector<char> export_buff(TGT_SZE);
      size_t read_offset = comp * this->compsz;
      for(size_t i=0; i<this->npix; i++)
        memcpy(export_buff.data() + i*this->compsz, this->databuff.data() + i*this->pixsz + read_offset, this->compsz);

      do_write(export_buff);
    } else {
      TGT_SZE = this->datasz;
      do_write(this->databuff);
    }

    printf("[ITKIMAGE]: written to '%s' \n", filename);
    return (SZE == this->datasz);
  }
  else {
    printf("[ITKIMAGE]: failed to open '%s' \n", filename);
    return false;
  }
}

bool itk_image::write_file_raw(const char * filename)
{
  FILE * fp = fopen(filename, "wb");
  if (fp) {
    const size_t SZE = fwrite(databuff.begin(), sizeof(char), databuff.size(), fp);
    fclose(fp);
    printf("[ITKIMAGE]: written to '%s' \n", filename);    
    return (SZE == this->datasz);    
  }
  else {
    printf("[ITKIMAGE]: failed to open '%s' \n", filename);
    return false;
  }
}
