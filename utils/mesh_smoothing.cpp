/**
* @file mesh_smoothing.cpp
* @brief Mesh smoothing algorithms and classes.
* @author Aurel Neic
* @version
* @date 2017-08-16
*/

#include "mt_utils_base.h"
#include "mesh_smoothing.h"
#include "topology_utils.h"
#include "mesh_quality.h"
#include "mesh_utils.h"
#include "lookup_table.hpp"

bool centroid_smoothing_kernel(const mt_meshdata & m,
                               const mt_real * xyz,
                               const mt_real* vols,
                               const mt_idx_t nidx,
                               const mt_real sca,
                               vec3r & np)
{
  // full mesh connectivity needs to be set up
  assert(m.e2n_dsp.size() > 0);
  assert(m.n2n_cnt.size() > 0 && m.n2e_cnt.size() > 0);
  mt_real totvol = 0.0;
  //get the point to smooth
  np.get(xyz + 3*nidx);
  vec3r avrg(0,0,0);

  const mt_idx_t start = m.n2e_dsp[nidx], stop = start + m.n2e_cnt[nidx];
  for(mt_idx_t i=start; i < stop; i++) {
    const mt_idx_t eidx = m.n2e_con[i];
    const mt_idx_t* con = m.e2n_con.data() + m.e2n_dsp[eidx];
    vec3r bary = barycenter(m.e2n_cnt[eidx], con, xyz);

    const mt_real vol = vols[eidx];
    avrg += (bary * vol);
    totvol += vol;
  }
  if(totvol > 0.0) {
    avrg /= totvol;
    np += (np - avrg) * sca;
  }
  return (totvol > 0.0);
}

bool circumsphere_smoothing_kernel(const mt_meshdata & m,
                                   const mt_real * xyz,
                                   const mt_real* vols,
                                   const mt_idx_t nidx,
                                   const mt_real sca,
                                   vec3r & np)
{
  // full mesh connectivity needs to be set up
  assert(m.e2n_dsp.size() > 0);
  assert(m.n2n_cnt.size() > 0 && m.n2e_cnt.size() > 0);
  mt_real totvol = 0.0;
  //get the point to smooth
  np.get(xyz + 3*nidx);
  vec3r avrg(0,0,0);

  const mt_idx_t start = m.n2e_dsp[nidx], stop = start + m.n2e_cnt[nidx];
  for(mt_idx_t i=start; i < stop; i++) {
    const mt_idx_t eidx = m.n2e_con[i];
    const mt_idx_t* con = m.e2n_con.data() + m.e2n_dsp[eidx];
    vec3r midpnt;
    circumsphere(m.etype[eidx], con, xyz, midpnt);
    const mt_real vol = vols[eidx];
    avrg.x += vol * midpnt.x;
    avrg.y += vol * midpnt.y;
    avrg.z += vol * midpnt.z;
    totvol += vol;
  }
  if(totvol > 0.0) {
    avrg /= totvol;
    np += (np - avrg) * sca;
  }
  return (totvol > 0.0);
}

bool laplace_smoothing_kernel(const mt_meshdata & m, const mt_real * xyz, const mt_idx_t nidx, const mt_real sca, vec3r & np)
{
  const mt_idx_t start = m.n2n_dsp[nidx], stop = start + m.n2n_cnt[nidx];
  vec3r avrg(0,0,0), cp;
  mt_idx_t numadd = 0;

  //get the point to smooth
  np.get(xyz + 3*nidx);
  vec3r op = np;

  for(mt_idx_t i=start; i<stop; i++) {
    const mt_idx_t cidx = m.n2n_con[i];
    cp.get(xyz + cidx*3);
    avrg += cp;
    numadd++;
  }
  // instead of testing cidx != nidx, we remove nidx after the loop
  numadd--;
  avrg -= op;

  if(numadd) {
    avrg /= mt_real(numadd);
    np += (avrg - np) * sca;
  }

  return (numadd > 0);
}


void smooth_iter(mt_meshdata & mesh,
                 mt_vector<vec3r> & upd,
                 const mt_meshdata & manifold,
                 const mt_mask & mnfld_mask,
                 const mt_vector<mt_idx_t> & nodes,
                 mt_real sca)
{
  size_t nnodes = nodes.size();

  #ifdef OPENMP
  #pragma omp parallel
  #endif
  {
    #ifdef OPENMP
    #pragma omp for schedule(dynamic, 100)
    #endif
    for(size_t k=0; k < nnodes; k++)
    {
      const mt_idx_t nidx = nodes[k];
      const mt_meshdata & msh = mnfld_mask.count(nidx) ? manifold : mesh;
      laplace_smoothing_kernel(msh, mesh.xyz.data(), nidx, sca, upd[nidx]);
    }

    #ifdef OPENMP
    #pragma omp barrier
    #endif

    #ifdef OPENMP
    #pragma omp for schedule(dynamic, 100)
    #endif
    for(size_t k=0; k < nnodes; k++)
    {
      const mt_idx_t nidx = nodes[k];

      mesh.xyz[nidx*3+0] = upd[nidx].x;
      mesh.xyz[nidx*3+1] = upd[nidx].y;
      mesh.xyz[nidx*3+2] = upd[nidx].z;
    }
  }
}

void quality_aware_smoother::init_qual_thr(const mt_meshdata & checkmesh,
                   const mt_vector<mt_idx_t> & nodes,
                   const mt_real inp_thr)
{
  const size_t nnodes = nodes.size();
  _qual_thr.resize(nnodes);

  #ifdef OPENMP
  #pragma omp parallel for schedule(dynamic, 100)
  #endif
  for(size_t k=0; k < nnodes; k++)
  {
    const mt_idx_t nidx = nodes[k];
    mt_real qual; mt_idx_t qual_eidx;

    nbhd_quality_max(checkmesh, nidx, qual, qual_eidx);

    if(qual != qual) {
      _qual_thr[k] = inp_thr;
    }
    else if(qual < QUAL_THR_CHK_SCA * inp_thr) {
      _qual_thr[k] = 0.0;
    }
    else {
      _qual_thr[k] = qual < inp_thr ? inp_thr : qual;
    }
  }
}

void quality_aware_smoother::qual_smooth_iter_fwd(mt_meshdata & checkmesh,
                          mt_meshdata & smoothmesh,
                          mt_meshdata & manifold,
                          const mt_mask & mnfld_mask,
                          const mt_vector<mt_idx_t> & nodes,
                          const mt_real sca,
                          const mt_real inp_thr)
{
  const size_t nnodes = nodes.size();
  mt_real *__restrict xyz = checkmesh.xyz.data();

  #ifdef OPENMP
  #pragma omp parallel
  #endif
  {
    #ifdef OPENMP
    #pragma omp for schedule(guided)
    #endif
    for(size_t k=0; k < nnodes; k++)
    {
      const mt_idx_t nidx = nodes[k];
      mt_point<mt_real> np, op(xyz + nidx*3);

      const mt_meshdata &        msh = mnfld_mask.count(nidx) ? manifold : smoothmesh;
      laplace_smoothing_kernel(msh, xyz, nidx, sca, np);

      _applied[k] = true;
      _upd[nidx] = np;

      const double qual_thr = _qual_thr[k];
      if(qual_thr > 0.0) {
        const double qual = nbhd_quality_max(checkmesh, nidx, np);

        if(qual < qual_thr) {
          if(qual_thr > inp_thr)
            _qual_thr[k] = qual;
        }
        else {
          _applied[k] = false;
        }
      }
    }

    #ifdef OPENMP
    #pragma omp barrier
    #endif

    #ifdef OPENMP
    #pragma omp for schedule(guided)
    #endif
    for(size_t k=0; k < nnodes; k++)
    {
      if(_applied[k]) {
        const mt_idx_t nidx = nodes[k];
        _upd[nidx].set(xyz + nidx*3);
      }
    }
  }
}

void quality_aware_smoother::qual_smooth_iter_fwd(const mt_vector<mt_meshdata*> & manifolds,
                                                  const mt_vector<int> & mnfld_idx,
                                                  mt_vector<mt_real> & inp_xyz,
                                                  const mt_vector<mt_idx_t> & nodes,
                                                  const mt_real sca,
                                                  const mt_real inp_thr)
{
  const size_t nnodes = nodes.size();
  mt_real *__restrict xyz = inp_xyz.data();

  // we check the mesh quality w.r.t. the first mesh in manifolds.
  const mt_meshdata & checkmesh = *manifolds[0];

  #ifdef OPENMP
  #pragma omp parallel for schedule(guided)
  #endif
  for(size_t k=0; k < nnodes; k++)
  {
    const mt_idx_t nidx = nodes[k];
    vec3r avrg(0, 0, 0), np, op(xyz + nidx*3);

    // each node is associated to a manifold graph
    const mt_meshdata & msh = *manifolds[mnfld_idx[nidx]];
    bool did_smth = laplace_smoothing_kernel(msh, xyz, nidx, sca, np);

    if(did_smth) {
      #ifdef OPENMP
      # pragma omp atomic write
      xyz[nidx*3+0] = np.x;
      # pragma omp atomic write
      xyz[nidx*3+1] = np.y;
      # pragma omp atomic write
      xyz[nidx*3+2] = np.z;
      #else
      xyz[nidx*3+0] = np.x, xyz[nidx*3+1] = np.y, xyz[nidx*3+2] = np.z;
      #endif
    }
    _applied[k] = true;
    const double qual_thr = _qual_thr[k];

    if(qual_thr > 0.0) {
      const double qual = nbhd_quality_max(checkmesh, nidx);

      if(qual < qual_thr) {
        if(qual_thr > inp_thr)
          _qual_thr[k] = qual;
      }
      else {
        #ifdef OPENMP
        # pragma omp atomic write
        xyz[nidx*3+0] = op.x;
        # pragma omp atomic write
        xyz[nidx*3+1] = op.y;
        # pragma omp atomic write
        xyz[nidx*3+2] = op.z;
        #else
        xyz[nidx*3+0] = op.x, xyz[nidx*3+1] = op.y, xyz[nidx*3+2] = op.z;
        #endif
        _applied[k] = false;
      }
    }
  }
}

void quality_aware_smoother::qual_smooth_iter_bwd(mt_meshdata & checkmesh,
                          mt_meshdata & smoothmesh,
                          mt_meshdata & manifold,
                          const mt_mask & mnfld_mask,
                          const mt_vector<mt_idx_t> & nodes,
                          mt_real sca,
                          mt_real inp_thr)
{
  size_t nnodes = nodes.size();
  mt_real *__restrict xyz = checkmesh.xyz.data();

  #ifdef OPENMP
  #pragma omp parallel
  #endif
  {
    #ifdef OPENMP
    #pragma omp for schedule(guided)
    #endif
    for(size_t k=0; k < nnodes; k++)
    {
      if(_applied[k]) {
        const mt_idx_t nidx = nodes[k];
        vec3r avrg(0, 0, 0), np, op(xyz + nidx*3);
        const mt_meshdata &        msh = mnfld_mask.count(nidx) ? manifold : smoothmesh;

        laplace_smoothing_kernel(msh, xyz, nidx, sca, np);
        _upd[nidx] = np;

        const double qual_thr = _qual_thr[k];
        if(qual_thr > 0.0) {
          const double qual = nbhd_quality_max(checkmesh, nidx, np);

          if(qual < qual_thr) {
            if(qual_thr > inp_thr)
              _qual_thr[k] = qual;
          } else {
            _upd[nidx] = op;
          }
        }
      }
    }

    #ifdef OPENMP
    #pragma omp barrier
    #endif

    #ifdef OPENMP
    #pragma omp for schedule(guided)
    #endif
    for(size_t k=0; k < nnodes; k++)
    {
      if(_applied[k]) {
        const mt_idx_t nidx = nodes[k];
        _upd[nidx].set(xyz + nidx*3);

        _applied[k] = false;
      }
    }
  }
}

void quality_aware_smoother::qual_smooth_iter_bwd(const mt_vector<mt_meshdata*> & manifolds,
                                                  const mt_vector<int> & mnfld_idx,
                                                  mt_vector<mt_real> & inp_xyz,
                                                  const mt_vector<mt_idx_t> & nodes,
                                                  const mt_real sca,
                                                  const mt_real inp_thr)
{
  size_t nnodes = nodes.size();
  mt_real *__restrict xyz = inp_xyz.data();

  // we check the mesh quality w.r.t. the first mesh in manifolds.
  const mt_meshdata & checkmesh = *manifolds[0];

  #ifdef OPENMP
  #pragma omp parallel for schedule(guided)
  #endif
  for(size_t k=0; k < nnodes; k++)
  {
    const mt_idx_t nidx = nodes[k];

    if(_applied[k]) {
      mt_point<mt_real> avrg(0, 0, 0), np(xyz + nidx*3);
      mt_point<mt_real> op = np;

      // each node is associated to a manifold graph
      const mt_meshdata & msh = *manifolds[mnfld_idx[nidx]];
      bool did_smth = laplace_smoothing_kernel(msh, xyz, nidx, sca, np);

      if(did_smth) {
        #ifdef OPENMP
        # pragma omp atomic write
        xyz[nidx*3+0] = np.x;
        # pragma omp atomic write
        xyz[nidx*3+1] = np.y;
        # pragma omp atomic write
        xyz[nidx*3+2] = np.z;
        #else
        xyz[nidx*3+0] = np.x, xyz[nidx*3+1] = np.y, xyz[nidx*3+2] = np.z;
        #endif
      }
      _applied[k] = false;
      const double qual_thr = _qual_thr[k];

      if(qual_thr > 0.0) {
        const double qual = nbhd_quality_max(checkmesh, nidx);

        if(qual < qual_thr) {
          if(qual_thr > inp_thr)
            _qual_thr[k] = qual;
        }
        else {
          #ifdef OPENMP
          # pragma omp atomic write
          xyz[nidx*3+0] = op.x;
          # pragma omp atomic write
          xyz[nidx*3+1] = op.y;
          # pragma omp atomic write
          xyz[nidx*3+2] = op.z;
          #else
          xyz[nidx*3+0] = op.x, xyz[nidx*3+1] = op.y, xyz[nidx*3+2] = op.z;
          #endif
        }
      }
    }
  }
}


void quality_aware_smoother::operator()(mt_meshdata & checkmesh,
                mt_meshdata & smoothmesh,
                mt_meshdata & manifold,
                const mt_mask & mnfld_mask,
                const mt_vector<mt_idx_t> & nodes,
                const size_t nsmooth,
                const mt_real sca,
                const mt_real inp_thr)
{
  _applied.assign(nodes.size(), false);
  _upd.resize(checkmesh.xyz.size() / 3);

  init_qual_thr(checkmesh, nodes, inp_thr);
  PROGRESS<size_t> progress(nsmooth, "laplace smoothing progress: ");

  for(size_t sidx = 0; sidx < nsmooth; sidx++) {
    progress.next();

    qual_smooth_iter_fwd(checkmesh, smoothmesh, manifold, mnfld_mask, nodes, sca, inp_thr);
    qual_smooth_iter_bwd(checkmesh, smoothmesh, manifold, mnfld_mask, nodes, -(sca*SMOOTH_FREQ_SCA), inp_thr);

    if((sidx % QUAL_THR_UPD) == 0)
      init_qual_thr(checkmesh, nodes, inp_thr);
  }
  progress.finish();
}

void quality_aware_smoother::operator()(mt_meshdata & checkmesh,
                mt_meshdata & smoothmesh,
                mt_meshdata & manifold,
                const mt_mask & mnfld_mask,
                const mt_vector<mt_idx_t> & nodes,
                const size_t nsmooth,
                const size_t nlevel,
                const mt_real sca,
                const mt_real inp_thr)
{
  if(nlevel == 1)
    return (*this)(checkmesh, smoothmesh, manifold, mnfld_mask, nodes, nsmooth, sca, inp_thr);

  _applied.assign(nodes.size(), false);
  _upd.resize(checkmesh.xyz.size() / 3);

  init_qual_thr(checkmesh, nodes, inp_thr);
  PROGRESS<size_t> progress(nsmooth, "laplace smoothing progress: ");

  mt_vector<mt_cnt_t> mesh_n2n_cnt, man_n2n_cnt;
  mt_vector<mt_idx_t> mesh_n2n_con, man_n2n_con;

  // we extend the connectivity to n levels
  get_nlevel_graph_laplace(smoothmesh, int(nlevel), mesh_n2n_cnt, mesh_n2n_con);
  smoothmesh.n2n_cnt.swap(mesh_n2n_cnt); smoothmesh.n2n_con.swap(mesh_n2n_con);
  bucket_sort_offset(smoothmesh.n2n_cnt, smoothmesh.n2n_dsp);

  if(mnfld_mask.size()) {
    get_nlevel_graph_laplace(manifold, int(nlevel), man_n2n_cnt, man_n2n_con);
    manifold.n2n_cnt.swap(man_n2n_cnt); manifold.n2n_con.swap(man_n2n_con);
    bucket_sort_offset(manifold.n2n_cnt, manifold.n2n_dsp);
  }

  for(size_t sidx = 0; sidx < nsmooth; sidx++) {
    progress.next();

    qual_smooth_iter_fwd(checkmesh, smoothmesh, manifold, mnfld_mask, nodes, sca, inp_thr);
    qual_smooth_iter_bwd(checkmesh, smoothmesh, manifold, mnfld_mask, nodes, -(sca*SMOOTH_FREQ_SCA), inp_thr);

    if((sidx % QUAL_THR_UPD) == 0)
      init_qual_thr(checkmesh, nodes, inp_thr);
  }

  // after smoothing, we reset the connectivity to the old state
  smoothmesh.n2n_cnt.swap(mesh_n2n_cnt); smoothmesh.n2n_con.swap(mesh_n2n_con);
  bucket_sort_offset(smoothmesh.n2n_cnt, smoothmesh.n2n_dsp);
  if(mnfld_mask.size()) {
    manifold.n2n_cnt.swap(man_n2n_cnt); manifold.n2n_con.swap(man_n2n_con);
    bucket_sort_offset(manifold.n2n_cnt, manifold.n2n_dsp);
  }

  progress.finish();
}

void quality_aware_smoother::operator()(const mt_vector<mt_meshdata*> & manifolds,
                                                  const mt_vector<int> & mnfld_idx,
                                                  mt_vector<mt_real> & inp_xyz,
                                                  const mt_vector<mt_idx_t> & nodes,
                                                  const size_t  nsmooth,
                                                  const mt_real sca,
                                                  const mt_real inp_thr)
{
  // we check the mesh quality w.r.t. the first mesh in manifolds.
  mt_meshdata & checkmesh    = *manifolds[0];
  bool  checkmesh_had_no_xyz = false;

  if(checkmesh.xyz.size() != inp_xyz.size()) {
    // if the checkmesh has no coords, we do a shallow copy of the input xyz
    if(checkmesh.xyz.size()) {
      fprintf(stderr, "%s error: Coords of base manifold do not match input coords! Aborting!\n",
              __func__);
      exit(EXIT_FAILURE);
    }

    checkmesh.xyz.assign(inp_xyz.size(), inp_xyz.data(), false);
    checkmesh_had_no_xyz = true;
  }

  _applied.assign(nodes.size(), false);
  init_qual_thr(checkmesh, nodes, inp_thr);

  PROGRESS<size_t> progress(nsmooth, "Smoothing progress: ");

  for(size_t sidx = 0; sidx < nsmooth; sidx++) {
    progress.next();

    qual_smooth_iter_fwd(manifolds, mnfld_idx, inp_xyz, nodes, sca, inp_thr);
    qual_smooth_iter_bwd(manifolds, mnfld_idx, inp_xyz, nodes, -(sca*SMOOTH_FREQ_SCA), inp_thr);

    if((sidx % QUAL_THR_UPD) == 0)
      init_qual_thr(checkmesh, nodes, inp_thr);
  }
  progress.finish();

  if(checkmesh_had_no_xyz)
    checkmesh.xyz.assign(size_t(0), NULL, false);
}



void smooth_nodes(mt_meshdata & mesh,
                  mt_meshdata & manifold,
                  const mt_mask & mnfld_mask,
                  const mt_vector<mt_idx_t> & nodes,
                  size_t nsmooth,
                  mt_real sca,
                  bool verbose,
                  bool taubin_type)
{
  mt_vector<vec3r>   upd(mesh.xyz.size() / 3);
  PROGRESS<size_t>* progress = verbose ? new PROGRESS<size_t>(nsmooth, "laplace smoothing progress: ") :
                                         NULL;

  for(size_t sidx = 0; sidx < nsmooth; sidx++)
  {
    if(progress) progress->next();
    smooth_iter(mesh, upd, manifold, mnfld_mask, nodes, sca);
    if(taubin_type)
      smooth_iter(mesh, upd, manifold, mnfld_mask, nodes, -(sca*SMOOTH_FREQ_SCA));
  }

  if(progress) {
    progress->finish();
    delete progress;
  }
}

void smooth_nodes(mt_meshdata & mesh,
                  mt_meshdata & manifold,
                  const mt_mask & mnfld_mask,
                  const mt_vector<mt_idx_t> & nodes,
                  size_t nsmooth,
                  size_t nlevel,
                  mt_real sca,
                  bool verbose,
                  bool taubin_type)
{
  if(nlevel == 1) {
    smooth_nodes(mesh, manifold, mnfld_mask, nodes, nsmooth, sca, verbose);
    return;
  }

  mt_vector<vec3r>   upd(mesh.xyz.size() / 3);
  PROGRESS<size_t>* progress = verbose ?
                        new PROGRESS<size_t>(nsmooth, "n-level laplace smoothing progress: ") :
                        NULL;

  mt_vector<mt_cnt_t> mesh_n2n_cnt, man_n2n_cnt;
  mt_vector<mt_idx_t> mesh_n2n_con, man_n2n_con;

  // we extend the connectivity to n levels
  get_nlevel_graph_laplace(mesh, int(nlevel), mesh_n2n_cnt, mesh_n2n_con);
  mesh.n2n_cnt.swap(mesh_n2n_cnt); mesh.n2n_con.swap(mesh_n2n_con);
  bucket_sort_offset(mesh.n2n_cnt, mesh.n2n_dsp);

  if(mnfld_mask.size()) {
    get_nlevel_graph_laplace(manifold, int(nlevel), man_n2n_cnt, man_n2n_con);
    manifold.n2n_cnt.swap(man_n2n_cnt); manifold.n2n_con.swap(man_n2n_con);
    bucket_sort_offset(manifold.n2n_cnt, manifold.n2n_dsp);
  }

  for(size_t sidx = 0; sidx < nsmooth; sidx++)
  {
    if(progress) progress->next();
    smooth_iter(mesh, upd, manifold, mnfld_mask, nodes, sca);
    if(taubin_type)
      smooth_iter(mesh, upd, manifold, mnfld_mask, nodes, -(sca*SMOOTH_FREQ_SCA));
  }

  // after smoothing, we reset the connectivity to the old state
  mesh.n2n_cnt.swap(mesh_n2n_cnt); mesh.n2n_con.swap(mesh_n2n_con);
  bucket_sort_offset(mesh.n2n_cnt, mesh.n2n_dsp);
  if(mnfld_mask.size()) {
    manifold.n2n_cnt.swap(man_n2n_cnt); manifold.n2n_con.swap(man_n2n_con);
    bucket_sort_offset(manifold.n2n_cnt, manifold.n2n_dsp);
  }

  if(progress) {
    progress->finish();
    delete progress;
  }
}

void volumetric_smooth_from_tags(mt_meshdata & mesh,
                                 const mt_vector<MT_USET<mt_tag_t>> & stags,
                                 const int iter,
                                 const mt_real smth,
                                 const short   nlevel,
                                 const mt_real edge_ang,
                                 const mt_real max_qual,
                                 const bool skip_lines,
                                 const bool verbose)
{
  mt_meshdata surfmesh, linemesh;
  MT_USET<mt_idx_t> sm_vtx, ln_vtx, open_surf_vtx, bad_ln_vtx;
  int viter = iter*0.5, siter = iter*0.5;

  // extract surface and line manifolds
  if(verbose)
    std::cout << "Processing tag surfaces into unified surface .." << std::endl;

  unified_surface_from_tags(mesh, stags, surfmesh, &sm_vtx);
  compute_full_mesh_connectivity(surfmesh);

  // check if we are working on an open surface
  mt_meshdata border;
  identify_surface_border(surfmesh, border);

  if(border.e2n_cnt.size()) {
    printf("%s warning: Input surface is not closed.!\n", __func__);
    open_surf_vtx.insert(border.e2n_con.begin(), border.e2n_con.end());
    compute_full_mesh_connectivity(border, false);
    border.xyz.assign(surfmesh.xyz.size(), surfmesh.xyz.data(), true);
  }

  if(verbose)
    std::cout << "Computing line interfaces between surfaces .." << std::endl;

  compute_line_interfaces(surfmesh, mesh.xyz, stags, edge_ang, linemesh);
  compute_full_mesh_connectivity(linemesh);

  // line vertices connected to more than two neighbours are corners and not proper
  // line manifold vertices -> smoothing does not work for them. So we remove from the
  // smoothing vertex set later on. Note that we check n2n_cnt > 3 since one of the
  // edges in the n2n graph is the node itself.
  for(size_t i=0; i<linemesh.n2n_cnt.size(); i++)
    if(linemesh.n2n_cnt[i] > 3) bad_ln_vtx.insert(i);

  ln_vtx.insert(linemesh.e2n_con.begin(), linemesh.e2n_con.end());

  // remove lines from smoothing
  sm_vtx.sort(); ln_vtx.sort();
  for(auto n : ln_vtx) sm_vtx.erase(n);

  // remove open surface border from smoothing
  for(auto n : open_surf_vtx) sm_vtx.erase(n);

  mt_vector<mt_idx_t> sm_nod;
  mt_mask isMnfld(mesh.xyz.size() / 3);

  if(verbose)
    std::cout << "Smoothing .." << std::endl;

  // smooth open edges
  if(!skip_lines && open_surf_vtx.size()) {
    sm_nod.assign(open_surf_vtx.begin(), open_surf_vtx.end());
    isMnfld.insert(border.e2n_con.begin(), border.e2n_con.end());

    smooth_nodes(mesh, border, isMnfld, sm_nod, viter, smth);

    isMnfld.clear();
  }

  // smooth
  sm_nod.assign(sm_vtx.begin(), sm_vtx.end());
  isMnfld.insert(surfmesh.e2n_con.begin(), surfmesh.e2n_con.end());
  quality_aware_smoother smoother;

  if(max_qual)
    smoother(mesh, mesh, surfmesh, isMnfld, sm_nod, viter, nlevel, smth, max_qual);
  else
    smooth_nodes(mesh, surfmesh, isMnfld, sm_nod, viter, nlevel, smth);

  // prepare data-structs for line smoothing
  sm_vtx.clear(); isMnfld.clear();
  sm_vtx.insert(surfmesh.e2n_con.begin(), surfmesh.e2n_con.end());
  for(auto n : bad_ln_vtx)   sm_vtx.erase(n);
  for(auto n : open_surf_vtx) sm_vtx.erase(n);

  if(skip_lines) {
    for(const mt_idx_t & n : ln_vtx) sm_vtx.erase(n);
  }

  sm_nod.assign(sm_vtx.begin(), sm_vtx.end());
  isMnfld.insert(ln_vtx.begin(), ln_vtx.end());

  // a shallow copy of coords into surfmesh
  surfmesh.xyz.assign(mesh.xyz.size(), mesh.xyz.data(), false);

  if(max_qual)
    smoother(mesh, surfmesh, linemesh, isMnfld, sm_nod, siter, nlevel, smth, max_qual);
  else
    smooth_nodes(surfmesh, linemesh, isMnfld, sm_nod, siter, nlevel, smth);

  surfmesh.xyz.assign(0, NULL, false);
}


void surface_smooth(mt_meshdata & surfmesh,
                    const int iter,
                    const mt_real smth,
                    const mt_real edge_ang,
                    const mt_real max_qual,
                    const bool skip_lines,
                    const bool verbose)
{
  mt_meshdata linemesh;
  MT_USET<mt_idx_t> sm_vtx, ln_vtx, bad_ln_vtx;

  if(verbose)
    std::cout << "Computing line interfaces between surfaces .." << std::endl;

  compute_line_interfaces(surfmesh, surfmesh, edge_ang, false, linemesh);
  compute_full_mesh_connectivity(linemesh, false);

  // line vertices connected to more than two neighbours are corners and not proper
  // line manifold vertices -> smoothing does not work for them. So we remove from the
  // smoothing vertex set later on. Note that we check n2n_cnt > 3 since one of the
  // edges in the n2n graph is the node itself.
  for(size_t i=0; i<linemesh.n2n_cnt.size(); i++)
    if(linemesh.n2n_cnt[i] > 3) bad_ln_vtx.insert(i);

  ln_vtx.insert(linemesh.e2n_con.begin(), linemesh.e2n_con.end());
  ln_vtx.sort();

  mt_vector<mt_idx_t> sm_nod;
  mt_mask isMnfld(surfmesh.xyz.size() / 3);

  if(verbose) std::cout << "Smoothing .." << std::endl;
  quality_aware_smoother smoother;

  // prepare data-structs for line smoothing
  sm_vtx.insert(surfmesh.e2n_con.begin(), surfmesh.e2n_con.end());
  for(auto n : bad_ln_vtx) sm_vtx.erase(n);

  if(skip_lines) {
    for(const mt_idx_t & n : ln_vtx) sm_vtx.erase(n);
  }

  sm_nod.assign(sm_vtx.begin(), sm_vtx.end());
  isMnfld.insert(ln_vtx.begin(), ln_vtx.end());

  if(max_qual) smoother(surfmesh, surfmesh, linemesh, isMnfld, sm_nod, iter, smth, max_qual);
  else         smooth_nodes(surfmesh, linemesh, isMnfld, sm_nod, iter, smth, verbose);
}


void directional_smoothing(mt_meshdata & mesh,
                           const mt_vector<mt_idx_t> & nod,
                           const mt_vector<mt_real> & scale_dir,
                           mt_real scale,
                           mt_real ortho_scale,
                           const bool only_positive,
                           const mt_real smth,
                           const int iter)
{
  size_t nnodes = nod.size();

  for(int sidx = 0; sidx < iter; sidx++) {
    #ifdef OPENMP
    #pragma omp parallel for schedule(dynamic, 100)
    #endif
    for(size_t k=0; k < nnodes; k++)
    {
      const mt_idx_t nidx = nod[k];
      vec3r avrg;
      //This way np contains np = np + 1.0 * (avrg - np) = avrg
      bool did_smth = laplace_smoothing_kernel(mesh, mesh.xyz.data(), nidx, 1.0, avrg);
      if(did_smth) {
        vec3r np(mesh.xyz.data() + nidx*3);
        vec3r dir = avrg - np;       // the direction towards the average
        mt_real len = dir.length();  // the step length towards the average
        dir /= len;

        // we (at least partially) orthogonalize w.r.t. to scale_dir
        vec3r sdir(scale_dir.data() + nidx*3);

        // we compute the vectors spanning the orthogonal plane
        vec3r odir1 = sdir.x < 0.95 ? vec3r(1,0,0) : vec3r(0,1,0);
        odir1 -= sdir * (sdir.scaProd(odir1)); odir1.normalize();
        vec3r odir2 = sdir.crossProd(odir1);

        mt_real sdir_proj = sdir.scaProd(dir);

        // if only_positive == true, we allow only positive movement, thus
        // for sdir_proj < 0, we set the scale to 1.0 regardless of user
        // setting, in order to remove that component from dir
        if(only_positive && sdir_proj < 0.0)
          dir -= sdir * sdir_proj;
        else
          dir -= sdir * (sdir_proj * scale);

        // now we (partially) remove the orthogonal directions
        dir -= odir1 * (odir1.scaProd(dir) * ortho_scale);
        dir -= odir2 * (odir2.scaProd(dir) * ortho_scale);

        // we reapply the original step length
        mt_real newlen = dir.length();
        if(newlen > 1e-10) {
          dir = dir * len / newlen;
          np += dir * smth;
          np.set(mesh.xyz.data() + nidx*3);
        }
      }
    }
  }
}

void directional_smoothing_elem(mt_meshdata & mesh,
                           const mt_vector<mt_idx_t> & nod,
                           const mt_vector<mt_real> & ortho_dir,
                           mt_real ortho_scale,
                           const bool only_positive,
                           const mt_real smth,
                           const int iter)
{
  size_t nnodes = nod.size();

  for(int sidx = 0; sidx < iter; sidx++) {
    #ifdef OPENMP
    #pragma omp parallel for schedule(dynamic, 100)
    #endif
    for(size_t k=0; k < nnodes; k++)
    {
      const mt_idx_t nidx = nod[k];
      vec3r avrg(mesh.xyz.data() + nidx*3);
      vec3r np(mesh.xyz.data() + nidx*3);
      bool did_smth = laplace_smoothing_kernel(mesh, mesh.xyz.data(), nidx, 1.0, avrg);
      if(did_smth) {
        vec3r dir = avrg - np;       // the direction towards the average
        mt_real len = dir.length();  // the step length towards the average
        dir /= len;

        for(mt_idx_t i=0; i<mesh.n2e_cnt[nidx]; i++) {
          mt_idx_t eidx = mesh.n2e_con[mesh.n2e_dsp[nidx]+i];

          // we (at least partially) orthogonalize w.r.t. to ortho_dir
          vec3r ortho(ortho_dir.data() + eidx*3);
          mt_real ortho_proj = ortho.scaProd(dir);

          // if only_positive == true, we allow only positive movement, thus
          // for ortho_proj < 0, we set the ortho_scale to 1.0 regardless of user
          // setting, in order to remove that component from dir
          if(only_positive && ortho_proj < 0.0)
            dir -= ortho * ortho_proj;
          else
            dir -= ortho * (ortho_proj * ortho_scale);

        }

        // we reapply the original step length
        mt_real newlen = dir.length();

        if(newlen > 1e-10) {
          dir = dir * len / newlen;
          np += dir * smth;

          mesh.xyz[nidx*3+0] = np.x;
          mesh.xyz[nidx*3+1] = np.y;
          mesh.xyz[nidx*3+2] = np.z;
        }
      }
    }
  }
}

void generate_cell_neighbours(const mt_meshdata & mesh,
                              const mt_vector<mt_idx_t> & selected_elems,
                              mt_vector<mt_vector<mt_idx_t>*> & cell_neighbours)
{
  size_t num_selected_elems = selected_elems.size();
  cell_neighbours.resize(mesh.e2n_cnt.size());
  cell_neighbours.zero();

  #ifdef OPENMP
  #pragma omp parallel for schedule(guided)
  #endif
  for (size_t i = 0; i < num_selected_elems; i++) {
    mt_idx_t cell_idx = selected_elems[i];
    MT_USET<mt_idx_t> nodes_set, neighbours_set;

    neighbours_set.insert(cell_idx);
    elemSet_to_nodeSet(mesh, neighbours_set, nodes_set);
    nodeSet_to_elemSet(mesh, nodes_set, neighbours_set);
    neighbours_set.erase(cell_idx);

    cell_neighbours[cell_idx] = new mt_vector<mt_idx_t>();
    cell_neighbours[cell_idx]->assign(neighbours_set.begin(), neighbours_set.end());
  }
}

/// smooth by projecting the selected_nodes towards the neighbouring
/// triangle planes formed by the tri_normals
static void project_vertices(mt_meshdata & mesh,
                             const mt_vector<mt_idx_t> & selected_nodes,
                             const mt_vector<vec3r> & tri_normals,
                             const mt_real sfac)
{
  size_t num_selected_nodes = selected_nodes.size();

  #ifdef OPENMP
  #pragma omp parallel for schedule(guided)
  #endif
  for (size_t i = 0; i < num_selected_nodes; i++) {
    mt_idx_t pnt_idx = selected_nodes[i];
    mt_real areasum = 0.0;
    vec3r v_projection;

    mt_idx_t start = mesh.n2e_dsp[pnt_idx], stop = start + mesh.n2e_cnt[pnt_idx];
    for (mt_idx_t j = start; j<stop; j++) {
      mt_idx_t elem = mesh.n2e_con[j];

      mt_idx_t *tri_con = mesh.e2n_con.data() + mesh.e2n_dsp[elem];
      mt_real area = volume(mesh.etype[elem], tri_con, mesh.xyz.data());
      areasum += area;

      const vec3r & nrml = tri_normals[elem];
      vec3r centroid = barycenter(mesh.e2n_cnt[elem], tri_con, mesh.xyz.data());
      vec3r vec_pc   = centroid - vec3r(mesh.xyz.data() + pnt_idx * 3);

      v_projection += nrml * vec_pc.scaProd(nrml) * area;
    }

    vec3r dspl = v_projection * mt_real(1.0 / areasum);
    mesh.xyz[pnt_idx*3+0] += dspl.x * sfac;
    mesh.xyz[pnt_idx*3+1] += dspl.y * sfac;
    mesh.xyz[pnt_idx*3+2] += dspl.z * sfac;
  }
}

void update_normals_median_angle(const mt_meshdata & mesh,
                                 const mt_vector<mt_idx_t> & selected_elems,
                                 const mt_vector<mt_vector<mt_idx_t>*> & cell_neighbours,
                                 mt_vector<vec3r> & diffused_normal)
{
  size_t num_selected_elems = selected_elems.size();
  diffused_normal.resize(mesh.e2n_cnt.size());

  #ifdef OPENMP
  #pragma omp parallel
  #endif
  {
    mt_vector<mt_mixed_tuple<mt_real, mt_idx_t> > angles;

    #ifdef OPENMP
    #pragma omp for schedule(guided)
    #endif
    for (size_t i = 0; i < num_selected_elems; i++) {
      mt_idx_t cell_idx = selected_elems[i];

      angles.resize(0);
      angles.reserve(cell_neighbours[cell_idx]->size());

      const mt_idx_t *tri_con = mesh.e2n_con.data() + mesh.e2n_dsp[cell_idx];
      vec3r n1 = triangle_normal(tri_con[0], tri_con[1], tri_con[2], mesh.xyz);

      for (mt_idx_t & cell_neighbour : (*cell_neighbours[cell_idx])) {
        tri_con = mesh.e2n_con.data() + mesh.e2n_dsp[cell_neighbour];
        vec3r n0 = triangle_normal(tri_con[0], tri_con[1], tri_con[2], mesh.xyz);

        mt_real dotproduct = n0.scaProd(n1);
        mt_real cosval = clamp(dotproduct, -1.0, 1.0, true);
        // mt_real alpha = acos(cosval) * MT_PI / 180.0;
        mt_real alpha = acos(cosval);
        angles.push_back({alpha, cell_neighbour});
      }
      std::sort(angles.begin(), angles.end());

      mt_idx_t median_idx = angles[angles.size() / 2].v2;
      tri_con = mesh.e2n_con.data() + mesh.e2n_dsp[median_idx];
      diffused_normal[cell_idx] = triangle_normal(tri_con[0], tri_con[1], tri_con[2], mesh.xyz);
    }
  }
}

void update_normals_median_curvature(const mt_meshdata & mesh,
                                     const mt_vector<mt_idx_t> & selected_elems,
                                     const mt_vector<mt_vector<mt_idx_t>*> & cell_neighbours,
                                     mt_vector<vec3r> & diffused_normal)
{
  size_t num_selected_elems = selected_elems.size();
  diffused_normal.resize(mesh.e2n_cnt.size());

  #ifdef OPENMP
  #pragma omp parallel
  #endif
  {
    mt_vector<mt_mixed_tuple<mt_real, mt_idx_t> > angles;

    #ifdef OPENMP
    #pragma omp for schedule(guided)
    #endif
    for (size_t i = 0; i < num_selected_elems; i++) {
      mt_idx_t cell_idx = selected_elems[i];

      angles.resize(0);
      angles.reserve(cell_neighbours[cell_idx]->size());

      const mt_idx_t *tri_con = mesh.e2n_con.data() + mesh.e2n_dsp[cell_idx];
      vec3r n1 = triangle_normal(tri_con[0], tri_con[1], tri_con[2], mesh.xyz);
      vec3r c1 = barycenter(3, tri_con, mesh.xyz.data());

      for (mt_idx_t & cell_neighbour : (*cell_neighbours[cell_idx])) {
        tri_con = mesh.e2n_con.data() + mesh.e2n_dsp[cell_neighbour];
        vec3r n0 = triangle_normal(tri_con[0], tri_con[1], tri_con[2], mesh.xyz);
        vec3r c0 = barycenter(3, tri_con, mesh.xyz.data());

        mt_real dotproduct = n0.scaProd(n1);
        mt_real cosval = clamp(dotproduct, -1.0, 1.0, true);
        mt_real alpha = acos(cosval) / (c1 - c0).length();
        angles.push_back({alpha, cell_neighbour});
      }
      std::sort(angles.begin(), angles.end());

      mt_idx_t median_idx = angles[angles.size() / 2].v2;
      tri_con = mesh.e2n_con.data() + mesh.e2n_dsp[median_idx];
      diffused_normal[cell_idx] = triangle_normal(tri_con[0], tri_con[1], tri_con[2], mesh.xyz);
    }
  }
}

void update_normals_mean(const mt_meshdata & mesh,
                         const mt_vector<mt_idx_t> & selected_elems,
                         const mt_vector<mt_vector<mt_idx_t>*> & cell_neighbours,
                         mt_vector<vec3r> & diffused_normal)
{
  size_t num_selected_elems = selected_elems.size();
  diffused_normal.resize(mesh.e2n_cnt.size());

  #ifdef OPENMP
  #pragma omp parallel for schedule(guided)
  #endif
  for (size_t i = 0; i < num_selected_elems; i++) {
    mt_idx_t  cell_idx = selected_elems[i];
    vec3r   final_n;
    mt_real total_area = 0.0;

    for (mt_idx_t & cell_neighbour : (*cell_neighbours[cell_idx])) {
      const mt_idx_t* tri_con = mesh.e2n_con.data() + mesh.e2n_dsp[cell_neighbour];
      vec3r n      = triangle_normal(tri_con[0], tri_con[1], tri_con[2], mesh.xyz);
      mt_real area = volume(mesh.etype[cell_neighbour], tri_con, mesh.xyz.data());

      total_area += area;
      final_n    += n * area;
    }

    diffused_normal[cell_idx] = final_n / total_area;
  }
}


void normals_filter(mt_meshdata & mesh,
                    const mt_vector<mt_idx_t> & selected_nodes,
                    const mt_vector<mt_idx_t> & selected_elems,
                    const mt_real sfac,
                    const size_t iters,
                    const int type)
{
  mt_vector<vec3r> diffused_normal;

  // we compute the cell neighbours beforehand ..
  mt_vector<mt_vector<mt_idx_t>*> cell_neighbours;
  generate_cell_neighbours(mesh, selected_elems, cell_neighbours);

  void (*upd_ptr) (const mt_meshdata &, const mt_vector<mt_idx_t> &,
                   const mt_vector<mt_vector<mt_idx_t>*> &, mt_vector<vec3r> &);

  switch(type) {
    default:
    case 0: upd_ptr = update_normals_median_angle; break;
    case 1: upd_ptr = update_normals_mean; break;
    case 2: upd_ptr = update_normals_median_curvature; break;
  }

  PROGRESS<size_t> prg(iters, "normals smoothing progress:");

  for (size_t iteration = 0; iteration < iters; iteration++)
  {
    prg.next();
    upd_ptr(mesh, selected_elems, cell_neighbours, diffused_normal);
    project_vertices(mesh, selected_nodes, diffused_normal, sfac);
  }
  prg.finish();

  for(mt_vector<mt_idx_t>* t : cell_neighbours)
    if(t) delete t;
}

void smooth_tags_surf(mt_meshdata & mesh, MT_USET<mt_tag_t> & tags, int iter, float smth)
{
  auto get_tag_itf = [&](mt_tag_t tag, MT_USET<mt_idx_t> & itf)
  {
    MT_USET<mt_tag_t> curtags;
    for(size_t i=0; i<mesh.n2e_cnt.size(); i++) {
      curtags.clear();
      mt_idx_t start = mesh.n2e_dsp[i], stop = start + mesh.n2e_cnt[i];
      for(mt_idx_t j=start; j<stop; j++)
        curtags.insert(mesh.etags[mesh.n2e_con[j]]);

      if(curtags.size() > 1 && curtags.count(tag)) {
        itf.insert(i);
      }
    }
  };

  auto elem_in_itf = [&mesh](mt_idx_t e, MT_USET<mt_idx_t> & itf)
  {
    mt_idx_t* con  = mesh.e2n_con.data() + mesh.e2n_dsp[e];
    mt_cnt_t  nnod = mesh.e2n_cnt[e];
    mt_cnt_t  nin  = 0;

    for(mt_cnt_t i=0; i<nnod; i++)
      if(itf.count(con[i])) nin++;

    return nin == nnod; // we return true if all element nodes are on itf
  };

  auto get_alternative_tag = [&mesh](mt_idx_t e)
  {
    mt_tag_t curtag = mesh.etags[e];

    for(mt_idx_t i=mesh.e2n_dsp[e]; i<mesh.e2n_dsp[e]+mesh.e2n_cnt[e]; i++) {
      mt_idx_t n = mesh.e2n_con[i];
      mt_idx_t start = mesh.n2e_dsp[n], stop = start + mesh.n2e_cnt[n];
      for(mt_idx_t j=start; j<stop; j++) {
        mt_idx_t eidx = mesh.n2e_con[j];
        if(mesh.etags[eidx] != curtag)
          return mesh.etags[eidx];
      }
    }

    printf("%s: no different tag found ?!\n", __func__);
    return curtag;
  };

  MT_USET<mt_idx_t> nod, ele;

  for(mt_tag_t t : tags) {
    // first step: we switch tags for those elements that would get squashed by smoothing the
    // interface
    nod.clear(), ele.clear();
    get_tag_itf(t, nod);
    nodeSet_to_elemSet(mesh, nod, ele);

    for(mt_idx_t e : ele) {
      if(mesh.etags[e] == t) {
        if(elem_in_itf(e, nod)) {
          mt_tag_t nt = get_alternative_tag(e);
          mesh.etags[e] = nt;
        }
      }
    }

    // second step: build a line mesh from the interface
    nod.clear(), ele.clear();
    get_tag_itf(t, nod);
    nodeSet_to_elemSet(mesh, nod, ele);

    mt_meshdata linemesh;
    mt_vector<mt_tuple<mt_idx_t>> edgebuff;

    for(mt_idx_t e : ele) {
      if(mesh.etags[e] == t) {
        edgebuff.reserve_and_clear(3);
        get_edges(mesh, e, edgebuff);

        for(mt_tuple<mt_idx_t> & tt : edgebuff) {
          if(nod.count(tt.v1) && nod.count(tt.v2)) {
            linemesh.e2n_con.push_back(tt.v1);
            linemesh.e2n_con.push_back(tt.v2);
          }
        }
      }
    }

    linemesh.e2n_cnt.assign(linemesh.e2n_con.size() / 2, 2);
    linemesh.etype.assign(linemesh.e2n_cnt.size(), Line);
    linemesh.etags.assign(linemesh.e2n_cnt.size(), 0);
    compute_full_mesh_connectivity(linemesh, false);

    // third step: smooth surrounding with linemesh as a manifold
    mt_mask mnfld_mask(mesh.xyz.size() / 3);
    for(mt_idx_t c : linemesh.e2n_con) mnfld_mask.insert(c);

    elemSet_to_nodeSet(mesh, ele, nod);
    nodeSet_to_elemSet(mesh, nod, ele);
    elemSet_to_nodeSet(mesh, ele, nod);

    mt_vector<mt_idx_t> smth_nod;
    smth_nod.assign(nod.begin(), nod.end());

    linemesh.xyz.assign(mesh.xyz.size(), mesh.xyz.data(), false);
    smooth_nodes(mesh, linemesh, mnfld_mask, smth_nod, iter, smth, false);
    linemesh.xyz.assign(0, NULL, false);
  }
}

void smooth_tags_tetvol(mt_meshdata & mesh, MT_USET<mt_tag_t> & surf_tags, MT_USET<mt_tag_t> & tags,
                        int iter, float smth)
{
  auto get_tag_itf = [&mesh](mt_tag_t tag, MT_USET<mt_idx_t> & itf)
  {
    MT_USET<mt_tag_t> curtags;
    for(size_t i=0; i<mesh.n2e_cnt.size(); i++) {
      curtags.clear();
      mt_idx_t start = mesh.n2e_dsp[i], stop = start + mesh.n2e_cnt[i];
      for(mt_idx_t j=start; j<stop; j++)
        curtags.insert(mesh.etags[mesh.n2e_con[j]]);

      if(curtags.size() > 1 && curtags.count(tag))
        itf.insert(i);
    }
  };

  auto elem_in_itf = [&mesh](mt_idx_t e, MT_USET<mt_idx_t> & itf)
  {
    mt_idx_t* con  = mesh.e2n_con.data() + mesh.e2n_dsp[e];
    mt_cnt_t  nnod = mesh.e2n_cnt[e];
    mt_cnt_t  nin  = 0;

    for(mt_cnt_t i=0; i<nnod; i++)
      if(itf.count(con[i])) nin++;

    return nin == nnod; // we return true if all element nodes are on itf
  };

  auto get_alternative_tag = [&mesh](mt_idx_t e)
  {
    mt_tag_t curtag = mesh.etags[e];

    for(mt_idx_t i=mesh.e2n_dsp[e]; i<mesh.e2n_dsp[e]+mesh.e2n_cnt[e]; i++) {
      mt_idx_t n = mesh.e2n_con[i];
      mt_idx_t start = mesh.n2e_dsp[n], stop = start + mesh.n2e_cnt[n];
      for(mt_idx_t j=start; j<stop; j++) {
        mt_idx_t eidx = mesh.n2e_con[j];
        if(mesh.etags[eidx] != curtag)
          return mesh.etags[eidx];
      }
    }

    printf("%s: no different tag found ?!\n", __func__);
    return curtag;
  };

  MT_USET<mt_idx_t> nod, ele;
  mt_meshdata base_surf;
  compute_surface(mesh, surf_tags, base_surf, true);

  for(mt_tag_t t : tags) {
    // first step: we switch tags for those elements that would get squashed by smoothing the
    // interface
    nod.clear(), ele.clear();
    get_tag_itf(t, nod);
    nodeSet_to_elemSet(mesh, nod, ele);

    for(mt_idx_t e : ele) {
      if(mesh.etags[e] == t) {
        if(elem_in_itf(e, nod)) {
          mt_tag_t nt = get_alternative_tag(e);
          mesh.etags[e] = nt;
        }
      }
    }

    // second step: build a triangle mesh from the interface and the geometric surface
    nod.clear(), ele.clear();
    get_tag_itf(t, nod);
    nodeSet_to_elemSet(mesh, nod, ele);

    mt_meshdata trimesh;
    trimesh.e2n_con.assign(base_surf.e2n_con.begin(), base_surf.e2n_con.end());

    mt_vector<mt_idx_t> facebuff;

    for(mt_idx_t e : ele) {
      if(mesh.etags[e] == t) {
        facebuff.reserve_and_clear(12);
        get_faces(mesh, e, facebuff);

        for(size_t i=0; i<4; i++) {
          mt_idx_t* con = facebuff.data() + i*3;
          if(nod.count(con[0]) && nod.count(con[1]) && nod.count(con[2]))
            trimesh.e2n_con.push_back(con, con+3);
        }
      }
    }

    trimesh.e2n_cnt.assign(trimesh.e2n_con.size() / 3, 3);
    trimesh.etype  .assign(trimesh.e2n_cnt.size(), Tri);
    trimesh.etags  .assign(trimesh.e2n_cnt.size(), 0);
    compute_full_mesh_connectivity(trimesh, false);

    mt_meshdata linemesh;
    compute_line_interfaces(mesh, trimesh, 0.0, false, linemesh);
    compute_full_mesh_connectivity(linemesh, false);

    // third step: smooth surrounding with linemesh as a manifold
    mt_mask surf_mask(mesh.xyz.size() / 3);
    mt_mask line_mask(mesh.xyz.size() / 3);
    for(mt_idx_t c : trimesh.e2n_con) surf_mask.insert(c);
    for(mt_idx_t c : linemesh.e2n_con) line_mask.insert(c);

    // prepare smooth nod: we want to go over volume, but not over lines
    mt_vector<mt_idx_t> smth_nod;

    elemSet_to_nodeSet(mesh, ele, nod);
    short nlvl = 4;
    for(short i=0; i<nlvl; i++) {
      nodeSet_to_elemSet(mesh, nod, ele);
      elemSet_to_nodeSet(mesh, ele, nod);
    }

    for(mt_idx_t c : linemesh.e2n_con) nod.erase(c);
    smth_nod.assign(nod.begin(), nod.end());

    quality_aware_smoother qsmth;

    // volumetric smooth
    trimesh.xyz.assign(mesh.xyz.size(), mesh.xyz.data(), false);
    linemesh.xyz.assign(mesh.xyz.size(), mesh.xyz.data(), false);
    qsmth(mesh, mesh, trimesh, surf_mask, smth_nod, size_t(iter), mt_real(smth), mt_real(0.9));

    // prepare smooth nod: we only want to go over surface vtx
    smth_nod.clear();
    elemSet_to_nodeSet(mesh, ele, nod);
    for(mt_idx_t n : nod)
      if(surf_mask.count(n)) smth_nod.push_back(n);

    // smooth_nodes(trimesh, linemesh, line_mask, smth_nod, iter, smth, false);
    qsmth(mesh, trimesh, linemesh, line_mask, smth_nod, size_t(iter), mt_real(smth), mt_real(0.9));

    trimesh.xyz.assign(0, NULL, false);
    linemesh.xyz.assign(0, NULL, false);
  }
}


