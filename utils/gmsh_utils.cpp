#include "mt_utils_base.h"
#include "io_utils.h"
#include "gmsh_utils.h"
#include "mesh_utils.h"


void parse_msh_header(FILE* in, int & vers, int & type, int & data_size, bool & need_endian_swap, int & error_flag)
{
  const int linelength = 2048;
  char current_line[linelength];
  error_flag = 0;

  char* ret = fgets(current_line, linelength, in);

  if(ret == NULL || strcmp(current_line, "$MeshFormat\n") != 0) {
    fprintf(stderr, "%s error: File doesn't start with \"$MeshFormat\"! Wrong file format? Aborting!\n", __func__);
    error_flag = 1;
    return;
  }

  ret = fgets(current_line, linelength, in);

  float fvers;
  int endian;

  sscanf(current_line, "%f %d %d", &fvers, &type, &data_size);
  vers = int(fvers*10.0f);

  if(type == 1) {
    need_endian_swap = false;
    fread(&endian, sizeof(int), 1, in);
    if(endian != 1) {
      need_endian_swap = true;
      endian = byte_swap(endian);
      if(endian != 1) {
        fprintf(stderr, "%s error: Endian check failed! Aborting!\n", __func__);
        error_flag = 2;
        return;
      }
    }
  }

  ret = fgets(current_line, linelength, in);
  if(ret == NULL || strcmp(current_line, "$EndMeshFormat\n") != 0) {
    fprintf(stderr, "%s error: File header doesn't end with \"$MeshFormatEnd\"! Wrong file format? Aborting!\n", __func__);
    error_flag = 3;
    return;
  }
}

void read_msh_file(mt_meshdata & mesh, std::string filename)
{
  FILE* in = fopen(filename.c_str(), MT_FOPEN_READ);
  if(!in) {
    treat_file_open_error(filename);
    return;
  }

  int vers, type, data_size, error;
  bool endian_swap;
  parse_msh_header(in, vers, type, data_size, endian_swap, error);

  if(!error) {
    if(vers == 22 && type == 1)
      read_msh2_binary(mesh, in, endian_swap);
    else {
      fprintf(stderr, "%s error: Only binary MSH vers. 2.2 supported. Aborting!\n", __func__);
    }
  }

  fclose(in);
}

elem_t get_msh_elem_type(int t)
{
  switch(t) {
    case 1: return Line;
    case 2: return Tri;
    case 3: return Quad;
    case 4: return Tetra;
    case 5: return Hexa;
    case 6: return Prism;
    case 7: return Pyramid;
    case 15: return Node;
  }

  return Tetra;
}

int get_msh_elem_size(int t)
{
  switch(t) {
    case 1: return 2;
    case 2: return 3;
    case 3: return 4;
    case 4: return 4;
    case 5: return 8;
    case 6: return 6;
    case 7: return 5;
    case 15: return 1;
  }

  return 0;
}

void read_msh2_binary(mt_meshdata & mesh, FILE* in, bool endian_swap)
{
  const int linelength = 2048;
  char current_line[linelength];
  char* ret = fgets(current_line, linelength, in);

  if(ret == NULL || strcmp(current_line, "$Nodes\n") != 0) {
    fprintf(stderr, "%s format error: Expected \"$Nodes\". Aborting!\n", __func__);
    return;
  }

  int number_of_nodes = -1;
  ret = fgets(current_line, linelength, in);
  sscanf(current_line, "%d", &number_of_nodes);

  mesh.xyz.resize(number_of_nodes*3);

  int num_i;
  double xyz[3];
  size_t nread = 0;

  for(int i = 0; i < number_of_nodes; i++){
    nread += fread(&num_i, sizeof(int), 1, in);
    nread += fread(xyz, sizeof(double), 3, in);

    if(endian_swap) {
      num_i  = byte_swap(num_i);
      xyz[0] = byte_swap(xyz[0]);
      xyz[1] = byte_swap(xyz[1]);
      xyz[2] = byte_swap(xyz[2]);
    }

    num_i -= 1; // indexing in gmsh is 1 based
    mesh.xyz[num_i*3+0] = xyz[0];
    mesh.xyz[num_i*3+1] = xyz[1];
    mesh.xyz[num_i*3+2] = xyz[2];
  }

  if(nread != size_t(number_of_nodes * 4)) {
    fprintf(stderr, "%s error: Wrong number of nodes read. Aborting!\n", __func__);
    return;
  }

  ret = fgets(current_line, linelength, in);
  if(ret == NULL || strcmp(current_line, "$EndNodes\n") != 0) {
    fprintf(stderr, "%s format error: Expected \"$EndNodes\". Aborting!\n", __func__);
    return;
  }

  ret = fgets(current_line, linelength, in);
  if(ret == NULL || strcmp(current_line, "$Elements\n") != 0) {
    fprintf(stderr, "%s format error: Expected \"$Elements\". Aborting!\n", __func__);
    return;
  }

  int number_of_elems = -1;
  ret = fgets(current_line, linelength, in);
  sscanf(current_line, "%d", &number_of_elems);

  mesh.e2n_cnt.resize(number_of_elems);
  mesh.e2n_con.assign(size_t(number_of_elems*8), mt_idx_t(-1));
  mesh.etype.resize(number_of_elems);
  mesh.etags.assign(size_t(number_of_elems), mt_tag_t(0));
  mesh.lon.assign(size_t(number_of_elems*3), mt_real(0));

  nread = 0;
  int elem_header[3];
  mt_vector<int> intbuff;

  for(int i = 0; i < number_of_elems; i++){
    nread += fread(elem_header, sizeof(int), 3, in);

    int & elm_type = elem_header[0];
    int & elm_num  = elem_header[1];
    int & num_tags = elem_header[2];

    if(endian_swap) {
      elm_type = byte_swap(elm_type);
      elm_num  = byte_swap(elm_num);
      num_tags = byte_swap(num_tags);
    }

    int elm_size = get_msh_elem_size(elm_type);
    elem_t type  = get_msh_elem_type(elm_type);

    intbuff.resize(1 + elm_size + num_tags);

    for(int j=0; j<elm_num; j++, i++) {
      nread += fread(intbuff.data(), sizeof(int), intbuff.size(), in);

      if(endian_swap) {
        for(size_t k=0; k<intbuff.size(); k++)
          intbuff[k] = byte_swap(intbuff[k]);
      }

      int & eidx = intbuff[0];
      eidx -= 1;  // convert to zero-based indexing
      mesh.etype[eidx] = type;

      if(num_tags)
        mesh.etags[eidx] = intbuff[1];

      mesh.e2n_cnt[eidx] = elm_size;
      for(int k=0; k<elm_size; k++)
        mesh.e2n_con[eidx*8+k] = intbuff[1+num_tags+k] - 1;
    }
  }

  reduce_by_flag(mesh.e2n_con, mt_idx_t(-1));

  ret = fgets(current_line, linelength, in);
  if(ret == NULL || strcmp(current_line, "$EndElements\n") != 0) {
    fprintf(stderr, "%s format error: Expected \"$EndElements\". Aborting!\n", __func__);
    return;
  }
}
