#ifndef STRING_UTILS_H
#define STRING_UTILS_H

#include <string>

/**
* @brief Parse a meshtool-style parameter into a result string.
*
* @param str     The string.
* @param param   The parameter we want to match.
* @param res     The result string containing the value of the parameter.
* @param check   The optional check to perform on the parameter.
*
* @return        True if the parameter contained the flag.
*/
bool parse_param (const std::string & str, const std::string & param,
                  std::string & res, const mt_check_type check = dont_check);

/**
* @brief Parse a meshtool-style parameter into a result string with a default value.
*
* @param str     The string.
* @param param   The parameter we want to match.
* @param def     The default string.
* @param res     The result string containing the value of the parameter.
* @param check   The optional check to perform on the parameter.
*
* @return        True if the parameter contained the flag.
*/
bool parse_param_def (const std::string & str, const std::string & param, const std::string & def,
                      std::string & res, const mt_check_type check = dont_check);

/**
 * @brief reformat param specifier to standard form a=b
 *
 * @param argv command line arguments
 * @param i    current argument
 * @param argc number of argument
 *
 * @return parameter in standardized form
 *
 * @note if the "=" is missing, the next argument is appended and i is incremented
 */
std::string reformat_param(char **argv, int &i, int argc);

/**
 * @brief Parse a string as a flag
 * 
 * @param str      The parameter string .
 * @param flag     The flag we check for.
 * @param found    Whether we found the flag.
 * @return    Whether we found the flag.
 */
bool parse_flag(const std::string & str, const std::string & flag, bool & found);

/// globing
void mt_glob(const std::string& pattern, mt_vector<std::string> & ret);

/**
* @brief Check if a string ends with a certain substring.
*
* @param value    String we check.
* @param ending   Substring we check with.
*
* @return True if there is a match.
*/
bool endswith(const std::string value, const std::string ending);


/**
* @brief Check if a string beginss with a certain substring.
*
* @param value    String we check.
* @param ending   Substring we check with.
*
* @return True if there is a match.
*/
bool beginswith(const std::string value, const std::string beginning);

/**
 * @brief Remove leading and tailing whitespace
 *
 * @param[in,out]  str  the string to crop
 */
void crop_string(std::string& str);

/**
 * @brief Remove whitespaces
 *
 * @param[in,out]  str  the string compact
 */
void compact_string(std::string& str);

/**
* @brief Wrapper for glibc basename.
*
* @param path  Input path.
*
* @return      Output basename.
*/
std::string mt_basename(const std::string path);
char*       mt_basename(const char* path, mt_vector<char> & strbuff);
std::string mt_dirname(const std::string path);

/// get the absolute path, or NULL if file does not exist
std::string mt_fullpath(const char* path);

/**
* @brief Add a '.' to the basename of mesh if it is not present.
*
* @param base The mesh basename.
*/
void fixBasename(std::string & base);

/**
* @brief Split a string in a list of substrings.
*
* Note that the delimiter is not stored.
*
* @param input  The input string.
* @param s      The substring delimiter.
* @param list   The list of substrings.
* @param crop   If true, substrings will be cropped
*/
void split_string(const std::string & input, char s, mt_vector<std::string> & list, const bool allow_zero=false);

/**
* @brief Check if filename contains the given extension
*
* @param fname The filename
* @param ext   The extension
* @param pos   Position where the extension was found
*
* @return Whether the extension was found
*/
bool find_extension(std::string fname, std::string ext, size_t & pos);
bool remove_extension(std::string & fname);

/// swap a char to another one
int swap_char(std::string & str, const char from, const char to);

/**
* @brief Check wheter a string can be interpreted as an integer.
*
* @param s  The string.
*
* @return   Whether the string can be interpreted as an integer.
*/
bool is_integer(const std::string & s);

/**
* @brief Check wheter a string can be interpreted as an float.
*
* @param s  The string.
*
* @return   Whether the string can be interpreted as an float.
*/
bool is_float(const std::string & s);

/**
* @brief In-place character removal.
*/
void remove_in_place(std::string & str, const char c);
/**
* @brief In-place indentation removal.
*/
void remove_indent(std::string & str, const char c = ' ');
void remove_indent(char* str, const char c = ' ');

/// replace multiple space with just one
void replace_multi_with_single(char *str, char c);
void replace_multi_with_single(std::string str, char c);

#endif
