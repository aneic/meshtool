/**
* @file itk_utils.h
* @brief A collection of ITK utility functions.
* @authors Matthias Gsell, Aurel Neic
* @version
* @date 2017-02-16
*/


#ifndef _ITK_UTILS_H
#define _ITK_UTILS_H

#include <cstring>
#include <cstdio>

#include <limits.h>

#include "mt_utils.h"

#define ITKSMOOTH_CHECK_RAD 1
#define ITK_MAX_PIXEL_COMP 4

enum ITK_AXES
{
  ITK_AXIS_0 = 0,
  ITK_AXIS_X = 1,
  ITK_AXIS_Y = 2,
  ITK_AXIS_Z = 4
};

typedef enum DICOM_ORIENT
{
  DCM_AXES_UNKOWN = 0,
  LPS = 1,
  RAS = 2,
  LAS = 3
} dcm_orient;

/**
* @brief Convert the ITK type string to a type ID
*
* @param typestr The type string.
*
* @return The type ID.
*/
inline int itk_get_datatype_id_vtk(const char * typestr)
{
  int id = -1;
  if (strncmp(typestr, "bit", 3) == 0)                  id = 0;
  else if (strncmp(typestr, "unsigned_char", 13) == 0)  id = 1;
  else if (strncmp(typestr, "char", 4) == 0)            id = 2;
  else if (strncmp(typestr, "unsigned_short", 14) == 0) id = 3;
  else if (strncmp(typestr, "short", 5) == 0)           id = 4;
  else if (strncmp(typestr, "unsigned_int", 12) == 0)   id = 5;
  else if (strncmp(typestr, "int", 3) == 0)             id = 6;
  else if (strncmp(typestr, "unsigned_long", 13) == 0)  id = 7;
  else if (strncmp(typestr, "long", 4) == 0)            id = 8;
  else if (strncmp(typestr, "float", 5) == 0)           id = 9;
  else if (strncmp(typestr, "double", 6) == 0)          id = 10;

  return id;
}

/**
* @brief Convert the type ID into an ITK type string
*
*/
inline const char * itk_get_datatype_str_vtk(const int type)
{
  switch (type)
  {
    case 0: return "bit";
    case 1: return "unsigned_char";
    case 2: return "char";
    case 3: return "unsigned_short";
    case 4: return "short";
    case 5: return "unsigned_int";
    case 6: return "int";
    case 7: return "unsigned_long";
    case 8: return "long";
    case 9: return "float";
    case 10: return "double";
    default: return NULL;
  }
}

inline int itk_get_datatype_id_nrrd(const char * typestr)
{
   int id = -1;
   if (strncmp(typestr, "uchar", 5) == 0) id = 1;
   else if (strncmp(typestr, "unsigned char", 13) == 0) id = 1;
   else if (strncmp(typestr, "uint8", 5) == 0) id = 1;
   else if (strncmp(typestr, "uint8_t", 7) == 0) id = 1;
   else if (strncmp(typestr, "signed char", 11) == 0) id = 2;
   else if (strncmp(typestr, "char", 4) == 0) id = 2;
   else if (strncmp(typestr, "int8", 4) == 0) id = 2;
   else if (strncmp(typestr, "int8_t", 6) == 0) id = 2;
   else if (strncmp(typestr, "ushort", 6) == 0) id = 3;
   else if (strncmp(typestr, "unsigned short", 14) == 0) id = 3;
   else if (strncmp(typestr, "unsigned short int", 18) == 0) id = 3;
   else if (strncmp(typestr, "uint16", 6) == 0) id = 3;
   else if (strncmp(typestr, "uint16_t", 8) == 0) id = 3;
   else if (strncmp(typestr, "short", 5) == 0) id = 4;
   else if (strncmp(typestr, "short int", 9) == 0) id = 4;
   else if (strncmp(typestr, "signed short", 12) == 0) id = 4;
   else if (strncmp(typestr, "signed short int", 16) == 0) id = 4;
   else if (strncmp(typestr, "int16", 5) == 0) id = 4;
   else if (strncmp(typestr, "int16_t", 7) == 0) id = 4;
   else if (strncmp(typestr, "uint", 4) == 0) id = 5;
   else if (strncmp(typestr, "unsigned int", 12) == 0) id = 5;
   else if (strncmp(typestr, "uint32", 6) == 0) id = 5;
   else if (strncmp(typestr, "uint32_t", 8) == 0) id = 5;
   else if (strncmp(typestr, "int", 3) == 0) id = 6;
   else if (strncmp(typestr, "signed int", 10) == 0) id = 6;
   else if (strncmp(typestr, "int32", 5) == 0) id = 6;
   else if (strncmp(typestr, "int32_t", 7) == 0) id = 6;
   else if (strncmp(typestr, "ulonglong", 9) == 0) id = 7;
   else if (strncmp(typestr, "unsigned long long", 18) == 0) id = 7;
   else if (strncmp(typestr, "unsigned long long int", 22) == 0) id = 7;
   else if (strncmp(typestr, "uint64", 6) == 0) id = 7;
   else if (strncmp(typestr, "uint64_t", 8) == 0) id = 7;
   else if (strncmp(typestr, "longlong", 8) == 0) id = 8;
   else if (strncmp(typestr, "long long", 9) == 0) id = 8;
   else if (strncmp(typestr, "long long int", 13) == 0) id = 8;
   else if (strncmp(typestr, "signed long long", 16) == 0) id = 8;
   else if (strncmp(typestr, "signed long long int", 20) == 0) id = 8;
   else if (strncmp(typestr, "int64", 5) == 0) id = 8;
   else if (strncmp(typestr, "int64_t", 7) == 0) id = 8;
   else if (strncmp(typestr, "float", 5) == 0) id = 9;
   else if (strncmp(typestr, "double", 6) == 0) id = 10;
   return id;
}

inline const char * itk_get_datatype_str_nrrd(const int type)
{
   switch (type)
   {
      case 0: return "bit";
      case 1: return "unsigned char";
      case 2: return "signed char";
      case 3: return "unsigned short";
      case 4: return "short";
      case 5: return "unsigned int";
      case 6: return "int";
      case 7: return "unsigned long";
      case 8: return "long";
      case 9: return "float";
      case 10: return "double";
      default: return NULL;
   }
}
inline dcm_orient itk_get_dicom_orientation_nrrd(const char * typestr)
{
   dcm_orient orient = DCM_AXES_UNKOWN;

   if (strncmp(typestr, "right-anterior-superior", 23) == 0) orient = RAS;
   else if (strncmp(typestr, "RAS", 3) == 0) orient = RAS;
   else if (strncmp(typestr, "left-anterior-superior", 22) == 0) orient = LAS;
   else if (strncmp(typestr, "LAS", 3) == 0) orient = LAS;
   else if (strncmp(typestr, "left-posterior-superior", 23) == 0) orient = LPS;
   else if (strncmp(typestr, "LPS", 3) == 0) orient = LPS;

   return orient;
}

inline int itk_get_spacedimension_nrrd(const char * typestr)
{
   int dim = -1;
   if (strncmp(typestr, "right-anterior-superior", 23) == 0) dim = 3;
   else if (strncmp(typestr, "RAS", 3) == 0) dim = 3;
   else if (strncmp(typestr, "left-anterior-superior", 22) == 0) dim = 3;
   else if (strncmp(typestr, "LAS", 3) == 0) dim = 3;
   else if (strncmp(typestr, "left-posterior-superior", 23) == 0) dim = 3;
   else if (strncmp(typestr, "LPS", 3) == 0) dim = 3;
   else if (strncmp(typestr, "right-anterior-superior-time", 28) == 0) dim = 4;
   else if (strncmp(typestr, "RAST", 3) == 0) dim = 4;
   else if (strncmp(typestr, "left-anterior-superior-time", 27) == 0) dim = 4;
   else if (strncmp(typestr, "LAST", 3) == 0) dim = 4;
   else if (strncmp(typestr, "left-posterior-superior-time", 28) == 0) dim = 4;
   else if (strncmp(typestr, "LPST", 3) == 0) dim = 4;
   else if (strncmp(typestr, "scanner-xyz", 11) == 0) dim = 3;
   else if (strncmp(typestr, "scanner-xyz-time", 16) == 0) dim = 4;
   else if (strncmp(typestr, "3D-right-handed", 15) == 0) dim = 3;
   else if (strncmp(typestr, "3D-left-handed", 14) == 0) dim = 3;
   else if (strncmp(typestr, "3D-right-handed-time", 20) == 0) dim = 4;
   else if (strncmp(typestr, "3D-left-handed-time", 19) == 0) dim = 4;
   return dim;
}

inline int itk_get_filetype_id_nrrd(const char * typestr)
{
   int id = -1;
   if (strncmp(typestr, "raw", 3) == 0) id = 0;
   else if (strncmp(typestr, "txt", 3) == 0) id = 1;
   else if (strncmp(typestr, "text", 4) == 0) id = 1;
   else if (strncmp(typestr, "ascii", 5) == 0) id = 1;
   else if (strncmp(typestr, "hex", 3) == 0) id = 2;
   else if (strncmp(typestr, "gz", 2) == 0) id = 3;
   else if (strncmp(typestr, "gzip", 4) == 0) id = 3;
   else if (strncmp(typestr, "bz2", 3) == 0) id = 4;
   else if (strncmp(typestr, "bzip2", 5) == 0) id = 5;
   return id;
}

/// Get the size in bytes from an type ID.
inline unsigned int itk_get_datatype_size(const int type)
{
  switch (type)
  {
    case 0: return UINT_MAX;
    case 1: return sizeof(unsigned char);
    case 2: return sizeof(char);
    case 3: return sizeof(unsigned short);
    case 4: return sizeof(short);
    case 5: return sizeof(unsigned int);
    case 6: return sizeof(int);
    case 7: return sizeof(unsigned long);
    case 8: return sizeof(long);
    case 9: return sizeof(float);
    case 10: return sizeof(double);
    default: return UINT_MAX;
  }
}

/**
* @brief Read one ascii pixel.
*
* @param fp          The file descriptor to read from.
* @param ncomp       The number of components of one pixel.
* @param comptypeid  The type ID of one pixel component.
* @param data        The data pointer where the pixel is read.
*
* @return the number of read components.
*/
inline unsigned int itk_read_pixel_ascii(FILE * fp, const unsigned int ncomp, const int comptypeid, char * data)
{
  const unsigned int compsz = itk_get_datatype_size(comptypeid);
  if (compsz == UINT_MAX) return 0;
  unsigned int i;
  bool readerror = false;
  char compdata[8];
  const char * fmtstr = NULL;
  if ((comptypeid == 1) || (comptypeid == 3) || (comptypeid == 5) || (comptypeid == 7)) fmtstr = "%lu";
  else if ((comptypeid == 2) || (comptypeid == 4) || (comptypeid == 6) || (comptypeid == 8)) fmtstr = "%ld";
  else if ((comptypeid == 9) || (comptypeid == 10)) fmtstr = "%lf";
  for (i = 0; (i < ncomp) && (!readerror); i++)
  {
    if (fscanf(fp, fmtstr, compdata) == 1)
    {
      memcpy(&data[i*compsz], &compdata, compsz*sizeof(char));
      //TODO perform byte swap !!!
    }
    else readerror = true;
  }
  return i;
}

template <typename T>
inline bool itk_comp_as(const char* compdata, const int comptypeid, T& val)
{
  bool success = true;
  switch (comptypeid)
  {
    case 1:  val = static_cast<T>(*((unsigned char*)compdata)); break;
    case 2:  val = static_cast<T>(*((char*)compdata)); break;
    case 3:  val = static_cast<T>(*((unsigned short*)compdata)); break;
    case 4:  val = static_cast<T>(*((short*)compdata)); break;
    case 5:  val = static_cast<T>(*((unsigned int*)compdata)); break;
    case 6:  val = static_cast<T>(*((int*)compdata)); break;
    case 7:  val = static_cast<T>(*((unsigned long*)compdata)); break;
    case 8:  val = static_cast<T>(*((long*)compdata)); break;
    case 9:  val = static_cast<T>(*((float*)compdata)); break;
    case 10: val = static_cast<T>(*((double*)compdata)); break;
    default: return false; break;
  }
  return success;
}

inline bool itk_comp_nonzero(const char* compdata, const int comptypeid)
{
  switch (comptypeid)
  {
    case 1:  return bool(*((unsigned char*)compdata));
    case 2:  return bool(*((char*)compdata));
    case 3:  return bool(*((unsigned short*)compdata));
    case 4:  return bool(*((short*)compdata));
    case 5:  return bool(*((unsigned int*)compdata));
    case 6:  return bool(*((int*)compdata));
    case 7:  return bool(*((unsigned long*)compdata));
    case 8:  return bool(*((long*)compdata));
    case 9:  return bool(*((float*)compdata));
    case 10: return bool(*((double*)compdata));

    default: return false;
  }
}

template <typename T>
inline bool itk_comp_from(char* compdata, const int comptypeid, const T& val)
{
  bool success = true;
  switch (comptypeid)
  {
    case 1:
    {
      unsigned char comp = static_cast<unsigned char>(val);
      memcpy(compdata, &comp, sizeof(unsigned char));
    } break;
    case 2:
    {
      char comp = static_cast<char>(val);
      memcpy(compdata, &comp, sizeof(char));
    } break;
    case 3:
    {
      unsigned short comp = static_cast<unsigned short>(val);
      memcpy(compdata, &comp, sizeof(unsigned short));
    } break;
    case 4:
    {
      short comp = static_cast<short>(val);
      memcpy(compdata, &comp, sizeof(short));
    } break;
    case 5:
    {
      unsigned int comp = static_cast<unsigned int>(val);
      memcpy(compdata, &comp, sizeof(unsigned int));
    } break;
    case 6:
    {
      int comp = static_cast<int>(val);
      memcpy(compdata, &comp, sizeof(int));
    } break;
    case 7:
    {
      unsigned long comp = static_cast<unsigned long>(val);
      memcpy(compdata, &comp, sizeof(unsigned long));
    } break;
    case 8:
    {
      long comp = static_cast<long>(val);
      memcpy(compdata, &comp, sizeof(long));
    } break;
    case 9:
    {
      float comp = static_cast<float>(val);
      memcpy(compdata, &comp, sizeof(float));
    } break;
    case 10:
    {
      double comp = static_cast<double>(val);
      memcpy(compdata, &comp, sizeof(double));
    } break;
    default: success = false; break;
  }
  return success;
}

inline bool itk_comp_as_mt_idx_t(const char* compdata, const int comptypeid, mt_idx_t& val)
{
  return itk_comp_as<mt_idx_t>(compdata, comptypeid, val);
}

inline bool itk_comp_as_mt_real(const char* compdata, const int comptypeid, mt_real& val)
{
  return itk_comp_as<mt_real>(compdata, comptypeid, val);
}

inline void cstring_rstrip(char * cstr)
{
  const size_t l = strlen(cstr);
  int e(l-1); // end
  while ((e >= 0) && (isspace(cstr[e]))) cstr[e--] = 0;
}

#ifdef __WIN32__
#define LITTLE_ENDIAN 1234
#define BIG_ENDIAN    4231
#endif

inline int itk_get_endianness()
{
  int x = 1;

  int ret = *((char*) &x) == 1 ? LITTLE_ENDIAN : BIG_ENDIAN;
  return ret;
}

/**
* @brief The ITK image class
*/
class itk_image
{
  public:

  size_t npix;          ///< The number of pixels.
  unsigned int ncomp;   ///< The number of components.
  unsigned int compsz;  ///< The size in bytes of one component.
  unsigned int pixsz;   ///< The size in bytes of one pixel.
  size_t datasz;        ///< The overall size in bytes of the data.
  dcm_orient dcmo;      ///< dicom axes orientation

  mt_triple<size_t> dim;    ///< The number of pixel in each dimension.
  int comptypeid;           ///< ID encoding the data type of the data components.
  mt_triple<double> pixspc; ///< The spacing of the pixels.
  mt_triple<double> orig;   ///< The origin coordinate.
  mt_vector<char> databuff; ///< The data buffer.

  private:

  /**
  * @brief Since binary ITK data is stored in big endian, byteswap_data() does a
  * byte swap of the whole data array if the machine is little endian.
  */
  inline void byteswap_data()
  {
    #if __BYTE_ORDER == __LITTLE_ENDIAN
    if(itk_get_datatype_size(this->comptypeid) == 1)
      return;

    switch(this->compsz)
    {
      default:
      case 2:
      {
        short* dp = (short*) this->databuff.data();
        for(unsigned int i=0, idx=0; i<this->npix; i++)
          for(unsigned int j=0; j<this->ncomp; j++, idx++)
            dp[idx] = byte_swap(dp[idx]);
        break;
      }
      case 4:
      {
        int* dp = (int*) this->databuff.data();
        for(unsigned int i=0, idx=0; i<this->npix; i++)
          for(unsigned int j=0; j<this->ncomp; j++, idx++)
            dp[idx] = byte_swap(dp[idx]);
        break;
      }
      case 8:
      {
        double* dp = (double*) this->databuff.data();
        for(unsigned int i=0, idx=0; i<this->npix; i++)
          for(unsigned int j=0; j<this->ncomp; j++, idx++)
            dp[idx] = byte_swap(dp[idx]);
        break;
      }
    }
    #endif
  }

  /**
  * @brief Convert the type of the internal data buffer.
  *
  * The data types are defined through the input pointer types.
  *
  * @tparam T   Output type.
  * @tparam S   Input type.
  * @param out  Output pointer.
  * @param in   Input pointer.
  */
  template<typename T, typename S>
  inline void convert_dtype(T* out, S* in)
  {
    for(size_t c = 0; c < this->npix * this->ncomp; c++)
      out[c] = in[c];
  }

  public:
  /// Empty constructor initializes to 0
  itk_image() :
  npix(0), ncomp(0), compsz(0), pixsz(0), datasz(0),
  dim({0, 0, 0}), comptypeid(0), pixspc({0.0, 0.0, 0.0}),
  orig({0.0, 0.0, 0.0}), databuff()
  {}

  itk_image(const itk_image & inp) :
  npix(0), ncomp(0), compsz(0), pixsz(0), datasz(0),
  dim({0, 0, 0}), comptypeid(0), pixspc({0.0, 0.0, 0.0}),
  orig({0.0, 0.0, 0.0}), databuff()
  {
    this->assign(inp);
  }

  inline void clear()
  {
    npix = 0;
    ncomp = 0; 
    compsz = 0;
    pixsz = 0;
    datasz = 0;
    dim = {0, 0, 0};
    comptypeid = 0;
    pixspc = {0.0, 0.0, 0.0};
    orig = {0.0, 0.0, 0.0};
    databuff.clear();
  }

  inline bool read_file(const std::string& filename)
  {
    if (endswith(filename, VTK_EXT) || endswith(filename, ITK_EXT))
      return read_file_vtk(filename.c_str());
    else if (endswith(filename, NRRD_EXT))
      return read_file_nrrd(filename.c_str());
    else return false;
  }

  /**
  * @brief Read an itk image from file
  *
  * @param filename The filename.
  */

  bool read_file_vtk(const char* filename);

  bool read_file_nrrd(const char * filename);

  /**
  * @brief Write an itk image file. By default (comp == -1) we try to write all
  *        components. This only works for itk format, not for nrrd.
  *
  * @param filename  The filename.
  * @param comp      The component to write, -1 for all components.
  */
  inline bool write_file(const std::string& filename, int comp = -1)
  {
    if (endswith(filename, VTK_EXT) || endswith(filename, ITK_EXT))
      return write_file_vtk(filename.c_str(), comp);
    else if (endswith(filename, NRRD_EXT))
      return write_file_nrrd(filename.c_str(), comp < 0 ? 0 : comp);
    else if (endswith(filename, ".raw"))
      return write_file_raw(filename.c_str());
    else return false;
  }

  // write image as vtk image. by default (comp = -1) we write all components,
  // else a specificly selected one.
  bool write_file_vtk(const char * filename, int comp = -1);

  // write image in nrrd format, for nrrd, we have to choose one component,
  // as currently we cannot write all components
  bool write_file_nrrd(const char * filename, unsigned int comp = 0);

  bool write_file_raw(const char * filename);

  void write_file_color_scalars(const char * filename)
  {
    if (this->comptypeid != 1) {
      printf("[ITKIMAGE]: failed to write color-scalar-file, wrong data type %s\n",
             itk_get_datatype_str_vtk(this->comptypeid));
    }
    else {
      byteswap_data();

      FILE * fp;
      fp = fopen(filename, "wb");
      if (fp)
      {
        char line[256];
        sprintf(line, "# vtk DataFile Version 3.0\n"
                      "GENERATED BY CFEL\n"
                      "BINARY\n"
                      "DATASET STRUCTURED_POINTS\n"
                      "DIMENSIONS %zu %zu %zu\n", this->dim.v1, this->dim.v2, this->dim.v3);
        fwrite(line, sizeof(char), strlen(line), fp);
        sprintf(line, "SPACING %.16e %.16e %.16e\n"
                      "ORIGIN %.16e %.16e %.16e\n", this->pixspc.v1, this->pixspc.v2, this->pixspc.v3,
                                                    this->orig.v1, this->orig.v2, this->orig.v3);
        fwrite(line, sizeof(char), strlen(line), fp);
        sprintf(line, "POINT_DATA %zu\n"
                      "COLOR_SCALARS scalars %u\n", this->npix, this->ncomp);
        fwrite(line, sizeof(char), strlen(line), fp);
        fwrite(this->databuff.data(), sizeof(char), this->datasz, fp);
        fclose(fp);
        printf("[ITKIMAGE]: written to '%s' \n", filename);
      }
      else {
        printf("[ITKIMAGE]: failed to open '%s' \n", filename);
      }

      byteswap_data();
    }
  }

  inline bool to_hex_mesh(mt_meshdata& mesh, unsigned int c=0, mt_real scale=1.0) const
  {
    if ((this->comptypeid < 1) || (this->comptypeid > 8))
      return false;

    if (c >= ncomp) c = 0;
    if (!(scale > 0.0)) scale = 1.0;
    mesh.clear();

    const mt_idx_t delta[24] = {0,1,1, 1,1,1, 1,0,1, 0,0,1, 0,1,0 ,0,0,0 ,1,0,0, 1,1,0};
    mt_idx_t vtx[8];
    mt_real pnt[3];

    size_t nelem = static_cast<size_t>(this->npix*0.5); // first estimate
    size_t npnts = (nelem*4); // first estimate

    // allocate some memory for the
    // elements and points
    mesh.e2n_dsp.reserve(nelem);
    mesh.e2n_con.reserve(8*nelem);
    mesh.etags.reserve(nelem);
    mesh.xyz.reserve(3*npnts);

    // get data buffer
    const char* data =this->databuff.data();
    mt_idx_t elem_tag;

    // number of grid points
    npnts = (this->dim.v1+1)*(this->dim.v2+1)*(this->dim.v3+1);
    mt_vector<mt_idx_t> pnt_idx(npnts, -1);

    npnts = 0;
    // iterate over all image pixels
    for (size_t i=0; i<this->dim.v1; i++) {
      for (size_t j=0; j<this->dim.v2; j++) {
        for (size_t k=0; k<this->dim.v3; k++) {
          // get index of pixel component
          const size_t cidx = ((k*this->dim.v1*this->dim.v2+j*this->dim.v1+i)*ncomp+c)*this->compsz;
          elem_tag = 0;
          itk_comp_as_mt_idx_t(&data[cidx], this->comptypeid, elem_tag);

          // check if the pixel component is zero
          if (elem_tag) {
            // get current number of elements
            nelem = mesh.e2n_dsp.size();
            mesh.e2n_dsp.push_back(8*nelem);
            // collect hex vertices
            for (size_t l=0; l<8; l++) {
              // point index in grid
              const size_t pidx = (k+delta[3*l+2])*(this->dim.v1+1)*(this->dim.v2+1) +
                                  (j+delta[3*l+1])*(this->dim.v1+1) + (i+delta[3*l+0]);
              // check if grid point is a mesh point
              if (pnt_idx[pidx] == -1) {
                // add point to mesh and assign mesh index
                pnt[0] = (orig.v1+pixspc.v1*(i+delta[3*l+0]))*scale;
                pnt[1] = (orig.v2+pixspc.v2*(j+delta[3*l+1]))*scale;
                pnt[2] = (orig.v3+pixspc.v3*(k+delta[3*l+2]))*scale;
                mesh.xyz.append(pnt, pnt+3);
                pnt_idx[pidx] = npnts++;
              }
              vtx[l] = pnt_idx[pidx];
            }
            // fill element data
            mesh.e2n_con.append(vtx, vtx+8);
            mesh.etags.push_back(elem_tag);
          }
        }
      }
    }
    // get number of elements
    nelem = mesh.e2n_dsp.size();
    // set missing element data
    mesh.e2n_cnt.assign(nelem, 8);
    mesh.etype.assign(nelem, Hexa);
    // reallocate memory
    mesh.e2n_dsp.shrink_to_fit();
    mesh.e2n_con.shrink_to_fit();
    mesh.etags.shrink_to_fit();
    mesh.xyz.shrink_to_fit();

    return true;
  }

  inline bool to_hex_mesh(mt_meshdata& mesh, const MT_USET<mt_tag_t> & sel_reg, unsigned int c=0, mt_real scale=1.0) const
  {
    if ((this->comptypeid < 1) || (this->comptypeid > 8))
      return false;

    if (c >= ncomp) c = 0;
    if (!(scale > 0.0)) scale = 1.0;
    mesh.clear();

    const mt_idx_t delta[24] = {0,1,1, 1,1,1, 1,0,1, 0,0,1, 0,1,0 ,0,0,0 ,1,0,0, 1,1,0};
    mt_idx_t vtx[8];
    mt_real pnt[3];

    // get data buffer
    const char* data = this->databuff.data();

    size_t nelem = 0;
    for (size_t i=0; i<this->dim.v1; i++) {
      for (size_t j=0; j<this->dim.v2; j++) {
        for (size_t k=0; k<this->dim.v3; k++) {
          // get index of pixel component
          const size_t cidx = ((k*this->dim.v1*this->dim.v2+j*this->dim.v1+i)*ncomp+c)*this->compsz;
          mt_idx_t elem_tag = 0;
          itk_comp_as_mt_idx_t(&data[cidx], this->comptypeid, elem_tag);

          if(sel_reg.count(elem_tag))
            nelem++;
        }
      }
    }

    size_t npnts = (nelem*4); // first estimate
    // allocate some memory for the
    // elements and points
    mesh.e2n_dsp.reserve(nelem);
    mesh.e2n_con.reserve(8*nelem);
    mesh.etags.reserve(nelem);
    mesh.xyz.reserve(3*npnts);

    // number of grid points
    npnts = (this->dim.v1+1)*(this->dim.v2+1)*(this->dim.v3+1);
    mt_vector<mt_idx_t> pnt_idx(npnts, -1);

    npnts = 0;
    // iterate over all image pixels
    for (size_t k=0; k<this->dim.v3; k++) {
      for (size_t j=0; j<this->dim.v2; j++) {
        for (size_t i=0; i<this->dim.v1; i++) {
          // get index of pixel component
          const size_t cidx = ((k*this->dim.v1*this->dim.v2+j*this->dim.v1+i)*ncomp+c)*this->compsz;
          mt_idx_t elem_tag = 0;
          itk_comp_as_mt_idx_t(&data[cidx], this->comptypeid, elem_tag);

          // check if the pixel component is zero
          if (sel_reg.count(elem_tag)) {
            // get current number of elements
            nelem = mesh.e2n_dsp.size();
            mesh.e2n_dsp.push_back(8*nelem);
            // collect hex vertices
            for (size_t l=0; l<8; l++) {
              // point index in grid
              const size_t pidx = (k+delta[3*l+2])*(this->dim.v1+1)*(this->dim.v2+1) +
                                  (j+delta[3*l+1])*(this->dim.v1+1) + (i+delta[3*l+0]);
              // check if grid point is a mesh point
              if (pnt_idx[pidx] == -1) {
                // add point to mesh and assign mesh index
                pnt[0] = (orig.v1+pixspc.v1*(i+delta[3*l+0]))*scale;
                pnt[1] = (orig.v2+pixspc.v2*(j+delta[3*l+1]))*scale;
                pnt[2] = (orig.v3+pixspc.v3*(k+delta[3*l+2]))*scale;
                mesh.xyz.push_back(pnt, pnt+3);
                pnt_idx[pidx] = npnts++;
              }
              vtx[l] = pnt_idx[pidx];
            }
            // fill element data
            mesh.e2n_con.push_back(vtx, vtx+8);
            mesh.etags.push_back(elem_tag);
          }
        }
      }
    }
    // get number of elements
    nelem = mesh.e2n_dsp.size();
    // set missing element data
    mesh.e2n_cnt.assign(nelem, 8);
    mesh.etype.assign(nelem, Hexa);
    return true;
  }

  inline bool to_hex_surf(mt_meshdata& mesh, const MT_USET<mt_tag_t> & sel_reg, unsigned int c=0, mt_real scale=1.0) const
  {
    if ((this->comptypeid < 1) || (this->comptypeid > 8))
      return false;

    // struct timeval t1, t2;
    // gettimeofday(&t1, NULL);

    if(sel_reg.size() == 0) {
      fprintf(stderr, "%s error: empty region set provided! skipping!\n", __func__);
      return false;
    }

    if (c >= ncomp) c = 0;
    if (!(scale > 0.0)) scale = 1.0;
    mesh.clear();

    const mt_idx_t delta[24] = {0,1,1, 1,1,1, 1,0,1, 0,0,1, 0,1,0 ,0,0,0 ,1,0,0, 1,1,0};
    mt_idx_t vtx[8];
    mt_real pnt[3];

    // get data buffer
    const char* data = this->databuff.data();

    // number of grid points
    size_t npnts = (this->dim.v1+1)*(this->dim.v2+1)*(this->dim.v3+1);
    size_t nelem = 0;

    mt_vector<mt_idx_t> pnt_idx(npnts, -1);
    mt_vector<mt_tag_t> orig_tags;
    mt_vector<mt_idx_t> elem_orig;

    orig_tags.reserve(npnts*0.25);
    elem_orig.reserve(npnts*0.25);
    mesh.xyz.reserve(npnts*0.25);
    mesh.e2n_con.reserve(npnts*0.25);

    npnts = 0;

    auto need_surf = [&sel_reg, &data, &c](const itk_image & img, int i, int j, int k)
    {
      if ((i < 0) || (j < 0) || (k < 0)) return true;
      if (unsigned(i) >= img.dim.v1 || unsigned(j) >= img.dim.v2 || unsigned(k) >= img.dim.v3)
        return true;

      const size_t cidx = ((k*img.dim.v1*img.dim.v2+j*img.dim.v1+i)*img.ncomp+c)*img.compsz;
      mt_tag_t elem_tag = 0;
      itk_comp_as<mt_tag_t>(&data[cidx], img.comptypeid, elem_tag);

      return sel_reg.count(elem_tag) == 0;
    };

    mt_vector<bool> surf_flags(6);
    auto fill_surf_flags = [&need_surf,&surf_flags] (const itk_image & img, int i, int j, int k) {
      bool have_itf = false;

      surf_flags[0] = need_surf(img, i+1,j,k); have_itf |= surf_flags[0];
      surf_flags[1] = need_surf(img, i-1,j,k); have_itf |= surf_flags[1];
      surf_flags[2] = need_surf(img, i,j-1,k); have_itf |= surf_flags[2];
      surf_flags[3] = need_surf(img, i,j+1,k); have_itf |= surf_flags[3];
      surf_flags[4] = need_surf(img, i,j,k-1); have_itf |= surf_flags[4];
      surf_flags[5] = need_surf(img, i,j,k+1); have_itf |= surf_flags[5];

      return have_itf;
    };

    auto push_quad_tris = [&mesh, &elem_orig] (mt_idx_t v1, mt_idx_t v2, mt_idx_t v3, mt_idx_t v4, mt_idx_t eidx)
    {
      mesh.e2n_con.push_back(v1); mesh.e2n_con.push_back(v2); mesh.e2n_con.push_back(v3);
      elem_orig.push_back(eidx);

      mesh.e2n_con.push_back(v1); mesh.e2n_con.push_back(v3); mesh.e2n_con.push_back(v4);
      elem_orig.push_back(eidx);
    };

    // iterate over all image pixels
    for (size_t k=0; k<this->dim.v3; k++) {
      for (size_t j=0; j<this->dim.v2; j++) {
        for (size_t i=0; i<this->dim.v1; i++) {
          // get index of pixel component
          const size_t cidx = ((k*this->dim.v1*this->dim.v2+j*this->dim.v1+i)*ncomp+c)*this->compsz;
          mt_idx_t elem_tag = 0;
          itk_comp_as_mt_idx_t(&data[cidx], this->comptypeid, elem_tag);
          const bool sel_pixel = sel_reg.count(elem_tag);

          // check if the pixel component is zero
          if (sel_pixel) {
            // get current number of elements
            mt_idx_t eidx = nelem++;
            orig_tags.push_back(mt_tag_t(elem_tag));

            if(fill_surf_flags(*this, i, j, k))
            {
              // collect hex vertices
              for (size_t l=0; l<8; l++) {
                // point index in grid
                const size_t pidx = (k+delta[3*l+2])*(this->dim.v1+1)*(this->dim.v2+1) +
                                    (j+delta[3*l+1])*(this->dim.v1+1) + (i+delta[3*l+0]);
                // check if grid point is a mesh point
                if (pnt_idx[pidx] == -1) {
                  // add point to mesh and assign mesh index
                  pnt[0] = (orig.v1+pixspc.v1*(i+delta[3*l+0]))*scale;
                  pnt[1] = (orig.v2+pixspc.v2*(j+delta[3*l+1]))*scale;
                  pnt[2] = (orig.v3+pixspc.v3*(k+delta[3*l+2]))*scale;
                  mesh.xyz.push_back(pnt, pnt+3);
                  pnt_idx[pidx] = npnts++;
                }
                vtx[l] = pnt_idx[pidx];
              }

              // surfaces are defined according to carpmanual in *clockwise* order
              // surfaces are (1,2,3,4) , (3,2,8,7) , (4,3,7,6) , (1,4,6,5) , (2,1,5,8), (6,7,8,5)
              /*
                     6 ------------ 7
                    /              /|
                   /              / |
                  5 ------------ 8  |
                  |              |  |
                  |              |  |
                  |  4           |  3
                  |              | /
                  |              |/
                  1 ------------ 2
              */
              mt_idx_t n1 = vtx[0], n2 = vtx[1], n3 = vtx[2], n4 = vtx[3],
                       n5 = vtx[4], n6 = vtx[5], n7 = vtx[6], n8 = vtx[7];

              if(surf_flags[0])
                push_quad_tris(n3, n2, n8, n7, eidx);
              if(surf_flags[1])
                push_quad_tris(n1, n4, n6, n5, eidx);
              if(surf_flags[2])
                push_quad_tris(n4, n3, n7, n6, eidx);
              if(surf_flags[3])
                push_quad_tris(n2, n1, n5, n8, eidx);
              if(surf_flags[4])
                push_quad_tris(n6, n7, n8, n5, eidx);
              if(surf_flags[5])
                push_quad_tris(n1, n2, n3, n4, eidx);
            }
          }
        }
      }
    }

    mesh.e2n_cnt.assign(mesh.e2n_con.size() / 3, 3);
    mesh.etype.assign(mesh.e2n_cnt.size(), Tri);

    mesh.etags.resize(mesh.e2n_cnt.size());
    for(size_t i=0; i<elem_orig.size(); i++)
      mesh.etags[i] = orig_tags[elem_orig[i]];

    // gettimeofday(&t2, NULL);
    // printf("Extracted hex surf in %.3f sec..\n", (float)timediff_sec(t1, t2));

    return true;
  }


  inline bool to_hex_mesh(mt_meshdata& mesh, const mt_triple<MT_USET<mt_idx_t>>& slices,
                          const MT_USET<mt_tag_t> & sel_reg, unsigned int c=0, mt_real scale=1.0) const
  {
    if ((this->comptypeid < 1) || (this->comptypeid > 8))
      return false;

    if (c >= ncomp) c = 0;
    if (!(scale > 0.0)) scale = 1.0;
    mesh.clear();

    const mt_idx_t delta[24] = {0,1,1, 1,1,1, 1,0,1, 0,0,1, 0,1,0 ,0,0,0 ,1,0,0, 1,1,0};
    mt_idx_t vtx[8];
    mt_real pnt[3];

    size_t nelem = static_cast<size_t>(this->npix*0.5); // first estimate
    size_t npnts = (nelem*4); // first estimate

    // allocate some memory for the
    // elements and points
    mesh.e2n_dsp.reserve(nelem);
    mesh.e2n_con.reserve(8*nelem);
    mesh.etags.reserve(nelem);
    mesh.xyz.reserve(3*npnts);

    // get data buffer
    const char* data =this->databuff.data();
    mt_idx_t elem_tag;

    // number of grid points
    npnts = (this->dim.v1+1)*(this->dim.v2+1)*(this->dim.v3+1);
    mt_vector<mt_idx_t> pnt_idx(npnts, -1);

    npnts = 0;
    // iterate over all image pixels
    for (size_t i=0; i<this->dim.v1; i++) {
      for (size_t j=0; j<this->dim.v2; j++) {
        for (size_t k=0; k<this->dim.v3; k++) {
          // get index of pixel component
          const size_t cidx = ((k*this->dim.v1*this->dim.v2+j*this->dim.v1+i)*ncomp+c)*this->compsz;
          elem_tag = 0;
          itk_comp_as_mt_idx_t(&data[cidx], this->comptypeid, elem_tag);
          const bool sel_pixel = ((elem_tag) && ((sel_reg.empty()) || (sel_reg.count(elem_tag))));

          // check if the pixel component is zero
          if ((sel_pixel) && ((slices.v1.count(i)) || (slices.v2.count(j)) || (slices.v3.count(k))))
          {
            // get current number of elements
            nelem = mesh.e2n_dsp.size();
            mesh.e2n_dsp.push_back(8*nelem);
            // collect hex vertices
            for (size_t l=0; l<8; l++) {
              // point index in grid
              const size_t pidx = (k+delta[3*l+2])*(this->dim.v1+1)*(this->dim.v2+1) +
                                  (j+delta[3*l+1])*(this->dim.v1+1) + (i+delta[3*l+0]);
              // check if grid point is a mesh point
              if (pnt_idx[pidx] == -1) {
                // add point to mesh and assign mesh index
                pnt[0] = (orig.v1+pixspc.v1*(i+delta[3*l+0]))*scale;
                pnt[1] = (orig.v2+pixspc.v2*(j+delta[3*l+1]))*scale;
                pnt[2] = (orig.v3+pixspc.v3*(k+delta[3*l+2]))*scale;
                mesh.xyz.append(pnt, pnt+3);
                pnt_idx[pidx] = npnts++;
              }
              vtx[l] = pnt_idx[pidx];
            }
            // fill element data
            mesh.e2n_con.append(vtx, vtx+8);
            mesh.etags.push_back(elem_tag);
          }
        }
      }
    }
    // get number of elements
    nelem = mesh.e2n_dsp.size();
    // set missing element data
    mesh.e2n_cnt.assign(nelem, 8);
    mesh.etype.assign(nelem, Hexa);
    // reallocate memory
    mesh.e2n_dsp.shrink_to_fit();
    mesh.e2n_con.shrink_to_fit();
    mesh.etags.shrink_to_fit();
    mesh.xyz.shrink_to_fit();

    return true;
  }

  inline bool to_hex_mesh(mt_meshdata& mesh, mt_vector<mt_real>& values,
                          unsigned int c=0, mt_real scale=1.0) const
  {
    if ((this->comptypeid < 1) || (this->comptypeid > 10))
      return false;

    if (c >= ncomp) c = 0;
    if (!(scale > 0.0)) scale = 1.0;
    mesh.clear();
    values.clear();

    const mt_idx_t delta[24] = {0,1,1, 1,1,1, 1,0,1, 0,0,1, 0,1,0 ,0,0,0 ,1,0,0, 1,1,0};

    size_t nelem = this->npix;
    size_t npnts = (this->dim.v1+1)*(this->dim.v2+1)*(this->dim.v3+1);

    // allocate some memory for the
    // elements and points
    mesh.e2n_dsp.resize(nelem);
    mesh.e2n_cnt.assign(nelem, 8);
    mesh.e2n_con.resize(8*nelem);
    mesh.etags.assign(nelem, mt_tag_t(0));
    mesh.etype.assign(nelem, Hexa);
    mesh.xyz.resize(3*npnts);

    values.resize(nelem);

    // get data buffer
    const char* data = this->databuff.data();
    mt_real elem_val;

    nelem = 0;
    // iterate over all image pixels
    for (size_t i=0; i<this->dim.v1; i++) {
      for (size_t j=0; j<this->dim.v2; j++) {
        for (size_t k=0; k<this->dim.v3; k++, nelem++) {

          // get index of pixel component
          const size_t cidx = ((k*this->dim.v1*this->dim.v2+j*this->dim.v1+i)*ncomp+c)*this->compsz;
          elem_val = 0.0;
          itk_comp_as_mt_real(&data[cidx], this->comptypeid, elem_val);

          // get current number of elements
          mesh.e2n_dsp[nelem] = 8*nelem;
          // collect hex vertices
          for (size_t l=0; l<8; l++) {
            // point index
            const size_t pidx = (k+delta[3*l+2])*(this->dim.v1+1)*(this->dim.v2+1) +
                                (j+delta[3*l+1])*(this->dim.v1+1) + (i+delta[3*l+0]);
            // add point to mesh and assign mesh index
            mesh.xyz[3*pidx+0] = (orig.v1+pixspc.v1*(i+delta[3*l+0]))*scale;
            mesh.xyz[3*pidx+1] = (orig.v2+pixspc.v2*(j+delta[3*l+1]))*scale;
            mesh.xyz[3*pidx+2] = (orig.v3+pixspc.v3*(k+delta[3*l+2]))*scale;

            mesh.e2n_con[8*nelem+l] = pidx;
          }

          values[nelem] = elem_val;
        }
      }
    }

    return true;
  }

  inline bool to_hex_mesh(mt_meshdata& mesh, mt_vector<mt_real>& values, const mt_triple<MT_USET<mt_idx_t>>& slices,
                          unsigned int c=0, mt_real scale=1.0) const
  {
    if ((this->comptypeid < 1) || (this->comptypeid > 10))
      return false;

    if (c >= ncomp) c = 0;
    if (!(scale > 0.0)) scale = 1.0;
    mesh.clear();

    const mt_idx_t delta[24] = {0,1,1, 1,1,1, 1,0,1, 0,0,1, 0,1,0 ,0,0,0 ,1,0,0, 1,1,0};
    mt_idx_t vtx[8];
    mt_real pnt[3];

    size_t nelem = static_cast<size_t>(this->npix*0.5); // first estimate
    size_t npnts = (nelem*4); // first estimate

    // allocate some memory for the
    // elements and points
    mesh.e2n_dsp.reserve(nelem);
    mesh.e2n_con.reserve(8*nelem);
    mesh.xyz.reserve(3*npnts);
    values.reserve(nelem);

    // get data buffer
    const char* data =this->databuff.data();
    mt_real elem_val;

    // number of grid points
    npnts = (this->dim.v1+1)*(this->dim.v2+1)*(this->dim.v3+1);
    mt_vector<mt_idx_t> pnt_idx(npnts, -1);

    npnts = 0;
    // iterate over all image pixels
    for (size_t i=0; i<this->dim.v1; i++) {
      for (size_t j=0; j<this->dim.v2; j++) {
        for (size_t k=0; k<this->dim.v3; k++) {
          // get index of pixel component
          const size_t cidx = ((k*this->dim.v1*this->dim.v2+j*this->dim.v1+i)*ncomp+c)*this->compsz;
          elem_val = 0.0;
          itk_comp_as_mt_real(&data[cidx], this->comptypeid, elem_val);

          // check if the pixel component is zero
          if ((slices.v1.count(i)) || (slices.v2.count(j)) || (slices.v3.count(k))) {
            // get current number of elements
            nelem = mesh.e2n_dsp.size();
            mesh.e2n_dsp.push_back(8*nelem);
            values.push_back(elem_val);
            // collect hex vertices
            for (size_t l=0; l<8; l++) {
              // point index in grid
              const size_t pidx = (k+delta[3*l+2])*(this->dim.v1+1)*(this->dim.v2+1) +
                                  (j+delta[3*l+1])*(this->dim.v1+1) + (i+delta[3*l+0]);
              // check if grid point is a mesh point
              if (pnt_idx[pidx] == -1) {
                // add point to mesh and assign mesh index
                pnt[0] = (orig.v1+pixspc.v1*(i+delta[3*l+0]))*scale;
                pnt[1] = (orig.v2+pixspc.v2*(j+delta[3*l+1]))*scale;
                pnt[2] = (orig.v3+pixspc.v3*(k+delta[3*l+2]))*scale;
                mesh.xyz.append(pnt, pnt+3);
                pnt_idx[pidx] = npnts++;
              }
              vtx[l] = pnt_idx[pidx];
            }
            // fill element data
            mesh.e2n_con.append(vtx, vtx+8);
          }
        }
      }
    }
    // get number of elements
    nelem = mesh.e2n_dsp.size();
    // set missing element data
    mesh.e2n_cnt.assign(nelem, 8);
    mesh.etype.assign(nelem, Hexa);
    mesh.etags.assign(nelem, mt_tag_t(0));
    // reallocate memory
    mesh.e2n_dsp.shrink_to_fit();
    mesh.e2n_con.shrink_to_fit();
    mesh.xyz.shrink_to_fit();
    values.shrink_to_fit();

    return true;
  }
  void convert(const int dtype)
  {
    if ((dtype < 1) || (dtype > 11))
      return;

    if (dtype == this->comptypeid)
      return;

    itk_image tmp_img;
    tmp_img.assign(*this, itk_get_datatype_str_vtk(dtype));

    this->comptypeid = dtype;
    this->compsz     = tmp_img.compsz;
    this->pixsz      = tmp_img.pixsz;
    this->datasz     = tmp_img.datasz;
    this->databuff   = tmp_img.databuff;
  }

  /**
  * @brief Copy the contents of a given itk_image, possibly changing the datatype
  *
  * @param inp   Input itk_image.
  * @param type  The new data type. Use "" to keep the old type.
  *
  * @post Data "inp" has been copied.
  */
  inline void assign(const itk_image & inp, const char* typestr = "")
  {
    this->npix   = inp.npix;
    this->ncomp  = inp.ncomp;
    this->dim    = inp.dim;
    this->pixspc = inp.pixspc;
    this->orig   = inp.orig;

    if(strlen(typestr) == 0) {
      this->compsz     = inp.compsz;
      this->comptypeid = inp.comptypeid;
    }
    else {
      this->comptypeid = itk_get_datatype_id_vtk(typestr);
      this->compsz     = itk_get_datatype_size(this->comptypeid);
    }

    this->pixsz  = this->ncomp * this->compsz;
    this->datasz = this->npix  * this->pixsz;

    this->databuff.assign(this->datasz, char(0));

    const char *input  = inp.databuff.data();
    char       *output = this->databuff.data();

    switch(this->comptypeid) // dtype to convert to
    {
      case 1:  // (unsigned char);
      {
         switch(inp.comptypeid) {
          case 1:  convert_dtype((unsigned char*)output, (unsigned char*) input); break;
          case 2:  convert_dtype((unsigned char*)output, (char*) input); break;
          case 3:  convert_dtype((unsigned char*)output, (unsigned short*) input); break;
          case 4:  convert_dtype((unsigned char*)output, (short*) input); break;
          case 5:  convert_dtype((unsigned char*)output, (unsigned int*) input); break;
          case 6:  convert_dtype((unsigned char*)output, (int*) input); break;
          case 7:  convert_dtype((unsigned char*)output, (unsigned long*) input); break;
          case 8:  convert_dtype((unsigned char*)output, (long*) input); break;
          case 9:  convert_dtype((unsigned char*)output, (float*) input); break;
          case 10: convert_dtype((unsigned char*)output, (double*) input); break;
          default: assert(0);
        }
        break;
      }
      case 2:  // (char);
      {
         switch(inp.comptypeid) {
          case 1:  convert_dtype((char*)output, (unsigned char*) input); break;
          case 2:  convert_dtype((char*)output, (char*) input); break;
          case 3:  convert_dtype((char*)output, (unsigned short*) input); break;
          case 4:  convert_dtype((char*)output, (short*) input); break;
          case 5:  convert_dtype((char*)output, (unsigned int*) input); break;
          case 6:  convert_dtype((char*)output, (int*) input); break;
          case 7:  convert_dtype((char*)output, (unsigned long*) input); break;
          case 8:  convert_dtype((char*)output, (long*) input); break;
          case 9:  convert_dtype((char*)output, (float*) input); break;
          case 10: convert_dtype((char*)output, (double*) input); break;
          default: assert(0);
        }
        break;
      }
      case 3:  // (unsigned short);
      {
        switch(inp.comptypeid) {
          case 1:  convert_dtype((unsigned short*)output, (unsigned char*) input); break;
          case 2:  convert_dtype((unsigned short*)output, (char*) input); break;
          case 3:  convert_dtype((unsigned short*)output, (unsigned short*) input); break;
          case 4:  convert_dtype((unsigned short*)output, (short*) input); break;
          case 5:  convert_dtype((unsigned short*)output, (unsigned int*) input); break;
          case 6:  convert_dtype((unsigned short*)output, (int*) input); break;
          case 7:  convert_dtype((unsigned short*)output, (unsigned long*) input); break;
          case 8:  convert_dtype((unsigned short*)output, (long*) input); break;
          case 9:  convert_dtype((unsigned short*)output, (float*) input); break;
          case 10: convert_dtype((unsigned short*)output, (double*) input); break;
          default: assert(0);
        }
        break;
      }
      case 4:  // (short);
      {
        switch(inp.comptypeid) {
          case 1:  convert_dtype((short*)output, (unsigned char*) input); break;
          case 2:  convert_dtype((short*)output, (char*) input); break;
          case 3:  convert_dtype((short*)output, (unsigned short*) input); break;
          case 4:  convert_dtype((short*)output, (short*) input); break;
          case 5:  convert_dtype((short*)output, (unsigned int*) input); break;
          case 6:  convert_dtype((short*)output, (int*) input); break;
          case 7:  convert_dtype((short*)output, (unsigned long*) input); break;
          case 8:  convert_dtype((short*)output, (long*) input); break;
          case 9:  convert_dtype((short*)output, (float*) input); break;
          case 10: convert_dtype((short*)output, (double*) input); break;
          default: assert(0);
        }
        break;
      }
      case 5:  // (unsigned int);
      {
        switch(inp.comptypeid) {
          case 1:  convert_dtype((unsigned int*)output, (unsigned char*) input); break;
          case 2:  convert_dtype((unsigned int*)output, (char*) input); break;
          case 3:  convert_dtype((unsigned int*)output, (unsigned short*) input); break;
          case 4:  convert_dtype((unsigned int*)output, (short*) input); break;
          case 5:  convert_dtype((unsigned int*)output, (unsigned int*) input); break;
          case 6:  convert_dtype((unsigned int*)output, (int*) input); break;
          case 7:  convert_dtype((unsigned int*)output, (unsigned long*) input); break;
          case 8:  convert_dtype((unsigned int*)output, (long*) input); break;
          case 9:  convert_dtype((unsigned int*)output, (float*) input); break;
          case 10: convert_dtype((unsigned int*)output, (double*) input); break;
          default: assert(0);
        }
        break;
      }
      case 6:  // (int);
      {
        switch(inp.comptypeid) {
          case 1:  convert_dtype((int*)output, (unsigned char*) input); break;
          case 2:  convert_dtype((int*)output, (char*) input); break;
          case 3:  convert_dtype((int*)output, (unsigned short*) input); break;
          case 4:  convert_dtype((int*)output, (short*) input); break;
          case 5:  convert_dtype((int*)output, (unsigned int*) input); break;
          case 6:  convert_dtype((int*)output, (int*) input); break;
          case 7:  convert_dtype((int*)output, (unsigned long*) input); break;
          case 8:  convert_dtype((int*)output, (long*) input); break;
          case 9:  convert_dtype((int*)output, (float*) input); break;
          case 10: convert_dtype((int*)output, (double*) input); break;
          default: assert(0);
        }
        break;
      }
      case 7:  // (unsigned long);
      {
        switch(inp.comptypeid) {
          case 1:  convert_dtype((unsigned long*)output, (unsigned char*) input); break;
          case 2:  convert_dtype((unsigned long*)output, (char*) input); break;
          case 3:  convert_dtype((unsigned long*)output, (unsigned short*) input); break;
          case 4:  convert_dtype((unsigned long*)output, (short*) input); break;
          case 5:  convert_dtype((unsigned long*)output, (unsigned int*) input); break;
          case 6:  convert_dtype((unsigned long*)output, (int*) input); break;
          case 7:  convert_dtype((unsigned long*)output, (unsigned long*) input); break;
          case 8:  convert_dtype((unsigned long*)output, (long*) input); break;
          case 9:  convert_dtype((unsigned long*)output, (float*) input); break;
          case 10: convert_dtype((unsigned long*)output, (double*) input); break;
          default: assert(0);
        }
        break;
      }
      case 8:  // (long);
      {
        switch(inp.comptypeid) {
          case 1:  convert_dtype((long*)output, (unsigned char*) input); break;
          case 2:  convert_dtype((long*)output, (char*) input); break;
          case 3:  convert_dtype((long*)output, (unsigned short*) input); break;
          case 4:  convert_dtype((long*)output, (short*) input); break;
          case 5:  convert_dtype((long*)output, (unsigned int*) input); break;
          case 6:  convert_dtype((long*)output, (int*) input); break;
          case 7:  convert_dtype((long*)output, (unsigned long*) input); break;
          case 8:  convert_dtype((long*)output, (long*) input); break;
          case 9:  convert_dtype((long*)output, (float*) input); break;
          case 10: convert_dtype((long*)output, (double*) input); break;
          default: assert(0);
        }
        break;
      }
      case 9:  // (float);
      {
        switch(inp.comptypeid) {
          case 1:  convert_dtype((float*)output, (unsigned char*) input); break;
          case 2:  convert_dtype((float*)output, (char*) input); break;
          case 3:  convert_dtype((float*)output, (unsigned short*) input); break;
          case 4:  convert_dtype((float*)output, (short*) input); break;
          case 5:  convert_dtype((float*)output, (unsigned int*) input); break;
          case 6:  convert_dtype((float*)output, (int*) input); break;
          case 7:  convert_dtype((float*)output, (unsigned long*) input); break;
          case 8:  convert_dtype((float*)output, (long*) input); break;
          case 9:  convert_dtype((float*)output, (float*) input); break;
          case 10: convert_dtype((float*)output, (double*) input); break;
          default: assert(0);
        }
        break;
      }
      case 10: // (double);
      {
        switch(inp.comptypeid) {
          case 1:  convert_dtype((double*)output, (unsigned char*) input); break;
          case 2:  convert_dtype((double*)output, (char*) input); break;
          case 3:  convert_dtype((double*)output, (unsigned short*) input); break;
          case 4:  convert_dtype((double*)output, (short*) input); break;
          case 5:  convert_dtype((double*)output, (unsigned int*) input); break;
          case 6:  convert_dtype((double*)output, (int*) input); break;
          case 7:  convert_dtype((double*)output, (unsigned long*) input); break;
          case 8:  convert_dtype((double*)output, (long*) input); break;
          case 9:  convert_dtype((double*)output, (float*) input); break;
          case 10: convert_dtype((double*)output, (double*) input); break;
          default: assert(0);
        }
        break;
      }
      default: assert(0);
    }
  }

  inline void assign(const mt_triple<unsigned int> idim,
              const mt_triple<double>       iorig,
              const mt_triple<double>       ispacing,
              const short incomp,
              const char* datatype)
  {
    dim     = idim;
    orig    = iorig;
    pixspc  = ispacing;
    ncomp   = incomp;

    comptypeid = itk_get_datatype_id_vtk(datatype);
    compsz     = itk_get_datatype_size(comptypeid);

    npix   = dim.v1 * dim.v2 * dim.v3;
    pixsz  = ncomp * compsz;
    datasz = npix * pixsz;

    databuff.resize(datasz);
    databuff.zero();
  }

  inline bool bounding_box(mt_triple<unsigned int> & from, mt_triple<unsigned int> & to)
  {
    const char itkpixel_zero[8*ITK_MAX_PIXEL_COMP] = {0};
    bool have_nonzero = false;

    from = this->dim;
    to.v1 = to.v2 = to.v3 = 0;

    for (unsigned int k = 0; k < this->dim.v3; k++) {
      for (unsigned int j = 0; j < this->dim.v2; j++) {
        for (unsigned int i = 0; i < this->dim.v1; i++) {
          char * pixel = this->databuff.data() + (k*this->dim.v1*this->dim.v2 + j*this->dim.v1 + i) * this->pixsz;
          if (memcmp(pixel, itkpixel_zero, this->pixsz*sizeof(char)) != 0)
          {
            have_nonzero = true;

            if (i < from.v1) from.v1 = i;
            if (j < from.v2) from.v2 = j;
            if (k < from.v3) from.v3 = k;
            if (i > to.v1) to.v1 = i;
            if (j > to.v2) to.v2 = j;
            if (k > to.v3) to.v3 = k;
          }
        }
      }
    }
    printf("[ITKIMAGE]: bounding box ( %u %u %u ) - ( %u %u %u )\n", from.v1, from.v2, from.v3, to.v1, to.v2, to.v3);
    return have_nonzero;
  }

  template<class INT>
  inline void bounding_box(mt_triple<unsigned int> & from, mt_triple<unsigned int> & to, MT_USET<INT> & regions)
  {
    INT pixval = 0;

    from = this->dim;
    to.v1 = to.v2 = to.v3 = 0;

    for (unsigned int k = 0; k < this->dim.v3; k++) {
      for (unsigned int j = 0; j < this->dim.v2; j++) {
        for (unsigned int i = 0; i < this->dim.v1; i++) {
          char * pixel = this->databuff.data() + (k*this->dim.v1*this->dim.v2 + j*this->dim.v1 + i) * this->pixsz;
          itk_comp_as<INT>(pixel, this->comptypeid, pixval);
          if (regions.count(pixval))
          {
            if (i < from.v1) from.v1 = i;
            if (j < from.v2) from.v2 = j;
            if (k < from.v3) from.v3 = k;
            if (i > to.v1) to.v1 = i;
            if (j > to.v2) to.v2 = j;
            if (k > to.v3) to.v3 = k;
          }
        }
      }
    }
    printf("[ITKIMAGE]: bounding box ( %u %u %u ) - ( %u %u %u )\n", from.v1, from.v2, from.v3, to.v1, to.v2, to.v3);
  }

  /**
  * @brief extract slices with respect to a given plane
  */
  inline void extract_slices(unsigned int plane, const std::set<unsigned int> & idx)
  {
    if ((plane != 3) && (plane != 5) && (plane != 6))
      return;

    mt_triple<size_t> newdim = this->dim;
    if (plane == 3)
    {
      newdim.v3 = idx.size();
      if (newdim.v3 > this->dim.v3) return;
    }
    else if (plane == 5)
    {
      newdim.v2 = idx.size();
      if (newdim.v2 > this->dim.v2) return;
    }
    else
    {
      newdim.v1 = idx.size();
      if (newdim.v1 > this->dim.v1) return;
    }

    size_t newnpix = newdim.v1*newdim.v2*newdim.v3;
    size_t newdatasz = newnpix*this->pixsz;
    char *newdata = new char[newdatasz];

    if (plane == 3)
    {
      unsigned int ik, ok;
      for (unsigned int i = 0; i < this->dim.v1; i++)
      {
        for (unsigned int j = 0; j < this->dim.v2; j++)
        {
          for (auto it = idx.cbegin(); it != idx.cend(); it++)
          {
            ik = *it;
            ok = std::distance(idx.cbegin(), it);
            if (ik < this->dim.v3)
            {
              char *src  = this->databuff.data() + (ik*this->dim.v1*this->dim.v2 + j*this->dim.v1 + i)*this->pixsz;
              char *dest = newdata + (ok*newdim.v1*newdim.v2 + j*newdim.v1 + i)*this->pixsz;
              memcpy(dest, src, this->pixsz*sizeof(char));
            }
          }
        }
      }
    }
    else if (plane == 5)
    {
      unsigned int ij, oj;
      for (size_t i = 0; i < this->dim.v1; i++)
      {
       for (auto it = idx.cbegin(); it != idx.cend(); it++)
        {
          for (size_t k = 0; k < this->dim.v3; k++)
          {
            ij = *it;
            oj = std::distance(idx.cbegin(), it);
            if (ij < this->dim.v2)
            {
              char *src  = this->databuff.data() + (k*this->dim.v1*this->dim.v2 + ij*this->dim.v1 + i)*this->pixsz;
              char *dest = newdata + (k*newdim.v1*newdim.v2 + oj*newdim.v1 + i)*this->pixsz;
              memcpy(dest, src, this->pixsz*sizeof(char));
            }
          }
        }
      }
    }
    else
    {
      unsigned int ii, oi;
      for (auto it = idx.cbegin(); it != idx.cend(); it++)
      {
        for (size_t j = 0; j < this->dim.v2; j++)
        {
          for (size_t k = 0; k < this->dim.v3; k++)
          {
            ii = *it;
            oi = std::distance(idx.cbegin(), it);
            if (ii < this->dim.v1)
            {
              char *src  = this->databuff.data() + (k*this->dim.v1*this->dim.v2 + j*this->dim.v1 + ii)*this->pixsz;
              char *dest = newdata + (k*newdim.v1*newdim.v2 + j*newdim.v1 + oi)*this->pixsz;
              memcpy(dest, src, this->pixsz*sizeof(char));
            }
          }
        }
      }
    }

    this->dim = newdim;
    this->npix = newnpix;
    this->datasz = newdatasz;
    this->databuff.assign(newdatasz, newdata);
  }

  /**
  * @brief Normalize the voxel spacing and change image dimension to keep the image spacing
  */
  inline void normalize_spacing()
  {
    const double min_spacing = std::min(std::min(this->pixspc.v1, this->pixspc.v2), std::min(this->pixspc.v3, 1.0));
    this->pixspc.v1 /= min_spacing;
    this->pixspc.v2 /= min_spacing;
    this->pixspc.v3 /= min_spacing;
    printf("[ITKIMAGE]: normalize spacing, new spacing %lf\n", min_spacing);
    mt_triple<unsigned int> newdim;
    size_t l, m, n;
    newdim.v1 = static_cast<size_t>(ceil(this->dim.v1*this->pixspc.v1));
    newdim.v2 = static_cast<size_t>(ceil(this->dim.v2*this->pixspc.v2));
    newdim.v3 = static_cast<size_t>(ceil(this->dim.v3*this->pixspc.v3));
    if ((1.0*newdim.v1-0.5) > (this->dim.v1*this->pixspc.v1)) newdim.v1--;
    if ((1.0*newdim.v2-0.5) > (this->dim.v2*this->pixspc.v2)) newdim.v2--;
    if ((1.0*newdim.v3-0.5) > (this->dim.v3*this->pixspc.v3)) newdim.v3--;
    size_t newnpix = newdim.v1*newdim.v2*newdim.v3;
    size_t newdatasz = newnpix*this->pixsz;
    char *newdata = new char[newdatasz];
    for (size_t i = 0; i < newdim.v1; i++)
    {
      for (size_t j = 0; j < newdim.v2; j++)
      {
        for (size_t k = 0; k < newdim.v3; k++)
        {
          char *dest = newdata + (k*newdim.v1*newdim.v2 + j*newdim.v1 + i)*this->pixsz;
          l = static_cast<size_t>(floor((1.0*i+0.5)/this->pixspc.v1));
          m = static_cast<size_t>(floor((1.0*j+0.5)/this->pixspc.v2));
          n = static_cast<size_t>(floor((1.0*k+0.5)/this->pixspc.v3));
          char *src = this->databuff.data() + (n*this->dim.v1*this->dim.v2 + m*this->dim.v1 + l)*this->pixsz;
          memcpy(dest, src, this->pixsz*sizeof(char));
        }
      }
    }
    this->dim = newdim;
    this->npix = newnpix;
    this->datasz = newdatasz;
    this->pixspc.v1 = this->pixspc.v2 = this->pixspc.v3 = min_spacing;
    this->databuff.assign(newdatasz, newdata);
  }

  /**
   * @brief Add padding to the voxel data
   */
  inline void padding(const mt_triple<size_t> & size)
  {
    mt_triple<size_t> newdim;
    newdim.v1 = this->dim.v1 + 2*size.v1;
    newdim.v2 = this->dim.v2 + 2*size.v2;
    newdim.v3 = this->dim.v3 + 2*size.v3;
    size_t newnpix = newdim.v1*newdim.v2*newdim.v3;
    size_t newdatasz = newnpix * this->pixsz;
    char *newdata = new char[newdatasz];
    memset(newdata, 0, newdatasz);

    for (size_t k = 0; k < this->dim.v3; k++) {
      for (size_t j = 0; j < this->dim.v2; j++) {
        for (size_t i = 0; i < this->dim.v1; i++) {
          char *src = this->databuff.data() + (k*this->dim.v1*this->dim.v2 + j*this->dim.v1 + i)*this->pixsz;
          char *dest = newdata + ((k+size.v3)*newdim.v1*newdim.v2 + (j+size.v2)*newdim.v1 + (i+size.v1))*this->pixsz;
          memcpy(dest, src, this->pixsz*sizeof(char));
        }
      }
    }

    this->orig = {this->orig.v1 - size.v1 * this->pixspc.v1,
                  this->orig.v2 - size.v2 * this->pixspc.v2,
                  this->orig.v3 - size.v3 * this->pixspc.v3};

    this->dim = newdim;
    this->npix = newnpix;
    this->datasz = newdatasz;
    this->databuff.assign(newdatasz, newdata, true);
  }

  /**
   * @brief Crop pixel data
   */
  inline void crop()
  {
    mt_triple<unsigned int> from, to;
    mt_triple<size_t> newdim;
    bool have_nonzero = this->bounding_box(from, to);

    if(!have_nonzero) return;

    newdim.v1 = to.v1 - from.v1 + 1;
    newdim.v2 = to.v2 - from.v2 + 1;
    newdim.v3 = to.v3 - from.v3 + 1;
    size_t newnpix = newdim.v1*newdim.v2*newdim.v3;
    size_t newdatasz = newnpix * this->pixsz;
    char *newdata = new char[newdatasz];

    #ifdef OPENMP
    #pragma omp parallel for schedule(guided)
    #endif
    for (size_t k = 0; k < newdim.v3; k++) {
      for (size_t j = 0; j < newdim.v2; j++) {
        for (size_t i = 0; i < newdim.v1; i++) {
          char *src = this->databuff.data() + ((k+from.v3)*this->dim.v1*this->dim.v2 + (j+from.v2)*this->dim.v1 + (i+from.v1))*this->pixsz;
          char *dest = newdata + (k*newdim.v1*newdim.v2 + j*newdim.v1 + i)*this->pixsz;
          memcpy(dest, src, this->pixsz*sizeof(char));
        }
      }
    }

    this->orig = {this->orig.v1 + from.v1*this->pixspc.v1,
                  this->orig.v2 + from.v2*this->pixspc.v2,
                  this->orig.v3 + from.v3*this->pixspc.v3};
    this->dim = newdim;
    this->npix = newnpix;
    this->datasz = newdatasz;
    this->databuff.assign(newdatasz, newdata);
  }

  /**
   * @brief Crop pixel data
   */
  inline void crop(const mt_triple<size_t> & from, const mt_triple<size_t> & to)
  {
    mt_triple<size_t> newdim;
    newdim.v1 = to.v1 - from.v1 + 1;
    newdim.v2 = to.v2 - from.v2 + 1;
    newdim.v3 = to.v3 - from.v3 + 1;

    size_t newnpix = newdim.v1*newdim.v2*newdim.v3;
    size_t newdatasz = newnpix * this->pixsz;
    char *newdata = new char[newdatasz];

    #ifdef OPENMP
    #pragma omp parallel for schedule(guided)
    #endif
    for (size_t k = 0; k < newdim.v3; k++) {
      for (size_t j = 0; j < newdim.v2; j++) {
        for (size_t i = 0; i < newdim.v1; i++) {
          char *src = this->databuff.data() + ((k+from.v3)*this->dim.v1*this->dim.v2 +
                                               (j+from.v2)*this->dim.v1 + (i+from.v1))*this->pixsz;
          char *dest = newdata + (k*newdim.v1*newdim.v2 + j*newdim.v1 + i)*this->pixsz;
          memcpy(dest, src, this->pixsz*sizeof(char));
        }
      }
    }

    this->orig = {this->orig.v1 + from.v1*this->pixspc.v1,
                  this->orig.v2 + from.v2*this->pixspc.v2,
                  this->orig.v3 + from.v3*this->pixspc.v3};
    this->dim = newdim;
    this->npix = newnpix;
    this->datasz = newdatasz;
    this->databuff.assign(newdatasz, newdata);
  }

  inline void flip(unsigned int axes)
  {
    size_t ni, nj, nk;
    char *newdata = new char[this->datasz];
    for (size_t i = 0; i < this->dim.v1; i++)
    {
      for (size_t j = 0; j < this->dim.v2; j++)
      {
        for (size_t k = 0; k < this->dim.v3; k++)
        {
          char *src = this->databuff.data() + (k*this->dim.v1*this->dim.v2 + j*this->dim.v1 + i)*this->pixsz;
          ni = (((axes & ITK_AXIS_X) == ITK_AXIS_X) ? (this->dim.v1-i-1) : i);
          nj = (((axes & ITK_AXIS_Y) == ITK_AXIS_Y) ? (this->dim.v2-j-1) : j);
          nk = (((axes & ITK_AXIS_Z) == ITK_AXIS_Z) ? (this->dim.v3-k-1) : k);
          char *dest = newdata + (nk*this->dim.v1*this->dim.v2 + nj*this->dim.v1 + ni)*this->pixsz;
          memcpy(dest, src, this->pixsz*sizeof(char));
        }
      }
    }
    this->databuff.assign(datasz, newdata);
  }

  inline void scale(mt_real factor, bool from_ctr)
  {
    vec3r p = {orig.v1, orig.v2, orig.v3};

    if(from_ctr) {
      vec3r delta = {(dim.v1 * pixspc.v1)*0.5, (dim.v2 * pixspc.v2)*0.5, (dim.v3 * pixspc.v3)*0.5};
      // vec3r ctr = p + delta;
      // p = ctr + (p - ctr) * factor;
      // p = (p + delta) - delta * factor;
      p += delta * (1.0 - factor);
    } else {
      p *= factor;
    }

    orig = {p.x, p.y, p.z};
    pixspc.v1 *= factor;
    pixspc.v2 *= factor;
    pixspc.v3 *= factor;
  }

  inline void shift(const mt_triple<size_t> & offset)
  {
    size_t ni, nj, nk;
    char *newdata = new char[this->datasz];
    for (size_t i = 0; i < this->dim.v1; i++)
    {
      for (size_t j = 0; j < this->dim.v2; j++)
      {
        for (size_t k = 0; k < this->dim.v3; k++)
        {
          char *src = this->databuff.data() + (k*this->dim.v1*this->dim.v2 + j*this->dim.v1 + i)*this->pixsz;
          ni = (i + offset.v1) % this->dim.v1;
          nj = (j + offset.v2) % this->dim.v2;
          nk = (k + offset.v3) % this->dim.v3;
          char *dest = newdata + (nk*this->dim.v1*this->dim.v2 + nj*this->dim.v1 + ni)*this->pixsz;
          memcpy(dest, src, this->pixsz*sizeof(char));
        }
      }
    }
    this->databuff.assign(datasz, newdata);
  }

  void operator=(const itk_image & inp)
  {
    this->assign(inp);
  }

  const char* operator()(size_t i, size_t j, size_t k, unsigned int c = 0) const
  {
    const size_t cidx = ((k*this->dim.v1*this->dim.v2+j*this->dim.v1+i)*ncomp+c)*this->compsz;
    return this->databuff.data() + cidx;
  }

  char* operator()(size_t i, size_t j, size_t k, unsigned int c = 0)
  {
    const size_t cidx = ((k*this->dim.v1*this->dim.v2+j*this->dim.v1+i)*ncomp+c)*this->compsz;
    return this->databuff.data() + cidx;
  }
};

class itk_slice
{
public:
  explicit itk_slice(const itk_image& img)
    : img_(img), axis_(ITK_AXIS_X)
  {
    create_mesh_topology();
  }

  itk_slice(const itk_image& img, const ITK_AXES axis)
    : img_(img), axis_(axis)
  {
    create_mesh_topology();
  }

  virtual ~itk_slice() = default;

  inline void set_axis(const ITK_AXES axis)
  {
    if (axis_ != axis) {
      axis_ = axis;
      slice_.clear();
      create_mesh_topology();
    }
  }

  inline ITK_AXES get_axis() const
  {
    return axis_;
  }

  inline void update(mt_real pos, size_t c=0)
  {
    update_slice(pos, c);
  }

  inline const mt_meshdata& mesh() const
  {
    return slice_;
  }
  inline mt_meshdata& mesh()
  {
    return slice_;
  }

  inline const mt_vector<mt_real>& data() const
  {
    return data_;
  }

private:

  inline void create_mesh_topology()
  {
    if (axis_ == ITK_AXIS_X)
    {
      const size_t d0 = img_.dim.v2;
      const size_t d1 = img_.dim.v3;
      const size_t npnts = (d0+1)*(d1+1);
      size_t nelem = 2*d0*d1;

      slice_.e2n_dsp.resize(nelem);
      slice_.e2n_cnt.assign(nelem, 3);
      slice_.e2n_con.resize(3*nelem);
      slice_.etags.assign(nelem, mt_tag_t(0));
      slice_.etype.assign(nelem, Tri);
      slice_.xyz.resize(3*npnts);

      data_.resize(nelem);

      nelem = 0;
      for (size_t i=0; i<d0; i++)
      {
        for (size_t j=0; j<d1; j++, nelem+=2)
        {
          const size_t pidx0 = j*(d0+1)+i;
          const size_t pidx1 = j*(d0+1)+(i+1);
          const size_t pidx2 = (j+1)*(d0+1)+(i+1);
          const size_t pidx3 = (j+1)*(d0+1)+i;

          slice_.xyz[3*pidx0+0] = img_.orig.v1;
          slice_.xyz[3*pidx0+1] = img_.orig.v2 + i*img_.pixspc.v2;
          slice_.xyz[3*pidx0+2] = img_.orig.v3 + j*img_.pixspc.v3;

          slice_.xyz[3*pidx1+0] = img_.orig.v1;
          slice_.xyz[3*pidx1+1] = img_.orig.v2 + (i+1)*img_.pixspc.v2;
          slice_.xyz[3*pidx1+2] = img_.orig.v3 + j*img_.pixspc.v3;

          slice_.xyz[3*pidx2+0] = img_.orig.v1;
          slice_.xyz[3*pidx2+1] = img_.orig.v2 + (i+1)*img_.pixspc.v2;
          slice_.xyz[3*pidx2+2] = img_.orig.v3 + (j+1)*img_.pixspc.v3;

          slice_.xyz[3*pidx3+0] = img_.orig.v1;
          slice_.xyz[3*pidx3+1] = img_.orig.v2 + i*img_.pixspc.v2;
          slice_.xyz[3*pidx3+2] = img_.orig.v3 + (j+1)*img_.pixspc.v3;

          slice_.e2n_dsp[nelem+0] = 3*nelem;
          slice_.e2n_dsp[nelem+1] = 3*(nelem+1);

          slice_.e2n_con[3*nelem+0] = pidx0;
          slice_.e2n_con[3*nelem+1] = pidx1;
          slice_.e2n_con[3*nelem+2] = pidx2;

          slice_.e2n_con[3*(nelem+1)+0] = pidx0;
          slice_.e2n_con[3*(nelem+1)+1] = pidx2;
          slice_.e2n_con[3*(nelem+1)+2] = pidx3;
        }
      }
    }
    else if (axis_ == ITK_AXIS_Y)
    {
      const size_t d0 = img_.dim.v1;
      const size_t d1 = img_.dim.v3;
      const size_t npnts = (d0+1)*(d1+1);
      size_t nelem = 2*d0*d1;

      slice_.e2n_dsp.resize(nelem);
      slice_.e2n_cnt.assign(nelem, 3);
      slice_.e2n_con.resize(3*nelem);
      slice_.etags.assign(nelem, mt_tag_t(0));
      slice_.etype.assign(nelem, Tri);
      slice_.xyz.resize(3*npnts);

      data_.resize(nelem);

      nelem = 0;
      for (size_t i=0; i<d0; i++)
      {
        for (size_t j=0; j<d1; j++, nelem+=2)
        {
          const size_t pidx0 = j*(d0+1)+i;
          const size_t pidx1 = j*(d0+1)+(i+1);
          const size_t pidx2 = (j+1)*(d0+1)+(i+1);
          const size_t pidx3 = (j+1)*(d0+1)+i;

          slice_.xyz[3*pidx0+0] = img_.orig.v1 + i*img_.pixspc.v1;
          slice_.xyz[3*pidx0+1] = img_.orig.v2;
          slice_.xyz[3*pidx0+2] = img_.orig.v3 + j*img_.pixspc.v3;

          slice_.xyz[3*pidx1+0] = img_.orig.v1 + (i+1)*img_.pixspc.v1;
          slice_.xyz[3*pidx1+1] = img_.orig.v2;
          slice_.xyz[3*pidx1+2] = img_.orig.v3 + j*img_.pixspc.v3;

          slice_.xyz[3*pidx2+0] = img_.orig.v1 + (i+1)*img_.pixspc.v1;
          slice_.xyz[3*pidx2+1] = img_.orig.v2;
          slice_.xyz[3*pidx2+2] = img_.orig.v3 + (j+1)*img_.pixspc.v3;

          slice_.xyz[3*pidx3+0] = img_.orig.v1 + i*img_.pixspc.v1;
          slice_.xyz[3*pidx3+1] = img_.orig.v2;
          slice_.xyz[3*pidx3+2] = img_.orig.v3 + (j+1)*img_.pixspc.v3;

          slice_.e2n_dsp[nelem+0] = 3*nelem;
          slice_.e2n_dsp[nelem+1] = 3*(nelem+1);

          slice_.e2n_con[3*nelem+0] = pidx0;
          slice_.e2n_con[3*nelem+1] = pidx1;
          slice_.e2n_con[3*nelem+2] = pidx2;

          slice_.e2n_con[3*(nelem+1)+0] = pidx0;
          slice_.e2n_con[3*(nelem+1)+1] = pidx2;
          slice_.e2n_con[3*(nelem+1)+2] = pidx3;
        }
      }
    }
    else if (axis_ == ITK_AXIS_Z)
    {
      const size_t d0 = img_.dim.v1;
      const size_t d1 = img_.dim.v2;
      const size_t npnts = (d0+1)*(d1+1);
      size_t nelem = 2*d0*d1;

      slice_.e2n_dsp.resize(nelem);
      slice_.e2n_cnt.assign(nelem, 3);
      slice_.e2n_con.resize(3*nelem);
      slice_.etags.assign(nelem, mt_tag_t(0));
      slice_.etype.assign(nelem, Tri);
      slice_.xyz.resize(3*npnts);

      data_.resize(nelem);

      nelem = 0;
      for (size_t i=0; i<d0; i++)
      {
        for (size_t j=0; j<d1; j++, nelem+=2)
        {
          const size_t pidx0 = j*(d0+1)+i;
          const size_t pidx1 = j*(d0+1)+(i+1);
          const size_t pidx2 = (j+1)*(d0+1)+(i+1);
          const size_t pidx3 = (j+1)*(d0+1)+i;

          slice_.xyz[3*pidx0+0] = img_.orig.v1 + i*img_.pixspc.v1;
          slice_.xyz[3*pidx0+1] = img_.orig.v2 + j*img_.pixspc.v2;
          slice_.xyz[3*pidx0+2] = img_.orig.v3;

          slice_.xyz[3*pidx1+0] = img_.orig.v1 + (i+1)*img_.pixspc.v1;
          slice_.xyz[3*pidx1+1] = img_.orig.v2 + j*img_.pixspc.v2;
          slice_.xyz[3*pidx1+2] = img_.orig.v3;

          slice_.xyz[3*pidx2+0] = img_.orig.v1 + (i+1)*img_.pixspc.v1;
          slice_.xyz[3*pidx2+1] = img_.orig.v2 + (j+1)*img_.pixspc.v2;
          slice_.xyz[3*pidx2+2] = img_.orig.v3;

          slice_.xyz[3*pidx3+0] = img_.orig.v1 + i*img_.pixspc.v1;
          slice_.xyz[3*pidx3+1] = img_.orig.v2 + (j+1)*img_.pixspc.v2;
          slice_.xyz[3*pidx3+2] = img_.orig.v3;

          slice_.e2n_dsp[nelem+0] = 3*nelem;
          slice_.e2n_dsp[nelem+1] = 3*(nelem+1);

          slice_.e2n_con[3*nelem+0] = pidx0;
          slice_.e2n_con[3*nelem+1] = pidx1;
          slice_.e2n_con[3*nelem+2] = pidx2;

          slice_.e2n_con[3*(nelem+1)+0] = pidx0;
          slice_.e2n_con[3*(nelem+1)+1] = pidx2;
          slice_.e2n_con[3*(nelem+1)+2] = pidx3;
        }
      }
    }
  }

  inline void update_slice(mt_real pos, size_t c=0)
  {
    if (pos < 0.0) pos = 0.0;
    if (pos > 1.0) pos = 1.0;
    if (c >= img_.ncomp) c = 0;
    if (axis_ == ITK_AXIS_X)
    {
      const size_t d0 = img_.dim.v2; // Y-component (j)
      const size_t d1 = img_.dim.v3; // Z-component (k)
      const mt_real pos_x = img_.orig.v1 + pos*img_.dim.v1*img_.pixspc.v1;
      size_t dx = static_cast<size_t>(std::floor(pos*img_.dim.v1)); // X-component (i)
      if (dx >= img_.dim.v1) dx = img_.dim.v1-1;
      mt_real val = 0.0;
      size_t nelem = 0;
      const char* img_data = img_.databuff.data();
      for (size_t i=0; i<d0; i++)
      {
        for (size_t j=0; j<d1; j++, nelem+=2)
        {
          // update mesh position
          const size_t pidx0 = j*(d0+1)+i;
          const size_t pidx1 = j*(d0+1)+(i+1);
          const size_t pidx2 = (j+1)*(d0+1)+(i+1);
          const size_t pidx3 = (j+1)*(d0+1)+i;
          slice_.xyz[3*pidx0+0] = pos_x;
          slice_.xyz[3*pidx1+0] = pos_x;
          slice_.xyz[3*pidx2+0] = pos_x;
          slice_.xyz[3*pidx3+0] = pos_x;
          // update slice values
          //const size_t cidx = ((k*this->dim.v1*this->dim.v2+j*this->dim.v1+i)*ncomp+c)*this->compsz;
          const size_t cidx = ((j*img_.dim.v1*d0+i*img_.dim.v1+dx)*img_.ncomp+c)*img_.compsz;
          val = 0.0;
          itk_comp_as_mt_real(&img_data[cidx], img_.comptypeid, val);
          data_[nelem] = data_[nelem+1] = val;
        }
      }
    }
    else if (axis_ == ITK_AXIS_Y)
    {
      const size_t d0 = img_.dim.v1; // X-component (i)
      const size_t d1 = img_.dim.v3; // Z-component (k)
      const mt_real pos_y = img_.orig.v2 + pos*img_.dim.v2*img_.pixspc.v2;
      size_t dy = static_cast<size_t>(std::floor(pos*img_.dim.v2)); // Y-component (j)
      if (dy >= img_.dim.v2) dy = img_.dim.v2-1;
      mt_real val = 0.0;
      size_t nelem = 0;
      const char* img_data = img_.databuff.data();
      for (size_t i=0; i<d0; i++)
      {
        for (size_t j=0; j<d1; j++, nelem+=2)
        {
          // update mesh position
          const size_t pidx0 = j*(d0+1)+i;
          const size_t pidx1 = j*(d0+1)+(i+1);
          const size_t pidx2 = (j+1)*(d0+1)+(i+1);
          const size_t pidx3 = (j+1)*(d0+1)+i;
          slice_.xyz[3*pidx0+1] = pos_y;
          slice_.xyz[3*pidx1+1] = pos_y;
          slice_.xyz[3*pidx2+1] = pos_y;
          slice_.xyz[3*pidx3+1] = pos_y;
          // update slice values
          //const size_t cidx = ((k*this->dim.v1*this->dim.v2+j*this->dim.v1+i)*ncomp+c)*this->compsz;
          const size_t cidx = ((j*d0*img_.dim.v2+dy*d0+i)*img_.ncomp+c)*img_.compsz;
          val = 0.0;
          itk_comp_as_mt_real(&img_data[cidx], img_.comptypeid, val);
          data_[nelem] = data_[nelem+1] = val;
        }
      }
    }
    else if (axis_ == ITK_AXIS_Z)
    {
      const size_t d0 = img_.dim.v1; // X-component (i)
      const size_t d1 = img_.dim.v2; // Y-component (j)
      const mt_real pos_z = img_.orig.v3 + pos*img_.dim.v3*img_.pixspc.v3;
      size_t dz = static_cast<size_t>(std::floor(pos*img_.dim.v3)); // Z-component (k)
      if (dz >= img_.dim.v3) dz = img_.dim.v3-1;
      mt_real val = 0.0;
      size_t nelem = 0;
      const char* img_data = img_.databuff.data();
      for (size_t i=0; i<d0; i++)
      {
        for (size_t j=0; j<d1; j++, nelem+=2)
        {
          // update mesh position
          const size_t pidx0 = j*(d0+1)+i;
          const size_t pidx1 = j*(d0+1)+(i+1);
          const size_t pidx2 = (j+1)*(d0+1)+(i+1);
          const size_t pidx3 = (j+1)*(d0+1)+i;
          slice_.xyz[3*pidx0+2] = pos_z;
          slice_.xyz[3*pidx1+2] = pos_z;
          slice_.xyz[3*pidx2+2] = pos_z;
          slice_.xyz[3*pidx3+2] = pos_z;
          // update slice values
          //const size_t cidx = ((k*this->dim.v1*this->dim.v2+j*this->dim.v1+i)*ncomp+c)*this->compsz;
          const size_t cidx = ((dz*d0*d1+j*d0+i)*img_.ncomp+c)*img_.compsz;
          val = 0.0;
          itk_comp_as_mt_real(&img_data[cidx], img_.comptypeid, val);
          data_[nelem] = data_[nelem+1] = val;
        }
      }
    }
  }

private:
  const itk_image&    img_;
  ITK_AXES           axis_;
  mt_meshdata       slice_;
  mt_vector<mt_real> data_;
};

inline void get_spc_fct(const itk_image & img, mt_triple<float> & fct)
{
  fct.v1 = img.pixspc.v1 / img.pixspc.v1;
  fct.v2 = img.pixspc.v1 / img.pixspc.v2;
  fct.v3 = img.pixspc.v1 / img.pixspc.v3;
}

inline double pix_distance(const mt_triple<double> & pixspc,
                           unsigned int i1, unsigned int j1, unsigned int k1,
                           unsigned int i2, unsigned int j2, unsigned int k2)
{
  double len_x = i1 * pixspc.v1 - i2 * pixspc.v1;
  double len_y = j1 * pixspc.v2 - j2 * pixspc.v2;
  double len_z = k1 * pixspc.v3 - k2 * pixspc.v3;

  return sqrt(len_x*len_x + len_y*len_y + len_z*len_z);
}

/**
* @brief Class used for accessing itk image data.
*
* @tparam T image data type.
*/
template<typename T>
class itk_access
{
  private:
  itk_image & _img;

  public:
  mt_triple<size_t> & dim;
  mt_triple<double> & pixspc;
  mt_triple<float> spc_fct;

  /// constructor
  itk_access(itk_image & img) : _img(img), dim(_img.dim), pixspc(_img.pixspc)
  {
    get_spc_fct(this->_img, this->spc_fct);
  }

  /**
  * @brief Data access function
  *
  * @param [in] i    Discrete coordinate in x.
  * @param [in] j    Discrete coordinate in y.
  * @param [in] k    Discrete coordinate in z.
  * @param [in] c    Optional component index.
  *
  * @return Reference to data value.
  */
  inline T & operator()(unsigned int i, unsigned int j, unsigned int k, unsigned int c = 0)
  {
    T* data = (T*) this->data_at(i, j, k);
    return data[c];
  }

  /**
  * @brief Data access function
  *
  * @param [in] i    Discrete coordinate in x.
  * @param [in] j    Discrete coordinate in y.
  * @param [in] k    Discrete coordinate in z.
  * @param [in] c    Optional component index.
  *
  * @return Reference to data value.
  */
  inline char* data_at(size_t i, size_t j, size_t k)
  {
    char* data = _img.databuff.data();
    return (data + (k * dim.v1 * dim.v2 + j * dim.v1 + i) * _img.pixsz);
  }

  /**
  * @brief Compute physical distance for two pixels.
  *
  * @param i1 Pixel 1, discrete coordinate in x.
  * @param j1 Pixel 1, discrete coordinate in y.
  * @param k1 Pixel 1, discrete coordinate in z.
  * @param i2 Pixel 2, discrete coordinate in x.
  * @param j2 Pixel 2, discrete coordinate in y.
  * @param k2 Pixel 2, discrete coordinate in z.
  *
  * @return Distance between the pixels.
  */
  inline double distance(unsigned int i1, unsigned int j1, unsigned int k1,
                         unsigned int i2, unsigned int j2, unsigned int k2)
  {
    return pix_distance(pixspc, i1, j1, k1, i2, j2, k2);
  }

  inline bool is_in_ellipsoid(unsigned int i1, unsigned int j1, unsigned int k1,
                       unsigned int i2, unsigned int j2, unsigned int k2,
                       double rada, double radb, double radc)
  {
    double len_x = (i1 * pixspc.v1 - i2 * pixspc.v1)/rada;
    double len_y = (j1 * pixspc.v2 - j2 * pixspc.v2)/radb;
    double len_z = (k1 * pixspc.v3 - k2 * pixspc.v3)/radc;
    return ((len_x*len_x + len_y*len_y + len_z*len_z) < 1.0);
  }


  template<class V>
  inline mt_point<V> position(unsigned int i, unsigned int j, unsigned int k)
  {
    mt_point<V> pos;
    pos.x = _img.orig.v1 + i * pixspc.v1 + 0.5*pixspc.v1;
    pos.y = _img.orig.v2 + j * pixspc.v2 + 0.5*pixspc.v2;
    pos.z = _img.orig.v3 + k * pixspc.v3 + 0.5*pixspc.v3;

    return pos;
  }

  template<class V>
  inline vec3i pixel(mt_point<V> pt)
  {
    mt_real i = ((pt.x - _img.orig.v1 - 0.5*pixspc.v1) / pixspc.v1)+0.5;
    mt_real j = ((pt.y - _img.orig.v2 - 0.5*pixspc.v2) / pixspc.v2)+0.5;
    mt_real k = ((pt.z - _img.orig.v3 - 0.5*pixspc.v3) / pixspc.v3)+0.5;

    vec3i ijk = {(mt_idx_t)i, (mt_idx_t)j, (mt_idx_t)k};
    return ijk;
  }
};

/**
* @brief Refine the itk image data.
*
* @param scale Each pixel is refined into "scale" many pixels in each dimension.
*/
inline void resample_image(itk_image & img, const mt_triple<float> & scale)
{
  itk_image old_img; old_img.assign(img);
  // we dont care about the data type
  itk_access<float> oi(old_img);
  mt_triple<float> sc;
  sc.v1 = fabs(scale.v1);
  sc.v2 = fabs(scale.v2);
  sc.v3 = fabs(scale.v3);

  img.dim.v1 = float(img.dim.v1) * sc.v1;
  img.dim.v2 = float(img.dim.v2) * sc.v2;
  img.dim.v3 = float(img.dim.v3) * sc.v3;

  img.pixspc.v1 /= sc.v1;
  img.pixspc.v2 /= sc.v2;
  img.pixspc.v3 /= sc.v3;

  img.npix = img.dim.v1 * img.dim.v2 * img.dim.v3;
  img.datasz = img.npix * img.pixsz;
  img.databuff.resize(img.datasz);

  // we dont care about the data type
  itk_access<short> ni(img);

  for(size_t i = 0; i < img.dim.v1; i++)
    for(size_t j = 0; j < img.dim.v2; j++)
      for(size_t k = 0; k < img.dim.v3; k++)
      {
        vec3r pos = ni.position<mt_real>(i,j,k);
        vec3i pix = oi.pixel<mt_real>(pos);
        char* input  = oi.data_at(pix.x, pix.y, pix.z);
        char* output = ni.data_at(i, j, k);
        memcpy(output, input, img.pixsz);
      }
}



/**
* @brief Check if pixel has any non-zero data in a certain radius
*
* @tparam T Image data type.
*
* @param [in] data Data access.
* @param [in] rad  Radius around pixel we check for.
* @param [in] i    Discrete coordinate in x.
* @param [in] j    Discrete coordinate in y.
* @param [in] k    Discrete coordinate in z.
*
* @return Whether there is non-zero data.
*/
template<typename T>
inline bool pix_has_data(itk_access<T> & data, mt_triple<short> rad, unsigned i, unsigned j, unsigned k)
{
  unsigned int rad_i = rad.v1 * data.spc_fct.v1;
  unsigned int rad_j = rad.v2 * data.spc_fct.v2;
  unsigned int rad_k = rad.v3 * data.spc_fct.v3;
  const unsigned int i_start = int(i - rad_i) > 0 ? i - rad_i : 0;
  const unsigned int j_start = int(j - rad_j) > 0 ? j - rad_j : 0;
  const unsigned int k_start = int(k - rad_k) > 0 ? k - rad_k : 0;
  rad_i += 1, rad_j += 1, rad_k += 1;
  const unsigned int i_end = i + rad_i < data.dim.v1 ? i + rad_i : data.dim.v1;
  const unsigned int j_end = j + rad_j < data.dim.v2 ? j + rad_j : data.dim.v2;
  const unsigned int k_end = k + rad_k < data.dim.v3 ? k + rad_k : data.dim.v3;

  for(unsigned int i2 = i_start; i2 < i_end; i2++)
    for(unsigned int j2 = j_start; j2 < j_end; j2++)
      for(unsigned int k2 = k_start; k2 < k_end; k2++)
        if(data(i2, j2, k2) != T(0))
          return true;

  return false;
}
inline bool pix_has_data(itk_image & img, mt_triple<short> rad, unsigned i, unsigned j, unsigned k)
{

  mt_triple<float> spc_fct;
  get_spc_fct(img, spc_fct);

  unsigned int rad_i = rad.v1 * spc_fct.v1;
  unsigned int rad_j = rad.v2 * spc_fct.v2;
  unsigned int rad_k = rad.v3 * spc_fct.v3;

  const unsigned int i_start = int(i - rad_i) > 0 ? i - rad_i : 0;
  const unsigned int j_start = int(j - rad_j) > 0 ? j - rad_j : 0;
  const unsigned int k_start = int(k - rad_k) > 0 ? k - rad_k : 0;
  rad_i += 1, rad_j += 1, rad_k += 1;

  const unsigned int i_end = i + rad_i < img.dim.v1 ? i + rad_i : img.dim.v1;
  const unsigned int j_end = j + rad_j < img.dim.v2 ? j + rad_j : img.dim.v2;
  const unsigned int k_end = k + rad_k < img.dim.v3 ? k + rad_k : img.dim.v3;

  for(unsigned int i2 = i_start; i2 < i_end; i2++)
    for(unsigned int j2 = j_start; j2 < j_end; j2++)
      for(unsigned int k2 = k_start; k2 < k_end; k2++)
        if(itk_comp_nonzero(img(i2,j2,k2), img.comptypeid))
          return true;

  return false;
}

inline bool pix_has_value_in_rad(itk_image & img, mt_triple<short> rad, unsigned i, unsigned j, unsigned k, int val)
{
  unsigned int rad_i = rad.v1;
  unsigned int rad_j = rad.v2;
  unsigned int rad_k = rad.v3;

  const unsigned int i_start = i > rad_i ? i - rad_i : 0;
  const unsigned int j_start = j > rad_j ? j - rad_j : 0;
  const unsigned int k_start = k > rad_k ? k - rad_k : 0;
  rad_i += 1, rad_j += 1, rad_k += 1;

  const unsigned int i_end = i + rad_i < img.dim.v1 ? i + rad_i : img.dim.v1;
  const unsigned int j_end = j + rad_j < img.dim.v2 ? j + rad_j : img.dim.v2;
  const unsigned int k_end = k + rad_k < img.dim.v3 ? k + rad_k : img.dim.v3;

  for(unsigned int i2 = i_start; i2 < i_end; i2++)
    for(unsigned int j2 = j_start; j2 < j_end; j2++)
      for(unsigned int k2 = k_start; k2 < k_end; k2++) {
        int rd_val = 0;
        itk_comp_as<int>(img(i2,j2,k2), img.comptypeid, rd_val);

        if(rd_val == val)
          return true;
      }

  return false;
}

inline bool pix_has_bdry_in_rad(itk_image & img, mt_triple<short> rad, unsigned i, unsigned j, unsigned k,
                                const MT_USET<int> & bdry)
{
  unsigned int rad_i = rad.v1;
  unsigned int rad_j = rad.v2;
  unsigned int rad_k = rad.v3;

  const unsigned int i_start = i > rad_i ? i - rad_i : 0;
  const unsigned int j_start = j > rad_j ? j - rad_j : 0;
  const unsigned int k_start = k > rad_k ? k - rad_k : 0;
  rad_i += 1, rad_j += 1, rad_k += 1;

  const unsigned int i_end = i + rad_i < img.dim.v1 ? i + rad_i : img.dim.v1;
  const unsigned int j_end = j + rad_j < img.dim.v2 ? j + rad_j : img.dim.v2;
  const unsigned int k_end = k + rad_k < img.dim.v3 ? k + rad_k : img.dim.v3;

  for(unsigned int i2 = i_start; i2 < i_end; i2++)
    for(unsigned int j2 = j_start; j2 < j_end; j2++)
      for(unsigned int k2 = k_start; k2 < k_end; k2++) {
        int rd_val = 0;
        itk_comp_as<int>(img(i2,j2,k2), img.comptypeid, rd_val);

        if(bdry.count(rd_val))
          return true;
      }

  return false;
}

inline bool pix_has_value_in_rad(itk_image & img, short rad, unsigned i, unsigned j, unsigned k, int val)
{
  unsigned int rad_i = rad;
  unsigned int rad_j = rad;
  unsigned int rad_k = rad;

  double spc_rad = ((img.pixspc.v1 + img.pixspc.v2 + img.pixspc.v3) / 3.0) * rad;

  const unsigned int i_start = i > rad_i ? i - rad_i : 0;
  const unsigned int j_start = j > rad_j ? j - rad_j : 0;
  const unsigned int k_start = k > rad_k ? k - rad_k : 0;
  rad_i += 1, rad_j += 1, rad_k += 1;

  const unsigned int i_end = i + rad_i < img.dim.v1 ? i + rad_i : img.dim.v1;
  const unsigned int j_end = j + rad_j < img.dim.v2 ? j + rad_j : img.dim.v2;
  const unsigned int k_end = k + rad_k < img.dim.v3 ? k + rad_k : img.dim.v3;

  for(unsigned int i2 = i_start; i2 < i_end; i2++)
    for(unsigned int j2 = j_start; j2 < j_end; j2++)
      for(unsigned int k2 = k_start; k2 < k_end; k2++) {
        double dist = pix_distance(img.pixspc, i, j, k, i2, j2, k2);

        if(rad == 1 || dist <= spc_rad) {
          int rd_val = 0;
          itk_comp_as<int>(img(i2,j2,k2), img.comptypeid, rd_val);

          if(rd_val == val)
            return true;
        }
      }

  return false;
}

inline bool pix_has_bdry_in_rad(itk_image & img, short rad, unsigned i, unsigned j, unsigned k,
                                const MT_USET<int> & bdry)
{
  unsigned int rad_i = rad;
  unsigned int rad_j = rad;
  unsigned int rad_k = rad;

  double spc_rad = ((img.pixspc.v1 + img.pixspc.v2 + img.pixspc.v3) / 3.0) * rad;

  const unsigned int i_start = i > rad_i ? i - rad_i : 0;
  const unsigned int j_start = j > rad_j ? j - rad_j : 0;
  const unsigned int k_start = k > rad_k ? k - rad_k : 0;
  rad_i += 1, rad_j += 1, rad_k += 1;

  const unsigned int i_end = i + rad_i < img.dim.v1 ? i + rad_i : img.dim.v1;
  const unsigned int j_end = j + rad_j < img.dim.v2 ? j + rad_j : img.dim.v2;
  const unsigned int k_end = k + rad_k < img.dim.v3 ? k + rad_k : img.dim.v3;

  for(unsigned int i2 = i_start; i2 < i_end; i2++)
    for(unsigned int j2 = j_start; j2 < j_end; j2++)
      for(unsigned int k2 = k_start; k2 < k_end; k2++) {
        double dist = pix_distance(img.pixspc, i, j, k, i2, j2, k2);

        if(rad == 1 || dist <= spc_rad) {
          int rd_val = 0;
          itk_comp_as<int>(img(i2,j2,k2), img.comptypeid, rd_val);

          if(bdry.count(rd_val))
            return true;
        }
      }

  return false;
}

inline void
pix_select_in_rad(itk_image & img,
                  mt_triple<short> rad,
                  unsigned i, unsigned j, unsigned k,
                  const MT_USET<int> & bdry,
                  MT_USET<mt_triple<unsigned>> & sel)
{
  unsigned int rad_i = rad.v1;
  unsigned int rad_j = rad.v2;
  unsigned int rad_k = rad.v3;

  const unsigned int i_start = i > rad_i ? i - rad_i : 0;
  const unsigned int j_start = j > rad_j ? j - rad_j : 0;
  const unsigned int k_start = k > rad_k ? k - rad_k : 0;
  rad_i += 1, rad_j += 1, rad_k += 1;

  const unsigned int i_end = i + rad_i < img.dim.v1 ? i + rad_i : img.dim.v1;
  const unsigned int j_end = j + rad_j < img.dim.v2 ? j + rad_j : img.dim.v2;
  const unsigned int k_end = k + rad_k < img.dim.v3 ? k + rad_k : img.dim.v3;

  for(unsigned int i2 = i_start; i2 < i_end; i2++)
    for(unsigned int j2 = j_start; j2 < j_end; j2++)
      for(unsigned int k2 = k_start; k2 < k_end; k2++) {
        int rd_val = 0;
        itk_comp_as<int>(img(i2,j2,k2), img.comptypeid, rd_val);

        if(bdry.count(rd_val))
          sel.insert({i2,j2,k2});
      }
}

inline void
pix_set_in_rad(itk_image & img,
               short rad,
               unsigned i, unsigned j, unsigned k,
               const MT_USET<int> & bdry,
               int val)
{
  unsigned int rad_i = rad;
  unsigned int rad_j = rad;
  unsigned int rad_k = rad;

  double spc_rad = ((img.pixspc.v1 + img.pixspc.v2 + img.pixspc.v3) / 3.0) * rad;

  const unsigned int i_start = i > rad_i ? i - rad_i : 0;
  const unsigned int j_start = j > rad_j ? j - rad_j : 0;
  const unsigned int k_start = k > rad_k ? k - rad_k : 0;
  rad_i += 1, rad_j += 1, rad_k += 1;

  const unsigned int i_end = i + rad_i < img.dim.v1 ? i + rad_i : img.dim.v1;
  const unsigned int j_end = j + rad_j < img.dim.v2 ? j + rad_j : img.dim.v2;
  const unsigned int k_end = k + rad_k < img.dim.v3 ? k + rad_k : img.dim.v3;

  for(unsigned int i2 = i_start; i2 < i_end; i2++)
    for(unsigned int j2 = j_start; j2 < j_end; j2++)
      for(unsigned int k2 = k_start; k2 < k_end; k2++) {
        int rd_val = 0;
        itk_comp_as<int>(img(i2,j2,k2), img.comptypeid, rd_val);
        double dist = pix_distance(img.pixspc, i, j, k, i2, j2, k2);

        if(bdry.count(rd_val) && (rad == 1 || dist <= spc_rad))
          itk_comp_from(img(i2, j2, k2), img.comptypeid, val);
      }
}


template <typename T>
inline bool pix_has_data(itk_access<T> & data,  unsigned i, unsigned j, unsigned k)
{
  return (data(i,j,k) != T(0));
}

inline bool pix_has_data(itk_image & img, unsigned i, unsigned j, unsigned k)
{
  return itk_comp_nonzero(img(i,j,k), img.comptypeid);
}


/**
* @brief Check if pixel data in a certain radius consists only of non-zero vals
*
* @tparam T Image data type.
*
* @param [in] data Data access.
* @param [in] rad  Radius around pixel we check for.
* @param [in] i    Discrete coordinate in x.
* @param [in] j    Discrete coordinate in y.
* @param [in] k    Discrete coordinate in z.
*
* @return Whether there is non-zero data.
*/
template<typename T>
inline bool pix_full_data(itk_access<T> & data, mt_triple<short> rad, unsigned i, unsigned j, unsigned k)
{
  unsigned int rad_i = rad.v1 * data.spc_fct.v1;
  unsigned int rad_j = rad.v2 * data.spc_fct.v2;
  unsigned int rad_k = rad.v3 * data.spc_fct.v3;
  const unsigned int i_start = int(i - rad_i) > 0 ? i - rad_i : 0;
  const unsigned int j_start = int(j - rad_j) > 0 ? j - rad_j : 0;
  const unsigned int k_start = int(k - rad_k) > 0 ? k - rad_k : 0;
  rad_i += 1, rad_j += 1, rad_k += 1;
  const unsigned int i_end = i + rad_i < data.dim.v1 ? i + rad_i : data.dim.v1;
  const unsigned int j_end = j + rad_j < data.dim.v2 ? j + rad_j : data.dim.v2;
  const unsigned int k_end = k + rad_k < data.dim.v3 ? k + rad_k : data.dim.v3;

  for(unsigned int i2 = i_start; i2 < i_end; i2++)
    for(unsigned int j2 = j_start; j2 < j_end; j2++)
      for(unsigned int k2 = k_start; k2 < k_end; k2++)
        if(data(i2, j2, k2) == T(0))
          return false;

  return true;
}
inline bool pix_full_data(itk_image & img, mt_triple<short> rad, unsigned i, unsigned j, unsigned k)
{
  mt_triple<float> spc_fct;
  get_spc_fct(img, spc_fct);

  unsigned int rad_i = rad.v1 * spc_fct.v1;
  unsigned int rad_j = rad.v2 * spc_fct.v2;
  unsigned int rad_k = rad.v3 * spc_fct.v3;

  const unsigned int i_start = int(i - rad_i) > 0 ? i - rad_i : 0;
  const unsigned int j_start = int(j - rad_j) > 0 ? j - rad_j : 0;
  const unsigned int k_start = int(k - rad_k) > 0 ? k - rad_k : 0;
  rad_i += 1, rad_j += 1, rad_k += 1;

  const unsigned int i_end = i + rad_i < img.dim.v1 ? i + rad_i : img.dim.v1;
  const unsigned int j_end = j + rad_j < img.dim.v2 ? j + rad_j : img.dim.v2;
  const unsigned int k_end = k + rad_k < img.dim.v3 ? k + rad_k : img.dim.v3;

  for(unsigned int i2 = i_start; i2 < i_end; i2++)
    for(unsigned int j2 = j_start; j2 < j_end; j2++)
      for(unsigned int k2 = k_start; k2 < k_end; k2++)
        if(!itk_comp_nonzero(img(i2,j2,k2), img.comptypeid))
          return false;

  return true;
}


/**
* @brief Smooth (average) the pixel data, taking a certain radius into account.
*
* @tparam T Image data type.
*
* @param [in] data Data access.
* @param [in] rad  Radius around pixel.
* @param [in] smth Smoothing coefficient.
* @param [in] i    Discrete coordinate in x.
* @param [in] j    Discrete coordinate in y.
* @param [in] k    Discrete coordinate in z.
*
* @return
*/
template<typename T>
inline void pix_smooth(itk_access<T> & data, mt_triple<short> rad, float smth, unsigned i, unsigned j, unsigned k)
{
  unsigned int numadd = 0;
  T avrg = 0.0f;
  T curr = data(i,j,k);

  unsigned int rad_i = rad.v1 * data.spc_fct.v1;
  unsigned int rad_j = rad.v2 * data.spc_fct.v2;
  unsigned int rad_k = rad.v3 * data.spc_fct.v3;
  const unsigned int i_start = int(i - rad_i) > 0 ? i - rad_i : 0;
  const unsigned int j_start = int(j - rad_j) > 0 ? j - rad_j : 0;
  const unsigned int k_start = int(k - rad_k) > 0 ? k - rad_k : 0;
  rad_i += 1, rad_j += 1, rad_k += 1;
  const unsigned int i_end = i + rad_i < data.dim.v1 ? i + rad_i : data.dim.v1;
  const unsigned int j_end = j + rad_j < data.dim.v2 ? j + rad_j : data.dim.v2;
  const unsigned int k_end = k + rad_k < data.dim.v3 ? k + rad_k : data.dim.v3;

  #ifdef ITKSMOOTH_CHECK_RAD
  mt_triple<double> space_rad = {rad_i*data.pixspc.v1, rad_j*data.pixspc.v2, rad_k*data.pixspc.v3};
  #endif

  for(unsigned int k2 = k_start; k2 < k_end; k2++)
    for(unsigned int j2 = j_start; j2 < j_end; j2++)
      for(unsigned int i2 = i_start; i2 < i_end; i2++)
      {
        #ifdef ITKSMOOTH_CHECK_RAD
        double len_x = (i*data.pixspc.v1 - i2*data.pixspc.v1)/space_rad.v1;
        double len_y = (j*data.pixspc.v2 - j2*data.pixspc.v2)/space_rad.v2;
        double len_z = (k*data.pixspc.v3 - k2*data.pixspc.v3)/space_rad.v3;
        if((len_x*len_x + len_y*len_y + len_z*len_z) < 1.0)
        #endif
        {
          avrg += data(i2,j2,k2);
          numadd++;
        }
      }

  avrg /= numadd;
  data(i,j,k) = curr + (avrg - curr)*smth;
}

/**
* @brief Smooth an itk image.
*
* The data has to be of float type.
*
* @param [in, out] img  ITK image.
* @param [in]      rad  Smoothing radius.
* @param [in]      smth Smoothing coefficent.
* @param [in]      iter Number of iterations.
*/
inline void smooth_itk(itk_image & img, mt_triple<short> rad, float smth, unsigned int iter)
{
  itk_access<float> data(img);
  mt_vector<mt_triple<unsigned int>> smth_list;
  smth_list.reserve(img.dim.v1 * img.dim.v2 * img.dim.v3 * 0.25);

  for(unsigned int k = 0; k < img.dim.v3; k++)
    for(unsigned int j = 0; j < img.dim.v2; j++)
      for(unsigned int i = 0; i < img.dim.v1; i++)
        if(pix_has_data(data, rad, i, j, k)) smth_list.push_back( {i, j, k} );

  PROGRESS<unsigned int> prg(iter, "Smoothing progress: ");

  for(unsigned int it = 0; it < iter; it++)
  {
    #ifdef OPENMP
    #pragma omp parallel for schedule(guided, 10)
    #endif
    for(size_t w = 0; w < smth_list.size(); w++) {
      mt_triple<unsigned int> & s = smth_list[w];
      pix_smooth(data, rad, smth, s.v1, s.v2, s.v3);
    }

    #ifdef OPENMP
    #pragma omp parallel for schedule(guided, 10)
    #endif
    for(size_t w = 0; w < smth_list.size(); w++) {
      mt_triple<unsigned int> & s = smth_list[w];
      pix_smooth(data, rad, -smth * SMOOTH_FREQ_SCA, s.v1, s.v2, s.v3);
    }

    prg.next();
  }

  prg.finish();
}

template <typename T>
class m3
{
  public:
    int N;
    int M;
    int K;
    T*** mat;

  public:
  /// Empty constructor initializes to default values around center 0., 0., 0.
  m3() : N(0),M(0),K(0),mat(NULL)
  {}

  m3(int n, int m, int k) : N(n), M(m), K(k)
  {
    alloc_mat();
  }

  ~m3() {
    dealloc_mat();
  }

  void assign(int n, int m, int k)
  {
    if(mat) dealloc_mat();

    N = n;
    M = m;
    K = k;
    alloc_mat();
  }

  private:
  void alloc_mat()
  {
    mat = new T**[N];
    for (int i = 0; i < N; ++i) {
      // Step 2: For each i, allocate memory for M pointers to type T
      mat[i] = new T*[M];

      for (int j = 0; j < M; ++j) {
        // Step 3: For each j, allocate memory for K values of type T 
        mat[i][j] = new T[K];
      }
    }
  }

  void dealloc_mat()
  {
    for (int i = 0; i < N; ++i) {
      for (int j = 0; j < M; ++j)
        delete [] mat[i][j];
      delete [] mat[i];
    }

    delete [] mat;
  }
};


inline void gaussian_kernel(const itk_image &img, const mt_triple<double> &sigma, const short max_rad,
                            m3<double> & kernel)
{
  // spacing of stack 
  double dx = img.pixspc.v1;
  double dy = img.pixspc.v2;
  double dz = img.pixspc.v3;

  int n_sigma = 2;
  int fx = n_sigma * sigma.v1 / dx;
  int fy = n_sigma * sigma.v2 / dy;
  int fz = n_sigma * sigma.v3 / dz;
 
  // enforce minimum filter size, and limit maximum size
  fx = fx<1?1:fx;
  fy = fy<1?1:fy;
  fz = fz<1?1:fz;

  // limit half kernel width
  fx = fx > max_rad ? max_rad : fx;
  fy = fy > max_rad ? max_rad : fy;
  fz = fz > max_rad ? max_rad : fz;
  // compute kernel width along all dimensions
  int fxw = 2*fx + 1;
  int fyw = 2*fy + 1;
  int fzw = 2*fz + 1;

  kernel.assign(fxw, fyw, fzw);
  mt_triple<double> x0 = {0., 0., 0.};

  for(int i=0; i<fxw; i++)
    for(int j=0; j<fyw; j++)
      for(int k=0; k<fzw; k++)  {
        double x = (i-fx)*dx;
        double y = (j-fy)*dy;
        double z = (k-fz)*dz;
        kernel.mat[i][j][k] = exp(-(x-x0.v1)*(x-x0.v1)/(sigma.v1*sigma.v1*2)) *
                              exp(-(y-x0.v2)*(y-x0.v2)/(sigma.v2*sigma.v2*2)) *
                              exp(-(z-x0.v3)*(z-x0.v3)/(sigma.v3*sigma.v3*2));
      }

  // plot_kernel(ckernel, img.pixspc);
}

template<typename T> inline
T pix_filter(itk_access<T> & data, m3<double> & kernel, unsigned i, unsigned j, unsigned k)
{
  T acc = 0.0f, sum = 0.0f;

  unsigned int rad_i = (kernel.N-1)/2;
  unsigned int rad_j = (kernel.M-1)/2;
  unsigned int rad_k = (kernel.K-1)/2;

  const unsigned int i_start = int(i - rad_i) > 0 ? i - rad_i : 0;
  const unsigned int j_start = int(j - rad_j) > 0 ? j - rad_j : 0;
  const unsigned int k_start = int(k - rad_k) > 0 ? k - rad_k : 0;
  rad_i += 1, rad_j += 1, rad_k += 1;
  const unsigned int i_end = i + rad_i < data.dim.v1 ? i + rad_i : data.dim.v1;
  const unsigned int j_end = j + rad_j < data.dim.v2 ? j + rad_j : data.dim.v2;
  const unsigned int k_end = k + rad_k < data.dim.v3 ? k + rad_k : data.dim.v3;

  for(unsigned int k2 = k_start; k2 < k_end; k2++)
    for(unsigned int j2 = j_start; j2 < j_end; j2++)
      for(unsigned int i2 = i_start; i2 < i_end; i2++)
      {
        T w = kernel.mat[i2-i_start][j2-j_start][k2-k_start];
        acc += data(i2,j2,k2) * w;
        sum += w;
      }

  acc /= sum;

  return acc;
}

template<typename T> inline
T pix_median(itk_access<T> & data, mt_vector<T> & buff, short rad, unsigned i, unsigned j, unsigned k)
{
  buff.resize(0);

  const unsigned int i_start = int(i - rad) > 0 ? i - rad : 0;
  const unsigned int j_start = int(j - rad) > 0 ? j - rad : 0;
  const unsigned int k_start = int(k - rad) > 0 ? k - rad : 0;
  rad++;
  const unsigned int i_end = i + rad < data.dim.v1 ? i + rad : data.dim.v1;
  const unsigned int j_end = j + rad < data.dim.v2 ? j + rad : data.dim.v2;
  const unsigned int k_end = k + rad < data.dim.v3 ? k + rad : data.dim.v3;

  for(unsigned int k2 = k_start; k2 < k_end; k2++)
    for(unsigned int j2 = j_start; j2 < j_end; j2++)
      for(unsigned int i2 = i_start; i2 < i_end; i2++)
        buff.push_back(data(i2,j2,k2));

  std::sort(buff.begin(), buff.end());

  T ret = buff[buff.size()/2];
  return ret;
}

inline void itk_filter_gaussian(itk_image &img, mt_triple<double> &sigma, short max_rad)
{
  itk_image fimg;
  fimg.assign(img, "float");

  itk_access<float> acc(fimg);

  m3<double> kernel;
  gaussian_kernel(img, sigma, max_rad, kernel);

  char str_buff[128];
  snprintf(str_buff, 128, "Gaussian smoothing (%dx%dx%d): ", kernel.N, kernel.M, kernel.K);
  PROGRESS<size_t> prg(img.dim.v3, str_buff);

  for(unsigned int k = 0; k < img.dim.v3; k++) {
    for(unsigned int j = 0; j < img.dim.v2; j++)
      for(unsigned int i = 0; i < img.dim.v1; i++)
        acc(i,j,k) = pix_filter(acc, kernel, i, j, k);

    prg.next();
  }
  prg.finish();

  img.assign(fimg, itk_get_datatype_str_vtk(img.comptypeid));
}

inline void itk_filter_mean(itk_image &img, short rad)
{
  itk_image fimg;
  fimg.assign(img, "float");

  itk_access<float> acc(fimg);

  short dim = rad*2+1;
  m3<double> kernel(dim,dim,dim);
  for(short i=0; i<dim; i++)
    for(short j=0; j<dim; j++)
      for(short k=0; k<dim; k++) kernel.mat[i][j][k] = 1.0;

  char str_buff[128];
  snprintf(str_buff, 128, "Mean smoothing (%d^3): ", rad);
  PROGRESS<size_t> prg(img.dim.v3, str_buff);

  for(unsigned int k = 0; k < img.dim.v3; k++) {
    for(unsigned int j = 0; j < img.dim.v2; j++)
      for(unsigned int i = 0; i < img.dim.v1; i++)
        acc(i,j,k) = pix_filter(acc, kernel, i, j, k);
    prg.next();
  }
  prg.finish();

  img.assign(fimg, itk_get_datatype_str_vtk(img.comptypeid));
}

inline void itk_filter_median(itk_image &img, short rad)
{
  itk_image fimg;
  fimg.assign(img, "float");

  itk_access<float> acc(fimg);
  mt_vector<float> buff(rad*rad*rad);

  char str_buff[128];
  snprintf(str_buff, 128, "Median smoothing (%d^3): ", rad);
  PROGRESS<size_t> prg(img.dim.v3, str_buff);

  for(unsigned int k = 0; k < img.dim.v3; k++) {
    for(unsigned int j = 0; j < img.dim.v2; j++)
      for(unsigned int i = 0; i < img.dim.v1; i++)
        acc(i,j,k) = pix_median(acc, buff, rad, i, j, k);
    prg.next();
  }
  prg.finish();

  img.assign(fimg, itk_get_datatype_str_vtk(img.comptypeid));
}

inline void itk_mask(itk_image & img, float val, float zeroval = 0.0f)
{
  itk_access<float> data(img);

  for(unsigned int k = 0; k < img.dim.v3; k++)
    for(unsigned int j = 0; j < img.dim.v2; j++)
      for(unsigned int i = 0; i < img.dim.v1; i++)
        if(data(i,j,k) != val) data(i,j,k) = zeroval;
}


inline void itk_extract_vals(itk_image & in,
                             itk_image & out,
                             const std::set<float> & vals,
                             float zeroval = 0.0f)
{
  assert(in.dim.v1 == out.dim.v1 &&
         in.dim.v2 == out.dim.v2 && in.dim.v3 == out.dim.v3);

  for(unsigned int k = 0; k < in.dim.v3; k++)
    for(unsigned int j = 0; j < in.dim.v2; j++)
      for(unsigned int i = 0; i < in.dim.v1; i++) {
        float v = 0.0f;
        itk_comp_as<float>(in(i,j,k), in.comptypeid, v);

        if(vals.count(v)) {
          itk_comp_from(in (i,j,k), in .comptypeid, zeroval);
          itk_comp_from(out(i,j,k), out.comptypeid, v);
        }
      }
}

template<class SET>
inline void itk_zero_vals(itk_image & img,
                          const SET & vals,
                          int zeroval = 0)
{
  for(unsigned int k = 0; k < img.dim.v3; k++)
    for(unsigned int j = 0; j < img.dim.v2; j++)
      for(unsigned int i = 0; i < img.dim.v1; i++) {
        int v = 0;
        itk_comp_as<int>(img(i,j,k), img.comptypeid, v);

        if(vals.count(v))
          itk_comp_from(img (i,j,k), img .comptypeid, zeroval);
      }
}


inline void itk_insert_vals(itk_image & in,
                            itk_image & out,
                            const std::set<float> & vals)
{
  assert(in.dim.v1 == out.dim.v1 &&
         in.dim.v2 == out.dim.v2 && in.dim.v3 == out.dim.v3);

  for(unsigned int k = 0; k < in.dim.v3; k++)
    for(unsigned int j = 0; j < in.dim.v2; j++)
      for(unsigned int i = 0; i < in.dim.v1; i++) {
        float v = 0.0f;
        itk_comp_as<float>(in(i,j,k), in.comptypeid, v);

        if(vals.size() == 0 || vals.count(v)) {
          itk_comp_from(out(i,j,k), out.comptypeid, v);
        }
      }
}

inline void itk_select_interface(itk_image & img,
                                 const int reg,
                                 const MT_USET<int> & bdry,
                                 mt_vector<mt_triple<unsigned int>> & sel,
                                 const bool inside = false)
{
  mt_triple<short> onepx = {1,1,1};

  #ifdef OPENMP
  #pragma omp parallel
  #endif
  {
    mt_vector<mt_triple<unsigned int> > sel_loc; sel_loc.reserve(10000);

    #ifdef OPENMP
    #pragma omp for schedule(guided, 10)
    #endif
    for(unsigned int k = 0; k < img.dim.v3; k++) {
      for(unsigned int j = 0; j < img.dim.v2; j++) {
        for(unsigned int i = 0; i < img.dim.v1; i++) {
          int rd_val = 0;
          itk_comp_as<int>(img(i,j,k), img.comptypeid, rd_val);

          if(inside) {
            if(rd_val == reg) {
              if(pix_has_bdry_in_rad(img, onepx, i, j, k, bdry))
                sel_loc.push_back( {i,j,k} );
            }
          } else {
            if(bdry.count(rd_val)) {
              if(pix_has_value_in_rad(img, onepx, i, j, k, reg))
                sel_loc.push_back( {i,j,k} );
            }
          }
        }
      }
    }

    #ifdef OPENMP
    #pragma omp critical
    #endif
    sel.push_back(sel_loc.begin(), sel_loc.end());
  }
}

inline void itk_dilate(itk_image & img,
                        mt_triple<short> rad,
                        const int reg,
                        const MT_USET<int> & bdry)
{
  mt_vector<mt_triple<unsigned int>> dilate_list;
  MT_USET<mt_triple<unsigned>> dilate_set;

  if(rad.v1 > 1 || rad.v2 > 1 || rad.v3 > 1) {
    // here we select the interface pixesl *inside* of reg, then
    // set values into the boundary
    itk_select_interface(img, reg, bdry, dilate_list, true);

    if(rad.v1 == rad.v2 && rad.v2 == rad.v3) {
      for(size_t i=0; i<dilate_list.size(); i++) {
        const mt_triple<unsigned int> & t = dilate_list[i];
        pix_set_in_rad(img, rad.v1, t.v1, t.v2, t.v3, bdry, reg);
      }
    } else {
      for(const mt_triple<unsigned int> & t : dilate_list)
        pix_select_in_rad(img, rad, t.v1, t.v2, t.v3, bdry, dilate_set);

      for(const mt_triple<unsigned int> & t : dilate_set)
        itk_comp_from(img(t.v1, t.v2, t.v3), img.comptypeid, reg);
    }
  } else {
    // here we select the interface pixesl *outside* of reg, then
    // we are already done
    itk_select_interface(img, reg, bdry, dilate_list, false);

    for(const mt_triple<unsigned int> & t : dilate_list)
      itk_comp_from(img(t.v1, t.v2, t.v3), img.comptypeid, reg);
  }
}

inline void itk_erode(itk_image & img,
                        mt_triple<short> rad,
                        const int reg,
                        const MT_USET<int> & bdry)
{
  #ifdef OPENMP
  #pragma omp parallel
  #endif
  {
    std::vector<mt_triple<unsigned int> > rem_list; rem_list.reserve(10000);

    if(rad.v1 == rad.v2 && rad.v2 == rad.v3) {
#ifdef OPENMP
#pragma omp for schedule(guided, 10)
#endif
      for(unsigned int k = 0; k < img.dim.v3; k++) {
        for(unsigned int j = 0; j < img.dim.v2; j++) {
          for(unsigned int i = 0; i < img.dim.v1; i++) {
            int rd_val = 0;
            itk_comp_as<int>(img(i,j,k), img.comptypeid, rd_val);

            if(rd_val == reg) {
              if(pix_has_bdry_in_rad(img, rad.v1, i, j, k, bdry))
                rem_list.push_back( {i,j,k} );
            }
          }
        }
      }
    } else {
#ifdef OPENMP
#pragma omp for schedule(guided, 10)
#endif
      for(unsigned int k = 0; k < img.dim.v3; k++) {
        for(unsigned int j = 0; j < img.dim.v2; j++) {
          for(unsigned int i = 0; i < img.dim.v1; i++) {
            int rd_val = 0;
            itk_comp_as<int>(img(i,j,k), img.comptypeid, rd_val);

            if(rd_val == reg) {
              if(pix_has_bdry_in_rad(img, rad, i, j, k, bdry))
                rem_list.push_back( {i,j,k} );
            }
          }
        }
      }
    }

    #ifdef OPENMP
    #pragma omp barrier
    #endif

    for(const mt_triple<unsigned int> & t : rem_list)
      itk_comp_from(img(t.v1, t.v2, t.v3), img.comptypeid, 0);
  }
}

inline void itk_extrude(itk_image & img,
                        short rad,
                        const int old_reg,
                        const int new_reg,
                        const MT_USET<int> & bdry,
                        bool inward)
{
  mt_vector<mt_triple<unsigned int>> itf;
  MT_USET<mt_triple<unsigned>>       extrude_set;

  // itf are pixels on the bdry regions that are next to our old_reg
  itk_select_interface(img, old_reg, bdry, itf);

  if(inward) {
    // itf is on the outside, we have to select with the full radius pixels for the
    // inside (i.e. new boundary set with old_reg tag).
    MT_USET<int> inside;
    inside.insert(old_reg);

    for(const mt_triple<unsigned int> & t : itf)
      pix_set_in_rad(img, rad, t.v1, t.v2, t.v3, inside, new_reg);
  } else {
    // itf is on the outside. if rad > 1, we have to select with rad-1 pixels
    if(rad > 1) {
      rad -= 1;
      for(const mt_triple<unsigned int> & t : itf)
        pix_set_in_rad(img, rad, t.v1, t.v2, t.v3, bdry, new_reg);
    } else {
      for(const mt_triple<unsigned int> & t : itf)
        itk_comp_from(img(t.v1, t.v2, t.v3), img.comptypeid, new_reg);
    }
  }
}

inline bool valid_pixel(const vec3i & px, const itk_image & img)
{
  bool valid_x = px.x >= 0 && px.x < mt_idx_t(img.dim.v1);
  bool valid_y = px.y >= 0 && px.y < mt_idx_t(img.dim.v2);
  bool valid_z = px.z >= 0 && px.z < mt_idx_t(img.dim.v3);

  return valid_x && valid_y && valid_z;
}

inline void itk_insert_image(itk_image & dest,
                             itk_image & src,
                             bool insert_zero = false)
{
  itk_access<short> dest_acc(dest);
  itk_access<short> src_acc (src);

  #ifdef OPENMP
  #pragma omp for schedule(guided, 10)
  #endif
  for(unsigned int k = 0; k < dest.dim.v3; k++) {
    for(unsigned int j = 0; j < dest.dim.v2; j++) {
      for(unsigned int i = 0; i < dest.dim.v1; i++) {
        vec3r dst_p = dest_acc.position<mt_real>(i, j, k);
        vec3i src_p = src_acc .pixel   <mt_real>(dst_p);

        if(valid_pixel(src_p, src)) {
          float v = 0.0f;

          if(dest.ncomp == 1 || dest.ncomp != src.ncomp) {
            itk_comp_as<float>(src(src_p.x, src_p.y, src_p.z), src.comptypeid, v);
            if(v || insert_zero)
              itk_comp_from(dest(i,j,k), dest.comptypeid, v);
          } else {
            for(unsigned int c = 0; c < dest.ncomp; c++) {
              itk_comp_as<float>(src(src_p.x, src_p.y, src_p.z, c), src.comptypeid, v);
              itk_comp_from(dest(i,j,k,c), dest.comptypeid, v);
            }
          }
        }
      }
    }
  }
}

inline void itk_insert_image(itk_image & dest,
                             itk_image & src,
                             const MT_USET<int> & reg)
{
  itk_access<short> dest_acc(dest);
  itk_access<short> src_acc(src);

  #ifdef OPENMP
  #pragma omp for schedule(guided, 10)
  #endif
  for(unsigned int k = 0; k < dest.dim.v3; k++) {
    for(unsigned int j = 0; j < dest.dim.v2; j++) {
      for(unsigned int i = 0; i < dest.dim.v1; i++) {
        vec3r dst_p = dest_acc.position<mt_real>(i, j, k);
        vec3i src_p = src_acc .pixel   <mt_real>(dst_p);

        if(valid_pixel(src_p, src)) {
          int v = 0;

          if(dest.ncomp == 1 || dest.ncomp != src.ncomp) {
            itk_comp_as<int>(src(src_p.x, src_p.y, src_p.z), src.comptypeid, v);
            if(reg.count(v))
              itk_comp_from(dest(i,j,k), dest.comptypeid, v);
          } else {
            for(unsigned int c = 0; c < dest.ncomp; c++) {
              itk_comp_as<int>(src(src_p.x, src_p.y, src_p.z, c), src.comptypeid, v);
              itk_comp_from(dest(i,j,k,c), dest.comptypeid, v);
            }
          }
        }
      }
    }
  }
}

inline void itk_merge_images(itk_image & dest,
                             itk_image & src)
{
  vec3r dest_p0 = vec3r(dest.orig.v1, dest.orig.v2, dest.orig.v3);
  vec3r dest_p1 = dest_p0 + vec3r(dest.dim.v1*dest.pixspc.v1, dest.dim.v2*dest.pixspc.v2, dest.dim.v3*dest.pixspc.v3);

  vec3r src_p0 = vec3r(src.orig.v1, src.orig.v2, src.orig.v3);
  vec3r src_p1 = src_p0 + vec3r(src.dim.v1*src.pixspc.v1, src.dim.v2*src.pixspc.v2, src.dim.v3*src.pixspc.v3);

  bbox new_box  = {point_min(dest_p0, src_p0), point_max(dest_p1, src_p1)};
  vec3r delta   = new_box.bounds[1] - new_box.bounds[0];

  itk_image ni;
  ni.comptypeid = dest.comptypeid;
  ni.pixsz      = dest.pixsz;
  ni.pixspc     = dest.pixspc;
  ni.compsz     = dest.compsz;
  ni.ncomp      = dest.ncomp;

  ni.orig       = {new_box.bounds[0].x, new_box.bounds[0].y, new_box.bounds[0].z};
  ni.dim        = {((unsigned int) (delta.x / ni.pixspc.v1)) + 1, ((unsigned int) (delta.y / ni.pixspc.v2)) + 1,
                   ((unsigned int) (delta.z / ni.pixspc.v3)) + 1};
  ni.npix       = ni.dim.v1 * ni.dim.v2 * ni.dim.v3;
  ni.datasz     = ni.npix * ni.pixsz;
  ni.databuff.assign(size_t(ni.datasz), char(0));

  itk_insert_image(ni, dest);
  itk_insert_image(ni, src);

  dest = ni;
}

/**
* @brief Clamp to a value based on minimal distance.
*
* @param [in] curr        Current value.
* @param [in] candidates  Set of value candidates.
*
* @return The value we clamped to.
*/
template<typename T, typename S>
inline T clamp_min_dist(T curr, const std::set<S> & candidates)
{
  auto it = candidates.begin();

  T dist = fabs(T(*it) - curr);
  T choice = T(*it);
  ++it;

  while(it != candidates.end())
  {
    T f = fabs(T(*it) - curr);
    if(dist > f) {
      dist = f;
      choice = T(*it);
    }
    ++it;
  }

  return choice;
}


/**
* @brief Clamp data of one pixel based on reference data in a radius.
*
* @param [in] cdata  Clamping data.
* @param [in] rdata  Reference data.
* @param [in] rad    Radius of interest.
* @param [in] i      Discrete x coordinate of pixel.
* @param [in] j      Discrete y coordinate of pixel.
* @param [in] k      Discrete z coordinate of pixel.
*/
template<typename T, typename S>
inline void pix_clamp(itk_access<T> & cdata, itk_access<S> & rdata, mt_triple<short> rad, unsigned i, unsigned j, unsigned k)
{
  std::set<S> candidates;

  unsigned int rad_i = rad.v1 * rdata.spc_fct.v1;
  unsigned int rad_j = rad.v2 * rdata.spc_fct.v2;
  unsigned int rad_k = rad.v3 * rdata.spc_fct.v3;
  const unsigned int i_start = int(i - rad_i) > 0 ? i - rad_i : 0;
  const unsigned int j_start = int(j - rad_j) > 0 ? j - rad_j : 0;
  const unsigned int k_start = int(k - rad_k) > 0 ? k - rad_k : 0;
  rad_i += 1, rad_j += 1, rad_k += 1;
  const unsigned int i_end = i + rad_i < rdata.dim.v1 ? i + rad_i : rdata.dim.v1;
  const unsigned int j_end = j + rad_j < rdata.dim.v2 ? j + rad_j : rdata.dim.v2;
  const unsigned int k_end = k + rad_k < rdata.dim.v3 ? k + rad_k : rdata.dim.v3;

  mt_triple<double> space_rad = {rad_i*rdata.pixspc.v1, rad_j*rdata.pixspc.v2, rad_k*rdata.pixspc.v3};

  // pixels of reference data in a given radius are selected as candidates
  for(unsigned int k2 = k_start; k2 < k_end; k2++)
    for(unsigned int j2 = j_start; j2 < j_end; j2++)
      for(unsigned int i2 = i_start; i2 < i_end; i2++)
        if(rdata.is_in_ellipsoid(i,j,k,i2,j2,k2,space_rad.v1,space_rad.v2,space_rad.v3))
          candidates.insert(rdata(i2,j2,k2));

  T curr = cdata(i,j,k);
  if(candidates.size() > 0)
    cdata(i,j,k) = clamp_min_dist(curr, candidates);
  else {
    std::cerr << "Clamping error: No candidates!" << std::endl;
    exit(1);
  }
}


/**
* @brief Clamp the pixel data of an itk image.
*
* @param [in] cimg   Image of floating point data to clamp.
* @param [in] rimg   Image of integer reference data to clamp to.
* @param [in] rad    Radius of interest.
*/
inline void clamp_itk(itk_image & cimg, itk_image & rimg, mt_triple<short> rad)
{
  itk_access<float> cdata(cimg);  // data to clamp
  itk_access<short> rdata(rimg);  // reference data

  mt_vector<mt_triple<unsigned int> > clamp_list;
  clamp_list.reserve(cimg.dim.v1 * cimg.dim.v2 * cimg.dim.v3 * 0.25);

  // all pixels containing non-zero data are clamped
  for(unsigned int k = 0; k < cimg.dim.v3; k++)
    for(unsigned int j = 0; j < cimg.dim.v2; j++)
      for(unsigned int i = 0; i < cimg.dim.v1; i++)
        if(pix_has_data(cdata, i, j, k)) clamp_list.push_back( {i, j, k} );

  PROGRESS<unsigned int> prg(clamp_list.size(), "Clamping progress: ");

  for(auto c = clamp_list.begin(); c != clamp_list.end(); ++c) {
    pix_clamp(cdata, rdata, rad, c->v1, c->v2, c->v3);
    prg.next();
  }
  prg.finish();
}

inline bool vtkfile_is_image(const char* filename)
{
  bool ret = false;
  char line[256];
  const char* test_str = "DATASET STRUCTURED_POINTS";

  FILE * fp;
  fp = fopen(filename, MT_FOPEN_READ);
  if (fp) {
    int testlines = 100;

    for(int i=0; i<testlines; i++) {
      if(fgets(line, 256, fp)) {
        if(strncmp(line, test_str, strlen(test_str)) == 0) {
          ret = true;
          break;
        }
      }
    }
    fclose(fp);
  }

  return ret;
}

inline bool
itk_is_inttype(const itk_image & img)
{
  return (img.comptypeid > 0 && img.comptypeid < 8);
}

inline void
itk_region_info(const itk_image & img, double & min, double & max,
                MT_MAP<mt_idx_t, size_t> & label_counts)
{
  const char*  data = img.databuff.data();
  const bool   data_is_int = itk_is_inttype(img);
  const size_t num_comp    = img.dim.v1 * img.dim.v2 * img.dim.v3 * img.ncomp;

  itk_comp_as<double>(data, img.comptypeid, min);
  itk_comp_as<double>(data, img.comptypeid, max);

  mt_real val = 0.0;
  mt_idx_t ival = 0;

  for(size_t i=0; i < num_comp; i++) {
    itk_comp_as_mt_real(data, img.comptypeid, val);

    if(min > val) min = val;
    if(max < val) max = val;

    if(data_is_int) {
      itk_comp_as_mt_idx_t(data, img.comptypeid, ival);
      label_counts[ival] = label_counts[ival] + 1;
    }

    data += img.compsz;
  }
}

template<class MAP> inline
void itk_map_regions(itk_image & img, MAP & map)
{
  int tag = 0;

  for(size_t i=0; i<img.dim.v1 * img.dim.v2 * img.dim.v3; i++) {
    char* pixel = img.databuff.data() + i * img.pixsz;

    itk_comp_as<int>(pixel, img.comptypeid, tag);

    if(map.count(tag)) {
      int newval = map[tag];
      itk_comp_from(pixel, img.comptypeid, newval);
    }
  }
}

inline
void itk_extract_component(const itk_image & in, itk_image & out, unsigned int comp)
{
  if(comp >= in.ncomp) {
    fprintf(stderr, "%s error: chosen component (%ud) exceeds number of image components (%ud)\n",
            __func__, comp, in.ncomp);
    return;
  }

  out.dim        = in.dim;
  out.pixspc     = in.pixspc;
  out.orig       = in.orig;
  out.comptypeid = in.comptypeid;
  out.npix       = in.npix;
  out.compsz     = in.compsz;

  out.ncomp  = 1;
  out.pixsz  = out.compsz;
  out.datasz = out.npix * out.pixsz;

  out.databuff.resize(out.datasz);

  size_t read_offset = comp * in.compsz;
  for(size_t i=0; i<out.npix; i++) {
    memcpy(out.databuff.data() + i*out.pixsz,
           in .databuff.data() + i*in .pixsz + read_offset,
           in .compsz);
  }
}


#endif
