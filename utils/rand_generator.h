#ifndef _RAND_GENERATOR_H
#define _RAND_GENERATOR_H

#include <stdlib.h>
#include <vector>

#ifdef OPENMP
#include <omp.h>
#endif

class rand_generator
{
  private:
  std::vector<float> _rands;
  size_t _ridx;

  public:
  rand_generator(size_t rsize) : _rands(rsize), _ridx(0)
  {
    for(size_t i=0; i<_rands.size(); i++)
      _rands[i] = drand48();
  }

  rand_generator() : rand_generator(4*1024*1024)
  {}

  inline float get_rand() {
    float ret = _rands[_ridx];
    _ridx = (_ridx+1) % _rands.size();
    return ret;
  }
};

class rand_generator_state {
  mt_vector<rand_generator> global_rands;

  public:
  rand_generator_state() {
#ifdef OPENMP
#pragma omp parallel
    {
      int tid      = omp_get_thread_num();
      int nthreads = omp_get_num_threads();
      if (tid == 0)
        global_rands.resize(nthreads);
    }
#else
    global_rands.resize(1);
#endif
  }

  inline rand_generator & get_rnd()
  {
#ifdef OPENMP
    return global_rands[omp_get_thread_num()];
#else
    return global_rands[0];
#endif
  }
};

extern rand_generator_state mt_rand_gen;

inline rand_generator & GET_RND()
{
  return mt_rand_gen.get_rnd();
}
#endif
