/**
* @file mesh_smoothing.h
* @brief Mesh smoothing algorithms and classes.
* @author Aurel Neic
* @version
* @date 2016-12-13
*/


#ifndef _MESH_SMOOTHING
#define _MESH_SMOOTHING

#define SMOOTH_ITER_DEFAULT    100
#define SMOOTH_DEFAULT         0.15
#define EDGE_DETECT_DEFAULT    0.0
#define SMOOTH_LPC_LVL_DEFAULT 1

/// factor between forward and backward smoothing scaling
#define SMOOTH_FREQ_SCA 1.025f
/// default value for the mesh quality threshold
#define SMOOTH_THR_DEFAULT 0.95
#define QUAL_THR_CHK_SCA  0.5
/// every QUAL_THR_UPD, element quality thresholds will be refreshed
#define QUAL_THR_UPD     8
#define VOL_UPD          2

#include "topology_utils.h"

/**
* @brief Class for mesh smoothing while keeping mesh quality in check
*
*/
class quality_aware_smoother
{
  private:
  mt_vector<bool>     _applied;    ///< flag buffer to keep track of applied displacements
  mt_vector<mt_real>  _qual_thr;   ///< per-node quality threshold
  mt_vector<mt_real>  _vols_msh,   ///< main mesh volumes
                      _vols_mnfld; ///< manifold volumes
  mt_vector<vec3r>    _upd;

  /**
  * @brief initialize quality threshold
  *
  * @param checkmesh The mesh we check element quality on.
  * @param nodes     The nodes we are interested in because they will be smoothed.
  * @param inp_thr   The user-specified quality threshold
  */
  void init_qual_thr(const mt_meshdata & checkmesh,
                     const mt_vector<mt_idx_t> & nodes,
                     const mt_real inp_thr);

  /**
  * @brief The forward smoothing iteration
  *
  * @param checkmesh   The mesh we check quality on.
  * @param smoothmesh  The main smoothing mesh.
  * @param manifold    The manifold mesh.
  * @param onManifold  Flag encoding which nodes are on the manifold.
  * @param nodes       Vector holding the nodes to smooth.
  * @param sca         Smoothing scale factor.
  * @param inp_thr     User-spcifed quality threshold.
  */
  void qual_smooth_iter_fwd(mt_meshdata & checkmesh,
                            mt_meshdata & smoothmesh,
                            mt_meshdata & manifold,
                            const mt_mask & onManifold,
                            const mt_vector<mt_idx_t> & nodes,
                            const mt_real sca,
                            const mt_real inp_thr);

  void qual_smooth_iter_fwd(const mt_vector<mt_meshdata*> & manifolds,
                            const mt_vector<int> & mnfld_idx,
                            mt_vector<mt_real> & inp_xyz,
                            const mt_vector<mt_idx_t> & nodes,
                            const mt_real sca,
                            const mt_real inp_thr);

  /**
  * @brief Backward smoothing iteration.
  *
  * @param checkmesh   The mesh we check quality on.
  * @param smoothmesh  The main smoothing mesh.
  * @param manifold    The manifold mesh.
  * @param onManifold  Flag encoding which nodes are on the manifold.
  * @param nodes       Vector holding the nodes to smooth.
  * @param sca         Smoothing scale factor.
  * @param inp_thr     User-spcifed quality threshold.
  */
  void qual_smooth_iter_bwd(mt_meshdata & checkmesh,
                            mt_meshdata & smoothmesh,
                            mt_meshdata & manifold,
                            const mt_mask & onManifold,
                            const mt_vector<mt_idx_t> & nodes,
                            mt_real sca,
                            mt_real inp_thr);

  void qual_smooth_iter_bwd(const mt_vector<mt_meshdata*> & manifolds,
                            const mt_vector<int> & mnfld_idx,
                            mt_vector<mt_real> & inp_xyz,
                            const mt_vector<mt_idx_t> & nodes,
                            const mt_real sca,
                            const mt_real inp_thr);
  public:

  /**
  * @brief Smooth a mesh with quality control.
  *
  * @param checkmesh   The mesh we check quality on.
  * @param smoothmesh  The main smoothing mesh.
  * @param manifold    The manifold mesh.
  * @param onManifold  Flag encoding which nodes are on the manifold.
  * @param nodes       Vector holding the nodes to smooth.
  * @param nsmooth     The number of smoothing operations.
  * @param sca         Smoothing scale factor.
  * @param inp_thr     User-spcifed quality threshold.
  */
  void operator()(mt_meshdata & checkmesh,
                  mt_meshdata & mesh,
                  mt_meshdata & manifold,
                  const mt_mask & onManifold,
                  const mt_vector<mt_idx_t> & nodes,
                  const size_t nsmooth,
                  const mt_real sca,
                  const mt_real inp_thr);

  void operator()(const mt_vector<mt_meshdata*> & manifolds,
                  const mt_vector<int> & mnfld_idx,
                  mt_vector<mt_real> & inp_xyz,
                  const mt_vector<mt_idx_t> & nodes,
                  const size_t  nsmooth,
                  const mt_real sca,
                  const mt_real inp_thr);

  void operator()(mt_meshdata & checkmesh,
                  mt_meshdata & smoothmesh,
                  mt_meshdata & manifold,
                  const mt_mask & mnfld_mask,
                  const mt_vector<mt_idx_t> & nodes,
                  const size_t nsmooth,
                  const size_t nlevel,
                  const mt_real sca,
                  const mt_real inp_thr);

};

void smooth_iter(mt_meshdata & mesh,
                 mt_vector<vec3r> & upd,
                 const mt_meshdata & manifold,
                 const mt_mask & mnfld_mask,
                 const mt_vector<mt_idx_t> & nodes,
                 mt_real sca);

/**
* @brief Apply a given number of smoothing iterations.
*
* For volume preservation, smoothing is applied in forward and backward directions.
*
* @param [in]  mesh        The mesh.
* @param [in]  manifold    A mesh containing a manifold (either surface or line) of the mesh.
* @param [in]  onManifold  onManifold[i] tells wether node i is on the given manifold.
* @param [in]  nodes       The nodes to smooth.
* @param [in]  nsmooth     The number of smoothing iterations.
* @param [in]  sca         The smoothing coefficient.
*
* @post The vertex coordinates mesh.xyz have been updated.
*/
void smooth_nodes(mt_meshdata & mesh,
                  mt_meshdata & manifold,
                  const mt_mask & onManifold,
                  const mt_vector<mt_idx_t> & nodes,
                  size_t nsmooth,
                  mt_real sca,
                  bool verbose = true,
                  bool taubin_type = true);

void smooth_nodes(mt_meshdata & mesh,
                  mt_meshdata & manifold,
                  const mt_mask & mnfld_mask,
                  const mt_vector<mt_idx_t> & nodes,
                  size_t nsmooth,
                  size_t nlevel,
                  mt_real sca,
                  bool verbose = true,
                  bool taubin_type = true);

/**
* @brief Perform one data smoothing operation
*
* @param mesh      The mesh.
* @param nodes     Nodes to smooth data on.
* @param onSubset  Only smooth data using contributions from a nodal subset.
* @param sca       Smoothing scale factor.
* @param data      The data to smooth.
*/
template<class S>
void smooth_data_iter(const mt_vector<mt_cnt_t> & cnt,
                      const mt_vector<mt_idx_t> & dsp,
                      const mt_vector<mt_idx_t> & con,
                      const mt_vector<mt_idx_t> & idx,
                      const mt_vector<bool> & onSubset,
                      const mt_real sca,
                      mt_vector<S> & data)
{
  size_t nnodes = idx.size();

  #ifdef OPENMP
  #pragma omp parallel for schedule(guided)
  #endif
  for(size_t i=0; i<nnodes; i++)
  {
    S avrg = S();
    mt_idx_t ii = idx[i];

    mt_idx_t start = dsp[ii], stop = start + cnt[ii];
    mt_idx_t counter = 0;

    for(mt_idx_t j=start; j<stop; j++)
    {
      mt_idx_t n = con[j];
      if(onSubset[n]) {
        avrg += data[n];
        counter++;
      }
    }
    avrg /= counter;

    data[ii] += (avrg - data[ii]) * sca;
  }
}

/**
* @brief Perform one data smoothing operation
*
* @param mesh      The mesh.
* @param nodes     Nodes to smooth data on.
* @param onSubset  Only smooth data using contributions from a nodal subset.
* @param sca       Smoothing scale factor.
* @param data      The data to smooth.
*/
template<class S>
void smooth_direction_iter(const mt_vector<mt_cnt_t> & n2n_cnt,
                           const mt_vector<mt_idx_t> & n2n_dsp,
                           const mt_vector<mt_idx_t> & n2n_con,
                           const mt_vector<mt_idx_t> & nodes,
                           const mt_vector<bool> & onSubset,
                           const mt_real sca,
                           mt_vector<mt_point<S>> & data)
{
  size_t nnodes = nodes.size();

  #ifdef OPENMP
  #pragma omp parallel for schedule(guided)
  #endif
  for(size_t i=0; i<nnodes; i++)
  {
    mt_point<S> avrg;
    mt_idx_t nidx = nodes[i];

    mt_idx_t start = n2n_dsp[nidx], stop = start + n2n_cnt[nidx];
    mt_idx_t counter = 0;

    for(mt_idx_t j=start; j<stop; j++) {
      mt_idx_t n = n2n_con[j];
      if(onSubset[n]) {
        mt_point<S> d = data[n];
        // if(d.scaProd(avrg) < -0.3)
        //   d *= S(-1);

        avrg += d;
        counter++;
      }
    }

    if(counter)
      avrg /= counter;

    data[nidx] = unit_vector(data[nidx] + avrg  * sca);
  }
}

#if 0
/**
* @brief Smooth an arbitrary data vector.
*
* @tparam S   Data vector data-type.
*
* @param mesh           The mesh.
* @param nodes          Nodes to smooth data on.
* @param onSubset       Only smooth data using contributions from a nodal subset.
* @param data           The data to smooth.
* @param nsmooth        The number of smoothing operations.
* @param sca            Smoothing scale factor.
* @param data_is_nodal  Whether the input data is in nodal representation.
*/
template<class S>
void smooth_data(const mt_meshdata & mesh,
                 const mt_vector<mt_idx_t> & nodes,
                 const mt_vector<bool> & onSubset,
                 const size_t nsmooth,
                 const mt_real sca,
                 mt_vector<S> & data,
                 const bool data_is_nodal)
{
  mt_vector<S> * d = &data;
  mt_vector<S> nodal_data;

  // if the input data is element-wise, we convert it to nodal
  if(!data_is_nodal) {
    elemData_to_nodeData(mesh, data, nodal_data);
    d = &nodal_data;
  }

  for(size_t sidx = 0; sidx < nsmooth; sidx++) {
    smooth_data_iter(mesh, nodes, onSubset, sca, *d);
    smooth_data_iter(mesh, nodes, onSubset, -(sca*SMOOTH_FREQ_SCA), *d);
  }

  // if the input data was element-wise, we convert the ouput nodal data back to
  // element data
  if(!data_is_nodal)
    nodeData_to_elemData(mesh, nodal_data, data);
}
#endif

template<class S>
void smooth_data_nodal(const mt_meshdata & mesh,
                       const mt_vector<mt_idx_t> & nodes,
                       const mt_vector<bool> & onSubset,
                       const size_t nsmooth,
                       const mt_real sca,
                       mt_vector<S> & data)
{
  check_condition((mesh.xyz.size()/3) == data.size(), "data is node-wise", "smooth data nodal");

  for(size_t sidx = 0; sidx < nsmooth; sidx++) {
    smooth_data_iter(mesh.n2n_cnt, mesh.n2n_dsp, mesh.n2n_con, nodes, onSubset, sca, data);
    smooth_data_iter(mesh.n2n_cnt, mesh.n2n_dsp, mesh.n2n_con, nodes, onSubset, -(sca*SMOOTH_FREQ_SCA), data);
  }
}

template<class S>
void smooth_data_elem(const mt_meshdata & mesh,
                      const mt_vector<mt_idx_t> & eidx,
                      const mt_vector<bool> & onSubset,
                      const size_t nsmooth,
                      const mt_real sca,
                      mt_vector<S> & data)
{
  check_same_size(mesh.e2n_cnt, data, "smooth direction elem: data is element-wise");

  mt_vector<mt_cnt_t> e2e_cnt;
  mt_vector<mt_idx_t> e2e_dsp, e2e_con;
  compute_n2n_par(mesh.n2e_cnt, mesh.n2e_dsp, mesh.n2e_con, mesh.e2n_cnt, mesh.e2n_dsp, mesh.e2n_con, e2e_cnt, e2e_dsp, e2e_con);

  for(size_t sidx = 0; sidx < nsmooth; sidx++) {
    smooth_data_iter(e2e_cnt, e2e_dsp, e2e_con, eidx, onSubset, sca, data);
    smooth_data_iter(e2e_cnt, e2e_dsp, e2e_con, eidx, onSubset, -(sca*SMOOTH_FREQ_SCA), data);
  }
}

template<class S>
void smooth_direction_nodal(const mt_meshdata & mesh,
                            const mt_vector<mt_idx_t> & nodes,
                            const mt_vector<bool> & onSubset,
                            const size_t nsmooth,
                            const mt_real sca,
                            mt_vector<mt_point<S>> & data)
{
  check_condition((mesh.xyz.size()/3) == data.size(), "data is node-wise", "smooth direction nodal");

  for(size_t sidx = 0; sidx < nsmooth; sidx++)
    smooth_direction_iter(mesh.n2n_cnt, mesh.n2n_dsp, mesh.n2n_con, nodes, onSubset, sca, data);
}

template<class S>
void smooth_direction_elem(const mt_meshdata & mesh,
                           const mt_vector<mt_idx_t> & eidx,
                           const mt_vector<bool> & onSubset,
                           const size_t nsmooth,
                           const mt_real sca,
                           mt_vector<mt_point<S>> & data)
{
  check_same_size(mesh.e2n_cnt, data, "smooth direction elem: data is element-wise");

  mt_vector<mt_cnt_t> e2e_cnt;
  mt_vector<mt_idx_t> e2e_dsp, e2e_con;
  compute_n2n_par(mesh.n2e_cnt, mesh.n2e_dsp, mesh.n2e_con, mesh.e2n_cnt, mesh.e2n_dsp, mesh.e2n_con, e2e_cnt, e2e_dsp, e2e_con);

  for(size_t sidx = 0; sidx < nsmooth; sidx++)
    smooth_direction_iter(e2e_cnt, e2e_dsp, e2e_con, eidx, onSubset, sca, data);
}

/// this variant works directly for xyz_data, and only for nodal data
template<class S>
void smooth_direction(const mt_meshdata & mesh,
                 const mt_vector<mt_idx_t> & nodes,
                 const mt_vector<bool> & onSubset,
                 const size_t nsmooth,
                 const mt_real sca,
                 mt_vector<S> & xyz_data)
{
  size_t nnodes = nodes.size();

  for(size_t sidx = 0; sidx < nsmooth; sidx++) {
    #ifdef OPENMP
    #pragma omp parallel for schedule(guided)
    #endif
    for(size_t i=0; i<nnodes; i++)
    {
      mt_point<S> avrg;
      mt_idx_t nidx = nodes[i];

      mt_idx_t start = mesh.n2n_dsp[nidx], stop = start + mesh.n2n_cnt[nidx];
      mt_idx_t counter = 0;

      for(mt_idx_t j=start; j<stop; j++) {
        mt_idx_t n = mesh.n2n_con[j];
        if(onSubset[n]) {
          mt_point<S> d(xyz_data.data() + n*3);

          if(d.scaProd(avrg) < -0.3)
            d *= S(-1);

          avrg += d;
          counter++;
        }
      }

      if(counter)
        avrg /= counter;

      avrg = unit_vector(mt_point<S>(xyz_data.data() + nidx*3) + avrg * sca);
      avrg.set(xyz_data.data() + nidx*3);
    }
  }
}


/**
* @brief Function that bundles the usual volumetric smoothing steps
*
* @param mesh         The mesh we smooth
* @param stags        Groups of tags. The surface of each group will be smoothed.
* @param iter         Number of smoothing iterations.
* @param smth         Smoothing coefficient.
* @param nlevel       Number of laplace level to use. works only when ignoring mesh quality.
* @param edge_ang     Angle threshold defining sharp edges.
* @param max_qual     Maximum allowed elem qual.
* @param skip_lines   Whether to skip lines when smoothing.
* @param verbose      Whether to be verbose.
*/
void volumetric_smooth_from_tags(mt_meshdata & mesh,
                                 const mt_vector<MT_USET<mt_tag_t>> & stags,
                                 const int iter,
                                 const mt_real smth,
                                 const short   nlevel,
                                 const mt_real edge_ang,
                                 const mt_real max_qual,
                                 const bool skip_lines,
                                 const bool verbose = false);

void surface_smooth(mt_meshdata & surfmesh,
                    const int iter,
                    const mt_real smth,
                    const mt_real edge_ang,
                    const mt_real max_qual,
                    const bool skip_lines,
                    const bool verbose);

/**
* @brief Smooth mesh vertices. Each vertex has also a direction that can be
*        removed from the smoothing update motion.
*
* @param mesh            The mesh to smooth.
* @param nod             The nodes to smooth in the mesh.
* @param ortho_dir       The nodal direction vectors used to orthogonalize the smoothing.
* @param ortho_scale     Scaling factor for the orthogonalization. (0 = no ortho, 1 = full ortho)
* @param only_positive   Only motion in direction of the provided vector field.
* @param smth            Smoothing coefficient.
* @param iter            Number of iterations.
*/
void directional_smoothing(mt_meshdata & mesh,
                           const mt_vector<mt_idx_t> & nod,
                           const mt_vector<mt_real> & scale_dir,
                           mt_real scale,
                           mt_real ortho_scale,
                           const bool only_positive,
                           const mt_real smth,
                           const int iter);

void directional_smoothing_elem(mt_meshdata & mesh,
                           const mt_vector<mt_idx_t> & nod,
                           const mt_vector<mt_real> & ortho_dir,
                           mt_real ortho_scale,
                           const bool only_positive,
                           const mt_real smth,
                           const int iter);

/**
* @brief Apply normals filtering technique to smooth mesh
*
* @param mesh            The mesh we smooth.
* @param selected_nodes  The nodes we will be modifying.
* @param selected_elems  The elements connected to the selected_nodes
* @param sfac            Smoothing coefficient.
* @param iters           Number of iterations.
* @param type            Filter type: 0 = median angle, 1 = mean, 2 = median curvature
*/
void normals_filter(mt_meshdata & mesh,
                    const mt_vector<mt_idx_t> & selected_nodes,
                    const mt_vector<mt_idx_t> & selected_elems,
                    const mt_real sfac,
                    const size_t iters,
                    const int type);

/// smooth tags: we want to have smooth interfaces between different regions. we achieve
/// this by minor re-labeling and then some smoothing.
void smooth_tags_surf(mt_meshdata & mesh, MT_USET<mt_tag_t> & tags, int iter, float smth);
void smooth_tags_tetvol(mt_meshdata & mesh, MT_USET<mt_tag_t> & surf_tags, MT_USET<mt_tag_t> & tags, int iter, float smth);

#endif

