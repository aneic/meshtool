#include "mt_utils.h"

#ifdef __WIN32__
#include <windows.h>
#else
#include <glob.h>      // for glob()
#endif

#include <libgen.h>    // for basename()
#include <ctype.h>     // for isalpha()

bool parse_param (const std::string & str, const std::string & param,
                  std::string & res, const mt_check_type check)
{
  bool matched = false;
  if(str.compare(0, param.size(), param) == 0)
  {
    res.assign(str.begin()+param.size(), str.end());
    matched = true;
  }

  if(matched) {
    switch(check) {
      case chk_nonzero:
        check_nonzero(res.size(), "parse_param"); break;
      case chk_fexists:
        check_file_exists(res, "parse_param"); break;
      case dont_check: break;
    }
  }

  return matched;
}

bool parse_param_def (const std::string & str, const std::string & param, const std::string & def,
                      std::string & res, const mt_check_type check)
{
  const size_t str_size = str.size();
  const size_t prm_size = param.size();

  bool matched = false;
  // we expect a param string of the form '-X=' at least
  if ((prm_size < 3) || (str_size < (prm_size-1)))
    return matched;
   
  // if the string is equal to '-X' the default value is assigned
  if ((str_size == (prm_size-1)) && (str.compare(0, str_size, param, 0, prm_size-1) == 0)) {
    res.assign(def);
    matched = true;
  }
  else if ((str_size > prm_size) && (str.compare(0, prm_size, param) == 0)) {
    res.assign(str.begin()+prm_size, str.end());
    matched = true;
  }

  if (matched) {
    switch(check) {
      case chk_nonzero:
        check_nonzero(res.size(), "parse_param"); break;
      case chk_fexists:
        check_file_exists(res, "parse_param"); break;
      case dont_check: break;
    }
  }

  return matched;
}

std::string reformat_param(char **argv, int & i, int argc)
{
  std::string param = argv[i];

  if(param.find( '=' ) == std::string::npos)
  {
    if(i < argc-1) {
      bool first_letter_starts_with_minus = argv[i+1][0] == '-';
      bool second_letter_is_alphabetic = strlen(argv[i+1]) > 1 && isalpha(argv[i+1][1]);

      if(!(first_letter_starts_with_minus && second_letter_is_alphabetic))
      {
        param += "=";
        param += argv[++i];
      }
    }
  }

  return param;
}

bool parse_flag(const std::string & str, const std::string & flag, bool & found)
{
  bool matched = false;
  if(str.compare(flag) == 0)
  {
    found = matched = true;
  }

  return matched;
}

void fixBasename(std::string & base)
{
  size_t len = base.size();
  if( (len > 0) && (base[len-1] == '.') ) base.resize(len-1);
}

#ifndef __WIN32__
void mt_glob(const std::string& pattern, mt_vector<std::string> & ret)
{
  glob_t glob_result;
  //glob(pattern.c_str(), GLOB_TILDE, NULL, &glob_result);
  glob(pattern.c_str(), 0, NULL, &glob_result);

  // Initialise return vector
  ret.resize(glob_result.gl_pathc);

  // Put results in return vector
  for(unsigned int i=0; i<glob_result.gl_pathc; i++)
    ret[i] = std::string(glob_result.gl_pathv[i]);
  globfree(&glob_result);
}
#endif

bool endswith(const std::string value, const std::string ending) {
  if (ending.size() > value.size())
    return false;
  return std::equal(ending.rbegin(), ending.rend(), value.rbegin());
}

bool beginswith(const std::string value, const std::string beginning) {
  if (beginning.size() > value.size())
    return false;
  return std::equal(beginning.begin(), beginning.end(), value.begin());
}

void crop_string(std::string& str)
{
  if (str.empty()) return;
  if (std::all_of(str.begin(), str.end(), [](char c){return std::isspace(c);}))
    str.clear();
  else {
    std::size_t from(0), to(str.size()-1);
    while (std::isspace(str[from]) && (from<to)) from++;
    while (std::isspace(str[to]) && (to>from)) to--;
    str.erase(0, from);
    str.erase(to-from+1);
  }
}

void compact_string(std::string& str)
{
  if (str.empty()) return;
  auto it = std::remove_if(str.begin(), str.end(), isspace);
  str.erase(it, str.end());
}

std::string mt_basename(const std::string path)
{
  char * cstr = new char[path.length()+1];
  std::strcpy(cstr, path.c_str());
  std::string ret = std::string(basename(cstr));
  delete [] cstr;

  return ret;
}

std::string mt_dirname(const std::string path)
{
  char * cstr = new char[path.length()+1];
  std::strcpy(cstr, path.c_str());
  std::string ret = std::string(dirname(cstr));
  delete [] cstr;

  return ret;
}

char* mt_basename(const char* path, mt_vector<char> & strbuff)
{
  if(strbuff.size() < strlen(path))
    strbuff.resize(strlen(path)+1);

  std::strcpy(strbuff.data(), path);
  char* ret = basename(strbuff.data());
  return ret;
}

std::string mt_fullpath(const char* path)
{
#ifndef __WIN32__
  char* resolved_path = NULL;
  std::string ret = "";
  if((resolved_path = realpath(path, NULL)) != NULL) {
    ret = resolved_path;
    free(resolved_path);
  }
#else
  static char buff[1024];
  GetFullPathNameA(path, 1024, buff, NULL);
  std::string ret = buff;
#endif

  return ret;
}

void split_string(const std::string & input, char s, mt_vector<std::string> & list, const bool allow_zero)
{
  list.resize(0);

  mt_vector<int> spos;
  for(size_t i=0; i<input.size(); i++)
    if(input[i] == s) spos.push_back(i);

  if(spos.size() == 0) {
    list.push_back(input);
    return;
  }

  // first item until first delimiter
  if(allow_zero || spos.front() > 0) {
    std::string & ns = list.push_back({});
    if(spos.front() > 0)
      ns.assign(input.begin(), input.begin() + spos.front());
  }

  // items between delimiters
  for(size_t i=0; i<spos.size()-1; i++) {
    size_t start = spos[i]+1, end = spos[i+1];
    if(allow_zero || (start < end)) {
      std::string & ns = list.push_back({});
      if(start < end)
        ns.assign(input.begin() + start, input.begin() + end);
    }
  }

  // last item from last delimiter till end
  if(allow_zero || (spos.back()+1) < input.size()) {
    std::string & ns = list.push_back({});
    if((spos.back()+1) < input.size())
      ns.assign(input.begin() + spos.back()+1, input.end());
  }
}

bool find_extension(std::string fname, std::string ext, size_t & pos)
{
  pos = fname.rfind(ext);
  return (pos != std::string::npos) && (fname.size() == (pos + ext.size()));
}

bool remove_extension(std::string & fname)
{
  size_t pos = fname.rfind(".");
  if(pos != std::string::npos && pos != 0) {
    fname.resize(pos);
    return true;
  }

  return false;
}

int swap_char(std::string & str, const char from, const char to)
{
  int cnt = 0;

  for(size_t i=0; i<str.size(); i++)
    if(str[i] == from) {
      str[i] = to;
      cnt++;
    }

  return cnt;
}

bool is_integer(const std::string & s)
{
  int buff;
  char tailbuff[1024];

  int nread = sscanf(s.c_str(), "%d%s", &buff, tailbuff);

  if(nread == 1) return true;
  else           return false;
}

bool is_float(const std::string & s)
{
  float buff;
  char tailbuff[1024];

  int nread = sscanf(s.c_str(), "%f%s", &buff, tailbuff);

  if(nread == 1) return true;
  else           return false;
}

void remove_in_place(std::string & str, const char c)
{
  size_t widx=0, ridx=0;
  for(ridx=0; ridx < str.size(); ridx++)
    if(str[ridx] != c) str[widx++] = str[ridx];

  str.resize(widx);
}

void remove_indent(std::string & str, const char c)
{
  size_t widx=0, ridx=0;
  while(str[ridx] == c) ridx++;

  for(; ridx < str.size(); ridx++)
    str[widx++] = str[ridx];

  str.resize(widx);
}

void remove_indent(char* str, const char c)
{
  size_t widx=0, ridx=0, l = strlen(str);

  while(str[ridx] == c) ridx++;

  for(; ridx < l; ridx++)
    str[widx++] = str[ridx];

  str[widx] = '\0';
}

void replace_multi_with_single(char *str, char c)
{
  char *dest = str;

  while (*str != '\0') {
    // skip when current and next chars are c
    while (*str == c && *(str + 1) == c)
      str++;

    *dest++ = *str++;
  }

  // Make sure the string is properly terminated
  *dest = '\0';
}

void replace_multi_with_single(std::string str, char c)
{
  size_t ridx = 0, widx = 0;

  while (ridx < str.size()) {
    // skip when current and next chars are c
    while ( ridx < str.size() && str[ridx] == c && str[ridx+1] == c)
      ridx++;

    str[widx++] = str[ridx++];
  }

  str.resize(widx);
}

