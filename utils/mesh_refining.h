/**
* @file mesh_refining.h
* @brief Mesh refinement classes.
* @author Aurel Neic
* @version
* @date 2017-06-02
*/
#ifndef _MESH_REFINING
#define _MESH_REFINING

#include "mesh_utils.h"
#include "topology_utils.h"
#include <set>

// The mesh quality threshold for resampling operations
#define RESAMPLE_QUAL_THR 0.8

/// inner iteration threshold for recomputing node and edge
/// indices
#define RECOMP_THR 64
// forward declarations for edge manager
class mt_edge_splitter;
class mt_edge_collapser;

/**
* @brief Class managing the edges of a mesh.
*
* It is used to conviniently refresh and access edge data.
*
*/
class mt_edge_manager
{
  friend class mt_edge_splitter;
  friend class mt_edge_collapser;

  protected:
  mt_meshdata &                        _mesh;          ///< the mesh we operate on
  // mt_mapping<mt_idx_t>                 _ele2edge;      ///< elements-to-edges mapping
  MT_MAP<mt_tuple<mt_idx_t>, mt_idx_t> _edge_map;      ///< edges of the provided tag regions

  public:
  /**
  * @brief Refresh the edge data-structs. Optionally remap mesh node indices.
  *
  * @param remap  Wheter to remap the mesh node indices.
  */
  void refresh_edge_data(bool remap)
  {
    if(remap)
      reindex_nodes(_mesh, false);

    _edge_map.clear();
    compute_edges(_mesh, _edge_map);
  }

  /// getter function for the edges of the initially provided tag regions
  const MT_MAP<mt_tuple<mt_idx_t>, mt_idx_t> & edges() const {
    return _edge_map;
  }

  /// constructor
  mt_edge_manager(mt_meshdata & inp_mesh) : _mesh(inp_mesh)
  {
    bool remap_nod = false;
    refresh_edge_data(remap_nod);
  }
};

/**
* @brief Class for splitting edges in a mesh.
*/
class mt_edge_splitter
{
  private:
  mt_edge_manager   & _edge_manager;  ///< reference to the edge management class
  PROGRESS<size_t>  * _prg;           ///< a pointer to the current progress
  size_t              _edgeidx;       ///< edge index counter for adding new edges
  size_t              _inner_iter;    ///< sweep index counter

  /**
  * @brief Split a line into 2 lines
  *
  * @param eidx      Index of the element to split.
  * @param ele_start Start index for writing element data.
  * @param con_start Start index for writing connectivity data.
  * @param edge      Edge to split along.
  * @param nv        New vertex index
  */
  void split_line(const mt_idx_t eidx,
                  const mt_idx_t ele_start,
                  const mt_idx_t con_start,
                  const mt_tuple<mt_idx_t> & edge,
                  const mt_idx_t nv);
  /**
  * @brief Split a triangle into 2 triangles along a given edge.
  *
  * @param eidx      Index of the element to split.
  * @param ele_start Start index for writing element data.
  * @param con_start Start index for writing connectivity data.
  * @param edge      Edge to split along.
  * @param nv        New vertex index
  */
  void split_tri(const mt_idx_t eidx,
                 const mt_idx_t ele_start,
                 const mt_idx_t con_start,
                 const mt_tuple<mt_idx_t> & edge,
                 const mt_idx_t nv);

  /**
  * @brief Split a tetra into 2 tetras along a given edge.
  *
  * @param eidx      Index of the element to split.
  * @param ele_start Start index for writing element data.
  * @param con_start Start index for writing connectivity data.
  * @param edge      Edge to split along.
  * @param nv        New vertex index
  */
  void split_tet(const mt_idx_t eidx,
                 const mt_idx_t ele_start,
                 const mt_idx_t con_start,
                 const mt_tuple<mt_idx_t> & edge,
                 const mt_idx_t nv);

  /**
  * @brief Compute one splitting sweep.
  *
  * @param split_edge  The edges we split in the current sweep.
  * @param split_elem  The elems we split in the current sweep.
  */
  void split_iter(mt_vector<mt_mixed_triple<mt_idx_t,mt_idx_t,float>> & split_edge,
                  MT_USET<mt_idx_t> & split_elem);

  public:

  /**
  * @brief Constructor
  *
  * @param [in] inp_mesh The mesh to operate on.
  */
  mt_edge_splitter(mt_edge_manager & inp_manager) :
                   _edge_manager(inp_manager),
                   _inner_iter(0)
  {
    _edgeidx = _edge_manager.edges().size();
  }

  /**
  * @brief Split mesh edges iteratively.
  *
  * @param [in] edge_set  The edges to split.
  * @param [in] inp_prg   Progress output class.
  */
  void operator()(mt_vector<edgeele> & edge_set, PROGRESS<size_t>* inp_prg);
};

/**
* @brief Class for collapsing edges
*/
class mt_edge_collapser
{
  private:
  mt_edge_manager  & _edge_manager;  ///< reference to the edge management class

  /**
   * @brief Apply the collapsing on the mesh
   *
   * @param nodmap    A map holding the nodes to remove, the indices that they will be removed with and the new coords.
   * @param rem_elem  The elements to remove because share a collapsed edge.
   */
  void collapse_iter(MT_MAP<mt_idx_t, mt_mixed_tuple<mt_idx_t, mt_point<mt_real> > > & nodmap,
      MT_USET<mt_idx_t> & rem_elem);

  /**
   * @brief Check if tetrahedral elements will deteriorate
   * because of the planned collapsing.
   *
   * @param edgeelems The elements to check.
   * @param nodmask   A nodal mask holding whether a node is in nodmap.
   * @param nodmap    The planned collapsing information.
   *
   * @return Whether the collapsing will yield bad elements.
   */
   bool check_bad_tet(const MT_USET<mt_idx_t> & edgeelems,
          const mt_vector<bool> & nodmask,
          const MT_MAP<mt_idx_t, mt_mixed_tuple<mt_idx_t, mt_point<mt_real> > > & nodmap);

  /**
   * @brief Check if triangle elements will deteriorate
   * because of the planned collapsing.
   *
   * @param edgeelems The elements to check.
   * @param nodmap    The planned collapsing information.
   *
   * @return Whether the collapsing will yield bad elements.
   */
  bool check_bad_tri(const MT_USET<mt_idx_t> & edgeelems,
      MT_MAP<mt_idx_t, mt_mixed_tuple<mt_idx_t, mt_point<mt_real> > > & nodmap);

  public:
  bool collapse_along_fixed = false;

  /// constructor
  mt_edge_collapser(mt_edge_manager & inp_manager): _edge_manager(inp_manager)
  {}

  /**
  * @brief Try to collapse a given set of edges on a volumetric (tet) mesh.
  *
  * @param edge_set          The edges to collapse.
  * @param surf_nodes        The surface nodes (internal and external surfaces).
  * @param surf_nrml         The surface node normals.
  * @param fixed_surf_nodes  Surface nodes that cannot be moved.
  * @param surf_corr         The surface normal correlation coefficient. (0 = no correlation, 1 = full correlation).
  * @param qual_thr          Element quality threshold used to preserve mesh quality.
  * @param inp_prg           Progress display class.
  */
  void operator()(std::set<edgeele, edge_len_asc> & edge_set,
                  const mt_vector<bool> & surf_nodes,
                  const mt_vector<mt_real> & surf_nrml,
                  const mt_vector<bool> & fixed_surf_nodes,
                  const mt_real surf_corr,
                  PROGRESS<size_t>* prg);

  /**
  * @brief Try to collapse a given set of edges on a surface (tri) mesh.
  *
  * @param edge_set          The edges to collapse.
  * @param surf_nrml         The surface node normals.
  * @param fixed_surf_nodes  Surface nodes that cannot be moved.
  * @param surf_corr         The surface normal correlation coefficient. (0 = no correlation, 1 = full correlation).
  * @param inp_prg           Progress display class.
  */
  void operator()(std::set<edgeele, edge_len_asc> & edge_set,
                  const mt_vector<mt_real> & surf_nrml,
                  const mt_vector<bool> & fixed_surf_nodes,
                  const mt_real surf_corr,
                  PROGRESS<size_t>* prg);
};

#define SMTH_BASE 0.14f
#define SMTH_CHANGE 0.95f
#define ITER_BASE 40
#define ITER_THRS 4

bool non_uniform_splitting(mt_meshdata & mesh,
                           const MT_USET<mt_tag_t> & tags,
                           mt_edge_splitter & splitter,
                           mt_edge_manager & edge_manager,
                           const mt_vector<bool> & fixed_nod,
                           const mt_real max);

bool uniform_splitting(mt_meshdata & mesh,
                       const MT_USET<mt_tag_t> & tags,
                       mt_edge_manager & edge_manager,
                       const mt_real max);

void resample_surface(mt_meshdata & mesh,
                      const mt_real min,
                      const mt_real max,
                      const MT_USET<mt_tag_t> & tags,
                      const mt_real surf_corr,
                      const bool fix_bnd,
                      const bool uniform_splitting,
                      const bool dosmooth,
                      const float sharp_edge);

void resample_mesh(mt_meshdata & mesh,
                   const mt_real min,
                   const mt_real max,
                   const mt_vector<MT_USET<mt_tag_t>> & alltags,
                   const MT_USET<mt_tag_t> & tags,
                   const mt_real surf_corr,
                   const bool fix_bnd,
                   const bool do_conv,
                   bool unif_split,
                   const bool dosmooth, 
                   const float sharp_edge);

void surface_intersect_splitting(mt_meshdata & mesh,
                                 mt_edge_splitter & splitter,
                                 mt_edge_manager & edge_manager,
                                 const kdtree & tree,
                                 const MT_USET<mt_idx_t> & fixed_nod,
                                 const float prec,
                                 const bool final_split);

void surface_distance_splitting(mt_meshdata & mesh,
                                mt_edge_splitter & splitter,
                                mt_edge_manager & edge_manager,
                                const kdtree & bdry_tree,
                                const kdtree & tree,
                                const mt_mask & bdry_mask,
                                const float bdry_res,
                                const float final_res);


/**
* @brief Descretize line cables of a PS to as many intervals as required for
*        an interval size matching given size parameter.
*
* @param [in, out] ps     Purkinje system
* @param [in]      cables The cables we want to refine.
* @param [in]      size   Required interval size.
*/
void resample_purkinje(struct mt_psdata & ps, mt_vector<int> & cables, float size);


mt_real avrg_purk_resolution(struct mt_psdata & ps, mt_vector<int> & eval_cab);

inline mt_real avrg_purk_resolution(struct mt_psdata & ps)
{
  mt_vector<int> eval_cab;
  range<int>(0, int(ps.cables.size()), eval_cab);

  return avrg_purk_resolution(ps, eval_cab);
}

/// get lengths between the segments of the cables. last entry is total length.
void get_cable_displacements(const mt_pscable & cab, mt_vector<mt_real> & dsp);

/**
* @brief resample only one cable, used by resample_purkinje.
*
* @param cab         input cable.
* @param dsp         interval length displacements of input cable. computed with get_cable_displacements.
* @param xyzbuff     auxilliary buffer we dont want to re-allocate all the time
* @param numint      number of intervals to resample to.
*/
void resample_purk_cable(mt_pscable & cab,
                         const mt_vector<mt_real> & dsp,
                         mt_vector<mt_real> & xyzbuff,
                         const int numint);

/**
 * @brief Make a mt_psdata struct of minimal size. Line elements are reduced to 2 points.
 * 
 * @param ps The PS data we minimize.
 */
void minimize_purkinje(struct mt_psdata & ps);

/**
* @brief Check if the points of a cable are located along a line.
*
* @param [in] cab  The cable.
* @param [in] thr  Threshold for the scalar product testing alignment.
*                  Should be in (0,1), with 0 = orthogonality, and 1 = identical alignment
*
* @return True for line, else False.
*/
bool cable_is_line(const mt_pscable & cab, mt_real thr = 0.9);

#endif

