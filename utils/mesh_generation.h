#ifndef _MESH_GENERATION_H
#define _MESH_GENERATION_H

#include "mt_utils_base.h"
#include "mesh_utils.h"
#include "topology_utils.h"
#include "mesh_smoothing.h"
#include "tetgen/tetgen.h"

/**
* @brief Convert a meshtool mesh struct into a tetgenio datastruct
*/
void mesh_to_tetgenio(const mt_meshdata & mesh, tetgenio & io);

/**
* @brief Convert a tetgenio datastruct into a meshtool mesh struct
*/
void tetgenio_to_mesh(const tetgenio & io, mt_meshdata & mesh);

/**
* @brief Wrapper function that call tetgen to mesh a surface into a volume.
*
* @param surfmesh           The input surface mesh.
* @param volmesh            The output volumetric mesh.
* @param region_attributes  Five values per region. Region volume seed point, region tag and element volume constraint.
* @param holes_xyz          Hole seeding points that get passed to tetgen.
* @param tetgen_qual        Desired mesh quality. 0 = low, 1 = mid, 2 = high.
* @param size_field         Optional space dependent sizing function (e.g. average edge length in each node).
* @param preserve_boundary  Whether to preserve the boundary discretization.
*/
void mesh_with_tetgen(mt_meshdata & surfmesh,
                      mt_meshdata & volmesh,
                      mt_vector<double> & region_attributes,
                      mt_vector<mt_real> & holes_xyz,
                      int tetgen_qual,
                      mt_vector<mt_real> * size_field,
                      bool preserve_boundary);

void convex_hull_with_tetgen(const mt_vector<vec3r> & pts, mt_meshdata & surfmesh);

  void generate_hex_mesh(const mt_triple<mt_vector<mt_real>> & layout,
                         const mt_triple<mt_real> & res,
                         mt_meshdata& mesh);

/**
* @brief Generate Prism boundary layers from a triangle input surface.
*
* @param outer_surf         The input surface.
* @param num_layers         The number of boundary layers to generate.
* @param transition_scale   The scale to the normal direction tranisition distance.
* @param transition_inc     The (linear) increment in normal transition.
* @param sizing_mode        The sizing mode. 0 = globally edge length average. 1 = local sizing field.
* @param symmetric          Whether the boundary is applied symmetrically inside and outside the input surface.
* @param inner_surf         The triangular final inner surface.
* @param output_mesh        The volumetric output mesh holding the boundary layers.
*/
void generate_boundary_layers(const mt_meshdata & outer_surf,
                              const int num_layers,
                              const mt_real transition_scale,
                              const mt_real transition_inc,
                              const int sizing_mode,
                              const bool symmetric,
                              mt_meshdata & inner_surf,
                              mt_meshdata & output_mesh);

/// boundary layer version with absolute transition control
void generate_boundary_layers(const mt_meshdata & outer_surf,
                              const int num_layers,
                              const mt_real transition,
                              const mt_real transition_inc,
                              const bool symmetric,
                              mt_meshdata & inner_surf,
                              mt_meshdata & output_mesh);

void generate_bbox_mesh(const bbox & box, const float res, mt_meshdata & out_mesh);

inline void generate_bbox_mesh(const mt_meshdata & inp_mesh, const float res, const float scale, mt_meshdata & out_mesh)
{
  bbox imesh_bbox;
  generate_bbox(inp_mesh.xyz, imesh_bbox);
  cartesian_csys::bbox_scale_centered(imesh_bbox, scale);

  generate_bbox_mesh(imesh_bbox, res, out_mesh);
}

void generate_box_mesh(float width, float height, float depth, float res, mt_meshdata & mesh);

void mesh_box_with_tetgen(const bbox & box,
                          mt_meshdata & volmesh,
                          const float res);

#endif

