/**
* @file io_utils.h
* @brief General IO utils.
* @author Aurel Neic
* @version
* @date 2016-12-13
*/



#ifndef _IO_UTILS
#define _IO_UTILS

#define BIN_HDR_SIZE 1024

#if __BYTE_ORDER == __LITTLE_ENDIAN
#define htobe(x) byte_swap(x)
#define betoh(x) byte_swap(x)
#define htole(x) (x)
#define letoh(x) (x)
#define MT_ENDIANNESS 0
#else
#define htobe(x) (x)
#define betoh(x) (x)
#define htole(x) byte_swap(x)
#define letoh(x) byte_swap(x)
#define MT_ENDIANNESS 1
#endif

#define CRP_READ_ELEM 1
#define CRP_READ_PTS  2
#define CRP_READ_LON  4

#define MT_FOPEN_READ   "rb"
#define MT_FOPEN_WRITE  "wb"
#define MT_FOPEN_APPEND "ab"

#include "mt_utils_base.h"
#include "string_utils.h"

static const std::string carp_txt_fmt     = "carp_txt";
static const std::string carp_bin_fmt     = "carp_bin";
static const std::string vtk_fmt          = "vtk";
static const std::string vtk_bin_fmt      = "vtk_bin";
static const std::string vtu_fmt          = "vtu";
static const std::string vtk_polydata_fmt = "vtk_polydata";
static const std::string stellar_fmt      = "stellar";
static const std::string purk_fmt         = "purk";
static const std::string mmg_fmt          = "mmg";
static const std::string bw_fmt           = "bw_tri";
static const std::string netgen_fmt       = "neu";
static const std::string obj_fmt          = "obj";
static const std::string off_fmt          = "off";
static const std::string stl_fmt          = "stl";
static const std::string gmsh_fmt         = "gmsh";
static const std::string vcflow_fmt       = "vcflow";
static const std::string ens_txt_fmt      = "ensight_txt";
static const std::string ens_bin_fmt      = "ensight_bin";

#define  CARPTXT_ELEM_EXT  ".elem"
#define  CARPTXT_LON_EXT   ".lon"
#define  CARPTXT_PTS_EXT   ".pts"
#define  CARPBIN_ELEM_EXT  ".belem"
#define  CARPBIN_LON_EXT   ".blon"
#define  CARPBIN_PTS_EXT   ".bpts"
#define  VTK_EXT           ".vtk"
#define  NRRD_EXT          ".nrrd"
#define  ITK_EXT           ".itk"
#define  VTU_EXT           ".vtu"
#define  MMG_EXT           ".mesh"
#define  NETGEN_EXT        ".neu"
#define  PURK_EXT          ".pkje"
#define  EIDX_EXT          ".eidx"
#define  NOD_EXT           ".nod"
#define  SURF_EXT          ".surf"
#define  NBC_EXT           ".neubc"
#define  VTX_EXT           ".vtx"
#define  SURF_VTX_EXT      SURF_EXT VTX_EXT
#define  DAT_EXT           ".dat"
#define  TDAT_EXT          ".tdat"
#define  BDAT_EXT          ".bdat"
#define  VEC_EXT           ".vec"
#define  ADJ_EXT           ".adj"
#define  IGB_EXT           ".igb"
#define  DYNPT_EXT         ".dynpt"
#define  OBJ_EXT           ".obj"
#define  OFF_EXT           ".off"
#define  STL_EXT           ".stl"
#define  GMSH_EXT          ".msh"
#define  VCFLOW_ELEM_EXT   "_connectivity.bin"
#define  VCFLOW_PTS_EXT    "_coordinates.bin"
#define  VCFLOW_ADJ_EXT    "_adjacency.bin"
#define  UVC_EXT           ".uvc"
#define  UAC_EXT           ".uac"
#define  UCC_EXT           ".ucc"
#define  IUCC_EXT          ".iucc"
#define  UVC_PTS_EXT       UVC_EXT "_pts"
#define  UAC_PTS_EXT       UAC_EXT "_pts"
#define  UCC_PTS_EXT       UCC_EXT "_pts"
#define  HRT_PTS_EXT       ".hpts"
#define  ENSIGHT_MESH_EXT  ".geo"
#define  ENSIGHT_CASE_EXT  ".case"
#define  ENSIGHT_DATA_EXT  ".ens"
#define  FULL_CON_EXT      ".fcon"
#define  SPLIT_EXT         ".splt"
#define  TXT_TAGS_EXT      ".tag"
#define  BIN_TAGS_EXT      ".btag"

#define  AUG_EXT           ".aug"
#define  PAR_EXT           ".par"
#define  YAML_EXT          ".yaml"
#define  JSON_EXT          ".json"
#define  PATH_EXT          ".path"
#define  PNG_EXT           ".png"
#define  TEXT_EXT          ".txt"
#define  TRC_EXT           ".trc"
#define  TRFM_EXT          ".trfm"
#define  CSV_EXT           ".csv"

// input / output formats
static const std::string input_formats = carp_txt_fmt+", "+carp_bin_fmt+", "+vtk_fmt+", "+
        vtk_bin_fmt +", "+mmg_fmt+", "+netgen_fmt+", "+purk_fmt+", "+obj_fmt+", "+stl_fmt+", "+
        off_fmt+", "+gmsh_fmt+", "+stellar_fmt+", "+vcflow_fmt+", "+bw_fmt;

static const std::string output_formats =carp_txt_fmt+", "+carp_bin_fmt+", "+vtk_fmt+", "+
        vtk_bin_fmt+", "+vtk_polydata_fmt + ", "+mmg_fmt+", "+netgen_fmt+", "+obj_fmt+", "+stl_fmt+", "+
        off_fmt+", "+stellar_fmt+", "+vcflow_fmt+", "+ens_txt_fmt;

// include igb function declarations
#include "igb_utils.hpp"

size_t mt_ftell(FILE* fd);
void mt_fseek(FILE* fd, long delta, int start_flag);

/**
* @brief Convert an element type string into an enum
*
* @param eletype element type string
*
* @return element type enum
*/
elem_t getElemTypeID(char *eletype);

template<typename T>
T byte_swap(T in)
{
  T out; // output buffer

  char* inp   = ( char* ) (& in );
  char* outp  = ( char* ) (& out);
  size_t size = sizeof(T);

  switch(size)
  {
    case 4:
      outp[0] = inp[3];
      outp[1] = inp[2];
      outp[2] = inp[1];
      outp[3] = inp[0];
      break;

    case 2:
      outp[0] = inp[1];
      outp[1] = inp[0];
      break;

    case 8:
      outp[0] = inp[7], outp[1] = inp[6];
      outp[2] = inp[5], outp[3] = inp[4];
      outp[4] = inp[3], outp[5] = inp[2];
      outp[6] = inp[1], outp[7] = inp[0];
      break;

    default:
      for(size_t i=0; i<size; i++)
        outp[i] = inp[size-1-i];
  }

  return out;
}


/// give explanation why a file open failed
void treat_file_open_error(const std::string & file);
/// some general error happend that we describe in msg
void treat_file_error(const char* msg, FILE* fd);

/**
* @brief Check if file exists.
*
* @param filename [in]   The file to check
*
* @return True if file exists.
*/
bool file_exists(const std::string & filename);

/// get the size of a file in bytes
size_t file_size(FILE* fd);
size_t file_size(const char* file);

bool file_is_dir(const char* filename);

inline bool file_is_dir(const std::string & filename)
{
  return file_is_dir(filename.c_str());
}


bool file_is_regfile(const char* filename);

inline bool file_is_regfile(const std::string & filename)
{
  return file_is_regfile(filename.c_str());
}

/// create the directory specified by path
bool make_dir(const std::string & path);

/// construct a path relative to a root path
std::string make_path_relative(std::string root, std::string path);

/// file copy
bool copy_file(const std::string & in, const std::string & out);

bool remove_file(const std::string& path);
bool remove_directory(const std::string& path, const bool verbose);

void generate_default_fib(size_t numelem, mt_vector<mt_fib_t> & lon);

/**
 * The binary_read procedure reads binary data from a file into a memory block.
 * \param vec Output: Vector we read into.
 * \param filename Input: Name of the input file.
 * \param seek Input: Offset into the data file
 */
template<class T> inline
void binary_read(mt_vector<T> & vec, const std::string filename, const int seek = 0)
{
  FILE* fd = fopen(filename.c_str(), MT_FOPEN_READ);
  if(fd == NULL){
    treat_file_open_error(filename);
    return;
  }

  if(vec.size() == 0) {
    size_t fsize = file_size(fd);
    size_t vsize = fsize / sizeof(T);
    if(fsize % sizeof(T)) {
      fprintf(stderr, "%s error: File size and requested datatype dont match.\n", __func__);
      return;
    }
    vec.resize(vsize);
  }

  if(seek)
    mt_fseek(fd, seek, SEEK_SET);

  fread(vec.data(), sizeof(T), vec.size(), fd);
  fclose(fd);
}

/**
 * The binary_write procedure writes binary data to a file from a memory block.
 * \param _First Input: Iterator pointing to the first element.
 * \param _Last Input: Iterator pointing past the last element.
 * \param _Filename Input: Name of the output file.
 */
template<class T>
inline void binary_write(T* _First, T* _Last, const std::string _Filename)
{
  FILE* fd = fopen(_Filename.c_str(), MT_FOPEN_WRITE);
  if(!fd){
    treat_file_open_error(_Filename);
    return;
  }

  fwrite((char*)&*_First, (_Last - _First) * sizeof(*_First), 1, fd);
  fclose(fd);
}

/**
* @brief Write element data in text CARP format.
*
* @param mesh [in]  The mesh.
* @param file [in]  Element file name.
*/
void writeElements(mt_meshdata & mesh, std::string file);


/**
* @brief Write element data in binary CARP format.
*
* @param mesh [in]  The mesh.
* @param file [in]  Element file name.
*/
void writeElementsBinary(mt_meshdata & mesh, const std::string file);

/**
* @brief Read element data from text CARP format.
*
* @param mesh [out] The mesh.
* @param file [in]  Element file name.
*/
void readElements(mt_meshdata & mesh, std::string file);

/**
* @brief Read element data from binary CARP format.
*
* @param mesh [out] The mesh.
* @param file [in]  Element file name.
*/
void readElementsBinary(mt_meshdata & mesh, std::string file);

void readElements_general(mt_meshdata & mesh, std::string basename);

/**
* @brief Read element-tags.
*
* @param etags [out] preallocated vector to read the tag-data into
* @param file  [in]  Element-tags file name.
*/
void readElementTags_general(mt_vector<mt_tag_t> & etags, std::string file);

size_t readNumPoints(std::string file);

void readPoints(mt_vector<mt_real> & xyz, std::string file);
void readPointsBinary(mt_vector<mt_real> & xyz, std::string file);
void readPoints_general(mt_vector<mt_real> & xyz, std::string basename);

/**
* @brief Read the UVC coordinates of a mesh and store them in two vectors (one for LV, one for RV).
*
* @param xyz_lv   uvc coordinates of LV
* @param xyz_rv   uvc coordinates of RV
* @param pos_lv   index of LV uvc coordinates w.r.t. global UVC list
* @param pos_rv   index of RV uvc coordinates w.r.t. global UVC list
* @param idx_to_uvc_points  map between global index and local LV/RV list index
* @param file     The file we read from.
*/
void readUVCPoints(mt_vector<mt_real> & uvc,
                   mt_mask & is_lv,
                   std::string filename);

void readCardiacCoordinates(mt_vector<mt_real>& ucc,
                            mt_vector<int8_t>& ventricle,
                            const std::string& filename);

void writePoints(mt_vector<mt_real> & xyz, std::string file);
void writePointsBinary(mt_vector<mt_real> & xyz, std::string file);

int readFibers(mt_vector<mt_fib_t> & lon, size_t numelem, std::string file);
int readFibersBinary(mt_vector<mt_fib_t> & lon, size_t numelem, std::string file);
void readFibers_general(mt_vector<mt_fib_t> & lon, size_t numelem, std::string basename);

void writeFibers(mt_vector<mt_fib_t> & lon, size_t numelem, std::string file);
void writeFibersBinary(mt_vector<mt_fib_t> & lon, size_t numelem, std::string file);

void write_surf(const mt_vector<mt_cnt_t> & cnt, const mt_vector<mt_idx_t> & con, const std::string filename);

void read_vtx(mt_vector<mt_idx_t> & nod, std::string filename);

#define WRITE_VTX_NOHEAD 0
#define WRITE_VTX_EXTRA  1
#define WRITE_VTX_INTRA  2
template<class CONTAINER>
void write_vtx(const CONTAINER & nod, const std::string filename, short write_header = WRITE_VTX_EXTRA)
{
  FILE* file = fopen(filename.c_str(), MT_FOPEN_WRITE);
  if(file == NULL) {
    treat_file_open_error(filename);
  } else {
    // Header
    if(write_header) {
      fprintf(file, "%zu\n", nod.size());

      if(write_header == WRITE_VTX_INTRA) {
        fprintf(file, "intra\n");
      } else {
        fprintf(file, "extra\n");
      }
    }

    // Content
    auto it = nod.begin();
    while(it != nod.end()) {
      fprintf(file, "%ld\n", long(*it));
      ++it;
    }

    fclose(file);
  }
}

void read_nbc(nbc_data & nbc, mt_vector<mt_idx_t> & con, std::string filename);
void write_nbc(const nbc_data & nbc, const mt_vector<mt_idx_t> & con, int npts, int nelem, const std::string filename);


/**
* @brief Write a vector to disk as ascii data.
*
* @tparam T   Data type.
* @param [in] vec  Data vector.
* @param [in] file File name.
*/
template<class T>
void write_vector_ascii(const mt_vector<T> & vec, const std::string file, short dpn = 1)
{
  FILE* out_file = fopen(file.c_str(), MT_FOPEN_WRITE);
  if(out_file == NULL) {
    treat_file_open_error(file);
  } else {
    char msg[1024];
    sprintf(msg, "Writing %s: ", file.c_str());
    PROGRESS<size_t> prg(vec.size(), msg);

    for (size_t i=0; i<vec.size() / dpn; i++ ) {
      for(short j=0; j<dpn-1; j++)
        fprintf(out_file, "%g ", double(vec[i*dpn + j]) );
      fprintf(out_file, "%g\n", double(vec[i*dpn + (dpn-1)]) );
      prg.next();
    }

    prg.finish();
    fclose(out_file);
  }
}
template<class T>
void write_vector_ascii(const mt_vector<mt_point<T> > & vec, const std::string file, const bool write_num=false)
{
  FILE* out_file = fopen(file.c_str(), MT_FOPEN_WRITE);
  if(out_file == NULL) {
    treat_file_open_error(file);
  } else {
    char msg[1024];
    sprintf(msg, "Writing %s: ", file.c_str());
    PROGRESS<size_t> prg(vec.size(), msg);

    if (write_num)
      fprintf(out_file, "%ld\n", (long int) vec.size());

    for (size_t i=0; i<vec.size(); i++ ) {
      fprintf(out_file, "%g %g %g\n", double(vec[i].x), double(vec[i].y), double(vec[i].z));
      prg.next();
    }

    prg.finish();
    fclose(out_file);
  }
}

/**
* @brief write a binary data vector.
*
* @tparam T              vector data type.
* @param [in] vec        vector we write from.
* @param [in] file       file we write to.
* @param [in] writeFloat whether we want to interpret the data as float64 or int32
*/
template<class T>
void write_vector_bin(const mt_vector<T> & vec, const std::string file, bool writeFloat = true)
{
  if(writeFloat) {
    mt_vector<double> buff;
    buff.assign(vec.begin(), vec.end());

    binary_write(buff.begin(), buff.end(), file);
  } else {
    mt_vector<int32_t> buff;
    buff.assign(vec.begin(), vec.end());

    binary_write(buff.begin(), buff.end(), file);
  }
}

template<class T>
void write_vector(const mt_vector<T> & vec, const std::string file, bool writeFloat = true)
{
  if(endswith(file, BDAT_EXT)) {
    write_vector_bin(vec, file, writeFloat);
  } else {
    write_vector_ascii(vec, file);
  }
}

/**
* @brief Read an ascii data vector
*
* @tparam T     Integer or floating point type
*
* @param [out] vec       Data vector.
* @param [in]  file      File to read from
* @param [in]  readFloat Whether to read floating point or integer values.
*/
template<class T>
void read_vector_ascii(mt_vector<T> & vec, const std::string file, bool readFloat = true)
{
  FILE* in_file = fopen(file.c_str(), MT_FOPEN_READ);
  if(in_file == NULL) {
    treat_file_open_error(file);
    return;
  }

  const int bufsize = 2056;
  char buffer[bufsize+1];
  char* ptr;
  double fbuf[3];
  long int ibuf[3];
  short entr_per_line = 0, byte_per_line = 0;
  size_t num_lines = 0;

  size_t size_of_file = file_size(in_file);

  if(!size_of_file) {
    fprintf(stderr, "%s error: file %s is empty! Aborting!\n", __func__, file.c_str());
    return;
  }

  // first get the number of lines and entries per line
  ptr = fgets( buffer, bufsize, in_file);
  if(ptr != NULL) {
    byte_per_line = strlen(ptr);
    entr_per_line = sscanf(buffer, "%lf %lf %lf", fbuf, fbuf+1, fbuf+2);
  } else {
    fprintf(stderr, "%s error: cannot read from file %s! Aborting!\n", __func__, file.c_str());
    return;
  }

  // we estimate file size to be able to have a progress bar
  size_t num_lines_estimate = (size_of_file / byte_per_line) * 1.25f;
  char msg[1024];
  sprintf(msg, "Reading %s: ", file.c_str());
  PROGRESS<size_t> prg(num_lines_estimate*2, msg);

  if(entr_per_line == 0) {
    fprintf(stderr, "%s error: Could not determine number of entries per line in file %s.\n\n",
            __func__, file.c_str());
    fclose(in_file);
    return;
  }

  while(ptr) {
    num_lines++;
    ptr = fgets( buffer, bufsize, in_file);
    prg.next();
  }

  vec.resize(num_lines * entr_per_line);
  mt_fseek(in_file, 0L, SEEK_SET);

  // read file into a list so that we dont need to know its size
  if(readFloat) {
    for(size_t l=0, widx=0; l<num_lines; l++)
    {
      fgets( buffer, bufsize, in_file);
      short nread = sscanf(buffer, "%lf %lf %lf", fbuf, fbuf+1, fbuf+2);
      for(short i=0; i < nread; i++)
        vec[widx++] = T(fbuf[i]);

      prg.next();
    }
  }
  else {
    for(size_t l=0, widx=0; l<num_lines; l++)
    {
      fgets( buffer, bufsize, in_file);
      short nread = sscanf(buffer, "%ld %ld %ld", ibuf, ibuf+1, ibuf+2);
      for(short i=0; i < nread; i++)
        vec[widx++] = T(ibuf[i]);

      prg.next();
    }
  }

  prg.finish();
  fclose(in_file);
}

/**
* @brief Read a binary data vector.
*
* @tparam T              vector data type.
* @param [out] vec       vector we read to.
* @param [in]  file      file we read from.
* @param [in]  readFloat whether we want to interpret the data as float64 or int32
*/
template<class T>
void read_vector_bin(mt_vector<T> & vec, const std::string file, bool readFloat = true)
{
  if(readFloat) {
    mt_vector<double> buff;
    binary_read(buff, file);
    vec.assign(buff.begin(), buff.end());
  } else {
    mt_vector<int32_t> buff;
    binary_read(buff, file);
    vec.assign(buff.begin(), buff.end());
  }
}

template<class T>
void read_vector(mt_vector<T> & vec, const std::string file, bool readFloat = true)
{
  if(endswith(file, DAT_EXT)) {
    read_vector_ascii(vec, file, readFloat);
  } else if (endswith(file, BDAT_EXT)) {
    read_vector_bin(vec, file, readFloat);
  } else {
    fprintf(stderr, "%s error: File %s has unknown extension! Aborting!\n",
            __func__, file.c_str());
    return;
  }
}

template<class S> inline
void write_vtx_and_data(const mt_vector<mt_idx_t> & nod, const mt_vector<S> & dat,
                        const std::string filename, bool write_header)
{
  FILE* file = fopen(filename.c_str(), MT_FOPEN_WRITE);
  if(file == NULL) {
    treat_file_open_error(filename);
  } else {
    // Header
    if(write_header) {
      fprintf(file, "%lu\n", (long unsigned int) nod.size());
      fprintf(file, "extra\n");
    }

    // Content
    for(size_t i=0; i<nod.size(); i++)
      fprintf(file, "%ld %lf\n", long(nod[i]), double(dat[i]));

    fclose(file);
  }
}

template<class S> inline
void read_vtx_and_data(const std::string filename, mt_vector<mt_idx_t> & nod, mt_vector<S> & dat)
{
  FILE* file = fopen(filename.c_str(), MT_FOPEN_READ);
  if(file == NULL) {
    treat_file_open_error(filename);
  } else {
    const int bufsize = 2048;
    char buffer[bufsize+1];

    long int ibuf;
    double   fbuf;
    long int numentries = -1;

    nod.resize(0);
    dat.resize(0);

    fgets(buffer, bufsize, file);
    short nread = sscanf(buffer, "%ld %lf", &ibuf, &fbuf);

    if(nread == 1) {
      fgets(buffer, bufsize, file);
      if(strcmp(buffer, "extra") == 0 ||strcmp(buffer, "intra") == 0) {
        // we have a header, set
        numentries = ibuf;
        nod.reserve(numentries);
        dat.reserve(numentries);
      }
    } else if(nread == 2) {
      nod.push_back(ibuf);
      dat.push_back(fbuf);
    } else {
      fprintf(stderr, "%s error: Cannot parse file %s! Aborting!\n", __func__, filename.c_str());
      return;
    }

    do {
      char* ptr = fgets(buffer, bufsize, file);
      if(ptr) {
        nread = sscanf(buffer, "%ld %lf", &ibuf, &fbuf);
        if(nread == 2) {
          nod.push_back(ibuf);
          dat.push_back(fbuf);
        }
      } else {
        nread = 0;
      }
    } while(nread == 2);

    fclose(file);
  }
}

/**
* @brief Write a mesh in a given format.
*
* If the given format is an empty string, the function defaults to carp_txt.
* The output format is compared against the *_fmt string constants from
* mesh_mode_funcs.h.
*
* @param mesh     [in]  The mesh
* @param format   [in]  The output format.
* @param basename [in]  Basename of the mesh to output.
*/
void write_mesh_selected(mt_meshdata & mesh,
                         std::string format,
                         std::string basename);
/**
* @brief Read a mesh in a given format.
*
* If the given format is an empty string, the function defaults to carp_txt.
* The output format is compared against the *_fmt string constants from
* mesh_mode_funcs.h.
*
* @param mesh     [out] The mesh
* @param format   [in]  The output format.
* @param basename [in]  Basename of the mesh to output.
* @param readmask [in]  Bitmask for what to read (only affect CARP formats).
*/
void read_mesh_selected(mt_meshdata & mesh,
                        std::string format,
                        std::string basename,
                        short readmask = (CRP_READ_ELEM | CRP_READ_PTS | CRP_READ_LON) );

/**
* @brief Read the data of a purkinje file.
*
* @param [out] ps   PS data struct
* @param [in]  file PS data file name.
*/
void read_purkinje(mt_psdata & ps, std::string file);
/**
* @brief Write the data of a purkinje file.
*
* @param [in]  ps   PS data struct
* @param [in]  file PS data file name.
*
* @post  PS has been writen to disk.
*/
void write_purkinje(mt_psdata & ps, std::string file);

/**
* @brief Class for filename and format management
*/
class mt_filename {
  public:
  std::string base;   ///< the filename without extension
  std::string format; ///< the file format

  /// construct empty filename
  mt_filename(){}
  /// construct file, calls assign()
  mt_filename(std::string fname, std::string fmt);

  /// construct file, format may be empty
  void assign(std::string fname, std::string fmt);

  /// check whether filename is set
  bool isSet() const;
  /// check wheterh file already exists
  bool exists() const;

  // const char* path(bool must_exist = true);

  /// get path to file for eading. it must exist or we get NULL.
  const char* read_path() const;
  /// get path to file for writing. it may not exist yet.
  const char* write_path() const;
};

/// chars used in the base64 encoding
static const std::string base64_chars =
             "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
             "abcdefghijklmnopqrstuvwxyz"
             "0123456789+/";


/// test if a char is base64 encoded
static inline bool is_base64(unsigned char c) {
  return (isalnum(c) || (c == '+') || (c == '/'));
}

/**
* @brief Encode a data vector to base64 encoding.
*
* @tparam T   Data type of the input data
* @param in   Input data vector
* @param out  Output data vector
*/
template<class T>
void encode_base64(const mt_vector<T> & in, mt_vector<unsigned char> & out)
{
  const unsigned char* input  = (const unsigned char*) in.data();
  size_t               in_len = in.size() * sizeof(T);

  int i = 0;
  int j = 0;
  unsigned char buff3[3], buff4[4];

  size_t widx = 0;
  out.resize((in.size()*sizeof(T) + 2) / 3 * 4 + 2);

  while (in_len--)
  {
    buff3[i++] = *(input++);
    if (i == 3) {
      buff4[0] = (buff3[0] & 0xfc) >> 2;
      buff4[1] = ((buff3[0] & 0x03) << 4) + ((buff3[1] & 0xf0) >> 4);
      buff4[2] = ((buff3[1] & 0x0f) << 2) + ((buff3[2] & 0xc0) >> 6);
      buff4[3] = buff3[2] & 0x3f;

      for(i = 0; (i <4) ; i++)
        out[widx++] = base64_chars[buff4[i]];
      i = 0;
    }
  }

  if(i) {
    for(j = i; j < 3; j++) buff3[j] = '\0';

    buff4[0] = (buff3[0] & 0xfc) >> 2;
    buff4[1] = ((buff3[0] & 0x03) << 4) + ((buff3[1] & 0xf0) >> 4);
    buff4[2] = ((buff3[1] & 0x0f) << 2) + ((buff3[2] & 0xc0) >> 6);
    buff4[3] = buff3[2] & 0x3f;

    for (j = 0; (j < i + 1); j++)
      out[widx++] = base64_chars[buff4[j]];

    while((i++ < 3)) out[widx++] = '=';
  }

  out[widx++] = '\0';
  out.resize(widx);
}

/**
* @brief Decode a data vector from base64 encoding.
*
* @tparam T   Data type of the output data
* @param in   Input data vector
* @param out  Output data vector
*/
template<class T>
void decode_base64(const mt_vector<unsigned char> & in, mt_vector<T> & out)
{
  size_t in_len = in.size();
  int i = 0;
  int j = 0;
  size_t ridx = 0, widx = 0;
  unsigned char buff3[3], buff4[4];

  out.resize( (in.size() + sizeof(T) - 1) / sizeof(T) );
  unsigned char * op = (unsigned char*) out.data();

  while (in_len-- && ( in[ridx] != '=') && is_base64(in[ridx])) {
    buff4[i++] = in[ridx++];

    if (i ==4) {
      for (i = 0; i <4; i++) buff4[i] = base64_chars.find(buff4[i]);

      buff3[0] = (buff4[0] << 2) + ((buff4[1] & 0x30) >> 4);
      buff3[1] = ((buff4[1] & 0xf) << 4) + ((buff4[2] & 0x3c) >> 2);
      buff3[2] = ((buff4[2] & 0x3) << 6) + buff4[3];

      for (i = 0; (i < 3); i++) op[widx++] = buff3[i];
      i = 0;
    }
  }

  if(i) {
    for (j = i; j <4; j++) buff4[j] = 0;
    for (j = 0; j <4; j++) buff4[j] = base64_chars.find(buff4[j]);

    buff3[0] = (buff4[0] << 2) + ((buff4[1] & 0x30) >> 4);
    buff3[1] = ((buff4[1] & 0xf) << 4) + ((buff4[2] & 0x3c) >> 2);
    buff3[2] = ((buff4[2] & 0x3) << 6) + buff4[3];

    for (j = 0; (j < i - 1); j++) op[widx++] = buff3[j];
  }
  out.resize(widx / sizeof(T));
}

template<class T, typename V> inline
std::string to_base64_str(const mt_vector<T> & vec)
{
  mt_vector<unsigned char> base64_data;
  mt_vector<V> data;
  data.assign(vec.begin(), vec.end()); // here we do a data conversion

  encode_base64(data, base64_data);

  return std::string((const char*) base64_data.data());
}

template<class T, typename V> inline
void from_base64_str(const std::string & str, mt_vector<T> & vec)
{
  mt_vector<unsigned char> base64_data;
  mt_vector<V> data;

  base64_data.assign(str.length(), (unsigned char *)str.data(), false);
  decode_base64(base64_data, data);
  base64_data.assign(0, NULL, false);

  vec.assign(data.begin(), data.end()); // here we do a data conversion
}

void seek_over_char(FILE* fd, const char c);

/**
* @brief Parse the key value pairs of an xml expression.
*
* The name of the expression is stored as "tag"
*
* @param line   A line with an xml expression.
* @param fields A dictionary with the key value pairs.
*/
void parse_xml_line(std::string line, MT_MAP<std::string, std::string> & fields);

/**
* @brief Find out the type of input data (.dat, .vec, .igb).
*
* @param [in]  inp_dat      Name of input data file.
* @param [out] data_idx     Data type index.
* @param [out] inp_header   Input igb header (if data is igb).
*/
void setup_data_format(std::string inp_dat, short & data_idx, igb_header & inp_header);

/**
* @brief Reader function for the Wavefront obj format.
*
* This format supports only triangle (=surface) meshes.
*
* @param mesh       The mesh we read into.
* @param nrml_idx   Vector holding the surface normal index for each node of an element
* @param nrml_xyz   Vector holding the surface normals indexed by nrml_idx
* @param file       The file name we read from
*/
void read_obj_file(mt_meshdata & mesh, mt_vector<mt_idx_t> & nrml_idx,
                   mt_vector<mt_real> & nrml_xyz, std::string file);


void write_obj_file(mt_meshdata & mesh, std::string filename, bool continuous_normals = true);


/**
* @brief Compute an evend distribution of the parts of a value among the entries of a
*        vector.
*
* @tparam T   Integer type.
* @param vec  The vector we distribute in.
* @param val  The value we distribute.
*/
template<class T> inline
void even_distribution(mt_vector<T> & vec, T val)
{
  for(size_t i=0; i<vec.size(); i++) vec[i] = val / vec.size();
  for(size_t i=0; i<val % vec.size(); i++) vec[i]++;
}

/**
* @brief Return the timedifference t2 - t1 in seconds.
*
* @param t1 Time one.
* @param t2 Time two.
*
* @return The time difference.
*/
double timediff_sec(struct timeval & t1, struct timeval & t2);

/**
* @brief Write the compute_full_mesh_connectivity() information to disk.
*
* @param mesh       The mesh data.
* @param filename   filename used for outputting.
*/
void write_full_mesh_connectivity(const mt_meshdata & mesh, const std::string filename);

/**
* @brief Read the compute_full_mesh_connectivity() information from disk.
*
* @param mesh       The mesh data.
* @param filename   filename used for reading.
*/
void read_full_mesh_connectivity(mt_meshdata & mesh, const std::string filename);

/// a split list item
struct split_item {
  int elemIdx;   ///< element we split at
  int oldIdx;    ///< the old index
  int newIdx;    ///< the new index
};

/// write a split file
void write_split_file(mt_vector<split_item> & splitlist, std::string filename);
/// read a split file
void read_split_file(mt_vector<split_item> & splitlist, std::string filename);

void read_off_file(mt_meshdata & mesh, std::string filename);
void write_off_file(const mt_meshdata & mesh, std::string filename);

/// ionic model size data needed for EP state IO
struct ionic_model {
  int size = 0;    // ionic model size
  int offset = 0;  // plugin offset
  int nplug = 0;   // number of plugins

  mt_vector<ionic_model> plug;
  std::string name;
};

/// the structure of an ep state
struct ep_state {
  unsigned int format = 0, version = 0;
  time_t save_date;
  float time = 0.0f;

  std::string miif_id = "Dump_MIIF_ID";

  int saved_nnodes = 0;
  mt_vector<mt_vector<double>> gdata;
  mt_vector<std::string> gdata_names;

  mt_vector<ionic_model> imps;
  mt_vector<int>         imp_sizes;

  mt_vector<char>        nodal_imp_mask;
  mt_vector<size_t>      nodal_imp_offset;
  mt_vector<char>        nodal_imp_data;
};

/// reading an EP state
bool read_EP_state(ep_state & st, std::string file);
/// writing an EP state
bool write_EP_state(const ep_state & st, std::string file);

/// struct managing io error state
struct io_error_manager {
  int cur_errno = 0;
  char error_strbuff[2048] = "";
  char file_strbuff[2048] = "";
  // indicator whether the error occurred during input or output
  bool input_error = true;
  // by default we quit on errors, this can be changed by the application (e.g. a GUI)
  bool quit_on_error = true;

  inline void update_error(const char* file, const bool input) {
    cur_errno = errno;
    strcpy(error_strbuff, strerror(cur_errno));
    strcpy(file_strbuff,  file);
    input_error = input;
  }

  inline void update_error(const char* file, const char* msg, const bool input, const int error_num=0) {
    cur_errno = error_num;
    strcpy(error_strbuff, msg);
    strcpy(file_strbuff,  file);
    input_error = input;
  }

  inline void reset_error() {
    cur_errno = 0;
    error_strbuff[0] = '\0';
    file_strbuff[0]  = '\0';
    input_error = true;
  }

  inline const char* error_msg()
  {
    const char* ptr = error_strbuff;
    return ptr;
  }

  inline const char* error_file()
  {
    const char* ptr = file_strbuff;
    return ptr;
  }

  inline bool error() {
    return cur_errno != 0;
  }
};

extern io_error_manager mt_err_mng;

/// get the path of the running executable. return success
bool get_self_path(char* path_buffer, const size_t buffsize);

/// buffered file class
//
//  This class allows to read a file into memory in one chunk, then
//  parse it from memory. This greatly improves IO speeds.
//
class mt_bfile {
private:
  FILE* fd = NULL;
  mt_vector<char> buff;
  size_t pos = 0;
  size_t flush_size = 0;
  bool is_ready = false;

public:
  inline void open(const char* file)
  {
    fd = fopen(file, MT_FOPEN_READ);

    if(fd) {
      size_t fsize = file_size(fd);
      buff.resize(fsize);
      fread(buff.data(), buff.size(), 1, fd);
      fclose(fd);
      is_ready = true;
    } else {
      std::string filename(file);
      treat_file_open_error(filename);
      is_ready = false;
    }
  }

  inline void open_write(const char* file, size_t buffer_size)
  {
    fd = fopen(file, MT_FOPEN_WRITE);

    if(fd) {
      flush_size = buffer_size;
      pos        = 0;

      buff.resize(size_t(flush_size * 1.25f));
      is_ready = true;
    } else {
      std::string filename(file);
      treat_file_open_error(filename);
      is_ready = false;
    }
  }

  template<typename T> inline
  size_t read(T* dest, size_t esize, size_t nele)
  {
    if(buff.size() == 0)
      return 0;

    if(pos > buff.size() - 1)
      return 0;

    size_t nread = esize * nele;
    if((pos + nread) > buff.size())
      nread = buff.size() - pos;

    memcpy((void*)dest, (void*)(buff.data() + pos), nread);

    pos += nread;

    return (nread / esize);
  }

  template<typename T> inline
  size_t write(T* src, size_t esize, size_t nele)
  {
    char* p = (char*) src;
    memcpy(buff.data() + pos, p, esize*nele);
    pos += esize*nele;

    if(pos >= flush_size) {
      fwrite(buff.data(), pos, 1, fd);
      pos = 0;
    }

    return nele;
  }

  inline char* get_str(char* str, size_t buffsize)
  {
    if(buff.size() == 0)
      return NULL;

    if(pos > buff.size() - 1)
      return NULL;

    size_t idx = 0;
    while(idx < (buffsize-1) && pos < buff.size() && buff[pos] != 0) {
      bool do_break = buff[pos] == '\n';

      str[idx++] = buff[pos++];
      if(do_break) break;
    }

    if(idx == 0) return NULL;

    str[idx] = 0;
    return str;
  }

  template<typename T>
  inline bool read_int(T & val)
  {
    if(buff.size() == 0)
      return false;

    if(pos > buff.size() - 1)
      return false;

    char *start = buff.data() + pos, *end = NULL;
    val = strtol(start, &end, 0);

    while(start != end) {
      pos++;
      start++;
    }

    return true;
  }

  template<typename T>
  inline bool read_float(T & val)
  {
    if(buff.size() == 0)
      return false;

    if(pos > buff.size() - 1)
      return false;

    char *start = buff.data() + pos, *end = NULL;
    val = strtod(start, &end);

    while(start != end) {
      pos++;
      start++;
    }

    return true;
  }

  void clear() {
    if(fd && flush_size && pos) {
      fwrite(buff.data(), pos, 1, fd);
      fclose(fd);
    }

    buff.clear();
    fd = NULL;
    pos = 0;
    flush_size = 0;
    is_ready = false;
  }

  ~mt_bfile()
  {
    clear();
  }

  inline bool ok() const {return is_ready;}
};

bool is_stl_binary(std::string file);
void read_stl_binary(std::string file,  mt_meshdata & mesh);
void write_stl_binary(std::string file, const mt_meshdata & mesh);

struct dir_entry {
  char name[512];
  char mod_str[32];

  bool is_dir = false;      ///< whether the file is a directory
  bool from_link = false;   ///< whether the file type was derived form a link
  size_t size = 0;
};

inline bool operator< (const dir_entry & lhs, const dir_entry & rhs)
{
  if(lhs.is_dir != rhs.is_dir) return lhs.is_dir;
  else {
    std::string lhsstr(lhs.name);
    std::string rhsstr(rhs.name);
    std::transform(lhsstr.begin(), lhsstr.end(), lhsstr.begin(), [](unsigned char c){ return std::tolower(c); });
    std::transform(rhsstr.begin(), rhsstr.end(), rhsstr.begin(), [](unsigned char c){ return std::tolower(c); });

    return lhsstr < rhsstr;
  }
}

bool get_dir_entries(const char* path, mt_vector<dir_entry> & entries);
std::string mt_get_cwd();



#endif

