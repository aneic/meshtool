#ifndef SPARSE_MATRIX_HPP
#define SPARSE_MATRIX_HPP

#include "mt_utils_base.h"

class mt_sparse_matrix {

  public:
  /// accumulation buffer for sparse matrices
  class accbuff {
    public:
    MT_MAP<mt_tuple<mt_idx_t>,mt_real> buff; ///< map from (row, col) to matrix value

    /// add matrix entry
    inline void add(mt_idx_t i, mt_idx_t j, mt_real e) {
      buff[{i,j}] += e;
    }
  };

  mt_vector<accbuff*> buffers;  ///< vector to allocate multiple assembly buffers. e.g., one per thread

  // standard CRS sparse matrix format
  mt_vector<mt_idx_t> dsp;
  mt_vector<mt_idx_t> con;
  mt_vector<mt_real>  ele;

  // dimensions
  size_t row_dim = 0, col_dim = 0;

  mt_sparse_matrix()
  {}

  ~mt_sparse_matrix() {
    clear_buffers();
  }

  /// get an accumulation buffer
  inline accbuff* get_buffer() {
    buffers.push_back(new accbuff());
    return buffers.back();
  }

  /// clear list of accumualtion buffers
  inline void clear_buffers() {
    for(accbuff* b : buffers)
      delete b;
    buffers.resize(0);
  }

  /// start assembly, should be called before get_buffer(), and buffer.add()
  inline void start_assembly() {
    clear_buffers();
    dsp.resize(0);
    con.resize(0);
    ele.resize(0);
  }

  /// finish assembly, should be called after all values have been added to any accumulation
  /// buffers
  inline void finish_assembly() {
    // abort if we have no assembly buffers
    if(buffers.size() == 0) {
      fprintf(stderr, "mt_sparse_matrix error: no assembly buffers. nothing to assemble!\n");
      return;
    }

    // the first assembly buffer is our main one
    accbuff & main_buff = *buffers[0];

    // if we have more, we accumulate into the first one
    if(buffers.size() > 1) {
      for(size_t i=1; i<buffers.size(); i++) {
        for(auto & it : buffers[i]->buff)
          main_buff.add(it.first.v1, it.first.v2, it.second);
      }
    }

    size_t nnz = main_buff.buff.size();
    mt_vector<mt_idx_t> row(nnz);
    con.resize(nnz); ele.resize(nnz);

    row.resize(0), con.resize(0), ele.resize(0);
    mt_idx_t max_row = 0, max_col = 0;

    for(auto & it : main_buff.buff) {
      mt_idx_t r = it.first.v1, c = it.first.v2;
      if(max_row < r) max_row = r;
      if(max_col < c) max_col = c;

      row.push_back(r);
      con.push_back(c);
      ele.push_back(it.second);
    }

    row_dim = max_row + 1;
    col_dim = max_col + 1;

    binary_sort_sort_copy(row, con, ele);

    mt_vector<mt_idx_t> cnt(row_dim, 0);
    bucket_sort_count(row, cnt);
    dsp_from_cnt(cnt, dsp);

    clear_buffers();
  }

  /// apply matrix to a vector
  template<class V> inline
  void apply(const mt_vector<V> & inp, mt_vector<V> & out)
  {
    out.resize(row_dim);

#ifdef OPENMP
    #pragma omp parallel for schedule(dynamic, 10)
#endif
    for(size_t i=0; i<row_dim; i++) {
      mt_idx_t start = dsp[i], end = dsp[i+1];

      V t = V();
      for(mt_idx_t j=start; j<end; j++) {
        const mt_idx_t c = con[j];
        const mt_real  e = ele[j];
        t += inp[c] * e;
      }

      out[i] = t;
    }
  }
};


#endif
