#include "io_utils.h"
#include "mesh_generation.h"

void mesh_to_tetgenio(const mt_meshdata & mesh, tetgenio & io)
{
  io.firstnumber = 0;
  io.numberofpoints = mesh.xyz.size() / 3;
  io.pointlist = new REAL[mesh.xyz.size()];

  for(size_t i=0; i<mesh.xyz.size(); i++) io.pointlist[i] = mesh.xyz[i];
  io.numberoffacets = mesh.e2n_cnt.size();
  io.facetlist = new tetgenio::facet[io.numberoffacets];

  for(size_t eidx = 0, ridx=0; eidx < mesh.e2n_cnt.size(); eidx++)
  {
    tetgenio::facet* f = &io.facetlist[eidx];
    f->numberofpolygons = 1;
    f->polygonlist = new tetgenio::polygon[f->numberofpolygons];
    f->numberofholes = 0;
    f->holelist = NULL;

    tetgenio::polygon* p = &f->polygonlist[0];
    p->numberofvertices = mesh.e2n_cnt[eidx];
    p->vertexlist = new int[p->numberofvertices];

    for(mt_idx_t i=0; i<mesh.e2n_cnt[eidx]; i++)
      p->vertexlist[i] = mesh.e2n_con[ridx+i];

    ridx += mesh.e2n_cnt[eidx];
  }
}

void pointcloud_to_tetgenio(const mt_vector<vec3r> & pts, tetgenio & io)
{
  io.firstnumber = 0;
  io.numberofpoints = pts.size();
  io.pointlist = new REAL[pts.size()*3];

  for(size_t i=0; i<pts.size(); i++) {
    io.pointlist[i*3+0] = pts[i].x;
    io.pointlist[i*3+1] = pts[i].y;
    io.pointlist[i*3+2] = pts[i].z;
  }
}

void tetgenio_to_mesh(const tetgenio & io, mt_meshdata & mesh)
{
  mesh.xyz.resize(io.numberofpoints * 3);
  for(size_t i=0; i<mesh.xyz.size(); i++) mesh.xyz[i] = io.pointlist[i];

  if(io.numberoftetrahedra == 0) {
    if(io.numberoftrifaces) {
      size_t numele = io.numberoftrifaces;
      mesh.e2n_cnt.assign(numele, mt_idx_t(3));
      mesh.etype.assign(numele, Tri);
      mesh.etags.assign(numele, mt_tag_t(0));

      mesh.e2n_con.resize(numele * 3);
      for(size_t i=0; i<mesh.e2n_con.size(); i++)
        mesh.e2n_con[i] = io.trifacelist[i];
    }
    else {
      fprintf(stderr, "%s error: tetgenio holds neither tets nor triangles! Aborting!\n",
              __func__);
      exit(1);
    }
  }
  else {
    size_t numele = io.numberoftetrahedra;
    mesh.e2n_cnt.assign(numele, mt_idx_t(4));
    mesh.etype.assign(numele, Tetra);

    if(io.numberoftetrahedronattributes == 1)
      mesh.etags.assign(io.tetrahedronattributelist, io.tetrahedronattributelist + numele);
    else
      mesh.etags.assign(numele, mt_idx_t(-1));

    mesh.e2n_con.resize(numele * 4);
    for(size_t i=0; i<mesh.e2n_con.size(); i++)
      mesh.e2n_con[i] = io.tetrahedronlist[i];
  }
}

const char* get_tetgen_smooth_string(int tetgen_qual)
{
  const char* ret = NULL;

  // syntax:
  // s<type>/<iter>
  // type is a bitmask with 0 = nothing, 1 = volume, 2 = surface
  switch(tetgen_qual) {
    case 0: ret = "s0"; break;
    case 1: ret = "s3/5"; break;
    case 2: ret = "s3/8"; break;
  }

  return ret;
}

const char* get_tetgen_quality_string(int tetgen_qual)
{
  const char* ret = NULL;

  // syntax:
  // s<type>/<iter>
  // type is a bitmask with 0 = nothing, 1 = volume, 2 = surface
  switch(tetgen_qual) {
    case 0: ret = "q1.8"; break;
    case 1: ret = "q1.4/10"; break;
    case 2: ret = "q1.2/15"; break;
  }

  return ret;
}

void mesh_with_tetgen(mt_meshdata & surfmesh,
                      mt_meshdata & volmesh,
                      mt_vector<double> & region_attributes,
                      mt_vector<mt_real> & holes_xyz,
                      int tetgen_qual,
                      mt_vector<mt_real> * size_field,
                      bool preserve_boundary)
{
  tetgenio in, out;
  tetgen_qual = clamp(tetgen_qual, 0, 2);

  mesh_to_tetgenio(surfmesh, in);

  // include holes into tetgenio
  if(holes_xyz.size()) {
    in.numberofholes = holes_xyz.size() / 3;
    in.holelist = new REAL[holes_xyz.size()];

    for(size_t i=0; i<holes_xyz.size(); i++)
      in.holelist[i] = holes_xyz[i];
  }

  // add volumetric regions
  in.numberofregions = region_attributes.size() / 5;
  in.regionlist = new double[region_attributes.size()];
  memcpy(in.regionlist, region_attributes.data(), region_attributes.size() * sizeof(double));

  char tetgen_par_str[1024];

  // include sizing field into tetgenio
  if(size_field) {
    check_size(*size_field, surfmesh.xyz.size() / 3, __func__);
    in.numberofpointmtrs = 1;
    in.pointmtrlist      = new double[size_field->size()];
    for(size_t i=0; i<size_field->size(); i++) in.pointmtrlist[i] = (*size_field)[i];
    sprintf(tetgen_par_str, "p%sA%sm", get_tetgen_quality_string(tetgen_qual), get_tetgen_smooth_string(tetgen_qual));
  }
  else {
    sprintf(tetgen_par_str, "p%sA%sa", get_tetgen_quality_string(tetgen_qual), get_tetgen_smooth_string(tetgen_qual));
  }

  if(preserve_boundary) {
    int old_len = strlen(tetgen_par_str);
    tetgen_par_str[old_len] = 'Y';
    tetgen_par_str[old_len+1] = '\0';
  }

  tetrahedralize(tetgen_par_str, &in, &out, (tetgenio*)NULL, (tetgenio*)NULL);
  tetgenio_to_mesh(out, volmesh);

  // generate fibers. We use the same amount of directions as are stored in the
  // input surface
  int numfib = surfmesh.lon.size() == surfmesh.e2n_cnt.size() * 6 ? 2 : 1;
  printf("Generating empty fibers for output mesh (%d fiber directions) ..\n", numfib);
  volmesh.lon.assign(numfib * 3 * volmesh.e2n_cnt.size(), 0.0);
}

void convex_hull_with_tetgen(const mt_vector<vec3r> & pts, mt_meshdata & surfmesh)
{
  tetgenio in, out;
  mt_meshdata volmesh;
  char tetgen_par_str[16] = "";

  pointcloud_to_tetgenio(pts, in);

  tetrahedralize(tetgen_par_str, &in, &out, (tetgenio*)NULL, (tetgenio*)NULL);
  tetgenio_to_mesh(out, volmesh);

  compute_surface(volmesh, surfmesh, true);
  surfmesh.xyz = volmesh.xyz;
  surfmesh.lon.assign(surfmesh.e2n_cnt.size() * 3);
  surfmesh.etags.assign(surfmesh.e2n_cnt.size());

  reindex_nodes(surfmesh, false);
}

void generate_boundary_layers(const mt_meshdata & outer_surf,
                              const int num_layers,
                              const mt_real transition_scale,
                              const mt_real transition_inc,
                              const int sizing_mode,
                              const bool symmetric,
                              mt_meshdata & inner_surf,
                              mt_meshdata & output_mesh)
{
  check_nonzero(outer_surf.xyz.size(), "generate_boundary_layers");
  mt_real t_scale = transition_scale;

  mt_vector<mt_real> edge_sizes;

  // configure sizing field
  if(sizing_mode == 1) {
    generate_surfmesh_sizing_field(outer_surf, edge_sizes);
    mt_vector<mt_idx_t> smth_nodes(outer_surf.xyz.size() / 3);
    for(size_t i=0; i<smth_nodes.size(); i++) smth_nodes[i] = i;
    mt_vector<bool>   on_set(smth_nodes.size(), true);
    smooth_data_nodal(outer_surf, smth_nodes, on_set, 100, 0.35, edge_sizes);
  } else {
    edge_sizes.assign(outer_surf.xyz.size() / 3, avrg_edgelength_estimate(outer_surf, false));
  }

  inner_surf = outer_surf;

  // determine if an open border is used
  mt_meshdata border;
  mt_vector<mt_idx_t> border_vtx;
  identify_surface_border(inner_surf, border);

  if(border.e2n_cnt.size()) {
    printf("%s warning: Input surface is not closed. Skipping surf normal consistency check!\n", __func__);

    compute_full_mesh_connectivity(border, false);
    border.xyz.assign(inner_surf.xyz.size(), inner_surf.xyz.data(), true);
    border_vtx = border.e2n_con;
    binary_sort(border_vtx); unique_resize(border_vtx);
  } else {
    // compute inwards facing surface normals.
    apply_normal_orientation(inner_surf, true);
  }

  mt_vector<mt_real> surf_normals;
  compute_nodal_surface_normals(inner_surf, inner_surf.xyz, surf_normals);

  MT_USET<mt_idx_t> smth_nod_set;
  for(mt_idx_t c : inner_surf.e2n_con)
    smth_nod_set.insert(c);

  for(mt_idx_t c : border_vtx)
    smth_nod_set.erase(c);

  mt_vector<mt_idx_t> smth_nod;
  smth_nod.assign(smth_nod_set.begin(), smth_nod_set.end());

  size_t num_tri  = inner_surf.e2n_cnt.size();
  size_t num_vert = inner_surf.xyz.size() / 3;

  mt_vector<mt_idx_t> e2n_cnt(num_tri, 6), e2n_con(num_tri*6), tags(num_tri);
  mt_vector<elem_t> type(num_tri, Prism);

  mt_idx_t  iter = 6;
  mt_real smth = 0.12;

  // directional_smoothing(inner_surf, smth_nod, surf_normals, 1.0, true, 0.1, 50);
  // if(border.e2n_cnt.size())
  //   directional_smoothing(border, border_vtx, surf_normals, 0.8, 0.5, true, smth, iter*2);

  // if we are meshing symmetrically around the input surface, we first move the starting
  // surface outwards by half the layers
  if(symmetric) {
    int num_preproc = num_layers / 2;
    t_scale = -fabs(transition_scale);

    for(int layer = 0; layer < num_preproc; layer++) {
      for(size_t i=0; i<inner_surf.xyz.size() / 3; i++) {
        inner_surf.xyz[i*3+0] += surf_normals[i*3+0] * edge_sizes[i] * t_scale;
        inner_surf.xyz[i*3+1] += surf_normals[i*3+1] * edge_sizes[i] * t_scale;
        inner_surf.xyz[i*3+2] += surf_normals[i*3+2] * edge_sizes[i] * t_scale;
      }

      directional_smoothing(inner_surf, smth_nod, surf_normals, 0.5, 0.8, true, smth, iter);
      directional_smoothing(border, border_vtx, surf_normals, 0.95, 0.5, true, smth, iter);

      t_scale *= transition_inc;
    }
  }

  // we add the starting vertices to the output mesh
  output_mesh.xyz.append(inner_surf.xyz.begin(), inner_surf.xyz.end());

  t_scale = symmetric ? fabs(transition_scale) : transition_scale;
  bool mesh_forward = t_scale > 0.0;

  for(int layer = 0; layer < num_layers; layer++) {
    tags.assign(num_tri, layer);
    for(size_t i=0; i<num_tri; i++) {
      if(mesh_forward) {
        e2n_con[i*6+0] = inner_surf.e2n_con[i*3+0] + (num_vert * (layer+1));
        e2n_con[i*6+1] = inner_surf.e2n_con[i*3+1] + (num_vert * (layer+1));
        e2n_con[i*6+2] = inner_surf.e2n_con[i*3+2] + (num_vert * (layer+1));
        e2n_con[i*6+3] = inner_surf.e2n_con[i*3+0] + (num_vert * (layer+0));
        e2n_con[i*6+5] = inner_surf.e2n_con[i*3+1] + (num_vert * (layer+0));
        e2n_con[i*6+4] = inner_surf.e2n_con[i*3+2] + (num_vert * (layer+0));
      } else {
        e2n_con[i*6+0] = inner_surf.e2n_con[i*3+0] + (num_vert * (layer+0));
        e2n_con[i*6+1] = inner_surf.e2n_con[i*3+1] + (num_vert * (layer+0));
        e2n_con[i*6+2] = inner_surf.e2n_con[i*3+2] + (num_vert * (layer+0));
        e2n_con[i*6+3] = inner_surf.e2n_con[i*3+0] + (num_vert * (layer+1));
        e2n_con[i*6+5] = inner_surf.e2n_con[i*3+1] + (num_vert * (layer+1));
        e2n_con[i*6+4] = inner_surf.e2n_con[i*3+2] + (num_vert * (layer+1));
      }
    }

    output_mesh.e2n_cnt.append(e2n_cnt.begin(), e2n_cnt.end());
    output_mesh.e2n_con.append(e2n_con.begin(), e2n_con.end());
    output_mesh.etags.append(tags.begin(), tags.end());
    output_mesh.etype.append(type.begin(), type.end());

    for(size_t i=0; i<inner_surf.xyz.size() / 3; i++) {
      inner_surf.xyz[i*3+0] += surf_normals[i*3+0] * edge_sizes[i] * t_scale;
      inner_surf.xyz[i*3+1] += surf_normals[i*3+1] * edge_sizes[i] * t_scale;
      inner_surf.xyz[i*3+2] += surf_normals[i*3+2] * edge_sizes[i] * t_scale;
    }

    directional_smoothing(inner_surf, smth_nod, surf_normals, 0.5, 0.8, true, smth, iter);
    directional_smoothing(border, border_vtx, surf_normals, 0.95, 0.5, true, smth, iter);

    t_scale *= transition_inc;

    output_mesh.xyz.append(inner_surf.xyz.begin(), inner_surf.xyz.end());
    compute_nodal_surface_normals(inner_surf, inner_surf.xyz, surf_normals);
  }

  border.xyz.assign(0, NULL, false);

  int numfib = 1;
  if(outer_surf.lon.size())
    numfib = outer_surf.lon.size() == outer_surf.e2n_cnt.size() * 6 ? 6 : 3;

  output_mesh.lon.assign(output_mesh.e2n_cnt.size() * numfib);
}

void generate_boundary_layers(const mt_meshdata & outer_surf,
                              const int num_layers,
                              const mt_real transition,
                              const mt_real transition_inc,
                              const bool symmetric,
                              mt_meshdata & inner_surf,
                              mt_meshdata & output_mesh)
{
  check_nonzero(outer_surf.xyz.size(), "generate_boundary_layers");

  inner_surf = outer_surf;

  // determine if an open border is used
  mt_meshdata border;
  mt_vector<mt_idx_t> border_vtx;
  identify_surface_border(inner_surf, border);

  if(border.e2n_cnt.size()) {
    printf("%s warning: Input surface is not closed. Skipping surf normal consistency check!\n", __func__);

    compute_full_mesh_connectivity(border, false);
    border.xyz.assign(inner_surf.xyz.size(), inner_surf.xyz.data(), true);
    border_vtx = border.e2n_con;
    binary_sort(border_vtx); unique_resize(border_vtx);
  } else {
    // compute inwards facing surface normals.
    apply_normal_orientation(inner_surf, true);
  }

  mt_vector<mt_real> surf_normals;
  compute_nodal_surface_normals(inner_surf, inner_surf.xyz, surf_normals);

  MT_USET<mt_idx_t> smth_nod_set;
  for(mt_idx_t c : inner_surf.e2n_con)
    smth_nod_set.insert(c);

  for(mt_idx_t c : border_vtx)
    smth_nod_set.erase(c);

  mt_vector<mt_idx_t> smth_nod;
  smth_nod.assign(smth_nod_set.begin(), smth_nod_set.end());

  size_t num_tri  = inner_surf.e2n_cnt.size();
  size_t num_vert = inner_surf.xyz.size() / 3;

  mt_vector<mt_idx_t> e2n_cnt(num_tri, 6), e2n_con(num_tri*6), tags(num_tri);
  mt_vector<elem_t> type(num_tri, Prism);

  mt_idx_t  iter = 6;
  mt_real smth = 0.12;

  // directional_smoothing(inner_surf, smth_nod, surf_normals, 1.0, true, 0.1, 50);
  // if(border.e2n_cnt.size())
  //   directional_smoothing(border, border_vtx, surf_normals, 0.8, 0.5, true, smth, iter*2);

  // if we are meshing symmetrically around the input surface, we first move the starting
  // surface outwards by half the layers
  if(symmetric) {
    int num_preproc = num_layers / 2;
    mt_real t_scale = -fabs(transition);

    for(int layer = 0; layer < num_preproc; layer++) {
      for(size_t i=0; i<inner_surf.xyz.size() / 3; i++) {
        inner_surf.xyz[i*3+0] += surf_normals[i*3+0] * t_scale;
        inner_surf.xyz[i*3+1] += surf_normals[i*3+1] * t_scale;
        inner_surf.xyz[i*3+2] += surf_normals[i*3+2] * t_scale;
      }

      directional_smoothing(inner_surf, smth_nod, surf_normals, 0.5, 0.8, true, smth, iter);
      directional_smoothing(border, border_vtx, surf_normals, 0.95, 0.5, true, smth, iter);

      t_scale *= transition_inc;
    }
  }

  // we add the starting vertices to the output mesh
  output_mesh.xyz.append(inner_surf.xyz.begin(), inner_surf.xyz.end());

  mt_real t_scale = symmetric ? fabs(transition) : transition;
  bool mesh_forward = t_scale > 0.0;

  for(int layer = 0; layer < num_layers; layer++) {
    tags.assign(num_tri, layer);
    for(size_t i=0; i<num_tri; i++) {
      if(mesh_forward) {
        e2n_con[i*6+0] = inner_surf.e2n_con[i*3+0] + (num_vert * (layer+1));
        e2n_con[i*6+1] = inner_surf.e2n_con[i*3+1] + (num_vert * (layer+1));
        e2n_con[i*6+2] = inner_surf.e2n_con[i*3+2] + (num_vert * (layer+1));
        e2n_con[i*6+3] = inner_surf.e2n_con[i*3+0] + (num_vert * (layer+0));
        e2n_con[i*6+5] = inner_surf.e2n_con[i*3+1] + (num_vert * (layer+0));
        e2n_con[i*6+4] = inner_surf.e2n_con[i*3+2] + (num_vert * (layer+0));
      } else {
        e2n_con[i*6+0] = inner_surf.e2n_con[i*3+0] + (num_vert * (layer+0));
        e2n_con[i*6+1] = inner_surf.e2n_con[i*3+1] + (num_vert * (layer+0));
        e2n_con[i*6+2] = inner_surf.e2n_con[i*3+2] + (num_vert * (layer+0));
        e2n_con[i*6+3] = inner_surf.e2n_con[i*3+0] + (num_vert * (layer+1));
        e2n_con[i*6+5] = inner_surf.e2n_con[i*3+1] + (num_vert * (layer+1));
        e2n_con[i*6+4] = inner_surf.e2n_con[i*3+2] + (num_vert * (layer+1));
      }
    }

    output_mesh.e2n_cnt.append(e2n_cnt.begin(), e2n_cnt.end());
    output_mesh.e2n_con.append(e2n_con.begin(), e2n_con.end());
    output_mesh.etags.append(tags.begin(), tags.end());
    output_mesh.etype.append(type.begin(), type.end());

    for(size_t i=0; i<inner_surf.xyz.size() / 3; i++) {
      inner_surf.xyz[i*3+0] += surf_normals[i*3+0] * t_scale;
      inner_surf.xyz[i*3+1] += surf_normals[i*3+1] * t_scale;
      inner_surf.xyz[i*3+2] += surf_normals[i*3+2] * t_scale;
    }

    directional_smoothing(inner_surf, smth_nod, surf_normals, 0.5, 0.8, true, smth, iter);
    directional_smoothing(border, border_vtx, surf_normals, 0.95, 0.5, true, smth, iter);

    t_scale *= transition_inc;

    output_mesh.xyz.append(inner_surf.xyz.begin(), inner_surf.xyz.end());
    compute_nodal_surface_normals(inner_surf, inner_surf.xyz, surf_normals);
  }

  border.xyz.assign(0, NULL, false);

  int numfib = 1;
  if(outer_surf.lon.size())
    numfib = outer_surf.lon.size() == outer_surf.e2n_cnt.size() * 6 ? 6 : 3;

  output_mesh.lon.assign(output_mesh.e2n_cnt.size() * numfib);
}


void generate_bbox_mesh(const bbox & imesh_bbox, const float res, mt_meshdata & out_mesh)
{
  const vec3r pmin = imesh_bbox.bounds[0];
  const vec3r pmax = imesh_bbox.bounds[1];
  const vec3r delta = pmax - pmin;
  const vec3r mid = (pmax + pmin) * mt_real(0.5);

  generate_box_mesh(delta.x, delta.y, delta.z, res, out_mesh);

  vec3r p;
  for(size_t i=0; i<out_mesh.xyz.size()/3; i++) {
    p.get(out_mesh.xyz.data() + i*3);
    p += mid;
    p.set(out_mesh.xyz.data() + i*3);
  }
}

void mesh_rect_plane(const vec3f x_axis, const vec3f y_axis,
                     const float x_size, const float y_size,
                     const vec3f start, const float res_x, const float res_y,
                     mt_meshdata & mesh)
{
  const int x_steps = int(x_size / res_x) + 1;
  const int y_steps = int(y_size / res_y) + 1;
  const float dx = x_size / x_steps;
  const float dy = y_size / y_steps;
  const int num_pts_start = (int) mesh.xyz.size() / 3;

  for(int j=0; j <= y_steps; j++) {
    vec3f linestart = start + y_axis * (j*dy);

    for(int i=0; i <= x_steps; i++) {
      vec3f p = linestart + x_axis * (i*dx);
      mesh.xyz.push_back(p.x);
      mesh.xyz.push_back(p.y);
      mesh.xyz.push_back(p.z);
    }
  }

  for(int j=0; j < y_steps; j++) {
    const int cur_line_start = num_pts_start + j    *(x_steps+1);
    const int nxt_line_start = num_pts_start + (j+1)*(x_steps+1);

    for(int i=0; i < x_steps; i++) {
      mesh.e2n_con.push_back(cur_line_start + i);
      mesh.e2n_con.push_back(nxt_line_start + i + 1);
      mesh.e2n_con.push_back(nxt_line_start + i);

      mesh.e2n_con.push_back(cur_line_start + i);
      mesh.e2n_con.push_back(cur_line_start + i + 1);
      mesh.e2n_con.push_back(nxt_line_start + i + 1);
    }
  }

  mesh.e2n_cnt.assign(mesh.e2n_con.size() / 3, 3);
  mesh.etype.assign(mesh.e2n_cnt.size(), Tri);
  mesh.etags.assign(mesh.e2n_cnt.size(), 0);
  mesh.lon.assign(mesh.e2n_cnt.size() * 3);
}

void generate_box_mesh(float width, float height, float depth, float res, mt_meshdata & mesh)
{
/*
     5      6
   4     7
     3      2
   0     1

  Hex faces: (0,1,2,3) (0,4,7,1) (1,7,6,2) (2,6,5,3) (3,5,4,0) (4,5,6,7)
*/

  float hw = width/2, hh = height / 2, hd = depth / 2;

  vec3f p0(-hw, -hh, -hd);
  vec3f x(1,0,0), y(0,1,0), z(0,0,1);
  vec3f p;

  p = p0;
  // face 0,4,7,1
  mesh_rect_plane(x, y, width, height, p, res, res, mesh);
  // face 0,1,2,3
  mesh_rect_plane(z, x, depth, width, p, res, res, mesh);

  p = p0 + x * width;
  // face 1,7,6,2
  mesh_rect_plane(z, y, depth, height, p, res, res, mesh);

  p = p0 + z * depth;
  // face 3,5,4,0
  mesh_rect_plane(-z, y, depth, height, p, res, res, mesh);

  p = p0 + x * width + y * height;
  // face 4,5,6,7
  mesh_rect_plane(z, -x, depth, width, p, res, res, mesh);

  p = p0 + x * width + z * depth;
  // face 2,6,5,3
  mesh_rect_plane(-x, y, width, height, p, res, res, mesh);

  correct_duplicate_vertices(mesh, res*0.1f, false);
}

void generate_simple_box_surf(const bbox & box, mt_meshdata & out_mesh)
{
/*
     5      6 
   4     7
     3      2
   0     1
*/
  // Hex faces: (0,1,2,3) (0,4,7,1) (1,7,6,2) (2,6,5,3) (3,5,4,0) (4,5,6,7)
  // Associated triangle faces:
  // (0,1,2) (0,2,3) (0,4,7) (0,7,1) (1,7,6) (1,6,2) (2,6,5) (2,5,3) (3,5,4) (3,4,0) (4,5,6) (4,6,7)
  const vec3r pmin  = box.bounds[0];
  const vec3r delta = vec3r(box.bounds[1]) - pmin;

  mt_vector<vec3r> points(8);
  points[0] = pmin;
  points[1] = pmin + vec3r(delta.x,       0, 0);
  points[2] = pmin + vec3r(delta.x, delta.y, 0);
  points[3] = pmin + vec3r(0,       delta.y, 0);
  points[4] = pmin + vec3r(0,             0, delta.z);
  points[5] = pmin + vec3r(0,       delta.y, delta.z);
  points[6] = pmin + vec3r(delta.x, delta.y, delta.z);
  points[7] = pmin + vec3r(delta.x,       0, delta.z);

  points_to_array(points, out_mesh.xyz);
  out_mesh.e2n_cnt.assign(size_t(12), 3);
  out_mesh.etype.assign(size_t(12), Tri);
  out_mesh.etags.assign(size_t(12), mt_tag_t(0));

  out_mesh.e2n_con.resize(size_t(12*3));
  mt_idx_t* con = out_mesh.e2n_con.data();

  con[ 0*3+0] = 0, con[ 0*3+1] = 1, con[ 0*3+2] = 2; // (0,1,2)
  con[ 1*3+0] = 0, con[ 1*3+1] = 2, con[ 1*3+2] = 3; // (0,2,3)
  con[ 2*3+0] = 0, con[ 2*3+1] = 4, con[ 2*3+2] = 7; // (0,4,7)
  con[ 3*3+0] = 0, con[ 3*3+1] = 7, con[ 3*3+2] = 1; // (0,7,1)
  con[ 4*3+0] = 1, con[ 4*3+1] = 7, con[ 4*3+2] = 6; // (1,7,6)
  con[ 5*3+0] = 1, con[ 5*3+1] = 6, con[ 5*3+2] = 2; // (1,6,2)
  con[ 6*3+0] = 2, con[ 6*3+1] = 6, con[ 6*3+2] = 5; // (2,6,5)
  con[ 7*3+0] = 2, con[ 7*3+1] = 5, con[ 7*3+2] = 3; // (2,5,3)
  con[ 8*3+0] = 3, con[ 8*3+1] = 5, con[ 8*3+2] = 4; // (3,5,4)
  con[ 9*3+0] = 3, con[ 9*3+1] = 4, con[ 9*3+2] = 0; // (3,4,0)
  con[10*3+0] = 4, con[10*3+1] = 5, con[10*3+2] = 6; // (4,5,6)
  con[11*3+0] = 4, con[11*3+1] = 6, con[11*3+2] = 7; // (4,6,7)
}

void mesh_box_with_tetgen(const bbox & box,
                          mt_meshdata & volmesh,
                          const float res)
{
  mt_meshdata surfmesh;
  generate_simple_box_surf(box, surfmesh);

  vec3r vol_pt = (box.bounds[0] + box.bounds[1]) * mt_real(0.5);

  tetgenio in, out;
  int tetgen_qual = 1; // medium quality
  double edge_length = res;
  double desired_volume = edge_length * edge_length * edge_length / 8.48528;

  mesh_to_tetgenio(surfmesh, in);

  // add volumetric regions
  in.numberofregions = 1;
  in.regionlist = new double[5];

  in.regionlist[0] = vol_pt.x;          // point inside
  in.regionlist[1] = vol_pt.y;
  in.regionlist[2] = vol_pt.z;
  in.regionlist[3] = 0;                 // region tag
  in.regionlist[4] = desired_volume;    // resolution

  char tetgen_par_str[1024];
  sprintf(tetgen_par_str, "p%sA%sa", get_tetgen_quality_string(tetgen_qual), get_tetgen_smooth_string(tetgen_qual));

  tetrahedralize(tetgen_par_str, &in, &out, (tetgenio*)NULL, (tetgenio*)NULL);
  tetgenio_to_mesh(out, volmesh);

  volmesh.lon.resize(3 * volmesh.e2n_cnt.size());
  for(size_t i=0; i<volmesh.e2n_cnt.size(); i++) {
    volmesh.lon[i*3+0] = 1.0;
    volmesh.lon[i*3+1] = 0.0;
    volmesh.lon[i*3+2] = 0.0;
  }
}

  void generate_hex_mesh(const mt_triple<mt_vector<mt_real>> & layout,
                         const mt_triple<mt_real> & res,
                         mt_meshdata& mesh)
  {
    mesh.clear();
    mt_triple<mt_real> size(0,0,0);

    const mt_vector<mt_real> & x_layout = layout.v1;
    const mt_vector<mt_real> & y_layout = layout.v2;
    const mt_vector<mt_real> & z_layout = layout.v3;
    for(size_t i=0; i<x_layout.size(); i++) size.v1 += x_layout[i];
    for(size_t i=0; i<y_layout.size(); i++) size.v2 += y_layout[i];
    for(size_t i=0; i<z_layout.size(); i++) size.v3 += z_layout[i];
    
    mt_triple<unsigned int> dim;
    dim.v1 = size.v1 / res.v1;
    dim.v2 = size.v2 / res.v2;
    dim.v3 = size.v3 / res.v3;

    if(!dim.v1 || !dim.v2 || !dim.v3) {
      fprintf(stderr, "%s error: one or more empty dimensions. (x=%u,y=%u,z=%u). Aborting!\n",
              __func__, dim.v1, dim.v2, dim.v3);
      return;
    }

    auto get_idx_in_cntvec = [](mt_real val, const mt_vector<mt_real> & cntvec) {
      size_t idx = 0;
      mt_real dsp = cntvec[0];
      while (idx < (cntvec.size()-1) && val > dsp) {
        idx++;
        dsp += cntvec[idx];
      }
      return idx;
    };

    auto get_tag = [&](unsigned int i, unsigned int j, unsigned int k)
    {
      mt_real x = (0.5+i)*res.v1, y = (0.5+j)*res.v2, z = (0.5+k)*res.v3;

      size_t dim_x = clamp<size_t>(x_layout.size(), 1, 10000);
      size_t dim_y = clamp<size_t>(y_layout.size(), 1, 10000);

      size_t x_idx = get_idx_in_cntvec(x, x_layout);
      size_t y_idx = get_idx_in_cntvec(y, y_layout);
      size_t z_idx = get_idx_in_cntvec(z, z_layout);

      mt_tag_t tag = z_idx*dim_x*dim_y + y_idx*dim_x + x_idx;
      return tag;
    };

    const mt_idx_t delta[24] = {0,1,1, 1,1,1, 1,0,1, 0,0,1, 0,1,0 ,0,0,0 ,1,0,0, 1,1,0};
    mt_idx_t vtx[8];
    mt_real pnt[3];

    size_t nelem = size_t(dim.v1) * dim.v2 * dim.v3; // first estimate
    size_t npnts = (nelem*4); // first estimate

    // allocate some memory for the
    // elements and points
    mesh.e2n_dsp.reserve(nelem);
    mesh.e2n_con.reserve(8*nelem);
    mesh.etags.reserve(nelem);
    mesh.xyz.reserve(3*npnts);

    // number of grid points
    npnts = size_t(dim.v1+1)*(dim.v2+1)*(dim.v3+1);
    mt_vector<mt_idx_t> pnt_idx(npnts, -1);

    npnts = 0;
    // iterate over all image pixels
    for (size_t i=0; i<dim.v1; i++) {
      for (size_t j=0; j<dim.v2; j++) {
        for (size_t k=0; k<dim.v3; k++) {
          // get current number of elements
          nelem = mesh.e2n_dsp.size();
          mesh.e2n_dsp.push_back(8*nelem);
          // collect hex vertices
          for (size_t l=0; l<8; l++) {
            // point index in grid
            const size_t pidx = (k+delta[3*l+2])*(dim.v1+1)*(dim.v2+1) +
                                (j+delta[3*l+1])*(dim.v1+1) + (i+delta[3*l+0]);
            // check if grid point is a mesh point
            if (pnt_idx[pidx] == -1) {
              // add point to mesh and assign mesh index
              pnt[0] = res.v1*(i+delta[3*l+0]);
              pnt[1] = res.v2*(j+delta[3*l+1]);
              pnt[2] = res.v3*(k+delta[3*l+2]);
              mesh.xyz.append(pnt, pnt+3);
              pnt_idx[pidx] = npnts++;
            }
            vtx[l] = pnt_idx[pidx];
          }
          // fill element data
          mesh.e2n_con.append(vtx, vtx+8);

          mt_tag_t elem_tag = get_tag(i,j,k);
          mesh.etags.push_back(elem_tag);
        }
      }
    }
    // get number of elements
    nelem = mesh.e2n_dsp.size();
    // set missing element data
    mesh.e2n_cnt.assign(nelem, 8);
    mesh.etype.assign(nelem, Hexa);
    // reallocate memory
    mesh.e2n_dsp.shrink_to_fit();
    mesh.e2n_con.shrink_to_fit();
    mesh.etags.shrink_to_fit();
    mesh.xyz.shrink_to_fit();
    mesh.lon.assign(mesh.e2n_cnt.size()*3, mt_fib_t(0));
  }