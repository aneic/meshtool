/**
* @file data_structs.h
* @brief The main meshtool data structs.
* @author Aurel Neic
* @version
* @date 2016-12-13
*/

#ifndef _DATA_STRUCTS_H
#define _DATA_STRUCTS_H

#include <cmath>
#include <cstring>
#include <cstdlib>
#include <sys/time.h>

#include "mt_vector.h"
#include "hashmap.hpp"

#include "sort.h"

typedef int64_t mt_idx_t;
typedef int32_t mt_tag_t;
typedef int16_t mt_cnt_t;
typedef double  mt_real;
typedef float   mt_fib_t;

enum elem_t : int8_t
{
  Tetra = 0,
  Hexa = 1,
  Octa = 2,
  Pyramid = 3,
  Prism = 4,
  Quad = 5,
  Tri = 6,
  Line = 7,
  Node = 8,
  NumElemTypes = 9
};

extern const char* const elem_t_str[NumElemTypes];

inline short
get_nnodes(elem_t type)
{
  switch(type) {
    case Node:    return 1;
    case Line:    return 2;
    case Tri:     return 3;
    case Quad:
    case Tetra:   return 4;
    case Pyramid: return 5;
    case Octa:
    case Prism:   return 6;
    case Hexa:    return 8;
    default:      return 0;
  }

  return -1;
}

/**
* @brief The graph of a (sub)mesh
*/
struct mt_meshgraph
{
  mt_vector<elem_t> etype;
  mt_vector<mt_cnt_t> e2n_cnt;
  mt_vector<mt_idx_t> e2n_con;
  mt_vector<mt_idx_t> eidx;
};

/**
* @brief Struct needed for processing data for Neumann BC files.
*/
struct nbc_data {
  mt_vector<mt_idx_t> eidx;
  mt_vector<mt_idx_t> sp_vtx;
  mt_vector<mt_tag_t> tag;
};


/**
* @brief The main mesh struct.
*
* Note the following naming convention:
*  e2n: graph connecting elements to nodes
*  n2e: graph connecting nodes to elements, thus e2n transposed
*  n2n: graph connecting nodes with nodes
*/
struct mt_meshdata
{
  mt_vector<mt_cnt_t>       e2n_cnt;
  mutable mt_vector<mt_idx_t> e2n_dsp;
  mt_vector<mt_idx_t>         e2n_con;

  mt_vector<mt_cnt_t>       n2e_cnt;
  mutable mt_vector<mt_idx_t> n2e_dsp;
  mt_vector<mt_idx_t>         n2e_con;

  mt_vector<mt_cnt_t>       n2n_cnt;
  mutable mt_vector<mt_idx_t> n2n_dsp;
  mt_vector<mt_idx_t>         n2n_con;

  mt_vector<elem_t>   etype;
  mt_vector<mt_tag_t> etags;
  mt_vector<mt_real>  xyz;
  mt_vector<mt_fib_t> lon;

  /// here we register the different element types. e.g.: treg[int(Tetra)] tells us how many Tets we have in the mesh
  mt_vector<size_t>   treg;

  inline void refresh_dsp() const
  {
    if(e2n_cnt.size())
      bucket_sort_offset(e2n_cnt, e2n_dsp);
    if(n2e_cnt.size())
      bucket_sort_offset(n2e_cnt, n2e_dsp);
    if(n2n_cnt.size())
      bucket_sort_offset(n2n_cnt, n2n_dsp);
  }

  inline void clear()
  {
    // using mt_vector::clear() seems a bad idea. we do not want to free, and malloc again. this are
    // syscalls and should be used as little as possible. we already have the memory, why dealloc?
    // switching to a resize(0). -Aurel 01.07.2023
    e2n_cnt.resize(0);
    e2n_dsp.resize(0);
    e2n_con.resize(0);
    n2e_cnt.resize(0);
    n2e_dsp.resize(0);
    n2e_con.resize(0);
    n2n_cnt.resize(0);
    n2n_dsp.resize(0);
    n2n_con.resize(0);
    etype  .resize(0);
    etags  .resize(0);
    xyz    .resize(0);
    lon    .resize(0);
    treg   .resize(0);
  }

  inline void swap(mt_meshdata & inp)
  {
    e2n_cnt.swap(inp.e2n_cnt);
    e2n_dsp.swap(inp.e2n_dsp);
    e2n_con.swap(inp.e2n_con);
    n2e_cnt.swap(inp.n2e_cnt);
    n2e_dsp.swap(inp.n2e_dsp);
    n2e_con.swap(inp.n2e_con);
    n2n_cnt.swap(inp.n2n_cnt);
    n2n_dsp.swap(inp.n2n_dsp);
    n2n_con.swap(inp.n2n_con);
    etype  .swap(inp.etype  );
    etags  .swap(inp.etags  );
    xyz    .swap(inp.xyz    );
    lon    .swap(inp.lon    );
    treg   .swap(inp.treg   );
  }
};

inline void generate_type_reg(mt_meshdata & mesh)
{
  // we create the element type registry to quickly query the element types we have in the mesh
  mesh.treg.assign(size_t(NumElemTypes), size_t(0));

  for(elem_t et : mesh.etype)
    mesh.treg[int(et)]++;
}

inline void generate_single_type_reg(mt_meshdata & mesh, elem_t type, size_t count)
{
  // we create the element type registry to quickly query the element types we have in the mesh
  mesh.treg.assign(size_t(NumElemTypes), size_t(0));
  mesh.treg[int(type)] = count;
}

inline bool
has_surface_elems(const mt_meshdata & mesh)
{
  if(mesh.treg.size() == 0) {
    fprintf(stderr, "warning: mesh type registry not set up! Generating it now!\n");
    mt_meshdata* m = (mt_meshdata*) &mesh;
    generate_type_reg(*m);
  }
  return mesh.treg.size() && ((mesh.treg[int(Tri)] + mesh.treg[int(Quad)]) > 0);
}

inline bool
has_volumetric_elems(const mt_meshdata & mesh)
{
  if(mesh.treg.size() == 0) {
    fprintf(stderr, "warning: mesh type registry not set up! Generating it now!\n");
    mt_meshdata* m = (mt_meshdata*) &mesh;
    generate_type_reg(*m);
  }
  return mesh.treg.size() && ((mesh.treg[int(Tetra)] + mesh.treg[int(Hexa)] + mesh.treg[int(Octa)] + mesh.treg[int(Pyramid)] + mesh.treg[int(Prism)]) > 0);
}

inline bool
mesh_is_hexmesh(const mt_meshdata & mesh)
{
  if(mesh.treg.size() == 0) {
    fprintf(stderr, "warning: mesh type registry not set up! Generating it now!\n");
    mt_meshdata* m = (mt_meshdata*) &mesh;
    generate_type_reg(*m);
  }

  return mesh.e2n_cnt.size() == mesh.treg[int(Hexa)];
}

inline bool
has_line_elems(const mt_meshdata & mesh)
{
  if(mesh.treg.size() == 0) {
    fprintf(stderr, "warning: mesh type registry not set up! Generating it now!\n");
    mt_meshdata* m = (mt_meshdata*) &mesh;
    generate_type_reg(*m);
  }
  return mesh.treg.size() && ((mesh.treg[int(Line)]) > 0);
}

inline bool
has_only_tets(const mt_meshdata & mesh)
{
  if(mesh.treg.size() == 0) {
    fprintf(stderr, "warning: mesh type registry not set up! Generating it now!\n");
    mt_meshdata* m = (mt_meshdata*) &mesh;
    generate_type_reg(*m);
  }

  size_t num_tets = mesh.treg[int(Tetra)];
  size_t num_other =  mesh.treg[int(Hexa)] + mesh.treg[int(Octa)] + mesh.treg[int(Pyramid)] + mesh.treg[int(Prism)] +
                      mesh.treg[int(Tri)]  + mesh.treg[int(Quad)];

  return (num_tets > 0 && num_other == 0);
}

inline bool
has_only_resample_elems(const mt_meshdata & mesh)
{
  if(mesh.treg.size() == 0) {
    fprintf(stderr, "warning: mesh type registry not set up! Generating it now!\n");
    mt_meshdata* m = (mt_meshdata*) &mesh;
    generate_type_reg(*m);
  }

  size_t num_good  =  mesh.treg[int(Tetra)] + mesh.treg[int(Tri)];
  size_t num_other =  mesh.treg[int(Hexa)] + mesh.treg[int(Octa)] + mesh.treg[int(Pyramid)] + mesh.treg[int(Prism)] +
                      mesh.treg[int(Quad)];

  return (num_good > 0 && num_other == 0);
}

class mt_manifold {
  public:
  mt_meshdata mesh;
  mt_mask     on_mnfld;

  inline void set_mask() {
    on_mnfld.insert(mesh.e2n_con.begin(), mesh.e2n_con.end());
  }
};


/**
* @brief Clamp a value into an interval [start, end]
*
* @tparam V   Value type
* @param val    The value we clamp
* @param start  The interval start value
* @param end    The interval end value
*
* @return The clamped value
*/
template<typename V> inline
V clamp(const V val, const V start, const V end, bool warn = false)
{
  bool outside_clamp_region = false;
  V ret = val;

  if(val < start) {
    ret = start;
    outside_clamp_region = true;
  }
  else if(val > end) {
    ret = end;
    outside_clamp_region = true;
  }
  static size_t cnt = 0;
  if(warn && outside_clamp_region) {
    if(cnt < 20)
      fprintf(stderr, "Warning: Clamped value %g outside clamping region [%g, %g]!\n", static_cast<mt_real>(val), static_cast<mt_real>(start), static_cast<mt_real>(end));
    else if(cnt == 20)
      fprintf(stderr, "Warning: Further clamping warnings will be suppressed!\n");
    cnt++;
  }
  return ret;
}

/**
* @brief Get an estimate for the decimal power of a value.
*
* This may be useful for estimate how to scale a mesh etc..
*
*/
template<typename V> inline
int get_dec_power_estimate(V val)
{
  double dec_log = log(val) / log(10);

  // we want to round towards negative infinity also for negative values
  int ret = dec_log < 0 ? (dec_log - 1.0) : dec_log;

  return ret;
}

/**
* @brief A class that stores the graph of a mapping a -> b,
* its transposed (backward) mapping b -> a, a -> a and b -> b.
*
* @tparam T  Index data type.
*/
template<class T>
struct mt_mapping
{
  mt_vector<mt_cnt_t> fwd_cnt;
  mt_vector<T> fwd_dsp;
  mt_vector<T> fwd_con;

  mt_vector<mt_cnt_t> bwd_cnt;
  mt_vector<T> bwd_dsp;
  mt_vector<T> bwd_con;

  mt_vector<mt_cnt_t> a2a_cnt;
  mt_vector<T> a2a_dsp;
  mt_vector<T> a2a_con;

  mt_vector<mt_cnt_t> b2b_cnt;
  mt_vector<T> b2b_dsp;
  mt_vector<T> b2b_con;

  /// create b -> a from a -> b
  void transpose()
  {
    transpose_connectivity(fwd_cnt, fwd_con, bwd_cnt, bwd_con, false);
  }

  void setup_a2a()
  {
    if(bwd_cnt.size() == 0)
      transpose();

    multiply_connectivities(fwd_cnt, fwd_con, bwd_cnt, bwd_con, a2a_cnt, a2a_con);
  }
  void setup_b2b()
  {
    if(bwd_cnt.size() == 0)
      transpose();

    multiply_connectivities(bwd_cnt, bwd_con, fwd_cnt, fwd_con, b2b_cnt, b2b_con);
  }

  /// update displacement vectors
  void setup_dsp()
  {
    fwd_dsp.resize(fwd_cnt.size());
    bucket_sort_offset(fwd_cnt, fwd_dsp);

    bwd_dsp.resize(bwd_cnt.size());
    bucket_sort_offset(bwd_cnt, bwd_dsp);

    a2a_dsp.resize(a2a_cnt.size());
    bucket_sort_offset(a2a_cnt, a2a_dsp);

    b2b_dsp.resize(b2b_cnt.size());
    bucket_sort_offset(b2b_cnt, b2b_dsp);
  }

  void resize(size_t n)
  {
    if(fwd_cnt.size()) fwd_cnt.resize(n), fwd_cnt.shrink_to_fit();
    if(fwd_dsp.size()) fwd_dsp.resize(n), fwd_dsp.shrink_to_fit();
    if(fwd_con.size()) fwd_con.resize(n), fwd_con.shrink_to_fit();
    if(bwd_cnt.size()) bwd_cnt.resize(n), bwd_cnt.shrink_to_fit();
    if(bwd_dsp.size()) bwd_dsp.resize(n), bwd_dsp.shrink_to_fit();
    if(bwd_con.size()) bwd_con.resize(n), bwd_con.shrink_to_fit();
    if(a2a_cnt.size()) a2a_cnt.resize(n), a2a_cnt.shrink_to_fit();
    if(a2a_dsp.size()) a2a_dsp.resize(n), a2a_dsp.shrink_to_fit();
    if(a2a_con.size()) a2a_con.resize(n), a2a_con.shrink_to_fit();
    if(b2b_cnt.size()) b2b_cnt.resize(n), b2b_cnt.shrink_to_fit();
    if(b2b_dsp.size()) b2b_dsp.resize(n), b2b_dsp.shrink_to_fit();
    if(b2b_con.size()) b2b_con.resize(n), b2b_con.shrink_to_fit();
  }
};

struct mt_pscable
{
  mt_vector<mt_real> pts;
  mt_idx_t par1 = -1, par2 = -1;
  mt_idx_t br1 = -1, br2 = -1;
  mt_real size = 7.0, rgj = 100.0, cond = 0.0006;
};

struct mt_psdata {

  mt_vector<mt_pscable> cables;

};


template<class T>
struct mt_tuple {
  T v1;
  T v2;
};
template<class T, class S>
struct mt_mixed_tuple
{
  T v1;
  S v2;
};


template<class T>
struct mt_triple {
  T v1;
  T v2;
  T v3;

  inline T &operator[](const mt_idx_t &idx) {
    switch (idx) {
      default:
      case 0: return v1;
      case 1: return v2;
      case 2: return v3;
    }
  }

  template<class V> inline
  void operator=(const mt_triple<V> & rhs) {
    v1 = T(rhs.v1);
    v2 = T(rhs.v2);
    v3 = T(rhs.v3);
  }

  mt_triple() : v1(T()), v2(T()), v3(T())
  {}

  template<class V>
  mt_triple(V vv1, V vv2, V vv3) : v1(vv1), v2(vv2), v3(vv3)
  {}

  template<class V>
  mt_triple(const mt_triple<V> & rhs) : v1(T(rhs.v1)), v2(T(rhs.v2)), v3(T(rhs.v3))
  {}
};

template<class T, class S, class V>
struct mt_mixed_triple {
  T v1;
  S v2;
  V v3;
};

template<class T>
struct mt_quadruple {
  T v1;
  T v2;
  T v3;
  T v4;
};

template<class T, class S, class V, class W>
struct mt_mixed_quadruple {
  T v1;
  S v2;
  V v3;
  W v4;
};

template<class T>
bool operator<(const struct mt_tuple<T> & lhs, const struct mt_tuple<T> & rhs)
{
  if(lhs.v1 != rhs.v1)
    return lhs.v1 < rhs.v1;
  else
    return lhs.v2 < rhs.v2;
}
template<class T>
bool operator==(const struct mt_tuple<T> & lhs, const struct mt_tuple<T> & rhs)
{
    return lhs.v1 == rhs.v1 && lhs.v2 == rhs.v2;
}

template<class T, class S>
bool operator<(const struct mt_mixed_tuple<T, S> & lhs, const struct mt_mixed_tuple<T, S> & rhs)
{
  if(lhs.v1 != rhs.v1)
    return lhs.v1 < rhs.v1;
  else
    return lhs.v2 < rhs.v2;
}

template<class T>
bool operator<(const struct mt_triple<T> & lhs, const struct mt_triple<T> & rhs)
{
  if(lhs.v1 != rhs.v1)
    return lhs.v1 < rhs.v1;
  else if(lhs.v2 != rhs.v2)
    return lhs.v2 < rhs.v2;
  else
    return lhs.v3 < rhs.v3;
}

template<class T>
bool operator<(const struct mt_quadruple<T> & lhs, const struct mt_quadruple<T> & rhs)
{
  if(lhs.v1 != rhs.v1)
    return lhs.v1 < rhs.v1;
  else if(lhs.v2 != rhs.v2)
    return lhs.v2 < rhs.v2;
  else if(lhs.v3 != rhs.v3)
    return lhs.v3 < rhs.v3;
  else
    return lhs.v4 < rhs.v4;
}

template<class T>
short common_components(const mt_triple<T> & lhs, const mt_triple<T> & rhs)
{
  short c = 0;
  c += short(lhs.v1 == rhs.v1 || lhs.v1 == rhs.v2 || lhs.v1 == rhs.v3);
  c += short(lhs.v2 == rhs.v1 || lhs.v2 == rhs.v2 || lhs.v2 == rhs.v3);
  c += short(lhs.v3 == rhs.v1 || lhs.v3 == rhs.v2 || lhs.v3 == rhs.v3);

  return c;
}

struct edgeele {
  mt_idx_t v1;
  mt_idx_t v2;
  mt_idx_t idx;
  mt_real length;
  float split_at;
};

template<class T, class S>
bool operator<(const struct edgeele & lhs, const struct edgeele & rhs)
{
  if(lhs.v1 != rhs.v1)
    return lhs.v1 < rhs.v1;
  else
    return lhs.v2 < rhs.v2;
}

struct edge_len_dsc {
  bool operator() (const edgeele & lhs, const edgeele & rhs) const
  {
    if (lhs.length != rhs.length) return lhs.length > rhs.length;
    else                          return lhs.idx > rhs.idx;
  }
};
struct edge_len_asc {
  bool operator() (const edgeele & lhs, const edgeele & rhs) const
  {
    if (lhs.length != rhs.length) return lhs.length < rhs.length;
    else                          return lhs.idx < rhs.idx;
  }
};

// expand hashing for custom mt_triple struct
namespace hashmap {

template<typename T>
struct hash_ops< mt_tuple<T> >
{
  static inline bool cmp(mt_tuple<T> a, mt_tuple<T> b) {
    return a.v1 == b.v1 && a.v2 == b.v2;
  }

  static inline hm_uint hash(mt_tuple<T> a) {
    hm_uint h = mkhash_init;
    h = mkhash(h, hash_ops<T>::hash(a.v1));
    h = mkhash(h, hash_ops<T>::hash(a.v2));
    return h;
  }
};

template<typename T>
struct hash_ops< mt_triple<T> >
{
  static inline bool cmp(mt_triple<T> a, mt_triple<T> b) {
    return a.v1 == b.v1 && a.v2 == b.v2 && a.v3 == b.v3;
  }

  static inline hm_uint hash(mt_triple<T> a) {
    hm_uint h = mkhash_init;
    h = mkhash(h, hash_ops<T>::hash(a.v1));
    h = mkhash(h, hash_ops<T>::hash(a.v2));
    h = mkhash(h, hash_ops<T>::hash(a.v3));
    return h;
  }
};
// expand hashing for custom mt_triple struct
template<typename T>
struct hash_ops< mt_quadruple<T> >
{
  static inline bool cmp(mt_quadruple<T> a, mt_quadruple<T> b) {
    return a.v1 == b.v1 && a.v2 == b.v2 && a.v3 == b.v3 && a.v4 == b.v4;
  }

  static inline hm_uint hash(mt_quadruple<T> a) {
    hm_uint h = mkhash_init;
    h = mkhash(h, hash_ops<T>::hash(a.v1));
    h = mkhash(h, hash_ops<T>::hash(a.v2));
    h = mkhash(h, hash_ops<T>::hash(a.v3));
    h = mkhash(h, hash_ops<T>::hash(a.v4));
    return h;
  }
};

template<>
struct hash_ops<edgeele>
{
  static inline bool cmp(edgeele a, edgeele b) {
    return a.v1 == b.v1 && a.v2 == b.v2;
  }

  static inline hm_uint hash(edgeele a) {
    hm_uint h = mkhash_init;
    h = mkhash(h, hash_ops<mt_idx_t>::hash(a.v1));
    h = mkhash(h, hash_ops<mt_idx_t>::hash(a.v2));
    return h;
  }
};

}


inline bool operator== (const edgeele & lhs, const edgeele & rhs)
{
  return lhs.idx == rhs.idx;
}

struct tri_sele {
  mt_idx_t v1;
  mt_idx_t v2;
  mt_idx_t v3;
  mt_idx_t eidx;
};

struct quad_sele {
  mt_idx_t v1;
  mt_idx_t v2;
  mt_idx_t v3;
  mt_idx_t v4;
  mt_idx_t eidx;
};

/**
* @brief A point class to simplify geometric computations.
*
* Only use this for local computations. Do not store an array of mt_points.
*
* @tparam S floating point type.
*/
template<class S>
class mt_point
{
public:
  S x, y, z;

  mt_point()
    : x(S()), y(S()), z(S())
  {}

  explicit mt_point(S val)
    : x(val), y(val), z(val)
  {}

  mt_point(S ix, S iy, S iz)
    : x(ix), y(iy), z(iz)
  {}

  template<class V>
  mt_point(const mt_point<V> & vec)
    : x(vec.x), y(vec.y), z(vec.z)
  {}

  template<class V>
  mt_point(const V* pts)
    : mt_point()
  {
    x = pts[0];
    y = pts[1];
    z = pts[2];
  }

  bool any_is_nan() {
    return std::isnan(x) || std::isnan(y) || std::isnan(z);
  }

  template<class V>
  void operator= (const mt_point<V> & pt)
  {
    x = pt.x;
    y = pt.y;
    z = pt.z;
  }

  void operator+= (const mt_point<S> & pt)
  {
    x += pt.x;
    y += pt.y;
    z += pt.z;
  }

  void operator-= (const mt_point<S> & pt)
  {
    x -= pt.x;
    y -= pt.y;
    z -= pt.z;
  }

  void operator*= (S s)
  {
    x *= s;
    y *= s;
    z *= s;
  }

  void operator/= (S s)
  {
    x /= s;
    y /= s;
    z /= s;
  }

  bool operator== (const mt_point<S>& pt)
  {
      return pt.x == x && pt.y == y && pt.z == z;
  }

  S& operator[] (S idx)
  {
    switch(idx) {
      default:
      case 0: return x;
      case 1: return y;
      case 2: return z;
    }
  }

  const S& operator[] (S idx) const
  {
    switch(idx) {
      default:
      case 0: return x;
      case 1: return y;
      case 2: return z;
    }
  }

  /// return vector length
  S length() const
  {
    S len = sqrt(x*x + y*y + z*z);
    return len;
  }

  /// return squared vector length
  S length2() const
  {
    return (x*x + y*y + z*z);
  }

  mt_point<S> crossProd(const mt_point<S> & v) const
  {
    return mt_point<S>((y * v.z) - (z * v.y),
                       (z * v.x) - (x * v.z),
                       (x * v.y) - (y * v.x));
  }

  S scaProd(const mt_point<S> & v) const
  {
    return x*v.x + y*v.y + z*v.z;
  }

  bool normalize()
  {
    const S len = this->length();
    if (len > S(0)) {
      x /= len;
      y /= len;
      z /= len;
    }
    return (len > S(0));
  }

  mt_point<S> normalized() const
  {
    mt_point<S> nvec(x,y,z);
    const S len = nvec.length();
    if (len > 0.0) {
      nvec.x /= len;
      nvec.y /= len;
      nvec.z /= len;
    }
    return nvec;
  }

  template<class V>
  void get(const V* loc)
  {
    x = loc[0];
    y = loc[1];
    z = loc[2];
  }

  template<class V>
  void set(V* loc) const
  {
    loc[0] = x;
    loc[1] = y;
    loc[2] = z;
  }

  void assign(S ix, S iy, S iz)
  {
    x = ix;
    y = iy;
    z = iz;
  }

  void assign(S val)
  {
    x = y = z = val;
  }

  inline S max_component() const
  {
    S max = x;
    if(max < y) max = y;
    if(max < z) max = z;
    return max;
  }

  std::string to_string() const
  {
      return std::string("[ " + std::to_string(x) + ", " + std::to_string(y) + ", " + std::to_string(z) + " ]");
  }
};

template<class S>
class mt_mat3x3
{
public:
  S xx, xy, xz, yx, yy, yz, zx, zy, zz;

  mt_mat3x3()
    : xx(S()), xy(S()), xz(S()),
      yx(S()), yy(S()), yz(S()),
      zx(S()), zy(S()), zz(S())
  {}

  explicit mt_mat3x3(S val)
    : xx(val), xy(val), xz(val),
      yx(val), yy(val), yz(val),
      zx(val), zy(val), zz(val)
  {}

  mt_mat3x3(S ixx, S ixy, S ixz, S iyx, S iyy, S iyz, S izx, S izy, S izz)
    : xx(ixx), xy(ixy), xz(ixz),
      yx(iyx), yy(iyy), yz(iyz),
      zx(izx), zy(izy), zz(izz)
  {}

  template<class V>
  mt_mat3x3(const mt_point<V> &col0, const mt_point<V> &col1, const mt_point<V> &col2)
    : xx(col0.x), xy(col1.x), xz(col2.x),
      yx(col0.y), yy(col1.y), yz(col2.y),
      zx(col0.z), zy(col1.z), zz(col2.z)
 {}

  template<class V>
  mt_mat3x3(const mt_mat3x3<V> &mat)
    : xx(mat.xx), xy(mat.xy), xz(mat.xz),
      yx(mat.yx), yy(mat.yy), yz(mat.yz),
      zx(mat.zx), zy(mat.zy), zz(mat.zz)
 {}

  template<class V>
  mt_mat3x3(const V *mat)
    : mt_mat3x3()
  {
    xx = mat[0]; xy = mat[1]; xz = mat[2];
    yx = mat[3]; yy = mat[4]; yz = mat[5];
    zx = mat[6]; zy = mat[7]; zz = mat[8];
  }

  void assign(S ixx, S ixy, S ixz, S iyx, S iyy, S iyz, S izx, S izy, S izz)
  {
    xx = ixx; xy = ixy; xz = ixz;
    yx = iyx; yy = iyy; yz = iyz;
    zx = izx; zy = izy; zz = izz;
  }

  void assign(S val)
  {
    xx = xy = xz = val; 
    yx = yy = yz = val;
    zx = zy = zz = val;
  }

  void transpose()
  {
    this->assign(xx, yx, zx, xy, yy, zy, xz, yz, zz);
  }
 
  inline mt_mat3x3<S> transposed() const
  {
    return mt_mat3x3<S>(xx, yx, zx, xy, yy, zy, xz, yz, zz); 
  }

  template<class V>
  void assign(const mt_point<V> &col0, const mt_point<V> &col1, const mt_point<V> &col2)
  {
     xx = col0.x; xy = col1.x; xz = col2.x;
     yx = col0.y; yy = col1.y; yz = col2.y;
     zx = col0.z; zy = col1.z; zz = col2.z;
  }

  S determinant() const
  {
    return xx*(yy*zz-yz*zy)-xy*(yx*zz-yz*zx)+xz*(yx*zy-yy*zx);
  }

  S solve(const mt_point<S> &rhs, mt_point<S> &sol) const
  {
    const S det = determinant();
    if (std::fabs(det) > 0.0)
    {
      sol.x = ((yy*zz-yz*zy)*rhs.x + (xz*zy-xy*zz)*rhs.y + (xy*yz-xz*yy)*rhs.z)/det;
      sol.y = ((yz*zx-yx*zz)*rhs.x + (xx*zz-xz*zx)*rhs.y + (xz*yx-xx*yz)*rhs.z)/det;
      sol.z = ((yx*zy-yy*zx)*rhs.x + (xy*zx-xx*zy)*rhs.y + (xx*yy-xy*yx)*rhs.z)/det;
    }
    return det;
  }

  void disp()
  {
    printf("\n%g %g %g\n", xx, xy, xz);
    printf("%g %g %g\n", yx, yy, yz);
    printf("%g %g %g\n", zx, zy, zz);
  }

  template<class V>
  void operator= (const mt_mat3x3<V> &mat)
  {
    xx = mat.xx; xy = mat.xy; xz = mat.xz;
    yx = mat.yx; yy = mat.yy; yz = mat.yz;
    zx = mat.zx; zy = mat.zy; zz = mat.zz;
  }

  void operator+= (const mt_mat3x3<S> &mat)
  {
    xx += mat.xx; xy += mat.xy; xz += mat.xz;
    yx += mat.yx; yy += mat.yy; yz += mat.yz;
    zx += mat.zx; zy += mat.zy; zz += mat.zz;
  }
  
  void operator-= (const mt_mat3x3<S> &mat)
  {
    xx -= mat.xx; xy -= mat.xy; xz -= mat.xz;
    yx -= mat.yx; yy -= mat.yy; yz -= mat.yz;
    zx -= mat.zx; zy -= mat.zy; zz -= mat.zz;
  }

  inline mt_mat3x3<S> operator+ (const mt_mat3x3<S> &mat)
  {
    return mt_mat3x3<S>(xx+mat.xx, xy+mat.xy, xz+mat.xz,
                        yx+mat.yx, yy+mat.yy, yz+mat.yz,
                        zx+mat.zx, zy+mat.zy, zz+mat.zz);
  }

  inline mt_mat3x3<S> operator- (const mt_mat3x3<S> &mat)
  {
    return mt_mat3x3<S>(xx-mat.xx, xy-mat.xy, xz-mat.xz,
                        yx-mat.yx, yy-mat.yy, yz-mat.yz,
                        zx-mat.zx, zy-mat.zy, zz-mat.zz);
  }

  void operator*= (S s)
  {
    xx *= s; xy *= s; xz *= s;
    yx *= s; yy *= s; yz *= s;
    zx *= s; zy *= s; zz *= s;
  }

  void operator/= (S s)
  {
    xx /= s; xy /= s; xz /= s;
    yx /= s; yy /= s; yz /= s;
    zx /= s; zy /= s; zz /= s;
  }

  inline mt_mat3x3<S> operator* (const S s) const
  {
    return mt_mat3x3<S>(xx*s, xy*s, xz*s, 
                        yx*s, yy*s, yz*s, 
                        zx*s, zy*s, zz*s);
  }

  inline mt_point<S> dotProd(const mt_point<S> &pt) const
  {
    return mt_point<S>(xx*pt.x + xy*pt.y + xz*pt.z,
                       yx*pt.x + yy*pt.y + yz*pt.z,
                       zx*pt.x + zy*pt.y + zz*pt.z);
  }

  inline mt_mat3x3<S> dotProd(const mt_mat3x3<S> &mat) const
  {
    return mt_mat3x3<S>(mat.zx*xz + mat.yx*xy + mat.xx*xx, 
                        mat.zy*xz + mat.yy*xy + mat.xy*xx, 
                        mat.zz*xz + mat.yz*xy + mat.xz*xx,
                        mat.zx*yz + mat.yx*yy + mat.xx*yx, 
                        mat.zy*yz + mat.yy*yy + mat.xy*yx, 
                        mat.zz*yz + mat.yz*yy + mat.xz*yx,
                        mat.zx*zz + mat.yx*zy + mat.xx*zx, 
                        mat.zy*zz + mat.yy*zy + mat.xy*zx, 
                        mat.zz*zz + mat.yz*zy + mat.xz*zx);
  }
};

template<class S>
mt_mat3x3<S> operator- (const mt_mat3x3<S> &m)
{
  return mt_mat3x3<S>(-m.xx, -m.xy, -m.xz,
                      -m.yx, -m.yy, -m.yz,
                      -m.zx, -m.zy, -m.zz);
}

template<class S>
mt_point<S> operator+ (const mt_point<S> & lhs, const mt_point<S> & rhs)
{
  return mt_point<S>(lhs.x + rhs.x, lhs.y + rhs.y, lhs.z + rhs.z);
}

template<class S>
mt_point<S> operator- (const mt_point<S> & lhs, const mt_point<S> & rhs)
{
  return mt_point<S>(lhs.x - rhs.x, lhs.y - rhs.y, lhs.z - rhs.z);
}

template<class S, class V>
mt_point<S> operator* (const mt_point<S> & lhs, const V s)
{
  return mt_point<S>(lhs.x * s, lhs.y * s, lhs.z * s);
}

template<class S, class V>
mt_point<V> operator* (const mt_mat3x3<S> & lhs, const mt_point<V> & rhs)
{
  return mt_point<S>(lhs.xx * rhs.x + lhs.xy * rhs.y + lhs.xz * rhs.z,
                     lhs.yx * rhs.x + lhs.yy * rhs.y + lhs.yz * rhs.z,
                     lhs.zx * rhs.x + lhs.zy * rhs.y + lhs.zz * rhs.z);
}

template<class S, class V>
mt_point<S> operator* (const mt_point<S> & lhs, const mt_point<V> & rhs)
{
  return mt_point<S>(lhs.x * rhs.x, lhs.y * rhs.y, lhs.z * rhs.z);
}

template<class S, class V>
mt_point<S> operator/ (const mt_point<S> & lhs, const V s)
{
  return mt_point<S>(lhs.x / s, lhs.y / s, lhs.z / s);
}

template<class S>
mt_point<S> operator- (const mt_point<S> &p)
{
  return mt_point<S>(-p.x, -p.y, -p.z);
}

template<class S>
inline mt_mat3x3<S> outerProd(const mt_point<S> &u, const mt_point<S> &v)
{
  return mt_mat3x3<S>(u.x*v.x, u.x*v.y, u.x*v.z,
                      u.y*v.x, u.y*v.y, u.y*v.z,
                      u.z*v.x, u.z*v.y, u.z*v.z);

}

template<class S>
S invert_mat3x3(mt_mat3x3<S> &mat)
{
  const S det = mat.determinant();
  if (std::fabs(det) > 0.0) {
    mat.assign((mat.yy*mat.zz-mat.yz*mat.zy)/det, (mat.xz*mat.zy-mat.xy*mat.zz)/det, (mat.xy*mat.yz-mat.xz*mat.yy)/det,
               (mat.yz*mat.zx-mat.yx*mat.zz)/det, (mat.xx*mat.zz-mat.xz*mat.zx)/det, (mat.xz*mat.yx-mat.xx*mat.yz)/det,
               (mat.yx*mat.zy-mat.yy*mat.zx)/det, (mat.xy*mat.zx-mat.xx*mat.zy)/det, (mat.xx*mat.yy-mat.xy*mat.yx)/det);
  }
  return det;
}

template<class S>
S invert_mat3x3(mt_mat3x3<S> & mat, const S det)
{
  mat.assign((mat.yy*mat.zz-mat.yz*mat.zy)/det, (mat.xz*mat.zy-mat.xy*mat.zz)/det, (mat.xy*mat.yz-mat.xz*mat.yy)/det,
             (mat.yz*mat.zx-mat.yx*mat.zz)/det, (mat.xx*mat.zz-mat.xz*mat.zx)/det, (mat.xz*mat.yx-mat.xx*mat.yz)/det,
             (mat.yx*mat.zy-mat.yy*mat.zx)/det, (mat.xy*mat.zx-mat.xx*mat.zy)/det, (mat.xx*mat.yy-mat.xy*mat.yx)/det);

  return det;
}

template<class T> inline
void identity(mt_mat3x3<T> & mat)
{
  mat.xx = 1.0, mat.xy = 0.0, mat.xz = 0.0;
  mat.yx = 0.0, mat.yy = 1.0, mat.yz = 0.0;
  mat.zx = 0.0, mat.zy = 0.0, mat.zz = 1.0;
}

template<class S>
S invert_mat2x2(mt_mat3x3<S> &mat)
{
  // save block entries in temp variables
  const S e0 = mat.xx, e1 = mat.xy, e2 = mat.yx, e3 = mat.yy;
  const S det = e0*e3 - e1*e2;

  if (std::fabs(det) > 0.0)
  {
    mat.assign( e3, -e1, 0.0,
               -e2,  e0, 0.0,
               0.0, 0.0, 0.0);
    mat /= det;
  }

  return det;
}

template<class S>
inline S determinant_mat2x2(mt_mat3x3<S> &mat)
{
  return (mat.xx*mat.yy-mat.xy*mat.yx);
}

template<class S, class V>
void array_to_points(const mt_vector<S> & xyz, mt_vector<mt_point<V> > & p)
{
  p.resize(xyz.size()/3);
  for(size_t i=0; i<p.size(); i++)
  {
    p[i].x = xyz[i*3+0];
    p[i].y = xyz[i*3+1];
    p[i].z = xyz[i*3+2];
  }
}

template<class S, class V>
void points_to_array(const mt_vector<mt_point<S> > & p, mt_vector<V> & xyz)
{
  xyz.resize(p.size()*3);
  for(size_t i=0; i<p.size(); i++)
  {
    xyz[i*3+0] = p[i].x;
    xyz[i*3+1] = p[i].y;
    xyz[i*3+2] = p[i].z;
  }
}

template<class S>
S point_distance(const mt_point<S> & p0, const mt_point<S> & p1)
{
  return std::sqrt((p0.x-p1.x)*(p0.x-p1.x)+(p0.y-p1.y)*(p0.y-p1.y)+(p0.z-p1.z)*(p0.z-p1.z));
}

template<class S>
mt_point<S> point_min(const mt_point<S> & p0, const mt_point<S> & p1)
{
  return mt_point<S>(std::min<S>(p0.x, p1.x), std::min<S>(p0.y, p1.y), std::min<S>(p0.z, p1.z));
}

template<class S>
mt_point<S> point_max(const mt_point<S> & p0, const mt_point<S> & p1)
{
  return mt_point<S>(std::max<S>(p0.x, p1.x), std::max<S>(p0.y, p1.y), std::max<S>(p0.z, p1.z));
}

template<class S>
mt_point<S> unit_vector(const mt_point<S> & v)
{
  S len = v.length();
  return mt_point<S>(v.x / len, v.y / len, v.z / len);
}

template<class T, class V>
inline mt_point<T> cast_vec(const V & vec)
{
  mt_point<T> ret;
  ret.x = vec.x, ret.y = vec.y, ret.z = vec.z;

  return ret;
}

template<class T, class V>
inline mt_tuple<T> cast_mt_tuple(const mt_tuple<V> & t)
{
  return mt_tuple<T>{static_cast<T>(t.v1), static_cast<T>(t.v2)};
}

template<class T, class V>
inline mt_tuple<T> cast_mt_tuple(const V & v1, const V & v2)
{
  return mt_tuple<T>{static_cast<T>(v1), static_cast<T>(v2)};
}
  
  
template<class T, class V>
inline mt_triple<T> cast_mt_triple(const mt_triple<V> & t)
{
  return mt_triple<T>{static_cast<T>(t.v1), static_cast<T>(t.v2), static_cast<T>(t.v3)};
}

template<class T, class V>
inline mt_triple<T> cast_mt_triple(const V & v1, const V & v2, const V & v3)
{
  return mt_triple<T>{static_cast<T>(v1), static_cast<T>(v2), static_cast<T>(v3)};
}


typedef mt_point<float>   vec3f;
typedef mt_point<mt_real> vec3r;
typedef mt_point<mt_idx_t>  vec3i;

typedef mt_mat3x3<float> mat3x3f;
typedef mt_mat3x3<mt_real> mat3x3r;

// wrapper routine to switch between fast inverse sqrt algorithm (for floats) or normal
float inv_sqrtf(const float number );
// wrapper routine to switch between fast inverse sqrt algorithm (for doubles) or normal
double inv_sqrtd(const double number);

//Explict specialization for float values with fast inverse sq algorithm
template<> inline
bool mt_point<float>::normalize() {
  const float len2 = this->length2();
  if (len2 > 0.0) {
    const float invsq = inv_sqrtf(len2);//fast_inv_sqrtf(lensq);
    this->x *= invsq;
    this->y *= invsq;
    this->z *= invsq;
  }
 return (len2 > 0.0);  
}

//Explict specialization for double values with fast inverse sq algorithm
template<> inline
bool mt_point<double>::normalize() {
  const double len2 = this->length2();
  if (len2 > 0.0) {
    const double invsq = inv_sqrtd(len2);
    this->x *= invsq;
    this->y *= invsq;
    this->z *= invsq;    
  }
  return (len2 > 0.0);
}

//Explicit specialization for floats
template<> inline
mt_point<float> unit_vector(const mt_point<float> & v)
{
  const float invsq = inv_sqrtf(v.length2());
  return mt_point<float>(v.x * invsq, v.y * invsq, v.z * invsq);
}

template<> inline
mt_point<double> unit_vector(const mt_point<double> & v)
{
  const double invsq = inv_sqrtd(v.length2());
  return mt_point<double>(v.x * invsq, v.y * invsq, v.z * invsq);
}

/**
* @brief Linear interpolation.
*
* The scaling between y0 and y1 is computed w.r.t. the location of
* x between x0 and y1
*
* @tparam S   Floating point type for the scaling
* @tparam V   Value type of the interpolateion
*
* @return The interpolated value
*/
template<class S, class V>
V lerp(S x0, S x1, S x, V y0, V y1)
{
  S alpha = (x - x0) / (x1 - x0);
  alpha = clamp(alpha, S(0), S(1));

  V y = (y0 * (S(1) - alpha)) + (y1 * alpha);

  return y;
}
/// lerp with directly provided interpolation parameter
template<class S, class V> inline
V lerp(S t, V y0, V y1)
{
  V y = (y0 * (S(1) - t)) + (y1 * t);
  return y;
}

/// 2nd order bezier interpolation
template<class S, class V>
V bezier2(S x0, S x1, S x, V y0, V y1, V y2)
{
  S t = (x - x0) / (x1 - x0);
  t = clamp(t, S(0), S(1));
  S one_minus_t = 1 - t;

  V y = y0 * (one_minus_t*one_minus_t) +
        y1 * (2*one_minus_t*t)         +
        y2 * (t*t);

  return y;
}

/// 3rd order bezier interpolation
template<class S, class V>
V bezier3(S x0, S x1, S x, V y0, V y1, V y2, V y3)
{
  S t = (x - x0) / (x1 - x0);
  t = clamp(t, S(0), S(1));
  S one_minus_t = 1 - t;

  V y = y0 * (one_minus_t*one_minus_t*one_minus_t) +
        y1 * (one_minus_t*one_minus_t*t*3)         +
        y2 * (one_minus_t*t*t*3)                   +
        y3 * (t*t*t);

  return y;
}

/// n-order implementation
template<class S, class V>
V bezier_n(S x0, S x1, S x, V* y_start, V* y_end)
{
  S t = (x - x0) / (x1 - x0);
  t = clamp(t, S(0), S(1));

  mt_vector<V> yy; yy.assign(y_start, y_end);

  while(yy.size() > 1) {
    for(size_t i=0; i < yy.size()-1; i++)
      yy[i] = lerp(t, yy[i], yy[i+1]);
    yy.pop_back();
  }

  return yy[0];
}

/// generic bezier wrapper, that calls the most efficient interpolation
template<class S, class V, class VEC>
V bezier(S x0, S x1, S x, VEC & y)
{
  V ret = V();

  switch(y.size()) {
    case 2: ret = lerp   (x0, x1, x, y[0], y[1]);             break;
    case 3: ret = bezier2(x0, x1, x, y[0], y[1], y[2]);       break;
    case 4: ret = bezier3(x0, x1, x, y[0], y[1], y[2], y[3]); break;

    default:
      ret = bezier_n(x0, x1, x, y.begin(), y.end());          break;
  }

  return ret;
}


void mesh_serialize_base64(const mt_meshdata & mesh, mt_vector<unsigned char> & buff);
void mesh_deserialize_base64(mt_meshdata & mesh, const mt_vector<unsigned char> & buff);

class mt_timer {
private:
  struct timeval t1;
  struct timeval t2;
  double value = 0.0;

  inline double comp_timediff_msec() {
    return ((t2.tv_sec - t1.tv_sec)*1e6 + (t2.tv_usec - t1.tv_usec))/1e3;
  }

public:
  inline void start() {
    gettimeofday(&t1, NULL);
  }

  inline void stop() {
    gettimeofday(&t2, NULL);
    value += comp_timediff_msec();
    t1 = t2;
  }

  inline void reset() {
    value = 0.0;
  }

  inline void reset_and_start() {
    reset();
    start();
  }

  inline double get_time_msec() {
    return value;
  }

  inline double get_time_sec() {
    return value/1000.0;
  }
};

#endif
