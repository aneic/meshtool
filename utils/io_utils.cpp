/**
* @file io_utils.cpp
* @brief General IO utils.
* @author Aurel Neic
* @version
* @date 2017-08-04
*/

#include "mt_utils.h"
#include "asciireader.hpp"

#include <unistd.h>  // needed for access()
#include <dirent.h>
#include <sys/stat.h>

#ifdef __WIN32__
#include <windows.h>
#endif

// global error manager variable
io_error_manager mt_err_mng;

elem_t getElemTypeID(char *eletype)
{
  elem_t ret = NumElemTypes;

  if ( !strcmp( eletype, "Tt" ) ) {
    ret = Tetra;
  } else if ( !strcmp( eletype, "Hx" ) ) {
    ret = Hexa;
//  } else if ( !strcmp( eletype, "Oc" ) ) {
//    ret = Octa;
  } else if ( !strcmp( eletype, "Py" ) ) {
    ret = Pyramid;
  } else if ( !strcmp( eletype, "Pr" ) ) {
    ret = Prism;
  } else if ( !strcmp( eletype, "Qd" ) ) {
    ret = Quad;
  } else if ( !strcmp( eletype, "Tr" ) ) {
    ret = Tri;
  } else if ( !strcmp( eletype, "Ln" ) ) {
    ret = Line;
  }
  return ret;
}

size_t mt_ftell(FILE* fd)
{
  size_t ret = 0;
#ifdef __WIN32__
  off64_t pos = ftello64(fd);
  ret = pos < 0 ? -pos : pos;
#else
  ret = ftell(fd);
#endif
  return ret;
}

void mt_fseek(FILE* fd, long delta, int start_flag)
{
#ifdef __WIN32__
  off64_t od = (off64_t) delta;
  fseeko64(fd, od, start_flag);
#else
  fseek(fd, delta, start_flag);
#endif
}

void treat_file_open_error(const std::string & file)
{
  mt_err_mng.update_error(file.c_str(), true);

  fprintf(stderr, "An IO error occured when opening file %s:\n%s\n\n",
          mt_err_mng.error_file(), mt_err_mng.error_msg());

  if(mt_err_mng.quit_on_error)
    exit(EXIT_FAILURE);
}

void treat_file_error(const char* msg, FILE* fd)
{
  fprintf(stderr, "%s\n", msg);
  fclose(fd);
  exit(1);
}

bool file_exists(const std::string & filename)
{
  return (access(filename.c_str(), F_OK) == 0);
}

bool file_is_dir(const char* filename)
{
   struct stat statbuf;

   if (stat(filename, &statbuf) != 0)
       return false;

   return S_ISDIR(statbuf.st_mode);
}


bool file_is_regfile(const char* filename)
{
   struct stat statbuf;

   if (stat(filename, &statbuf) != 0)
       return false;

   return S_ISREG(statbuf.st_mode);
}

bool make_dir(const std::string & path)
{
#ifdef __WIN32__
  const BOOL rval = CreateDirectory(path.c_str(), NULL);
  return ((rval == TRUE) || (GetLastError() == ERROR_ALREADY_EXISTS));
#else
  const int rval = mkdir(path.c_str(), S_IRWXU | S_IRWXG | S_IRWXO);
  return ((rval == 0) || ((rval != 0) && (errno == EEXIST)));
#endif
}

#ifdef __APPLE__
#include <mach-o/dyld.h>
#endif

bool get_self_path(char* path_buffer, const size_t buffsize)
{
#ifdef __WIN32__
  GetModuleFileNameA(NULL, path_buffer, buffsize);
  return true;
#else
  #ifdef __APPLE__
  uint32_t bsz = buffsize;
  int ret = _NSGetExecutablePath(path_buffer, &bsz);
  if(bsz < buffsize-1) path_buffer[bsz] = 0;

  return ret == 0;
  #else // LINUX
  ssize_t count = readlink("/proc/self/exe", path_buffer, buffsize);

  // readlink does not append a zero. very nice, thank you ..
  if(count < buffsize-1) path_buffer[count] = 0;
  else path_buffer[buffsize-1] = 0;

  return count > 0;
  #endif
#endif
}

std::string make_path_relative(std::string root, std::string path)
{
  if (root.empty() || path.empty())
    return path;

  bool success  = false;
  std::string ret;
  std::string inp_path = path;

  constexpr char delim = '/';
#ifdef __WIN32__
  if(root[0] != path[0]) // we are not no the same drive, abort!
    return inp_path;
  else {
    // get rid of drive letters, since we are on the same drive
    root = std::string(root.begin()+2, root.end());
    path = std::string(path.begin()+2, path.end());
  }

  constexpr char win_delim = '\\';
  // also for windows, we want to use unix delims to be cross-platform compatible
  for(char & c : root) if(c == win_delim) c = delim;
  for(char & c : path) if(c == win_delim) c = delim;
#else

  // here we deal with symbolic links in the path name
  if(file_exists(root)) {
    root = mt_fullpath(root.c_str());
  }
  if(file_exists(path)) {
    path = mt_fullpath(path.c_str());
  }
#endif

  std::string path_dir = mt_dirname(path);

  mt_vector<std::string> path_dirlist, root_dirlist;
  split_string(path_dir, delim, path_dirlist);
  split_string(root,     delim, root_dirlist);

  size_t ndir_root = root_dirlist.size(), ndir_path = path_dirlist.size(), ndir_common = 0;

  size_t common_end = 1, idx = 0, max_idx = std::min(ndir_root, ndir_path);
  while(idx < max_idx && root_dirlist[idx] == path_dirlist[idx]) {
    ndir_common++;
    common_end += root_dirlist[idx].size() + 1;
    idx++;
  }

  if(ndir_common == root_dirlist.size()) {
    ret = std::string(".") + delim;
    ret += std::string(path.begin() + common_end, path.end());
    success = true;
  } else {
    if(ndir_common < ndir_root) {
      for(int i=0; i<int(ndir_root - ndir_common); i++)
        ret += std::string("..") + delim;

      ret += std::string(path.begin() + common_end, path.end());
      success = true;
    } else {
      success = false;
    }
  }

  if(success) {
    // printf("success!\nroot: %s\npath: %s\nrel path:%s\n", root.c_str(), path.c_str(), ret.c_str());
    return ret;
  } else {
    // printf("fail!\nroot: %s\npath: %s\n\n", root.c_str(), path.c_str());
    return inp_path;
  }
}

/// Get the size of the file behind the file-descriptor fd
size_t file_size(FILE* fd)
{
  size_t oldpos = mt_ftell(fd);
  mt_fseek(fd, 0L, SEEK_END);
  size_t sz = mt_ftell(fd);
  mt_fseek(fd, oldpos, SEEK_SET);

  return sz;
}

size_t file_size(const char* file)
{
  FILE* fd = fopen(file, MT_FOPEN_READ);
  if(fd == NULL) return 0;

  size_t ret = file_size(fd);
  fclose(fd);

  return ret;
}

bool copy_file(const std::string & in, const std::string & out)
{
  size_t num_read = 0, num_write = 0;
  FILE* fd_in = fopen(in.c_str(), MT_FOPEN_READ);
  if(fd_in) {
    FILE* fd_out = fopen(out.c_str(), MT_FOPEN_WRITE);
    if(fd_out) {
      char buff[1024];
      const size_t buffsize = sizeof(buff)/sizeof(char);

      size_t count = fread(buff, 1, buffsize, fd_in);
      num_read += count;

      while(count) {
        num_write += fwrite(buff, 1, count, fd_out);
        count = fread(buff, 1, buffsize, fd_in);
        num_read += count;
      }

      fclose(fd_out);
    }
    fclose(fd_in);
  }

  return num_read > 0 && num_read == num_write;
}

// ----------------------------------------------------------------------------
// remove_file
bool remove_file(const std::string& path) {
  return (unlink(path.c_str()) == 0);
} // remove_file(...)


// ----------------------------------------------------------------------------
// remove_directory
bool remove_directory(const std::string& path, const bool verbose) {
#ifdef __WIN32__
  // if path does not exists or is not dir - exit with status -1
  if (!file_is_dir(path))
    return false;

  // if not possible to read the directory for this user
  DIR* dir = nullptr;
  if ((dir = opendir(path.c_str())) == nullptr)
    return false;

  // iteration through entries in the directory
  std::string entry_name, entry_path;
  dirent* entry = nullptr;
  bool error = false;
  while ((!error) && ((entry = readdir(dir)) != nullptr))
  {
    entry_name.assign(entry->d_name);

    // skip entries "." and ".."
    if ((entry_name == ".") || (entry_name == ".."))
      continue;

    // determinate a full path of an entry
    entry_path = path + '/' + entry_name;

    // recursively remove a nested directory, file or link
    if (file_is_dir(entry_path))
      error = !remove_directory(entry_path, verbose);
    else if (file_is_regfile(entry_path)) {
      if (verbose) std::cout << "removing file '" << entry_path << "' ..." << std::endl;
      error = (unlink(entry_path.c_str()) != 0);
      if ((verbose) && (error))
        std::cerr << "failed to remove file '" << entry_path << "'!" << std::endl;
    } 
    else
      error = true;
  }
  closedir(dir);

  // remove the devastated directory and close the object of it
  if (verbose) std::cout << "removing directory '" << path << "' ..." << std::endl;
  error = (rmdir(path.c_str()) != 0);
  if ((verbose) && (error)) std::cerr << "failed to remove directory '" << path << "'!" << std::endl;

  return !error;
#else
  struct stat statbuf;

  // stat for the path
  lstat(path.c_str(), &statbuf);

  // if path does not exists or is not dir - exit with status -1
  if (!S_ISDIR(statbuf.st_mode))
    return false;

  // if not possible to read the directory for this user
  DIR* dir = nullptr;
  if ((dir = opendir(path.c_str())) == nullptr)
    return false;

  // iteration through entries in the directory
  std::string entry_name, entry_path;
  dirent* entry = nullptr;
  bool error = false;
  while ((!error) && ((entry = readdir(dir)) != nullptr)) {

    entry_name.assign(entry->d_name);

    // skip entries "." and ".."
    if ((entry_name == ".") || (entry_name == ".."))
      continue;

    // determinate a full path of an entry
    entry_path = path + '/' + entry_name;

    // lstat for the entry
    lstat(entry_path.c_str(), &statbuf);

    // recursively remove a nested directory, file or link
    if (S_ISDIR(statbuf.st_mode))
      error = !remove_directory(entry_path.c_str(), verbose);
    else if (S_ISREG(statbuf.st_mode)) {
      if (verbose)
        std::cout << "removing file '" << entry_path << "' ..." << std::endl;
      error = (unlink(entry_path.c_str()) != 0);
      if ((verbose) && (error))
        std::cerr << "failed to remove file '" << entry_path << "'!" << std::endl;
    }
    else if (S_ISLNK(statbuf.st_mode)) {
      if (verbose)
        std::cout << "removing link '" << entry_path << "' ..." << std::endl;
      error = (unlink(entry_path.c_str()) != 0);
      if ((verbose) && (error))
        std::cerr << "failed to remove link '" << entry_path << "'!" << std::endl;
    }
    else
      error = true;
  }
  closedir(dir);

  // remove the devastated directory and close the object of it
  if (verbose)
    std::cout << "removing directory '" << path << "' ..." << std::endl;
  error = (rmdir(path.c_str()) != 0);
  if ((verbose) && (error))
    std::cerr << "failed to remove directory '" << path << "'!" << std::endl;

  return !error;
#endif
} // remove_directory(...)



void generate_default_fib(size_t numelem, mt_vector<mt_fib_t> & lon)
{
  lon.resize(numelem*3);
  for(size_t i=0; i<numelem; i++) {
    lon[i*3+0] = 1.0;
    lon[i*3+1] = 0.0;
    lon[i*3+2] = 0.0;
  }
}

void writeElements(mt_meshdata & mesh, std::string file)
{
  FILE* ele_file = fopen(file.c_str(), MT_FOPEN_WRITE);
  if(ele_file == NULL) {
    treat_file_open_error(file);
  } else {
    size_t numele = mesh.e2n_cnt.size();
    fprintf(ele_file, "%zu\n", numele);
    PROGRESS<size_t> prg(numele, "Writing elements in text CARP format: ");

    for(size_t i=0, idx = 0; i<numele; i++)
    {
      prg.next();

      std::string etyp;
      switch(mesh.etype[i])
      {
        case Line:
          etyp = "Ln";
          break;

        case Tri:
          etyp = "Tr";
          break;

        case Quad:
          etyp = "Qd";
          break;

        case Tetra:
          etyp = "Tt";
          break;

        case Pyramid:
          etyp = "Py";
          break;

        case Prism:
          etyp = "Pr";
          break;

        case Hexa:
          etyp = "Hx";
          break;

        default:
          fprintf(stderr, "%s error: Unsupported element type!\n", __func__);
          fclose(ele_file);
          exit(1);
      }
      fprintf(ele_file, "%s ", etyp.c_str());
      for(mt_idx_t j=0; j<mesh.e2n_cnt[i]; j++, idx++)
        fprintf(ele_file, "%ld ", (long)mesh.e2n_con[idx]);

      fprintf(ele_file, "%d\n", (int)mesh.etags[i]);
    }
    prg.finish();

    fclose(ele_file);
  }
}

void writeElementsBinary(mt_meshdata & mesh, const std::string file)
{
  mt_bfile ele_file;
  ele_file.open_write(file.c_str(), 10*1024*1024);

  if(ele_file.ok()) {
    size_t numele = mesh.e2n_cnt.size();

    // we need to write the CARP header
    char header[BIN_HDR_SIZE]; memset(header, 0, sizeof(header));
    int checksum = 666;
    int n[10];

    // first elements, second endiannes, third checksum
    sprintf(header, "%zu %d %d", numele, MT_ENDIANNESS, checksum);
    ele_file.write(header, sizeof(char), BIN_HDR_SIZE);

    PROGRESS<size_t> prg(numele, "Writing elements in binary CARP format: ");

    mt_idx_t* elem = mesh.e2n_con.data();
    for(size_t i=0; i<numele; i++)
    {
      prg.next();

      mt_idx_t nodes = mesh.e2n_cnt[i];

      // element type
      n[0] = mesh.etype[i];
      ele_file.write(n, sizeof(int), 1);

      // element connectivity
      for(mt_idx_t j=0; j<nodes; j++) n[j] = elem[j];
      ele_file.write(n, sizeof(int), nodes);

      // element tag
      n[0] = mesh.etags[i];
      ele_file.write(n, sizeof(int), 1);

      elem += nodes;
    }
    prg.finish();

    ele_file.clear();
  }
}

void readElements(mt_meshdata & mesh, std::string file)
{
  const int bufsize = 2048;
  char buffer[bufsize];
  const int max_elem_size = 9;

  unsigned long int numele = 0;

  asciireader reader;
  reader.read_file(file, false);

  if(reader.num_lines() == 0) {
    treat_file_open_error(file);
  } else {
    if(reader.get_line(0, buffer, bufsize))
      sscanf(buffer, "%lu", &numele);

    mesh.e2n_cnt.resize(numele);
    mesh.etags.assign(size_t(numele), mt_cnt_t(0));
    mesh.etype.resize(numele);
    mesh.e2n_con.assign(size_t(numele*max_elem_size), mt_idx_t(-1));
    PROGRESS<size_t> prg(numele, "Reading elements in text CARP format: ");

    #ifdef OPENMP
    #pragma omp parallel private(buffer)
    #endif
    {
      char etype_str[16];
      long int n[max_elem_size]; memset(n, 0, max_elem_size*sizeof(long int));
      int num_read = 0;
      const int check_intervall = 100;

      #ifdef OPENMP
      #pragma omp for schedule(dynamic, 100)
      #endif
      for(size_t i=0; i<numele; i++)
      {
        if(reader.get_line(i+1, buffer, bufsize))
        {
          // read the element
          //int numread =
          int nread = sscanf(buffer, "%s %ld %ld %ld %ld %ld %ld %ld %ld %ld", etype_str,
                                     n, n+1, n+2, n+3, n+4, n+5, n+6, n+7, n+8);
          mesh.etype[i] = getElemTypeID(etype_str);
          mt_idx_t nodes;
          switch(mesh.etype[i])
          {
            case Line:
              nodes = 2;
              break;

            case Tri:
              nodes = 3;
              break;

            case Quad:
              nodes = 4;
              break;

            case Tetra:
              nodes = 4;
              break;

            case Pyramid:
              nodes = 5;
              break;

            case Prism:
              nodes = 6;
              break;

            case Hexa:
              nodes = 8;
              break;

            default:
              fprintf(stderr, "%s error: Unsupported element type!\n", __func__);
              exit(1);
          }
          // set element size
          mesh.e2n_cnt[i] = nodes;

          // copy the element connectivity
          for(int j=0; j<nodes; j++) mesh.e2n_con[i*max_elem_size+j] = n[j];

          if(nread > nodes)
            mesh.etags[i] = n[nodes];

          num_read++;
        }

        if(num_read == check_intervall) {
          #ifdef OPENMP
          #pragma omp critical
          #endif
          {
            prg.increment(num_read);
          }
          num_read = 0;
        }
      }
    }

    prg.finish();

    reduce_by_flag(mesh.e2n_con, mt_idx_t(-1));
    mesh.e2n_con.shrink_to_fit();
  }
}

void readElementsBinary(mt_meshdata & mesh, std::string file)
{
  mt_bfile ele_file;
  ele_file.open(file.c_str());

  if(ele_file.ok()) {
    const int max_elem_size = 8;
    unsigned long int numele, numentries = 0;
    int n[max_elem_size]; memset(n, 0, max_elem_size*sizeof(int));

    char header[BIN_HDR_SIZE]; memset(header, 0, sizeof(header));
    ele_file.read(header, sizeof(char), BIN_HDR_SIZE);
    sscanf(header, "%lu", &numele);

    mesh.e2n_cnt.resize(numele);
    mesh.etags.resize(numele);
    mesh.etype.resize(numele);
    mesh.e2n_con.assign(numele*max_elem_size, -1);
    int etbuff;
    mt_idx_t* elem = mesh.e2n_con.data();
    PROGRESS<size_t> prg(numele, "Reading elements in binary CARP format: ");

    for(size_t i=0; i<numele; i++)
    {
      prg.next();

      ele_file.read(&etbuff, sizeof(int), 1);
      mesh.etype[i] = (elem_t)etbuff;
      int nodes = 0;
      switch(mesh.etype[i])
      {
        case Line:
          nodes = 2;
          break;

        case Tri:
          nodes = 3;
          break;

        case Quad:
        case Tetra:
          nodes = 4;
          break;

        case Pyramid:
          nodes = 5;
          break;

        case Octa:
        case Prism:
          nodes = 6;
          break;

        case Hexa:
          nodes = 8;
          break;

        default:
          fprintf(stderr, "%s error: Unsupported element type!\n", __func__);
          exit(1);
      }
      mesh.e2n_cnt[i] = nodes;

      int nread = ele_file.read(n, sizeof(int), nodes);

      if(nread != nodes)
        fprintf(stderr, "%s error: read only %d nodes instead of %d.\n", __func__, nread, nodes);

      // copy the element connectivity
      for(int j=0; j<nodes; j++) elem[j] = n[j];
      elem += nodes;
      numentries += nodes;

      // read tag
      nread = ele_file.read(n, sizeof(int), 1);
      mesh.etags[i] = n[0];

      if(nread != 1)
        fprintf(stderr, "%s error: could not read tag.\n", __func__);
    }
    prg.finish();

    mesh.e2n_con.resize(numentries);
    mesh.e2n_con.shrink_to_fit();
  }
}

void readElements_general(mt_meshdata & mesh,
                          std::string basename)
{
  std::string binname = basename + CARPBIN_ELEM_EXT;
  std::string txtname = basename + CARPTXT_ELEM_EXT;

  if(file_exists(binname))
    readElementsBinary(mesh, binname);
  else
    readElements(mesh, txtname);
}

void readElementTags_general(mt_vector<mt_tag_t> & etags, std::string file)
{
  if(!file_exists(file)) {
    printf("Element-tags file %s does not exist!", file.c_str());
    return;
  }

  if (endswith(file, BIN_TAGS_EXT)) {
    mt_vector<int32_t> invec;
    binary_read(invec, file);
    etags.assign(invec.begin(), invec.end());
  }
  else
    read_vector_ascii(etags, file, false);
}

size_t readNumPoints(std::string file)
{
  FILE* pts_file = fopen(file.c_str(), MT_FOPEN_READ);
  if(pts_file == NULL) {
    treat_file_open_error(file);
    return 0;
  }

  size_t numpts = 0;
  const int bufsize = 2056;
  char buffer[bufsize];
  char *ptr = fgets( buffer, bufsize, pts_file);
  if(ptr != NULL) sscanf(buffer, "%zu", &numpts);

  fclose(pts_file);
  return numpts;
}

void readPoints(mt_vector<mt_real> & xyz, std::string file)
{
  const int bufsize = 2048;
  char buffer[bufsize];
  unsigned long int numpts = 0;
  asciireader reader;
  reader.read_file(file, false);

  if(reader.num_lines() == 0) {
    treat_file_open_error(file);
  } else {
    if(reader.get_line(0, buffer, bufsize))
      sscanf(buffer, "%lu", &numpts);

    xyz.resize(numpts*3);

    PROGRESS<size_t> prg(numpts, "Reading points in text CARP format: ");

    #ifdef OPENMP
    #pragma omp parallel private(buffer)
    #endif
    {
      float pts[3];
      int num_read = 0;
      const int check_intervall = 100;

      #ifdef OPENMP
      #pragma omp for schedule(dynamic, 100)
      #endif
      for(size_t i=0; i<numpts; i++)
      {

        if(reader.get_line(i+1, buffer, bufsize))
        {
          sscanf(buffer, "%f %f %f", pts, pts+1, pts+2);
          xyz[i*3+0] = pts[0]; xyz[i*3+1] = pts[1]; xyz[i*3+2] = pts[2];
          num_read++;
        }

        if(num_read == check_intervall) {
          #ifdef OPENMP
          #pragma omp critical
          #endif
          {
            prg.increment(num_read);
          }
          num_read = 0;
        }
      }
    }

    prg.finish();
  }
}

void readPointsBinary(mt_vector<mt_real> & xyz, std::string file)
{
  mt_bfile pts_file;
  pts_file.open(file.c_str());

  if(pts_file.ok()) {
    unsigned long int numpts;
    char header[BIN_HDR_SIZE]; memset(header, 0, sizeof(header));

    pts_file.read(header, sizeof(char), BIN_HDR_SIZE);
    sscanf(header, "%lu", &numpts);

    xyz.resize(numpts*3);
    mt_real* wp = xyz.data();
    float pts[3];
    PROGRESS<size_t> prg(size_t(numpts), "Reading points in binary CARP format: ");

    for(unsigned long int i=0; i<numpts; i++)
    {
      prg.next();

      pts_file.read(pts, sizeof(float), 3);
      wp[0] = pts[0]; wp[1] = pts[1]; wp[2] = pts[2];
      wp += 3;
    }
    prg.finish();
  }
}

void readPoints_general(mt_vector<mt_real> & xyz, std::string basename)
{
  std::string binname = basename + CARPBIN_PTS_EXT;
  std::string txtname = basename + CARPTXT_PTS_EXT;

  if(file_exists(binname))
    readPointsBinary(xyz, binname);
  else
    readPoints(xyz, txtname);
}

void readUVCPoints(mt_vector<mt_real> & uvc,
                   mt_mask & is_lv,
                   std::string filename)
{
  const int bufsize = 2048;
  char buffer[bufsize];
  unsigned long int numpts = 0;

  asciireader reader;
  reader.read_file(filename);

  if(reader.get_line(0, buffer, bufsize))
    sscanf(buffer, "%lu", &numpts);

  uvc.resize(numpts); uvc.resize(0);
  is_lv.resize(numpts); is_lv.clear();

  PROGRESS<size_t> prg(numpts, "Reading UVC points: ");
  float pts[3], v;
  int num_read = 0;
  const int check_intervall = 100;

  for(size_t i=0; i<numpts; i++)
  {
    if(reader.get_line(i+1, buffer, bufsize))
    {
      // on disc the components are
      //         z, rho, phi, vent,
      // but in memory we store
      //         rho, phi, z
      // and v is saved separately
      sscanf(buffer, "%f %f %f %f", pts, pts+1, pts+2, &v);
      // To be consistent with the Bayer et. al., 2018 paper,
      // LV .. -1.0 and RV .. 1.0

      uvc.push_back(pts[1]);
      uvc.push_back(pts[2]);
      uvc.push_back(pts[0]);

      if(v < 0.0) { // LV
        is_lv.insert(i);
      }

      num_read++;
    }

    if(num_read == check_intervall) {
      prg.increment(num_read);
      num_read = 0;
    }
  }
  prg.finish();
}

void readCardiacCoordinates(mt_vector<mt_real>& ucc,
                            mt_vector<int8_t>& ventricle,
                            const std::string& filename)
{
  const int bufsize = 2048;
  char buffer[bufsize];
  unsigned long int numpts = 0;

  asciireader reader;
  reader.read_file(filename);

  if(reader.get_line(0, buffer, bufsize))
    sscanf(buffer, "%lu", &numpts);

  ucc.resize(3*numpts); 
  ucc.zero();
  ventricle.resize(numpts); 
  ventricle.zero();

  PROGRESS<size_t> prg(numpts, "Reading heart coordinates: ");
  float crd[3], ven;
  int num_read = 0;
  const int check_intervall = 100;

  for (size_t i=0; i<numpts; i++)
  {
    if (reader.get_line(i+1, buffer, bufsize))
    {
      sscanf(buffer, "%f %f %f %f", crd, crd+1, crd+2, &ven);

      if ((-2.5 < ven) && (ven < -1.5))      // LA
        ventricle[i] = -2;
      else if ((-1.5 < ven) && (ven < -0.5)) // LV
        ventricle[i] = -1;
      else if ((0.5 < ven) && (ven < 1.5))   // RV
        ventricle[i] = 1;
      else if ((1.5 < ven) && (ven < 2.5))   // RA
        ventricle[i] = 2;
      else 
        ventricle[i] = 0; 
      
      if ((ventricle[i] == -1) || (ventricle[i] == 1)) {
        // for the UVC:
        //   on disc the components are (z, rho, phi, ven)
        //   but in memory we store     (rho, phi, z)
        //   and v is saved separately
        ucc[3*i+0] = crd[1];
        ucc[3*i+1] = crd[2];
        ucc[3*i+2] = crd[0];
      }
      else if ((ventricle[i] == -2) || (ventricle[i] == 2)) {
        ucc[3*i+0] = crd[0];
        ucc[3*i+1] = crd[1];
        ucc[3*i+2] = crd[2];
      }
      num_read++;
    }

    if (num_read == check_intervall) {
      prg.increment(num_read);
      num_read = 0;
    }
  }
  prg.finish();
}

void writePoints(mt_vector<mt_real> & xyz, std::string file)
{
  FILE* pts_file = fopen(file.c_str(), MT_FOPEN_WRITE);
  if(pts_file == NULL) {
    treat_file_open_error(file);
  } else {
    unsigned long int numpts = xyz.size() / 3;
    fprintf(pts_file, "%lu\n", numpts);

    mt_real* wp = xyz.data();
    float pts[3];
    PROGRESS<size_t> prg(numpts, "Writing points in text CARP format: ");

    for(size_t i=0; i<numpts; i++)
    {
      prg.next();

      pts[0] = wp[0]; pts[1] = wp[1]; pts[2] = wp[2];
      fprintf(pts_file, "%f %f %f\n", pts[0], pts[1], pts[2]);
      wp += 3;
    }
    prg.finish();

    fclose(pts_file);
  }
}

void writePointsBinary(mt_vector<mt_real> & xyz, std::string file)
{
  mt_bfile pts_file;
  pts_file.open_write(file.c_str(), 10*1024*1024);

  if(pts_file.ok()) {
    // we need to write the CARP header
    unsigned long int numpts = xyz.size() / 3;
    char header[BIN_HDR_SIZE]; memset(header, 0, sizeof(header));
    int checksum = 666;

    sprintf(header, "%lu %d %d", numpts, MT_ENDIANNESS, checksum);
    pts_file.write(header, sizeof(char), BIN_HDR_SIZE);

    mt_real* wp = xyz.data();
    float pts[3];
    PROGRESS<size_t> prg(numpts, "Writing points in binary CARP format: ");

    for(unsigned long int i=0; i<numpts; i++)
    {
      prg.next();

      pts[0] = wp[0]; pts[1] = wp[1]; pts[2] = wp[2];
      pts_file.write(pts, sizeof(float), 3);
      wp += 3;
    }
    prg.finish();

    pts_file.clear();
  }
}

int readFibers(mt_vector<mt_fib_t> & lon, size_t numelem, std::string file)
{
  const int bufsize = 2048;
  char buffer[bufsize];
  int numfibres = 0;

  asciireader reader;
  bool exit_on_error = false;
  reader.read_file(file, exit_on_error);

  // we may not have fibers
  if(reader.num_lines() == 0) return 0;

  // the increment we need from fiber number to fibre file line
  int line_inc = 1;

  if(reader.get_line(0, buffer, bufsize)) {
    float pts[6];
    size_t nread = sscanf(buffer, "%f %f %f %f %f %f",
                          pts, pts+1, pts+2, pts+3, pts+4, pts+5);
    switch(nread) {
      case 1: numfibres = pts[0]; break;
      case 3: numfibres = 1; line_inc = 0; break;
      case 6: numfibres = 2; line_inc = 0; break;
      default:
        fprintf(stderr, "%s error parsing first line of %s ! Aborting!\n", __func__,
                file.c_str());
        exit(EXIT_FAILURE);
    }
  }

  lon.resize(numelem*numfibres*3);

  char msgstring[1024];
  sprintf(msgstring, "Reading fibers (%d directions) in text CARP format: ", numfibres);
  PROGRESS<size_t> prg(numelem, msgstring);
  std::string errdeco = std::string(80, '~');
  int global_err = 0;

  #ifdef OPENMP
  #pragma omp parallel private(buffer)
  #endif
  {
    float pts[6];
    bool readerror = false;
    int num_read = 0;
    const int check_intervall = 100;

    #ifdef OPENMP
    #pragma omp for schedule(dynamic, 100)
    #endif
    for(size_t i=0; i<numelem; i++)
    {
      readerror = !reader.get_line(i+line_inc, buffer, bufsize);
      if(!readerror) {
        if(numfibres == 2) {
          sscanf(buffer, "%f %f %f %f %f %f", pts, pts+1, pts+2, pts+3, pts+4, pts+5);
          lon[i*6+0] = pts[0]; lon[i*6+1] = pts[1]; lon[i*6+2] = pts[2];
          lon[i*6+3] = pts[3]; lon[i*6+4] = pts[4]; lon[i*6+5] = pts[5];
        }
        else {
          sscanf(buffer, "%f %f %f", pts, pts+1, pts+2);
          lon[i*3+0] = pts[0]; lon[i*3+1] = pts[1]; lon[i*3+2] = pts[2];
        }
        num_read++;
      }

      if(num_read == check_intervall) {
        #ifdef OPENMP
        #pragma omp critical
        #endif
        {
          global_err += int(readerror);
          prg.increment(num_read);
        }
        num_read = 0;
      }
    }
  }
  prg.finish();

  if (global_err)
  {
    std::cerr << std::endl << errdeco << std::endl;
    std::cerr << "ATTENTION: fibre number does not match element number!" << std::endl;
    std::cerr << errdeco << std::endl;
  }

  return numfibres;
}

int readFibersBinary(mt_vector<mt_fib_t> & lon, size_t numelem, std::string file)
{
  mt_bfile fib_file;

  // for fibers, we know that we can proceed even on read errors,
  // thus we ignore the quit-on-error flag
  bool quit_flag = false;
  std::swap(mt_err_mng.quit_on_error, quit_flag);

  fib_file.open(file.c_str());

  std::swap(mt_err_mng.quit_on_error, quit_flag);

  if(!fib_file.ok())
    return 0;

  int numfibres;
  char header[BIN_HDR_SIZE]; memset(header, 0, sizeof(header));
  fib_file.read(header, sizeof(char), BIN_HDR_SIZE);
  sscanf(header, "%d", &numfibres);

  bool errmsg = false;
  std::string errdeco = std::string(80, '~');
  if(numfibres == 2)
  {
    lon.resize(numelem*6);
    mt_fib_t* wp = lon.data();
    float pts[6];
    PROGRESS<size_t> prg(numelem, "Reading fibers (2 directions) in binary CARP format: ");

    for(size_t i=0; i<numelem; i++)
    {
      prg.next();
      if (fib_file.read(pts, sizeof(float), 6) == 6)
      {
        wp[0] = pts[0]; wp[1] = pts[1]; wp[2] = pts[2];
        wp[3] = pts[3]; wp[4] = pts[4]; wp[5] = pts[5];
        wp += 6;
      } else if (errmsg == false) {
        std::cerr << std::endl << errdeco << std::endl;
        std::cerr << std::string() << "WARNING: only " << i << " / " << numelem << " fibre directions read!" << std::endl;
        std::cerr << errdeco << std::endl;
        errmsg = true;
      }
    }
    prg.finish();
  }
  else {
    lon.resize(numelem*3);
    mt_fib_t* wp = lon.data();
    float pts[3];
    PROGRESS<size_t> prg(numelem, "Reading fibers (1 directions) in binary CARP format: ");

    for(size_t i=0; i<numelem; i++)
    {
      prg.next();

      if (fib_file.read(pts, sizeof(float), 3) == 3)
      {
        wp[0] = pts[0]; wp[1] = pts[1]; wp[2] = pts[2];
        wp += 3;
      } else if (errmsg == false) {
        std::cerr << std::endl << errdeco << std::endl;
        std::cerr << std::string() << "WARNING: only " << i << " / " << numelem << " fibre directions read!" << std::endl;
        std::cerr << errdeco << std::endl;
        errmsg = true;
      }
    }
    prg.finish();
  }

  return numfibres;
}

void readFibers_general(mt_vector<mt_fib_t> & lon, size_t numelem, std::string basename)
{
  std::string binname = basename + CARPBIN_LON_EXT;
  std::string txtname = basename + CARPTXT_LON_EXT;

  if(file_exists(binname))
    readFibersBinary(lon, numelem, binname);
  else
    readFibers(lon, numelem, txtname);
}

void writeFibers(mt_vector<mt_fib_t> & lon, size_t numelem, std::string file)
{
  FILE* _file = fopen(file.c_str(), MT_FOPEN_WRITE);
  if(_file == NULL) {
    treat_file_open_error(file);
  } else {
    int numfibres = (lon.size() / 6) == numelem ? 2 : 1;
    fprintf(_file, "%d\n", numfibres);

    if(numfibres == 2)
    {
      mt_fib_t* wp = lon.data();
      float pts[6];
      PROGRESS<size_t> prg(numelem, "Writing fibers (2 directions) in text CARP format: ");

      for(size_t i=0; i<numelem; i++)
      {
        prg.next();

        pts[0] = wp[0]; pts[1] = wp[1]; pts[2] = wp[2];
        pts[3] = wp[3]; pts[4] = wp[4]; pts[5] = wp[5];
        fprintf(_file, "%f %f %f %f %f %f\n", pts[0], pts[1], pts[2], pts[3], pts[4], pts[5]);
        wp += 6;
      }
      prg.finish();
    }
    else {
      mt_fib_t* wp = lon.data();
      float pts[3];
      PROGRESS<size_t> prg(numelem, "Writing fibers (1 direction) in text CARP format: ");

      for(size_t i=0; i<numelem; i++)
      {
        prg.next();

        pts[0] = wp[0]; pts[1] = wp[1]; pts[2] = wp[2];
        fprintf(_file, "%f %f %f\n", pts[0], pts[1], pts[2]);
        wp += 3;
      }
      prg.finish();
    }

    fclose(_file);
  }
}

void writeFibersBinary(mt_vector<mt_fib_t> & lon, size_t numelem, std::string file)
{
  mt_bfile fib_file;
  fib_file.open_write(file.c_str(), 10*1024*1024);

  if(fib_file.ok()) {
    int numfibres = (lon.size() / 6) == numelem ? 2 : 1;
    // we need to write the CARP header
    char header[BIN_HDR_SIZE]; memset(header, 0, sizeof(header));
    int checksum = 666;

    // first numaxes, second numelems, third endiannes, last checksum
    sprintf(header, "%d %zu %d %d", numfibres, numelem, MT_ENDIANNESS, checksum);
    fib_file.write(header, sizeof(char), BIN_HDR_SIZE);

    if(numfibres == 2)
    {
      mt_fib_t* wp = lon.data();
      float pts[6];
      PROGRESS<size_t> prg(numelem, "Writing fibers (2 directions) in binary CARP format: ");

      for(size_t i=0; i<numelem; i++)
      {
        prg.next();

        pts[0] = wp[0]; pts[1] = wp[1]; pts[2] = wp[2];
        pts[3] = wp[3]; pts[4] = wp[4]; pts[5] = wp[5];
        fib_file.write(&pts, sizeof(float), 6);
        wp += 6;
      }
      prg.finish();
    }
    else {
      mt_fib_t* wp = lon.data();
      float pts[3];
      PROGRESS<size_t> prg(numelem, "Writing fibers (1 direction) in binary CARP format: ");

      for(size_t i=0; i<numelem; i++)
      {
        prg.next();

        pts[0] = wp[0]; pts[1] = wp[1]; pts[2] = wp[2];
        fib_file.write(&pts, sizeof(float), 3);
        wp += 3;
      }
      prg.finish();
    }

    fib_file.clear();
  }
}

void write_surf(const mt_vector<mt_cnt_t> & cnt, const mt_vector<mt_idx_t> & con,
               const std::string filename)
{
  FILE* file = fopen(filename.c_str(), MT_FOPEN_WRITE);
  if(file == NULL) {
    treat_file_open_error(filename);
  } else {
    size_t numsurf = cnt.size();
    fprintf(file, "%zu\n", numsurf);

    for(size_t i=0,k=0; i<numsurf; i++) {
      switch(cnt[i]) {
        case 3:
          fprintf(file, "Tr %ld %ld %ld\n", long(con[k]), long(con[k+1]), long(con[k+2]));
          k += 3;
          break;
        case 4:
          fprintf(file, "Qd %ld %ld %ld %ld\n",
                  long(con[k]), long(con[k+1]), long(con[k+2]), long(con[k+3]));
          k += 4;
          break;
        default: break;
      }
    }
    fclose(file);
  }
}

void read_vtx(mt_vector<mt_idx_t> & nod, std::string filename)
{
  FILE* file = fopen(filename.c_str(), MT_FOPEN_READ);
  if(file == NULL) {
    treat_file_open_error(filename);
  } else {
    const int bufsize = 2048;
    char buffer[bufsize];
    long numpts = 0;

    // Read just the number of points from the header
    char *fp = fgets(buffer, bufsize, file);
    if(fp != NULL) sscanf(buffer, "%ld", &numpts);
    nod.resize(numpts);

    long i = 0, nb;
    // the next line is either "intra", "extra" or already a index number
    fgets(buffer, bufsize, file);
    // if we can parse an index, we store it in nod
    if(sscanf(buffer, "%ld", &nb))
      nod[i++] = nb;

    for(; i<numpts; i++) {
      fp = fgets(buffer, bufsize, file);
      if (fp != NULL) {
        sscanf(buffer, "%ld", &nb);
        nod[i] = nb;
      }
    }

    fclose(file);
  }
}

void read_nbc(nbc_data & nbc, mt_vector<mt_idx_t> & con, std::string filename)
{
  FILE* file = fopen(filename.c_str(), MT_FOPEN_READ);
  if(file == NULL) {
    treat_file_open_error(filename);
  } else {
    const int bufsize = 2048;
    char buffer[bufsize];
    int numface = 0;

    // Read just the number of faces from the header
    char *ptr = fgets(buffer, bufsize, file);
    if(ptr != NULL) sscanf(buffer, "%d", &numface);

    // Allocate structures
    nbc.eidx.resize(numface); nbc.sp_vtx.resize(numface); nbc.tag.resize(numface);
    con.resize(numface * 3);

    // Get data pointers
    long rb[6];

    for(int i=0, k=0; i<numface; i++) {
      ptr = fgets(buffer, bufsize, file);

      if (ptr != NULL) {
        sscanf(buffer, "%ld %ld %ld %ld %ld %ld", rb, rb+1, rb+2, rb+3, rb+4, rb+5);
        con[k] = rb[0], con[k+1] = rb[1], con[k+2] = rb[2];
        nbc.tag[i] = rb[3], nbc.sp_vtx[i] = rb[4], nbc.eidx[i] = rb[5];
      }

      k += 3;
    }
    fclose(file);
  }
}

void write_nbc(const nbc_data & nbc, const mt_vector<mt_idx_t> & con, int npts, int nelem,
               const std::string filename)
{
  FILE* file = fopen(filename.c_str(), MT_FOPEN_WRITE);
  if(file == NULL) {
    treat_file_open_error(filename);
  } else {
    int numsurf = nbc.tag.size();
    fprintf(file, "%d # mesh elast %d %d :\n", numsurf, nelem, npts);

    for(int i=0, k=0; i<numsurf; i++) {
      fprintf(file, "%ld %ld %ld %ld %ld %ld\n", long(con[k]), long(con[k+1]),
              long(con[k+2]), long(nbc.tag[i]), long(nbc.sp_vtx[i]), long(nbc.eidx[i]));
      k += 3;
    }

    fclose(file);
  }
}

void write_mesh_selected(mt_meshdata & mesh,
                         std::string format,
                         std::string basename)
{
  mt_err_mng.reset_error();

  if(basename.size() == 0) {
    fprintf(stderr, "Attempted to write mesh with empty filename! Aborting!\n");
    return;
  }

  // if there are no fibers present we create default (1,0,0) fibers
  if(mesh.lon.size() == 0) {
    fprintf(stderr, "%s warning: No fibers detected. Generating default fibers.\n", __func__);
    mesh.lon.resize(3 * mesh.e2n_cnt.size());
    for(size_t eidx=0; eidx < mesh.e2n_cnt.size(); eidx++) {
      mesh.lon[eidx*3+0] = 1;
      mesh.lon[eidx*3+1] = 0;
      mesh.lon[eidx*3+2] = 0;
    }
  }

  if( (format.size() == 0) || (format.compare(carp_txt_fmt) == 0) )
  {
    std::string filename = basename + CARPTXT_ELEM_EXT;
    writeElements(mesh, filename);
    filename = basename + CARPTXT_PTS_EXT;
    writePoints(mesh.xyz, filename);
    filename = basename + CARPTXT_LON_EXT;
    writeFibers(mesh.lon, mesh.e2n_cnt.size(), filename);
  }
  else if(format.compare(carp_bin_fmt) == 0) {
    std::string filename = basename + CARPBIN_ELEM_EXT;
    writeElementsBinary(mesh, filename);
    filename = basename + CARPBIN_PTS_EXT;
    writePointsBinary(mesh.xyz, filename);
    filename = basename + CARPBIN_LON_EXT;
    writeFibersBinary(mesh.lon, mesh.e2n_cnt.size(), filename);
  }
  else if(format.compare(vtk_fmt) == 0) {
    std::string filename = basename + VTK_EXT;
    writeVTKmesh(mesh, filename);
  }
  else if(format.compare(vtk_bin_fmt) == 0) {
    std::string filename = basename + VTK_EXT;
    writeVTKmesh_binary(mesh, filename);
  }
  else if(format.compare(vtu_fmt) == 0) {
    std::string filename = basename + VTU_EXT;
    writeXMLVTKmesh(mesh, filename);
  }
  else if(format.compare(vtk_polydata_fmt) == 0) {
    std::string filename = basename + VTK_EXT;
    writeVTKPolydataMesh(mesh, filename);
  }
  else if(format.compare(mmg_fmt) == 0) {
    std::string filename = basename + MMG_EXT;
    write_mmg_mesh(mesh, filename);
  }
  else if(format.compare(netgen_fmt) == 0) {
    std::string filename = basename + NETGEN_EXT;
    write_netgen_mesh(mesh, filename);
  }
  else if(format.compare(obj_fmt) == 0) {
    std::string filename = basename + OBJ_EXT;
    write_obj_file(mesh, filename);
  }
  else if(format.compare(stl_fmt) == 0) {
    std::string filename = basename + STL_EXT;
    write_stl_binary(filename, mesh);
  }
  else if(format.compare(off_fmt) == 0) {
    std::string filename = basename + OFF_EXT;
    write_off_file(mesh, filename);
  }
  else if(format.compare(stellar_fmt) == 0) {
    MT_USET<mt_idx_t> surf_vtx;
    mt_meshdata trisurf;

    std::cout << "Computing mesh surface for stellar format .." << std::endl;
    compute_surface(mesh, trisurf, false);
    surf_vtx.insert(trisurf.e2n_con.begin(), trisurf.e2n_con.end());

    write_stellar_mesh(mesh, surf_vtx, basename);
  }
  else if(format.compare(vcflow_fmt) == 0) {
    std::string filename = basename + VCFLOW_ELEM_EXT;
    write_vc_flow_elements(mesh, filename);
    filename = basename + VCFLOW_PTS_EXT;
    write_vc_flow_points(mesh.xyz, filename);
    filename = basename + VCFLOW_ADJ_EXT;
    write_vc_flow_adjacency(mesh, filename);
  }
  else if(format.compare(ens_txt_fmt) == 0 || format.compare(ens_bin_fmt) == 0) {
    std::string filename = basename + ENSIGHT_MESH_EXT;
    std::string casefile = basename + ENSIGHT_CASE_EXT;

    bool write_binary = format.compare(ens_bin_fmt) == 0;
    ens_typeinfo ti;
    get_ensight_type_info(mesh, ti);
    write_ensight_mesh(mesh, ti, write_binary, filename);
    write_ensight_case_header(casefile, filename,
                              mesh.lon.size() == mesh.e2n_cnt.size()*6);
  }
  else {
    std::cerr << "Mesh output error: Unrecognized output format. Aborting!" << std::endl;
    return;
  }
}

void read_mesh_selected(mt_meshdata & mesh,
                        std::string format,
                        std::string basename,
                        short readmask)
{
  mt_err_mng.reset_error();

  if(format.size() == 0)
  {
    if( readmask & CRP_READ_ELEM ) readElements_general(mesh, basename);
    if( readmask & CRP_READ_PTS  ) readPoints_general(mesh.xyz, basename);
    if( readmask & CRP_READ_LON  ) readFibers_general(mesh.lon, mesh.e2n_cnt.size(), basename);
  }
  else if(format.compare(carp_txt_fmt) == 0)
  {
    if( readmask & CRP_READ_ELEM ) readElements(mesh, basename + CARPTXT_ELEM_EXT);
    if( readmask & CRP_READ_PTS  ) readPoints(mesh.xyz, basename + CARPTXT_PTS_EXT);
    if( readmask & CRP_READ_LON  ) readFibers(mesh.lon, mesh.e2n_cnt.size(), basename + CARPTXT_LON_EXT);
  }
  else if(format.compare(carp_bin_fmt) == 0) {
    if( readmask & CRP_READ_ELEM ) readElementsBinary(mesh, basename + CARPBIN_ELEM_EXT);
    if( readmask & CRP_READ_PTS  ) readPointsBinary(mesh.xyz, basename + CARPBIN_PTS_EXT);
    if( readmask & CRP_READ_LON  ) readFibersBinary(mesh.lon, mesh.e2n_cnt.size(), basename + CARPBIN_LON_EXT);
  }
  else if( (format.compare(vtk_fmt) == 0) || (format.compare(vtk_bin_fmt) == 0) ) {
    readVTKmesh(mesh, basename + VTK_EXT);
  }
  else if( (format.compare(vtu_fmt) == 0) ) {
    readXMLVTKmesh(mesh, basename + VTU_EXT);
  }
  else if(format.compare(mmg_fmt) == 0) {
    read_mmg_mesh(mesh, basename + MMG_EXT);
  }
  else if(format.compare(netgen_fmt) == 0) {
    read_netgen_mesh(mesh, basename + NETGEN_EXT);
  }
  else if(format.compare(stellar_fmt) == 0) {
    MT_USET<mt_idx_t> surf_vtx;
    read_stellar_mesh(mesh, surf_vtx, basename);
  }
  else if(format.compare(obj_fmt) == 0) {
    mt_vector<mt_idx_t> nrml_idx;
    mt_vector<mt_real> nrml_xyz;
    read_obj_file(mesh, nrml_idx, nrml_xyz, basename + OBJ_EXT);
  }
  else if(format.compare(off_fmt) == 0) {
    read_off_file(mesh, basename + OFF_EXT);
  }
  else if(format.compare(stl_fmt) == 0) {
    std::string filename = basename + STL_EXT;
    if(is_stl_binary(filename)) {
      read_stl_binary(filename, mesh);
    } else {
      std::cerr << "We can currently only read binary STL. Aborting!" << std::endl;
    }
  }
  else if(format.compare(gmsh_fmt) == 0) {
    read_msh_file(mesh, basename + GMSH_EXT);
  }
  else if(format.compare(bw_fmt) == 0) {
    read_bw_trimesh(mesh, basename + MMG_EXT);
  }
  else if(format.compare(purk_fmt) == 0) {
    mt_psdata ps;
    read_purkinje(ps, basename + PURK_EXT);
    psdata_to_mesh(ps, mesh);
  }
  else if(format.compare(vcflow_fmt) == 0) {
    read_vc_flow_elements(mesh, basename + VCFLOW_ELEM_EXT);
    read_vc_flow_points(mesh.xyz, basename + VCFLOW_PTS_EXT);
  }
  else {
    std::cerr << "Mesh input error: Unrecognized input format. Aborting!" << std::endl;
    exit(1);
  }

  // if there are no fibers present we create default (1,0,0) fibers
  if(mesh.lon.size() == 0 && (readmask & CRP_READ_LON) ) {
    fprintf(stderr, "%s warning: No fibers detected. Generating default fibers.\n", __func__);
    mesh.lon.resize(3 * mesh.e2n_cnt.size());
    for(size_t eidx=0; eidx < mesh.e2n_cnt.size(); eidx++) {
      mesh.lon[eidx*3+0] = 1;
      mesh.lon[eidx*3+1] = 0;
      mesh.lon[eidx*3+2] = 0;
    }
  }

  generate_type_reg(mesh);
}

void read_purkinje(mt_psdata & ps, std::string file)
{
  FILE* psfile = fopen(file.c_str(), MT_FOPEN_READ);
  if(psfile == NULL) {
    treat_file_open_error(file);
    return;
  }

  const int bufsize = 2048;
  char buffer[bufsize];
  long unsigned int rcidx, npt;
  double pt[3];
  int    n[2];

  unsigned long int numcab = 0;
  char *ptr = fgets( buffer, bufsize, psfile);
  if(ptr != NULL) sscanf(buffer, "%lu", &numcab);

  ps.cables.resize(numcab);

  for(size_t cidx = 0; cidx < numcab; cidx++)
  {
    mt_pscable & ccab = ps.cables[cidx];

    // read cable index
    ptr = fgets( buffer, bufsize, psfile);
    if(ptr != NULL) {
      sscanf(buffer, "Cable %lu", &rcidx);
    } else {
      break;
    }


    if(rcidx != cidx) {
      fprintf(stderr, "Error: Wrong PS cable order!\n");
      return;
    }

    // read parents
    ptr = fgets( buffer, bufsize, psfile);
    if(ptr != NULL) sscanf(buffer, "%d %d", n, n+1);
    ccab.par1 = n[0], ccab.par2 = n[1];
    // read branches
    ptr = fgets( buffer, bufsize, psfile);
    if(ptr != NULL) sscanf(buffer, "%d %d", n, n+1);
    ccab.br1 = n[0], ccab.br2 = n[1];

    // read number of points
    ptr = fgets( buffer, bufsize, psfile);
    if(ptr != NULL) sscanf(buffer, "%lu", &npt);
    ccab.pts.resize(npt*3);
    // read fiber data
    if(fgets( buffer, bufsize, psfile) != NULL) sscanf(buffer, "%lf", pt+0);
    if(fgets( buffer, bufsize, psfile) != NULL) sscanf(buffer, "%lf", pt+1);
    if(fgets( buffer, bufsize, psfile) != NULL) sscanf(buffer, "%lf", pt+2);
    ccab.size = pt[0], ccab.rgj = pt[1], ccab.cond = pt[2];
    // read points
    for(size_t i=0; i<npt; i++) {
      ptr = fgets( buffer, bufsize, psfile);
      if(ptr != NULL) sscanf(buffer, "%lf %lf %lf", pt, pt+1, pt+2);
      ccab.pts[i*3+0] = pt[0];
      ccab.pts[i*3+1] = pt[1];
      ccab.pts[i*3+2] = pt[2];
    }
  }
  fclose(psfile);
}

void write_purkinje(mt_psdata & ps, std::string file)
{
  FILE* psfile = fopen(file.c_str(), MT_FOPEN_WRITE);
  if(psfile == NULL) {
    treat_file_open_error(file);
  } else {
    float pt[3];

    unsigned long int numcab = ps.cables.size();
    fprintf(psfile, "%lu\n", numcab);

    for(size_t cidx = 0; cidx < numcab; cidx++)
    {
      mt_pscable & ccab = ps.cables[cidx];
      long unsigned int npt = ccab.pts.size()/3;

      // write cable index
      fprintf(psfile, "Cable %lu\n", (long unsigned int)cidx);
      // write parents
      fprintf(psfile, "%ld %ld\n", long(ccab.par1), long(ccab.par2));
      // write branches
      fprintf(psfile, "%ld %ld\n", long(ccab.br1), long(ccab.br2));
      // write number of points
      fprintf(psfile, "%lu\n", long(npt));

      // write fiber data
      fprintf(psfile, "%f\n", ccab.size);
      fprintf(psfile, "%f\n", ccab.rgj);
      fprintf(psfile, "%f\n", ccab.cond);
      // write points
      for(size_t i=0; i<npt; i++) {
        pt[0] = ccab.pts[i*3+0];
        pt[1] = ccab.pts[i*3+1];
        pt[2] = ccab.pts[i*3+2];
        fprintf(psfile, "%f %f %f\n", pt[0], pt[1], pt[2]);
      }
    }
    fclose(psfile);
  }
}

inline bool is_carp_txt_format(std::string & fname, size_t & ext_pos)
{
  return (find_extension(fname, CARPTXT_ELEM_EXT, ext_pos) ||
          find_extension(fname, CARPTXT_PTS_EXT, ext_pos)  ||
          find_extension(fname, CARPTXT_LON_EXT, ext_pos));
}

inline bool is_carp_bin_format(std::string & fname, size_t & ext_pos)
{
  return (find_extension(fname, CARPBIN_ELEM_EXT, ext_pos) ||
          find_extension(fname, CARPBIN_PTS_EXT, ext_pos)  ||
          find_extension(fname, CARPBIN_LON_EXT, ext_pos));
}

mt_filename::mt_filename(std::string fname, std::string fmt)
{
  this->assign(fname, fmt);
}

void mt_filename::assign(std::string fname, std::string fmt)
{
  std::string ext_format = "";
  size_t len = 0;

  fixBasename(fname);

  if(find_extension(fname, VTK_EXT, len))
    ext_format = vtk_bin_fmt;
  else if(is_carp_txt_format(fname, len))
    ext_format = carp_txt_fmt;
  else if(is_carp_bin_format(fname, len))
    ext_format = carp_bin_fmt;
  else if(find_extension(fname, VTU_EXT, len))
    ext_format = vtu_fmt;
  else if(find_extension(fname, MMG_EXT, len))
    ext_format = mmg_fmt;
  else if(find_extension(fname, OBJ_EXT, len))
    ext_format = obj_fmt;
  else if(find_extension(fname, STL_EXT, len))
    ext_format = stl_fmt;
  else if(find_extension(fname, OFF_EXT, len))
    ext_format = off_fmt;
  else if(find_extension(fname, GMSH_EXT, len))
    ext_format = gmsh_fmt;
  else if(find_extension(fname, PURK_EXT, len))
    ext_format = purk_fmt;
  else if(find_extension(fname, ENSIGHT_CASE_EXT, len) ||
          find_extension(fname, ENSIGHT_MESH_EXT, len))
    ext_format = ens_bin_fmt;

  // if user specifies a format, we will use that format
  if(fmt.size() > 0) {
    format = fmt;

    // we check if user-specified format matches our extension, except for VTK where it is non-unique
    if(ext_format.size() && ext_format != vtk_bin_fmt && ext_format != fmt) {
      fprintf(stderr, "mt_filename::assign() warning: user-specified format (%s) does not match extension of file (%s)\n",
              fmt.c_str(), fname.c_str());
    }
  } else {
    if(ext_format.size())
      format = ext_format;
    else {
      // the user hasnt provided a format, and also we haven't found a format based on the file extension.
      // as a last try, we will check if we have a CARP_TXT or CARP_BIN file

      auto check_for_file = [](std::string & base, std::string ext) {
        return file_exists(base + ext);
      };

      bool have_txt_file = check_for_file(fname, CARPTXT_ELEM_EXT) || check_for_file(fname, CARPTXT_LON_EXT) ||
                           check_for_file(fname, CARPTXT_PTS_EXT);
      bool have_bin_file = check_for_file(fname, CARPBIN_ELEM_EXT) || check_for_file(fname, CARPBIN_LON_EXT) ||
                           check_for_file(fname, CARPBIN_PTS_EXT);

      if(have_bin_file) {
        format = carp_bin_fmt;
      } else if (have_txt_file) {
        format = carp_txt_fmt;
      }
    }
  }

  // we trim the extension from the filename
  if(ext_format.size())
    fname.resize(len);

  base = fname;
}

bool mt_filename::isSet() const
{
  return base.size();
}

bool mt_filename::exists() const
{
  return this->read_path() != NULL;
}

const char* mt_filename::write_path() const
{
  static char buff[1024];

  assert(this->isSet() && "file must be set up");

  if(format == vtk_bin_fmt || format == vtk_fmt) {
    sprintf(buff, "%s%s", base.c_str(), VTK_EXT);
  }
  else if(format == carp_txt_fmt) {
    sprintf(buff, "%s%s", base.c_str(), CARPTXT_ELEM_EXT);
  }
  else if(format == carp_bin_fmt) {
    sprintf(buff, "%s%s", base.c_str(), CARPBIN_ELEM_EXT);
  }
  else if(format == vtu_fmt) {
    sprintf(buff, "%s%s", base.c_str(), VTU_EXT);
  }
  else if(format == mmg_fmt) {
    sprintf(buff, "%s%s", base.c_str(), MMG_EXT);
  }
  else if(format == obj_fmt) {
    sprintf(buff, "%s%s", base.c_str(), OBJ_EXT);
  }
  else if(format == stl_fmt) {
    sprintf(buff, "%s%s", base.c_str(), STL_EXT);
  }
  else if(format == off_fmt) {
    sprintf(buff, "%s%s", base.c_str(), OFF_EXT);
  }
  else if(format == gmsh_fmt) {
    sprintf(buff, "%s%s", base.c_str(), GMSH_EXT);
  }
  else if(format == purk_fmt) {
    sprintf(buff, "%s%s", base.c_str(), PURK_EXT);
  }
  else {
    sprintf(buff, "%s", base.c_str());
  }

  return buff;
}

const char* mt_filename::read_path() const
{
  const char* ptr = this->write_path();

  if(!file_exists(ptr))
    return NULL;

  return ptr;
}

void parse_xml_line(std::string line, MT_MAP<std::string, std::string> & fields)
{
  fields.clear();
  remove_indent(line);
  remove_in_place(line, '"');
  remove_in_place(line, '\n');

  mt_vector<std::string> words;
  split_string(line, ' ', words);
  int numwords = words.size();

  if(numwords == 1 && words[0][0] == '<') {
    remove_in_place(words[0], '<');
    remove_in_place(words[0], '>');
    fields["tag"] = words[0];
  }
  else if(numwords > 1) {
    remove_in_place(words[0], '<');
    fields["tag"] = words[0];

    for(int i=1; i<numwords-1; i++) {
      mt_vector<std::string> kvpair;
      split_string(words[i], '=', kvpair);
      if(kvpair.size() == 2)
        fields[kvpair[0]] = kvpair[1];
    }

    if(words[numwords-1].size() != 2)
    {
      remove_in_place(words[numwords-1], '>');
      mt_vector<std::string> kvpair;
      split_string(words[numwords-1], '=', kvpair);
      if(kvpair.size() == 2)
        fields[kvpair[0]] = kvpair[1];
    }
  }
  else
    fields["tag"] = "NONE";
}

void seek_over_char(FILE* fd, const char c)
{
  char buff;
  do {
    fread(&buff, 1, 1, fd);
  } while(buff == c);
  fseek(fd, -1, SEEK_CUR);
}

void setup_data_format(std::string inp_dat, short & data_idx, igb_header & inp_header)
{
  // data_idx may be:
  // 0 : scalar dat file
  // 1 : scalar igb file
  // 2 : vec file
  // 3 : vec3 igb file
  // 4 : vec9 igb file
  // 5 : nodal adjustment file
  // 6 : text tags file
  // 7 : binary tags file
  // 8 : lon file
  // 9 : binary lon file
  //10 : UCC file 

  data_idx = -1;

  if(endswith(inp_dat, DAT_EXT) || endswith(inp_dat, BDAT_EXT))
    data_idx = 0;
  else if(endswith(inp_dat, VEC_EXT))
    data_idx = 2;
  else if( endswith(inp_dat, IGB_EXT) || endswith(inp_dat, DYNPT_EXT) )
  {
    // setup input igb header
    init_igb_header(inp_dat, inp_header);
    read_igb_header(inp_header);

    switch(Num_Components[inp_header.v_type]) {
      case 1: data_idx = 1; break;
      case 3: data_idx = 3; break;
      case 9: data_idx = 4; break;
     default: break;
    }
  }
  else if(endswith(inp_dat, ADJ_EXT)) {
    data_idx = 5;
  }
  else if(endswith(inp_dat, TXT_TAGS_EXT))
    data_idx = 6;
  else if(endswith(inp_dat, BIN_TAGS_EXT))
    data_idx = 7;
  else if(endswith(inp_dat, CARPTXT_LON_EXT))
    data_idx = 8;
  else if(endswith(inp_dat, CARPBIN_LON_EXT))
    data_idx = 9;
  else if(endswith(inp_dat, UCC_EXT))
    data_idx = 10;

  if(data_idx == -1) {
    fprintf(stderr, "%s error: Input data type could not be identified!\n", __func__);
  }
}

// defines used in the get_face_comp function
#define COMP_HAVE_NO_SLASHES 0
#define COMP_HAVE_VERT 1
#define COMP_HAVE_TEXT 2
#define COMP_HAVE_NORM 4

// helper function to determine which components of a obj-format face definition are set
int get_face_comp(const char* line)
{
  mt_vector<std::string> words;
  split_string(line, ' ', words);

  mt_vector<std::string> comp;
  split_string(words[1], '/', comp, true);

  int ret = COMP_HAVE_NO_SLASHES;

  if(comp.size() > 1) {
    if(comp[0].size()) ret |= COMP_HAVE_VERT;
    if(comp[1].size()) ret |= COMP_HAVE_TEXT;
    if(comp.size() > 2 && comp[2].size()) ret |= COMP_HAVE_NORM;
  }

  return ret;
}

void read_obj_file(mt_meshdata & mesh, mt_vector<mt_idx_t> & nrml_idx,
                   mt_vector<mt_real> & nrml_xyz, std::string file)
{
  int current_material = 0;
  const int linelength = 2048;
  char current_line[linelength];
  int line_number = 0;

  // open scene
  FILE* fd = fopen( file.c_str(), MT_FOPEN_READ);
  if(fd == NULL) {
    treat_file_open_error(file);
    return;
  }

  size_t filesz = file_size(fd);
  size_t numread = filesz / 50; // 50 bytes per line
  PROGRESS<size_t> prg(numread, "Reading wavefront obj: ");

  size_t vtx_reserve = 1000, elems_reserve = 5000;
  mesh.xyz.resize(0); mesh.xyz.reserve(vtx_reserve*3);
  nrml_xyz.resize(0); nrml_xyz.reserve(vtx_reserve*3);
  nrml_idx.resize(0); nrml_idx.reserve(elems_reserve*3);
  mesh.e2n_cnt.resize(0); mesh.e2n_cnt.reserve(elems_reserve);
  mesh.e2n_con.resize(0); mesh.e2n_con.reserve(elems_reserve*3);
  mesh.etags.resize(0); mesh.etags.reserve(elems_reserve);
  mesh.etype.resize(0); mesh.etype.reserve(elems_reserve);

  mt_tag_t curr_vert_reg = 0;
  MT_MAP<mt_triple<int>, mt_tag_t> vert_reg_map;
  mt_vector<mt_tag_t> vert_reg; vert_reg.reserve(vtx_reserve);

  auto update_vert_reg = [&](float r1, float r2, float r3)
  {
    mt_triple<int> reg_key = {(int)(r1*100),(int)(r2*100),(int)(r3*100)};
    if(vert_reg_map.count(reg_key)) {
      mt_tag_t insert_tag = vert_reg_map[reg_key];
      vert_reg.push_back(insert_tag);
      // printf("existing key (%d,%d,%d) -> %d\n", reg_key.v1, reg_key.v2, reg_key.v3, insert_tag);
    }
    else {
      // printf("new key (%d,%d,%d) <- %d\n", reg_key.v1, reg_key.v2, reg_key.v3, curr_vert_reg);
      vert_reg_map[reg_key] = curr_vert_reg;
      vert_reg.push_back(curr_vert_reg);
      curr_vert_reg++;
    }
  };

  int comp_flags = -1;

  //parser loop
  while( fgets(current_line, linelength, fd) )
  {
    line_number++;

    prg.next();

    //skip comments
    if(current_line[0] == '#') continue;

    // increment material index for every usemtl
    else if(current_line[0] == 'u' && current_line[1] == 's' && current_line[2] == 'e')
      current_material++;

    // increment material index for every usemtl
    else if(current_line[0] == 'g' && current_line[1] == ' ')
      current_material++;

    //parse objects
    else if(current_line[0] == 'v' && current_line[1] == ' ') //process vertex
    {
      float e1, e2, e3, e4, e5, e6;
      int numfound = sscanf(current_line, "v %f %f %f %f %f %f", &e1, &e2, &e3, &e4, &e5, &e6);

      switch(numfound) {
        case 3:
          mesh.xyz.push_back(e1);
          mesh.xyz.push_back(e2);
          mesh.xyz.push_back(e3);
          break;

        case 6:
          mesh.xyz.push_back(e1);
          mesh.xyz.push_back(e2);
          mesh.xyz.push_back(e3);
          update_vert_reg(e4, e5, e6);
          break;

        default: break;
      }
    }

    else if(current_line[0] == 'v' && current_line[1] == 'n') //process vertex normal
    {
      float e1, e2, e3;
      sscanf(current_line, "vn %f %f %f", &e1, &e2, &e3);
      nrml_xyz.push_back(e1);
      nrml_xyz.push_back(e2);
      nrml_xyz.push_back(e3);
    }

    else if(current_line[0] == 'f') //process face
    {
      int v0 = 0, t0 = 0, n0 = 0, v1 = 0, t1 = 0, n1 = 0, v2 = 0, t2 = 0, n2 = 0, v3 = 0, t3 = 0, n3 = 0;

      // we dont want to determine components in every line, they need to stay constant
      if(comp_flags == -1)
        comp_flags = get_face_comp(current_line);

      int num_read = 0;
      bool read_quad = false;

      switch(comp_flags) {
        case COMP_HAVE_NO_SLASHES:
          num_read = sscanf(current_line, "f %d %d %d %d", &v0, &v1, &v2, &v3);
          read_quad = num_read == 4;
          break;
        case COMP_HAVE_VERT:
          num_read = sscanf(current_line, "f %d// %d// %d// %d//", &v0, &v1, &v2, &v3);
          if(num_read < 3)
            num_read = sscanf(current_line, "f %d/ %d/ %d/ %d/", &v0, &v1, &v2, &v3);

          if(num_read < 3) {
            fprintf(stderr, "%s error: could not parse face at line %d! Aborting!\n", __func__, line_number);
            return;
          }
          read_quad = num_read == 4;
          break;
        case COMP_HAVE_VERT | COMP_HAVE_NORM:
          num_read = sscanf(current_line, "f %d//%d %d//%d %d//%d %d//%d", &v0, &n0, &v1, &n1, &v2, &n2, &v3, &n3);
          read_quad = num_read == 8;
          break;
        case COMP_HAVE_VERT | COMP_HAVE_TEXT:
          num_read = sscanf(current_line, "f %d/%d/ %d/%d/ %d/%d/ %d/%d/", &v0, &t0, &v1, &t1, &v2, &t2, &v3, &t3);
          read_quad = num_read == 8;
          break;
        case COMP_HAVE_VERT | COMP_HAVE_NORM | COMP_HAVE_TEXT:
          num_read = sscanf(current_line, "f %d/%d/%d %d/%d/%d %d/%d/%d %d/%d/%d",
                            &v0, &t0, &n0, &v1, &t1, &n1, &v2, &t2, &n2, &v3, &t3, &n3);
          read_quad = num_read == 12;
          break;
        default:
          fprintf(stderr, "Error: Cannot parse face definition in line %d! Aborting!\n", line_number);
          return;
      }

      mesh.e2n_cnt.push_back(3);
      mesh.etags.push_back(current_material);
      mesh.etype.push_back(Tri);

      mesh.e2n_con.push_back(v0-1);
      mesh.e2n_con.push_back(v1-1);
      mesh.e2n_con.push_back(v2-1);

      nrml_idx.push_back(n0-1);
      nrml_idx.push_back(n1-1);
      nrml_idx.push_back(n2-1);

      if(read_quad) {
        mesh.e2n_cnt.push_back(3);
        mesh.etags.push_back(current_material);
        mesh.etype.push_back(Tri);

        mesh.e2n_con.push_back(v0-1);
        mesh.e2n_con.push_back(v2-1);
        mesh.e2n_con.push_back(v3-1);

        nrml_idx.push_back(n0-1);
        nrml_idx.push_back(n2-1);
        nrml_idx.push_back(n3-1);
      }
    }
  }

  fclose(fd);
  prg.finish();

  if(vert_reg.size() == mesh.xyz.size()/3) {
    printf("\n%s: vertex data provided. converting vertex-data into regions..\n", __func__);
    compute_full_mesh_connectivity(mesh);
    nodeData_to_elemData(mesh, vert_reg, mesh.etags);
  }

  generate_default_fib(mesh.e2n_cnt.size(), mesh.lon);
}

void write_obj_file(mt_meshdata & mesh, std::string filename, bool continuous_normals)
{
  if(mesh.etype[0] != Tri) {
    fprintf(stderr, "Error: Wavefront obj files support only triangle elements! Aborting!\n");
    exit(1);
  }

  FILE* fd = fopen(filename.c_str(), MT_FOPEN_WRITE);
  if(!fd) {
    treat_file_open_error(filename);
  } else {
    size_t nnod = mesh.xyz.size() / 3;
    size_t nele = mesh.e2n_cnt.size();

    mt_vector<vec3r> enrml(nele), nnrml(nnod);
    for(size_t eidx = 0; eidx < nele; eidx++)
    {
      mt_idx_t v1 = mesh.e2n_con[eidx*3+0];
      mt_idx_t v2 = mesh.e2n_con[eidx*3+1];
      mt_idx_t v3 = mesh.e2n_con[eidx*3+2];

      vec3r e1 = vec3r(mesh.xyz.data() + v2*3) - vec3r(mesh.xyz.data() + v1*3);
      vec3r e2 = vec3r(mesh.xyz.data() + v3*3) - vec3r(mesh.xyz.data() + v1*3);
      vec3r n  = unit_vector(e1.crossProd(e2));
      enrml[eidx] = n;
    }

    if(continuous_normals) {
      if(mesh.n2e_con.size() == 0) compute_full_mesh_connectivity(mesh, false);
      elemData_to_nodeData(mesh, enrml, nnrml);
    }

    MT_MAP<mt_tag_t,mt_vector<mt_idx_t>> tag2eidx;
    for(size_t i=0; i<nele; i++)
      tag2eidx[mesh.etags[i]].push_back(mt_idx_t(i));

    tag2eidx.sort();

    fprintf(fd, "# Wavefront OBJ file\n");
    fprintf(fd, "# Generated by meshtool\n");
    fprintf(fd, "\n");

    for(size_t i=0; i<nnod; i++)
      fprintf(fd, "v %f %f %f\n",
              float(mesh.xyz[i*3+0]), float(mesh.xyz[i*3+1]), float(mesh.xyz[i*3+2]));
    fprintf(fd, "\n");

    if(continuous_normals) {
      for(size_t i=0; i<nnod; i++)
        fprintf(fd, "vn %f %f %f\n", float(nnrml[i].x), float(nnrml[i].y), float(nnrml[i].z));
      fprintf(fd, "\n");
    } else {
      for(size_t i=0; i<nele; i++)
        fprintf(fd, "vn %f %f %f\n", float(enrml[i].x), float(enrml[i].y), float(enrml[i].z));
      fprintf(fd, "\n");
    }

    for(auto & t : tag2eidx) {
      fprintf(fd, "g __TAG_%d__\n", int(t.first));
      mt_vector<mt_idx_t> & elems = t.second;
      if(continuous_normals) {
        for(size_t i=0; i<elems.size(); i++) {
          mt_idx_t e = elems[i];
          fprintf(fd, "f %d//%d %d//%d %d//%d\n",
                  int(mesh.e2n_con[e*3+0]+1), int(mesh.e2n_con[e*3+0]+1), int(mesh.e2n_con[e*3+1]+1), int(mesh.e2n_con[e*3+1]+1), int(mesh.e2n_con[e*3+2]+1), int(mesh.e2n_con[e*3+2]+1));
        }
      } else {
        for(size_t i=0; i<elems.size(); i++) {
          mt_idx_t e = elems[i];
          fprintf(fd, "f %d//%d %d//%d %d//%d\n",
                  int(mesh.e2n_con[e*3+0]+1), int(e+1), int(mesh.e2n_con[e*3+1]+1), int(e+1),
                  int(mesh.e2n_con[e*3+2]+1), int(e+1));
        }
      }
    }
    fprintf(fd, "\n");
    fclose(fd);
  }
}

double timediff_sec(struct timeval & t1, struct timeval & t2)
{
  return ((t2.tv_sec - t1.tv_sec)*1e6 + (t2.tv_usec - t1.tv_usec))/1e6;
}

// we can bump the fcon version when the format / datatypes change. this will trigger a
// re-generation of the .fcon files
#define FCON_VERS 2

void write_full_mesh_connectivity(const mt_meshdata & mesh, const std::string filename)
{
  std::cout << "Writing full mesh connectivity to: " << filename << std::endl;

  FILE* fd = fopen(filename.c_str(), MT_FOPEN_WRITE);
  if(fd == NULL) {
    treat_file_open_error(filename);
  } else {
    int32_t fcon_vers = FCON_VERS;
    fwrite(&fcon_vers, sizeof(int32_t), 1, fd);

    mesh.n2e_cnt.write(fd);
    mesh.n2e_con.write(fd);
    mesh.n2n_cnt.write(fd);
    mesh.n2n_con.write(fd);

    fclose(fd);
  }
}

void read_full_mesh_connectivity(mt_meshdata & mesh, const std::string filename)
{
  std::cout << "Reading full mesh connectivity from: " << filename << std::endl;

  FILE* fd = fopen(filename.c_str(), MT_FOPEN_READ);
  if(fd == NULL) {
    treat_file_open_error(filename);
  }
  else {
    int32_t fcon_vers;
    fread(&fcon_vers, sizeof(int32_t), 1, fd);

    if(fcon_vers == FCON_VERS) {
      mesh.n2e_cnt.read(fd);
      mesh.n2e_con.read(fd);
      mesh.n2n_cnt.read(fd);
      mesh.n2n_con.read(fd);
      fclose(fd);

      bucket_sort_offset(mesh.e2n_cnt, mesh.e2n_dsp);
      bucket_sort_offset(mesh.n2e_cnt, mesh.n2e_dsp);
      bucket_sort_offset(mesh.n2n_cnt, mesh.n2n_dsp);
    } else {
      fclose(fd);
      fprintf(stderr, "%s warning: fcon version not matching, re-computing connectivity.\n", __func__);
      compute_full_mesh_connectivity(mesh, false);
      write_full_mesh_connectivity(mesh, filename);
    }
  }
}

void write_split_file(mt_vector<split_item> & splitlist, std::string filename)
{
  FILE* fd = fopen(filename.c_str(), MT_FOPEN_WRITE);
  if(!fd) {
    treat_file_open_error(filename);
  } else {
    fprintf(fd, "%lu\n", (unsigned long int)splitlist.size());

    for(auto it = splitlist.begin(); it != splitlist.end(); ++it)
      fprintf(fd, "%d %d %d\n", it->oldIdx, it->elemIdx, it->newIdx);

    fclose(fd);
  }
}

void read_split_file(mt_vector<split_item> & splitlist, std::string filename)
{
  FILE* fd = fopen(filename.c_str(), MT_FOPEN_READ);
  if(!fd) {
    treat_file_open_error(filename);
  } else {
    long int size = 0;
    fscanf(fd, "%lu\n", &size);

    splitlist.resize(size);

    for(size_t i = 0; i < splitlist.size(); ++i)
      fscanf(fd, "%d %d %d\n",
              &splitlist[i].oldIdx, &splitlist[i].elemIdx, &splitlist[i].newIdx);

    fclose(fd);
  }
}

char* read_bin_string(FILE* in)
{
  int len;
  fread(&len, sizeof(int), 1, in);

  char *s = new char[len+1];

  fread(s, sizeof(char), len, in);
  s[len] = '\0';

  return s;
}

void write_bin_string(FILE* out, const char *s)
{
  if(!s) return;

  int len = strlen(s);
  fwrite(&len, sizeof(int), 1, out);
  fwrite(s, sizeof(char), len, out);
}


bool read_EP_state(ep_state & st, std::string file)
{
  FILE* in = fopen(file.c_str(), MT_FOPEN_READ);
  if(!in) {
    treat_file_open_error(file);
    return false;
  }

  const char* MAGIC_MIIF_ID = "Dump_MIIF_ID";

  char* miif_id = read_bin_string(in);
  st.miif_id = miif_id;
  delete [] miif_id;

  if(st.miif_id.compare(MAGIC_MIIF_ID) != 0) {
    fprintf(stderr, "%s error: input file %s does no seem to be a MIIF dump (no ID found).\n", __func__, file.c_str());
    return false;
  }

  fread(&st.format,    sizeof(unsigned int), 1, in);
  fread(&st.version,   sizeof(unsigned int), 1, in);
  fread(&st.save_date, sizeof(time_t),       1, in);
  fread(&st.time,      sizeof(float),        1, in);

  printf("Loading state time %f from %s (format v%d) generated\n\tby calling "
          "program r%d on %s", st.time, file.c_str(), st.format, st.version, ctime(&st.save_date));

  fread(&st.saved_nnodes, sizeof(int), 1, in);

  int num_gdata = 0;
  fread(&num_gdata, sizeof(int), 1, in);

  if(num_gdata < 0 || num_gdata > 100) {
    fprintf(stderr, "%s error: number of global data vectors %d not in legal interval [0,100] ! Aborting!\n", __func__, num_gdata);
    return false;
  }

  st.gdata.resize(num_gdata);
  st.gdata_names.resize(num_gdata);

  for(int g=0; g<num_gdata; g++) {
    char *dataname = read_bin_string(in);
    printf("Loading global data %s ..\n", dataname);
    st.gdata[g].resize(st.saved_nnodes);
    fread(st.gdata[g].data(), sizeof(double), st.saved_nnodes, in);
    st.gdata_names[g] = dataname;
    delete [] dataname;
  }

  int N_IIF;
  fread(&N_IIF, sizeof(int), 1, in);

  st.imps.resize(N_IIF);
  st.imp_sizes.resize(N_IIF);

  for( int i=0; i<N_IIF; i++ ) {
    char* imp_name = read_bin_string(in);
    st.imps[i].name = imp_name;
    delete [] imp_name;

    fread(&st.imps[i].size,  sizeof(int), 1, in);
    fread(&st.imps[i].nplug, sizeof(int), 1, in);

    st.imps[i].offset = 0;
    st.imp_sizes[i] = st.imps[i].size;
    st.imps[i].plug.resize(st.imps[i].nplug);

    for(int j=0; j<st.imps[i].nplug; j++) {
      st.imps[i].plug[j].offset = st.imp_sizes[i];

      imp_name = read_bin_string(in);
      st.imps[i].plug[j].name = imp_name;
      delete [] imp_name;

      fread(&st.imps[i].plug[j].size, sizeof(int), 1, in);
      st.imp_sizes[i] += st.imps[i].plug[j].size;
    }
  }

  // read in region mask
  st.nodal_imp_mask.resize(st.saved_nnodes);
  fread(st.nodal_imp_mask.data(), sizeof(char), st.nodal_imp_mask.size(), in);

  // determine relative IMP data offsets based on canonical ordering
  st.nodal_imp_offset.resize(st.saved_nnodes+1, 0);
  for(int i=1; i<=st.saved_nnodes; i++)
    st.nodal_imp_offset[i] = st.nodal_imp_offset[i-1] + st.imp_sizes[(int)st.nodal_imp_mask[i-1]];

  size_t data_size = st.nodal_imp_offset[st.saved_nnodes];
  st.nodal_imp_data.resize(data_size);

  size_t nread = fread(st.nodal_imp_data.data(), 1, st.nodal_imp_data.size(), in);

  if(nread != st.nodal_imp_data.size()) {
    fprintf(stderr, "%s warning: Could not read nodal IMP data completely!\n", __func__);
  }

  fclose(in);
  return true;
}

bool write_EP_state(const ep_state & st, std::string file)
{
  FILE* out = fopen(file.c_str(), MT_FOPEN_WRITE);
  if(!out) {
    treat_file_open_error(file);
    return false;
  }

  write_bin_string(out, st.miif_id.c_str());
  fwrite(&st.format,    sizeof(unsigned int), 1, out);
  fwrite(&st.version,   sizeof(unsigned int), 1, out);
  fwrite(&st.save_date, sizeof(time_t),       1, out);
  fwrite(&st.time,      sizeof(float),        1, out);
  fwrite(&st.saved_nnodes, sizeof(int), 1, out);

  int num_gdata = st.gdata.size();
  fwrite(&num_gdata, sizeof(int), 1, out);

  for(int g=0; g<num_gdata; g++) {
    write_bin_string(out, st.gdata_names[g].c_str());
    fwrite(st.gdata[g].data(), sizeof(double), st.saved_nnodes, out);
  }

  int N_IIF = st.imps.size();
  fwrite(&N_IIF, sizeof(int), 1, out);

  for(int i=0; i<N_IIF; i++) {
    write_bin_string(out, st.imps[i].name.c_str());
    fwrite(&st.imps[i].size,  sizeof(int), 1, out);
    fwrite(&st.imps[i].nplug, sizeof(int), 1, out);
    for(int j=0; j<st.imps[i].nplug; j++) {
      write_bin_string(out, st.imps[i].plug[j].name.c_str());
      fwrite(&st.imps[i].plug[j].size, sizeof(int), 1, out);
    }
  }

  // write region mask
  fwrite(st.nodal_imp_mask.data(), sizeof(char), st.nodal_imp_mask.size(), out);
  // write imp data
  fwrite(st.nodal_imp_data.data(), 1, st.nodal_imp_data.size(), out);
  fclose(out);
  return true;
}

void read_off_file(mt_meshdata & mesh, std::string filename)
{
  FILE* in = fopen(filename.c_str(), MT_FOPEN_READ);
  if(!in) {
    treat_file_open_error(filename);
    return;
  }

  const int linelength = 2048;
  char current_line[linelength];
  int line_number = 1;
  int num_vtx = -1, num_face = -1, num_edge = -1;

  fgets(current_line, linelength, in);
  if(strcmp(current_line, "OFF\n") != 0) {
    fprintf(stderr, "%s error: File doesnt start with \"OFF\". Wrong format?\n", __func__);
    fclose(in);
    return;
  }

  // skip comments
  do {
    fgets(current_line, linelength, in);
    remove_indent(current_line);
    line_number++;
  } while(current_line[0] == '\n' || current_line[0] == '#');

  // read dimensions
  int nread = sscanf(current_line, "%d %d %d", &num_vtx, &num_face, &num_edge);

  if(nread != 3) {
    fprintf(stderr, "%s error, line %d: Could not read data dimensions \n", __func__, line_number);
    fclose(in);
    return;
  }

  mesh.xyz.resize(num_vtx*3);
  double ptsbuff[3];
  int intbuff[5];

  // read nodes
  for(int i=0; i<num_vtx; i++) {
    fgets(current_line, linelength, in);
    line_number++;

    nread = sscanf(current_line, "%lf %lf %lf", ptsbuff, ptsbuff + 1, ptsbuff + 2);

    mesh.xyz[i*3+0] = ptsbuff[0];
    mesh.xyz[i*3+1] = ptsbuff[1];
    mesh.xyz[i*3+2] = ptsbuff[2];
  }

  mesh.etype.resize(num_face);
  mesh.e2n_cnt.resize(num_face);
  mesh.e2n_con.resize(num_face*4);
  mesh.etags.assign(size_t(num_face), mt_tag_t(0));
  mesh.lon.assign(num_face*3, mt_fib_t(0));
  size_t widx = 0;

  // read faces
  for(int i=0; i<num_face; i++) {
    fgets(current_line, linelength, in);
    line_number++;

    nread = sscanf(current_line, "%d %d %d %d %d",
                   intbuff, intbuff+1, intbuff+2, intbuff+3, intbuff+4);

    if(intbuff[0] > 4) {
      fprintf(stderr, "%s error, line %d: Only elements of size 3 or 4 supported!\n", __func__, line_number);
      fclose(in);
      return;
    }

    // elem size
    mesh.e2n_cnt[i] = intbuff[0];
    // elem type
    mesh.etype[i]   = intbuff[0] == 3 ? Tri : Quad;
    // connectivity
    mesh.e2n_con[widx++] = intbuff[1];
    mesh.e2n_con[widx++] = intbuff[2];
    mesh.e2n_con[widx++] = intbuff[3];

    if(intbuff[0] > 3)
      mesh.e2n_con[widx++] = intbuff[4];
  }
  mesh.e2n_con.resize(widx);

  fclose(in);
}

void write_off_file(const mt_meshdata & mesh, std::string filename)
{
  FILE* out = fopen(filename.c_str(), MT_FOPEN_WRITE);
  if(!out) {
    treat_file_open_error(filename);
    return;
  }

  fprintf(out, "OFF\n");
  size_t num_vtx = mesh.xyz.size() / 3, num_face = mesh.e2n_cnt.size();

  // write dimensions
  fprintf(out, "%zd %zd 0\n", num_vtx, num_face);

  // write nodes
  for(size_t i=0; i<num_vtx; i++) {
    fprintf(out, "%lf %lf %lf\n", double(mesh.xyz[i*3+0]),
            double(mesh.xyz[i*3+1]), double(mesh.xyz[i*3+2]));
  }

  // write faces
  const mt_idx_t* con = mesh.e2n_con.data();
  for(size_t i=0; i<num_face; i++) {
    fprintf(out, "%d ", int(mesh.e2n_cnt[i]));
    for(int j=0; j<mesh.e2n_cnt[i]; j++)
      fprintf(out, "%d ", int(con[j]));
    fprintf(out, "\n");
    con += mesh.e2n_cnt[i];
  }

  fclose(out);
}

void mesh_serialize_base64(const mt_meshdata & mesh, mt_vector<unsigned char> & buff)
{
  int numpts = mesh.xyz.size() / 3, numele = mesh.e2n_cnt.size(), numcon = mesh.e2n_con.size(),
      numfib = mesh.lon.size() / mesh.e2n_cnt.size();

  const size_t buffsize = 4*sizeof(int) + numpts*3*sizeof(float) + numele*sizeof(short) +
                    numcon*sizeof(int) + numele*sizeof(short) + numfib*numele*sizeof(float) + 1;

  mt_vector<char> inp_buff(buffsize);
  char* ptr = inp_buff.data();

  // HEADER
  ((int*)ptr)[0] = numpts;
  ((int*)ptr)[1] = numele;
  ((int*)ptr)[2] = numcon;
  ((int*)ptr)[3] = numfib;

  ptr += 4*sizeof(int);

  // POINTS
  for(int i=0; i<numpts; i++) {
    ((float*)ptr)[i*3+0] = float(mesh.xyz[i*3+0]);
    ((float*)ptr)[i*3+1] = float(mesh.xyz[i*3+1]);
    ((float*)ptr)[i*3+2] = float(mesh.xyz[i*3+2]);
  }

  ptr += numpts*3*sizeof(float);

  // ELEM TYPE
  for(int i=0; i<numele; i++)
    ((short*)ptr)[i] = short(mesh.etype[i]);

  ptr += numele*sizeof(short);

  // CONNECTIVITY
  for(int i=0; i<numcon; i++)
    ((int*)ptr)[i] = int(mesh.e2n_con[i]);

  ptr += numcon*sizeof(int);

  // TAGS
  for(int i=0; i<numele; i++)
    ((short*)ptr)[i] = short(mesh.etags[i]);

  ptr += numele*sizeof(short);

  // FIBERS
  for(int i=0; i<numele; i++)
    for(int j=0; j<numfib; j++)
      ((float*)ptr)[(i*numfib)+j] = float(mesh.lon[(i*numfib)+j]);

  encode_base64(inp_buff, buff);
}

void mesh_deserialize_base64(mt_meshdata & mesh, const mt_vector<unsigned char> & buff)
{
  mt_vector<char> out_buff;
  decode_base64(buff, out_buff);

  int numpts, numele, numcon, numfib;
  const char* ptr = out_buff.data();

  // HEADER
  numpts = ((const int*)ptr)[0];
  numele = ((const int*)ptr)[1];
  numcon = ((const int*)ptr)[2];
  numfib = ((const int*)ptr)[3];

  ptr += 4*sizeof(int);

  // POINTS
  mesh.xyz.resize(numpts*3);
  for(int i=0; i<numpts; i++) {
    mesh.xyz[i*3+0] = ((const float*)ptr)[i*3+0];
    mesh.xyz[i*3+1] = ((const float*)ptr)[i*3+1];
    mesh.xyz[i*3+2] = ((const float*)ptr)[i*3+2];
  }

  ptr += numpts*3*sizeof(float);

  // ELEM TYPE
  mesh.etype.resize(numele);
  mesh.e2n_cnt.resize(numele);
  for(int i=0; i<numele; i++) {
    mesh.etype[i] = elem_t(((short*)ptr)[i]);

    switch(mesh.etype[i]) {
      case Tetra:   mesh.e2n_cnt[i] = 4; break;
      case Hexa:    mesh.e2n_cnt[i] = 8; break;
      case Octa:    mesh.e2n_cnt[i] = 6; break;
      case Pyramid: mesh.e2n_cnt[i] = 5; break;
      case Prism:   mesh.e2n_cnt[i] = 6; break;
      case Quad:    mesh.e2n_cnt[i] = 4; break;
      case Tri:     mesh.e2n_cnt[i] = 3; break;
      case Line:    mesh.e2n_cnt[i] = 2; break;
      case Node:    mesh.e2n_cnt[i] = 1; break;

      default:
        fprintf(stderr, "%s error: Unsupported element type!\n", __func__);
        exit(1);
    }
  }

  ptr += numele*sizeof(short);

  // CONNECTIVITY
  mesh.e2n_con.resize(numcon);
  for(int i=0; i<numcon; i++)
    mesh.e2n_con[i] = ((int*)ptr)[i];

  ptr += numcon*sizeof(int);

  // TAGS
  mesh.etags.resize(numele);
  for(int i=0; i<numele; i++)
    mesh.etags[i] = ((short*)ptr)[i];

  ptr += numele*sizeof(short);

  // FIBERS
  mesh.lon.resize(numele*numfib);
  for(int i=0; i<numele; i++)
    for(int j=0; j<numfib; j++)
      mesh.lon[(i*numfib)+j] = ((float*)ptr)[(i*numfib)+j];

  generate_type_reg(mesh);
}

bool is_stl_binary(std::string file)
{
  mt_bfile fd;
  fd.open(file.c_str());

  if(! fd.ok()) return false;

  // we skip the header
  char header[6];
  fd.read(header, 5, 1); header[5] = '\0';
  std::string solid_check_str = header;

  if(solid_check_str == "solid") return false;
  else                           return true;
}

void read_stl_binary(std::string file, mt_meshdata & mesh)
{
  mt_bfile fd;
  fd.open(file.c_str());

  if(! fd.ok()) return;

  // we skip the header
  char header[80];
  fd.read(header, 80, 1);

  fd.read(header, 4, 1); // number of tri
  uint32_t ntri = *((uint32_t*)header);

  auto read_vertex = [&] (vec3r & v) {
    size_t nread = fd.read(header, 1, 12);

    if(nread == 12) {
      float* ptr = (float*) header;
      v.x = *ptr; ptr++;
      v.y = *ptr; ptr++;
      v.z = *ptr;
    }

    return nread;
  };

  size_t vtx_idx = 0;
  mt_real  avrg_edgelen;

  auto read_tri = [&]() {
    fd.read(header, 1, 12); // skip normal

    vec3r v1, v2, v3;
    size_t nread = 0;
    nread += read_vertex(v1);
    nread += read_vertex(v2);
    nread += read_vertex(v3);

    if(nread == 36) {
      mesh.xyz.push_back(v1.x); mesh.xyz.push_back(v1.y); mesh.xyz.push_back(v1.z);
      mesh.e2n_con.push_back(vtx_idx+2);

      mesh.xyz.push_back(v2.x); mesh.xyz.push_back(v2.y); mesh.xyz.push_back(v2.z);
      mesh.e2n_con.push_back(vtx_idx+1);

      mesh.xyz.push_back(v3.x); mesh.xyz.push_back(v3.y); mesh.xyz.push_back(v3.z);
      mesh.e2n_con.push_back(vtx_idx);
      vtx_idx += 3;

      mt_real e1 = (v2 - v1).length();
      mt_real e2 = (v3 - v1).length();
      mt_real e3 = (v2 - v3).length();

      avrg_edgelen += (e1 + e2 + e3) / 3.0;
      fd.read(header, 1, 2); // skip attribute
      return true;
    } else {
      return false;
    }
  };

  bool success = false;
  size_t tri_read = 0;
  do {
    success = read_tri();
    if(success) tri_read++;
  } while(success && tri_read < ntri);

  if(tri_read) {
    avrg_edgelen /= mt_real(tri_read);

    mesh.e2n_cnt.assign(tri_read, 3);
    mesh.etype  .assign(tri_read, Tri);
    mesh.etags  .assign(tri_read, 1);

    correct_duplicate_vertices(mesh, avrg_edgelen * 1e-3, false);
  }
}

void write_stl_binary(std::string file, const mt_meshdata & mesh)
{
  if(! mesh_is_trisurf(mesh)) {
    fprintf(stderr, "%s error: STL supports only triangle meshes! Aborting!\n", __func__);
    return;
  }

  mt_bfile fd;
  fd.open_write(file.c_str(), 10*1024*1024);

  if(! fd.ok()) return;

  // we skip the header
  char header[80];
  memset(header, 0, 80);
  snprintf(header, 80, "binary STL by meshtool");
  fd.write(header, 80, 1);
  memset(header, 0, 80);

  uint32_t ntri = mesh.e2n_cnt.size();
  fd.write(&ntri, 4, 1); // number of tri

  auto write_vertex = [&] (vec3r & v) {
    size_t nwrite = 0;
    float buff;

    buff = float(v.x); nwrite += fd.write(&buff, 1, 4);
    buff = float(v.y); nwrite += fd.write(&buff, 1, 4);
    buff = float(v.z); nwrite += fd.write(&buff, 1, 4);

    return nwrite;
  };

  auto write_tri = [&](size_t eidx) {
    size_t nwrite = 0;
    const mt_idx_t* con = mesh.e2n_con.data() + eidx*3;
    vec3r n = triangle_normal(con[0], con[1], con[2], mesh.xyz);

    vec3r v1(mesh.xyz.data() + con[0]*3);
    vec3r v2(mesh.xyz.data() + con[1]*3);
    vec3r v3(mesh.xyz.data() + con[2]*3);

    nwrite += write_vertex(n);
    nwrite += write_vertex(v3);
    nwrite += write_vertex(v2);
    nwrite += write_vertex(v1);
    nwrite += fd.write(header, 1, 2);

    return nwrite == 50;
  };

  bool success = false;
  size_t tri_write = 0;
  do {
    success = write_tri(tri_write);
    if(success) tri_write++;
  } while(success && tri_write < ntri);
}

#ifdef __WIN32__
bool get_dir_entries(const char* path, mt_vector<dir_entry> & entries)
{
  WIN32_FIND_DATAA fdFile;
  HANDLE hFind = NULL;
  char sPath[2048];
  dir_entry entry;

  //Specify a file mask. *.* = We want everything!
  sprintf(sPath, "%s\\*", path);

  if ((hFind = FindFirstFileA(sPath, &fdFile)) == INVALID_HANDLE_VALUE)
  {
    fprintf(stderr, "Path not found: [%s]\n", path);
    return false;
  }

  do {
    if (!(strcmp(fdFile.cFileName, ".") == 0) &&
        !(fdFile.dwFileAttributes & FILE_ATTRIBUTE_REPARSE_POINT))
    {
      sprintf(entry.name, "%s", fdFile.cFileName);
      entry.is_dir = fdFile.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY;
      entry.size   = (fdFile.nFileSizeHigh * (MAXDWORD+1)) + fdFile.nFileSizeLow;

      SYSTEMTIME stime;
      if(FileTimeToSystemTime(&fdFile.ftLastWriteTime, &stime)) {
        const char* month = NULL;
        switch(stime.wMonth) {
          case 1: month = "Jan"; break;
          case 2: month = "Feb"; break;
          case 3: month = "Mar"; break;
          case 4: month = "Apr"; break;
          case 5: month = "May"; break;
          case 6: month = "Jun"; break;
          case 7: month = "Jul"; break;
          case 8: month = "Aug"; break;
          case 9: month = "Sep"; break;
          case 10: month = "Oct"; break;
          case 11: month = "Nov"; break;
          case 12: month = "Dec"; break;
          default: month = "---"; break;
        }
        sprintf(entry.mod_str, "%02d %s %4d", stime.wDay, month, stime.wYear);
      } else {
        strcpy(entry.mod_str, "---");
      }

      entries.push_back(entry);
    }
  } while (FindNextFileA(hFind, &fdFile));

  FindClose(hFind);

  std::sort(entries.begin(), entries.end());
  return true;
}
#else
bool get_dir_entries(const char* path, mt_vector<dir_entry> & entries)
{
  DIR *dir;
  struct dirent *ent;
  dir_entry entry;
  char lnk_buff[1024];

  if ((dir = opendir (path)) != NULL)
  {
    while ((ent = readdir(dir)) != NULL) {
      if(strcmp(ent->d_name, ".") == 0) continue;

      struct stat statbuff;
      sprintf(lnk_buff, "%s/%s", path, ent->d_name);
      lstat(lnk_buff, &statbuff);

      if(S_ISLNK(statbuff.st_mode)) {
        entry.from_link = true;

      if(stat(lnk_buff, &statbuff) == 0)
        entry.is_dir = S_ISDIR(statbuff.st_mode);
      else {
        fprintf(stderr, "%s error: could not evaluate the path \"%s\"! Reason: %s\n",
                __func__, lnk_buff, strerror(errno));
        entry.is_dir = false;
      }

      } else {
        entry.from_link = false;
        entry.is_dir    = S_ISDIR(statbuff.st_mode);
      }

      struct tm* mod_time = localtime(&statbuff.st_mtime);
      strftime(entry.mod_str, sizeof(entry.mod_str), "%d %b %Y", mod_time);

      entry.size = statbuff.st_size;
      sprintf(entry.name, "%s", ent->d_name);
      entries.push_back(entry);
    }

    closedir(dir);

    std::sort(entries.begin(), entries.end());
    return true;
  }
  else {
    treat_file_open_error(path);
    entries.resize(0);
    return false;
  }
}
#endif

std::string mt_get_cwd()
{
#ifndef __WIN32__
  char buff[512];
  getcwd(buff, 511);

  return std::string(buff);
#else
  static char ret[1024], buff[1024];
  if(!GetCurrentDirectoryA(1024, ret)) {
    GetModuleFileNameA(NULL, buff, 1024);
    strcpy(ret, mt_dirname(buff).c_str());
  }

  return std::string(ret);
#endif
}


