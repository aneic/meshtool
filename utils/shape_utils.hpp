#ifndef SHAPE_UTILS_H
#define SHAPE_UTILS_H

#include "mt_utils_base.h"
#include "dense_mat.hpp"
#include <iostream>
#include <array>

mt_real distance_triangle(const vec3r &p, const vec3r &v0, const vec3r &v1, const vec3r &v2, bool &behind);

mt_real distance_parallelogram(const vec3r &p, const vec3r &v0, const vec3r &v1, const vec3r &v2, bool &behind);

//mt_real distance_ref_tet(const vec3r &p);

extern const vec3r v000;
extern const vec3r v100;
extern const vec3r v010;
extern const vec3r v110;
extern const vec3r v001;
extern const vec3r v101;
extern const vec3r v011;
extern const vec3r v111;

class Shape
{
public:
  Shape()
  {}

  virtual ~Shape()
  {}

  virtual vec3r eval_shape(const vec3r &coord) const = 0;

  virtual mat3x3r eval_grad(const vec3r &coord) const = 0;

  /**
  * @brief Compute the barycentric coordinate that matches "point" when evaluating
  * the shape functions with the vertices defined on the element.
  *
  * @param point        Point we want to match.
  * @param bary_coord   Barycentric coordinate being optimized.
  * @param res_tol      Error tolerance threshold.
  * @param max_it       Max iteration count.
  * @param inv_tol      Tolerance of Jacobi inverse determinant (towards 0).
  *
  * @return error code, 0 for no error.
  */
  int barycentric_coordinate(const vec3r &point, vec3r &bary_coord, const mt_real res_tol=1.0e-8,
                             const std::size_t max_it=10, const mt_real inv_tol=1.0e-10)
  {
    vec3r start = bary_coord;

    vec3r rhs(point-eval_shape(bary_coord));
    mt_real res = rhs.length();

    if (res < res_tol)
      return 0;

    std::size_t count(0);
    vec3r update;
    mat3x3r lhs;
    while ((res > res_tol) && (count < max_it)) {
      count++;
      lhs = eval_grad(bary_coord);
      // lhs.disp();

      const mt_real det = invert_mat3x3(lhs);
      if (std::fabs(det) > inv_tol) {
        update = lhs.dotProd(rhs);

        vec3r old_coord = bary_coord;
        mt_real res_old = res;
        bary_coord = old_coord + update;
        rhs = point - eval_shape(bary_coord);
        res = rhs.length();

        if(res > res_old) {
          bary_coord = old_coord + ((start - old_coord) * mt_real(0.01));
          rhs = point - eval_shape(bary_coord);
          res = rhs.length();
        }
      }
      else {
        // fprintf(stderr, "%s error: cannot invert Jacobian!\n", __func__);
        return 1;
      }
    }

    if (count == max_it) {
      // fprintf(stderr, "%s error: reached max iter! Res: %g\n", __func__, res);
      return 2;
    }

    return 0;
  }
};

template <std::size_t N>
class VertexShape : public virtual Shape
{
private:
  mutable std::array<mt_real, N> _shape;
  mutable std::array<vec3r, N> _grad;

protected:
  std::array<vec3r, N> _vertices;

public:
  virtual void fill_shape(const vec3r &coord, std::array<mt_real, N> &shape) const = 0;

  virtual void fill_grad(const vec3r &coord, std::array<vec3r, N> &grad) const = 0;

  VertexShape(const std::array<vec3r, N> &vertices)
    : Shape(), _vertices(vertices)
  {}

  virtual ~VertexShape()
  {}

  vec3r eval_shape(const vec3r &coord) const override
  {
    fill_shape(coord, _shape);
    vec3r coord_shape;
    for (std::size_t i=0; i<N; i++)
      coord_shape += _vertices[i]*_shape[i];
    return coord_shape;
  }

  template<typename T>
  T eval_shape(const vec3r &coord, const T* data) const
  {
    fill_shape(coord, _shape);
    T res = T();

    for (std::size_t i=0; i<N; i++)
      res += data[i] * _shape[i];

    return res;
  }

  mat3x3r eval_grad(const vec3r &coord) const override
  {
    fill_grad(coord, _grad);
    mat3x3r coord_grad;
    for (std::size_t i=0; i<N; i++)
      coord_grad += outerProd(_vertices[i], _grad[i]);
    return coord_grad;
  }

  virtual mt_real barycentric_distance(const vec3r &coord) const = 0;
};


class TriShape : public virtual VertexShape<3>
{
public:
  static void fill_shape(const vec3r &coord,mt_real* shape)
  {
    shape[0] = 1.0 - coord.x - coord.y;
    shape[1] = coord.x;
    shape[2] = coord.y;
  }

  void fill_shape(const vec3r &coord, std::array<mt_real, 3> &shape) const override
  {
    TriShape::fill_shape(coord, shape.begin());
  }

  static void fill_grad(const vec3r &coord, vec3r* grad)
  {
    grad[0].assign(-1.0, -1.0, 0.0);
    grad[1].assign(1.0, 0.0, 0.0);
    grad[2].assign(0.0, 1.0, 0.0);
  }

  void fill_grad(const vec3r &coord, std::array<vec3r, 3> &grad) const override
  {
    TriShape::fill_grad(coord, grad.begin());
  }

  TriShape() : VertexShape({v000, v100, v010})
  {}

  TriShape(const vec3r &v0, const vec3r &v1, const vec3r &v2) : VertexShape({v0, v1, v2})
  {}

  TriShape(const std::array<vec3r, 3> &vertices) : VertexShape(vertices)
  {}

  ~TriShape()
  {}

  mt_real barycentric_distance(const vec3r &coord) const override
  {return 0.0;}

  void set_vertices(const vec3r &v0, const vec3r &v1, const vec3r &v2)
  {
    _vertices[0] = v0;
    _vertices[1] = v1;
    _vertices[2] = v2;
  }

  int barycentric_coordinate(const vec3r &point, vec3r &bary_coord, const mt_real res_tol=1.0e-8,
                             const std::size_t max_it=10, const mt_real inv_tol=1.0e-10)
  {
    vec3r start = bary_coord;

    vec3r rhs(point-eval_shape(bary_coord));
    mt_real res = rhs.length();

    if (res < res_tol)
      return 0;

    std::size_t count(0);
    vec3r update;
    mat3x3r lhs;

    dmat<mt_real> J, Jt, JtJ;

    auto get_J_inv = [&]() {
      J.set_size(3,2);
      J[0][0] = lhs.xx, J[0][1] = lhs.xy;
      J[1][0] = lhs.yx, J[1][1] = lhs.yy;
      J[2][0] = lhs.zx, J[2][1] = lhs.zy;

      Jt = J; Jt.transpose();
      Jt.mult(J, JtJ);

      mt_real det = 0.0;
      invert_2x2(JtJ, det);

      return det;
    };

    auto get_update = [&]() {
      mt_real r[] = {rhs.x, rhs.y, rhs.z}; // rhs
      mt_real rt[2]; // rhs time J transposed
      mt_real u[2];  // update

      Jt.mult(r, rt);
      JtJ.mult(rt, u);
      update.x = u[0], update.y = u[1], update.z = 0;
    };

    while ((res > res_tol) && (count < max_it)) {
      count++;
      lhs = eval_grad(bary_coord);
      mt_real det = get_J_inv();

      if (std::fabs(det) > inv_tol) {
        get_update();

        vec3r old_coord = bary_coord;
        mt_real res_old = res;

        bary_coord = old_coord + update;
        rhs = point - eval_shape(bary_coord);
        res = rhs.length();

        if(res > res_old) {
          bary_coord = old_coord + ((start - old_coord) * mt_real(0.01));
          rhs = point - eval_shape(bary_coord);
          res = rhs.length();
        }
      }
      else {
        // fprintf(stderr, "%s error: cannot invert Jacobian!\n", __func__);
        return 1;
      }
    }

    if (count == max_it) {
      // fprintf(stderr, "%s error: reached max iter! Res: %g\n", __func__, res);
      return 2;
    }

    return 0;
  }

};

class QuadShape : public virtual VertexShape<4>
{
public:
  static void fill_shape(const vec3r &coord, mt_real* shape)
  {
    shape[0] = (1.0-coord.x)*(1.0-coord.y);
    shape[1] = coord.x*(1.0-coord.y);
    shape[2] = coord.x*coord.y;
    shape[3] = (1.0-coord.x)*coord.y;
  }

  void fill_shape(const vec3r &coord, std::array<mt_real, 4> &shape) const override
  {
    QuadShape::fill_shape(coord, shape.begin());
  }

  static void fill_grad(const vec3r &p, vec3r* grad)
  {
    grad[0].assign( -(1.0-p.y), -(1.0-p.x), 0.0);
    grad[1].assign(  (1.0-p.y),       -p.x, 0.0);
    grad[2].assign(        p.y,        p.x, 0.0);
    grad[3].assign(       -p.y,  (1.0-p.x), 0.0);
  }

  void fill_grad(const vec3r &p, std::array<vec3r, 4> &grad) const override
  {
    QuadShape::fill_grad(p, grad.begin());
  }

  QuadShape() : VertexShape({v000, v100, v110, v010})
  {}

  QuadShape(const vec3r &v0, const vec3r &v1, const vec3r &v2, const vec3r &v3) : VertexShape({v0, v1, v2, v3})
  {}

  QuadShape(const std::array<vec3r, 4> &vertices) : VertexShape(vertices)
  {}

  ~QuadShape()
  {}

  mt_real barycentric_distance(const vec3r &coord) const override
  {return 0.0;}

  void set_vertices(const vec3r &v0, const vec3r &v1, const vec3r &v2, const vec3r &v3)
  {
    _vertices[0] = v0;
    _vertices[1] = v1;
    _vertices[2] = v2;
    _vertices[3] = v3;
  }

  int barycentric_coordinate(const vec3r &point, vec3r &bary_coord, const mt_real res_tol=1.0e-8,
                             const std::size_t max_it=10, const mt_real inv_tol=1.0e-10)
  {
    vec3r start = bary_coord;

    vec3r rhs(point-eval_shape(bary_coord));
    mt_real res = rhs.length();

    if (res < res_tol)
      return 0;

    std::size_t count(0);
    vec3r update;
    mat3x3r lhs;

    dmat<mt_real> J, Jt, JtJ;

    auto get_J_inv = [&]() {
      J.set_size(3,2);
      J[0][0] = lhs.xx, J[0][1] = lhs.xy;
      J[1][0] = lhs.yx, J[1][1] = lhs.yy;
      J[2][0] = lhs.zx, J[2][1] = lhs.zy;

      Jt = J; Jt.transpose();
      Jt.mult(J, JtJ);

      mt_real det = 0.0;
      invert_2x2(JtJ, det);

      return det;
    };

    auto get_update = [&]() {
      mt_real r[] = {rhs.x, rhs.y, rhs.z}; // rhs
      mt_real rt[2]; // rhs time J transposed
      mt_real u[2];  // update

      Jt.mult(r, rt);
      JtJ.mult(rt, u);
      update.x = u[0], update.y = u[1], update.z = 0;
    };

    while ((res > res_tol) && (count < max_it)) {
      count++;
      lhs = eval_grad(bary_coord);
      mt_real det = get_J_inv();

      if (std::fabs(det) > inv_tol) {
        get_update();

        vec3r old_coord = bary_coord;
        mt_real res_old = res;

        bary_coord = old_coord + update;
        rhs = point - eval_shape(bary_coord);
        res = rhs.length();

        if(res > res_old) {
          bary_coord = old_coord + ((start - old_coord) * mt_real(0.01));
          rhs = point - eval_shape(bary_coord);
          res = rhs.length();
        }
      }
      else {
        // fprintf(stderr, "%s error: cannot invert Jacobian!\n", __func__);
        return 1;
      }
    }

    if (count == max_it) {
      // fprintf(stderr, "%s error: reached max iter! Res: %g\n", __func__, res);
      return 2;
    }

    return 0;
  }
};

class TetraShape : public virtual VertexShape<4>
{
public:
  static void fill_shape(const vec3r &coord,mt_real* shape)
  {
    shape[0] = 1.0 - coord.x - coord.y - coord.z;
    shape[1] = coord.x;
    shape[2] = coord.y;
    shape[3] = coord.z;
  }

  void fill_shape(const vec3r &coord, std::array<mt_real, 4> &shape) const override
  {
    TetraShape::fill_shape(coord, shape.begin());
  }

  static void fill_grad(const vec3r &coord, vec3r* grad)
  {
    grad[0].assign(-1.0, -1.0, -1.0);
    grad[1].assign(1.0, 0.0, 0.0);
    grad[2].assign(0.0, 1.0, 0.0);
    grad[3].assign(0.0, 0.0, 1.0);
  }

  void fill_grad(const vec3r &coord, std::array<vec3r, 4> &grad) const override
  {
    TetraShape::fill_grad(coord, grad.begin());
  }

  TetraShape()
    : VertexShape({v000, v100, v010, v001})
  {}

  TetraShape(const vec3r &v0, const vec3r &v1, const vec3r &v2, const vec3r &v3)
    : VertexShape({v0, v1, v2, v3})
  {}

  TetraShape(const std::array<vec3r, 4> &vertices)
    : VertexShape(vertices)
  {}

  ~TetraShape()
  {}

  mt_real barycentric_distance(const vec3r &coord) const override
  {
#if 0
    // =================
    // TODO[MG]: finish
    // ================

    const mt_real d10 = (v100-v000).scaProd(coord-v000);
    const mt_real d20 = (v010-v000).scaProd(coord-v000);
    const mt_real d30 = (v001-v000).scaProd(coord-v000);
    const mt_real d01 = (v000-v100).scaProd(coord-v100);
    const mt_real d21 = (v010-v100).scaProd(coord-v100);
    const mt_real d31 = (v001-v100).scaProd(coord-v100);
    const mt_real d02 = (v000-v010).scaProd(coord-v010);
    const mt_real d12 = (v100-v010).scaProd(coord-v010);
    const mt_real d32 = (v001-v010).scaProd(coord-v010);
    const mt_real d03 = (v000-v001).scaProd(coord-v001);
    const mt_real d13 = (v100-v001).scaProd(coord-v001);
    const mt_real d23 = (v010-v001).scaProd(coord-v001);

    const mt_real e00 = (coord-v000).scaProd(-v010);
    const mt_real e01 = (coord-v000).scaProd(-v001);
    const mt_real e10 = (coord-v000).scaProd(-v100);
    const mt_real e11 = (coord-v000).scaProd(-v001);
    const mt_real e20 = (coord-v000).scaProd(-v100);
    const mt_real e21 = (coord-v000).scaProd(-v010);
    const mt_real e30 = (coord-v100).scaProd(v100+v010);
    const mt_real e31 = (coord-v100).scaProd(v100+v010-v001*2.0);
    const mt_real e40 = (coord-v100).scaProd(v100+v001);
    const mt_real e41 = (coord-v100).scaProd(v100-v010*2.0+v001);
    const mt_real e50 = (coord-v010).scaProd(v010+v001);
    const mt_real e51 = (coord-v010).scaProd(-v100*2.0+v010+v001);

    const mt_real n0 = (coord-v000).scaProd(-v100);
    const mt_real n1 = (coord-v100).scaProd(-v010);
    const mt_real n2 = (coord-v010).scaProd(-v001);
    const mt_real n3 = (coord-v001).scaProd(v100+v010+v001);

    vec3r proj(coord);
    // Vertices
    if      ((d10 <= 0.0) && (d20 <= 0.0) && (d30 <= 0.0)) proj = v000;
    else if ((d01 <= 0.0) && (d21 <= 0.0) && (d31 <= 0.0)) proj = v100;
    else if ((d02 <= 0.0) && (d12 <= 0.0) && (d32 <= 0.0)) proj = v010;
    else if ((d03 <= 0.0) && (d13 <= 0.0) && (d23 <= 0.0)) proj = v001;
    // Edges
    else if ((d10 > 0.0) && (d01 > 0.0) && (e00 >= 0.0) && (e01 >= 0.0)) proj = v000 + (v100-v000)*d10;
    else if ((d20 > 0.0) && (d02 > 0.0) && (e10 >= 0.0) && (e11 >= 0.0)) proj = v000 + (v010-v000)*d20;
    else if ((d30 > 0.0) && (d03 > 0.0) && (e20 >= 0.0) && (e21 >= 0.0)) proj = v000 + (v001-v000)*d30;
    else if ((d12 > 0.0) && (d21 > 0.0) && (e30 >= 0.0) && (e31 >= 0.0)) proj = v100 + (v010-v100)*d21*0.5;
    else if ((d13 > 0.0) && (d31 > 0.0) && (e40 >= 0.0) && (e41 >= 0.0)) proj = v100 + (v001-v100)*d31*0.5;
    else if ((d23 > 0.0) && (d32 > 0.0) && (e50 >= 0.0) && (e51 >= 0.0)) proj = v010 + (v001-v010)*d32*0.5;
    // Faces
    else if ((d20 > 0.0) && (d02 > 0.0) && (d30 > 0.0) && (d03 > 0.0) && (d23 > 0.0) && (d32 > 0.0) && (n0 > 0.0)) proj = v000 + (v010-v000)*d20 + (v001-v000)*d30;
    else if ((d10 > 0.0) && (d01 > 0.0) && (d13 > 0.0) && (d31 > 0.0) && (d03 > 0.0) && (d30 > 0.0) && (n1 > 0.0)) proj = v000 + (v100-v000)*d10 + (v001-v000)*d30;
    else if ((d10 > 0.0) && (d01 > 0.0) && (d12 > 0.0) && (d21 > 0.0) && (d02 > 0.0) && (d20 > 0.0) && (n2 > 0.0)) proj = v000 + (v100-v000)*d10 + (v010-v000)*d20;
    else if ((d12 > 0.0) && (d21 > 0.0) && (d13 > 0.0) && (d31 > 0.0) && (d32 > 0.0) && (d23 > 0.0) && (n3 > 0.0)) proj = v100 + (v010-v100)*d21*0.5 + (v001-v100)*d31*0.5;

    return (proj-coord).length();
#else
    if ((coord.x > 0.0) && (coord.y > 0.0) && (coord.z > 0.0) && (1.0-coord.x-coord.y-coord.z > 0.0))
      return 0.0;
    else {
      bool behind;
      const mt_real dist0 = distance_triangle(coord, v000, v010, v100, behind);
      const mt_real dist1 = distance_triangle(coord, v000, v100, v001, behind);
      const mt_real dist2 = distance_triangle(coord, v010, v000, v001, behind);
      const mt_real dist3 = distance_triangle(coord, v100, v010, v001, behind);
      return std::min({dist0, dist1, dist2, dist3});
    }
#endif
 }

  void set_vertices(const vec3r &v0, const vec3r &v1, const vec3r &v2, const vec3r &v3)
  {
    _vertices[0] = v0;
    _vertices[1] = v1;
    _vertices[2] = v2;
    _vertices[3] = v3;
  }
};

class PyramidShape : public virtual VertexShape<5>
{

/*
  Bedrosian, G. (1992). Shape functions and integration formulas for three‐dimensional 
  finite element analysis. International Journal for Numerical Methods in Engineering, 
  35(1), 95–108. https://doi.org/10.1002/nme.1620350106
*/

public:
  static void fill_shape(const vec3r &coord,mt_real* shape)
  {
    // changed shape functions according to the paper above
    // transformed with u1=2x+z-1, u2=2y+z-1 and u3=z to
    // the pyramid spanned by the vertices
    // { (0,0,0); (1,0,0); (1,1,0); (0,1,0); (0,0,1) }
    shape[0] = -(coord.z*coord.z + (coord.y+coord.x-2.0)*coord.z + (coord.x-1.0)*coord.y - coord.x + 1.0) / (coord.z-1.0);
    shape[1] =  (coord.x*coord.z + coord.x*coord.y - coord.x) / (coord.z-1.0);
    shape[2] = -(coord.x*coord.y) / (coord.z-1.0);
    shape[3] =  (coord.y*coord.z + (coord.x-1.0)*coord.y) / (coord.z-1.0);
    shape[4] =  coord.z;
  }

  void fill_shape(const vec3r &coord, std::array<mt_real, 5> &shape) const override
  {
    PyramidShape::fill_shape(coord, shape.begin());
  }

  static void fill_grad(const vec3r &p, vec3r* grad)
  {
    grad[0].assign(-(p.z+p.y-1.0)/(p.z-1.0), -(p.z+p.x-1.0)/(p.z-1.0), -(p.z*p.z-2.0*p.z-p.x*p.y+1.0)/(p.z*p.z-2.0*p.z+1.0));
    grad[1].assign( (p.z+p.y-1.0)/(p.z-1.0),            p.x/(p.z-1.0),                     -(p.x*p.y)/(p.z*p.z-2.0*p.z+1.0));
    grad[2].assign(          -p.y/(p.z-1.0),           -p.x/(p.z-1.0),                      (p.x*p.y)/(p.z*p.z-2.0*p.z+1.0));
    grad[3].assign(           p.y/(p.z-1.0),  (p.z+p.x-1.0)/(p.z-1.0),                     -(p.x*p.y)/(p.z*p.z-2.0*p.z+1.0));
    grad[4].assign(                     0.0,                      0.0,                                                  1.0);
  }

  void fill_grad(const vec3r &coord, std::array<vec3r, 5> &grad) const override
  {
    PyramidShape::fill_grad(coord, grad.begin());
  }

  PyramidShape()
    : VertexShape({v000, v100, v110, v010, v001})
  {}

  PyramidShape(const vec3r &v0, const vec3r &v1, const vec3r &v2,
          const vec3r &v3, const vec3r &v4)
    : VertexShape({v0, v1, v2, v3, v4})
  {}

  PyramidShape(const std::array<vec3r, 5> &vertices)
    : VertexShape(vertices)
  {}

  ~PyramidShape()
  {}

  mt_real barycentric_distance(const vec3r &coord) const override
  {
    bool behind, inside = true;
    const mt_real dist0 = distance_parallelogram(coord, v000, v010, v100, behind);
    if (!behind) inside = false;
    const mt_real dist1 = distance_triangle(coord, v000, v100, v001, behind);
    if (!behind) inside = false;
    const mt_real dist2 = distance_triangle(coord, v100, v110, v001, behind);
    if (!behind) inside = false;
    const mt_real dist3 = distance_triangle(coord, v110, v010, v001, behind);
    if (!behind) inside = false;
    const mt_real dist4 = distance_triangle(coord, v010, v000, v001, behind);
    if (!behind) inside = false;
    return (inside) ? 0.0 : std::min({dist0, dist1, dist2, dist3, dist4});
  }

  void set_vertices(const vec3r &v0, const vec3r &v1, const vec3r &v2,
      const vec3r &v3, const vec3r &v4)
  {
    _vertices[0] = v0;
    _vertices[1] = v1;
    _vertices[2] = v2;
    _vertices[3] = v3;
    _vertices[4] = v4;
  }
};

class PrismShape : public virtual VertexShape<6>
{
public:
  static void fill_shape(const vec3r &p, mt_real* shape)
  {
    shape[0] = (1.0 - p.x - p.y)*(1.0 - p.z);
    shape[1] = p.y*(1.0 - p.z);
    shape[2] = p.x*(1.0 - p.z);
    shape[3] = (1.0 - p.x - p.y)*p.z;
    shape[4] = p.x*p.z;
    shape[5] = p.y*p.z;
  }

  void fill_shape(const vec3r &coord, std::array<mt_real, 6> &shape) const override
  {
    PrismShape::fill_shape(coord, shape.begin());
  }

  static void fill_grad(const vec3r &p, vec3r* grad)
  {
    grad[0].assign( p.z-1.0, p.z-1.0, p.x+p.y-1.0);
    grad[1].assign(     0.0, 1.0-p.z,        -p.y);
    grad[2].assign( 1.0-p.z,     0.0,        -p.x);
    grad[3].assign(    -p.z,    -p.z, 1.0-p.x-p.y);
    grad[4].assign(     p.z,     0.0,         p.x);
    grad[5].assign(     0.0,     p.z,         p.y);
  }

  void fill_grad(const vec3r &p, std::array<vec3r, 6> &grad) const override
  {
    PrismShape::fill_grad(p, grad.begin());
  }

  PrismShape()
    : VertexShape({v000, v100, v010, v001, v101, v011})
  {}

  PrismShape(const vec3r &v0, const vec3r &v1, const vec3r &v2,
        const vec3r &v3, const vec3r &v4, const vec3r &v5)
    : VertexShape({v0, v1, v2, v3, v4, v5})
  {}

  PrismShape(const std::array<vec3r, 6> &vertices)
    : VertexShape(vertices)
  {}

  ~PrismShape()
  {}

  mt_real barycentric_distance(const vec3r &coord) const override
  {
    bool behind, inside = true;
    const mt_real dist0 = distance_triangle(coord, v000, v010, v100, behind);
    if (!behind) inside = false;
    const mt_real dist1 = distance_triangle(coord, v001, v101, v011, behind);
    if (!behind) inside = false;
    const mt_real dist2 = distance_parallelogram(coord, v000, v100, v001, behind);
    if (!behind) inside = false;
    const mt_real dist3 = distance_parallelogram(coord, v010, v000, v001, behind);
    if (!behind) inside = false;
    const mt_real dist4 = distance_parallelogram(coord, v100, v010, v101, behind);
    if (!behind) inside = false;
    return (inside) ? 0.0 : std::min({dist0, dist1, dist2, dist3, dist4});
  }

  void set_vertices(const vec3r &v0, const vec3r &v1, const vec3r &v2,
                    const vec3r &v3, const vec3r &v4, const vec3r v5)
  {
    _vertices[0] = v0;
    _vertices[1] = v1;
    _vertices[2] = v2;
    _vertices[3] = v3;
    _vertices[4] = v4;
    _vertices[5] = v5;
  }
};

class HexShape : public virtual VertexShape<8>
{
public:
  static void fill_shape(const vec3r &coord, mt_real* shape)
  {
    shape[0] = (1.0-coord.x)*(1.0-coord.y)*(1.0-coord.z);
    shape[1] = coord.x*(1.0-coord.y)*(1.0-coord.z);
    shape[2] = coord.x*coord.y*(1.0-coord.z);
    shape[3] = (1.0-coord.x)*coord.y*(1.0-coord.z);
    shape[4] = (1.0-coord.x)*(1.0-coord.y)*coord.z;
    shape[5] = (1.0-coord.x)*coord.y*coord.z;
    shape[6] = coord.x*coord.y*coord.z;
    shape[7] = coord.x*(1.0-coord.y)*coord.z;
  }

  void fill_shape(const vec3r &coord, std::array<mt_real, 8> &shape) const override
  {
    HexShape::fill_shape(coord, shape.begin());
  }

  static void fill_grad(const vec3r &p, vec3r* grad)
  {
    grad[0].assign( -(1.0-p.y)*(1.0-p.z), -(1.0-p.x)*(1.0-p.z), -(1.0-p.x)*(1.0-p.y));
    grad[1].assign(  (1.0-p.y)*(1.0-p.z),       -p.x*(1.0-p.z),       -p.x*(1.0-p.y));
    grad[2].assign(        p.y*(1.0-p.z),        p.x*(1.0-p.z),             -p.x*p.y);
    grad[3].assign(       -p.y*(1.0-p.z),  (1.0-p.x)*(1.0-p.z),       -(1.0-p.x)*p.y);
    grad[4].assign(       -(1.0-p.y)*p.z,       -(1.0-p.x)*p.z,  (1.0-p.x)*(1.0-p.y));
    grad[5].assign(             -p.y*p.z,        (1.0-p.x)*p.z,        (1.0-p.x)*p.y);
    grad[6].assign(              p.y*p.z,              p.x*p.z,              p.x*p.y);
    grad[7].assign(         (1.0-p.y)*p.z,            -p.x*p.z,        p.x*(1.0-p.y));
  }

  void fill_grad(const vec3r &coord, std::array<vec3r, 8> &grad) const override
  {
    HexShape::fill_grad(coord, grad.begin());
  }

  HexShape()
    : VertexShape({v000, v100, v110, v010, v001, v101, v111, v011})
  {}

  HexShape(const vec3r &v0, const vec3r &v1, const vec3r &v2, const vec3r &v3,
             const vec3r &v4, const vec3r &v5, const vec3r &v6, const vec3r &v7)
    : VertexShape({v0, v1, v2, v3, v4, v5, v6, v7})
  {}

  HexShape(const std::array<vec3r, 8> &vertices)
    : VertexShape(vertices)
  {}

  ~HexShape()
  {}

  mt_real barycentric_distance(const vec3r &coord) const override
  {
    bool behind, inside = true;
    const mt_real dist0 = distance_parallelogram(coord, v000, v010, v100, behind);
    if (!behind) inside = false;
    const mt_real dist1 = distance_parallelogram(coord, v000, v100, v001, behind);
    if (!behind) inside = false;
    const mt_real dist2 = distance_parallelogram(coord, v100, v110, v101, behind);
    if (!behind) inside = false;
    const mt_real dist3 = distance_parallelogram(coord, v110, v010, v111, behind);
    if (!behind) inside = false;
    const mt_real dist4 = distance_parallelogram(coord, v010, v000, v011, behind);
    if (!behind) inside = false;
    const mt_real dist5 = distance_parallelogram(coord, v101, v111, v001, behind);
    if (!behind) inside = false;
    return (inside) ? 0.0 : std::min({dist0, dist1, dist2, dist3, dist4, dist5});
  }

  void set_vertices(const vec3r &v0, const vec3r &v1, const vec3r &v2, const vec3r &v3,
                    const vec3r &v4, const vec3r &v5, const vec3r &v6, const vec3r &v7)
  {
    _vertices[0] = v0;
    _vertices[1] = v1;
    _vertices[2] = v2;
    _vertices[3] = v3;
    _vertices[4] = v4;
    _vertices[5] = v5;
    _vertices[6] = v6;
    _vertices[7] = v7;
  }
};

// The element centers of our shapes
static const vec3r elem_cntr_tri(1.0/3.0, 1.0/3.0,  0.0);
static const vec3r elem_cntr_qud(    0.5,     0.5,  0.0);
static const vec3r elem_cntr_tet(   0.25,    0.25, 0.25);
static const vec3r elem_cntr_pri(1.0/3.0, 1.0/3.0,  0.5);
static const vec3r elem_cntr_pyr(    0.4,     0.4,  0.2);
static const vec3r elem_cntr_hex(    0.5,     0.5,  0.5);

/// structure to hold the reference element gradient definitions
struct reference_grad {
  vec3r lgrad_tri[3], lgrad_quad[4], lgrad_tet[4], lgrad_pri[6], lgrad_pyr[5], lgrad_hex[8];

  reference_grad()
  {
    TriShape::    fill_grad(elem_cntr_tri, lgrad_tri);
    QuadShape::   fill_grad(elem_cntr_qud, lgrad_quad);
    TetraShape::  fill_grad(elem_cntr_tet, lgrad_tet);
    PrismShape::  fill_grad(elem_cntr_pri, lgrad_pri);
    PyramidShape::fill_grad(elem_cntr_pyr, lgrad_pyr);
    HexShape::    fill_grad(elem_cntr_hex, lgrad_hex);
  }
};

vec3r data_gradient(const vec3r* lgrad,
                    const mt_vector<vec3r>   & vtx,
                    const mt_vector<mt_real> & data);

/**
* @brief Compute the gradient vector field for a scalar input field.
*
* @param mesh          The mesh where the data is defined on.
* @param comp_tags     region tags that we compute the gradient on, rest is set to zero.
* @param data          Scalar input data.
* @param nodal_input   Whether the input is defined on the nodes or on the elements.
* @param nodal_output  Whether the output is defined on the nodes or on the elements.
* @param grad          Output vector holding the gradient vectors.
* @param mag           Output vector holding the gradient magnitudes.
*/
void compute_gradient(const mt_meshdata& mesh,
                      const MT_USET<mt_tag_t> & comp_tags,
                      const mt_vector<mt_real> & data,
                      const bool nodal_input, const bool nodal_output,
                      mt_vector<vec3r> & grad,
                      mt_vector<mt_real> & mag);

inline void compute_gradient(const mt_meshdata& mesh,
                      const mt_vector<mt_real> & data,
                      const bool nodal_input, const bool nodal_output,
                      mt_vector<vec3r> & grad,
                      mt_vector<mt_real> & mag)
{
  MT_USET<mt_tag_t> comp_tags;
  compute_gradient(mesh, comp_tags, data, nodal_input, nodal_output, grad, mag);
}


void compute_current_density(const mt_meshdata & mesh,
                             const mt_vector<mat3x3r> & cond,
                             const mt_vector<mt_real> & phie,
                             mt_vector<vec3r> & J_field);

template<class V> inline
V eval_shape(const mt_meshdata & mesh,
             const mt_idx_t eidx,
             const vec3r & bary_pt,
             const mt_vector<V> & dat)
{
  const mt_idx_t* con = mesh.e2n_con.data() + mesh.e2n_dsp[eidx];

  V data[8];
  for(mt_cnt_t i=0; i<mesh.e2n_cnt[eidx]; i++)
    data[i] = dat[con[i]];

  switch(mesh.etype[eidx]) {
    case Tri: {
      TriShape tri;
      V ret = tri.eval_shape(bary_pt, data);
      return ret;
    }

    case Tetra: {
      TetraShape tet;
      V ret = tet.eval_shape(bary_pt, data);
      return ret;
    }

    case Quad: {
      QuadShape quad;
      V ret = quad.eval_shape(bary_pt, data);
      return ret;
    }

    case Hexa: {
      HexShape hex;
      V ret = hex.eval_shape(bary_pt, data);
      return ret;
    }

    case Pyramid: {
      PyramidShape pyr;
      V ret = pyr.eval_shape(bary_pt, data);
      return ret;
    }

    case Prism: {
      PrismShape pri;
      V ret = pri.eval_shape(bary_pt, data);
      return ret;
    }

    default: return V();
  }
}

inline void fill_grad(const elem_t type,
                      const vec3r & bary_pt,
                      vec3r* grad)
{
  switch(type) {
    case Tri:
      TriShape::fill_grad(bary_pt, grad);
      break;

    case Tetra:
      TetraShape::fill_grad(bary_pt, grad);
      break;

    case Quad:
      QuadShape::fill_grad(bary_pt, grad);
      break;

    case Hexa:
      HexShape::fill_grad(bary_pt, grad);
      break;

    case Pyramid:
      PyramidShape::fill_grad(bary_pt, grad);
      break;

    case Prism:
      PrismShape::fill_grad(bary_pt, grad);
      break;

    default: break;
  }
}

inline void fill_shape(const elem_t type,
                       const vec3r & bary_pt,
                       mt_real* shape)
{
  switch(type) {
    case Tri:
      TriShape::fill_shape(bary_pt, shape);
      break;

    case Tetra:
      TetraShape::fill_shape(bary_pt, shape);
      break;

    case Quad:
      QuadShape::fill_shape(bary_pt, shape);
      break;

    case Hexa:
      HexShape::fill_shape(bary_pt, shape);
      break;

    case Pyramid:
      PyramidShape::fill_shape(bary_pt, shape);
      break;

    case Prism:
      PrismShape::fill_shape(bary_pt, shape);
      break;

    default: break;
  }
}

inline int
barycentric_coordinate(const mt_meshdata & mesh, const mt_idx_t eidx,
                       const vec3r &point, vec3r &bary_coord, const mt_real res_tol=1.0e-8,
                       const std::size_t max_it=10, const mt_real inv_tol=1.0e-10)
{
  const mt_idx_t* con = mesh.e2n_con.data() + mesh.e2n_dsp[eidx];
  vec3r p0(mesh.xyz.data() + con[0]*3);
  vec3r p1(mesh.xyz.data() + con[1]*3);

  switch(mesh.etype[eidx]) {
    case Tri: {
      vec3r p2(mesh.xyz.data() + con[2]*3);

      TriShape tri(p0, p1, p2);
      bary_coord = elem_cntr_tri;
      return tri.barycentric_coordinate(point, bary_coord, res_tol, max_it, inv_tol);
    }

    case Tetra: {
      vec3r p2(mesh.xyz.data() + con[2]*3);
      vec3r p3(mesh.xyz.data() + con[3]*3);

      TetraShape tet(p0, p1, p2, p3);
      bary_coord = elem_cntr_tet;
      return tet.barycentric_coordinate(point, bary_coord, res_tol, max_it, inv_tol);
    }

    case Quad: {
      vec3r p2(mesh.xyz.data() + con[2]*3);
      vec3r p3(mesh.xyz.data() + con[3]*3);

      QuadShape quad(p0, p1, p2, p3);
      bary_coord = elem_cntr_qud;
      return quad.barycentric_coordinate(point, bary_coord, res_tol, max_it, inv_tol);
    }

    case Hexa: {
      vec3r p2(mesh.xyz.data() + con[2]*3);
      vec3r p3(mesh.xyz.data() + con[3]*3);
      vec3r p4(mesh.xyz.data() + con[4]*3);
      vec3r p5(mesh.xyz.data() + con[5]*3);
      vec3r p6(mesh.xyz.data() + con[6]*3);
      vec3r p7(mesh.xyz.data() + con[7]*3);

      HexShape hex(p0, p1, p2, p3, p4, p5, p6, p7);
      bary_coord = elem_cntr_hex;
      return hex.barycentric_coordinate(point, bary_coord, res_tol, max_it, inv_tol);
    }

    case Pyramid: {
      vec3r p2(mesh.xyz.data() + con[2]*3);
      vec3r p3(mesh.xyz.data() + con[3]*3);
      vec3r p4(mesh.xyz.data() + con[4]*3);

      PyramidShape pyr(p0, p1, p2, p3, p4);
      bary_coord = elem_cntr_pyr;
      return pyr.barycentric_coordinate(point, bary_coord, res_tol, max_it, inv_tol);
    }

    case Prism: {
      vec3r p2(mesh.xyz.data() + con[2]*3);
      vec3r p3(mesh.xyz.data() + con[3]*3);
      vec3r p4(mesh.xyz.data() + con[4]*3);
      vec3r p5(mesh.xyz.data() + con[5]*3);

      PrismShape pri(p0, p1, p2, p3, p4, p5);
      bary_coord = elem_cntr_pri;
      return pri.barycentric_coordinate(point, bary_coord, res_tol, max_it, inv_tol);
    }

    default: return 1;
  }
}


#define IN_ITV(V, A, B) ((V >= A) && (V <= B))
inline bool valid_barycentric_coord(elem_t type, vec3r bari)
{
  double eps = 1e-4;

  switch(type) {
    case Tri:
      return IN_ITV(bari.x, -eps,  1.0+eps) &&
             IN_ITV(bari.y, -eps,  1.0+eps) &&
             IN_ITV(bari.z, -0.1, 0.1) &&
             (1.0 - bari.x - bari.y) > -eps;
    case Quad:
      return IN_ITV(bari.x, -eps, 1.0+eps) &&
             IN_ITV(bari.y, -eps, 1.0+eps);
    case Tetra:
      return IN_ITV(bari.x, -eps, 1.0+eps) &&
             IN_ITV(bari.y, -eps, 1.0+eps) &&
             IN_ITV(bari.z, -eps, 1.0+eps) &&
             (1.0 - bari.x - bari.y - bari.z) > -eps;
    case Pyramid:
      return IN_ITV(bari.z, 0.0, 1.0) && IN_ITV(bari.x, 0.0, 1.0-bari.z) && IN_ITV(bari.y, 0.0, 1.0-bari.z);
    case Prism:
      return IN_ITV(bari.x, 0.0, 1.0) && IN_ITV(bari.y, 0.0, 1.0) && IN_ITV(bari.z, 0.0, 1.0) &&
             (1.0 - bari.x - bari.y) > 0.0;
    case Hexa:
      return IN_ITV(bari.x, 0.0, 1.0) && IN_ITV(bari.y, 0.0, 1.0) && IN_ITV(bari.z, 0.0, 1.0);

    default: return true;
  }
}
#undef IN_ITV

#endif

