/**
* @file mesh_utils.h
* @brief Main mesh utilities header.
*
* This file includes the IO utils.
*
* @authors Aurel Neic
* @version
* @date 2016-12-13
*/



#ifndef _MESH_UTILS
#define _MESH_UTILS

#include "geometry_utils.h"
#include "io_utils.h"
#include "kdtree.h"

struct tag_surf_map {
  MT_MAP<mt_tag_t, MT_MAP<mt_triple<mt_idx_t>, tri_sele>*>     tri;
  MT_MAP<mt_tag_t, MT_MAP<mt_quadruple<mt_idx_t>, quad_sele>*> quad;

  inline void clear(const MT_USET<mt_tag_t> & tags) {
    for(mt_tag_t t : tags) {
      auto tit = tri.find(t);
      if(tit != tri.end()) {
        delete tit->second;
        tri.erase(tit);
      }

      auto qit = quad.find(t);
      if(qit != quad.end()) {
        delete qit->second;
        quad.erase(qit);
      }
    }
  }

  inline void clear() {
    for(auto & it : tri)
      delete it.second;
    for(auto & it : quad)
      delete it.second;

    tri.clear();
    quad.clear();
  }

  ~tag_surf_map() {
    clear();
  }
};


/**
* @brief Reduce a vector to all values not matching a certain flag.
*
* The vector is reduced in-place.
*
* @tparam T            Any type with a meaningful "!=" operator.
* @param vec [in,out]  Vector to reduce.
* @param fl  [in]      Flag value.
*/
template<class T>
void reduce_by_flag(mt_vector<T> &vec, T fl)
{
    size_t size = vec.size();
    size_t idx_read, idx_write;

    for(idx_read=0, idx_write=0; idx_read < size; idx_read++)
    {
        if(vec[idx_read] != fl)
        {
            vec[idx_write] = vec[idx_read];
            idx_write++;
        }
    }
    vec.resize(idx_write);
}

/**
* @brief Re-index the vertices of a mesh.
*
* @param mesh     The mesh.
* @param nod_out  The new-to-old index map.
*/
void reindex_nodes(mt_meshdata & mesh, mt_vector<mt_idx_t> & nod_out, bool verbose = true);

void reindex_nodes(mt_meshdata & mesh, bool verbose = true);

/**
* @brief Restrict the elements of a mesh.
*
* @param keep     [in]     Vector defining which element to keep.
* @param mesh     [in,out] The mesh.
* @param eidx_out [out]    The mapping vector from the current element indexing to the former one.
* @param verbose  [in]     Whether to print out progress.
*/
void restrict_elements(const mt_vector<bool> & keep,
                       mt_meshdata & mesh,
                       mt_vector<mt_idx_t> & eidx_out,
                       bool verbose = true);

/**
* @brief Restrict a mesh to a certain subset of elements.
*
* We keep the element with the index "eidx" if and only if
* keep[eidx] == true.
*
* The mesh data is restricted in-place. The nodes are re-indexed.
*
* @param keep     [in]     Vector defining which element to keep.
* @param mesh     [in,out] The mesh.
* @param nod_out  [out]    The mapping vector from the current node indexing to the former one.
* @param eidx_out [out]    The mapping vector from the current element indexing to the former one.
* @param verbose  [in]     Whether to print out progress.
*/
inline
void restrict_meshdata(const mt_vector<bool> & keep,
                       mt_meshdata & mesh,
                       mt_vector<mt_idx_t> & nod_out,
                       mt_vector<mt_idx_t> & eidx_out,
                       bool verbose = true)
{
  restrict_elements(keep, mesh, eidx_out, verbose);
  reindex_nodes(mesh, nod_out, verbose);
}

/**
* @brief Restrict a mesh to a certain subset of elements.
*
* We keep the element with the index "eidx" if and only if
* keep[eidx] == true.
*
* The mesh data is restricted in-place.
*
* @param keep     [in]     Vector defining which element to keep.
* @param mesh     [in,out] The mesh.
* @param eidx_out [out]    The mapping vector from the current element indexing to the former one.
* @param verbose  [in]     Whether to print out progress.
*/
inline
void restrict_meshdata(const mt_vector<bool> & keep,
                       mt_meshdata & mesh,
                       mt_vector<mt_idx_t> & eidx_out,
                       bool verbose = true)
{
  restrict_elements(keep, mesh, eidx_out, verbose);
}

/**
* @brief Extract a meshgraph from a mesh based on tags.
*
* @param tags  [in]  Tags defining what to extract.
* @param mesh  [in]  The mesh.
* @param graph [out] The extracted meshgraph.
*/
void extract_tagged_meshgraph(const MT_USET<mt_tag_t> & tags,
                              const mt_meshdata & mesh,
                              mt_meshgraph & graph);

/**
* @brief Extract a meshgraph from a mesh based on element indices.
*
* @param eidx  [in]  Element indices defining what to extract.
* @param mesh  [in]  The mesh.
* @param graph [out] The extracted meshgraph.
*/
void extract_meshgraph(const mt_vector<mt_idx_t> & eidx,
                       const mt_meshdata & mesh,
                       mt_meshgraph & graph);

/**
* @brief Extract the element data of a mesh from based on element indices.
*
* @param eidx  [in]  Element indices defining what to extract.
* @param mesh  [in]  The mesh.
* @param graph [out] The extracted mesh.
*/
void extract_elements(const mt_vector<mt_idx_t> & eidx,
                      const mt_meshdata & mesh,
                      mt_meshdata & outmesh);

/**
* @brief Extract a mesh (also vertices) from based on element indices.
*
* @param eidx  [in]  Element indices defining what to extract.
* @param mesh  [in]  The mesh.
* @param graph [out] The extracted mesh.
*/
void extract_mesh(const mt_vector<mt_idx_t> & eidx,
                       const mt_meshdata & mesh,
                       mt_meshdata & outmesh);

/**
 * @brief decompose mesh into elements of eidx and the complement
 * 
 * @param eidx       indices of the elements to extract
 * @param mesh       input mesh and output complement of extr_mesh 
 * @param extr_mesh  extracted mesh
 * @param reindex    whether to reindex mesh and extr_mesh
 */
void decompose_mesh(const mt_vector<mt_idx_t> & eidx,
                    mt_meshdata & mesh,
                    mt_meshdata & extr_mesh,
                    bool reindex);

/**
* @brief Insert new element tags into a mesh
*
* @param [out] mesh   The mesh we insert into.
* @param [in]  etags  The tags to insert.
* @param [in]  eidx   The indices where to insert the tags.
*/
void insert_etags(struct mt_meshdata & mesh, const mt_vector<mt_tag_t> & etags, const mt_vector<mt_idx_t> & eidx);

/**
* @brief Insert new element fibers into a mesh
*
* @param [out] mesh   The mesh we insert into.
* @param [in]  lon    The fibers to insert.
* @param [in]  eidx   The indices where to insert the fibers.
*/
void insert_fibers(struct mt_meshdata & mesh, const mt_vector<mt_fib_t> & lon, const mt_vector<mt_idx_t> & eidx);

/**
* @brief Insert new vertex coordinates into a mesh
*
* @param [out] mesh The mesh we insert into.
* @param [in]  xyz  The new coordinates.
* @param [in]  nod  The indices where to insert the coordinates.
*/
void insert_points(struct mt_meshdata & mesh, const mt_vector<mt_real> & xyz, const mt_vector<mt_idx_t> & nod);

/// sort the "in" mt_tuple into the "out" mt_tuple
template<class T>
void sortTuple(const T in1, const T in2, T & out1, T & out2)
{
   if(in1 < in2) out1 = in1, out2 = in2;
   else          out1 = in2, out2 = in1;
}

/// sort the "in" triple into the "out" triple
template<class T>
void sortTriple(const T in1, const T in2, const T in3, T & out1, T & out2, T & out3)
{
    bool t12 = in1 < in2;
  bool t13 = in1 < in3;
  bool t23 = in2 < in3;

  if(t12) {
    //(123),(312),(132)
    if(t23) {
      //123
      out1=in1; out2=in2; out3=in3;
    }
    else {
      //(312),(132)
      if(t13) {
        //132
        out1=in1; out2=in3; out3=in2;
      }
      else {
        //312
        out1=in3; out2=in1; out3=in2;
      }
    }
  }
  else {
    //(213),(231),(321)
    if(t23) {
      //(213),(231)
      if(t13) {
        //213
        out1=in2; out2=in1; out3=in3;
      }
      else {
        //231
        out1=in2; out2=in3; out3=in1;
      }
    }
    else {
      //321
      out1=in3; out2=in2; out3=in1;
    }
  }
}

/// sort the "in" triple into the "out" triple
template<class T>
void sortQuadruple(const T in1, const T in2, const T in3, const T in4, T & out1, T & out2, T & out3, T & out4)
{
  auto swap = [] (T & v1, T & v2) {
    T t = v1;
    v1 = v2, v2 = t;
  };

  out1 = in1, out2 = in2, out3 = in3, out4 = in4;

  if(out1 > out2) swap(out1, out2);
  if(out3 > out4) swap(out3, out4);
  if(out1 > out3) swap(out1, out3);
  if(out2 > out4) swap(out2, out4);
  if(out2 > out3) swap(out2, out3);
}

/**
* @brief Insert the triangle formed by indices (n1, n2, n3)
* into a map.
*
* @param n1       First triangle index.
* @param n2       Second triangle index.
* @param n3       Third triangle index.
* @param eidx     Index of the associated element.
* @param surf     Key buffer.
* @param sele     Value buffer.
* @param surfmap  The map we insert into.
*/
void insert_surf_tri(mt_idx_t n1, mt_idx_t n2, mt_idx_t n3, size_t eidx,
                     mt_triple<mt_idx_t> & surf, tri_sele & sele,
                     MT_MAP<mt_triple<mt_idx_t>, tri_sele> & surfmap);


/**
* @brief Insert the quad formed by indices (n1, n2, n3, n4)
* into a map.
*
* @param n1       First quad index.
* @param n2       Second quad index.
* @param n3       Third quad index.
* @param n4       Forth quad index.
* @param eidx     Index of the associated element.
* @param surf     Key buffer.
* @param sele     Value buffer.
* @param surfmap  The map we insert into.
*/
void insert_surf_quad(mt_idx_t n1, mt_idx_t n2, mt_idx_t n3, mt_idx_t n4, size_t eidx,
                      mt_quadruple<mt_idx_t> & surf,
                      quad_sele & sele,
                      MT_MAP<mt_quadruple<mt_idx_t>, quad_sele> & surfmap);

/// Insert all surfaces of a tetrahedral element into a map.
void insert_surf_tet(const mt_idx_t* nod,
                     const size_t eidx,
                     MT_MAP<mt_triple<mt_idx_t>, tri_sele> & surfmap);

/// Insert all surfaces of a pyramid element into a map.
void insert_surf_pyr(const mt_idx_t* nod, const size_t eidx,
                     MT_MAP<mt_triple<mt_idx_t>, tri_sele> & surfmap,
                     MT_MAP<mt_quadruple<mt_idx_t>, quad_sele> & qsurfmap);

/// Insert all surfaces of a prism element into a map.
void insert_surf_pri(const mt_idx_t* nod, const size_t eidx,
                     MT_MAP<mt_triple<mt_idx_t>, tri_sele> & surfmap,
                     MT_MAP<mt_quadruple<mt_idx_t>, quad_sele> & qsurfmap);

/// Insert all surfaces of a hexahedral element into a map.
void insert_surf_hex(const mt_idx_t* nod, const size_t eidx,
                     MT_MAP<mt_quadruple<mt_idx_t>, quad_sele> & surfmap);

/**
* @brief Compute surface of tag regions
*
* This version does not require the user to compute a meshgraph first when restricting
* the mesh to a subregion.
*
* @param [in]  mesh       The mesh.
* @param [in]  tags       The selected tag regions
* @param [out] tri_surf   Tri surface elements
* @param [out] quad_surf  Quad surface elements
*/
void compute_surface(const mt_meshdata & mesh,
                     const MT_USET<mt_tag_t> & tags,
                     MT_MAP<mt_triple<mt_idx_t>, tri_sele>     & tri_surf,
                     MT_MAP<mt_quadruple<mt_idx_t>, quad_sele> & quad_surf);

/**
* @brief Compute surface of tag regions
*
* This version does not require the user to compute a meshgraph first when restricting
* the mesh to a subregion.
*
* @param [in]  mesh      The mesh.
* @param [in]  tags      The selected tag regions
* @param [out] surfmesh  The surface mesh
* @param [in]  full_mesh Whether to generate the full mesh data (cnt, type)
*/
void compute_surface(const mt_meshdata & mesh,
                     const MT_USET<mt_tag_t> & tags,
                     mt_meshdata & surfmesh,
                     const bool full_mesh = true);


void compute_surface(const mt_meshdata & mesh,
                     const mt_vector<mt_idx_t> & elems,
                     MT_MAP<mt_triple<mt_idx_t>, tri_sele>     & tri_surf,
                     MT_MAP<mt_quadruple<mt_idx_t>, quad_sele> & quad_surf);

inline void compute_surface(const mt_meshdata & mesh,
                     MT_MAP<mt_triple<mt_idx_t>, tri_sele>     & tri_surf,
                     MT_MAP<mt_quadruple<mt_idx_t>, quad_sele> & quad_surf)
{
  mt_vector<mt_idx_t> elem(mesh.e2n_cnt.size());
  range<mt_idx_t>(0, mesh.e2n_cnt.size(), elem);

  compute_surface(mesh, elem, tri_surf, quad_surf);
}

void compute_surface(const mt_meshdata & mesh,
                     const mt_vector<mt_idx_t> & elems,
                     mt_meshdata & surfmesh,
                     const bool full_mesh,
                     mt_vector<mt_idx_t>* elem_orig = NULL);

void compute_all_surfaces_parallel(const mt_meshdata & mesh, tag_surf_map & tagsurf);
void update_surfaces_parallel(const mt_meshdata & mesh, const MT_USET<mt_tag_t> & tags, tag_surf_map & tagsurf);

void compute_surface(const mt_meshdata & mesh, const tag_surf_map & tagsurf,
                     const MT_USET<mt_tag_t> & tags, mt_meshdata & surface,
                     mt_vector<mt_idx_t> & elem_orig);

inline void compute_surface(const mt_meshdata & mesh, mt_meshdata & surfmesh,
                            const bool full_mesh, mt_vector<mt_idx_t>* elem_orig = NULL)
{
  mt_vector<mt_idx_t> elem(mesh.e2n_cnt.size());
  range<mt_idx_t>(0, mesh.e2n_cnt.size(), elem);

  compute_surface(mesh, elem, surfmesh, full_mesh, elem_orig);
}

/**
* @brief Convert a surface of triangle and quad maps to a triangle surface.
*
* @param tri         [in]  Triangle map.
* @param quad        [in]  Quad map.
* @param surf_con    [out] Vector of triangle indices.
* @param elem_origin [out] Vector with index of the associated volumetric element.
*/
void surfmap_to_vector(const MT_MAP<struct mt_triple<mt_idx_t>, struct tri_sele> & tri,
                       const MT_MAP<struct mt_quadruple<mt_idx_t>, struct quad_sele> & quad,
                       mt_vector<mt_idx_t> & surf_con,
                       mt_vector<mt_idx_t> & elem_origin);

/**
* @brief Convert a surface from tri / quad maps to mt_meshdata
*
* @param tri         [in]   Triangle map.
* @param quad        [in]   Quad map.
* @param surfmesh    [out]  The surface mesh.
* @param elem_origin [out]  The volumetric element index the surface element belongs to.
*/
void surfmap_to_surfmesh(const MT_MAP<struct mt_triple<mt_idx_t>, struct tri_sele> & tri,
                         const MT_MAP<struct mt_quadruple<mt_idx_t>, struct quad_sele> & quad,
                         mt_meshdata & surf_mesh,
                         mt_vector<mt_idx_t> & elem_origin);

/**
* @brief Add triangle surface elements from vector into a surface map.
*
* @param surf_con  Connectivity vector.
* @param trimap    Surface map.
*/
void vector_to_surfmap(const mt_vector<mt_idx_t> & surf_con,
                       MT_MAP<struct mt_triple<mt_idx_t>, struct tri_sele> & trimap);

/**
* @brief Add surface mesh elements into surface elements maps.
*
* @param surfmesh  The surface mesh.
* @param eidx      Vector with mesh element indices associated to each surface.
* @param tri       The trianlge elements map.
* @param quad      The quad elements map.
*/
void surfmesh_to_surfmap(const mt_meshdata & surfmesh,
                         const mt_vector<mt_idx_t> & eidx,
                         MT_MAP<mt_triple<mt_idx_t>, tri_sele> & tri,
                         MT_MAP<mt_quadruple<mt_idx_t>, quad_sele> & quad);

/**
* @brief Compute a set difference for two surfaces.
*
* The result will be stored in the lhs surface.
*
* @param lhs_tri    Triangle surface elements of lhs.
* @param lhs_quad   Quad surface elements of lhs.
* @param rhs_tri    Triangle surface elements of rhs.
* @param rhs_quad   Quad surface elements of rhs.
*/
void surface_difference(MT_MAP<mt_triple<mt_idx_t>, tri_sele> & lhs_tri,
                        MT_MAP<mt_quadruple<mt_idx_t>, quad_sele> & lhs_quad,
                        const MT_MAP<mt_triple<mt_idx_t>, tri_sele> & rhs_tri,
                        const MT_MAP<mt_quadruple<mt_idx_t>, quad_sele> & rhs_quad);

/**
* @brief Compute a set intersection for two surfaces.
*
* The result will be stored in the lhs surface.
*
* @param lhs_tri    Triangle surface elements of lhs.
* @param lhs_quad   Quad surface elements of lhs.
* @param rhs_tri    Triangle surface elements of rhs.
* @param rhs_quad   Quad surface elements of rhs.
*/
void surface_intersection(MT_MAP<mt_triple<mt_idx_t>, tri_sele> & lhs_tri,
                          MT_MAP<mt_quadruple<mt_idx_t>, quad_sele> & lhs_quad,
                          const MT_MAP<mt_triple<mt_idx_t>, tri_sele> & rhs_tri,
                          const MT_MAP<mt_quadruple<mt_idx_t>, quad_sele> & rhs_quad);

/**
* @brief Compute a set union for two surfaces.
*
* The result will be stored in the lhs surface.
*
* @param lhs_tri    Triangle surface elements of lhs.
* @param lhs_quad   Quad surface elements of lhs.
* @param rhs_tri    Triangle surface elements of rhs.
* @param rhs_quad   Quad surface elements of rhs.
*/
void surface_union(MT_MAP<mt_triple<mt_idx_t>, tri_sele> & lhs_tri,
                   MT_MAP<mt_quadruple<mt_idx_t>, quad_sele> & lhs_quad,
                   const MT_MAP<mt_triple<mt_idx_t>, tri_sele> & rhs_tri,
                   const MT_MAP<mt_quadruple<mt_idx_t>, quad_sele> & rhs_quad);

/**
* @brief Generate the data necessary for writing the *.nbc file of a surface.
*
* @param [in]  mesh         The mesh.
* @param [in]  surf         The surface mesh.
* @param [in]  elem_orig    The mesh elements associated with the surface elements.
* @param [out] nbc          The nbc data.
*/
void generate_nbc_data(const mt_meshdata & mesh,
                       const mt_meshdata & surf,
                       const mt_vector<mt_idx_t> & elem_orig,
                       struct nbc_data & nbc);

/**
* @brief Map data from global indexing to local indexing.
*
*  We map data, which consists of indices in the range (glob[0], ..., glob[N])
*  to the range (0, ..., N). Note that glob needs to be sorted in ascending order.
*
* @param glob   The global indices, indexing the local domain data.
* @param data   The data we need to map from global to local indexing.
*/
template<class T>
void map_glob2loc(const mt_vector<T> & glob, mt_vector<T> & data)
{
  size_t dsize = data.size(), gsize = glob.size();

  mt_vector<T> perm(dsize);
  for(size_t i=0; i<dsize; i++) perm[i] = i;

  binary_sort_copy(data, perm);
  for (size_t i=0, idx=0; i<dsize; i++)
  {
    while (glob[idx] < data[i] && idx < (gsize-1)) idx++;
    data[i] = idx;
  }
  binary_sort_copy(perm, data);
}

/**
* @brief Map data from global indexing to local indexing.
*
*  We map data, which consists of indices in the range (glob[0], ..., glob[N])
*  to the range (0, ..., N). Non-existent indices will be removed.
*
* @param glob   A map between the global indices and the local ones.
* @param data   The data we need to map from global to local indexing.
*/
template<class T>
void map_glob2loc(const MT_MAP<T,T> & g2l, mt_vector<T> & data)
{
  size_t widx=0;
  for(size_t ridx=0; ridx<data.size(); ridx++)
  {
    auto it = g2l.find(data[ridx]);
    if(it != g2l.end())
      data[widx++] = it->second;
  }
  data.resize(widx);
}

/**
* @brief Map data from global indexing to local indexing.
*
*  We map data, which consists of indices in the range (glob[0], ..., glob[N])
*  to the range (0, ..., N). Note that glob needs to be sorted in ascending order.
*
* @param glob   The global indices, indexing the local domain data.
* @param data   The data we need to map from global to local indexing.
*/
template<class C, class T>
void map_connectivity_glob2loc(const MT_MAP<T,T> & g2l,
                               mt_vector<C> & e2n_cnt,
                               mt_vector<T> & e2n_con,
                               mt_vector<bool> & removed)
{
  T widx=0, widx_con=0, ridx_con=0;
  removed.assign(e2n_cnt.size(), false);

  for(size_t ridx=0; ridx < e2n_cnt.size(); ridx++)
  {
    C esize = e2n_cnt[ridx];
    bool good = true;

    for(C i=0; i<esize; i++)
    {
      auto it = g2l.find(e2n_con[ridx_con + i]);
      if(it != g2l.end())
        e2n_con[widx_con + i] = it->second;
      else {
        good = false;
        break;
      }
    }
    ridx_con += esize;

    if(good)
    {
      e2n_cnt[widx] = esize;
      widx++;
      widx_con += esize;
    }
    else
      removed[ridx] = true;
  }
  e2n_cnt.resize(widx);
  e2n_con.resize(widx_con);
}

/**
* @brief Compute union of the surfaces defined by the given list of tag sets.
*
* Surface "i" is computed for the submesh formed by all tags in set tags[i].
*
* @param [in]  mesh       The mesh.
* @param [in]  tags       A sequence of tag sets.
* @param [out] surfmesh   The unified surface mesh.
* @param [out] vtx_set    A set of all vertices in the provided submeshes.
*/
void unified_surface_from_tags(const mt_meshdata & mesh,
                               const mt_vector<MT_USET<mt_tag_t>> & tags,
                               mt_meshdata & surfmesh,
                               MT_USET<mt_idx_t> * vtx_set);

/**
* @brief Compute a unified surface definition from a list of .surf files.
*
* @param list        The list of surface files.
* @param delimiter   The list entry delimiter.
* @param surfmesh    The unified surface.
*/
void unified_surface_from_list(const std::string & list,
                               const char delimiter,
                               mt_meshdata & surfmesh);

/**
* @brief Resize the element data vectors of a mesh.
*
* @param [out] mesh       The mesh to resize.
* @param [in]  size_elem  The new number of elements.
* @param [in]  size_con   The new number of connectivity entries.
*/
void mesh_resize_elemdata(mt_meshdata & mesh, size_t size_elem, size_t size_con);

/**
* @brief Convert surface definition into a full mesh.
*
* @param [in]  mesh       The full mesh.
* @param [in]  surf_con   The surface definition.
* @param [in]  nbc        Neumann BC data.
* @param [out] surfmesh   The surface mesh.
*/
void surface_to_mesh(const mt_meshdata & mesh,
                     const mt_vector<mt_idx_t> & surf_con,
                     const nbc_data & nbc,
                     mt_meshdata & surfmesh);


/**
* @brief Remove faces from a surface connectivity if they contain certain nodes.
*
*/
void remove_nodes_from_surf(const MT_USET<mt_idx_t> & nodes,
                            mt_vector<mt_cnt_t> & surf_cnt,
                            mt_vector<mt_idx_t> & surf_con,
                            struct nbc_data & nbc);

void remove_nodes_from_surf(const MT_USET<mt_idx_t> & nodes,
                            mt_vector<mt_cnt_t> & surf_cnt,
                            mt_vector<mt_idx_t> & surf_con);

/// remove faces. similar to restrict meshdata, but beter suited for lightweight surfs
void remove_elems_from_surf(const mt_vector<bool> & keep,
                            mt_vector<mt_cnt_t> & surf_cnt,
                            mt_vector<mt_idx_t> & surf_con,
                            struct nbc_data & nbc);

/**
* @brief Insert nbc data and mesh vertices into a surface mesh
*/
void nbc_data_into_surface(const mt_meshdata & mesh, const nbc_data & nbc,
                           mt_meshdata & surfmesh, bool flip = true);
/**
* @brief Convert element based data to node based data
*
* @tparam V    Data value type. Should support arithmetic operations.
* @param [in]  mesh  The mesh.
* @param [in]  edat  Element data.
* @param [out] ndat  Node data.
*/
template<class V>
void elemData_to_nodeData(const mt_meshdata & mesh,
                          const mt_vector<V> & edat,
                          mt_vector<V> & ndat)
{
  assert(edat.size() == mesh.e2n_cnt.size());
  assert(mesh.n2e_cnt.size() > 0);

  bool vol_weight = mesh.xyz.size() > 0;
  size_t nnodes = mesh.n2e_cnt.size();

  if(mesh.xyz.size())
    ndat.resize(mesh.xyz.size()/3, V());
  else if(ndat.size() < nnodes)
    ndat.resize(nnodes, V());

  #ifdef OPENMP
  #pragma omp parallel for schedule(guided)
  #endif
  for(size_t nidx = 0; nidx < nnodes; nidx++)
  {
    if(mesh.n2e_cnt[nidx]) {
      V avrg = V();
      mt_real total_weight = 0.0;

      mt_idx_t start = mesh.n2e_dsp[nidx], stop = start + mesh.n2e_cnt[nidx];
      for(mt_idx_t i = start; i < stop; i++)
      {
        mt_idx_t eidx = mesh.n2e_con[i];
        const mt_idx_t* con = mesh.e2n_con.data() + mesh.e2n_dsp[eidx];

        mt_real cur_weight = vol_weight ? volume(mesh.etype[eidx], con, mesh.xyz.data()) : 1.0;
        total_weight += cur_weight;
        avrg += edat[eidx] * cur_weight;
      }

      if(total_weight == 0.0)
        printf("%s warning: zero weight!\n", __func__);
      else
        ndat[nidx] = avrg / total_weight;
    }
  }
}

/**
* @brief Convert node based data to element based data
*
* @tparam V    Data value type. Should support arithmetic operations.
* @param [in]  mesh  The mesh.
* @param [in]  ndat  Node data.
* @param [out] edat  Element data.
*/
template<class V>
void nodeData_to_elemData(const mt_meshdata & mesh,
                          const mt_vector<V> & ndat,
                          mt_vector<V> & edat)
{
  size_t nelem = mesh.e2n_cnt.size();
  edat.resize(nelem);

  #ifdef OPENMP
  #pragma omp parallel for schedule(guided)
  #endif
  for(size_t eidx = 0; eidx < nelem; eidx++)
  {
    V avrg = V();
    mt_idx_t start = mesh.e2n_dsp[eidx], stop = start + mesh.e2n_cnt[eidx];
    for(mt_idx_t i = start; i < stop; i++)
    {
      mt_idx_t nidx = mesh.e2n_con[i];
      avrg += ndat[nidx];
    }
    edat[eidx] = avrg / mt_real(mesh.e2n_cnt[eidx]);
  }
}

/**
* @brief Convert node based data to element based data. Instead of averaging, we take the value with the highest
*        frequency.
*
* @tparam V    Data value type, should be an integer-type.
* @param [in]  mesh  The mesh.
* @param [in]  ndat  Node data.
* @param [out] edat  Element data.
*/
template<class V>
void nodeData_to_elemData_freq(const mt_meshdata & mesh,
                               const mt_vector<V> & ndat,
                               mt_vector<V> & edat)
{
  size_t nelem = mesh.e2n_cnt.size();
  edat.resize(nelem);

  #ifdef OPENMP
  #pragma omp parallel
  #endif
  {
  MT_MAP<int, size_t> counts;

    #ifdef OPENMP
    #pragma omp for schedule(guided)
    #endif
    for(size_t eidx = 0; eidx < nelem; eidx++)
    {
      counts.clear();
      mt_idx_t start = mesh.e2n_dsp[eidx], stop = start + mesh.e2n_cnt[eidx];
      for(mt_idx_t i = start; i < stop; i++)
      {
        mt_idx_t nidx = mesh.e2n_con[i];
        counts[int(ndat[nidx])] += 1;
      }

      size_t max_count = 0;
      V      max_val   = V();
      for(auto & it : counts) {
        if(max_count < it.second) {
          max_count = it.second;
          max_val   = it.first;
        }
      }

      edat[eidx] = max_val;
    }
  }
}
/// compute surface normals in each element
void compute_element_surface_normals(const mt_meshdata & surfmesh,
                                     const mt_vector<mt_real> & xyz,
                                     mt_vector<mt_real> & snrml);

/// compute surface normals in each node
void compute_nodal_surface_normals(const mt_meshdata & surfmesh,
                                   const mt_vector<mt_real> & xyz,
                                   mt_vector<mt_real> & snrml,
                                   const mt_real sign_scale = 1.0);

void update_nodal_surface_normals(const mt_meshdata & surfmesh,
                                  const mt_vector<mt_real> & xyz,
                                  const MT_USET<mt_idx_t> & nod,
                                  mt_vector<mt_real> & snrml,
                                  const mt_real sign_scale = 1.0);

/**
* @brief Make sure the surface normals have a desired orientation
*
* @param surf     the surface connectivity
* @param inward   true fore inward facing normals, false for outware facing
*
* @return whether a normals flip was needed
*/
bool apply_normal_orientation(mt_meshdata & surf, const bool inward);

bool apply_normal_orientation(mt_meshdata & surf, kdtree & surf_tree, mt_vector<mt_idx_t> & eidx, const bool inward);

/// remove n2n edges connecting nodal surfaces with a negative scalar product
void remove_bad_surface_edges(const mt_meshdata & mesh,
                              mt_meshdata & surfmesh);

/**
* @brief Convert the data of a PS struct into a mesh.
*
* @param [in]  ps     PS data struct
* @param [out] psmesh PS mesh.
*/
void psdata_to_mesh(const mt_psdata & ps, mt_meshdata & psmesh);


/**
* @brief Generate a unordered map from a coordinate mt_triple to a coordinate index
*
* @param [in]  mesh           The mesh holding the coordinates.
* @param [out] cmap           The unordered map.
* @param [out] scale_factor   The scale factor used when converting the floating point coords to integers.
*/
void generate_coord_map(mt_meshdata & mesh,
                        MT_MAP<mt_triple<mt_idx_t>,mt_idx_t> & cmap,
                        int & scale_factor);

/**
* @brief Unify two meshes into one (mesh1).
*
* @param [in, out] mesh1 First mesh. Holds mesh union after completition.
* @param [in] mesh2 Second mesh.
* @param [in] error_on_empty_intf  Whether an empty interface should yield an error abort.
*/
void mesh_union(mt_meshdata & mesh1, const mt_meshdata & mesh2,
                const bool error_on_empty_intf);


/**
* @brief Function evaluating if nodes are inside a closed surface or not.
*
* @param mesh     The mesh with the nodes we evaluate.
* @param tree     The kdtree of the surface.
* @param in_surf  The vector wheather a node is inside.
*/
void nodes_in_surface(const mt_meshdata & mesh,
                      const kdtree & tree,
                      mt_vector<bool> & in_surf);

void elems_in_surface(const mt_meshdata & mesh,
                      const kdtree & surface,
                      mt_vector<mt_idx_t> & elems);

void elems_on_surface(const mt_meshdata & mesh,
                      const kdtree & surface,
                      const mt_real eps,
                      mt_vector<mt_idx_t> & elems);

/**
* @brief For a given surface kdtree, retag the elements who are determined "inside".
*        There are two criteria for when an element is inside.
*
* @param mesh             The mesh we sample the elements in.
* @param tree             The kdtree of the surface.
* @param newtag           The new tag we set the inside elements to.
* @param sampling_type    The sampling type index. 0 = node based sampling, 1 = element center based sampling.
* @param grow             Grow the sampled region when inserting.
*/
void sample_elem_tags(mt_meshdata & mesh, mt_mask & tag_found, const kdtree & tree, const mt_idx_t newtag,
                     const int sampling_type, const int grow = 0);

/**
 * @brief Add an element into a meshdata struct
 * 
 * @param mesh The meshdata struct we add into.
 * @param type The element type to add.
 * @param con  Pointer to the connectivity to add from.
 * @param tag  The element tag to add.
 */
void mesh_add_elem(mt_meshdata & mesh, const elem_t type, const mt_idx_t* con, const mt_tag_t tag);

void mesh_add_elem(mt_meshdata & mesh, const elem_t type, const mt_idx_t* con, const mt_tag_t tag, short nfib, const mt_fib_t* fib);

/**
* @brief Check wheter the elements in a dataset are in a given interval.
*
* Each element must be equal or greater than the interval start and smaller
* than the interval end.
*
* @tparam InputIterator  Iterator type.
* @tparam T              Type that the iterator dereference to.
* @param s               Start of dataset.
* @param e               End of dataset.
* @param i_start         Start of interval.
* @param i_end           End of interval.
*
* @return Wether the elements are in the given interal.
*/
template<typename InputIterator, typename T>
bool set_in_interval(InputIterator s, InputIterator e, const T i_start, const T i_end)
{
  bool in_interv = true;

  while(s != e) {
    T val = *s;
    if(val < i_start || val >= i_end) {
      in_interv = false;
      break;
    }

    ++s;
  }

  return in_interv;
}

/**
* @brief Read as much surface information as possible from disk. Try to recover the rest.
*
* @param mesh       The mesh associated to the surface. Needed for recovery.
* @param surf       The surface meshdata struct.
* @param eidx       The indices of the volumetric elements associated to the surface elements.
* @param basefile   The path to the surface data on disk.
*/
void read_surf_info(const mt_meshdata & mesh, mt_meshdata & surf, mt_vector<mt_idx_t> & eidx,
                    std::string basefile);


/**
* @brief Generate surface maps from a list of strings that contain either tags, or surface files,
*        or the _all_ keyword.
*
* @param mesh      the mesh we extract the surface of
* @param list      list of strings we evaluate
* @param tri_surf  where we store the triangles
* @param quad_surf where we store the quads
*/
void unified_surf_from_mixed_list(const mt_meshdata & mesh,
                                  const mt_vector<std::string> & list,
                                  MT_MAP<mt_triple<mt_idx_t>,    tri_sele>  & tri_surf,
                                  MT_MAP<mt_quadruple<mt_idx_t>, quad_sele> & quad_surf);

/**
* @brief Write .surf, .vtx and .neubc files for a surface
*
* @param surface  Surface meshdata
* @param nbc      NBC data struct pointer, NULL if .neubc file should not be written
* @param numele   Number of elements in overall mesh
* @param npts     Number of points in overall mesh
* @param writeNBC Whether to write .neubc file.
* @param bname    Base name for all output files.
*/
void write_surf_info(const mt_meshdata & surface, const nbc_data* nbc,
                     const size_t numele, const size_t npts, std::string bname);


/**
* @brief Compute re-indexing based on the cuthill mcgee algorithm
*
* @param [in] n2n_cnt   node-to-node graph row counts
* @param [in] n2n_dsp   node-to-node graph row displacements
* @param [in] n2n_con   node-to-node graph edges
* @param [in] reverse   True for reverse algorithm false for forward one
* @param [out] old2new  old index to new index mapping
*/
void reindex_cuthill_mckee(const mt_vector<mt_cnt_t> & n2n_cnt,
                           const mt_vector<mt_idx_t> & n2n_dsp,
                           const mt_vector<mt_idx_t> & n2n_con,
                           const bool reverse,
                           MT_MAP<mt_idx_t, mt_idx_t> & old2new);

/// apply splitlist on mesh
void apply_split(mt_meshdata & mesh, const mt_vector<split_item> & splitlist);

/// undo a split
void undo_split(mt_meshdata & mesh, const mt_vector<split_item> & splitlist);

/// find a point inside a closed surface
bool point_inside_surface(const mt_meshdata & surf, const kdtree & tree, vec3r & found_point);

/**
* @brief Convert connectivity ordering from CARP to VTK ordering
*
* @param [in]  mesh     The mesh. Connectivity has CARP ordering.
* @param [out] e2n_con  The connectivity in VTK ordering.
*/
void perm_con_carp_to_vtk(const mt_meshdata & mesh, mt_vector<mt_idx_t> & e2n_con);
/**
* @brief Convert connectivity ordering from VTK to CARP ordering
*
* @param [out]  mesh     The mesh. Connectivity has CARP ordering.
* @param [in]   e2n_con  The connectivity in VTK ordering.
*/
void perm_con_vtk_to_carp(mt_meshdata & mesh, const mt_vector<mt_idx_t> & e2n_con);

/**
 * @brief Find the element index a given xyz point is located in.
 * 
 * @param [in]  pnt                The input xyz point.
 * @param [in]  closest_node_idx   The vertex index to start looking for the xyz coord.
 * @param [in]  mesh               The mesh we use for finding the xyz coords. Only the connectivity is used.
 * @param [in]  max_it             Number of maximum search iterations
 * 
 * @return mt_idx_t  the element index and the barycentric distance to the element
 */
mt_mixed_tuple<mt_idx_t, mt_real> 
find_elem_idx(const vec3r &pnt, const mt_idx_t closest_node_idx, 
              const mt_meshdata &mesh, std::size_t max_it);

/**
 * @brief Find the xyz that best matches a uvc triple when doing linear volumetric interpolation on a mesh.
 * 
 * @param [out]  pnt                The output xyz coords.
 * @param [in]   uvc                The input UVC coords (rho, phi, z)
 * @param [in]   closest_node_idx   The vertex index to start looking for the xyz coord.
 * @param [in]   ven_idx            -1 for LV, 1 for RV.
 * @param [in]   mesh               The mesh we use for finding the xyz coords. Only the connectivity is used.
 * @param [in]   UVC_data           The UVC data defined on the mesh.
 * @param [in]   CART_data          The cartesian coords defined on the mesh.
 * @param [in]   map                Nodal tuples. value 1 is ventricle idx (see ven_idx above), value 2 is an index into UVC_data / CART_data.
 * @param [in]   max_it 
 * 
 * @return mt_idx_t 
 */
mt_idx_t find_xyz_coord(vec3r &pnt, const vec3r &uvc, const mt_idx_t closest_node_idx, const short ven_idx,
                      const mt_meshdata &mesh, const mt_vector<vec3r> &UVC_data,
                      const mt_vector<mt_real> &CART_data, const mt_mask & is_LV,
                      std::size_t max_it);

/**
 * @brief Get the xyz coordinates from UVC coordinates
 * 
 * @param [in]  mesh        The mesh where the UVCs are defined on.
 * @param [in]  UVCs_vec    The UVCs (rho, phi, z)
 * @param [in]  is_LV       Whether a UVC triple is defined on the LV or not. 
 * @param [in]  coords      The UVC coords we do the lookup for.
 * @param [in]  coord_is_lv Whether coord[i] is on the LV
 * @param [out] vtxlist     Indices of the vertices closest to the mapped UVC coord
 * @param [out] ptslist     Interpolated xyz for the coords
 */
void get_xyz_from_UVCs(const mt_meshdata & mesh,
                       const mt_vector<vec3r> & UVCs_vec, const mt_mask & is_LV,
                       const mt_vector<mt_real> & coords, const mt_vector<bool> & coord_is_lv,
                       mt_vector<mt_idx_t> & vtxlist, mt_vector<vec3r> & ptslist, size_t max_it=10);

mt_idx_t find_xyz_coord(vec3r &pnt, const vec3r &ucc, const mt_idx_t closest_node_idx, const short ven_idx,
                       const mt_meshdata &mesh, const mt_vector<vec3r> &UCCs_vec,
                       const mt_vector<int8_t> &UCCs_chmbr, const mt_vector<mt_real> &xyz,
                       std::size_t max_it);

void get_xyz_from_UCCs(const mt_meshdata & mesh,
                       const mt_vector<vec3r> & UCCs_vec, const mt_vector<int8_t> & UCCs_chmbr,
                       const mt_vector<mt_real> & coords, const mt_vector<int8_t> & coord_chmbr,
                       mt_vector<mt_idx_t> & vtxlist, mt_vector<vec3r> & ptslist, size_t max_it=10);

bool parse_splitlist_operation(std::string op,
                               mt_vector<MT_USET<mt_tag_t>> & tagsA,
                               mt_vector<MT_USET<mt_tag_t>> & tagsB);

bool check_split_operations(mt_vector<MT_USET<mt_tag_t>> & tagsA,
                            mt_vector<MT_USET<mt_tag_t>> & tagsB, std::string* errstr = NULL);

void generate_splitlist(const mt_meshdata & mesh,
                        const mt_vector<MT_USET<mt_tag_t>> & tagsA,
                        const mt_vector<MT_USET<mt_tag_t>> & tagsB,
                        const mt_idx_t start_idx,
                        mt_vector<split_item> & list);

/**
* @brief set number of fiber directions
*
* @param nele          number of elements lon is associated to
* @param lon           the fibers, as stored in a mt_meshdata struct
* @param new_num_fib   the new number of fiber directions. either 1 or 2
*/
void set_num_fibers(const size_t nele, mt_vector<mt_fib_t> & lon, short new_num_fib);

#endif

