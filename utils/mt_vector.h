/**
* @file mt_vector.h
* @brief Vector class similar to std::vector.
*
* This implementation was inspired by the toolbox_vector implementation by Manfred Liebmann.
*
* @author Aurel Neic
* @version
* @date 2016-12-13
*/

#ifndef _TOOLBOX_VECTOR
#define _TOOLBOX_VECTOR

#include <assert.h>
#include <iterator>
#include <algorithm>
#include <stdio.h>
#include <string>


/*
   We live in a world where its is not garuanteed that fwrite can actually write files
   larger than 4 Gig. As such, this wrapper breakes larger writes into one gig chunks.
*/
template<class T> inline
size_t mt_fwrite(T* buff, size_t num, FILE* fd)
{
  const size_t four_gigs = 4LL * 1024 * 1024 * 1024;
  const size_t one_gig   = 1024LL * 1024 * 1024;
  const size_t file_size = sizeof(T) * num;
  size_t written = 0;

  if(file_size > four_gigs) {
    size_t  num_chunks = (file_size + one_gig - 1) / one_gig;
    T* write_buff = buff, *end_buff = buff + num;

    size_t num_write = one_gig / sizeof(T);
    for(size_t i=0; i<num_chunks-1; i++) {
      written += fwrite(write_buff, sizeof(T), num_write, fd);
      write_buff += num_write;
    }

    num_write = end_buff - write_buff;
    written += fwrite(write_buff, sizeof(T), num_write, fd);

    assert(written == num);
  } else {
    written += fwrite(buff, sizeof(T), num, fd);
  }

  return written;
}

template <class T>
class mt_vector
{
public:
  typedef T value_type;

public:
  mt_vector(): _data(NULL), _capacity(0), _size(0)
  {}

  mt_vector(size_t n): _data(NULL), _capacity(0), _size(0)
  {
    this->resize(n);
  }

  mt_vector(size_t n, const T val): _data(NULL), _capacity(0), _size(0)
  {
    this->assign(n, val);
  }

  mt_vector(const mt_vector<T> &vec): _data(NULL), _capacity(0), _size(0)
  {
    this->assign(vec.begin(), vec.end());
  }

  mt_vector(mt_vector<T> &&vec): _data(NULL), _capacity(0), _size(0)
  {
    vec.swap(*this);
  }

  mt_vector(std::initializer_list<T> c): _data(NULL), _capacity(0), _size(0)
  {
    this->assign(c.begin(), c.end());
  }

  virtual ~mt_vector()
  {
    clear();
  }

  const T& operator[](size_t i) const
  {
    return _data[i];
  }

  T& operator[](size_t i)
  {
    return _data[i];
  }

  void operator= (const mt_vector<T> & vec)
  {
    this->assign(vec.begin(), vec.end());
  }

  void operator= (mt_vector<T> && vec)
  {
    clear();
    vec.swap(*this);
  }

  T* data()
  {
    return _data;
  }

  const T* data() const
  {
    return _data;
  }

  bool check_access(long long idx, bool verbose = true)
  {
    bool good_access = idx >= 0 && idx < _size;

    if(!good_access && verbose) {
      printf("bad access: tried vec[%lld], for vector of size %lld\n", idx, (long long)_size);
    }

    return good_access;
  }

  inline size_t size() const
  {
    return _size;
  }

  inline size_t capacity() const
  {
    return _capacity;
  }

  void swap(mt_vector<T> & vec)
  {
    std::swap(_data, vec._data);
    std::swap(_size, vec._size);
    std::swap(_capacity, vec._capacity);
  }

  const T* begin() const
  {
    return _data;
  }

  T* begin()
  {
    return _data;
  }

  const T* end() const
  {
    return _data + _size;
  }

  T* end()
  {
    return _data + _size;
  }

  T& front()
  {
    assert(_size > 0);
    return _data[0];
  }

  const T& front() const
  {
    assert(_size > 0);
    return _data[0];
  }

  T& back()
  {
    return _data[_size - 1];
  }

  const T& back() const
  {
    return _data[_size - 1];
  }

  void erase(size_t idx) {
    if(idx < _size) {
      _size--;
      for(size_t i = idx; i < _size; i++)
        _data[i] = _data[i+1];
    }
  }

  bool find(const T & val) const {
    size_t tmp = 0;
    return find(val, tmp);
  }

  bool find(const T & val, size_t & idx) const {
    for(idx = 0; idx < _size; idx++) {
      if(_data[idx] == val) {
        return true;
      }
    }
    return false;
  }

  #if 0
  void assign(const T* s, const T* e) {
    size_t n = (e - s);
    if (_capacity < n) {
      delete [] _data;
      _data = new T[n];
      _capacity = n;
    }
    for (size_t i = 0; i < n; i++) _data[i] = s[i];
    _size = n;
  }
  #endif

  template<class InputIterator>
  void assign(InputIterator s, InputIterator e)
  {
    long int n = std::distance(s, e);
    assert(n >= 0);

    // realloc if necessary
    if ( ((long int)_capacity) < n) {
      delete [] _data;
      _data = new T[n];
      _capacity = n;
    }

    // copy data
    size_t idx = 0;
    while(s != e) {
      _data[idx++] = *s;
      ++s;
    }

    _size = n;
  }

  void assign(size_t n, T val = T()) {
    if (_capacity < n) {
      if(_capacity > 0) delete [] _data;
      _data = new T[n];
      _capacity = n;
    }
    for (size_t i = 0; i < n; i++) _data[i] = val;
    _size = n;
  }

  void assign(size_t n, T* array, bool del = true) {
    if (del) delete [] _data;
    _data = array;
    _size = n;
    _capacity = n;
  }

  void resize(size_t n)
  {
    if(_capacity < n)
    {
      if(_capacity > 0) {
        T* buf = _data;
        _data = new T[n];
        for (size_t i = 0; i < _size; i++) _data[i] = buf[i];
        delete [] buf;
      }
      else {
        _data = new T[n];
      }
      _capacity = n;
    }
    _size = n;
  }

  void resize(size_t n, const T val)
  {
    size_t oldsize = this->size();
    this->resize(n);

    if(n > oldsize) {
      T*     out = _data + oldsize;
      size_t num = n - oldsize;

      for (size_t i = 0; i < num; i++) out[i] = val;
    }
  }

  void reserve(size_t n)
  {
    size_t osize = _size;
    this->resize(n);
    this->resize(osize);
  }

  void reserve_and_clear(size_t n)
  {
    this->resize(n);
    this->resize(0);
  }

  void zero()
  {
    if(_size != 0) memset(_data, 0, _size * sizeof(T));
  }

  void shrink_to_fit()
  {
    if (_capacity > _size) {
      _capacity = _size;
      T* buf = _data;
      _data = new T[_size];
      for (size_t i = 0; i < _size; i++) _data[i] = buf[i];
      delete [] buf;
    }
  }

  void clear()
  {
    delete [] _data;
    _data = NULL;
    _size = _capacity = 0;
  }

  inline T pop_back() {
    if(_size == 0) return T();

    T ret = this->back();

    if(_size)
      _size--;
    return ret;
  }

  inline T pop_front() {
    if(_size == 0) return T();

    T ret = this->front();

    if(_size)
      _size--;

    for(size_t i = 0; i < _size; i++)
      _data[i] = _data[i+1];

    return ret;
  }

  inline bool empty() const {
    return _size == 0;
  }

  template<class InputIterator>
  void append(InputIterator s, InputIterator e)
  {
    long int n = std::distance(s, e);
    assert(n >= 0);

    if (n == 0)
      return;

    const size_t oldsize = _size;
    const size_t addsize = static_cast<size_t>(n);
    resize(oldsize + addsize);

    // copy data
    size_t idx = oldsize;
    while(s != e) {
      _data[idx++] = *s;
      ++s;
    }
  }

  template <class S>
  inline void append(const mt_vector<S>& other) {
    append(other.begin(), other.end());
  }

  T& push_back(const T & val)
  {
    if(_capacity > _size)
    {
      _data[_size] = val;
      _size++;
    }
    else {
      // if current capacity is not larger than size + 1 we reallocate, but keep size to
      // old value plus one.
      size_t newcap = _capacity > 0 ? _capacity * 2 : 1;
      size_t cursize = _size;
      this->resize(newcap);
      _data[cursize] = val;
      _size = cursize + 1;
    }

    return _data[_size-1];
  }

  T& push_front(const T & val)
  {
    if(_capacity == _size)
      this->reserve(_size*2);

    _size++;

    T br, bw = _data[0];
    for(size_t widx = 1; widx < _size; widx++) {
      br = _data[widx];
      _data[widx] = bw;
      bw = br;
    }
    _data[0] = val;

    return _data[0];
  }

  inline void insert(const T & val)
  {
    this->push_back(val);
  }

  inline void sort()
  {
    std::sort(this->begin(), this->end());
    unique_resize(*this);
  }

  T& push_back(T && val)
  {
    if(_capacity > _size)
    {
      _data[_size] = val;
      _size++;
    }
    else {
      // if current capacity is not larger than size + 1 we reallocate, but keep size to
      // old value plus one.
      size_t newcap = _capacity > 0 ? _capacity * 2 : 1;
      size_t cursize = _size;
      this->resize(newcap);
      _data[cursize] = val;
      _size = cursize + 1;
    }

    return _data[_size-1];
  }

  template<class InputIterator>
  void push_back(InputIterator s, InputIterator e)
  {
    while(s != e) {
      this->push_back(T(*s));
      s++;
    }
  }

  void insert(size_t idx, const T & val)
  {
    if(_size == 0) {
      this->push_back(val);
    } else if(idx < _size) {
      mt_vector<T> buff;
      buff.assign(_data + idx, _data + _size);

      this->reserve(_size + 1);
      this->resize(idx);
      this->push_back(val);

      for(const T & v : buff)
        this->push_back(v);
    }
  }

  size_t write(FILE* fd) const
  {
    size_t items_written = fwrite(&_size, sizeof(size_t), 1, fd);
    if(items_written != 1) {
      fprintf(stderr, "%s error: Cannot write vector size to provided FD!\n", __func__);
      return items_written;
    }

    // items_written = fwrite(_data, sizeof(T), _size, fd);
    items_written = mt_fwrite<T>(_data, _size, fd);

    if(items_written != _size)
      fprintf(stderr, "%s error: Cannot write vector data to provided FD!\n", __func__);

    return items_written;
  }

  size_t read(FILE* fd)
  {
    size_t newsize, items_read = 0;
    items_read = fread(&newsize, sizeof(size_t), 1, fd);
    if(items_read != 1) {
      fprintf(stderr, "%s error: Cannot read vector size from provided FD!\n", __func__);
      return items_read;
    }

    this->resize(newsize);

    items_read = fread(_data, sizeof(T), newsize, fd);
    if(items_read != newsize)
      fprintf(stderr, "%s error: Cannot read vector data from provided FD!\n", __func__);

    return items_read;
  }

private:
  T* _data;
  size_t _capacity;
  size_t _size;
};

/**
* @brief Class for storing a set of positive indices in a similar way to std::set, but using a
*        bool vector to be much faster for insert, erase and count.
*
* insert, erase and count are very very fast, but the class offers no iterator access.
* A vector of currently inserted entries can be retrieved via entries().
*
*/
class mt_mask {

  private:
  mt_vector<bool> _mask;      ///< the boolean mask holding which indices have been inserted
  mutable mt_vector<size_t> _entries; ///< a buffer that gets populated when entries() is called
  size_t _num_ent;
  mutable bool   _ent_dirty;

  /// check whether _mask is big enough to hold the new index
  inline bool can_access(size_t idx) const {
    return (idx < _mask.size());
  }

  inline void update_entries() const
  {
    _entries.reserve_and_clear(_num_ent);

    if(_num_ent) {
      for(size_t i=0; i<_mask.size(); i++)
        if(_mask[i]) _entries.push_back(i);
    }

    _ent_dirty = false;
  }

  public:
  mt_mask() : _num_ent(0), _ent_dirty(true) {}

  /// resize _mask
  inline void resize(size_t s) {
    _mask.resize(s, false);
  }

  /// constructor with pre-allocation of _mask
  mt_mask(size_t s) : _num_ent(0), _ent_dirty(true) {
    resize(s);
  }

  /// clear the mask
  inline void clear() {
    _mask.assign(_mask.size(), false);
    _num_ent = 0;
    _ent_dirty = true;
  }

  /// insert an index
  inline void insert(size_t idx) {
    if(!can_access(idx)) resize(idx+1);

    if(_mask[idx] == false) {
      _mask[idx] = true;
      _ent_dirty = true;
      _num_ent++;
    }
  }

  /// insert an index range
  template<typename InputIterator>
  inline void insert(InputIterator s, InputIterator e) {
    while(s != e) {
      insert(size_t(*s));
      ++s;
    }
  }

  /// check whether an index is present
  inline int count(size_t idx) const
  {
    if(!can_access(idx)) return 0;
    else                 return int(_mask[idx]);
  }

  /// check whether an index is present
  inline size_t size() const
  {
    return _num_ent;
  }

  /// erase an index
  inline void erase(size_t idx) {
    if(can_access(idx) && _mask[idx]) {
      _mask[idx] = false;
      _num_ent--;
      _ent_dirty = true;
    }
  }

  /// erase an index range
  template<typename InputIterator>
  inline void erase(InputIterator s, InputIterator e) {
    while(s != e) {
      erase(size_t(*s));
      ++s;
    }
  }

  inline const size_t* begin() const
  {
    if(_ent_dirty) update_entries();
    return _entries.begin();
  }

  inline const size_t* end() const
  {
    if(_ent_dirty) update_entries();
    return _entries.end();
  }

  /// get a const reference to the inserted indices
  inline const mt_vector<size_t> & entries() const
  {
    if(_ent_dirty) update_entries();
    return _entries;
  }
};

template<class T>
class mt_buffer {
private:
  T*     _data;
  size_t _size;

public:
  mt_buffer(T* buff, size_t size) : _data(buff), _size(size)
  {}

  mt_buffer() : _data(NULL), _size(0)
  {}

  ~mt_buffer() {} // We do not want to dealloc

  const T& operator[](size_t i) const
  {
    return _data[i];
  }

  T& operator[](size_t i)
  {
    return _data[i];
  }

  T* data()
  {
    return _data;
  }

  const T* data() const
  {
    return _data;
  }

  inline size_t size() const
  {
    return _size;
  }

  const T* begin() const
  {
    return _data;
  }

  T* begin()
  {
    return _data;
  }

  const T* end() const
  {
    return _data + _size;
  }

  T* end()
  {
    return _data + _size;
  }

  T& front()
  {
    assert(_size > 0);
    return _data[0];
  }

  const T& front() const
  {
    assert(_size > 0);
    return _data[0];
  }

  T& back()
  {
    return _data[_size - 1];
  }

  const T& back() const
  {
    return _data[_size - 1];
  }

  bool find(const T & val) const {
    size_t tmp = 0;
    return find(val, tmp);
  }

  bool find(const T & val, size_t & idx) const {
    for(idx = 0; idx < _size; idx++) {
      if(_data[idx] == val) {
        return true;
      }
    }
    return false;
  }

  template<class InputIterator>
  void assign(InputIterator s, InputIterator e)
  {
    long int n = std::distance(s, e);
    if(n < 0) return;

    // copy data
    size_t end = _size < size_t(n) ? _size : n;
    size_t idx = 0;

    while(idx < end) {
      _data[idx++] = *s;
      ++s;
    }

    _size = end;
  }

  void clear() {
    _size = 0;
    _data = NULL;
  }
};

template<class V> inline
V sum(const mt_vector<V> & vec)
{
  V ret = V();
  for(const V & e : vec) ret += e;

  return ret;
}

template<class T> inline
void invert_perm(const mt_vector<T> & perm, mt_vector<T> & inv_perm)
{
  inv_perm.resize(perm.size());

  for(size_t i=0; i<perm.size(); i++)
    inv_perm[perm[i]] = i;
}

template<class T> inline
void range(const T s, const T e, mt_vector<T> & vec)
{
  if(e > s) {
    vec.resize(e - s);
    for(T i=s, j=0; i < e; i++, j++)
      vec[j] = i;
  }
}

/// Compute displacements from counts.
template<class T>
inline void dsp_from_cnt(const mt_vector<T> & cnt, mt_vector<T> & dsp)
{
  dsp.resize(cnt.size()+1);
  dsp[0] = 0;
  for(size_t i=0; i<cnt.size(); i++) dsp[i+1] = dsp[i] + cnt[i];
}

/// Compute counts from displacements.
template<class T>
inline void cnt_from_dsp(const mt_vector<T> & dsp, mt_vector<T> & cnt)
{
  cnt.resize(dsp.size() - 1);
  for(size_t i=0; i<dsp.size()-1; i++) cnt[i] = dsp[i+1] - dsp[i];
}

inline bool charvec_has_value(const mt_vector<char>& vec)
{
  return ((vec.size() > 0) && (strnlen(vec.data(), vec.size()) > 0));
}

inline std::string charvec_to_string(const mt_vector<char>& vec)
{
  std::string ret = "";
  if(vec.size()) ret = vec.data();
  return ret;
}

inline void charvec_from_string(const std::string & str, mt_vector<char>& vec)
{
  if(vec.size() < str.size())
    vec.resize(str.size()*2);
  snprintf(vec.data(), vec.size()-1, "%s", str.c_str());
}

#endif

