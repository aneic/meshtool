#ifndef GMSH_UTILS_H
#define GMSH_UTILS_H

void read_msh_file(mt_meshdata & mesh, std::string filename);

void read_msh2_binary(mt_meshdata & mesh, FILE* in, bool endian_swap);

#endif
