/**
* @file refine_igb_time.cpp
* @brief Refine an igb file in time by upsampling it with a spline interpolation
* @author Elias Karabelas
* @version
* @date 2017-10-28
*/


#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <sstream>

#include "spline.h"
#include "mt_utils.h"

struct igb_concat_options
{
  std::string idat;
  std::string odat;
};

void print_igb_concat_help()
 {
  fprintf(stderr, "igb_concat: concatenate igb files in time. space dimensions must match.\n");
  fprintf(stderr, "parameters:\n");
  fprintf(stderr, "%s<file1,file2,..>\t (input) list of igb paths\n", idat_par.c_str());
  fprintf(stderr, "%s<path>\t\t (output) path to output igb file\n", odat_par.c_str());
  fprintf(stderr, "\n");
}

int igb_concat_parse_options(int argc, char** argv, struct igb_concat_options & opts)
{
  if(argc < 3) {
    print_igb_concat_help();
    return 1;
  }

  // parse all enclose parameters -----------------------------------------------------------------
  for(int i=1; i<argc; i++){
    std::string param = reformat_param(argv, i, argc);
    bool match = false;

    if(!match) match = parse_param(param, idat_par, opts.idat);
    if(!match) match = parse_param(param, odat_par, opts.odat);

    if(!match) {
      std::cerr << "Error: Cannot parse parameter " << param << std::endl;
      return 3;
    }
  }


  // check if all relevant parameters have been set ---------------------------------------------------
  if( !(opts.idat.size() && opts.odat.size()) )
  {
    std::cerr << "Error: Insufficient parameters provided." << std::endl;
    print_igb_concat_help();
    return 3;
  }

  return 0;
}

int main(int argc, char** argv)
{
  struct igb_concat_options opts;
  igb_header igb_from;
  igb_header igb_to;

  int ret = igb_concat_parse_options(argc, argv, opts);
  if(ret != 0) return 1;

  mt_vector<std::string> inp_files;
  split_string(opts.idat, ',', inp_files);

  if(inp_files.size() == 0)
    return 1;

  init_igb_header(inp_files[0], igb_from);
  read_igb_header(igb_from);

  int dim_space = igb_from.v_x;
  int dim_time  = igb_from.v_t;

  close_igb_fileptr(igb_from);

  bool error_space = false;

  for(size_t i=1; i < inp_files.size(); i++) {
    init_igb_header(inp_files[i], igb_from);
    read_igb_header(igb_from);

    if(igb_from.v_x != dim_space) {
      fprintf(stderr, "spatial dimension of %s is inconsistent (%ld instead of %ld)!\n",
              inp_files[i].c_str(), (long) igb_from.v_x, (long) dim_space);
      error_space = true;
    }

    dim_time += igb_from.v_t;
    close_igb_fileptr(igb_from);
  }

  if(!error_space) {
    printf("concating %s ..\n", inp_files[0].c_str());

    init_igb_header(inp_files[0], igb_from);
    read_igb_header(igb_from);

    igb_to = igb_from;
    igb_to.filename = opts.odat;
    igb_to.v_t = dim_time;

    write_igb_header(igb_to);
    mt_vector<mt_real> databuff;

    for(int t=0; t < igb_from.v_t; t++) {
      read_igb_slice(databuff, igb_from);
      write_igb_slice(databuff, igb_to);
    }
    close_igb_fileptr(igb_from);

    for(int i=1; i<int(inp_files.size()); i++) {
      printf("concating %s ..\n", inp_files[i].c_str());

      init_igb_header(inp_files[i], igb_from);
      read_igb_header(igb_from);

      for(int t=0; t < igb_from.v_t; t++) {
        read_igb_slice(databuff, igb_from);
        write_igb_slice(databuff, igb_to);
      }
      close_igb_fileptr(igb_from);
    }

    close_igb_fileptr(igb_to);

  } else {
    fprintf(stderr, "Aborting concat due to inconsistent spatial dimensions!\n");
  }

  return 0;
}
