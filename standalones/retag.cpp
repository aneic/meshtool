#include <stdio.h>
#include <stdlib.h>
#include <string>

#include "mt_modes_base.h"


struct retag_options{
  std::string msh_base;
  std::string outmsh_base;
  std::string ifmt;
  std::string ofmt;
  std::string con;
  std::string neigh;
};


#define CON_DFLT 3
#define NEIGH_DFLT 2

static const std::string con_par = "-ncon=";
static const std::string neigh_par = "-neigh=";

void print_retag_help()
{
  fprintf(stderr, "retag: identify and re-tag elements that are not connected to enough elements of the same tag.\n");
  fprintf(stderr, "parameters:\n");
  fprintf(stderr, "%s<path>\t (input) path to basename of the mesh.\n", mesh_par.c_str());
  fprintf(stderr, "%s<path>\t (output) path to basename of the ouput mesh.\n", outmesh_par.c_str());
  fprintf(stderr, "%s<int>\t (optional) Number of common nodes that define a element connection. Default is %d.\n",
      con_par.c_str(), CON_DFLT);
  fprintf(stderr, "%s<int>\t (optional) Number of neighbours threshold. Elements with less neighbours are isolated. Default is %d.\n",
      neigh_par.c_str(), NEIGH_DFLT);
  fprintf(stderr, "%s<format>\t (optional) mesh input format. may be: %s\n", inp_format_par.c_str(), input_formats.c_str());
  fprintf(stderr, "%s<format>\t (optional) mesh output format. may be: %s\n", out_format_par.c_str(), output_formats.c_str());
  fprintf(stderr, "\n");
}

int retag_parse_options(int argc, char** argv, struct retag_options & opts)
{
  if(argc < 2) {
    print_retag_help();
    return 1;
  }

  // parse all retag parameters -----------------------------------------------------------------
  for(int i=1; i<argc; i++){
    std::string param = reformat_param(argv, i, argc);
    bool match = false;

    if(!match) match = parse_param(param, mesh_par, opts.msh_base);
    if(!match) match = parse_param(param, outmesh_par, opts.outmsh_base);
    if(!match) match = parse_param(param, out_format_par, opts.ofmt);
    if(!match) match = parse_param(param, inp_format_par, opts.ifmt);
    if(!match) match = parse_param(param, con_par, opts.con);
    if(!match) match = parse_param(param, neigh_par, opts.neigh);

    if(!match) {
      std::cerr << "Error: Cannot parse parameter " << param << std::endl;
      return 3;
    }
  }
  fixBasename(opts.msh_base);
  fixBasename(opts.outmsh_base);

  // check if all relevant parameters have been set ---------------------------------------------------
  bool mshok = opts.msh_base.size() > 0, outmshok = opts.outmsh_base.size() > 0;

  if( !(mshok && outmshok) )
  {
    std::cerr << "Error: Insufficient parameters provided." << std::endl;
    print_retag_help();
    return 2;
  }

  return 0;
}

int main(int argc, char** argv)
{
  struct timeval t1, t2;
  struct retag_options opts;
  struct mt_meshdata mesh;

  int ret = retag_parse_options(argc, argv, opts);
  if (ret != 0) return 1;

  std::cout << "Reading mesh: " << opts.msh_base << std::endl;
  gettimeofday(&t1, NULL);
  read_mesh_selected(mesh, opts.ifmt, opts.msh_base);
  compute_full_mesh_connectivity(mesh);
  gettimeofday(&t2, NULL);
  std::cout << "Done in " << (float)timediff_sec(t1, t2) << " sec" << std::endl;

  mt_idx_t ncon   = opts.con.size() ? atoi(opts.con.c_str()) : CON_DFLT;
  mt_idx_t nneigh = opts.neigh.size() ? atoi(opts.neigh.c_str()) : NEIGH_DFLT;

  do_retag_iter(mesh, ncon, nneigh);

  std::cout << "Writing mesh: " << opts.outmsh_base << std::endl;
  gettimeofday(&t1, NULL);
  write_mesh_selected(mesh, opts.ofmt, opts.outmsh_base);
  gettimeofday(&t2, NULL);
  std::cout << "Done in " << (float)timediff_sec(t1, t2) << " sec" << std::endl;


  return 0;
}
