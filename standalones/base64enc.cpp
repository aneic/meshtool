#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "mt_utils.h"

int main(int argc, char** argv)
{
  if(argc < 3) {
    fprintf(stderr, "Wrong usage! Use: %s [forward | backward | header] <input file>\n", argv[0]);
    exit(EXIT_FAILURE);
  }

  const char* direction = argv[1];
  const char* inp_file = argv[2];

  FILE* f = fopen(inp_file, "r");
  size_t fsz = file_size(f);

  fprintf(stderr, "reading %d bytes..\n", (int) fsz);

  mt_vector<unsigned char> inp_buff(fsz), out_buff(fsz*2);
  fread(inp_buff.data(), 1, fsz, f);
  fclose(f);

  bool forward = strcmp(direction, "forward") == 0;
  bool backward = strcmp(direction, "backward") == 0;
  bool header = strcmp(direction, "header") == 0;

  if(forward || header) {
    encode_base64 (inp_buff, out_buff);
    fprintf(stderr, "converting to %d bytes..\n", (int) out_buff.size());

    if(header) {
      printf("const char var[%d] = \n", (int) out_buff.size()+1);
      printf("\"");
      for(size_t i=0; i<out_buff.size()-1; i++) {
        printf("%c", out_buff[i]);
        if(i>0 && (i % 80) == 0)
          printf("\"\n\"");
      }
      printf("\";");

      return 0;
    }
  } else if (backward) {
    decode_base64 (inp_buff, out_buff);
    fprintf(stderr, "converting to %d bytes..\n", (int) out_buff.size());
  }  else {
    fprintf(stderr, "Error: encoding direction not recognized. Must be either \"forward\" or \"backward\"\n");
    exit(EXIT_FAILURE);
  }

  for(size_t i=0; i<out_buff.size(); i++)
    printf("%c", out_buff[i]);

  return 0;
}

