#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <algorithm>
#include <iostream>

/* Files:
meshes/torso/torso.* -> root torso mesh
meshes/torso/torso_submsh_1_3.* -> submesh with tags 1 and 3 of torso mesh
meshes/torso/bot.surf -> bottom surface of torso mesh
meshes/torso/top.surf -> top surface of torso mesh
meshes/torso/torso_dat1_extract.igb -> igb data on torso, contains the 10 first streps, extracted from root igb data

meshes/block/block.* -> root block mesh

*/

// #define TST_NO_EXEC_OUT
#include "testing.hpp"

#define BUFFLENGTH 100000

int test_convert(char summary[]) {

  printf("\n\n    ##########################\n");
  printf(    "    ### Convert test suite ###\n");
  printf(    "    ##########################\n");

  int error = 0;

  tst::execution_test conv_bin("EXECUTABLE convert -imsh=meshes/block/block.elem -omsh=tmp/test_block.belem"),
                      conv_vtk("EXECUTABLE convert -imsh=meshes/block/block.elem -omsh=tmp/test_block.vtk"),
                      conv_vtu("EXECUTABLE convert -imsh=meshes/block/block.elem -omsh=tmp/test_block.vtu");

  conv_bin.add_check(new tst::bin_file_equal("equal binary elem test",
                     "convert/ref_block.belem", "tmp/test_block.belem"));
  conv_bin.add_check(new tst::bin_file_equal("equal binary lon test",
                     "convert/ref_block.blon", "tmp/test_block.blon"));
  conv_bin.add_check(new tst::bin_file_equal("equal binary pts test",
                     "convert/ref_block.bpts", "tmp/test_block.bpts"));

  conv_vtk.add_check(new tst::bin_file_equal("equal binary file test",
                     "convert/ref_block.vtk", "tmp/test_block.vtk"));

  conv_vtu.add_check(new tst::bin_file_equal("equal binary file test",
                     "convert/ref_block.vtu", "tmp/test_block.vtu"));

  tst::execution_test rem_tmp("rm tmp/test_block.b* tmp/test_block.vtk tmp/test_block.vtu");

  error += conv_bin.run();
  error += conv_vtk.run();
  error += conv_vtu.run();
  error += rem_tmp.run();

  if(error)
    snprintf(summary + strlen(summary), BUFFLENGTH-1, "Suite \"conversion tests\" failed with %d errors.\n\n", error);
  else
    snprintf(summary + strlen(summary), BUFFLENGTH-1, "Suite \"conversion tests\" succeeded with no errors.\n\n");

  return error;
}


int test_extract(char summary[]) {

  printf("\n\n    ##########################\n");
  printf(    "    ### Extract test suite ###\n");
  printf(    "    ##########################\n");

  int error = 0;

  tst::execution_test surf("EXECUTABLE extract surface -msh=meshes/block/block.elem -surf=tmp/block"),
                      submsh("EXECUTABLE extract mesh -msh=meshes/block/block.elem -tags=2 -submsh=tmp/test_tag2.vtk"),
                      data("EXECUTABLE extract data -msh=meshes/block/block -submsh=extract/tag2 "
                           "-msh_data=extract/block.act.dat -submsh_data=tmp/tag2.act.dat");

  surf.add_check(new tst::text_file_equal("equal .surf.vtx file",
                     "extract/block.surf.vtx", "tmp/block.surf.vtx"));

  submsh.add_check(new tst::bin_file_equal("equal binary file",
                     "extract/tag2.vtk", "tmp/test_tag2.vtk"));

  data.add_check(new tst::float_text_file_equal("equal .dat file",
                 "extract/tag2.act.dat", "tmp/tag2.act.dat", 1e-6));

  tst::execution_test rem_tmp("rm tmp/block.* tmp/test_tag2.vtk tmp/tag2.act.dat");

  error += int(surf.run());
  error += int(submsh.run());
  error += int(data.run());
  error += int(rem_tmp.run());

  if(error)
    snprintf(summary + strlen(summary), BUFFLENGTH-1, "Suite \"extraction tests\" failed with %d errors.\n\n", error);
  else
    snprintf(summary + strlen(summary), BUFFLENGTH-1, "Suite \"extraction tests\" succeeded with no errors.\n\n");

  return error;
}

int test_insert(char summary[]) {

  printf("\n\n    #########################\n");
  printf(    "    ### Insert test suite ###\n");
  printf(    "    #########################\n");

  int error = 0;

  tst::execution_test submsh("EXECUTABLE insert submesh -msh=meshes/block/block.elem"
                             " -submsh=extract/tag2 -outmsh=tmp/insert.block"),
                      data("EXECUTABLE insert data -msh=meshes/block/block -submsh=extract/tag2 "
                           "-msh_data=extract/block.act.dat -submsh_data=extract/tag2.act.dat -odat=tmp/insert.dat");

  submsh.add_check(new tst::text_file_equal("equal .elem file",
                     "meshes/block/block.elem", "tmp/insert.block.elem"));
  submsh.add_check(new tst::float_text_file_equal("equal .pts file",
                     "meshes/block/block.pts", "tmp/insert.block.pts", 1e-6));
  submsh.add_check(new tst::float_text_file_equal("equal .lon file",
                     "meshes/block/block.lon", "tmp/insert.block.lon", 1e-6));

  data.add_check(new tst::float_text_file_equal("equal .dat file",
                 "extract/block.act.dat", "tmp/insert.dat", 1e-6));

  tst::execution_test rem_tmp("rm tmp/insert.block.* tmp/insert.dat");

  error += int(submsh.run());
  error += int(data.run());
  error += int(rem_tmp.run());

  if(error)
    snprintf(summary + strlen(summary), BUFFLENGTH-1, "Suite \"insertion tests\" failed with %d errors.\n\n", error);
  else
    snprintf(summary + strlen(summary), BUFFLENGTH-1, "Suite \"insertion tests\" succeeded with no errors.\n\n");

  return error;
}

int test_resample(char summary[]) {

  printf("\n\n    ###########################\n");
  printf(    "    ### Resample test suite ###\n");
  printf(    "    ###########################\n");

  int error = 0;

  tst::execution_test mesh_ref("EXECUTABLE resample mesh -msh=meshes/block/block.elem -outmsh=tmp/block.ref.vtk -max=200 -postsmth=0"),
#ifdef __WIN32__
                      mesh_crs("EXECUTABLE resample mesh -msh=resample/block.ref.vtk -outmsh=tmp/block.crs.vtk -min=700 -surf_corr=0.95 -postsmth=0");
#else
                      mesh_crs("OMP_NUM_THREADS=1 EXECUTABLE resample mesh -msh=resample/block.ref.vtk -outmsh=tmp/block.crs.vtk -min=700 -surf_corr=0.95 -postsmth=0");
#endif

  mesh_ref.add_check(new tst::bin_file_equal("equal .vtk file",
                     "resample/block.ref.vtk", "tmp/block.ref.vtk"));

  mesh_crs.add_check(new tst::bin_file_equal("equal .vtk file",
                     "resample/block.crs.vtk", "tmp/block.crs.vtk"));

  tst::execution_test rem_tmp("rm tmp/block.*");

  error += int(mesh_ref.run());
  error += int(mesh_crs.run());
  error += int(rem_tmp.run());

  if(error)
    snprintf(summary + strlen(summary), BUFFLENGTH-1, "Suite \"resample tests\" failed with %d errors.\n\n", error);
  else
    snprintf(summary + strlen(summary), BUFFLENGTH-1, "Suite \"resample tests\" succeeded with no errors.\n\n");

  return error;
}

int test_smooth(char summary[]) {

  printf("\n\n    ############################\n");
  printf(    "    ### Smoothing test suite ###\n");
  printf(    "    ############################\n");

  int error = 0;


#ifdef __WIN32__
  tst::execution_test mesh_smth("EXECUTABLE smooth mesh -msh=resample/block.ref.vtk -outmsh=tmp/block.ref.smth.elem -smth=0.2 -iter=200 -tags=+ -thr=0.95 -edge=30");
#else
  tst::execution_test mesh_smth("OMP_NUM_THREADS=1 EXECUTABLE smooth mesh -msh=resample/block.ref.vtk -outmsh=tmp/block.ref.smth.elem -smth=0.2 -iter=200 -tags=+ -thr=0.95 -edge=30");
#endif


  mesh_smth.add_check(new tst::text_file_equal("equal .elem file",
                     "smooth/block.ref.smth.elem", "tmp/block.ref.smth.elem"));
  mesh_smth.add_check(new tst::float_text_file_equal("equal .pts file",
                     "smooth/block.ref.smth.pts", "tmp/block.ref.smth.pts", 1e-3));
  mesh_smth.add_check(new tst::float_text_file_equal("equal .lon file",
                     "smooth/block.ref.smth.lon", "tmp/block.ref.smth.lon", 1e-3));

  tst::execution_test rem_tmp("rm tmp/block.ref.smth.*");

  error += int(mesh_smth.run());
  error += int(rem_tmp.run());

  if(error)
    snprintf(summary + strlen(summary), BUFFLENGTH-1, "Suite \"smoothing tests\" failed with %d errors.\n\n", error);
  else
    snprintf(summary + strlen(summary), BUFFLENGTH-1, "Suite \"smoothing tests\" succeeded with no errors.\n\n");

  return error;
}


int test_indexsearch(char summary[]) {

  printf("\n\n    ###############################\n");
  printf(    "    ### Index search test suite ###\n");
  printf(    "    ###############################\n");

  int error = 0;

  tst::execution_test query_idxlist("EXECUTABLE query idxlist -msh=meshes/block/block -coord=indexsearch/searchfile.txt");

  query_idxlist.add_check(new tst::text_file_equal("equal output textfile",
                          "indexsearch/searchfile.txt.out.txt", "indexsearch/reference.results.txt"));

  tst::execution_test rem_tmp("rm indexsearch/searchfile.txt.out.txt");

  error += int(query_idxlist.run());
  error += int(rem_tmp.run());

  if(error)
    snprintf(summary + strlen(summary), BUFFLENGTH-1, "Suite \"indexsearch tests\" failed with %d errors.\n\n", error);
  else
    snprintf(summary + strlen(summary), BUFFLENGTH-1, "Suite \"indexsearch tests\" succeeded with no errors.\n\n");

  return error;
}


int test_pymt_insert_tags(char summary[]) {

  printf("\n\n    ##########################\n");
  printf(    "    ### PyMT - Insert Tags ###\n");
  printf(    "    ##########################\n");

  tst::execution_test test("EXECUTABLE insert tags -msh=meshes/torso/torso -ifmt=carp_bin -idat=pymt_insert_tags/tags.dat -outmsh=tmp/insert_tags_torso -ofmt=carp_bin");
  test.add_check(new tst::bin_file_equal("equal binary elem test",
                 "pymt_insert_tags/torso_ref.belem", "tmp/insert_tags_torso.belem"));
  tst::execution_test rm_tmp("rm tmp/insert_tags_torso.*");

  int error = 0;
  error += static_cast<int>(test.run());
  error += static_cast<int>(rm_tmp.run());
  if (error)
    snprintf(summary + strlen(summary), BUFFLENGTH-1, "Suite \"PyMT - Insert Tags\" failed with %d errors.\n\n", error);
  else
    snprintf(summary + strlen(summary), BUFFLENGTH-1, "Suite \"PyMT - Insert Tags\" succeeded with no errors.\n\n");

  return error;
}

int test_pymt_insert_tagsurfs(char summary[]) {

  printf("\n\n    ##########################\n");
  printf(    "    ### PyMT - Insert Tags ###\n");
  printf(    "    ##########################\n");

  tst::execution_test gen_box("EXECUTABLE generate mesh -surf=meshes/shapes/box_outer.vtk -ins_tag=0 -outmsh=tmp/insert_box.vtk");
  tst::execution_test test("EXECUTABLE insert tagsurfs -msh=tmp/insert_box.vtk -surf=meshes/shapes/sphere1.vtk,meshes/shapes/sphere2.vtk -tags=1,2 -mode 0 -outmsh=tmp/insert_tagsurfs_box.vtk");
  test.add_check(new tst::bin_file_equal("equal mesh test", "pymt_insert_tagsurfs/insert_tagsurfs_box.vtk", "tmp/insert_tagsurfs_box.vtk"));
  tst::execution_test rm_tmp("rm tmp/insert_*");

  int error = 0;
  error += static_cast<int>(gen_box.run());
  error += static_cast<int>(test.run());
  error += static_cast<int>(rm_tmp.run());
  if (error)
    snprintf(summary + strlen(summary), BUFFLENGTH-1, "Suite \"PyMT - Insert Tagsurfs\" failed with %d errors.\n\n", error);
  else
    snprintf(summary + strlen(summary), BUFFLENGTH-1, "Suite \"PyMT - Insert Tagsurfs\" succeeded with no errors.\n\n");

  return error;
}

int test_pymt_extract_mesh(char summary[]) {

  printf("\n\n    ###########################\n");
  printf(    "    ### PyMT - Extract Mesh ###\n");
  printf(    "    ###########################\n");


  tst::execution_test test("EXECUTABLE extract mesh -msh=meshes/torso/torso -ifmt=carp_bin -tags=1,3,5,7 -submsh=tmp/extract_mesh_torso -ofmt=carp_bin");
  test.add_check(new tst::bin_file_equal("equal binary elem test",
                 "pymt_extract_mesh/torso_ref.belem", "tmp/extract_mesh_torso.belem"));
  tst::execution_test rm_tmp("rm tmp/extract_mesh_torso.*");

  int error = 0;
  error += static_cast<int>(test.run());
  error += static_cast<int>(rm_tmp.run());
  if (error)
    snprintf(summary + strlen(summary), BUFFLENGTH-1, "Suite \"PyMT - Extract Mesh\" failed with %d errors.\n\n", error);
  else
    snprintf(summary + strlen(summary), BUFFLENGTH-1, "Suite \"PyMT - Extract Mesh\" succeeded with no errors.\n\n");

  return error;
}

int test_pymt_extract_volume(char summary[]) {

  printf("\n\n    #############################\n");
  printf(    "    ### PyMT - Extract Volume ###\n");
  printf(    "    #############################\n");

  tst::execution_test extract_vol_test("EXECUTABLE extract volume -msh=meshes/torso/torso -coord=0,10000,0,10000,0,10000 -submsh=tmp/torso_extract_vol -ofmt=carp_txt");
  // eidx, elem, lon, nod, pts
  extract_vol_test.add_check(new tst::bin_file_equal("equal binary eidx test",
                 "pymt_extract_volume/torso_extract_vol.eidx", "tmp/torso_extract_vol.eidx"));
  extract_vol_test.add_check(new tst::bin_file_equal("equal binary elem test",
                  "pymt_extract_volume/torso_extract_vol.elem", "tmp/torso_extract_vol.elem"));
  extract_vol_test.add_check(new tst::bin_file_equal("equal binary lon test",
                  "pymt_extract_volume/torso_extract_vol.lon", "tmp/torso_extract_vol.lon"));
  extract_vol_test.add_check(new tst::bin_file_equal("equal binary nod test",
                  "pymt_extract_volume/torso_extract_vol.nod", "tmp/torso_extract_vol.nod"));
  extract_vol_test.add_check(new tst::bin_file_equal("equal binary pts test",
                  "pymt_extract_volume/torso_extract_vol.pts", "tmp/torso_extract_vol.pts"));

  tst::execution_test rm_tmp("rm tmp/torso_extract_vol.*");

  int error = 0;
  error += static_cast<int>(extract_vol_test.run());
  error += static_cast<int>(rm_tmp.run());
  if (error)
    snprintf(summary + strlen(summary), BUFFLENGTH-1, "Suite \"PyMT - Extract Volume\" failed with %d errors.\n\n", error);
  else
    snprintf(summary + strlen(summary), BUFFLENGTH-1, "Suite \"PyMT - Extract Volume\" succeeded with no errors.\n\n");

  return error;
}

#if 0
int test_pymt_clean_topology(char summary[]) {

  printf("\n\n    #############################\n");
  printf(    "    ### PyMT - Clean Topology ###\n");
  printf(    "    #############################\n");


  tst::execution_test cp_tmp("cp meshes/torso/torso.bpts pymt_clean_topology/torso_flipped.bpts && cp meshes/torso/torso_flipped.belem pymt_clean_topology/torso_flipped.belem");
  tst::execution_test test("EXECUTABLE clean topology -msh=pymt_clean_topology/torso_flipped -ifmt=carp_bin -outmsh=tmp/clean_topology_torso -ofmt=carp_bin");
  test.add_check(new tst::bin_file_equal("equal binary elem test",
                 "pymt_clean_topology/torso_flipped_ref.belem", "tmp/clean_topology_torso.belem"));
  tst::execution_test rm_tmp("rm pymt_clean_topology/torso_flipped.* tmp/clean_topology_torso.*");

  int error = 0;
  error += static_cast<int>(cp_tmp.run());
  error += static_cast<int>(test.run());
  error += static_cast<int>(rm_tmp.run());
  if (error)
    snprintf(summary + strlen(summary), BUFFLENGTH-1, "Suite \"PyMT - Clean Topology\" failed with %d errors.\n\n", error);
  else
    snprintf(summary + strlen(summary), BUFFLENGTH-1, "Suite \"PyMT - Clean Topology\" succeeded with no errors.\n\n");

  return error;
}
#endif

int test_pymt_generate_distancefield(char summary[]) {

  printf("\n\n    #####################################\n");
  printf(    "    ### PyMT - Generate Distancefield ###\n");
  printf(    "    #####################################\n");


  tst::execution_test test("EXECUTABLE generate distancefield -msh=meshes/torso/torso -ssurf=meshes/torso/top -esurf=meshes/torso/bot -tags=1,3,5,7 -odat tmp/generate_distancefield_torso.dat");
  test.add_check(new tst::text_file_equal("equal text data test",
                 "pymt_generate_distancefield/generate_distancefield_torso.dat", "tmp/generate_distancefield_torso.dat"));
  tst::execution_test rm_tmp("rm tmp/generate_distancefield_torso.dat");

  int error = 0;
  error += static_cast<int>(test.run());
  error += static_cast<int>(rm_tmp.run());
  if (error)
    snprintf(summary + strlen(summary), BUFFLENGTH-1, "Suite \"PyMT - Generate Distancefield\" failed with %d errors.\n\n", error);
  else
    snprintf(summary + strlen(summary), BUFFLENGTH-1, "Suite \"PyMT - Generate Distancefield\" succeeded with no errors.\n\n");

  return error;
}

int test_pymt_generate_fibers(char summary[]) {

    printf("\n\n    ###############################\n");
    printf(    "    ### PyMT - Generate Fibers ###\n");
    printf(    "    ###############################\n");

    tst::execution_test test_1_fibers("EXECUTABLE generate fibres -msh=meshes/torso/torso -op=1 -outmsh=tmp/generate_fibers_torso_1");
    test_1_fibers.add_check(new tst::bin_file_equal("equal bin data test", "pymt_generate_fibers/generate_fibers_torso_1.blon", "tmp/generate_fibers_torso_1.blon"));

    tst::execution_test test_2_fibers("EXECUTABLE generate fibres -msh=meshes/torso/torso -op=2 -outmsh=tmp/generate_fibers_torso_2");
    test_2_fibers.add_check(new tst::bin_file_equal("equal bin data test", "pymt_generate_fibers/generate_fibers_torso_2.blon", "tmp/generate_fibers_torso_2.blon"));


    tst::execution_test rm_tmp("rm tmp/generate_fibers_torso*");

    int error = 0;
    error += static_cast<int>(test_1_fibers.run());
    error += static_cast<int>(test_2_fibers.run());
    error += static_cast<int>(rm_tmp.run());
    if (error)
      snprintf(summary + strlen(summary), BUFFLENGTH-1, "Suite \"PyMT - Generate Fibers\" failed with %d errors.\n\n", error);
    else
      snprintf(summary + strlen(summary), BUFFLENGTH-1, "Suite \"PyMT - Generate Fibers\" succeeded with no errors.\n\n");

    return error;
}

int test_pymt_generate_mesh(char summary[]) {

    printf("\n\n    ###############################\n");
    printf(    "    ### PyMT - Generate Mesh ###\n");
    printf(    "    ###############################\n");

    tst::execution_test test_1("EXECUTABLE generate mesh -surf=meshes/shapes/box_outer.vtk,meshes/shapes/box_small.vtk,meshes/shapes/sphere1.vtk,meshes/shapes/sphere2.vtk -ins_tag=0,1,2,3 -outmsh=tmp/generate_mesh.vtk");
    test_1.add_check(new tst::bin_file_equal("equal mesh data test", "pymt_generate_mesh/mesh.vtk", "tmp/generate_mesh.vtk"));
    tst::execution_test test_2("EXECUTABLE generate mesh -surf=meshes/shapes/box_small.vtk,meshes/shapes/sphere1.vtk,meshes/shapes/sphere2.vtk,meshes/shapes/box_outer.vtk -ins_tag=1,2,3,0 -outmsh=tmp/generate_mesh_padding.vtk -padding 1");
    test_2.add_check(new tst::bin_file_equal("equal mesh data test", "pymt_generate_mesh/mesh_padding.vtk", "tmp/generate_mesh_padding.vtk"));
#ifdef __WIN32__
    tst::execution_test test_3("EXECUTABLE generate mesh -surf=meshes/shapes/box_outer.vtk -ins_tag=0 -bdry_layers=4 -outmsh=tmp/generate_mesh_bdry.vtk");
#else
    tst::execution_test test_3("OMP_NUM_THREADS=1 EXECUTABLE generate mesh -surf=meshes/shapes/box_outer.vtk -ins_tag=0 -bdry_layers=4 -outmsh=tmp/generate_mesh_bdry.vtk");
#endif
    test_3.add_check(new tst::bin_file_equal("equal mesh data test", "pymt_generate_mesh/mesh_bdry.vtk", "tmp/generate_mesh_bdry.vtk"));

    tst::execution_test rm_tmp("rm tmp/generate_mesh*");

    int error = 0;
    error += static_cast<int>(test_1.run());
    error += static_cast<int>(test_2.run());
    error += static_cast<int>(test_3.run());
    error += static_cast<int>(rm_tmp.run());
    if (error)
      snprintf(summary + strlen(summary), BUFFLENGTH-1, "Suite \"PyMT - Generate Mesh\" failed with %d errors.\n\n", error);
    else
      snprintf(summary + strlen(summary), BUFFLENGTH-1, "Suite \"PyMT - Generate Mesh\" succeeded with no errors.\n\n");

    return error;
}

int test_pymt_interpolate(char summary[]) {
  printf("\n\n    #####################################\n");
  printf(    "    ### PyMT - Interpolate test suite ###\n");
  printf(    "    #####################################\n");

  int error = 0;

  // Test for interpolate_node2elem_mode
  tst::execution_test interpolate_node2elem("EXECUTABLE interpolate node2elem -omsh=./meshes/torso/torso -idat=./meshes/torso/torso.vec -odat=./tmp/interpolate_node2elem.vec");
  interpolate_node2elem.add_check(new tst::text_file_equal("equal interpolate_node2elem file",
                                  "pymt_interpolate/interpolate_node2elem.vec", "tmp/interpolate_node2elem.vec"));

  // Test for interpolate_elem2node_mode
  tst::execution_test interpolate_elem2node("EXECUTABLE interpolate elem2node -omsh=./meshes/torso/torso -idat=./meshes/torso/torso.vec -odat=./tmp/interpolate_elem2node.vec");
  interpolate_elem2node.add_check(new tst::text_file_equal("equal interpolate_elem2node file",
                                  "pymt_interpolate/interpolate_elem2node.vec", "tmp/interpolate_elem2node.vec"));

  // Test for interpolate_nodedata without igb
  tst::execution_test interpolate_nodedata_dat("EXECUTABLE interpolate nodedata -omsh=./meshes/torso/torso -imsh=./meshes/torso/torso -idat=./meshes/torso/dist_field.dat -odat=./tmp/interpolate_nodedata_noigb.dat");
  interpolate_nodedata_dat.add_check(new tst::text_file_equal("equal interpolate_nodedata_noigb file",
                                     "pymt_interpolate/interpolate_nodedata_noigb.dat", "tmp/interpolate_nodedata_noigb.dat"));

  // Test for interpolate_nodedata with igb
  tst::execution_test interpolate_nodedata_igb("EXECUTABLE interpolate nodedata -omsh=./meshes/torso/torso -imsh=./meshes/torso/torso -idat=./meshes/torso/torso_dat0.igb -odat=./tmp/interpolate_nodedata.igb");
  interpolate_nodedata_igb.add_check(new tst::bin_file_equal("equal interpolate_nodedata file",
                                     "pymt_interpolate/interpolate_nodedata.igb", "tmp/interpolate_nodedata.igb"));

  // Test for interpolate clouddata without igb (shepard)
  tst::execution_test interpolate_clouddata_dat0("EXECUTABLE interpolate clouddata -omsh=./meshes/torso/torso.belem -pts=./meshes/torso/torso_rot.pts -idat=./meshes/torso/dist_field.dat -odat=./tmp/interpolate_clouddata_shprd.dat -mode=0");
  interpolate_clouddata_dat0.add_check(new tst::float_text_file_equal("equal interpolate_clouddata file",
                                       "pymt_interpolate/interpolate_clouddata_shprd.dat", "tmp/interpolate_clouddata_shprd.dat", 1.0e-3));

  // Test for interpolate clouddata without igb (global shepard)
  tst::execution_test interpolate_clouddata_dat1("EXECUTABLE interpolate clouddata -omsh=./meshes/torso/torso.belem -pts=./meshes/torso/torso_rot.pts -idat=./meshes/torso/dist_field.dat -odat=./tmp/interpolate_clouddata_gshprd.dat -mode=1");
  interpolate_clouddata_dat1.add_check(new tst::float_text_file_equal("equal interpolate_clouddata file",
                                       "pymt_interpolate/interpolate_clouddata_gshprd.dat", "tmp/interpolate_clouddata_gshprd.dat", 1.0e-3));

  // Test for interpolate clouddata without igb (rbf)
  tst::execution_test interpolate_clouddata_dat2("EXECUTABLE interpolate clouddata -omsh=./meshes/torso/torso.belem -pts=./meshes/torso/torso_rot.pts -idat=./meshes/torso/dist_field.dat -odat=./tmp/interpolate_clouddata_rbf.dat -mode=2");
  interpolate_clouddata_dat2.add_check(new tst::float_text_file_equal("equal interpolate_clouddata file",
                                       "pymt_interpolate/interpolate_clouddata_rbf.dat", "tmp/interpolate_clouddata_rbf.dat", 1.0e-3));

  // Test for interpolate clouddata with igb
  tst::execution_test interpolate_clouddata_igb("EXECUTABLE interpolate clouddata -omsh=./meshes/block/block.elem -pts=./meshes/torso/torso_rot.pts -idat=./meshes/torso/torso_dat0.igb -odat=./tmp/interpolate_clouddata.igb");
  interpolate_clouddata_igb.add_check(new tst::bin_file_equal("equal interpolate_clouddata file",
                                      "pymt_interpolate/interpolate_clouddata_extract.igb", "tmp/interpolate_clouddata.igb"));


  tst::execution_test rem_tmp("rm tmp/interpolate*");

  error += int(interpolate_node2elem.run());
  error += int(interpolate_elem2node.run());
  error += int(interpolate_nodedata_dat.run());
  error += int(interpolate_nodedata_igb.run());
  error += int(interpolate_clouddata_dat0.run());
  error += int(interpolate_clouddata_dat1.run());
  error += int(interpolate_clouddata_dat2.run());
  error += int(interpolate_clouddata_igb.run());
  error += int(rem_tmp.run());

  if(error)
    snprintf(summary + strlen(summary), BUFFLENGTH-1, "Suite \"PyMT - Interpolate test suite\" failed with %d errors.\n\n", error);
  else
    snprintf(summary + strlen(summary), BUFFLENGTH-1, "Suite \"PyMT - Interpolate test suite\" succeeded with no errors.\n\n");

  return error;
}


int test_pymt_extract_gradient(char summary[]) {
  printf("\n\n    ##########################################\n");
  printf(    "    ### PyMT - Extract gradient test suite ###\n");
  printf(    "    ##########################################\n");

  int error = 0;

  // Test for interpolate_node2elem_mode
  tst::execution_test extract_gradient("EXECUTABLE extract gradient -msh=meshes/torso/torso -idat=meshes/torso/dist_field.dat -odat=tmp/extract_gradient");

  extract_gradient.add_check(new tst::text_file_equal("equal .grad.vec file",
                             "pymt_extract_gradient/extract_gradient.grad.vec", "tmp/extract_gradient.grad.vec"));
  extract_gradient.add_check(new tst::text_file_equal("equal .gradmag.dat file",
                             "pymt_extract_gradient/extract_gradient.gradmag.dat", "tmp/extract_gradient.gradmag.dat"));

  tst::execution_test rem_tmp("rm tmp/extract_gradient*");

  error += int(extract_gradient.run());
  error += int(rem_tmp.run());

  if(error)
    snprintf(summary + strlen(summary), BUFFLENGTH-1, "Suite \"PyMT - Extract gradient test suite\" failed with %d errors.\n\n", error);
  else
    snprintf(summary + strlen(summary), BUFFLENGTH-1, "Suite \"PyMT - Extract gradient test suite\" succeeded with no errors.\n\n");

  return error;
}

int test_pymt_extract_data(char summary[]) {
  printf("\n\n    ######################################\n");
  printf(    "    ### PyMT - Extract data test suite ###\n");
  printf(    "    ######################################\n");

  int error = 0;

  tst::execution_test extract_data("EXECUTABLE extract data -submsh=meshes/torso/torso_submsh_1_3 -msh_data=meshes/torso/torso_dat0.igb -submsh_data=tmp/torso_submsh_1_3_extracted.igb");

  extract_data.add_check(new tst::text_file_equal("equal .igb file",
                         "pymt_extract_data/torso_submsh_1_3_extracted.igb", "tmp/torso_submsh_1_3_extracted.igb"));

  tst::execution_test rem_tmp("rm tmp/torso_submsh_1_3_extracted*");

  error += int(extract_data.run());
  error += int(rem_tmp.run());

  if(error)
    snprintf(summary + strlen(summary), BUFFLENGTH-1, "Suite \"PyMT - Extract data test suite\" failed with %d errors.\n\n", error);
  else
    snprintf(summary + strlen(summary), BUFFLENGTH-1, "Suite \"PyMT - Extract data test suite\" succeeded with no errors.\n\n");

  return error;
}

int test_pymt_extract_tags(char summary[]) {
  printf("\n\n    ######################################\n");
  printf(    "    ### PyMT - Extract tags test suite ###\n");
  printf(    "    ######################################\n");

  int error = 0;

  tst::execution_test extract_tags("EXECUTABLE extract tags -msh=meshes/torso/torso -odat=tmp/torso_tags.dat");

  extract_tags.add_check(new tst::text_file_equal("equal .dat file",
                         "pymt_extract_tags/torso_tags.dat", "tmp/torso_tags.dat"));

  tst::execution_test rem_tmp("rm tmp/torso_tags.dat");

  error += int(extract_tags.run());
  error += int(rem_tmp.run());

  if(error)
    snprintf(summary + strlen(summary), BUFFLENGTH-1, "Suite \"PyMT - Extract tags test suite\" failed with %d errors.\n\n", error);
  else
    snprintf(summary + strlen(summary), BUFFLENGTH-1, "Suite \"PyMT - Extract tags test suite\" succeeded with no errors.\n\n");

  return error;
}

int test_pymt_extract_myocard(char summary[]) {
  printf("\n\n    ######################################\n");
  printf(    "    ### PyMT - Extract myocard test suite ###\n");
  printf(    "    ######################################\n");

  int error = 0;

  tst::execution_test extract_myocard("EXECUTABLE extract myocard -msh=meshes/torso/torso -submsh=tmp/torso_myocard");

  // eidx, elem, lon, nod, pts
  extract_myocard.add_check(new tst::text_file_equal("equal .eidx file", "pymt_extract_myocard/torso_myocard.eidx", "tmp/torso_myocard.eidx"));
  extract_myocard.add_check(new tst::text_file_equal("equal .elem file", "pymt_extract_myocard/torso_myocard.elem", "tmp/torso_myocard.elem"));
  extract_myocard.add_check(new tst::text_file_equal("equal .lon file", "pymt_extract_myocard/torso_myocard.lon", "tmp/torso_myocard.lon"));
  extract_myocard.add_check(new tst::text_file_equal("equal .nod file", "pymt_extract_myocard/torso_myocard.nod", "tmp/torso_myocard.nod"));
  extract_myocard.add_check(new tst::text_file_equal("equal .pts file", "pymt_extract_myocard/torso_myocard.pts", "tmp/torso_myocard.pts"));

  tst::execution_test rem_tmp("rm tmp/torso_myocard.*");

  error += int(extract_myocard.run());
  error += int(rem_tmp.run());

  if(error)
    snprintf(summary + strlen(summary), BUFFLENGTH-1, "Suite \"PyMT - Extract myocard test suite\" failed with %d errors.\n\n", error);
  else
    snprintf(summary + strlen(summary), BUFFLENGTH-1, "Suite \"PyMT - Extract myocard test suite\" succeeded with no errors.\n\n");

  return error;
}

int test_pymt_query(char summary[]) {
  printf("\n\n    ###############################\n");
  printf(    "    ### PyMT - Query test suite ###\n");
  printf(    "    ###############################\n");

  int error = 0;

  tst::execution_test query_tags("EXECUTABLE query tags -msh=meshes/torso/torso | sed -n '/Myocardium tags:/,$p' > tmp/query_tags_output.txt");

  query_tags.add_check(new tst::text_file_equal("equal output",
                      "pymt_query_tags/query_tags_output.txt", "tmp/query_tags_output.txt"));


  // test for query curvature
  tst::execution_test query_curvature("EXECUTABLE query curvature -msh=meshes/torso/torso -surf=meshes/torso/all.surf -size=1 -odat=tmp/query_curvature.dat");

  query_curvature.add_check(new tst::text_file_equal("equal dat files",
                            "pymt_query_tags/query_curvature.dat", "tmp/query_curvature.dat"));

  tst::execution_test query_edge("EXECUTABLE query edges -msh=meshes/torso/torso -odat=tmp/query_edge");
  query_edge.add_check(new tst::text_file_equal("equal .concnt.dat file",
                      "pymt_query_edges/query_edge.concnt.dat", "tmp/query_edge.concnt.dat"));
  query_edge.add_check(new tst::text_file_equal("equal .len.dat file",
                      "pymt_query_edges/query_edge.len.dat", "tmp/query_edge.len.dat"));
  query_edge.add_check(new tst::text_file_equal("equal .vol.dat file",
                      "pymt_query_edges/query_edge.vol.dat", "tmp/query_edge.vol.dat"));


  tst::execution_test query_quality("EXECUTABLE query quality -msh=meshes/torso/torso -odat=tmp/query_quality");

  query_quality.add_check(new tst::text_file_equal("equal .qual.dat file",
                      "pymt_query_quality/torso.qual.dat", "tmp/query_quality.qual.dat"));


  tst::execution_test query_bbox("EXECUTABLE query bbox -msh=meshes/torso/torso | sed -n '/mesh statistics/,$p' | sed '/Done/d' > tmp/query_bbox_output.txt");

  query_bbox.add_check(new tst::text_file_equal("equal output", "pymt_query_bbox/query_bbox_output.txt", "tmp/query_bbox_output.txt"));


  tst::execution_test query_idx("EXECUTABLE query idx -msh=meshes/torso/torso -coord=0,0,0 | sed -n '/Vertex/,$p' > tmp/query_idx_0-0-0.txt");
  query_idx.add_check(new tst::text_file_equal("equal output", "pymt_query_idx/torso_0-0-0.txt", "tmp/query_idx_0-0-0.txt"));

  tst::execution_test query_idx_thr("EXECUTABLE query idx -msh=meshes/torso/torso -coord=0,0,0 -thr=2000 | sed -n '/Vertex/,$p' > tmp/query_idx_0-0-0_2000.txt");
  query_idx_thr.add_check(new tst::text_file_equal("equal output", "pymt_query_idx/torso_0-0-0_2000.txt", "tmp/query_idx_0-0-0_2000.txt"));

  tst::execution_test rem_tmp("rm tmp/query_*");

  error += int(query_tags.run());
  error += int(query_curvature.run());
  error += int(query_edge.run());
  error += int(query_quality.run());
  error += int(query_bbox.run());
  error += int(query_idx.run());
  error += int(query_idx_thr.run());
  error += int(rem_tmp.run());

  if(error)
    snprintf(summary + strlen(summary), BUFFLENGTH-1, "Suite \"PyMT - Query test suite\" failed with %d errors.\n\n", error);
  else
    snprintf(summary + strlen(summary), BUFFLENGTH-1, "Suite \"PyMT - Query test suite\" succeeded with no errors.\n\n");

  return error;
}

bool mode_enabled(const std::vector<std::string>& args, const std::string& mode) {
  // modes are either enabled if no args are given or if they are in the list of args
  return args.size() == 0 || std::find(args.begin(), args.end(), mode) != args.end();
}

int main(int argc, char** argv)
{
  std::vector<std::string> args;
  if(argc > 1) {
    // args are comma separated in argv[1]
    char* arg = strtok(argv[1], ",");
    while(arg != NULL) {
      args.push_back(std::string(arg));
      arg = strtok(NULL, ",");
    }
  }
  if(args.size() > 0) {
    std::cout << "Running tests with modes: ";
    for (const auto& arg : args) {
      std::cout << arg << " ";
    }
    std::cout << std::endl;
  } else {
    std::cout << "Running all tests" << std::endl;
  }

  char summary[BUFFLENGTH];
  int error_sum = 0;
  snprintf(summary, BUFFLENGTH-1, "\n\n");
  tst::execution_test::fail_fast = true;

  tst::execution_test rem_tmp("rm -rf tmp");
  rem_tmp.run();
  tst::execution_test make_tmp("mkdir tmp");
  make_tmp.run();

  if(mode_enabled(args, "convert")) error_sum += test_convert(summary);
  if(mode_enabled(args, "extract")) error_sum += test_extract(summary);
  if(mode_enabled(args, "insert")) error_sum += test_insert(summary);
  if(mode_enabled(args, "resample")) error_sum += test_resample(summary);
  if(mode_enabled(args, "smooth")) error_sum += test_smooth(summary);
  if(mode_enabled(args, "indexsearch")) error_sum += test_indexsearch(summary);

#ifdef EXECUTABLE
    std::string exe(EXECUTABLE " buildinfo");
#else
    printf("WARNING: Set executable location in makefile using -DEXECUTABLE=meshtool. Defaulting to 'meshtool'.\n");
    std::string exe("meshtool buildinfo");
#endif

  FILE* fp = popen(exe.c_str(), "r");
  if (fp) {
    char buffer[1024];
    if (fgets(buffer, sizeof(buffer), fp) != NULL) {
      printf("\n\n%s\n", buffer);
      if (strstr(buffer, "PYMT") != NULL || true) {
        if(mode_enabled(args, "insert")) error_sum += test_pymt_insert_tags(summary);
        if(mode_enabled(args, "insert")) error_sum += test_pymt_insert_tagsurfs(summary);
        if(mode_enabled(args, "extract")) error_sum += test_pymt_extract_mesh(summary);
        if(mode_enabled(args, "extract")) error_sum += test_pymt_extract_volume(summary);
        // if(mode_enabled(args, "clean")) error_sum += test_pymt_clean_topology(summary);
        if(mode_enabled(args, "generate")) error_sum += test_pymt_generate_distancefield(summary);
        if(mode_enabled(args, "generate")) error_sum += test_pymt_generate_fibers(summary);
        if(mode_enabled(args, "generate")) error_sum += test_pymt_generate_mesh(summary);
        if(mode_enabled(args, "interpolate")) error_sum += test_pymt_interpolate(summary);
        if(mode_enabled(args, "extract")) error_sum += test_pymt_extract_gradient(summary);
        if(mode_enabled(args, "extract")) error_sum += test_pymt_extract_data(summary);
        if(mode_enabled(args, "extract")) error_sum += test_pymt_extract_tags(summary);
        if(mode_enabled(args, "extract")) error_sum += test_pymt_extract_myocard(summary);
        if(mode_enabled(args, "query")) error_sum += test_pymt_query(summary);
      }
      else
        puts("Skipping PyMT tests, not the PyMeshtool version of meshool !\n\n");
    }
    pclose(fp);
  } else {
    printf("Failed to run buildinfo command\n");
  }


  printf("%s", summary);
  printf("\n A total of %d errors occured\n", error_sum);
  return error_sum > 0;
}
