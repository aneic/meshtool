#ifndef MT_GENERATE_MODE_H_
#define MT_GENERATE_MODE_H_


#include "mt_modes_base.h"
#define BND_INC_DFLT 1.25
#define BND_STEP_DFLT 0.1

#include <set>
/**
 * @brief generates distancefield for mesh
 *
 * @param[in]  mesh         source mesh
 * @param[in]  start_vtx    vertices defining distance field start
 * @param[in]  end_vtx      vertices defining distance field end
 * @param[out] rvec         generated distancefield
 *
 * @return  true if mesh changed,
 *          false if mesh has not changed,
 */
void generate_distancefield_mode(
  mt_meshdata& mesh,
  mt_vector<mt_idx_t>& start_vtx,
  mt_vector<mt_idx_t>& end_vtx,
  mt_vector<mt_real>& rvec);


/**
 * @brief helper function for generate_mesh_mode to read all relevant data
 *
 * @param[in]  opts           struct holding various options to consider while reading data
 * @param[out] surfmeshes     surface meshes read from files
 * @param[out] tags           tags read and parsed from opts
 * @param[out] scale_factors  scaling factors read and parsed from opts
 * @param[out] holes_xyz      Hole seeding points that get passed to tetgen
 * @param[out] res            surface meshes read from files
 * @param[out] is_hole        vector holding bools defining whether a hole should be treated as one
 * @param[out] gen_msh_opts   generate mesh options defining how generate_mesh_mode will act
*/

void generate_mesh_mode_read(
  const struct generate_options& opts,
  mt_vector<mt_meshdata>& surfmeshes,
  mt_vector<mt_idx_t>& tags,
  mt_vector<mt_real>& scale_factors,
  mt_vector<mt_real>& res,
  mt_vector<bool>& is_hole,
  struct generate_mesh_options& gen_msh_opts);

/**
 * @brief generate a tetrahedral mesh from a list of nested triangle surfaces.
 * 
 * @param[in] surfmeshes     list of surface meshes to combine
 * @param[in] tags
 * @param[in] scale_factors  scale factors for each surface, if only one is provided, provided one is used for all surfaces
 * @param[in] holes_xyz      hole seeding points that get passed to tetgen
 * @param[in] res            is passed to netgen
 * @param[in] is_hole        whether specific surface is a hole
 * @param[in] gen_msh_opts   misc options for th generation of the mesh.
 * @param[out] output_mesh   final combined surface mesh
 *
 */

void generate_mesh_mode(
  mt_vector<mt_meshdata>& surfmeshes,
  mt_vector<mt_idx_t>& tags,
  mt_vector<mt_real>& scale_factors,
  mt_vector<mt_real>& holes_xyz,
  mt_vector<mt_real>& res,
  mt_vector<bool>& is_hole,
  struct generate_mesh_options& gen_msh_opts,
  mt_meshdata& output_mesh);




/**
 * @brief generates fibers for a given mesh
 *
 * @param[in, out]  mesh  mesh to generate fibres in
 * @param[in]       tags  
 * @param[in]       numfibers number of fibres to generate 
 */
void generate_fibers_mode(mt_meshdata &mesh, const std::set<mt_tag_t> &tags, const int numfibers);


enum gen_type { GEN_FIB, GEN_MESH, GEN_REGION, GEN_DIST, GEN_BBOX, GEN_SPLIT, GEN_SURFSPLIT, GEN_PURKINJE, GEN_EPSTATE};


/**
* @brief Options for generate mode
*/
struct generate_options {
  gen_type type;
  std::string msh_base;
  std::string inpmsh_base;
  std::string surf;
  std::string ssurf;
  std::string esurf;
  std::string outmsh_base;
  std::string ifmt;
  std::string ofmt;
  std::string holes;
  std::string ins_tag;
  std::string scale;
  std::string odat;
  std::string op;
  std::string boundary;
  std::string boundary_step;
  std::string boundary_inc;
  std::string boundary_pres;
  std::string boundary_symm;
  std::string mode;
  std::string lvsurf;
  std::string rvsurf;
  std::string uvcfile;
  std::string config;
  std::string out;
  std::string res;
  std::string res_outer;
  std::string padding;
  std::string qual;

  std::set<mt_tag_t> tags;
  bool verbose = false;
  bool tmpl_out = false;
  bool use_netgen = false;
};


struct generate_mesh_options {
  int boundary_layer       = 0;
  mt_real padding          = 0.0;
  mt_real boundary_step    = BND_STEP_DFLT;
  mt_real boundary_inc     = BND_INC_DFLT;
  bool boundary_symmetric  = false;
  bool preserve_boundary   = true;
  int qual_lvl             = 0;
  int sizing_mode          = 1;
  bool tags_provided       = false;
  bool use_netgen          = false;
};

#endif
