/**
* @file extract_mode.h
* @brief Meshtool extract mode.
*
* This mode allows to extract many entities (submeshes, surfaces, etc.) from a mesh.
*
* @author Aurel Neic
* @version
* @date 2016-12-13
*/

#include "data_structs.h"
#include "igb_utils.hpp"
#include "mt_modes_base.h"
#include "asciireader.hpp"
#include <cstddef>
#include "extract_mode.h"


#ifdef MT_ADDONS
#include "addons_utils.h"
#endif

static const std::string hyp_par = "-hybrid=";
static const std::string noflip_par = "-noflip";
static const std::string ang_thr_par = "-ang_thr=";
static const std::string tagfile_par = "-tag_file=";
/**
* @brief Extract mode options.
*/
struct extract_options {
  enum op_type type;
  std::string msh_base;
  std::string msh1_base;
  std::string msh2_base;
  std::string submsh_base;
  std::string msh_dat_file;
  std::string submsh_dat_file;
  std::string oper;
  std::string surf;
  std::string coord;
  std::string ifmt;
  std::string ofmt;
  std::string edge_thr;
  std::string rad;
  std::string lower_rad;
  std::string idat;
  std::string odat;
  std::string mode;
  std::string hybrid;
  std::string thr;
  std::string ang_thr;
  std::string tag_file;
  std::string vtx;
  std::set<int> tags;

  bool no_flip = false;
};

/**
* @brief Display extract mesh help message.
*/
void print_extract_mesh_help()
{
  fprintf(stderr, "extract mesh: a submesh is extracted from a given mesh based on given element tags\n");
  fprintf(stderr, "parameters:\n");
  fprintf(stderr, "%s<path>\t\t (input) path to basename of the mesh to extract from\n", mesh_par.c_str());
  fprintf(stderr, "%stag1%ctag2\t\t (input) \"%c\"-seperated list of tags\n", tags_par.c_str(), tag_separator, tag_separator);
  fprintf(stderr, "%s<path>\t (optional) path to an alternative tag file {*.tags, *.btags}.\n", tagfile_par.c_str());
  fprintf(stderr, "%s<path>\t\t (output) path to basename of the submesh to extract to\n", submesh_par.c_str());
  fprintf(stderr, "%s<format>\t\t (optional) mesh input format.\n", inp_format_par.c_str());
  fprintf(stderr, "%s<format>\t\t (optional) mesh output format.\n\n", out_format_par.c_str());
  fprintf(stderr, "The supported input formats are:\n%s\n", input_formats.c_str());
  fprintf(stderr, "The supported output formats are:\n%s\n", output_formats.c_str());
  fprintf(stderr, "\n");
}

/**
* @brief Display extract data help message.
*/
void print_extract_data_help()
{
  fprintf(stderr, "extract data: data defined on a mesh is extracted for a given submesh\n");
  fprintf(stderr, "parameters:\n");
  fprintf(stderr, "%s<path>\t\t (input) path to basename of the mesh\n", mesh_par.c_str());
  fprintf(stderr, "%s<path>\t\t (input) path to basename of the submesh\n", submesh_par.c_str());
  fprintf(stderr, "%s<path>\t (input) file the data is extracted from.\n", mesh_data_par.c_str());
  fprintf(stderr, "%s<path>\t (output) file the data is extracted into\n", submesh_data_par.c_str());
  fprintf(stderr, "%s<int>\t\t (optional) Data mode. 0 = nodal, 1 = element. Default is 0.\n", mode_par.c_str());
  fprintf(stderr, "\n");
  fprintf(stderr, "Note that the files defining the submesh must include a *.eidx and a *.nod file.\n"
                  "This files define how elements and nodes of the submesh map back into the original mesh.\n"
                  "The *.eidx and *.nod files are generated when using the \"extract mesh\" mode.\n");
  fprintf(stderr, "\n");
}
/**
* @brief Display extract data help message.
*/
void print_extract_vtkdata_help()
{
  fprintf(stderr, "extract vtkdata: extract all VTK datasets in a VTK file\n");
  fprintf(stderr, "parameters:\n");
  fprintf(stderr, "%s<path>\t (input) path to basename of the VTK file\n", mesh_par.c_str());
  fprintf(stderr, "%s<path>\t (output) basename of the output data.\n", odat_par.c_str());
  fprintf(stderr, "\n");
}

/**
* @brief Display extract surface help message.
*/
void print_extract_surf_help()
{
  fprintf(stderr, "extract surface: extract a sequence of surfaces defined by set operations on element tags\n");
  fprintf(stderr, "parameters:\n");
  fprintf(stderr, "%s<path>\t\t (input) path to basename of the mesh\n", mesh_par.c_str());
  fprintf(stderr, "%s<path>\t\t (output) list of names associated to the given operations\n", surf_par.c_str());
  fprintf(stderr, "%soperations\t\t (optional) list of operations to perform. By default, the\n"
                  "\t\t\t surface of the full mesh is computed.\n", oper_par.c_str());
  fprintf(stderr, "%s<path>\t (optional) path to an alternative tag file {*.tags, *.btags}.\n", tagfile_par.c_str());
  fprintf(stderr, "%s<deg. angle>\t (optional) if set, sharp edges will block surface traversal.\n"
                  "\t\t\t A sharp edge is defined by the nodes which connect elements\n"
                  "\t\t\t with normal vectors at angles above the given threshold.\n", edge_par.c_str());
  fprintf(stderr, "%s<deg. angle>\t (optional) if set, surface traversal stops when angle between\n"
                  "\t\t\t current and starting normal vectors exceeds threshold.\n", ang_thr_par.c_str());
  fprintf(stderr, "%s<xyz>:<xyz>:..\t (optional) restrict surfaces to those elements reachable by\n"
                  "\t\t\t surface edge-traversal from the surface vertices closest to the given\n"
                  "\t\t\t coordinates. If %s is also provided, sharp edges will block\n"
                  "\t\t\t traversal, thus limit what is reachable.\n", coord_par.c_str(), edge_par.c_str());
  fprintf(stderr, "%s<float>\t\t (optional) surface edge-traversal is limited to the given\n"
                  "\t\t\t radius from the initial index.\n", size_par.c_str());
  fprintf(stderr, "%s<float>\t (optional) surface edge-traversal limitation lower size (for extracting bands).\n", lower_size_par.c_str());
  fprintf(stderr, "%s<int>\t\t (optional) Write hybrid quad + tri surfaces. 1 == on, 0 == off. 0 is default.\n", hyp_par.c_str());
  fprintf(stderr, "%s\t\t\t (optional) If set, triangle winding is *not* inverted (e.g. for meshalyzer).\n", noflip_par.c_str());
  fprintf(stderr, "%s<format>\t\t (optional) mesh input format.\n", inp_format_par.c_str());
  fprintf(stderr, "%s<format>\t\t (optional) mesh output format. If set, the surfaces will also\n"
                  "\t\t\t be written as surface meshes.\n\n", out_format_par.c_str());
  fprintf(stderr, "The supported input formats are:\n%s\n", input_formats.c_str());
  fprintf(stderr, "The supported output formats are:\n%s\n", output_formats.c_str());
  fprintf(stderr, "\n");
  fprintf(stderr, "The format of the operations is:\n");
  fprintf(stderr, "tagA1,tagA2,[surfA1,surfA2..]..[+-:]tagB1,tagB2,[surfB1..]..;tagA1,..[+-:]tagB1..;..\n");
  fprintf(stderr, "Tag regions separated by \",\" will be unified into submeshes and their surface computed.\n"
                  "Alternatively, surfaces can be provided directly by .surf surface files (only basename, no extension).\n"
                  "If two surfaces are separated by \"-\", the rhs surface will be removed from the\n"
                  "lhs surface (set difference). Similarly, using \"+\" will compute the surface union.\n"
                  "If the submeshes are separated by \":\", the set intersection of the two submesh surfaces will be computed.\n"
                  "Individual operations are separated by \";\".\n\n");
  fprintf(stderr, "The number of names provided with \"%s\" must match the number of operations. If no operations are provided,\n"
                  "the surface of the whole geometry will be extracted. Individual names are separated by \",\".\n\n", surf_par.c_str());
  fprintf(stderr, "Further restrictions can be added to the surface extraction with the %s , %s , %s options.\n", edge_par.c_str(), coord_par.c_str(), size_par.c_str());
  fprintf(stderr, "\n");
}

/**
* @brief Display extract myocard help message.
*/
void print_extract_myo_help()
{
  fprintf(stderr, "extract myocard: the myocardium is extracted from a given mesh. The myocard is identified based on non-zero fibers.\n");
  fprintf(stderr, "parameters:\n");
  fprintf(stderr, "%s<path>\t (input) path to basename of the mesh to extract from\n", mesh_par.c_str());
  fprintf(stderr, "%s<path>\t (output) path to basename of the submesh to extract to\n", submesh_par.c_str());
  fprintf(stderr, "%s<format>\t (optional) mesh input format.\n", inp_format_par.c_str());
  fprintf(stderr, "%s<format>\t (optional) mesh output format.\n", out_format_par.c_str());
  fprintf(stderr, "%s<format>\t (optional) fiber threshold value (default=0.0).\n\n", out_format_par.c_str());
  fprintf(stderr, "The supported input formats are:\n%s\n", input_formats.c_str());
  fprintf(stderr, "The supported output formats are:\n%s\n", output_formats.c_str());
  fprintf(stderr, "\n");
}

/**
* @brief Display extract volume help message.
*/
void print_extract_volume_help()
{
  fprintf(stderr, "extract volume: extract elements inside a given box volume.\n");
  fprintf(stderr, "parameters:\n");
  fprintf(stderr, "%s<path>\t\t (input) path to basename of the mesh to extract from\n", mesh_par.c_str());
  fprintf(stderr, "%sx,xd,y,yd,z,zd\t (input) volume definition\n", coord_par.c_str());
  fprintf(stderr, "%s<path>\t\t (output) path to basename of the submesh to extract to\n", submesh_par.c_str());
  fprintf(stderr, "%s<int>\t\t (optional) mesh output mode. 0 = submesh, 1 = vtx file. Default is 0.\n", mode_par.c_str());
  fprintf(stderr, "%s<format>\t\t (optional) mesh input format.\n", inp_format_par.c_str());
  fprintf(stderr, "%s<format>\t\t (optional) mesh output format.\n\n", out_format_par.c_str());
  fprintf(stderr, "The supported input formats are:\n%s\n", input_formats.c_str());
  fprintf(stderr, "The supported output formats are:\n%s\n", output_formats.c_str());
  fprintf(stderr, "\n");
}

/**
* @brief Display extract unreachable help message.
*/
void print_extract_unreachable_help()
{
  fprintf(stderr, "extract unreachable: decompose mesh into parts based on mesh connectivity.\n");
  fprintf(stderr, "parameters:\n");
  fprintf(stderr, "%s<path>\t (input) path to basename of the mesh to extract from\n", mesh_par.c_str());
  fprintf(stderr, "%s<path>\t (output) path to basename of the submesh to extract to\n", submesh_par.c_str());
  fprintf(stderr, "%s<int>\t (optional) extract mode. 0 = all, 1 = largest, 2 = smallest. Default is 0.\n", mode_par.c_str());
  fprintf(stderr, "%s<format>\t (optional) mesh input format.\n", inp_format_par.c_str());
  fprintf(stderr, "%s<format>\t (optional) mesh output format.\n\n", out_format_par.c_str());
  fprintf(stderr, "The supported input formats are:\n%s\n", input_formats.c_str());
  fprintf(stderr, "The supported output formats are:\n%s\n", output_formats.c_str());
  fprintf(stderr, "\n");
}

void print_extract_gradient_help()
{
  fprintf(stderr, "extract gradient: compute gradient and gradient magnitude of a scalar function on a mesh.\n");
  fprintf(stderr, "parameters:\n");
  fprintf(stderr, "%s<path>\t (input) path to basename of the mesh to extract from\n", mesh_par.c_str());
  fprintf(stderr, "%s<path>\t (input) path to input data. Also specify file extension.\n", idat_par.c_str());
  fprintf(stderr, "%s<path>\t (output) path to output data.\n", odat_par.c_str());
  fprintf(stderr, "%s<int>\t (optional) output mode. 0 == nodal output, 1 == element output. 0 is default.\n", mode_par.c_str());
  fprintf(stderr, "%s<format>\t (optional) mesh input format.\n\n", inp_format_par.c_str());
  fprintf(stderr, "The supported input formats are:\n%s\n", input_formats.c_str());
  fprintf(stderr, "\n");
}

void print_extract_overlap_help()
{
  fprintf(stderr, "extract overlap: extract the elements of mesh1 that overlap with mesh2.\n");
  fprintf(stderr, "parameters:\n");
  fprintf(stderr, "%s<path>\t (input) path to basename of mesh1.\n", mesh1_par.c_str());
  fprintf(stderr, "%s<path>\t (input) path to basename of mesh2.\n", mesh2_par.c_str());
  fprintf(stderr, "%s<path>\t (output) path to submesh (the overlaping elements).\n", submesh_par.c_str());
  fprintf(stderr, "%s<int>\t (optional) output mode. 0 == only overlap, 1 == overlap and complement.\n"
                  "\t\t 0 is default.\n", mode_par.c_str());
  fprintf(stderr, "%s<float>\t (optional) overlap region radius, in multiples of the average mesh1 edge length.\n"
                  "\t\t default is 2.0.\n", size_par.c_str());
  fprintf(stderr, "%s<format>\t (optional) mesh input format.\n", inp_format_par.c_str());
  fprintf(stderr, "%s<format>\t (optional) mesh output format.\n\n", out_format_par.c_str());
  fprintf(stderr, "The supported input formats are:\n%s\n", input_formats.c_str());
  fprintf(stderr, "The supported output formats are:\n%s\n", output_formats.c_str());
  fprintf(stderr, "\n");
}

void print_extract_tags_help()
{
  fprintf(stderr, "extract tags: extract the elements tags into an element data vector.\n");
  fprintf(stderr, "parameters:\n");
  fprintf(stderr, "%s<path>\t (input) path to basename of the mesh.\n", mesh_par.c_str());
  fprintf(stderr, "%s<path>\t (input) .dat file the tags are extracted to.\n", odat_par.c_str());
  fprintf(stderr, "%s<format>\t (optional) mesh input format.\n", inp_format_par.c_str());
  fprintf(stderr, "The supported input formats are:\n%s\n", input_formats.c_str());
  fprintf(stderr, "\n");
}

void print_extract_fib_help()
{
  fprintf(stderr, "extract fibers: extract the elements fibers/sheets into element data vectors.\n");
  fprintf(stderr, "parameters:\n");
  fprintf(stderr, "%s<path>\t (input) path to basename of the mesh.\n", mesh_par.c_str());
  fprintf(stderr, "%s<path>\t (optional) basename of .dat file(s) the fibers are extracted to. By default mesh basename is used.\n", odat_par.c_str());
  fprintf(stderr, "%s<int>\t (optional) output mode. 0 == fibers and sheets, 1 == only fibers, 2 == only sheets. 0 is default.\n", mode_par.c_str());
  fprintf(stderr, "%s<format>\t (optional) mesh input format.\n", inp_format_par.c_str());
  fprintf(stderr, "The supported input formats are:\n%s\n", input_formats.c_str());
  fprintf(stderr, "\n");
}

void print_extract_adjustment_help()
{
  fprintf(stderr, "extract adjustment: extract nodal adjustment file from .dat and .vtx files.\n");
  fprintf(stderr, "parameters:\n");
  fprintf(stderr, "%s<path>\t (input) path to .dat file.\n", idat_par.c_str());
  fprintf(stderr, "%s<path>\t (input) path to vertices file. May be .vtx or .nod.\n", vtx_par.c_str());
  fprintf(stderr, "%s<0|1>\t (optional) output mode, 0 = no header, 1 = with header. default is 1.\n", mode_par.c_str());
  fprintf(stderr, "%s<path>\t (output) path to adjustment file.\n", odat_par.c_str());
  fprintf(stderr, "\n");
}

void print_extract_vtxhalo_help()
{
  fprintf(stderr, "extract vtxhalo: extract connected halo vertex sets for a given block vertex set.\n");
  fprintf(stderr, "parameters:\n");
  fprintf(stderr, "%s<path>\t\t (input) path to basename of the mesh.\n", mesh_par.c_str());
  fprintf(stderr, "%s<format>\t\t (optional) mesh input format.\n", inp_format_par.c_str());
  fprintf(stderr, "%s<path>\t\t (input) path to the block vertex file. May be .vtx or .nod.\n", vtx_par.c_str());
  fprintf(stderr, "%stag1%ctag2,..\t (optional) \"%c\"-seperated list of tags of regions to exclude from the mesh.\n", tags_par.c_str(), tag_separator, tag_separator);
  fprintf(stderr, "%s<path>\t (optional) path to an alternative tag file {*.tags, *.btags}.\n", tagfile_par.c_str());
  fprintf(stderr, "The supported input formats are:\n%s\n", input_formats.c_str());
  fprintf(stderr, "\n");
  fprintf(stderr, "The output files are named\n"
                  "   <vtxbase>_skipped.vtx   block vertices which are not in the extracted mesh (not written if empty)\n"
                  "   <vtxbase>_halo.vtx      all halo vertices of the block (not written if empty)\n"
                  "   <vtxbase>_halo*.vtx     connected halo vertex set ('*' is the corresponding domain index)\n"
                  "where <vtxbase> is the basename of the vertex file!\n");
  fprintf(stderr, "\n");
}

#ifdef MT_ADDONS
void print_extract_isosurf_help()
{
  fprintf(stderr, "extract isosurf: extract an isosurface.\n");
  fprintf(stderr, "parameters:\n");
  fprintf(stderr, "%s<path>\t (input) path to basename of mesh.\n", mesh_par.c_str());
  fprintf(stderr, "%s<path>\t (input) path to real data.\n", idat_par.c_str());
  fprintf(stderr, "%s<float>\t (input) value to compute isosurf for.\n", thr_par.c_str());
  fprintf(stderr, "%s<name>\t (output) path to basename of output surface.\n", surf_par.c_str());
  fprintf(stderr, "\n");
}
#endif

/**
* @brief Parse extract mode options.
*
* @param [in]  argc Arguments count.
* @param [in]  argv Arguments string-array.
* @param [out] opts Options structure.
*
* @return 0 for success, >0 otherwise.
*/
int extract_parse_options(int argc, char** argv, struct extract_options & opts)
{
  if(argc < 3) {
    std::cerr << "Please choose one of the following extract modes: " << std::endl << std::endl;
    print_extract_mesh_help();
    print_extract_data_help();
    print_extract_vtkdata_help();
    print_extract_surf_help();
    print_extract_myo_help();
    print_extract_volume_help();
    print_extract_unreachable_help();
    print_extract_gradient_help();
    print_extract_overlap_help();
    print_extract_tags_help();
    print_extract_fib_help();
    print_extract_adjustment_help();
    print_extract_vtxhalo_help();
#ifdef MT_ADDONS
    print_extract_isosurf_help();
#endif
    return 1;
  }

  std::string extract_type = argv[2];

  // parse all extract parameters -----------------------------------------------------------------
  for(int i=3; i<argc; i++){
    std::string param = reformat_param(argv, i, argc);
    bool match = false;

    if(!match) match = parse_param(param, mesh_par, opts.msh_base);
    if(!match) match = parse_param(param, mesh1_par, opts.msh1_base);
    if(!match) match = parse_param(param, mesh2_par, opts.msh2_base);
    if(!match) match = parse_param(param, submesh_par, opts.submsh_base);
    if(!match) match = parse_param(param, mesh_data_par, opts.msh_dat_file, chk_fexists);
    if(!match) match = parse_param(param, submesh_data_par, opts.submsh_dat_file);
    if(!match) match = parse_param(param, oper_par, opts.oper);
    if(!match) match = parse_param(param, surf_par, opts.surf);
    if(!match) match = parse_param(param, inp_format_par, opts.ifmt);
    if(!match) match = parse_param(param, out_format_par, opts.ofmt);
    if(!match) match = parse_param(param, coord_par, opts.coord);
    if(!match) match = parse_param(param, edge_par, opts.edge_thr);
    if(!match) match = parse_param(param, ang_thr_par, opts.ang_thr);
    if(!match) match = parse_param(param, size_par, opts.rad);
    if(!match) match = parse_param(param, lower_size_par, opts.lower_rad);
    if(!match) match = parse_param(param, idat_par, opts.idat, chk_fexists);
    if(!match) match = parse_param(param, odat_par, opts.odat);
    if(!match) match = parse_param(param, mode_par, opts.mode);
    if(!match) match = parse_param(param, thr_par, opts.thr);
    if(!match) match = parse_param(param, hyp_par, opts.hybrid);
    if(!match) match = parse_param(param, vtx_par, opts.vtx);
    if(!match) match = parse_param(param, tagfile_par, opts.tag_file, chk_fexists);
    if(!match) match = parse_flag(param, noflip_par, opts.no_flip);


    if( (!match) && param.compare(0, tags_par.size(), tags_par) == 0)
    {
      std::string tags_str;
      tags_str.assign(param.begin()+tags_par.size(), param.end());
      mt_vector<std::string> taglist;
      split_string(tags_str, tag_separator, taglist);
      for(size_t t=0; t<taglist.size(); t++)
        opts.tags.insert( atoi(taglist[t].c_str()) );
      match = true;
    }

    if(!match) {
      std::cerr << "Error: Cannot parse parameter " << param << std::endl;
      return 2;
    }
  }
  fixBasename(opts.surf);

  // check if all relevant parameters have been set ---------------------------------------------------
  if(extract_type.compare("mesh") == 0)
  {
    opts.type = OP_MESH;

    if( !(opts.msh_base.size() > 0 && opts.submsh_base.size() > 0 && opts.tags.size() > 0) )
    {
      std::cerr << "Mesh extract error: Insufficient parameters provided." << std::endl;
      print_extract_mesh_help();
      return 4;
    }
  }
  else if(extract_type.compare("data") == 0)
  {
    bool submsh_set = opts.submsh_base.size() > 0, submsh_data_set = opts.submsh_dat_file.size() > 0,
         msh_data_set = opts.submsh_dat_file.size() > 0;

    opts.type = OP_DATA;

    if( !(submsh_set && msh_data_set && submsh_data_set) )
    {
      std::cerr << "Data extract error: Insufficient parameters provided." << std::endl;
      print_extract_data_help();
      return 4;
    }
  }
  else if(extract_type.compare("vtkdata") == 0)
  {
    bool msh_set = opts.msh_base.size(), odat_set = opts.odat.size();

    opts.type = OP_VTKDATA;

    if( !(msh_set && odat_set) )
    {
      std::cerr << "vtkdata extract error: Insufficient parameters provided." << std::endl;
      print_extract_vtkdata_help();
      return 4;
    }
  }
  else if(extract_type.compare("surface") == 0)
  {
    opts.type = OP_SURF;
    if(! (opts.msh_base.size() > 0 && opts.surf.size() > 0))
    {
      std::cerr << "Surface extract error: Insufficient parameters provided." << std::endl;
      print_extract_surf_help();
      return 4;
    }
  }
  else if(extract_type.compare("myocard") == 0)
  {
    opts.type = OP_MYO;
    if(! (opts.msh_base.size() > 0 && opts.submsh_base.size() > 0))
    {
      std::cerr << "Myocard extract error: Insufficient parameters provided." << std::endl;
      print_extract_myo_help();
      return 4;
    }
  }
  else if(extract_type.compare("volume") == 0)
  {
    opts.type = OP_VOL;
    if(! (opts.msh_base.size() > 0 && opts.submsh_base.size() > 0 && opts.coord.size() > 0) )
    {
      std::cerr << "Volume extract error: Insufficient parameters provided." << std::endl;
      print_extract_volume_help();
      return 4;
    }
  }
  else if(extract_type.compare("unreachable") == 0)
  {
    opts.type = OP_UR;
    if(! (opts.msh_base.size() > 0 && opts.submsh_base.size() > 0 ) )
    {
      std::cerr << "Unreachables extract error: Insufficient parameters provided." << std::endl;
      print_extract_unreachable_help();
      return 4;
    }
  }
  else if(extract_type.compare("gradient") == 0)
  {
    opts.type = OP_GRAD;
    if(opts.msh_base.size() == 0 || opts.idat.size() == 0 || opts.odat.size() == 0 )
    {
      std::cerr << "Gradient extract error: Insufficient parameters provided." << std::endl;
      print_extract_gradient_help();
      return 4;
    }
  }
  else if(extract_type.compare("overlap") == 0)
  {
    opts.type = OP_OVERLP;
    if(opts.msh1_base.size() == 0 || opts.msh2_base.size() == 0 || opts.submsh_base.size() == 0 )
    {
      std::cerr << "Overlap extract error: Insufficient parameters provided." << std::endl;
      print_extract_overlap_help();
      return 4;
    }
  }
  else if(extract_type.compare("tags") == 0)
  {
    opts.type = OP_TAGS;
    if(opts.msh_base.size() == 0 || opts.odat.size() == 0)
    {
      std::cerr << "Tags extract error: Insufficient parameters provided." << std::endl;
      print_extract_tags_help();
      return 4;
    }
  }
  else if(extract_type.compare("fibers") == 0)
  {
    opts.type = OP_FIB;
    if(opts.msh_base.size() == 0)
    {
      std::cerr << "Fibers extract error: Insufficient parameters provided." << std::endl;
      print_extract_fib_help();
      return 4;
    }
  }
  else if(extract_type.compare("adjustment") == 0) {
    opts.type = OP_ADJ;
    if(!(opts.idat.size() && opts.vtx.size() && opts.odat.size()))
    {
      std::cerr << "Extract adjustment error: Insufficient parameters provided." << std::endl;
      print_extract_adjustment_help();
      return 4;
    }
  }
  else if(extract_type.compare("vtxhalo") == 0) {
    opts.type = OP_HALO;
    if(!(opts.msh_base.size() && opts.vtx.size()))
    {
      std::cerr << "Extract vtxhalo error: Insufficient parameters provided." << std::endl;
      print_extract_vtxhalo_help();
      return 4;
    }
  }
#ifdef MT_ADDONS
  else if(extract_type.compare("isosurf") == 0)
  {
    opts.type = OP_ISOSURF;
    if(opts.msh_base.size() == 0 || opts.idat.size() == 0 || opts.surf.size() == 0 ||
       opts.thr.size() == 0)
    {
      std::cerr << "Isosurface extract error: Insufficient parameters provided." << std::endl;
      print_extract_isosurf_help();
      return 4;
    }
  }
#endif
  else {
    print_usage(argv[0]);
    return 2;
  }

  return 0;
}


// ----------------------------------------------------------------------------
// read_uccs
void read_uccs(mt_vector<mt_real>& uccs, const std::string& file_name)
{
  constexpr int check_intervall = 100;

  const int bufsize = 2048;
  char buffer[bufsize];
  unsigned long int numpts = 0;

  asciireader reader;
  reader.read_file(file_name);

  if(reader.get_line(0, buffer, bufsize))
    sscanf(buffer, "%lu", &numpts);
  uccs.resize(4*numpts);

  PROGRESS<size_t> prg(numpts, "Reading UCC points: ");
  float pts[3], v;

  size_t num_read = 0;
  for (size_t i=0; i<numpts; i++)
  {
    if (reader.get_line(i+1, buffer, bufsize)) {
      sscanf(buffer, "%f %f %f %f", pts, pts+1, pts+2, &v);

      uccs[4*i+0] = pts[0]; // z   / alpha
      uccs[4*i+1] = pts[1]; // rho / beta
      uccs[4*i+2] = pts[2]; // phi / gamma
      uccs[4*i+3] = v; // ventricle
      num_read++;
    }
    if (num_read == check_intervall) {
      prg.increment(num_read);
      num_read = 0;
    }
  }
  prg.finish();
} // read_uccs(...)


// ----------------------------------------------------------------------------
// write_uccs
bool write_uccs(const mt_vector<mt_real>& uccs,
                const std::string& file_name)
{
  const size_t size = uccs.size();
  if ((size % 4) != 0)
    return false;
  const size_t num_coords = size / 4;

  FILE* fp = fopen(file_name.c_str(), "w");
  if (!fp)
    return false;

  fprintf(fp, "%zu\n", num_coords);
  for (size_t i=0; i<num_coords; i++)
    fprintf(fp, "%lf %lf %lf %lf\n", uccs[4*i+0], uccs[4*i+1], uccs[4*i+2], uccs[4*i+3]);
  fclose(fp);
  return true;
} // write_uccs(...)



/**
* @brief Extract mode function.
*
* @param [in]  argc Arguments count.
* @param [in]  argv Arguments string-array.
*/
void extract_mode(int argc, char** argv)
{
  struct extract_options opts;
  int ret = extract_parse_options(argc, argv, opts);
  mt_timer timer;

  if(ret != 0) return;

  // we "centralize" the mesh reading to reduce code length ------------------------------
  struct mt_meshdata mesh;
  mt_filename mshfile(opts.msh_base, opts.ifmt);

  if(opts.type != OP_DATA   &&
     opts.type != OP_OVERLP &&
     opts.type != OP_ADJ    &&
     opts.type != OP_VTKDATA)
  {
    std::cout << "Reading mesh: " << mshfile.base << std::endl;
    timer.start();
    read_mesh_selected(mesh, mshfile.format, mshfile.base);
    timer.stop();
    std::cout << "Done in " << timer.get_time_sec() << " sec" << std::endl;
  }

  switch(opts.type) {

    case OP_MESH: {
      // extract mesh  ------------------------------------------------------------
      if (opts.tag_file.size())
        readElementTags_general(mesh.etags, opts.tag_file);

      std::cout << "Extracting tags: ";
      for(std::set<int>::iterator it = opts.tags.begin(); it != opts.tags.end(); ++it)
        std::cout << *it << " ";

      std::cout << std::endl;
      timer.start();

      // run extraction
      mt_vector<mt_idx_t> nod, eidx;
      extract_mesh_mode(mesh, opts.tags, nod, eidx);

      timer.stop();
      std::cout << "Done in " << timer.get_time_sec() << " sec" << std::endl;

      mt_filename submshfile(opts.submsh_base, opts.ofmt);
      std::cout << "Writing mesh: " << submshfile.base << std::endl;
      timer.start();
      write_mesh_selected(mesh, submshfile.format, submshfile.base);
      binary_write(nod.begin(), nod.end(), submshfile.base + NOD_EXT);
      binary_write(eidx.begin(), eidx.end(), submshfile.base + EIDX_EXT);
      timer.stop();
      std::cout << "Done in " << timer.get_time_sec() << " sec" << std::endl;
      break;
    }

    case OP_DATA: {
      timer.start();
      // extract data  ------------------------------------------------------------
      bool nodal_data = true;
      if(opts.mode.size()) nodal_data = atoi(opts.mode.c_str()) != 1;

      mt_vector<mt_idx_t> indices;
      std::string indices_file;
      if(nodal_data)
        indices_file = opts.submsh_base + NOD_EXT;
      else
        indices_file = opts.submsh_base + EIDX_EXT;

      std::cout << "Reading " << indices_file << std::endl;
      binary_read(indices, indices_file);

      // read and insert data
      short data_idx = -1;
      mt_vector<mt_real> idat, odat;
      igb_header igb_msh, igb_submsh;

      setup_data_format(opts.msh_dat_file, data_idx, igb_msh);
      init_igb_header(igb_msh, opts.submsh_dat_file, igb_submsh);
      if(data_idx < 0) exit(EXIT_FAILURE);

      switch(data_idx) {
        case 0:
          read_vector(idat, opts.msh_dat_file, true);
          break;
        case 2:
          read_vector_ascii(idat, opts.msh_dat_file, true);
          break;
        case 1:
        case 3:
          igb_submsh.v_x = indices.size();
          std::cout << "extracting igb data .. " << std::endl
                    << "  v_t in: " << igb_msh.v_t << ", v_t out: " << igb_submsh.v_t << std::endl
                    << "  v_x in: " << igb_msh.v_x << ", v_x out: " << igb_submsh.v_x << std::endl;
          write_igb_header(igb_submsh);
          break;
        case 10:
         read_uccs(idat, opts.msh_dat_file);
         break;
        default:
         break;
      }

      extract_data_mode(data_idx, idat, odat, indices, igb_msh, igb_submsh);

      switch(data_idx) {
        case 0:
          write_vector(odat, opts.submsh_dat_file, 1);
          break;
        case 2:
          write_vector_ascii(odat, opts.submsh_dat_file, 3);
          break;
        case 10:
          write_uccs(odat, opts.submsh_dat_file);
          break;
        default:
          break;
      }

      timer.stop();
      std::cout << "Done in " << timer.get_time_sec() << " sec" << std::endl;

      break;
    }

    case OP_VTKDATA: {
      if(!endswith(opts.msh_base, VTK_EXT))
        opts.msh_base += VTK_EXT;

      if(!file_exists(opts.msh_base)) {
        fprintf(stderr, "extract vtkdata error: file %s does not exist!\n", opts.msh_base.c_str());
        exit(EXIT_FAILURE);
      }

      vtk_data data;
      readVTKdata(opts.msh_base, data);

      for(vtk_data::entry & e : data.entries) {
        if(e.prop & vtk_data::prop_int) {
          int* start = (int*) e.data.data(), *end = (int*)e.data.end();
          mt_vector<int> d; d.assign(start, end);

          bool is_vec = (e.prop & vtk_data::prop_vector);
          std::string outfile = opts.odat + "_" + e.name + (is_vec ? VEC_EXT : DAT_EXT);
          write_vector_ascii(d, outfile,  is_vec ? 3 : 1);
        } else {
          float* start = (float*) e.data.data(), *end = (float*)e.data.end();
          mt_vector<float> d; d.assign(start, end);

          bool is_vec = (e.prop & vtk_data::prop_vector);
          std::string outfile = opts.odat + "_" + e.name + (is_vec ? VEC_EXT : DAT_EXT);
          write_vector_ascii(d, outfile,  is_vec ? 3 : 1);
        }
      }

      break;
    }

    case OP_SURF: {
      timer.start();

      if (opts.tag_file.size())
        readElementTags_general(mesh.etags, opts.tag_file);

      float edge_thr = opts.edge_thr.size() > 0 ? atof(opts.edge_thr.c_str()) : 0.0f;
      float ang_thr  = opts.ang_thr.size()  > 0 ? atof(opts.ang_thr.c_str())  : 0.0f;

      if(edge_thr && ang_thr) {
        fprintf(stderr, "%s error: %s and %s cannot be set at the same time! Aborting!\n",
                __func__, edge_par.c_str(), ang_thr_par.c_str());
        exit(EXIT_FAILURE);
      }

      mt_vector<std::string> coords;

      if(opts.coord.size())
        split_string(opts.coord, ':' , coords);

      // some checks regarding edge_thr and ang_thr
      if(ang_thr && coords.size() != 1) {
        fprintf(stderr, "%s error: %s requires exactly one starting coordinate! Aborting!\n",
                __func__, ang_thr_par.c_str());
        exit(EXIT_FAILURE);
      }

      int npts = *(std::max_element(mesh.e2n_con.begin(), mesh.e2n_con.end())) + 1;
      // we need full mesh connectivity as we might need to recover surface info
      std::cout << "Setting up n2e / n2n graphs .. " << std::endl;
      compute_full_mesh_connectivity(mesh, mshfile.base);

      // extract surface names
      mt_vector<std::string> names, operlist;
      split_string(opts.surf, ',', names);
      split_string(opts.oper, ';', operlist);

      if(names.size() != operlist.size()) {
          fprintf(stderr, "Error: Number of provided names does not match number of operations!\n");
          return;
      }

      bool hybrid_output = false;
      if(opts.hybrid.size())
        hybrid_output = atoi(opts.hybrid.c_str()) == 1;

      int numoper = std::max(operlist.size(), (size_t)1);

      mt_real rad = -1, lower_rad = -1;
      if(opts.rad.size() > 0) {
        // we traverse only a given distance
        rad = atof(opts.rad.c_str());
      }
      if(opts.lower_rad.size() > 0) {
        // we traverse only a given distance
        lower_rad = atof(opts.lower_rad.c_str());
      }

      mt_vector<mt_meshdata> surf_mesh;
      mt_vector<nbc_data> nbc;
      extract_surface_mode(edge_thr, ang_thr, coords, mesh, opts.oper, hybrid_output, rad, lower_rad, surf_mesh, nbc);

      // currently neubc files are only set up for triangle surf elems, thus we
      // dont write them for hybrid surfs
      for (int i = 0; i < numoper; i++) {
        bool write_nbc = !hybrid_output;
        write_surf_info(surf_mesh[i], write_nbc ? &(nbc[i]) : NULL,
            mesh.e2n_cnt.size(), npts, names[i]);

        if (opts.ofmt.size() > 0) {
          mt_vector<mt_idx_t> vtx;
          vtx.assign(surf_mesh[i].e2n_con.begin(), surf_mesh[i].e2n_con.end());
          binary_sort(vtx);
          unique_resize(vtx);

          // nbc_data_into_surface changes the surface numbering! we must write all
          // indexing relative to the original mesh to disk before this point!!
          std::string surfmesh_file = names[i] + ".surfmesh";
          nbc_data_into_surface(mesh, nbc[i], surf_mesh[i], !opts.no_flip);

          std::cout << "Writing surface mesh " << surfmesh_file << std::endl;
          write_mesh_selected(surf_mesh[i], opts.ofmt, surfmesh_file);

          surfmesh_file += NOD_EXT;
          binary_write(vtx.begin(), vtx.end(), surfmesh_file);
        }
      }

      timer.stop();
      std::cout << "Done in " << timer.get_time_sec() << " sec" << std::endl;

      break;
    }

#ifdef MT_ADDONS
    case OP_ISOSURF:
    {
      compute_full_mesh_connectivity(mesh, mshfile.base);

      // read data
      mt_vector<mt_real> data;
      read_vector_ascii(data, opts.idat);

      // read threshold value
      mt_real thr = atof(opts.thr.c_str());

      mt_meshdata surf;
      MT_USET<mt_idx_t> skip_tags;
      mt_vector<mt_idx_t> elem_orig;
      compute_crinkled_isosurf(mesh, skip_tags, data, thr, surf, elem_orig, NULL);

      std::string surfname = opts.surf + SURF_EXT;

      std::cout << "Writing surface " << surfname << std::endl;
      write_surf(surf.e2n_cnt, surf.e2n_con, surfname);

      surf.xyz = mesh.xyz;
      reindex_nodes(surf);

      surfname = opts.surf + ".surfmesh";
      std::cout << "Writing mesh " << surfname << std::endl;
      write_mesh_selected(surf, "vtk_bin", surfname);
      break;
    }
#endif

    case OP_MYO:
    {
      // extract myocard --------------------------------------------------------------
      std::cout << "Extracting myocardium .." << std::endl;
      timer.start();
      mt_vector<mt_idx_t> nod, eidx;

      const mt_real thr = (opts.thr.size() > 0) ? atof(opts.thr.c_str()) : 0.0;
      extract_myocard_mode(mesh, nod, eidx, thr);

      timer.stop();
      std::cout << "Done in " << timer.get_time_sec() << " sec" << std::endl;

      mt_filename submshfile(opts.submsh_base, opts.ofmt);
      std::cout << "Writing mesh: " << submshfile.base << std::endl;
      timer.start();
      write_mesh_selected(mesh, submshfile.format, submshfile.base);
      binary_write(nod.begin(), nod.end(), submshfile.base + NOD_EXT);
      binary_write(eidx.begin(), eidx.end(), submshfile.base + EIDX_EXT);
      timer.stop();
      std::cout << "Done in " << timer.get_time_sec() << " sec" << std::endl;
      break;
    }

    case OP_VOL:
    {
      // extract volume --------------------------------------------------------------
      mt_vector<std::string> coord_string_list;
      split_string(opts.coord, ',', coord_string_list);

      if(coord_string_list.size() != 6) {
        std::cerr << "Error! Wrong volume format! Aborting!" << std::endl;
        return;
      }

      int output_mode = 0;
      if(opts.mode.size())
        output_mode = atoi(opts.mode.c_str());
      check_condition(output_mode == 0 || output_mode == 1, "output_mode == [0|1]", "extract volume");

      vec3r ref, ref_delta;
      ref.x  = atof(coord_string_list[0].c_str());
      ref_delta.x = atof(coord_string_list[1].c_str());
      ref.y  = atof(coord_string_list[2].c_str());
      ref_delta.y = atof(coord_string_list[3].c_str());
      ref.z  = atof(coord_string_list[4].c_str());
      ref_delta.z = atof(coord_string_list[5].c_str());

      mesh.e2n_dsp.resize(mesh.e2n_cnt.size());
      bucket_sort_offset(mesh.e2n_cnt, mesh.e2n_dsp);

      std::cout << "Extracting volume .." << std::endl;
      timer.start();

      mt_vector<mt_idx_t> nod, eidx;
      extract_volume_mode(mesh, ref, ref_delta, nod, eidx);

      timer.stop();
      std::cout << "Done in " << timer.get_time_sec() << " sec" << std::endl;

      mt_filename submshfile(opts.submsh_base, opts.ofmt);
      if(output_mode == 1) {
        std::string ofile = submshfile.base + VTX_EXT;
        std::cout << "Writing vertex file " << ofile << std::endl;
        write_vtx(nod, ofile, true);
      }
      else if(output_mode == 0){
        std::cout << "Writing mesh: " << submshfile.base << std::endl;
        timer.start();
        write_mesh_selected(mesh, submshfile.format, submshfile.base);
        binary_write(nod.begin(), nod.end(), submshfile.base + NOD_EXT);
        binary_write(eidx.begin(), eidx.end(), submshfile.base + EIDX_EXT);
        timer.stop();
        std::cout << "Done in " << timer.get_time_sec() << " sec" << std::endl;
      }

      break;
    }
    case OP_UR:
    {
      timer.start();

      // we extract unreachable elements -------------------------------------------------
      mt_vector<mt_meshdata> parts;
      mt_vector<mt_vector<mt_idx_t> > dcmp_eidx;

      compute_full_mesh_connectivity(mesh, mshfile.base);
      nodal_connectivity_decomposition(mesh, parts, dcmp_eidx);

      int mode = 0;
      if(opts.mode.size()) mode = atoi(opts.mode.c_str());

      std::cout << "Extracted " << parts.size() << " parts." << std::endl;
      if(parts.size() > 1) {
        switch(mode) {
          default:
          case 0: {
            for(size_t p = 0; p < parts.size(); p++)
            {
              mt_vector<mt_idx_t> nod;
              reindex_nodes(parts[p], nod, false);

              mt_filename submshfile(opts.submsh_base, opts.ofmt);
              submshfile.base += ".part" + std::to_string(p);
              std::cout << "Writing mesh " << submshfile.base << " .." << std::endl;
              write_mesh_selected(parts[p], submshfile.format, submshfile.base);

              binary_write(nod.begin(), nod.end(), submshfile.base + NOD_EXT);
              binary_write(dcmp_eidx[p].begin(), dcmp_eidx[p].end(), submshfile.base + EIDX_EXT);
            }
            break;
          }

          case 1: {
            int    max_idx = 0;
            size_t maxsize = parts[max_idx].e2n_cnt.size();

            for(size_t i=0; i<parts.size(); i++) {
              if(maxsize < parts[i].e2n_cnt.size()) {
                maxsize = parts[i].e2n_cnt.size();
                max_idx = i;
              }
            }

            mt_vector<mt_idx_t> nod;
            reindex_nodes(parts[max_idx], nod, false);

            mt_filename submshfile(opts.submsh_base, opts.ofmt);
            std::cout << "Writing mesh " << submshfile.base << " .." << std::endl;
            write_mesh_selected(parts[max_idx], submshfile.format, submshfile.base);

            binary_write(nod.begin(), nod.end(), submshfile.base + NOD_EXT);
            binary_write(dcmp_eidx[max_idx].begin(), dcmp_eidx[max_idx].end(), submshfile.base + EIDX_EXT);
            break;
          }

          case 2: {
            int    min_idx = 0;
            size_t minsize = parts[min_idx].e2n_cnt.size();

            for(size_t i=0; i<parts.size(); i++) {
              if(minsize > parts[i].e2n_cnt.size()) {
                minsize = parts[i].e2n_cnt.size();
                min_idx = i;
              }
            }

            mt_vector<mt_idx_t> nod;
            reindex_nodes(parts[min_idx], nod, false);

            mt_filename submshfile(opts.submsh_base, opts.ofmt);
            std::cout << "Writing mesh " << submshfile.base << " .." << std::endl;
            write_mesh_selected(parts[min_idx], submshfile.format, submshfile.base);

            binary_write(nod.begin(), nod.end(), submshfile.base + NOD_EXT);
            binary_write(dcmp_eidx[min_idx].begin(), dcmp_eidx[min_idx].end(), submshfile.base + EIDX_EXT);
            break;
          }
        }
      }
      else {
        std::cout << "Skipping mesh output .." << std::endl;
      }

      timer.stop();
      std::cout << "Done in " << timer.get_time_sec() << " sec" << std::endl;
      break;
    }

    case OP_GRAD:
    {
      compute_full_mesh_connectivity(mesh, mshfile.base);

      short data_idx = -1;
      mt_vector<mt_real> idat, odat;
      mt_vector<vec3r> odat_vec;
      igb_header igb, igb_out, igb_out_vec;
      setup_data_format(opts.idat, data_idx, igb);
      if(data_idx < 0) exit(EXIT_FAILURE);

      mt_filename outfile(opts.odat, "");
      bool nodal_input = true;
      bool nodal_output = true;
      if (opts.mode.size()) {
        const int flag = atoi(opts.mode.c_str());
        nodal_output = (flag == 0);
      }

      if (nodal_output)
        std::cout << "Output data is node based." << std::endl;
      else
        std::cout << "Output data is element based." << std::endl;

      // read data
      switch(data_idx) {
        case 0:
        {
          read_vector_ascii(idat, opts.idat, true);

          if (idat.size() == mesh.n2e_cnt.size()) {
            std::cout << "Input data is node based." << std::endl;
            nodal_input = true;
          }
          else if (idat.size() == mesh.e2n_cnt.size()) {
            std::cout << "Input data is element based." << std::endl;
            nodal_input = false;
          }
          else {
            std::cerr << "Error: Data dimension does not fit either nodes or elems. Aborting!" << std::endl;
            exit(1);
          }

          extract_gradient_mode(mesh, idat, odat, odat_vec, nodal_input, nodal_output);
          std::string outstr = outfile.base + ".grad" + VEC_EXT;
          write_vector_ascii(odat_vec, outstr);
          outstr = outfile.base + ".gradmag" + DAT_EXT;
          write_vector_ascii(odat, outstr);
        } break;
        case 1:
        {
          const size_t igb_data_size = static_cast<size_t>(igb.v_x);
          if (igb_data_size == mesh.n2e_cnt.size()) {
            std::cout << "Input data is node based." << std::endl;
            nodal_input = true;
          }
          else if (igb_data_size == mesh.e2n_cnt.size()) {
            std::cout << "Input data is element based." << std::endl;
            nodal_input = false;
          }
          else {
            std::cerr << "Error: Data dimension does not fit either nodes or elems. Aborting!" << std::endl;
            exit(1);
          }

          init_igb_header(igb, outfile.base + ".gradmag" + IGB_EXT, igb_out);
          set_igb_header_datatype("float", igb_out);
          init_igb_header(igb, outfile.base + ".grad" + IGB_EXT, igb_out_vec);
          set_igb_header_datatype("vec3f", igb_out_vec);

          if (nodal_output) {
            igb_out.v_x = mesh.n2e_cnt.size();
            igb_out_vec.v_x = mesh.n2e_cnt.size();
          } else {
            igb_out.v_x = mesh.e2n_cnt.size();
            igb_out_vec.v_x = mesh.e2n_cnt.size();
          }
          igb_out.v_inc_x = igb_out_vec.v_inc_x = 1;
          igb_out.v_dim_x = igb_out.v_x;
          igb_out_vec.v_dim_x = igb_out_vec.v_x;

          write_igb_header(igb_out);
          write_igb_header(igb_out_vec);

          extract_gradient_mode(mesh, igb, std::addressof(igb_out), std::addressof(igb_out_vec), nodal_input, nodal_output);

          close_igb_fileptr(igb);
          close_igb_fileptr(igb_out);
          close_igb_fileptr(igb_out_vec);
        } break;
      }

      break;
    }
    case OP_OVERLP:
    {
      mt_meshdata mesh1, mesh2;

      mt_filename msh1(opts.msh1_base, opts.ifmt);
      mt_filename msh2(opts.msh2_base, opts.ifmt);

      std::cout << "Reading mesh 1: " << msh1.base << std::endl;
      timer.start();
      read_mesh_selected(mesh1, msh1.format, msh1.base);
      bucket_sort_offset(mesh1.e2n_cnt, mesh1.e2n_dsp);
      timer.stop();
      std::cout << "Done in " << timer.get_time_sec() << " sec" << std::endl;

      std::cout << "Reading mesh 2: " << msh2.base << std::endl;
      timer.start();
      read_mesh_selected(mesh2, msh2.format, msh2.base);
      timer.stop();
      std::cout << "Done in " << timer.get_time_sec() << " sec" << std::endl;

      float radius = 2.0;
      if (opts.rad.size())
        radius = atof(opts.rad.c_str());

      std::cout << "Identifying overlaping elements .." << std::endl;
      timer.start();

      mt_vector<bool> keep;
      extract_overlap_mode(mesh1, mesh2, radius, keep);

      timer.stop();
      std::cout << "Done in " << timer.get_time_sec() << " sec" << std::endl;

      bool output_complement = false;
      if(opts.mode.size()) {
        if(atoi(opts.mode.c_str()) == 0) output_complement = false;
        if(atoi(opts.mode.c_str()) == 1) output_complement = true;
      }

      if(output_complement) {
        mt_vector<mt_idx_t> m1_eidx, comp_eidx;
        m1_eidx.reserve(mesh1.e2n_cnt.size() / 2);
        comp_eidx.reserve(mesh1.e2n_cnt.size() / 2);

        for (size_t eidx = 0; eidx < mesh1.e2n_cnt.size(); eidx++) {
          if (keep[eidx])
            m1_eidx.push_back(mt_idx_t(eidx));
          else
            comp_eidx.push_back(mt_idx_t(eidx));
        }

        mt_meshdata overlap, complement;

        mt_vector<mt_idx_t> nod;
        extract_mesh(m1_eidx, mesh1, overlap);
        reindex_nodes(overlap, nod, true);

        // write first mesh
        mt_filename submesh(opts.submsh_base, opts.ofmt);
        std::cout << "Writing mesh: " << submesh.base << std::endl;
        timer.start();
        write_mesh_selected(overlap, submesh.format, submesh.base);
        binary_write(nod.begin(), nod.end(), submesh.base + NOD_EXT);
        binary_write(m1_eidx.begin(), m1_eidx.end(), submesh.base + EIDX_EXT);
        timer.stop();
        std::cout << "Done in " << timer.get_time_sec() << " sec" << std::endl;

        extract_mesh(comp_eidx, mesh1, complement);
        reindex_nodes(complement, nod, true);

        // write second mesh
        submesh.base += ".compl";
        std::cout << "Writing mesh: " << submesh.base << std::endl;
        timer.start();
        write_mesh_selected(complement, submesh.format, submesh.base);
        binary_write(nod.begin(), nod.end(), submesh.base + NOD_EXT);
        binary_write(comp_eidx.begin(), comp_eidx.end(), submesh.base + EIDX_EXT);
        timer.stop();
        std::cout << "Done in " << timer.get_time_sec() << " sec" << std::endl;
      }
      else {
        mt_vector<mt_idx_t> nod, eidx;
        restrict_meshdata(keep, mesh1, nod, eidx);

        // write mesh
        mt_filename submesh(opts.submsh_base, opts.ofmt);
        std::cout << "Writing mesh: " << submesh.base << std::endl;
        timer.start();
        write_mesh_selected(mesh1, submesh.format, submesh.base);
        binary_write(nod.begin(), nod.end(), submesh.base + NOD_EXT);
        binary_write(eidx.begin(), eidx.end(), submesh.base + EIDX_EXT);
        timer.stop();
        std::cout << "Done in " << timer.get_time_sec() << " sec" << std::endl;
      }

      break;
    }

    case OP_TAGS:
    {
      // we dont need to call the calculate function since it already happens before the switch call.

      extract_tags_mode_write(mesh.etags, opts.odat);

      break;
    }

    case OP_FIB:
    {
      int mode = 0;
      if(opts.mode.size())
        mode = clamp(atoi(opts.mode.c_str()), 0, 2, true);

      std::string outname;
      if(opts.odat.size()) {
        outname = opts.odat;
      } else {
        mt_filename meshname(opts.msh_base, "");
        outname = meshname.base;
      }

      const int numfib = mesh.lon.size() == mesh.e2n_cnt.size() * 6 ? 6 : 3;
      // fail only if sheets were explicitly requested
      if(mode == 2 && numfib != 6) {
        fprintf(stderr, "sheet extract error: no sheet data present!\n");
        exit(EXIT_FAILURE);
      }
      mt_vector<mt_real> fibers, sheets;
      extract_fibers_mode(mesh, mode, fibers, sheets);
      if((mode == 0 || mode == 1) && (!fibers.empty())) {
        write_vector_ascii(fibers, outname + ".fiber" + VEC_EXT, 3);
      }
      if((mode == 0 || mode == 2) && (!sheets.empty())) {
        write_vector_ascii(sheets, outname + ".sheet" + VEC_EXT, 3);
      }
      break;
    }

    case OP_ADJ: {
      mt_vector<mt_idx_t> vtx;

      if(endswith(opts.vtx, VTX_EXT))
        read_vtx(vtx, opts.vtx);
      else if(endswith(opts.vtx, NOD_EXT))
        binary_read(vtx, opts.vtx);
      else {
        fprintf(stderr, "extract adjustment error: cannot read vertices %s. Unknown file type!\n", opts.vtx.c_str());
        exit(EXIT_FAILURE);
      }

      mt_vector<mt_real> data, adj;
      read_vector_ascii(data, opts.idat);
      adj.reserve(vtx.size());

      for(mt_idx_t v : vtx)
        adj.push_back(data[v]);

      int header_flag = 1;
      if(opts.mode.size())
        header_flag = atoi(opts.mode.c_str());

      write_vtx_and_data(vtx, adj, opts.odat, header_flag > 0);

      break;
    }

    case OP_HALO: {

      const size_t vtx_len = opts.vtx.size();
      size_t ext_len = 0;

      mt_vector<mt_idx_t> vtx;
      if (endswith(opts.vtx, VTX_EXT)) {
        read_vtx(vtx, opts.vtx);
        ext_len = strlen(VTX_EXT);
      }
      else if (endswith(opts.vtx, NOD_EXT)) {
         binary_read(vtx, opts.vtx);
         ext_len = strlen(NOD_EXT);
      }
      else {
        fprintf(stderr, "extract vtxhalo error: cannot read vertices %s. Unknown file type!\n", opts.vtx.c_str());
        exit(EXIT_FAILURE);
      }
      const std::string vtx_base = opts.vtx.substr(0, vtx_len-ext_len);

      std::cout << "vtx_base: " << vtx_base << std::endl;

      if (opts.tag_file.size())
        readElementTags_general(mesh.etags, opts.tag_file);

      if (opts.tags.size() > 0) {
        const size_t num_elems = mesh.e2n_cnt.size();
        mt_vector<bool> keep(num_elems, true);
        for (size_t i=0; i<num_elems; i++)
          keep[i] = (opts.tags.count(mesh.etags[i]) == 0);
        mt_vector<mt_idx_t> eidx;
        restrict_elements(keep, mesh, eidx, true);
        compute_full_mesh_connectivity(mesh, true);
      }
      else
        compute_full_mesh_connectivity(mesh, mshfile.base, true);

      /*
      std::cout << "num nodes:  " << num_nodes << std::endl
                << "num points: " << (mesh.xyz.size() / 3) << std::endl;
      */

      MT_USET<mt_idx_t> block;
      block.insert(vtx.begin(), vtx.end());
      block.sort();

      MT_USET<mt_idx_t> halo;
      mt_vector<mt_vector<mt_idx_t>> comp;

      extract_vtxhalo_mode(mesh, block, halo, comp);

      std::string out_file;

      if (vtx.size() > 0) {
        out_file = vtx_base+"_skipped"+VTX_EXT;
        binary_sort(vtx);
        write_vtx(vtx, out_file, WRITE_VTX_INTRA);

        std::cout << "skipped: size=" << vtx.size()
                  << ", file=" << out_file << std::endl;
      }

      if (halo.size() > 0)
      {
        // save halo to file
        out_file = vtx_base+"_halo"+VTX_EXT;
        binary_sort(vtx);
        vtx.assign(halo.begin(), halo.end());
        write_vtx(vtx, out_file, WRITE_VTX_INTRA);

        std::cout << "halo: size=" << halo.size()
                  << ", file=" << out_file << std::endl;

        for (size_t i = 0; i < comp.size(); ++i) {
          const mt_vector<int64_t>& sel_list = comp[i];

          // save individual sel components to files
          out_file = vtx_base+"_halo"+std::to_string(i)+VTX_EXT;
          vtx.assign(sel_list.begin(), sel_list.end());
          binary_sort(vtx);
          write_vtx(vtx, out_file, WRITE_VTX_INTRA);

          std::cout << "domain: size=" << vtx.size()
                    << ", file=" << out_file << std::endl;
        }

      }
      else
        std::cout << "empty halo" << std::endl;

      break;
    }

    default: break;
  }
}
