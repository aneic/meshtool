#include "itk_mode.h"

void itk_close_mode(itk_image& input_img, const int& iter, const mt_triple<short>& rad, bool use_ref,
                    const mt_triple<float> ref, bool use_taglist, mt_vector<std::string> regtag,
                    mt_vector<std::string> taglist) {
  struct timeval t1, t2;

  if (use_ref) {
    printf("Refining to (%g, %g, %g) of original resolution..\n", ref.v1, ref.v2, ref.v3);
    gettimeofday(&t1, NULL);
    resample_image(input_img, ref);
    gettimeofday(&t2, NULL);
    std::cout << "Done in " << (float)timediff_sec(t1, t2) << " sec" << std::endl;
  }

  printf("preprocessing image ..\n");
  gettimeofday(&t1, NULL);

  MT_USET<int> bdry, tg;
  bdry.insert(0);

  if (use_taglist) {
    for (std::string& str : regtag) tg.insert(atoi(str.c_str()));
  } else {
    char* data = input_img.databuff.data();

    mt_idx_t val = 0.0;
    size_t c = 0;

    for (size_t i = 0; i < input_img.dim.v1 * input_img.dim.v2 * input_img.dim.v3; i++) {
      data = input_img.databuff.data() + i * input_img.pixsz + c * input_img.compsz;
      itk_comp_as_mt_idx_t(data, input_img.comptypeid, val);
      if (val) tg.insert(val);
    }
  }

  if (use_taglist) {
    for (std::string& str : taglist) bdry.insert(atoi(str.c_str()));
  }

  itk_image iimg;
  iimg.assign(input_img);

  // we extract only the regions we work on
  mt_triple<unsigned int> from, to;

  iimg.bounding_box(from, to, tg);  // compute where the tags
  iimg.crop(from, to);
  iimg.padding({5, 5, 5});

  gettimeofday(&t2, NULL);
  std::cout << "Done in " << (float)timediff_sec(t1, t2) << " sec" << std::endl;

  printf("applying close operation ..\n");
  gettimeofday(&t1, NULL);

  for (int curtag : tg) {
    for (int it = 0; it < iter; it++) {
      itk_dilate(iimg, rad, curtag, bdry);
      itk_erode(iimg, rad, curtag, bdry);
    }
  }

  itk_insert_image(input_img, iimg, tg);

  gettimeofday(&t2, NULL);
  std::cout << "Done in " << (float)timediff_sec(t1, t2) << " sec" << std::endl;
}

bool itk_mesh_mode(itk_image& img, mt_meshdata& mesh, mt_vector<mt_real>& values, const bool has_slices,
                   const unsigned int comp, const mt_real scale, const mt_triple<MT_USET<mt_idx_t>>& slices) {
  struct timeval t1, t2;

  bool success = false;
  std::cout << "Converting to Hex-mesh ..." << std::endl;
  gettimeofday(&t1, NULL);

  if (has_slices)
    success = img.to_hex_mesh(mesh, values, slices, comp, scale);
  else
    success = img.to_hex_mesh(mesh, values, comp, scale);

  gettimeofday(&t2, NULL);
  std::cout << "Done in " << (float)timediff_sec(t1, t2) << " sec" << std::endl;
  return success;
}

void itk_extrude_mode(const int extrude_mode, itk_image& img, const short rad, const int reg_tag, int new_tag,
                      MT_USET<int>& bdry_tags) {
  struct timeval t1, t2;

  if (new_tag < 0) new_tag = reg_tag;

  if (!bdry_tags.size()) bdry_tags.insert(0);

  std::cout << "Extruding  .. " << std::endl;
  gettimeofday(&t1, NULL);
  if (extrude_mode == 0) {
    printf("  mode='in', radius=%d, regtag=%d, newtag=%d\n", rad, reg_tag, new_tag);
    itk_extrude(img, rad, reg_tag, new_tag, bdry_tags, true);
  } else if (extrude_mode == 1) {
    printf("  mode='out', radius=%d, regtag=%d, newtag=%d\n", rad, reg_tag, new_tag);
    itk_extrude(img, rad, reg_tag, new_tag, bdry_tags, false);
  } else {
    printf("  mode='both', radius=%d, regtag=%d, newtag=%d\n", rad, reg_tag, new_tag);
    itk_extrude(img, rad, reg_tag, new_tag, bdry_tags, true);
    itk_extrude(img, rad, reg_tag, new_tag, bdry_tags, false);
  }
  gettimeofday(&t2, NULL);
  std::cout << "Done in " << (float)timediff_sec(t1, t2) << " sec" << std::endl;
}