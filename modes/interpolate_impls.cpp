#include "geometry_utils.h"
#include "interpolate_mode.h"

static inline int interpolate_clouddata_mode(const interpolation_t interp_type, PointCloudInterpolator& cloud_interp,
                                             const bool update_rbf_coeff, const mt_meshdata& omesh,
                                             const mt_vector<mt_real>& idat, mt_vector<mt_real>& odat, int dpn) {
  const size_t num_ipnts = cloud_interp.num_pts();
  const size_t num_opnts = omesh.xyz.size() / 3;
  const size_t data_size = idat.size();
  // do some checks
  if ((num_ipnts == 0) || (num_opnts == 0) || (data_size == 0)) return -1;
  if ((dpn <= 0) && ((data_size % num_ipnts) == 0)) dpn = (data_size / num_ipnts);
  if ((dpn != 1) && (dpn != 3) && (dpn != 9)) return -2;
  if (data_size < dpn * num_ipnts) return -3;
  if ((interp_type == interpolation_t::shepard) || (interp_type == interpolation_t::glob_shepard)) {
    const bool global = (interp_type == interpolation_t::glob_shepard);
    cloud_interp.interpolate_shepard(omesh.xyz, idat, dpn, global, odat);
  } else {
    if (update_rbf_coeff)
      cloud_interp.recompute_rbf_coeffs(idat, false);
    else
      cloud_interp.setup_rbf_coeffs(idat, dpn);
    cloud_interp.interpolate_data(omesh.xyz, odat);
  }
  return 0;
}

int interpolate_clouddata_mode(const interpolation_t interp_type, const mt_vector<mt_real>& ipnts,
                               const mt_meshdata& omesh, const mt_vector<mt_real>& idat, mt_vector<mt_real>& odat,
                               int dpn) {
  constexpr int num_neighbours = 7;
  PointCloudInterpolator cloud_interp(ipnts);
  cloud_interp.setup_roi(num_neighbours);
  return interpolate_clouddata_mode(interp_type, cloud_interp, false, omesh, idat, odat, dpn);
}

int interpolate_clouddata_mode(const interpolation_t interp_type, const mt_vector<vec3r>& ipnts,
                               const mt_meshdata& omesh, const mt_vector<mt_real>& idat, mt_vector<mt_real>& odat,
                               int dpn) {
  constexpr int num_neighbours = 7;
  PointCloudInterpolator cloud_interp(ipnts);
  cloud_interp.setup_roi(num_neighbours);
  return interpolate_clouddata_mode(interp_type, cloud_interp, false, omesh, idat, odat, dpn);
}


int interpolate_clouddata_mode(const interpolation_t interp_type, mt_vector<mt_real>& ipnts, const mt_meshdata& omesh,
                               igb_header& iigb, igb_header& oigb, igb_header* dynpts, int dpn) {
  const size_t num_time_slices = static_cast<size_t>(iigb.v_t);
  if (num_time_slices == 0) return 1;
  if ((dynpts) && (static_cast<size_t>(dynpts->v_x) != (ipnts.size() / 3))) return 2;
  if ((dynpts) && (static_cast<size_t>(dynpts->v_t) != num_time_slices)) return 3;
  int rval = 0;
  mt_vector<mt_real> idat, odat;

  if (dynpts == nullptr) {
    constexpr int num_neighbours = 7;
    PointCloudInterpolator cloud_interp;
    cloud_interp.set_pts(ipnts);
    cloud_interp.setup_roi(num_neighbours);
    for (size_t t = 0; (t < num_time_slices) && (rval == 0); t++) {
      read_igb_slice(idat, iigb);
      rval = interpolate_clouddata_mode(interp_type, cloud_interp, (t > 0), omesh, idat, odat, dpn);
      if (rval == 0) write_igb_slice(odat, oigb);
    }
  } else {
    for (size_t t = 0; (t < num_time_slices) && (rval == 0); t++) {
      read_igb_slice(ipnts, *dynpts);
      read_igb_slice(idat, iigb);
      rval = interpolate_clouddata_mode(interp_type, ipnts, omesh, idat, odat, dpn);
      if (rval == 0) write_igb_slice(odat, oigb);
    }
  }
  return rval;
}

int interpolate_elem2node_mode(const mt_meshdata& mesh, const mt_vector<mt_real>& idat, mt_vector<mt_real>& odat,
                               const bool norm, int dpn) {
  const size_t num_elems = mesh.e2n_cnt.size();
  const size_t data_size = idat.size();
  // do some checks
  if ((num_elems == 0) || (data_size == 0)) return -1;
  if ((dpn <= 0) && ((data_size % num_elems) == 0)) dpn = (data_size / num_elems);
  if ((dpn != 1) && (dpn != 3) && (dpn != 9)) return -2;
  if (data_size < dpn * num_elems) return -3;
  // interpolate
  if (dpn == 1) {
    elemData_to_nodeData(mesh, idat, odat);
  } else if (dpn == 3) {
    mt_vector<mt_point<mt_real>> idat_vec, odat_vec;
    array_to_points(idat, idat_vec);
    elemData_to_nodeData(mesh, idat_vec, odat_vec);
    if (norm) {
      for (vec3r& v : odat_vec) v.normalize();
    }
    points_to_array(odat_vec, odat);
  } else {
    mt_vector<dmat<mt_real>> idat_ten, odat_ten;
    array_to_tensors(idat, idat_ten);
    elemData_to_nodeData(mesh, idat_ten, odat_ten);
    tensors_to_array(odat_ten, odat);
  }
  return 0;
}

int interpolate_elem2node_mode(const mt_meshdata& mesh, igb_header& iigb, igb_header& oigb, const bool norm, int dpn) {
  const size_t num_time_slices = static_cast<size_t>(iigb.v_t);
  if (num_time_slices == 0) return 1;
  int rval = 0;
  mt_vector<mt_real> idat, odat;
  for (size_t t = 0; (t < num_time_slices) && (rval == 0); t++) {
    read_igb_slice(idat, iigb);
    rval = interpolate_elem2node_mode(mesh, idat, odat, norm, dpn);
    if (rval == 0) write_igb_slice(odat, oigb);
  }
  return rval;
}

template<class V>
inline void n2e_data(const mt_meshdata& mesh, const mt_vector<V>& idat, mt_vector<V>& odat, const int mode) {
  if(mode == 0) {
    nodeData_to_elemData(mesh, idat, odat);
  } else {
    nodeData_to_elemData_freq(mesh, idat, odat);
  }
}

int interpolate_node2elem_mode(const mt_meshdata& mesh, const mt_vector<mt_real>& idat, mt_vector<mt_real>& odat,
                               const bool norm, const int mode, int dpn) {
  const size_t num_nodes = mesh.n2e_cnt.size();
  const size_t data_size = idat.size();
  // do some checks
  if ((num_nodes == 0) || (data_size == 0)) return -1;
  if ((dpn <= 0) && ((data_size % num_nodes) == 0)) dpn = (data_size / num_nodes);
  if ((dpn != 1) && (dpn != 3) && (dpn != 9)) return -2;
  if (data_size < dpn * num_nodes) return -3;
  // interpolate
  if (dpn == 1) {
    n2e_data(mesh, idat, odat, mode);
  } else if (dpn == 3) {
    mt_vector<mt_point<mt_real>> idat_vec, odat_vec;
    array_to_points(idat, idat_vec);
    nodeData_to_elemData(mesh, idat_vec, odat_vec);
    if (norm) {
      for (vec3r& v : odat_vec) v.normalize();
    }
    points_to_array(odat_vec, odat);
  } else {
    mt_vector<dmat<mt_real>> idat_ten, odat_ten;
    array_to_tensors(idat, idat_ten);
    nodeData_to_elemData(mesh, idat_ten, odat_ten);
    tensors_to_array(odat_ten, odat);
  }
  return 0;
};

int interpolate_node2elem_mode(const mt_meshdata& mesh, igb_header& iigb, igb_header& oigb, const bool norm, const int mode, int dpn)
{
  const size_t num_time_slices = static_cast<size_t>(iigb.v_t);
  if (num_time_slices == 0) return 1;
  int rval = 0;
  mt_vector<mt_real> idat, odat;
  for (size_t t = 0; (t < num_time_slices) && (rval == 0); t++) {
    read_igb_slice(idat, iigb);
    rval = interpolate_node2elem_mode(mesh, idat, odat, norm, mode, dpn);
    if (rval == 0) write_igb_slice(odat, oigb);
  }
  return rval;
}

int interpolate_nodes_mode(const interpolate_nodes_data& intplt_data, const mt_meshdata& imesh,
                           const mt_meshdata& omesh, const mt_vector<mt_real>& idat, mt_vector<mt_real>& odat,
                           const bool norm, int dpn)
{
  const size_t num_ipnts = imesh.xyz.size() / 3;
  const size_t num_opnts = omesh.xyz.size() / 3;
  const size_t data_size = idat.size();
  // do some checks
  if ((num_ipnts == 0) || (num_opnts == 0) || (data_size == 0)) return -1;

  if ((dpn <= 0) && ((data_size % num_ipnts) == 0)) dpn = (data_size / num_ipnts);
  if ((dpn != 1) && (dpn != 3) && (dpn != 9)) return -2;
  if (data_size < dpn * num_ipnts) return -3;

  // interpolate
  if (dpn == 1) {
    nodal_interpolation(imesh, omesh, intplt_data.ivtxtree, intplt_data.isurftree, intplt_data.corr,
                        intplt_data.corr_dist, idat, odat, INTERP_STAGE_ALL, false);
  } else if (dpn == 3) {
    mt_vector<vec3r> idat_vec, odat_vec;
    array_to_points(idat, idat_vec);
    nodal_interpolation(imesh, omesh, intplt_data.ivtxtree, intplt_data.isurftree, intplt_data.corr,
                        intplt_data.corr_dist, idat_vec, odat_vec, INTERP_STAGE_ALL, false);
    if (norm) {
      for (vec3r& v : odat_vec) v.normalize();
    }
    points_to_array(odat_vec, odat);
  } else {
    mt_vector<dmat<mt_real>> idat_ten, odat_ten;
    array_to_tensors(idat, idat_ten);
    nodal_interpolation(imesh, omesh, intplt_data.ivtxtree, intplt_data.isurftree, intplt_data.corr,
                        intplt_data.corr_dist, idat_ten, odat_ten, INTERP_STAGE_ALL, false);
    tensors_to_array(odat_ten, odat);
  }
  return 0;
}

int interpolate_nodes_mode(interpolate_nodes_data& intplt_data, mt_meshdata& imesh, const mt_meshdata& omesh,
                           igb_header& iigb, igb_header& oigb, igb_header* dynpts, const bool norm, int dpn, bool verbose)
{
  const size_t num_time_slices = static_cast<size_t>(iigb.v_t);
  if (num_time_slices == 0) return 1;
  if ((dynpts) && (size_t(dynpts->v_x) != (imesh.xyz.size() / 3))) return 2;
  if ((dynpts) && (size_t(dynpts->v_t) != num_time_slices)) return 3;

  mt_sparse_matrix itp_mat;
  nodal_interpolation_matrix(imesh, omesh, intplt_data.ivtxtree, intplt_data.isurftree,
                             intplt_data.corr, intplt_data.corr_dist, itp_mat, true);

  mt_vector<mt_real>       idat, odat;
  mt_vector<vec3r>         idat_vec, odat_vec;
  mt_vector<dmat<mt_real>> idat_ten, odat_ten;

  if(verbose)
    printf("Processing igb time-slices: %s to %s: \n", iigb.filename.c_str(), oigb.filename.c_str());

  for (size_t t = 0; t < num_time_slices; t++) {
    if(verbose)
      printf("\rcurrent time-slice %d / %d .. ", (int)t+1, int(iigb.v_t));

    if (dynpts) {
      read_igb_slice(imesh.xyz, *dynpts);
      intplt_data.update_correspondance(omesh, imesh);
      nodal_interpolation_matrix(imesh, omesh, intplt_data.ivtxtree, intplt_data.isurftree,
                                 intplt_data.corr, intplt_data.corr_dist, itp_mat, false);
    }

    read_igb_slice(idat, iigb);
    if(dpn == 1) {
      itp_mat.apply(idat, odat);
    } else if(dpn == 3) {
      array_to_points(idat, idat_vec);
      itp_mat.apply(idat_vec, odat_vec);

      if(norm) {
        for(vec3r & v : odat_vec) {
          mt_real len2 = v.length2();
          if(len2) v /= sqrt(len2);
        }
      }

      points_to_array(odat_vec, odat);
    } else {
      array_to_tensors(idat, idat_ten);
      itp_mat.apply(idat_ten, odat_ten);
      tensors_to_array(odat_ten, odat);
    }

    write_igb_slice(odat, oigb);
  }

  if(verbose) printf("\n");

  return 0;
}
