#ifndef MT_CLEAN_MODE_H_
#define MT_CLEAN_MODE_H_

#include "data_structs.h"
#include <optional>


/**
 * @brief clean the mesh from bad topology definitions
 *
 * @param[in, out]  mesh        source mesh to clean
 * @param[in]  threshold   distance threshold when checking co-location of vertices
 *
 * @return  true if mesh changed, false otherwise
 */
bool clean_topology_mode(mt_meshdata& mesh, const mt_real threshold);

#endif