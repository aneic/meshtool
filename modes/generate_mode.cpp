/**
* @file generate_mode.h
* @brief Generate a fiber file.
* @author Anton Prassl
* @version
* @date 2017-02-13
*/

#include "data_structs.h"
#include "io_utils.h"
#include "mt_modes_base.h"
#include "mesh_quality.h"
#include "distancefield.hpp"
#include "string_utils.h"
#include "generate_mode.h"

#ifdef MT_ADDONS
#include "addons_utils.h"
#include "ng_interface.h"
#include "bsp_tree.hpp"
#include "Yaml.h"
#endif




static const std::string holes_par = "-holes=";
static const std::string ssurf_par = "-ssurf=";
static const std::string esurf_par = "-esurf=";
static const std::string bdry_par = "-bdry_layers=";
static const std::string bdry_step_par = "-bdry_step=";
static const std::string bdry_step_inc_par = "-bdry_inc=";
static const std::string bdry_symm_par = "-bdry_symm=";
static const std::string bdry_pres_par = "-prsv_bdry=";
static const std::string padding_par = "-padding=";
static const std::string lv_msh_par = "-lvmsh=";
static const std::string rv_msh_par = "-rvmsh=";
static const std::string config_par = "-config=";
static const std::string res_par = "-res=";
static const std::string res_outer_par = "-res_outer=";
static const std::string qual_par = "-qual=";

static const std::string verb_flag = "-verbose";
static const std::string tmpl_flag = "-template";




/**
* @brief Help for generate mode.
*/
void print_generate_fibres_help()
{
  fprintf(stderr, "generate fibres: generate default fibers for a given mesh file. the optional element"
                  " tags identify bath regions.\n");
  fprintf(stderr, "parameters:\n");
  fprintf(stderr, "%s<path>\t (input) path to basename of the input mesh\n", mesh_par.c_str());
  fprintf(stderr, "%stag1%ctag2\t (input) BATH region tag sequence, \"%c\" separated tags.\n", tags_par.c_str(), tag_separator, tag_separator);
  fprintf(stderr, "%s<path>\t (output) path to the basename of the output mesh\n", outmesh_par.c_str());
  fprintf(stderr, "%s<int>\t (optional) number of fiber directions. 1 or 2. Default is 2.\n", oper_par.c_str());
  fprintf(stderr, "\n");
}

/**
* @brief Help for generate mode.
*/
void print_generate_mesh_help()
{
  fprintf(stderr, "generate mesh: generate a tetrahedral mesh from a list of nested triangle surfaces.\n");
  fprintf(stderr, "parameters:\n");
  fprintf(stderr, "%s<surf1,surf2,..>\t (input) list of paths to triangle meshes of closed surfaces.\n", surf_par.c_str());
  fprintf(stderr, "%s<path>\t\t (output) path to the basename of the output mesh\n", outmesh_par.c_str());
  fprintf(stderr, "%s<tag1,tag2,..>\t (optional) tag indices for meshed regions. Should match order of surfaces.\n", ins_tag_par.c_str());
  fprintf(stderr, "%s[0|1],[0|1],..\t (optional) One value per surface. Whether associated volume is a hole (1) or filled (0).\n", holes_par.c_str());
  fprintf(stderr, "%s<float,float,..>\t (optional) Global edge length scaling (one value provided) *OR* \n"
                  "\t\t\t regional edge length scaling (one value per surface provided).\n", scale_par.c_str());
#ifdef MT_ADDONS
  fprintf(stderr, "%s<float>\t (optional) Padding (in multiples of the average edge length) enforced between the surfaces.\n", padding_par.c_str());
  fprintf(stderr, "%s\t\t (optional) Flag set to use Netgen instead of Tetgen for meshing.\n", netgen_par.c_str());
#endif
  fprintf(stderr, "%s<int>\t (optional) Number of boundary element layers to include in mesh.\n"
                  "\t\t\t If negative, only the boundary mesh part will be generated.\n", bdry_par.c_str());
  fprintf(stderr, "%s<float>\t (optional) Size of a boundary layer step, relative to the average edge length. Default is %.2f.\n", bdry_step_par.c_str(), BND_STEP_DFLT);
  fprintf(stderr, "%s<float>\t (optional) Relative boundary layer step increment. Default is %.2f\n", bdry_step_inc_par.c_str(), BND_INC_DFLT);
  fprintf(stderr, "%s<int>\t (optional) if 1, the boundary layer will be applied symmetrically (inward and outward) to the input surface. default is 0.\n", bdry_symm_par.c_str());
  fprintf(stderr, "%s<int>\t\t (optional) Volumetric element sizing mode. 0 = uniform,\n"
                  "\t\t\t 1 = based on surface element size. Default is 1.\n", mode_par.c_str());
  fprintf(stderr, "%s<int>\t\t (optional) Meshing quality. 0 = default, 1 = higher, 2 = highest.\n", qual_par.c_str());
  fprintf(stderr, "%s<int>\t (optional) 0 = remesh surface, 1 = preserve surface. Default is 1.\n", bdry_pres_par.c_str());
  fprintf(stderr, "%s<format>\t\t (optional) mesh input format. (%s)\n", inp_format_par.c_str(), input_formats.c_str());
  fprintf(stderr, "%s<format>\t\t (optional) mesh output format. (%s)\n", out_format_par.c_str(), output_formats.c_str());
  fprintf(stderr, "\n");
}

void print_generate_distancefield_help()
{
  fprintf(stderr, "generate distancefield: generate a distance field from one or between two surfaces.\n");
  fprintf(stderr, "parameters:\n");
  fprintf(stderr, "%s<path>\t\t (input) path to basename of the input mesh\n", mesh_par.c_str());
  fprintf(stderr, "%s<surf1,surf2,..>\t (input) .surf surface files. Their union defines the distance field start.\n", ssurf_par.c_str());
  fprintf(stderr, "%s<surf1,surf2,..>\t (optional) .surf surface files. Their union defines the end of a relative distance field.\n", esurf_par.c_str());
  fprintf(stderr, "%stag1%ctag2\t\t (optional) sequence of tags. The associated regions well not contribute to distances.\n", tags_par.c_str(), tag_separator);
  fprintf(stderr, "%s<path>\t\t (output) path to output .dat file.\n", odat_par.c_str());
  fprintf(stderr, "%s<format>\t\t (optional) mesh input format. (%s)\n", inp_format_par.c_str(), input_formats.c_str());
  fprintf(stderr, "\n");
}

void print_generate_bboxmesh_help()
{
  fprintf(stderr, "generate bboxmesh: generate the bounding box mesh of a given mesh.\n");
  fprintf(stderr, "parameters:\n");
  fprintf(stderr, "%s<path>\t (input) path to basename of the input mesh\n", mesh_par.c_str());
  fprintf(stderr, "%s<float>\t (input) output mesh resolution\n", res_par.c_str());
  fprintf(stderr, "%s<path>\t (output) path to the basename of the output mesh\n", outmesh_par.c_str());
  fprintf(stderr, "%s<float>\t (optional) scale applied to bbox\n", scale_par.c_str());
  fprintf(stderr, "\n");
}

void print_generate_split_help()
{
  fprintf(stderr, "generate split: generate a split file for given split operations.\n");
  fprintf(stderr, "parameters:\n");
  fprintf(stderr, "%s<path>\t (input) path to basename of the input mesh\n", mesh_par.c_str());
  fprintf(stderr, "%s<path>\t (input) sequence of split operations.\n", oper_par.c_str());
  fprintf(stderr, "%s<format>\t (optional) input format. (%s)\n", inp_format_par.c_str(), input_formats.c_str());
  fprintf(stderr, "%s<path>\t (output) path to the split list file\n", split_par.c_str());
  fprintf(stderr, "\n");
  fprintf(stderr, "The format of the split operations is:\n");
  fprintf(stderr, "tagA1,tagA2,..:tagB1,../tagA1,..:tagB1../..\n");
  fprintf(stderr, "\",\" separates tags, \":\" separates tag groups to split, \"/\" separates split operations.\n");
  fprintf(stderr, "The splitting is applied to the elements defined by the \"tagA\" tags.\nThese MUST NOT repeat between several split operations!\n");
  fprintf(stderr, "\n");
}

void print_generate_surfsplit_help()
{
  fprintf(stderr, "generate surfsplit: generate a split file from a given surface.\n");
  fprintf(stderr, "parameters:\n");
  fprintf(stderr, "%s<path>\t\t (input) path to basename of the input mesh\n", mesh_par.c_str());
  fprintf(stderr, "%s<surf1,surf2>\t (input) list of surfaces.\n", surf_par.c_str());
  fprintf(stderr, "%s<format>\t\t (optional) input format. (%s)\n", inp_format_par.c_str(), input_formats.c_str());
  fprintf(stderr, "%s<path>\t\t (output) path to the split list file\n", split_par.c_str());
  fprintf(stderr, "\n");
}

void print_generate_region_help()
{
  fprintf(stderr, "generate regions: generate explicit subregions in an existing background mesh.\n");
  fprintf(stderr, "parameters:\n");
  fprintf(stderr, "%s<path>\t\t (input) path to basename of the background mesh\n", mesh_par.c_str());
  fprintf(stderr, "%s<mesh1,mesh2>\t (input) list of meshes to integrate.\n", inp_mesh_par.c_str());
  fprintf(stderr, "%s<path>\t\t (output) output mesh basename.\n", outmesh_par.c_str());
  fprintf(stderr, "%s<0|1>\t\t (optional) whether to integrate regions as holes. default is 0\n", holes_par.c_str());
  fprintf(stderr, "%s<float>\t\t (optional) spatial distance to use as padding. Default is 5000.\n", padding_par.c_str());
  fprintf(stderr, "%s<float>\t\t (optional) refine background mesh overlap. Inner resolution. If 0, no refinement is applied. Default is 0.\n", res_par.c_str());
  fprintf(stderr, "%s<float>\t\t (optional) refine background mesh overlap. Outer resolution. If 0, it will be auto-detected. Default is 0.\n", res_outer_par.c_str());
  fprintf(stderr, "%s<tag1,tag2>\t (optional) tags of the regions, one per input mesh.\n", ins_tag_par.c_str());
  fprintf(stderr, "%s<tag1%ctag2>\t (optional) background mesh regions to insert into, \"%c\" separated tags.\n", tags_par.c_str(), tag_separator, tag_separator);
  fprintf(stderr, "%s<format>\t\t (optional) input format. (%s)\n", inp_format_par.c_str(), input_formats.c_str());
  fprintf(stderr, "\n");
}

void print_generate_purkinje_help()
{
    fprintf(stderr, "generate purkinje: generate a purkinje network on surfaces.\n");
    fprintf(stderr, "parameters:\n");
    fprintf(stderr, "%s<path>\t (input) path to basename of the input mesh.\n",mesh_par.c_str());
    fprintf(stderr, "%s<path>\t (input) uvc file for the according input mesh.\n", uvc_par.c_str());
    fprintf(stderr, "%s<path>\t (input) endocardial surface mesh of the left ventricle.\n", lv_msh_par.c_str());
    fprintf(stderr, "%s<path>\t (input) endocardial surface mesh of the right ventricle.\n", rv_msh_par.c_str());
    fprintf(stderr, "%s<path>\t (input) config file for branches.\n", config_par.c_str());
    fprintf(stderr, "%s<path>\t (output) name of directory to output into.\n", out_par.c_str());
    fprintf(stderr, "%s\t (optional) print additional output to terminal.\n", verb_flag.c_str());
    fprintf(stderr, "%s\t (optional) output template configuration to `%s` file.\n", tmpl_flag.c_str(), config_par.c_str());
    fprintf(stderr, "\n");
}

void print_generate_epstate_help()
{
    fprintf(stderr, "generate epstate: generate a EP state checkpoint from formal description.\n");
    fprintf(stderr, "parameters:\n");
    fprintf(stderr, "%s<path>\t (input) path to .yaml file with the formal description\n", config_par.c_str());
    fprintf(stderr, "%s<path>\t (output) name of the EP state file.\n", out_par.c_str());
    fprintf(stderr, "\n");
}


/**
* @brief Generate mode argument parser.
*
* @param [in]  argc Arguments count.
* @param [in]  argv Arguments string-array.
* @param [out] opts Options structure.
*
* @return 0 for success, >0 otherwise.
*/
int generate_parse_options(int argc, char** argv, struct generate_options & opts)
{
  if(argc < 3) {
    print_generate_fibres_help();
    print_generate_mesh_help();
    print_generate_distancefield_help();
    print_generate_bboxmesh_help();
    print_generate_split_help();
    print_generate_surfsplit_help();
#ifdef MT_ADDONS
    print_generate_region_help();
    print_generate_purkinje_help();
    print_generate_epstate_help();
#endif
    return 1;
  }

  std::string gen_type_str = argv[2];

  for (int i=3; i<argc; i++){
    std::string param = reformat_param(argv, i, argc);
    bool match = false;
    opts.verbose = false;
    opts.tmpl_out = false;

    if(!match) match = parse_param(param, mesh_par, opts.msh_base);
    if(!match) match = parse_param(param, inp_mesh_par, opts.inpmsh_base);
    if(!match) match = parse_param(param, surf_par, opts.surf);
    if(!match) match = parse_param(param, ssurf_par, opts.ssurf);
    if(!match) match = parse_param(param, esurf_par, opts.esurf);
    if(!match) match = parse_param(param, outmesh_par, opts.outmsh_base);
    if(!match) match = parse_param(param, holes_par, opts.holes);
    if(!match) match = parse_param(param, ins_tag_par, opts.ins_tag);
    if(!match) match = parse_param(param, scale_par, opts.scale);
    if(!match) match = parse_param(param, odat_par, opts.odat);
    if(!match) match = parse_param(param, oper_par, opts.op);
    if(!match) match = parse_param(param, bdry_par, opts.boundary);
    if(!match) match = parse_param(param, bdry_step_par, opts.boundary_step);
    if(!match) match = parse_param(param, bdry_step_inc_par, opts.boundary_inc);
    if(!match) match = parse_param(param, bdry_symm_par, opts.boundary_symm);
    if(!match) match = parse_param(param, inp_format_par, opts.ifmt);
    if(!match) match = parse_param(param, out_format_par, opts.ofmt);
    if(!match) match = parse_param(param, mode_par, opts.mode);
    if(!match) match = parse_param(param, bdry_pres_par, opts.boundary_pres);
    if(!match) match = parse_param(param, split_par, opts.odat);
    if(!match) match = parse_param(param, uvc_par, opts.uvcfile);
    if(!match) match = parse_param(param, lv_msh_par, opts.lvsurf);
    if(!match) match = parse_param(param, rv_msh_par, opts.rvsurf);
    if(!match) match = parse_param(param, config_par, opts.config);
    if(!match) match = parse_param(param, qual_par, opts.qual);
    if(!match) match = parse_param(param, out_par, opts.out);
    if(!match) match = parse_param(param, res_par, opts.res);
    if(!match) match = parse_param(param, res_outer_par, opts.res_outer);
    if(!match) match = parse_param(param, padding_par, opts.padding);
    if(!match) match = parse_flag (param, netgen_par, opts.use_netgen);

    if(!match) match = parse_flag(param, verb_flag, opts.verbose);
    if(!match) match = parse_flag(param, tmpl_flag, opts.tmpl_out);
    fixBasename(opts.msh_base);
    fixBasename(opts.inpmsh_base);
    fixBasename(opts.outmsh_base);

    if( (!match) && param.compare(0, tags_par.size(), tags_par) == 0) {
      std::string tags_str;
      tags_str.assign(param.begin()+tags_par.size(), param.end());
      mt_vector<std::string> taglist;
      split_string(tags_str, tag_separator, taglist);
      for (size_t t=0; t<taglist.size(); t++) {
        const mt_tag_t tag = static_cast<mt_tag_t>(atoi(taglist[t].c_str()));
        opts.tags.insert(tag);
      }
      match = true;
    }

    if(!match) {
      std::cerr << "Error: Cannot parse parameter " << param << std::endl;
      return 3;
    }
  }

  if(gen_type_str.compare("fibres") == 0)
  {
    opts.type = GEN_FIB;
    if (opts.msh_base.size() == 0 || opts.outmsh_base.size() == 0)
    {
      std::cerr << "generate fibers error: Insufficient parameters provided." << std::endl;
      print_generate_fibres_help();
      return 4;
    }
  }
  else if(gen_type_str.compare("mesh") == 0) {
    opts.type = GEN_MESH;
    if (opts.surf.size() == 0 || opts.outmsh_base.size() == 0)
    {
      std::cerr << "generate mesh error: Insufficient parameters provided." << std::endl;
      print_generate_mesh_help();
      return 4;
    }
  }
  else if(gen_type_str.compare("distancefield") == 0) {
    opts.type = GEN_DIST;
    if (opts.ssurf.size() == 0 || opts.odat.size() == 0 || opts.msh_base.size() == 0)
    {
      std::cerr << "generate distancefield error: Insufficient parameters provided." << std::endl;
      print_generate_distancefield_help();
      return 4;
    }
  }
  else if(gen_type_str.compare("bboxmesh") == 0) {
    opts.type = GEN_BBOX;
    if (opts.msh_base.size() == 0 || opts.outmsh_base.size() == 0 || opts.res.size() == 0)
    {
      std::cerr << "generate bboxmesh error: Insufficient parameters provided." << std::endl;
      print_generate_bboxmesh_help();
      return 4;
    }
  }
  else if(gen_type_str.compare("split") == 0) {
    opts.type = GEN_SPLIT;
    if (opts.msh_base.size() == 0 || opts.odat.size() == 0 || opts.op.size() == 0)
    {
      std::cerr << "generate split error: Insufficient parameters provided." << std::endl;
      print_generate_split_help();
      return 4;
    }
  }
#ifdef MT_ADDONS
  else if(gen_type_str.compare("regions") == 0) {
    opts.type = GEN_REGION;
    if (opts.msh_base.size() == 0 || opts.inpmsh_base.size() == 0 || opts.outmsh_base.size() == 0)
    {
      std::cerr << "generate regions error: Insufficient parameters provided." << std::endl;
      print_generate_region_help();
      return 4;
    }
  }
  else if(gen_type_str.compare("purkinje") == 0) {
      opts.type = GEN_PURKINJE;
      if (((opts.tmpl_out) && (opts.config.empty())) ||
          ((!opts.tmpl_out) && (opts.config.empty() || opts.msh_base.empty() || opts.lvsurf.empty() ||
                                opts.rvsurf.empty() || opts.uvcfile.empty() || opts.config.empty() ||
                                opts.out.empty())))
      {
        std::cerr << "gererate purkinje error: Insufficient parameters provided." << std::endl;
        print_generate_purkinje_help();
        return 4;
      }
  }
  else if(gen_type_str.compare("epstate") == 0) {
    opts.type = GEN_EPSTATE;
    if (opts.config.empty() || opts.out.empty()) {
      std::cerr << "generate epstate error: Insufficient parameters provided." << std::endl;
      print_generate_epstate_help();
      return 4;
    }
  }
#endif
  else if(gen_type_str.compare("surfsplit") == 0) {
    opts.type = GEN_SURFSPLIT;
    if (opts.msh_base.size() == 0 || opts.odat.size() == 0 || opts.surf.size() == 0)
    {
      std::cerr << "generate surfsplit error: Insufficient parameters provided." << std::endl;
      print_generate_surfsplit_help();
      return 4;
    }
  }
  else {
    std::cerr << "Unknown mode: generate " << gen_type_str << std::endl;
    std::cerr << "Use one of:" << std::endl << std::endl;
    print_generate_fibres_help();
    print_generate_mesh_help();
    print_generate_distancefield_help();
    print_generate_bboxmesh_help();
    print_generate_split_help();
    print_generate_surfsplit_help();
#ifdef MT_ADDONS
    print_generate_region_help();
    print_generate_purkinje_help();
    print_generate_epstate_help();
#endif
    return 2;
  }

  return 0;
}



void generate_field_from_surface(mt_meshdata & mesh, mt_meshdata & surface,
                                 mt_vector<vec3r> & field)
{
  MT_USET<mt_idx_t> surf_nodes, ele;
  surf_nodes.insert(surface.e2n_con.begin(), surface.e2n_con.end());

  size_t num_field_nodes = mesh.xyz.size() / 3;

  // now split elements
  mt_vector<mt_real> nodal_surf_nrmls;
  compute_nodal_surface_normals(surface, mesh.xyz, nodal_surf_nrmls);

  // generate a averaged surface normal
  vec3r avrg_nrml;
  mt_real avrg_cnt = 0.0;

  for(mt_idx_t surf_nidx : surf_nodes) {
    vec3r n = vec3r(nodal_surf_nrmls.data() + surf_nidx*3);
    if(n.length2() > 0.0) {
      avrg_nrml += n;
      avrg_cnt += 1.0;
    }
  }
  avrg_nrml /= avrg_cnt;

  field.assign(num_field_nodes, avrg_nrml);

  for(mt_idx_t surf_nidx : surf_nodes) {
    vec3r n = vec3r(nodal_surf_nrmls.data() + surf_nidx*3);
    field[surf_nidx] = n;
  }

  #if 1
  for(int i=0; i<5; i++) {
    nodeSet_to_elemSet(mesh, surf_nodes, ele);
    elemSet_to_nodeSet(mesh, ele, surf_nodes);
  }

  // the nodes we smooth
  mt_vector<mt_idx_t> sm_nodes; sm_nodes.assign(surf_nodes.begin(), surf_nodes.end());
  // we take contributions from all nodes
  mt_vector<bool> contribute(num_field_nodes, true);

  smooth_data_nodal(mesh, sm_nodes, contribute, 10, 0.25, field);
  #endif
}

#ifdef MT_ADDONS
static void mesh_surf_with_netgen(mt_meshdata &surfmesh, mt_meshdata &output_mesh, mt_vector<mt_idx_t> &tags,
                                  mt_vector<mt_real> &res, mt_vector<kdtree *> &surface_trees,
                                  mt_vector<mt_real> &scale_factors, mt_vector<bool> &is_hole, int qual_lvl) {
  MT_MAP<int, int> bmap;
  get_direct_bounding_surface_map(surface_trees, bmap);

  mt_vector<mt_real> target_res(res);
  for (size_t i = 0; i < res.size(); i++) {
    target_res[i] *= scale_factors[i];
  }

  float max_avrg = *std::max_element(res.begin(), res.end());

  MESHING_QUALITY quality = MESHING_QUALITY(qual_lvl);

  mesh_with_netgen(surfmesh, output_mesh, bmap, target_res, is_hole, tags, max_avrg, quality);
}

static bool epstate_from_yaml(const char *filename, ep_state &EP) {
  Yaml::Node root;
  try {
    Yaml::Parse(root, filename);
  } catch (const Yaml::Exception &e) {
    std::cout << "parsing exception " << e.Type() << ": " << e.what() << std::endl;
    exit(EXIT_FAILURE);
  }

  Yaml::Node &global = root["globals"];

  for (Yaml::Iterator it = global.Begin(); it != global.End(); it++) {
    Yaml::Node &entry = (*it).second;

    if (entry["name"].IsNone() == false) {
      std::string name = entry["name"].As<std::string>("");
      EP.gdata_names.push_back(name);
    } else {
      fprintf(stderr, "%s error: malformed globals entry, name missing! Aborting!\n", __func__);
      return false;
    }

    if (entry["data"].IsNone() == false) {
      std::string datafile = entry["data"].As<std::string>("");
      mt_vector<double> &gdata = EP.gdata.push_back(mt_vector<double>{});
      read_vector(gdata, datafile);
    } else {
      fprintf(stderr, "%s error: malformed globals entry, data missing! Aborting!\n", __func__);
      return false;
    }
  }

  Yaml::Node &vars = root["variables"];
  mt_vector<mt_vector<double>> ep_vars;
  mt_vector<int> ep_var_idx;

  for (Yaml::Iterator it = vars.Begin(); it != vars.End(); it++) {
    Yaml::Node &entry = (*it).second;

    /*
        if(entry["idx"].IsNone() == false) {
          int idx = entry["idx"].As<int>(-1);
          if(idx > -1) {
            ep_var_idx.push_back(
          }
        } else {
          fprintf(stderr, "malformed variables entry, name missing! Aborting!\n");
          return;
        }
    */

    if (entry["data"].IsNone() == false) {
      std::string datafile = entry["data"].As<std::string>("");
      mt_vector<double> &var = ep_vars.push_back(mt_vector<double>{});
      read_vector(var, datafile);
    } else {
      fprintf(stderr, "%s error: malformed variables entry, data missing! Aborting!\n", __func__);
      return false;
    }
  }

  if (ep_vars.size()) {
    // for now we assume onle one IMP, with variables of type double
    int imp_size = ep_vars.size() * sizeof(double);

    ionic_model &imp = EP.imps.push_back(ionic_model{});
    imp.size = imp_size;
    imp.offset = 0;
    imp.nplug = 0;
    imp.name = root["imp name"].As<std::string>("");

    EP.imp_sizes.assign(size_t(1), imp_size);

    size_t num_nodes = ep_vars[0].size();
    EP.saved_nnodes = num_nodes;

    for (size_t i = 1; i < ep_vars.size(); i++) {
      if (ep_vars[i].size() != num_nodes) {
        fprintf(stderr, "%s error: inconsistent variable dataset sizes! Aborting!\n", __func__);
        return false;
      }
    }
    EP.nodal_imp_mask.assign(num_nodes, char(0));
    EP.nodal_imp_offset.resize(num_nodes);
    EP.nodal_imp_offset[0] = 0;
    for (size_t i = 1; i < num_nodes; i++) EP.nodal_imp_offset[i] = EP.nodal_imp_offset[i - 1] + imp_size;

    EP.nodal_imp_data.resize(num_nodes * imp_size);

    for (size_t i = 0; i < num_nodes; i++) {
      double *write_ptr = (double *)(EP.nodal_imp_data.data() + EP.nodal_imp_offset[i]);
      for (size_t j = 0; j < ep_vars.size(); j++) write_ptr[j] = ep_vars[j][i];
    }
  } else {
    fprintf(stderr, "%s error: No varaibles read! Aborting!\n", __func__);
    return false;
  }

  return true;
}

#endif



/**
* @brief The generate mode function.
*
* @param [in]  argc Arguments count.
* @param [in]  argv Arguments string-array.
*/
void generate_mode(int argc, char** argv)
{
  struct timeval t1, t2;
  struct generate_options opts;

  int ret = generate_parse_options(argc, argv, opts);
  if (ret != 0) return;

  switch(opts.type) {
    case GEN_FIB:
    {
      struct mt_meshdata mesh;

      mt_filename meshfile(opts.msh_base, opts.ifmt);

      std::cout << "Reading mesh: " << opts.msh_base << std::endl;
      gettimeofday(&t1, NULL);
      read_mesh_selected(mesh, meshfile.format, meshfile.base, CRP_READ_ELEM);
      gettimeofday(&t2, NULL);
      std::cout << "Done in " << (float)timediff_sec(t1, t2) << " sec" << std::endl;

      int numfibers = 2;
      if(opts.op.size()) {
        int inp_fib = atoi(opts.op.c_str());
        if(inp_fib == 1 || inp_fib == 2) numfibers = inp_fib;
      }

      generate_fibers_mode(mesh, opts.tags, numfibers);

      std::cout << "Writing mesh: " << opts.outmsh_base << CARPTXT_LON_EXT << std::endl;
      gettimeofday(&t1, NULL);

      std::string & inp_format = meshfile.format;

      if(inp_format == carp_txt_fmt)
        writeFibers(mesh.lon, mesh.e2n_cnt.size(), opts.outmsh_base + CARPTXT_LON_EXT);
      else if(inp_format == carp_bin_fmt)
        writeFibersBinary(mesh.lon, mesh.e2n_cnt.size(), opts.outmsh_base + CARPBIN_LON_EXT);
      else {
        mt_filename outfile(opts.outmsh_base, "");
        write_mesh_selected(mesh, outfile.format.size() ? outfile.format : inp_format, outfile.base);
      }

      gettimeofday(&t2, NULL);
      std::cout << "Done in " << (float)timediff_sec(t1, t2) << " sec" << std::endl;
      break;
    }

    case GEN_MESH:
    {

      struct generate_mesh_options gen_msh_opts;

      mt_vector<mt_meshdata> surfmeshes;
      mt_vector<mt_idx_t> tags;
      mt_vector<mt_real> scale_factors;
      mt_vector<mt_real> holes_xyz;
      mt_vector<mt_real> res;
      mt_vector<bool> is_hole;

      generate_mesh_mode_read(opts, surfmeshes, tags, scale_factors, res, is_hole, gen_msh_opts);

      mt_meshdata outmesh;

      generate_mesh_mode(surfmeshes, tags, scale_factors, holes_xyz, res, is_hole, gen_msh_opts, outmesh);

      std::cout << "Writing mesh " << opts.outmsh_base << " .." << std::endl;
      gettimeofday(&t1, NULL);

      mt_filename outfile(opts.outmsh_base, opts.ofmt);
      write_mesh_selected(outmesh, outfile.format, outfile.base);

      gettimeofday(&t2, NULL);
      std::cout << "Done in " << (float)timediff_sec(t1, t2) << " sec" << std::endl;

      break;
    }

    case GEN_DIST:
    {
      mt_meshdata mesh, surf_start, surf_end;

      std::cout << "Reading mesh: " << opts.msh_base << std::endl;
      gettimeofday(&t1, NULL);
      mt_filename file(opts.msh_base, opts.ifmt);
      read_mesh_selected(mesh, file.format, file.base);
      compute_full_mesh_connectivity(mesh, file.base);
      gettimeofday(&t2, NULL);
      std::cout << "Done in " << (float)timediff_sec(t1, t2) << " sec" << std::endl;

      std::cout << "Reading start surface(s): " << opts.ssurf << std::endl;

      unified_surface_from_list(opts.ssurf, ',', surf_start);

      bool have_end_surf = opts.esurf.size();
      if (have_end_surf) {
        std::cout << "Reading end surface(s): " << opts.esurf << std::endl;
        unified_surface_from_list(opts.esurf, ',', surf_end);
      }
      mt_vector<mt_real> rvec;

      generate_distancefield_mode(mesh, surf_start.e2n_con, surf_end.e2n_con, rvec);


      write_vector_ascii(rvec, opts.odat, 1);
      break;
    }

    case GEN_BBOX:
    {
      mt_meshdata mesh;
      std::cout << "Reading mesh: " << opts.msh_base << std::endl;
      gettimeofday(&t1, NULL);
      mt_filename file(opts.msh_base, opts.ifmt);
      read_mesh_selected(mesh, file.format, file.base);
      gettimeofday(&t2, NULL);
      std::cout << "Done in " << (float)timediff_sec(t1, t2) << " sec" << std::endl;

      float scale = 1.0;
      if(opts.scale.size())
        scale = atof(opts.scale.c_str());

      float res = atof(opts.res.c_str());

      mt_meshdata bbox_surf;
#if 1
      generate_bbox_mesh(mesh, res, scale, bbox_surf);
#else
      bbox imesh_bbox;
      cartesian_csys csys;
      generate_bbox(mesh.xyz, imesh_bbox);
      csys.bbox_scale_centered(imesh_bbox, scale);
      mesh_box_with_tetgen(imesh_bbox, bbox_surf, res);
#endif

      file.assign(opts.outmsh_base, opts.ofmt);
      std::cout << "Writing mesh: " << file.base << std::endl;
      write_mesh_selected(bbox_surf, file.format, file.base);

      break;
    }

    case GEN_SPLIT:
    {
      mt_vector<MT_USET<mt_tag_t>> tagsA, tagsB;
      bool success = parse_splitlist_operation(opts.op, tagsA, tagsB);
      if (!success) return;

      // read mesh ------------------------------------------------------------------
      struct mt_meshdata mesh;
      mt_filename file(opts.msh_base, opts.ifmt);

      std::cout << "Reading mesh: " << file.base << std::endl;
      gettimeofday(&t1, NULL);
      read_mesh_selected(mesh, file.format, file.base, CRP_READ_ELEM | CRP_READ_PTS);
      transpose_connectivity(mesh.e2n_cnt, mesh.e2n_con, mesh.n2e_cnt, mesh.n2e_con);
      bucket_sort_offset(mesh.e2n_cnt, mesh.e2n_dsp);
      bucket_sort_offset(mesh.n2e_cnt, mesh.n2e_dsp);
      gettimeofday(&t2, NULL);
      std::cout << "Done in " << (float)timediff_sec(t1, t2) << " sec" << std::endl;

      // try to open split file
      // Now process splits ---------------------------------------------------------
      std::cout << "Computing splitlist .." << std::endl;
      gettimeofday(&t1, NULL);
      mt_vector<split_item> fin_list; // index of the new indices
      generate_splitlist(mesh, tagsA, tagsB, mesh.xyz.size()/3, fin_list);
      gettimeofday(&t2, NULL);
      std::cout << "Done in " << (float)timediff_sec(t1, t2) << " sec" << std::endl;

      // write splitlist -------------------------------------------------------------
      std::cout << "Writing splitlist: " << opts.odat << std::endl;
      gettimeofday(&t1, NULL);
      write_split_file(fin_list, opts.odat);
      gettimeofday(&t2, NULL);
      std::cout << "Done in " << (float)timediff_sec(t1, t2) << " sec" << std::endl;

      break;
    }

    case GEN_SURFSPLIT:
    {
      struct mt_meshdata mesh, surf;
      mt_filename file(opts.msh_base, opts.ifmt);

      std::cout << "Reading mesh: " << file.base << std::endl;
      gettimeofday(&t1, NULL);
      read_mesh_selected(mesh, file.format, file.base);
      compute_full_mesh_connectivity(mesh, file.base, true);
      gettimeofday(&t2, NULL);
      std::cout << "Done in " << (float)timediff_sec(t1, t2) << " sec" << std::endl;

      std::cout << "Reading surfaces .." << std::endl;
      unified_surface_from_list(opts.surf, ',', surf);
      compute_full_mesh_connectivity(surf, true);

      MT_USET<mt_idx_t> surf_nodes;
      surf_nodes.insert(surf.e2n_con.begin(), surf.e2n_con.end());

      mt_vector<vec3r> surf_alignment_field;
      generate_field_from_surface(mesh, surf, surf_alignment_field);
      for(vec3r & v : surf_alignment_field) v *= -1.0;
      write_vector_ascii(surf_alignment_field, "surface_alignment.vec");

      mt_vector<mt_real> nod_selection(surf_alignment_field.size(), 0.0);

      mt_vector<mt_mixed_tuple<mt_idx_t, mt_real>> init;
      for(mt_idx_t v : surf_nodes) init.push_back({v, mt_real(1.0)});

      mt_vector<vec3r> vtx_nod;
      array_to_points(mesh.xyz, vtx_nod);

      mt_data_transport<mt_idx_t, mt_real> trsp(mesh.n2n_cnt, mesh.n2n_dsp, mesh.n2n_con, vtx_nod);
      mt_real edge_len = avrg_edgelength_estimate(mesh, false);

      trsp(surf_alignment_field, mt_real(-0.05), edge_len*100.0, nod_selection, init);
      write_vector_ascii(nod_selection, "nod_selection.dat");

      MT_USET<mt_idx_t> sel_nidx, sel_eidx;
      sel_nidx.insert(surf_nodes.begin(), surf_nodes.end());
      for(int i=0; i<3; i++) {
        nodeSet_to_elemSet(mesh, sel_nidx, sel_eidx);
        elemSet_to_nodeSet(mesh, sel_eidx, sel_nidx);
      }

      mt_vector<mt_idx_t> idxA, idxB;

      for(mt_idx_t eidx : sel_eidx) {
        mt_idx_t start = mesh.e2n_dsp[eidx], stop = start + mesh.e2n_cnt[eidx];
        float selcnt = 0.0f;
        for(mt_idx_t j=start; j<stop; j++) {
          mt_idx_t c = mesh.e2n_con[j];
          if(nod_selection[c] > 0.0)
            selcnt += 1.0f;
        }

        if(selcnt / float(mesh.e2n_cnt[eidx]) > 0.9f)
          idxA.push_back(eidx);
        else
          idxB.push_back(eidx);
      }

      mt_meshgraph mgA, mgB;
      extract_meshgraph(idxA, mesh, mgA);
      extract_meshgraph(idxB, mesh, mgB);

      mt_vector<split_item> splitlist;
      mt_idx_t Nidx = mesh.xyz.size() / 3;
      add_interface_to_splitlist(mesh, mgA, mgB, Nidx, splitlist);

      mt_vector<mt_idx_t> oldidx, newidx;
      oldidx.reserve(splitlist.size()), newidx.reserve(splitlist.size());
      for(split_item & sp : splitlist) {
        oldidx.push_back(sp.oldIdx);
        newidx.push_back(sp.newIdx);
      }

      binary_sort(oldidx); unique_resize(oldidx);
      binary_sort(newidx); unique_resize(newidx);

      std::string filename = opts.odat;
      write_split_file(splitlist, filename);

      filename = opts.odat + ".new" + VTX_EXT;
      write_vtx(newidx, filename, true);
      filename = opts.odat + ".old" + VTX_EXT;
      write_vtx(oldidx, filename, true);

      break;
    }

#ifdef MT_ADDONS
    case GEN_REGION: {
      mt_vector<std::string> mesh_list, ins_tags_list, tags_list;
      split_string(opts.inpmsh_base, ',' , mesh_list);

      if(opts.ins_tag.size())
        split_string(opts.ins_tag, ',' , tags_list);

      MT_USET<mt_tag_t> tags;
      if(opts.tags.size())
        tags.insert(opts.tags.begin(), opts.tags.end());

      mt_meshdata background, integ_mesh, output_mesh;

      mt_filename file(opts.msh_base, opts.ifmt);
      std::cout << "Reading background mesh: " << file.base << std::endl;
      gettimeofday(&t1, NULL);
      read_mesh_selected(background, file.format, file.base);
      compute_full_mesh_connectivity(background, file.base, true);
      gettimeofday(&t2, NULL);
      std::cout << "Done in " << (float)timediff_sec(t1, t2) << " sec" << std::endl;

      int num_tags = tags_list.size();
      int num_mesh = mesh_list.size();
      bool tags_provided = num_tags == num_mesh;

      std::cout << "Reading region meshes:" << std::endl;
      gettimeofday(&t1, NULL);
      for(int idx=0; idx < num_mesh; idx++) {
        file.assign(mesh_list[idx], opts.ifmt);
        mt_meshdata actmesh;

        std::cout << file.base << std::endl;
        read_mesh_selected(actmesh, file.format, file.base);

        if(mesh_is_trisurf(actmesh)) {
          fprintf(stderr, "error: region meshes have to be volumetric! Aborting!\n");
          exit(EXIT_FAILURE);
        }

        if(tags_provided) {
          mt_tag_t tag = atoi(tags_list[idx].c_str());
          actmesh.etags.assign(actmesh.e2n_cnt.size(), tag);
        }

        mesh_union(integ_mesh, actmesh, false);
      }
      compute_full_mesh_connectivity(integ_mesh, false);

      gettimeofday(&t2, NULL);
      std::cout << "Done in " << (float)timediff_sec(t1, t2) << " sec" << std::endl;

      std::cout << "Integrating new regions .." << std::endl;
      gettimeofday(&t1, NULL);
      float padding = 5000;
      if(opts.padding.size()) padding = atof(opts.padding.c_str());

      bool do_integ_hole = false;
      if(opts.holes.size())
        do_integ_hole = bool(atoi(opts.holes.c_str()));

      bool do_integ_mesh = do_integ_hole == false ? true : false;

      float ref_inner = 0.0f, ref_outer;
      if(opts.res.size())       ref_inner = atof(opts.res.c_str());
      if(opts.res_outer.size()) ref_outer = atof(opts.res_outer.c_str());

      if(ref_inner) {
        mt_meshdata refined_background;
        refine_mesh_overlap(background, integ_mesh, refined_background, ref_inner, ref_outer, padding);
        compute_full_mesh_connectivity(refined_background, false);
        integrate_mesh(refined_background, integ_mesh, tags, output_mesh, do_integ_hole, do_integ_mesh, 1, 1);
      } else {
        int layers = padding / avrg_edgelength_estimate(background);
        layers = clamp<int>(1, 10, layers);
        integrate_mesh(background, integ_mesh, tags, output_mesh, do_integ_hole, do_integ_mesh, 1, layers);
      }

      gettimeofday(&t2, NULL);
      std::cout << "Done in " << (float)timediff_sec(t1, t2) << " sec" << std::endl;

      file.assign(opts.outmsh_base, opts.ofmt);
      std::cout << "Writing mesh: " << file.base << std::endl;
      write_mesh_selected(output_mesh, file.format, file.base);

      break;
    }

    case GEN_PURKINJE: {
      if (opts.tmpl_out) {
        std::cout << "Output template for purkinje configuration file ..." << std::endl;
        gettimeofday(&t1, NULL);
        purkinje_output_config_template(opts.config);
        gettimeofday(&t2, NULL);
        std::cout << "Done in " << (float)timediff_sec(t1, t2) << " sec" << std::endl;
      }
      else {
        std::cout << "Generating purkinje mesh..." << std::endl;
        gettimeofday(&t1, NULL);
        generate_purkinje(opts.msh_base, opts.lvsurf, opts.rvsurf,
                          opts.uvcfile, opts.config, opts.out, opts.verbose);
        gettimeofday(&t2, NULL);
        std::cout << "Done in " << (float)timediff_sec(t1, t2) << " sec" << std::endl;
      }

      break;
    }

    case GEN_EPSTATE: {
      ep_state EP;
      epstate_from_yaml(opts.config.c_str(), EP);
      write_EP_state(EP, opts.out);
      break;
    }
#endif

    default: break;
  }
}
