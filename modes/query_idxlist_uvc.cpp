#include "query_mode.h"

void query_idxlist_uvc_mode(const mt_vector<mt_real> &coords, const mt_vector<bool> &coord_is_lv,
                            const mt_vector<mt_real> &thr, const mt_vector<mt_real> &eps,
                            const mt_vector<mt_real> &xyz_cart, const mt_vector<mt_real> &UVCs, const mt_mask &is_LV,
                            mt_vector<mt_vector<mt_idx_t>> &vtxlist) {
  kdtree tree_cart(10);
  mt_vector<vec3r> ptsvec_cart;
  array_to_points(xyz_cart, ptsvec_cart);
  tree_cart.build_vertex_tree(ptsvec_cart);

  // split UVCs of mesh into two sets to set up seperate kdtrees
  mt_vector<vec3r> UVCs_vec;
  array_to_points(UVCs, UVCs_vec);

  mt_vector<vec3r> UVCs_lv, UVCs_rv;
  mt_vector<int> lv_UVC_idx, rv_UVC_idx;

  for (size_t i = 0; i < UVCs_vec.size(); i++) {
    if (is_LV.count(i)) {
      lv_UVC_idx.push_back(i);
      UVCs_lv.push_back(UVCs_vec[i]);
    } else {
      rv_UVC_idx.push_back(i);
      UVCs_rv.push_back(UVCs_vec[i]);
    }
  }

  // count how many lv / rv coord inputs
  mt_vector<int> lv_coord_idx, rv_coord_idx;
  for (size_t i = 0; i < coord_is_lv.size(); i++)
    if (coord_is_lv[i])
      lv_coord_idx.push_back(i);
    else
      rv_coord_idx.push_back(i);

  vtxlist.clear();
  vtxlist.resize(coord_is_lv.size());

  std::cout << "mesh UVCs consist of:" << std::endl;
  std::cout << UVCs_lv.size() << " LV coordinates" << std::endl;
  std::cout << UVCs_rv.size() << " RV coordinates" << std::endl;

  std::cout << "input consists of:" << std::endl;
  std::cout << lv_coord_idx.size() << " LV coordinates" << std::endl;
  std::cout << rv_coord_idx.size() << " RV coordinates" << std::endl;

  // left ventricle
  if (UVCs_lv.size()) {
    kdtree uvc_tree(10);
    uvc_tree.build_vertex_tree(UVCs_lv);

    std::cout << "Searching for vertices in LV .." << std::endl;

    for (int i : lv_coord_idx) {
      vec3f ctr(coords.data() + i * 3);
      vec3r dummy;
      mt_real dummy_len;
      int idx = 0;
      uvc_tree.closest_vertex(ctr, idx, dummy, dummy_len);  // Closest vertex in uvc coordinate
      mt_idx_t mesh_idx = lv_UVC_idx[idx];

      // Now locate all points in the given thresholds
      if (thr[i] > 0.0) {
        mt_vector<mt_mixed_tuple<mt_real, int>> vertices_cart;
        tree_cart.vertices_in_sphere(ptsvec_cart[mesh_idx], thr[i], vertices_cart);
        // Throw out anything that has a rho value outside of the epsilon-box
        for (auto &v : vertices_cart)
          if (is_LV.count(v.v2) && (std::abs(UVCs_vec[v.v2].x - UVCs_vec[mesh_idx].x) < eps[i]))
            vtxlist[i].push_back(v.v2);
      } else
        vtxlist[i].push_back(mesh_idx);
    }
  } else {
    for (int i : lv_coord_idx) vtxlist[i].push_back(-1);
  }

  // right ventricle
  if (UVCs_rv.size()) {
    kdtree uvc_tree(10);
    uvc_tree.build_vertex_tree(UVCs_rv);

    std::cout << "Searching for vertices in RV .." << std::endl;

    for (int i : rv_coord_idx) {
      vec3r ctr(coords.data() + i * 3);
      vec3r dummy;
      mt_real dummy_len;

      int idx = 0;
      uvc_tree.closest_vertex(ctr, idx, dummy, dummy_len);  // Closest vertex in uvc coordinate
      mt_idx_t mesh_idx = rv_UVC_idx[idx];

      // Now locate all points in the given thresholds
      if (thr[i] > 0.0) {
        mt_vector<mt_mixed_tuple<mt_real, int>> vertices_cart;
        tree_cart.vertices_in_sphere(ptsvec_cart[mesh_idx], thr[i], vertices_cart);

        // Throw out anything that has a rho value outside of the epsilon-box
        for (auto &v : vertices_cart)
          if (is_LV.count(v.v2) == 0 && (std::abs(UVCs_vec[v.v2].x - UVCs_vec[mesh_idx].x) < eps[i]))
            vtxlist[i].push_back(v.v2);
      } else
        vtxlist[i].push_back(mesh_idx);
    }
  } else {
    for (int i : rv_coord_idx) vtxlist[i].push_back(-1);
  }
}