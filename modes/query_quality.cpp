#include "data_structs.h"
#include "query_mode.h"

void query_quality_mode_read(const query_options &opts, query_quality_options &mode_opts, mt_meshdata &mesh) {
  struct timeval t1, t2;
  std::cout << "Reading mesh: " << opts.msh.base << std::endl;
  gettimeofday(&t1, NULL);

  read_mesh_selected(mesh, opts.msh.format, opts.msh.base, CRP_READ_ELEM | CRP_READ_PTS);
  mesh.e2n_dsp.resize(mesh.e2n_cnt.size());
  bucket_sort_offset(mesh.e2n_cnt, mesh.e2n_dsp);

  gettimeofday(&t2, NULL);
  std::cout << "Done in " << (float)timediff_sec(t1, t2) << " sec" << std::endl;

  if (opts.mode.size()) mode_opts.mode = atoi(opts.mode.c_str());

  if (opts.thr.size() > 0) {
    mode_opts.thr = atof(opts.thr.c_str());
    mode_opts.comp_thr = true;
  }
}

void query_quality_mode(const mt_meshdata &mesh, const query_quality_options &mode_opts,
                        mt_vector<mt_real> &elemqual  // out
) {
  struct timeval t1, t2;

  if (mode_opts.mode == 1) {
    std::cout << "Checking self intersection .." << std::endl;
    gettimeofday(&t1, NULL);

    mt_vector<mt_tuple<mt_idx_t>> intersec_edges;
    mt_vector<mt_idx_t> intersec_eidx;

    check_self_intersection(mesh, intersec_edges, intersec_eidx);

    if (intersec_edges.size() > 0) {
      mt_meshdata intersec;

      for (size_t i = 0; i < intersec_edges.size(); i++) {
        const mt_tuple<mt_idx_t> &edge = intersec_edges[i];
        const mt_idx_t &eidx = intersec_eidx[i];

        mt_idx_t lncon[2] = {edge.v1, edge.v2};
        mt_idx_t curtag = intersec.e2n_cnt.size();

        mesh_add_elem(intersec, Line, lncon, curtag);
        mesh_add_elem(intersec, mesh.etype[eidx], mesh.e2n_con.data() + mesh.e2n_dsp[eidx], curtag);
      }

      intersec.xyz = mesh.xyz;

      mt_vector<mt_idx_t> nod;
      reindex_nodes(intersec, nod);

      const char *intersec_basename = "intersec.mesh";
      fprintf(stderr,
              "Self intersections detected. Outputting mesh intersections into "
              "mesh: %s.vtk \n",
              intersec_basename);
      write_mesh_selected(intersec, "vtk_bin", intersec_basename);
    }

    gettimeofday(&t2, NULL);
    std::cout << "Done in " << (float)timediff_sec(t1, t2) << " sec" << std::endl;
  }

  std::cout << "Computing mesh quality .." << std::endl;
  gettimeofday(&t1, NULL);
  mesh_quality(mesh, elemqual);

  gettimeofday(&t2, NULL);
  std::cout << "Done in " << (float)timediff_sec(t1, t2) << " sec" << std::endl;

  mt_real min = elemqual[0], max = 0;
  mt_real avrg = 0.0, stddev = 0.0;

  int above_thr = 0;

  // get basic min-max
  for (size_t i = 0; i < elemqual.size(); i++) {
    mt_real c = elemqual[i];
    if (min > c) min = c;
    if (max < c) max = c;
    if (mode_opts.comp_thr && (c > mode_opts.thr)) above_thr++;
    avrg += c;
  }
  avrg /= (double)elemqual.size();

  for (size_t i = 0; i < elemqual.size(); i++) {
    mt_real c = elemqual[i];
    stddev += (c - avrg) * (c - avrg);
  }
  stddev /= ((double)(elemqual.size()) - 1.0);
  stddev = std::sqrt(stddev);

  // build up histogramm
  size_t numint = 50;
  float delta = 1.0f / numint;
  std::vector<float> xval(numint + 1), yval(numint + 1, 0);

  asciiPlotter plotter(22, 90);

  for (size_t i = 0; i < numint + 1; i++) xval[i] = i * delta;
  for (size_t i = 0; i < elemqual.size(); i++) {
    size_t idx = elemqual[i] / delta;
    if (idx < numint + 1) yval[idx] += 1.0f;
  }
  for (size_t i = 0; i < numint + 1; i++) yval[i] /= (float)elemqual.size();

  plotter.set_xrange(0.0, 1.0);
  plotter.set_yrange(yval);
  plotter.add_graph(xval, yval, '*');
  std::cout << std::endl << "--------- element quality statistics ---------" << std::endl;
  std::cout << "Used method: " << QMETRIC_NAME << " with 0 == best, 1 == worst" << std::endl;
  std::cout << "Min: " << min << " Max: " << max << " Mean: " << avrg << " Stddev: " << stddev << std::endl;
  if (mode_opts.comp_thr)
    std::cout << "Number of elements with quality above " << mode_opts.thr << " : " << above_thr << std::endl;

  std::cout << std::endl << "Histogramm of element quality:" << std::endl << std::endl;
  plotter.print();
}