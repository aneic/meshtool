/**
* @file itk_mode.cpp
* @brief Manipulation of ITK structured data.
* @author Aurel Neic, Matthias Gsell
* @version
* @date 2017-02-16
*/

#include "mt_modes_base.h"
#include "itk_utils.h"
#include "kdtree.h"
#include "itk_mode.h"

/// The operation to perform
enum ITK_OP {ITK_SMOOTH, ITK_CLOSE, ITK_NORMALIZE, ITK_PADDING, ITK_CROP, ITK_FLIP,
             ITK_DTYPE, ITK_RESAMPLE, ITK_INTERPOLATE, ITK_SAMPLE, ITK_EXTRACT, ITK_MESH,
             ITK_TRANSFORM, ITK_EXTRUDE, ITK_CONVERT};

static const std::string ref_par = "-ref=";
static const std::string reclamp_par = "-reclamp=";
static const std::string axes_par = "-axes=";
static const std::string dtype_par = "-dtype=";
static const std::string slice_par = "-slice=";
static const std::string pts_par = "-pts=";
static const std::string rad_par = "-rad=";
static const std::string transl_par = "-trans=";
static const std::string regtag_par = "-regtag=";
static const std::string newtag_par = "-newtag=";

static const std::string scan_flag = "-scan";
static const std::string surf_flag = "-surf";


#define ITK_PADDING_SIZE_DEFAULT "1,1,1"
#define ITK_AXES_DEFAULT "xyz"
#define ITK_DTYPE_DEFAULT 4

/// The itk mode options struct
struct itk_options
{
  std::string msh;
  std::string outmsh;
  std::string tags;
  std::string smth;
  std::string idat;
  std::string iter;
  std::string thr;
  std::string ref;
  std::string reclamp;
  std::string size;
  std::string axes;
  std::string dtype;
  std::string min;
  std::string max;
  std::string surf;
  std::string idx;
  std::string pts;
  std::string radius;
  std::string out_fmt;
  std::string scale;
  std::string slice;
  std::string transl;
  std::string regtag;
  std::string newtag;
  std::string mode;
  bool is_scan   = false;
  bool surf_only = false;
  ITK_OP op;
};

/**
* @brief itk smooth help message.
*/
void print_itk_smooth_help()
{
  fprintf(stderr, "itk smooth: Smooth the voxel data.\n");
  fprintf(stderr, "parameters:\n");
  fprintf(stderr, "%s<path>\t\t\t (input) Path of the mesh\n", mesh_par.c_str());
  fprintf(stderr, "%s<int>[,<int>,<int>]\t (optional) Discrete radius used for smoothing, single int or int triple (default 1).\n", thr_par.c_str());
  fprintf(stderr, "%s<float>[,<float>,<float>]\t (optional) Pixel refinement factor, single value or triple (default 1).\n", ref_par.c_str());
  fprintf(stderr, "%s<int>\t\t\t (optional) Number of smoothing iterations per clamping step.\n"
                  "\t\t\t\t    0 means only clamp after smoothing. Default is 0.\n", reclamp_par.c_str());
  fprintf(stderr, "%s<int>\t\t\t (optional) Number of smoothing iter (default %d).\n", iter_par.c_str(), SMOOTH_ITER_DEFAULT);
  fprintf(stderr, "%s<float>\t\t\t (optional) Smoothing coefficient (default %.2f).\n", smooth_par.c_str(), SMOOTH_DEFAULT);
  fprintf(stderr, "%s<tag1,tag2/tag1..>\t (optional) List of tags.\n", tags_par.c_str());
  fprintf(stderr, "%s<path>\t\t\t (output) Path of the output mesh\n", outmesh_par.c_str());
  fputs("\n\n", stderr);
}

/**
* @brief itk smooth help message.
*/
void print_itk_close_help()
{
  fprintf(stderr, "itk close: Apply closing (i.e. dilate-erode) algorithm to itk data.\n");
  fprintf(stderr, "parameters:\n");
  fprintf(stderr, "%s<path>\t\t\t (input) Path of the mesh\n", mesh_par.c_str());
  fprintf(stderr, "%s<tag1,tag2,..>\t\t (input) List of tags. Each tag is processed individually.\n", regtag_par.c_str());
  fprintf(stderr, "%s<int,int,...>\t\t (optional) Comma separated list of tags the region can grow into\n", tags_par.c_str());
  fprintf(stderr, "%s<int>,[<int>,<int>]\t (optional) Discrete radius used for smoothing, single int or int triple (default 5).\n", thr_par.c_str());
  fprintf(stderr, "%s<float>[,<float>,<float>]\t (optional) Pixel refinement factor, single value or triple (default 1).\n", ref_par.c_str());
  fprintf(stderr, "%s<int>\t\t\t (optional) Number of iterations. Default is 1.\n", iter_par.c_str());
  fprintf(stderr, "%s<path>\t\t\t (output) Path of the output mesh\n", outmesh_par.c_str());
  fputs("\n\n", stderr);
}
/**
 * @brief itk normalize help
 */
void print_itk_normalize_help()
{
  fprintf(stderr, "itk normalize: Normalize voxel spacing.\n");
  fprintf(stderr, "parameters:\n");
  fprintf(stderr, "%s<path>\t\t\t (input) Path of the mesh\n", mesh_par.c_str());
  fprintf(stderr, "%s<path>\t\t\t (output) Path of the output mesh\n", outmesh_par.c_str());
  fprintf(stderr, "%s<float>[,<float>,<float>]\t (optional) Pixel refinement factor, single value or triple (default 1).\n", ref_par.c_str());
  fprintf(stderr, "%s<float>[,<float>,<float>]\t (optional) Pixel target size, single value or triple.", size_par.c_str());
  fputs("\n\n", stderr);
}


/**
 * @brief itk padding help
 */
void print_itk_padding_help()
{
  fprintf(stderr, "itk padding: add padding to voxel data.\n");
  fprintf(stderr, "parameters:\n");
  fprintf(stderr, "%s<path>\t\t\t (input) Path of the mesh\n", mesh_par.c_str());
  fprintf(stderr, "%s<path>\t\t\t (output) Path of the output mesh\n", outmesh_par.c_str());
  fprintf(stderr, "%s<int>,<int>,<int>\t\t (input) Comma separated padding sizes for each axis (default '%s').\n", size_par.c_str(), ITK_PADDING_SIZE_DEFAULT);
  fputs("\n\n", stderr);
}

/**
 * @brief itk crop help
 */
void print_itk_crop_help()
{
  fprintf(stderr, "itk crop: remove surrounding whitespace.\n");
  fprintf(stderr, "parameters:\n");
  fprintf(stderr, "%s<path>\t\t\t (input) Path of the mesh\n", mesh_par.c_str());
  fprintf(stderr, "%s<path>\t\t\t (output) Path of the output mesh\n", outmesh_par.c_str());
  fprintf(stderr, "%s<float>[,<float>,<float>]\t (optional) Pixel refinement factor, single value or triple (default 1).\n", ref_par.c_str());
  fputs("\n\n", stderr);
}

/**
 * @brief itk flip help
 */
void print_itk_flip_help()
{
  fprintf(stderr, "itk flip: flip the voxel data along given axes.\n");
  fprintf(stderr, "parameters:\n");
  fprintf(stderr, "%s<path>\t\t\t (input) Path of the mesh\n", mesh_par.c_str());
  fprintf(stderr, "%s<path>\t\t\t (output) Path of the output mesh\n", outmesh_par.c_str());
  fprintf(stderr, "%s<str>\t\t\t (input) Axes along which the image is flipped (default '%s')\n", axes_par.c_str(), ITK_AXES_DEFAULT);
  fprintf(stderr, "\t\t\t\t the string consists of the following characters {'x', 'y', 'z'}\n");
  fputs("\n\n", stderr);
}

/**
 * @brief itk dtype help
 */
void print_itk_dtype_help()
{
  fprintf(stderr, "itk dtype: convert datatype.\n");
  fprintf(stderr, "parameters:\n");
  fprintf(stderr, "%s<path>\t\t\t (input) Path of the mesh\n", mesh_par.c_str());
  fprintf(stderr, "%s<path>\t\t\t (output) Path of the output mesh\n", outmesh_par.c_str());
  fprintf(stderr, "%s<int>\t\t\t (input) New datatype (default '%d')\n", dtype_par.c_str(), ITK_DTYPE_DEFAULT);
  for (int i = 1; i < 11; i++)
    fprintf(stderr, "\t\t\t\t    %d  %s\n", i, itk_get_datatype_str_vtk(i));
  fprintf(stderr, "\t\t\t\t    %d  color scalars\n", 11);
  fputs("\n\n", stderr);
}

/**
 * @brief itk dtype help
 */
void print_itk_refine_help()
{
  fprintf(stderr, "itk resample: resample voxel data.\n");
  fprintf(stderr, "parameters:\n");
  fprintf(stderr, "%s<path>\t\t\t (input) Path of the mesh\n", mesh_par.c_str());
  fprintf(stderr, "%s<path>\t\t\t (output) Path of the output mesh\n", outmesh_par.c_str());
  fprintf(stderr, "%s<float>[,<float>,<float>]\t (optional) Pixel refinement factor, single value or triple (default 1).\n", ref_par.c_str());
  fprintf(stderr, "%s<float>[,<float>,<float>]\t (optional) Pixel target size, single value or triple.", size_par.c_str());
  fputs("\n\n", stderr);
}

/**
 * @brief itk sample help
 */
void print_itk_sample_help()
{
  fprintf(stderr, "itk sample: create an itk image stack from sampling surfaces.\n");
  fprintf(stderr, "parameters:\n");
  fprintf(stderr, "%s<path>\t\t\t (optional) Path of the image stack to sample on\n", mesh_par.c_str());
  fprintf(stderr, "%s<x,y,z>\t\t\t (optional) Lower left point of ITK image space\n", min_par.c_str());
  fprintf(stderr, "%s<x,y,z>\t\t\t (optional) Upper right point of ITK image space\n", max_par.c_str());
  fprintf(stderr, "%s<size>\t\t\t (input) ITK pixel spacing\n", size_par.c_str());
  fprintf(stderr, "%s<surf1,surf2,..>\t\t (input) List of surfaces to sample\n", surf_par.c_str());
  fprintf(stderr, "%s<tag1,tag2,..>\t\t (optional) tag indices for inserted regions. Should match order of surfaces.\n", tags_par.c_str());
  fprintf(stderr, "%s<path>\t\t\t (output) Path of the output file\n", outmesh_par.c_str());
  //fprintf(stderr, "%s<string>\t\t\t (optional) ITK datatype (default '%s')\n", dtype_par.c_str(), itk_get_datatype_str_vtk(ITK_DTYPE_DEFAULT));
  fputs("\n", stderr);
  fprintf(stderr, "Note: If indices of the '%s' arguments are '_', the tag of the corresponding\n"
                  "mesh is taken!\n", tags_par.c_str());
  fputs("\n\n", stderr);
}

/**
 * @brief itk extract help
 */
void print_itk_extract_help()
{
  fprintf(stderr, "itk extract: extract slices of an itk image stack.\n");
  fprintf(stderr, "parameters:\n");
  fprintf(stderr, "%s<path>\t\t\t (input) Path of the image stack to sample on\n", mesh_par.c_str());
  fprintf(stderr, "%s<plane>\t\t\t (input) Plane-axes from which to extract (xy, xz, yz)\n", axes_par.c_str());
  fprintf(stderr, "%s<int,>\t\t\t (input) Comma separated indices of slices to extract\n", idx_par.c_str());
  fprintf(stderr, "%s<path>\t\t\t (output) Path of the output file\n", outmesh_par.c_str());
  fputs("\n\n", stderr);
}

/**
 * @brief itk mesh help
 */
void print_itk_mesh_help()
{
  fprintf(stderr, "itk mesh: convert itk image to hexahedral mesh.\n");
  fprintf(stderr, "parameters:\n");
  fprintf(stderr, "%s<path>\t\t\t (input) Path of the image stack to sample on\n", mesh_par.c_str());
  fprintf(stderr, "%s<path>\t\t\t (output) Path of the output file\n", outmesh_par.c_str());
  fprintf(stderr, "%s<float>\t\t\t (optional) Scaling parameter (default 1.0)\n", scale_par.c_str());
  fprintf(stderr, "%s<format>\t\t\t (optional) Format of the output mesh (default carp_txt)\n", out_format_par.c_str());
  fprintf(stderr, "%s<slice>\t\t\t (optional) Comma separated list of slices to extract (example: 'x:1,y:4,6,z:3,4,6')\n", slice_par.c_str());
  fprintf(stderr, "%s<int,int,...>\t\t (optional) Comma separated list of tags to consider (ignored for scans)\n", tags_par.c_str());
  fprintf(stderr, "%s\t\t\t\t (optional) Consider integer image as scan\n", scan_flag.c_str());
  fprintf(stderr, "%s\t\t\t\t (optional) Extract surface mesh only (ignored for scans or slices)\n\n", surf_flag.c_str());
  fprintf(stderr, "The supported output formats are:\n%s\n\n", output_formats.c_str());
  fprintf(stderr, "Note: By default integer valued images are considered as segmentations,\n");
  fprintf(stderr, "      whereas real valued images are considered as scans.\n");
  fputs("\n\n", stderr);
}


/**
 * @brief itk interpolate help
 */
void print_itk_interpolate_help()
{
  fprintf(stderr, "itk interpolate: interpolate clouddata to itk image.\n");
  fprintf(stderr, "parameters:\n");
  fprintf(stderr, "%s<path>\t\t\t (input) Path of the image stack to interpolate the clouddata to\n", mesh_par.c_str());
  fprintf(stderr, "%s<path>\t\t\t (input) Path to the coordinates of the point cloud\n", pts_par.c_str());
  fprintf(stderr, "%s<path>\t\t\t (input) Path to input data\n", idat_par.c_str());
  fprintf(stderr, "%s<path>\t\t\t (output) Path of the output file\n", outmesh_par.c_str());
  // TODO: check if default is correctly set
  fprintf(stderr, "%s<float>\t\t\t (optional) Choose interpolation radius. Default is 1.0\n", rad_par.c_str());
  fputs("\n\n", stderr);
}


/**
 * @brief itk transform help
 */
void print_itk_transform_help()
{
  fprintf(stderr, "itk transform: transform an itk image stack.\n");
  fprintf(stderr, "parameters:\n");
  fprintf(stderr, "%s<path>\t\t\t (input) Path of the image stack to sample on\n", mesh_par.c_str());
  fprintf(stderr, "%s<float>\t\t\t (optional) Vertex scaling (single value or 3 comma separated values)\n", scale_par.c_str());
  fprintf(stderr, "%s<x,y,z>\t\t\t (optional) Vertex translation\n", transl_par.c_str());
  fprintf(stderr, "%s<path>\t\t\t (output) Path of the output file\n", outmesh_par.c_str());
  fputs("\n", stderr);
  fprintf(stderr, "translation is applied after scaling and not affected by the scaling.\n");
  fputs("\n\n", stderr);
}


/**
 * @brief itk extrude help
 */
void print_itk_extrude_help()
{
  fprintf(stderr, "itk extrude: transform an itk image stack.\n");
  fprintf(stderr, "parameters:\n");
  fprintf(stderr, "%s<path>\t\t\t (input) Path of the image stack to sample on\n", mesh_par.c_str());
  fprintf(stderr, "%s<mode>\t\t\t (optional) Extrude mode, choices are 'in','out','both' (default 'in')\n", mode_par.c_str());
  fprintf(stderr, "%s<size>\t\t\t (optional) Extrude radius > 0 (default 1)\n", rad_par.c_str());
  fprintf(stderr, "%s<int>\t\t\t (input) Tag of region to extrude\n", regtag_par.c_str());
  fprintf(stderr, "%s<int>\t\t\t (optional) Tag of new region (default -1)\n", newtag_par.c_str());
  fprintf(stderr, "\t\t\t\t    if negative, the tag of the extruded region is taken\n");
  fprintf(stderr, "%s<int,int,...>\t\t (optional) Comma separated list of tags the region can grow into (default 0)\n", tags_par.c_str());
  fprintf(stderr, "\t\t\t\t    the void space (tag=0) must be specified explicitly\n");
  fprintf(stderr, "%s<path>\t\t\t (output) Path of the output file\n", outmesh_par.c_str());
  fputs("\n\n", stderr);
}


/**
 * @brief itk convert help
 */
void print_itk_convert_help()
{
  fprintf(stderr, "itk convert: convert format of an image stack.\n");
  fprintf(stderr, "parameters:\n");
  fprintf(stderr, "%s<path>\t\t\t (input) Path of the input image stack\n", mesh_par.c_str());
  fprintf(stderr, "%s<path>\t\t\t (output) Path of the output file\n", outmesh_par.c_str());
  fputs("\n\n", stderr);
}


/**
* @brief itk mode options parser.
*
* @param [in]  argc Arguments count.
* @param [in]  argv Arguments string-array.
* @param [out] opts Options structure.
*
* @return
*/
int itk_parse_options(int argc, char** argv, struct itk_options & opts)
{
  if(argc < 3) {
    fprintf(stderr, "Please choose one of the following modes:\n");
    print_itk_smooth_help();
    print_itk_close_help();
    print_itk_normalize_help();
    print_itk_padding_help();
    print_itk_crop_help();
    print_itk_flip_help();
    print_itk_dtype_help();
    print_itk_refine_help();
    print_itk_sample_help();
    print_itk_extract_help();
    print_itk_mesh_help();
    print_itk_interpolate_help();
    print_itk_transform_help();
    print_itk_extrude_help();
    print_itk_convert_help();
    return 1;
  }

  std::string itkmode = argv[2];
  opts.is_scan = false;

  // parse parameters -----------------------------------------------------------------
  for(int i=3; i<argc; i++){
    std::string param = reformat_param(argv, i, argc);
    bool match = false;

    if(!match) match = parse_param(param, mesh_par, opts.msh);
    if(!match) match = parse_param(param, outmesh_par, opts.outmsh);
    if(!match) match = parse_param(param, tags_par, opts.tags);
    if(!match) match = parse_param(param, smooth_par, opts.smth);
    if(!match) match = parse_param(param, iter_par, opts.iter);
    if(!match) match = parse_param(param, thr_par, opts.thr);
    if(!match) match = parse_param(param, ref_par, opts.ref);
    if(!match) match = parse_param(param, reclamp_par, opts.reclamp);
    if(!match) match = parse_param(param, size_par, opts.size);
    if(!match) match = parse_param(param, axes_par, opts.axes);
    if(!match) match = parse_param(param, dtype_par, opts.dtype);
    if(!match) match = parse_param(param, min_par, opts.min);
    if(!match) match = parse_param(param, max_par, opts.max);
    if(!match) match = parse_param(param, pts_par, opts.pts);
    if(!match) match = parse_param(param, surf_par, opts.surf);
    if(!match) match = parse_param(param, idx_par, opts.idx);
    if(!match) match = parse_param(param, idat_par, opts.idat);
    if(!match) match = parse_param(param, rad_par, opts.radius);
    if(!match) match = parse_param(param, out_format_par, opts.out_fmt);
    if(!match) match = parse_param(param, scale_par, opts.scale);
    if(!match) match = parse_param(param, slice_par, opts.slice);
    if(!match) match = parse_param(param, transl_par, opts.transl);
    if(!match) match = parse_param(param, regtag_par, opts.regtag);
    if(!match) match = parse_param(param, newtag_par, opts.newtag);
    if(!match) match = parse_param(param, mode_par, opts.mode);
    if(!match) match = parse_flag(param, scan_flag, opts.is_scan);
    if(!match) match = parse_flag(param, surf_flag, opts.surf_only);

    if(!match) {
      std::cerr << "Error: Cannot parse parameter " << param << std::endl;
      return 2;
    }
  }

  if(itkmode.compare("smooth") == 0)
  {
    opts.op = ITK_SMOOTH;

    // check if all relevant parameters have been set ---------------------------------------------------
    if( ! (opts.msh.size() > 0 && opts.outmsh.size() > 0 ) )
    {
      std::cerr << "itk smooth: Insufficient parameters provided." << std::endl;
      print_itk_smooth_help();
      return 4;
    }
  }
  else if (itkmode.compare("close") == 0)
  {
    opts.op = ITK_CLOSE;

    // check if all relevant parameters have been set --------------------------------------------------
    if( ! (opts.msh.size() > 0 && opts.outmsh.size() > 0 ) )
    {
      std::cerr << "itk close: Insufficient parameters provided." << std::endl;
      print_itk_close_help();
      return 4;
    }
  }
  else if (itkmode.compare("normalize") == 0)
  {
    opts.op = ITK_NORMALIZE;

    // check if all relevant parameters have been set --------------------------------------------------
    if( ! (opts.msh.size() > 0 && opts.outmsh.size() > 0 ) )
    {
      std::cerr << "itk normalize: Insufficient parameters provided." << std::endl;
      print_itk_normalize_help();
      return 4;
    }
  }
  else if (itkmode.compare("padding") == 0)
  {
    opts.op = ITK_PADDING;

    // check if all relevant parameters have been set --------------------------------------------------
    if( ! (opts.msh.size() > 0 && opts.outmsh.size() > 0 ) )
    {
      std::cerr << "itk padding: Insufficient parameters provided." << std::endl;
      print_itk_padding_help();
      return 4;
    }
  }
  else if (itkmode.compare("crop") == 0)
  {
    opts.op = ITK_CROP;

    // check if all relevant parameters have been set --------------------------------------------------
    if( ! (opts.msh.size() > 0 && opts.outmsh.size() > 0 ) )
    {
      std::cerr << "itk crop: Insufficient parameters provided." << std::endl;
      print_itk_crop_help();
      return 4;
    }
  }
  else if (itkmode.compare("flip") == 0)
  {
    opts.op = ITK_FLIP;

    // check if all relevant parameters have been set -------------------------------
    if( ! (opts.msh.size() > 0 && opts.outmsh.size() > 0 ) )
    {
      std::cerr << "itk flip: Insufficient parameters provided." << std::endl;
      print_itk_flip_help();
      return 4;
    }
  }
  else if (itkmode.compare("dtype") == 0)
  {
    opts.op = ITK_DTYPE;

    // check if all relevant parameters have been set -------------------------------
    if( ! (opts.msh.size() > 0 && opts.outmsh.size() > 0 ) )
    {
      std::cerr << "itk dtype: Insufficient parameters provided." << std::endl;
      print_itk_dtype_help();
      return 4;
    }
  }
  else if (itkmode.compare("resample") == 0)
  {
    opts.op = ITK_RESAMPLE;

    // check if all relevant parameters have been set -------------------------------
    if( ! (opts.msh.size() > 0 && opts.outmsh.size() > 0 ) )
    {
      std::cerr << "itk dtype: Insufficient parameters provided." << std::endl;
      print_itk_refine_help();
      return 4;
    }
  }
  else if (itkmode.compare("sample") == 0)
  {
    opts.op = ITK_SAMPLE;

    // check if all relevant parameters have been set -------------------------------
    if( !(opts.surf.size() && opts.outmsh.size()) )
    {
      std::cerr << "itk sample: Insufficient parameters provided." << std::endl;
      print_itk_sample_help();
      return 4;
    }
  }
  else if (itkmode.compare("extract") == 0)
  {
    opts.op = ITK_EXTRACT;

    bool plane_defined = opts.axes.size() && ((opts.axes == "xy") || (opts.axes == "xz") || (opts.axes == "yz"));

    // check if all relevant parameters have been set -------------------------------
    if( !(opts.msh.size() && opts.idx.size() && plane_defined && opts.outmsh.size()) )
    {
      std::cerr << "itk extract: Insufficient parameters provided." << std::endl;
      print_itk_extract_help();
      return 4;
    }
  }
  else if (itkmode.compare("mesh") == 0)
  {
    opts.op = ITK_MESH;

    // check if all relevant parameters have been set -------------------------------
    if( !(opts.msh.size() && opts.outmsh.size()) )
    {
      std::cerr << "itk mesh: Insufficient parameters provided." << std::endl;
      print_itk_mesh_help();
      return 4;
    }
  }
  else if (itkmode.compare("interpolate") == 0)
  {
    opts.op = ITK_INTERPOLATE;

    // check if all relevant parameters have been set -------------------------------
    if( !(opts.msh.size() &&  opts.pts.size() && opts.outmsh.size() && opts.idat.size()) )
    {
      std::cerr << "itk interpolate: Insufficient parameters provided." << std::endl;
      print_itk_interpolate_help();
      return 4;
    }
  }
  else if (itkmode.compare("transform") == 0)
  {
    opts.op = ITK_TRANSFORM;

    // check if all relevant parameters have been set -------------------------------
    if( !(opts.msh.size() && (opts.scale.size() || opts.transl.size())) )
    {
      std::cerr << "itk transform: Insufficient parameters provided." << std::endl;
      print_itk_transform_help();
      return 4;
    }
  }
  else if (itkmode.compare("extrude") == 0)
  {
    opts.op = ITK_EXTRUDE;
    if( !(opts.msh.size() && opts.regtag.size() && opts.outmsh.size()) )
    {
      std::cerr << "itk extrude: Insufficient parameters provided." << std::endl;
      print_itk_extrude_help();
      return 4;
    }
  }
  else if (itkmode.compare("convert") == 0)
  {
    opts.op = ITK_CONVERT;
    if( !(opts.msh.size() && opts.outmsh.size()) )
    {
      std::cerr << "itk convert: Insufficient parameters provided." << std::endl;
      print_itk_convert_help();
      return 4;
    }
  }
  else {
    print_usage(argv[0]);
    return 2;
  }
  return 0;
}



void itk_sample(itk_image & img, const mt_meshdata & surfmesh, const kdtree & tree, mt_tag_t tag=0)
{
  itk_access<short> data(img);
  // we assume that the surface has a uniform tag index and require that it is nonzero
  short curtag = surfmesh.etags[0];
  if (tag != 0) curtag = static_cast<short>(tag);
  if (curtag == 0) curtag = 1;

  PROGRESS<unsigned int> prg(data.dim.v1, "Sampling progress: ");

  for(unsigned int i=0; i<data.dim.v1; i++) {
    #ifdef OPENMP
    #pragma omp parallel for schedule(dynamic)
    #endif
    for(unsigned int j=0; j<data.dim.v2; j++)
      for(unsigned int k=0; k<data.dim.v3; k++) {
        vec3f pos = data.position<float>(i,j,k);
        if(inside_closed_surface(tree, pos))
          data(i,j,k) = curtag;
      }

    prg.next();
  }

  prg.finish();
}

int read_refining_info(std::string & ref_opts, mt_triple<float>& ref, const mt_triple<float>& def)
{
  ref = def;
  if (ref_opts.empty())
    return -1;

  int rval = 0;
  mt_vector<std::string> reflist;
  split_string(ref_opts.c_str(), ',', reflist);
  if (reflist.size() == 1) {
    ref.v1 = ref.v2 = ref.v3 = atof(reflist[0].c_str());
    rval = 1;
  } else if (reflist.size() == 3) {
    ref.v1 = atof(reflist[0].c_str());
    ref.v2 = atof(reflist[1].c_str());
    ref.v3 = atof(reflist[2].c_str());
    rval = 3;
  } else {
    fprintf(stderr, "\n%s: cannot parse \"%s\". Expecting single value or triple!\n", __func__, ref_opts.c_str());
  }
  return rval;
}


bool get_slices(const std::string& slice_str, mt_triple<MT_USET<mt_idx_t>>& slices)
{
  if (slice_str.empty())
    return false;

  bool has_slices = false;

  mt_vector<std::string> slice_list;
  split_string(slice_str, ',', slice_list);
  size_t len;

  MT_USET<mt_idx_t>* slice_set = nullptr;
  std::string slice_nbr;

  for (size_t i=0; i<slice_list.size(); i++) {

    if ((slice_list[i][0] == 'x') && (slice_list[i][1] == ':')) {
      slice_set = &slices.v1;
      slice_nbr.assign(&slice_list[i].c_str()[2]);
    }
    else if ((slice_list[i][0] == 'y') && (slice_list[i][1] == ':')) {
      slice_set = &slices.v2;
      slice_nbr.assign(&slice_list[i].c_str()[2]);
    }
    else if ((slice_list[i][0] == 'z') && (slice_list[i][1] == ':')) {
      slice_set = &slices.v3;
      slice_nbr.assign(&slice_list[i].c_str()[2]);
    }
    else
      slice_nbr = slice_list[i];

    if (slice_set != nullptr)
    {
      try
      {
        const mt_idx_t idx = static_cast<mt_idx_t>(std::stoll(slice_nbr, &len));
        if (len == slice_nbr.length()) {
          slice_set->insert(idx);
          has_slices = true;
        }
        else
          std::cout << "Error, failed to parse '" << slice_list[i] << "' !" << std::endl;
      }
      catch (std::exception& ex) {
        std::cout << "Error, failed to parse '" << slice_list[i] << "' !" << std::endl;
      }
    }
    else
      std::cout << "Error, failed to parse '" << slice_list[i] << "' !" << std::endl;
  }

  return has_slices;
}


/**
* @brief Smoothing mode function.
*
* @param [in]  argc Arguments count.
* @param [in]  argv Arguments string-array.
*/
void itk_mode(int argc, char** argv)
{
  struct itk_options opts;
  int ret = itk_parse_options(argc, argv, opts);
  struct timeval t1, t2;

  if(ret != 0) return;

  switch(opts.op)
  {
    case ITK_SMOOTH:
    {
      double smooth  = opts.smth.size()    > 0 ? atof(opts.smth.c_str())    : SMOOTH_DEFAULT;
      int    iter    = opts.iter.size()    > 0 ? atoi(opts.iter.c_str())    : SMOOTH_ITER_DEFAULT;
      int    reclamp = opts.reclamp.size() > 0 ? atoi(opts.reclamp.c_str()) : 0;

      mt_triple<short> rad = {1, 1, 1};
      if (opts.thr.size() > 0)
      {
        mt_vector<std::string> radlist;
        split_string(opts.thr.c_str(), ',', radlist);
        if (radlist.size() == 1)
        {
          rad.v1 = rad.v2 = rad.v3 = atoi(radlist[0].c_str());
        }
        else if (radlist.size() == 3)
        {
          rad.v1 = atoi(radlist[0].c_str());
          rad.v2 = atoi(radlist[1].c_str());
          rad.v3 = atoi(radlist[2].c_str());
        }
        else
        {
          std::cerr << std::endl << "argument error: '" << thr_par.c_str() << "' expects one single int or a int triple!" << std::endl;
          print_itk_smooth_help();
          return;
        }
      }

      mt_triple<float> ref;
      read_refining_info(opts.ref, ref, {1.0, 1.0, 1.0});
      itk_image input_img, comp_img;

      std::cout << "Reading image: " << opts.msh << std::endl;
      gettimeofday(&t1, NULL);
      if(!input_img.read_file(opts.msh.c_str()))
      {
        std::cerr << "Error: Could not read image file " << opts.msh << std::endl;
        return;
      }
      gettimeofday(&t2, NULL);
      std::cout << "Done in " << (float)timediff_sec(t1, t2) << " sec" << std::endl;

      printf("Refining to (%g, %g, %g) of original resolution..\n", ref.v1, ref.v2, ref.v3);
      gettimeofday(&t1, NULL);
      resample_image(input_img, ref);
      gettimeofday(&t2, NULL);
      std::cout << "Done in " << (float)timediff_sec(t1, t2) << " sec" << std::endl;

      std::cout << "Converting image data to float .. " << std::endl;
      gettimeofday(&t1, NULL);
      comp_img.assign(input_img, "float");
      gettimeofday(&t2, NULL);
      std::cout << "Done in " << (float)timediff_sec(t1, t2) << " sec" << std::endl;

      if(opts.tags.size()) {
        mt_vector<std::string> operlist;
        split_string(opts.tags, '/' , operlist);

        itk_image timg;
        timg.assign(comp_img.dim, comp_img.orig, comp_img.pixspc, comp_img.ncomp, "float");

        for(size_t oper = 0; oper < operlist.size(); oper++)
        {
          mt_vector<std::string> taglist;
          std::set<float> tg;

          split_string(operlist[oper], ',' , taglist);
          for(std::string & s : taglist) tg.insert(atof(s.c_str()));

          timg.databuff.zero();
          itk_extract_vals(comp_img, timg, tg);

          printf("Smoothing tags %s\n", operlist[oper].c_str());
          gettimeofday(&t1, NULL);

          smooth_itk(timg, rad, smooth, iter);

          gettimeofday(&t2, NULL);
          std::cout << "Done in " << (float)timediff_sec(t1, t2) << " sec" << std::endl;

          std::cout << "Clamping .. " << std::endl;
          gettimeofday(&t1, NULL);
          clamp_itk(timg, input_img, rad);
          gettimeofday(&t2, NULL);
          std::cout << "Done in " << (float)timediff_sec(t1, t2) << " sec" << std::endl;

          itk_insert_vals(timg, comp_img, tg);
        }
      }
      else if (reclamp == 0) {
        std::cout << "Smoothing .. " << std::endl;
        gettimeofday(&t1, NULL);
        smooth_itk(comp_img, rad, smooth, iter);
        // comp_img.write_file("debug.smth.vtk");
        gettimeofday(&t2, NULL);
        std::cout << "Done in " << (float)timediff_sec(t1, t2) << " sec" << std::endl;

        std::cout << "Clamping .. " << std::endl;
        gettimeofday(&t1, NULL);
        clamp_itk(comp_img, input_img, rad);
        gettimeofday(&t2, NULL);
        std::cout << "Done in " << (float)timediff_sec(t1, t2) << " sec" << std::endl;
      }
      else {
        std::cout << "Smoothing .. " << std::endl;
        gettimeofday(&t1, NULL);
        int it_idx = iter % reclamp;

        smooth_itk(comp_img, rad, smooth, it_idx);
        clamp_itk(comp_img, input_img, rad);

        while(it_idx < iter)
        {
          smooth_itk(comp_img, rad, smooth, reclamp);
          clamp_itk(comp_img, input_img, rad);
          it_idx += reclamp;
        }
        gettimeofday(&t2, NULL);
        std::cout << "Done in " << (float)timediff_sec(t1, t2) << " sec" << std::endl;
      }

      std::cout << "Writing output .. " << std::endl;
      gettimeofday(&t1, NULL);
      input_img.assign(comp_img, itk_get_datatype_str_vtk(input_img.comptypeid));
      input_img.write_file(opts.outmsh);
      gettimeofday(&t2, NULL);
      std::cout << "Done in " << (float)timediff_sec(t1, t2) << " sec" << std::endl;
      break;
    }

    case ITK_CLOSE:
    {
      int    iter    = opts.iter.size()    > 0 ? atoi(opts.iter.c_str())    : 1;

      mt_triple<short> rad = {1, 1, 1};
      if (opts.thr.size() > 0)
      {
        mt_vector<std::string> radlist;
        split_string(opts.thr.c_str(), ',', radlist);
        if (radlist.size() == 1)
        {
          rad.v1 = rad.v2 = rad.v3 = atoi(radlist[0].c_str());
        }
        else if (radlist.size() == 3)
        {
          rad.v1 = atoi(radlist[0].c_str());
          rad.v2 = atoi(radlist[1].c_str());
          rad.v3 = atoi(radlist[2].c_str());
        }
        else
        {
          std::cerr << std::endl << "argument error: '" << thr_par.c_str() << "' expects one single int or a int triple!" << std::endl;
          print_itk_close_help();
          return;
        }
      }

      mt_triple<float> ref;
      read_refining_info(opts.ref, ref, {1.0, 1.0, 1.0});

      itk_image input_img;

      std::cout << "Reading image: " << opts.msh << std::endl;
      gettimeofday(&t1, NULL);
      input_img.read_file(opts.msh.c_str());
      gettimeofday(&t2, NULL);
      std::cout << "Done in " << (float)timediff_sec(t1, t2) << " sec" << std::endl;

      mt_vector<std::string> taglist, regtag;
      bool use_taglist = opts.tags.size() > 0;
      if(use_taglist) {
        split_string(opts.regtag, ',', regtag);
        split_string(opts.tags, ',', taglist);
      }

      itk_close_mode(input_img, iter, rad, opts.ref.size() > 0, ref, use_taglist, regtag, taglist);


      std::cout << "Writing output .. " << std::endl;
      gettimeofday(&t1, NULL);
      input_img.write_file(opts.outmsh);
      gettimeofday(&t2, NULL);
      std::cout << "Done in " << (float)timediff_sec(t1, t2) << " sec" << std::endl;
      break;
    }

    case ITK_NORMALIZE:
    {
      mt_triple<float> ref, size;
      read_refining_info(opts.ref, ref, {1.0, 1.0, 1.0});
      const bool has_size = !opts.size.empty();

      itk_image img;

      std::cout << "Reading image: " << opts.msh << std::endl;
      gettimeofday(&t1, NULL);
      img.read_file(opts.msh.c_str());
      gettimeofday(&t2, NULL);
      std::cout << "Done in " << (float)timediff_sec(t1, t2) << " sec" << std::endl;

      if (has_size) {
        read_refining_info(opts.size, size, {-1.0, -1.0, -1.0});
        std::cout << "!!! Size given, overwriting refinement factors !!!" << std::endl;
        if (size.v1 > 0.0) ref.v1 = img.pixspc.v1/size.v1;
        if (size.v2 > 0.0) ref.v2 = img.pixspc.v2/size.v2;
        if (size.v3 > 0.0) ref.v3 = img.pixspc.v3/size.v3;
      }

      std::cout << "Normalizing voxel spacing .. " << std::endl;
      gettimeofday(&t1, NULL);
      img.normalize_spacing();
      gettimeofday(&t2, NULL);
      std::cout << "Done in " << (float)timediff_sec(t1, t2) << " sec" << std::endl;

      printf("Refining to (%g, %g, %g) of original resolution..\n", ref.v1, ref.v2, ref.v3);
      gettimeofday(&t1, NULL);
      resample_image(img, ref);
      gettimeofday(&t2, NULL);
      std::cout << "Done in " << (float)timediff_sec(t1, t2) << " sec" << std::endl;

      std::cout << "Writing output .. " << std::endl;
      gettimeofday(&t1, NULL);
      img.write_file(opts.outmsh);
      gettimeofday(&t2, NULL);
      std::cout << "Done in " << (float)timediff_sec(t1, t2) << " sec" << std::endl;
      break;
    }

    case ITK_PADDING:
    {
      itk_image img;

      mt_vector<std::string> list;
      split_string(opts.size, ',', list);

      if (list.size() == 3)
      {
        char * endptr;
        mt_triple<unsigned int> size;
        size.v1 = strtoul(list[0].c_str(), &endptr, 10);
        size.v2 = strtoul(list[1].c_str(), &endptr, 10);
        size.v3 = strtoul(list[2].c_str(), &endptr, 10);

        std::cout << "Reading image: " << opts.msh << std::endl;
        gettimeofday(&t1, NULL);
        img.read_file(opts.msh.c_str());
        gettimeofday(&t2, NULL);
        std::cout << "Done in " << (float)timediff_sec(t1, t2) << " sec" << std::endl;

        std::cout << "Add padding to voxel data .. " << std::endl;
        gettimeofday(&t1, NULL);
        img.padding(size);
        gettimeofday(&t2, NULL);
        std::cout << "Done in " << (float)timediff_sec(t1, t2) << " sec" << std::endl;

        std::cout << "Writing output .. " << std::endl;
        gettimeofday(&t1, NULL);
        img.write_file(opts.outmsh);
        gettimeofday(&t2, NULL);
        std::cout << "Done in " << (float)timediff_sec(t1, t2) << " sec" << std::endl;
      }
      else
        std::cerr << "Error, check padding size format, ( 'i,j,k' expected )" << std::endl;

      break;
    }

    case ITK_INTERPOLATE:
    {
      itk_image img;
      mt_meshdata mesh;
      mt_vector<mt_real> clouddata, imgdata;
      mt_vector<mt_real> imgxyz;

      mt_real radius = 1.0;
      if(opts.radius.size() > 0)
      {
        radius = atof(opts.radius.c_str());
        if (radius < 1.0) radius = 1.0;
      }

      gettimeofday(&t1, NULL);
      std::cout << "Reading image: " << opts.msh << std::endl;
      img.read_file(opts.msh.c_str());

      const mt_real scale1 = 1.0/img.pixspc.v1;
      const mt_real scale2 = 1.0/img.pixspc.v2;
      const mt_real scale3 = 1.0/img.pixspc.v3;

      imgxyz.resize(3*img.npix);
      size_t cnt = 0;

      for (size_t k=0; k<img.dim.v3; k++) {
        for (size_t j=0; j<img.dim.v2; j++) {
          for (size_t i=0; i<img.dim.v1; i++) {
            imgxyz[3*cnt+0] = (img.orig.v1 + i*img.pixspc.v1 + img.pixspc.v1*0.5)*scale1;
            imgxyz[3*cnt+1] = (img.orig.v2 + j*img.pixspc.v2 + img.pixspc.v2*0.5)*scale2;
            imgxyz[3*cnt+2] = (img.orig.v3 + k*img.pixspc.v3 + img.pixspc.v3*0.5)*scale3;
            cnt++;
          }
        }
      }
      gettimeofday(&t2, NULL);
      std::cout << "Done in " << (float)timediff_sec(t1, t2) << " sec" << std::endl;

      std::cout << "Reading cloud points: " << opts.pts << std::endl;
      readPoints_general(mesh.xyz, opts.pts.c_str());
      const size_t num_pnts = mesh.xyz.size() / 3;
      for (size_t i=0; i<num_pnts; i++)
      {
        mesh.xyz[3*i+0] *= scale1;
        mesh.xyz[3*i+1] *= scale2;
        mesh.xyz[3*i+2] *= scale3;
      }
      gettimeofday(&t2, NULL);
      std::cout << "Done in " << (float)timediff_sec(t1, t2) << " sec" << std::endl;

      std::cout << "Reading cloud data: " << opts.idat << std::endl;
      read_vector_ascii(clouddata, opts.idat.c_str(), true);

      gettimeofday(&t2, NULL);
      std::cout << "Done in " << (float)timediff_sec(t1, t2) << " sec" << std::endl;

      if (clouddata.size() != num_pnts) {
        std::cerr << "size of point data does not match point cloud size" << std::endl;
        return;
      }

      std::cout << "Compute interpolation .." << std::endl;
      gettimeofday(&t1, NULL);

      //const int dpn = 1;
#if 1
      kdtree tree(10);
      tree.build_vertex_tree(mesh.xyz);

      imgdata.resize(cnt);
      imgdata.zero();
      for (size_t i=0; i<cnt; i++) {
        const vec3r cntr(imgxyz[3*i+0], imgxyz[3*i+1], imgxyz[3*i+2]);
        mt_vector<mt_mixed_tuple<mt_real,int> > vtx;
        tree.vertices_in_sphere(cntr, radius, vtx);
        if (vtx.size() > 0) {
          for (size_t j=0; j<vtx.size(); j++)
            imgdata[i] += clouddata[vtx[j].v2];
          imgdata[i] *= 1.0/vtx.size();
        }
      }
#else
      PointCloudInterpolator cloud_interp;
      cloud_interp.set_pts(mesh.xyz);
      cloud_interp.setup_roi(1);
      if(shepard)
        cloud_interp.interpolate_shepard(imgxyz, clouddata, dpn, shepard_global, imgdata);
      else {
        cloud_interp.setup_rbf_coeffs(clouddata, dpn);
        cloud_interp.interpolate_data(imgxyz, imgdata);
      }
#endif

      //write_vector_ascii(imgdata, opts.outmsh + ".dat", dpn);
      // !!! imgxyz points are scaled !!!
      //writePoints(imgxyz, opts.outmsh + CARPTXT_PTS_EXT);
      gettimeofday(&t2, NULL);
      std::cout << "Done in " << (float)timediff_sec(t1, t2) << " sec" << std::endl;


      std::cout << "Write image stack .." << std::endl;
      gettimeofday(&t1, NULL);

      mt_vector<float> imgdata_converted(imgdata.size());
      for (size_t i=0; i<imgdata.size(); i++)
        imgdata_converted[i] = static_cast<float>(imgdata[i]);

      img.comptypeid = 9; // float
      img.compsz = itk_get_datatype_size(img.comptypeid);
      img.ncomp = 1;
      img.pixsz = img.ncomp*img.compsz;
      img.datasz = img.npix*img.pixsz;
      img.databuff.assign(img.datasz, (char*)imgdata_converted.data(), true);
      imgdata_converted.assign(0, nullptr, false);

      img.write_file(opts.outmsh);

      gettimeofday(&t2, NULL);
      std::cout << "Done in " << (float)timediff_sec(t1, t2) << " sec" << std::endl;
      break;
    }

    case ITK_CROP:
    {
      mt_triple<float> ref;
      read_refining_info(opts.ref, ref, {1.0, 1.0, 1.0});

      itk_image img;

      std::cout << "Reading image: " << opts.msh << std::endl;
      gettimeofday(&t1, NULL);
      img.read_file(opts.msh.c_str());
      gettimeofday(&t2, NULL);
      std::cout << "Done in " << (float)timediff_sec(t1, t2) << " sec" << std::endl;

      std::cout << "Crop voxel data .. " << std::endl;
      gettimeofday(&t1, NULL);
      img.crop();
      gettimeofday(&t2, NULL);
      std::cout << "Done in " << (float)timediff_sec(t1, t2) << " sec" << std::endl;

      std::cout << "Refine " << ref.v1 << "-" << ref.v2 << "-" << ref.v3 << " x image data .." << std::endl;
      gettimeofday(&t1, NULL);
      resample_image(img, ref);
      gettimeofday(&t2, NULL);
      std::cout << "Done in " << (float)timediff_sec(t1, t2) << " sec" << std::endl;

      std::cout << "Writing output .. " << std::endl;
      gettimeofday(&t1, NULL);
      img.write_file(opts.outmsh);
      gettimeofday(&t2, NULL);
      std::cout << "Done in " << (float)timediff_sec(t1, t2) << " sec" << std::endl;

      break;
    }

    case ITK_FLIP:
    {
      itk_image img;

      unsigned int axes = ITK_AXIS_0;
      for (unsigned int i = 0; i < opts.axes.length(); i++)
      {
        switch (opts.axes[i])
        {
          case 'x': axes |= ITK_AXIS_X; break;
          case 'y': axes |= ITK_AXIS_Y; break;
          case 'z': axes |= ITK_AXIS_Z; break;
          default : break;
        }
      }

      std::cout << "Reading image: " << opts.msh << std::endl;
      gettimeofday(&t1, NULL);
      img.read_file(opts.msh.c_str());
      gettimeofday(&t2, NULL);
      std::cout << "Done in " << (float)timediff_sec(t1, t2) << " sec" << std::endl;

      std::cout << "Flip voxel data .. " << std::endl;
      gettimeofday(&t1, NULL);
      img.flip(axes);
      gettimeofday(&t2, NULL);
      std::cout << "Done in " << (float)timediff_sec(t1, t2) << " sec" << std::endl;

      std::cout << "Writing output .. " << std::endl;
      gettimeofday(&t1, NULL);
      img.write_file(opts.outmsh);
      gettimeofday(&t2, NULL);
      std::cout << "Done in " << (float)timediff_sec(t1, t2) << " sec" << std::endl;

      break;
    }

    case ITK_DTYPE:
    {
      itk_image inp_img, out_img;
      int dtype = atoi(opts.dtype.c_str());

      if ((dtype > 0) && (dtype < 11))
      {
        std::cout << "Reading image: " << opts.msh << std::endl;
        gettimeofday(&t1, NULL);
        inp_img.read_file(opts.msh.c_str());
        gettimeofday(&t2, NULL);
        std::cout << "Done in " << (float)timediff_sec(t1, t2) << " sec" << std::endl;

        std::cout << "Converting image data .. " << std::endl;
        gettimeofday(&t1, NULL);
        out_img.assign(inp_img, itk_get_datatype_str_vtk(dtype));
        gettimeofday(&t2, NULL);
        std::cout << "Done in " << (float)timediff_sec(t1, t2) << " sec" << std::endl;

        std::cout << "Writing output .. " << std::endl;
        gettimeofday(&t1, NULL);
        out_img.write_file(opts.outmsh);
        gettimeofday(&t2, NULL);
        std::cout << "Done in " << (float)timediff_sec(t1, t2) << " sec" << std::endl;
      }
      else if (dtype == 11)
      {
        std::cout << "Reading image: " << opts.msh << std::endl;
        gettimeofday(&t1, NULL);
        inp_img.read_file(opts.msh.c_str());
        gettimeofday(&t2, NULL);
        std::cout << "Done in " << (float)timediff_sec(t1, t2) << " sec" << std::endl;

        std::cout << "Converting image data .. " << std::endl;
        gettimeofday(&t1, NULL);
        out_img.assign(inp_img, itk_get_datatype_str_vtk(1));
        gettimeofday(&t2, NULL);
        std::cout << "Done in " << (float)timediff_sec(t1, t2) << " sec" << std::endl;

        std::cout << "Writing output .. " << std::endl;
        gettimeofday(&t1, NULL);
        out_img.write_file_color_scalars(opts.outmsh.c_str());
        gettimeofday(&t2, NULL);
        std::cout << "Done in " << (float)timediff_sec(t1, t2) << " sec" << std::endl;

      }
      else
        std::cerr << "Error, invalid dtype " << dtype << std::endl;

      break;
    }

    case ITK_RESAMPLE:
    {
      mt_triple<float> ref, size;
      read_refining_info(opts.ref, ref, {1.0, 1.0, 1.0});
      const bool has_size = !opts.size.empty();

      itk_image img;

      std::cout << "Reading image: " << opts.msh << std::endl;
      gettimeofday(&t1, NULL);
      img.read_file(opts.msh.c_str());
      gettimeofday(&t2, NULL);
      std::cout << "Done in " << (float)timediff_sec(t1, t2) << " sec" << std::endl;

      if (has_size) {
        read_refining_info(opts.size, size, {-1.0, -1.0, -1.0});
        std::cout << "!!! Size given, overwriting refinement factors !!!" << std::endl;
        if (size.v1 > 0.0) ref.v1 = img.pixspc.v1/size.v1;
        if (size.v2 > 0.0) ref.v2 = img.pixspc.v2/size.v2;
        if (size.v3 > 0.0) ref.v3 = img.pixspc.v3/size.v3;
      }

      printf("Refining to (%g, %g, %g) of original resolution..\n", ref.v1, ref.v2, ref.v3);
      gettimeofday(&t1, NULL);
      resample_image(img, ref);
      gettimeofday(&t2, NULL);
      std::cout << "Done in " << (float)timediff_sec(t1, t2) << " sec" << std::endl;

      std::cout << "Writing output .. " << std::endl;
      gettimeofday(&t1, NULL);
      if(!img.write_file(opts.outmsh))
      {
        std::cerr << "Error occured on writing image file " << opts.outmsh << std::endl;
        return;
      }
      gettimeofday(&t2, NULL);
      std::cout << "Done in " << (float)timediff_sec(t1, t2) << " sec" << std::endl;

      break;
    }

    case ITK_SAMPLE:
    {

      // read surface into mesh and set up kdtree
      std::cout << "Reading surfaces " << opts.surf << " and setting up kdtrees .." << std::endl;
      gettimeofday(&t1, NULL);

      mt_vector<std::string> surf_files, tags_list;
      split_string(opts.surf, ',' , surf_files);

      bool tags_provided = false;
      if(opts.tags.size())
      {
        tags_provided = true;
        split_string(opts.tags, ',' , tags_list);
      }

      const int num_surfs = surf_files.size();
      const int num_tags  = tags_list.size();
      if ((tags_provided) && (num_surfs != num_tags))
      {
        std::cerr << "Number of tags does not match the number of surfaces!" << std::endl;
        return;
      }

      mt_vector<mt_meshdata> meshes(num_surfs);
      mt_vector<kdtree>      trees(num_surfs);
      const int min_tris_per_leaf = 10;

      for(int s=0; s<num_surfs; s++)
      {
        mt_filename surffile(surf_files[s], "");
        read_mesh_selected(meshes[s], surffile.format, surffile.base, CRP_READ_PTS | CRP_READ_ELEM);

        if(mesh_is_trisurf(meshes[s]) == false) {
          mt_meshdata surfmesh;
          compute_surface(meshes[s], surfmesh, true);
          surfmesh.xyz.swap(meshes[s].xyz); // we swap the coords into the surf mesh, to have a complete mesh when
          meshes[s].swap(surfmesh);         // we swap the volumetric mesh with the surface mesh
        }

        trees[s].items_per_leaf = min_tris_per_leaf;
        trees[s].build_tree(meshes[s]);
      }
      gettimeofday(&t2, NULL);
      std::cout << "Done in " << (float)timediff_sec(t1, t2) << " sec" << std::endl;

      // set up itk image space from params
      itk_image img;

      if(opts.msh.size()) {
        std::cout << "Reading image stack " << opts.msh << " .." << std::endl;
        gettimeofday(&t1, NULL);
        itk_image rimg;
        rimg.read_file(opts.msh.c_str());
        img.assign(rimg, "short");
        gettimeofday(&t2, NULL);
        std::cout << "Done in " << (float)timediff_sec(t1, t2) << " sec" << std::endl;
      }
      else if (opts.min.size() && opts.max.size()) {
        mt_triple<double> orig, max, spacing;
        std::cout << "Setting up empty image stack .." << std::endl;
        gettimeofday(&t1, NULL);
        mt_vector<std::string> minstr, maxstr;
        split_string(opts.min, ',', minstr);
        split_string(opts.max, ',', maxstr);

        assert(minstr.size() == 3);
        assert(maxstr.size() == 3);

        orig.v1 = atof(minstr[0].c_str());
        orig.v2 = atof(minstr[1].c_str());
        orig.v3 = atof(minstr[2].c_str());

        max.v1 = atof(maxstr[0].c_str());
        max.v2 = atof(maxstr[1].c_str());
        max.v3 = atof(maxstr[2].c_str());

        float size = 100.0f;
        if(opts.size.size())
          size = atof(opts.size.c_str());

        mt_triple<unsigned int> dim;
        dim.v1 = fabs((max.v1 - orig.v1) / size) + 0.5;
        dim.v2 = fabs((max.v2 - orig.v2) / size) + 0.5;
        dim.v3 = fabs((max.v3 - orig.v3) / size) + 0.5;

        spacing.v1 = fabs((max.v1 - orig.v1) / dim.v1);
        spacing.v2 = fabs((max.v2 - orig.v2) / dim.v2);
        spacing.v3 = fabs((max.v3 - orig.v3) / dim.v3);

        img.assign(dim, orig, spacing, 1, "short");
        gettimeofday(&t2, NULL);
        std::cout << "Done in " << (float)timediff_sec(t1, t2) << " sec" << std::endl;
      } else {
        std::cout << "Setting up empty image stack from unified bbox.." << std::endl;
        gettimeofday(&t1, NULL);

        mt_real x = meshes[0].xyz[0], y = meshes[0].xyz[1], z = meshes[0].xyz[2];
        vec3r min(x,y,z), max(x,y,z);

        for(size_t s = 0; s < meshes.size(); s++)
        {
          mt_meshdata & curmesh = meshes[s];
          size_t numpts = curmesh.xyz.size() / 3;

          for(size_t i=0; i<numpts; i++) {
            x = curmesh.xyz[i*3+0], y = curmesh.xyz[i*3+1], z = curmesh.xyz[i*3+2];
            if(min.x > x) min.x = x;
            if(min.y > y) min.y = y;
            if(min.z > z) min.z = z;
            if(max.x < x) max.x = x;
            if(max.y < y) max.y = y;
            if(max.z < z) max.z = z;
          }
        }

        mt_triple<double> orig, spacing;

        orig.v1 = min.x;
        orig.v2 = min.y;
        orig.v3 = min.z;

        float size = 100.0f;
        if(opts.size.size())
          size = atof(opts.size.c_str());

        mt_triple<unsigned int> dim;
        dim.v1 = fabs((max.x - orig.v1) / size) + 0.5;
        dim.v2 = fabs((max.y - orig.v2) / size) + 0.5;
        dim.v3 = fabs((max.z - orig.v3) / size) + 0.5;

        spacing.v1 = fabs((max.x - orig.v1) / dim.v1);
        spacing.v2 = fabs((max.y - orig.v2) / dim.v2);
        spacing.v3 = fabs((max.z - orig.v3) / dim.v3);

        img.assign(dim, orig, spacing, 1, "short");
        gettimeofday(&t2, NULL);
      }

      std::cout << "Sampling image stack .." << std::endl;
      gettimeofday(&t1, NULL);
      for(int s=0; s<num_surfs; s++)
      {
        mt_tag_t tag = 0;
        if ((tags_provided) && (tags_list[s] != "_"))
          tag = static_cast<mt_tag_t>(atoi(tags_list[s].c_str()));
        itk_sample(img, meshes[s], trees[s], tag);
      }
      gettimeofday(&t2, NULL);
      std::cout << "Done in " << (float)timediff_sec(t1, t2) << " sec" << std::endl;

      std::cout << "Writing image stack " << opts.outmsh << " .." << std::endl;
      img.write_file(opts.outmsh);

      break;
    }

    case ITK_EXTRACT:
    {
      itk_image img;

      unsigned int plane = ITK_AXIS_0;
      if (opts.axes == "xy") plane = ITK_AXIS_X | ITK_AXIS_Y;
      else if (opts.axes == "xz") plane = ITK_AXIS_X | ITK_AXIS_Z;
      else if (opts.axes == "yz") plane = ITK_AXIS_Y | ITK_AXIS_Z;

      if (plane == ITK_AXIS_0)
      {
        std::cerr << "Error, invalid plane " << opts.axes << " !" << std::endl;
        return;
      }

      mt_vector<std::string> str_idx;
      split_string(opts.idx, ',' , str_idx);
      std::set<unsigned int> idx;
      for (size_t i = 0; i < str_idx.size(); i++)
        idx.insert(std::stoul(str_idx[i]));

      std::cout << "Reading image: " << opts.msh << std::endl;
      gettimeofday(&t1, NULL);
      img.read_file(opts.msh.c_str());
      gettimeofday(&t2, NULL);
      std::cout << "Done in " << (float)timediff_sec(t1, t2) << " sec" << std::endl;

      std::cout << "Extract voxel data .. " << std::endl;
      gettimeofday(&t1, NULL);
      img.extract_slices(plane, idx);
      gettimeofday(&t2, NULL);
      std::cout << "Done in " << (float)timediff_sec(t1, t2) << " sec" << std::endl;

      std::cout << "Writing output .. " << std::endl;
      gettimeofday(&t1, NULL);
      img.write_file(opts.outmsh);
      gettimeofday(&t2, NULL);
      std::cout << "Done in " << (float)timediff_sec(t1, t2) << " sec" << std::endl;

      break;
    }

    case ITK_TRANSFORM:
    {
      itk_image img;

      std::cout << "Reading image: " << opts.msh << std::endl;
      gettimeofday(&t1, NULL);
      const bool success = img.read_file(opts.msh.c_str());
      gettimeofday(&t2, NULL);
      std::cout << "Done in " << (float)timediff_sec(t1, t2) << " sec" << std::endl;

      if (success) {
        std::cout << "Apply transformation .. " << std::endl;
        gettimeofday(&t1, NULL);

        mt_vector<std::string> str_comp; // translation components
        str_comp.resize(0);
        mt_real transl_x(0.0), transl_y(0.0), transl_z(0.0);
        if (!opts.transl.empty()) {
          split_string(opts.transl, ',', str_comp);
          check_size(str_comp, 3, "transform_mode");
          transl_x = atof(str_comp[0].c_str());
          transl_y = atof(str_comp[1].c_str());
          transl_z = atof(str_comp[2].c_str());
        }
        str_comp.resize(0);
        mt_real scale_x(1.0), scale_y(1.0), scale_z(1.0);
        if (!opts.scale.empty()) {
          split_string(opts.scale, ',', str_comp);
          if (str_comp.size() == 1) {
            scale_x = scale_y = scale_z = atof(str_comp[0].c_str());
          }
          else if (str_comp.size() == 3) {
            scale_x = atof(str_comp[0].c_str());
            scale_y = atof(str_comp[1].c_str());
            scale_z = atof(str_comp[2].c_str());
          }
          else {
            fputs("transform_mode error: vector not of size 1 or 3! Aborting!\n", stderr);
            exit(1);
          }
        }

        img.pixspc.v1 *= scale_x;
        img.pixspc.v2 *= scale_y;
        img.pixspc.v3 *= scale_z;
        img.orig.v1 *= scale_x;
        img.orig.v2 *= scale_y;
        img.orig.v3 *= scale_z;
        img.orig.v1 += transl_x;
        img.orig.v2 += transl_y;
        img.orig.v3 += transl_z;

        gettimeofday(&t2, NULL);
        std::cout << "Done in " << (float)timediff_sec(t1, t2) << " sec" << std::endl;

        std::cout << "Writing output .. " << std::endl;
        gettimeofday(&t1, NULL);
        img.write_file(opts.outmsh);
        gettimeofday(&t2, NULL);
        std::cout << "Done in " << (float)timediff_sec(t1, t2) << " sec" << std::endl;
      }
      else
        std::cerr << "Error, failed to load image from file '" << opts.msh << "' !" << std::endl;

      break;
    }

    case ITK_MESH:
    {
      bool success;
      mt_filename omsh_name(opts.outmsh, opts.out_fmt);
      itk_image img;

      std::cout << "Reading image: " << opts.msh << std::endl;
      gettimeofday(&t1, NULL);
      success = img.read_file(opts.msh.c_str());
      gettimeofday(&t2, NULL);
      std::cout << "Done in " << (float)timediff_sec(t1, t2) << " sec" << std::endl;

      if (success) {

        mt_triple<MT_USET<mt_idx_t>> slices;
        const bool has_slices = get_slices(opts.slice, slices);

        mt_meshdata mesh;
        unsigned int comp = 0;
        const mt_real scale = (opts.scale.empty()) ? 1.0 : std::stof(opts.scale);

        if ((img.comptypeid == 9) || (img.comptypeid == 10) || (opts.is_scan))
        {
          if (opts.tags.size())
            std::cout << std::endl << "ATTENTION: Tags ignored for scans!"
                      << std::endl << std::endl;
          if (opts.surf_only)
            std::cout << std::endl << "ATTENTION: Surface extraction not working with scans!"
                      << std::endl << std::endl;
          
          mt_vector<mt_real> values;
          success = itk_mesh_mode(img, mesh, values, has_slices, comp, scale, slices);

          if (success) {
            std::cout << "Writing output .. " << std::endl;
            gettimeofday(&t1, NULL);
            write_mesh_selected(mesh, omsh_name.format, omsh_name.base);
            write_vector_ascii(values, omsh_name.base+".dat");
            gettimeofday(&t2, NULL);
            std::cout << "Done in " << (float)timediff_sec(t1, t2) << " sec" << std::endl;
          }
          else
            std::cerr << "Error, failed to convert image to mesh !" << std::endl;
        }
        else
        {
          if ((has_slices) && (opts.surf_only))
            std::cout << std::endl << "ATTENTION: Surface extraction not working with slices!"
                      << std::endl << std::endl;

          MT_USET<mt_tag_t> tags_set;
          if (opts.tags.size()) {
            mt_vector<std::string> tags_list;
            split_string(opts.tags, ',' , tags_list);
            for (size_t i=0; i<tags_list.size(); i++)
              tags_set.insert(static_cast<mt_tag_t>(std::stoi(tags_list[i])));
          } else {
            double min = 0.0, max = 0.0;
            MT_MAP<mt_idx_t, size_t> label_counts;
            itk_region_info(img, min, max, label_counts);

            for(auto & it : label_counts)
              if(it.first != 0)
                tags_set.insert(it.first);
          }

          const bool has_tags = (tags_set.size() > 0);

          std::cout << "Converting to Hex-mesh ..." << std::endl;
          gettimeofday(&t1, NULL);
          if (has_slices)
            success = img.to_hex_mesh(mesh, slices, tags_set, comp, scale);
          else {
            if (has_tags) {
              if (opts.surf_only)
                success = img.to_hex_surf(mesh, tags_set, comp, scale);
              else
                success = img.to_hex_mesh(mesh, tags_set, comp, scale);
            }
            else {
              if (opts.surf_only)
                success = img.to_hex_surf(mesh, tags_set, comp, scale);
              else
                success = img.to_hex_mesh(mesh, comp, scale);
            }
          }

          gettimeofday(&t2, NULL);
          std::cout << "Done in " << (float)timediff_sec(t1, t2) << " sec" << std::endl;

          if (success) {
            std::cout << "Writing output .. " << std::endl;
            gettimeofday(&t1, NULL);
            write_mesh_selected(mesh, omsh_name.format, omsh_name.base);
            gettimeofday(&t2, NULL);
            std::cout << "Done in " << (float)timediff_sec(t1, t2) << " sec" << std::endl;
          }
          else
            std::cerr << "Error, failed to convert image to mesh !" << std::endl;
        }
      }
      else
        std::cerr << "Error, failed to load image from file '" << opts.msh << "' !" << std::endl;

      break;
    }

    case ITK_EXTRUDE:
    {
      int extrude_mode = 0;
      if (opts.mode.size()) {
        if (opts.mode == "in") extrude_mode = 0;
        else if (opts.mode == "out") extrude_mode = 1;
        else if (opts.mode == "both") extrude_mode = 2;
        else extrude_mode = -1;
      }

      short rad = 1;
      if (opts.radius.size())
        rad = static_cast<short>(std::stoi(opts.radius));

      if ((extrude_mode >= 0) && (extrude_mode <= 2) && (rad > 0))
      {

        itk_image img;

        std::cout << "Reading image: " << opts.msh << std::endl;
        gettimeofday(&t1, NULL);
        img.read_file(opts.msh.c_str());
        gettimeofday(&t2, NULL);
        std::cout << "Done in " << (float)timediff_sec(t1, t2) << " sec" << std::endl;

        const int reg_tag = std::stoi(opts.regtag);

        int new_tag = reg_tag;
        if (opts.newtag.size())
          new_tag = std::stoi(opts.newtag);
        if (new_tag < 0)
          new_tag = reg_tag;

        MT_USET<int> bdry_tags;
        if(opts.tags.size()) {
          mt_vector<std::string> tags_list;
          split_string(opts.tags, ',' , tags_list);
          for (size_t i=0; i<tags_list.size(); i++)
            bdry_tags.insert(std::stoi(tags_list[i]));
        }
        else
          bdry_tags.insert(0);

        itk_extrude_mode(extrude_mode, img, rad, reg_tag, new_tag, bdry_tags);


        std::cout << "Writing output .. " << std::endl;
        gettimeofday(&t1, NULL);
        img.write_file(opts.outmsh);
        gettimeofday(&t2, NULL);
        std::cout << "Done in " << (float)timediff_sec(t1, t2) << " sec" << std::endl;
      }
      else
        std::cerr << "Error, unknown extrude mode '" << opts.mode << "' or invalid radius!" << std::endl;

      break;
    }

    case ITK_CONVERT:
    {
      itk_image img;

      std::cout << "Reading image: " << opts.msh << std::endl;
      gettimeofday(&t1, NULL);
      img.read_file(opts.msh.c_str());
      gettimeofday(&t2, NULL);
      std::cout << "Done in " << (float)timediff_sec(t1, t2) << " sec" << std::endl;

      std::cout << "Writing output .. " << std::endl;
      gettimeofday(&t1, NULL);
      img.write_file(opts.outmsh);
      gettimeofday(&t2, NULL);
      std::cout << "Done in " << (float)timediff_sec(t1, t2) << " sec" << std::endl;

      break;
    }
  }
}
