#include "data_structs.h"
#include "mt_modes_base.h"

#ifndef MT_QUERY_MODE_H_
#define MT_QUERY_MODE_H_

enum QRY_MODE {
  QRY_IDX,
  QRY_IDXLIST,
  QRY_VALS,
  QRY_TAGS,
  QRY_BBOX,
  QRY_EDGE,
  QRY_GRPH,
  QRY_QUAL,
  QRY_RESQUAL,
  QRY_SMTH,
  QRY_CURV,
  QRY_INSIDE,
  QRY_NRMLS,
  QRY_IDXLIST_UVC,
  QRY_IDXLIST_UCC,
  QRY_UVCOORDS_XYZ,
  QRY_UCCOORDS_XYZ,
  QRY_ELEM_IDX,
  QRY_DISTANCE,
  QRY_NON_MANIFOLD
};

struct query_options {
  enum QRY_MODE type;
  mt_filename msh;
  std::string coord;
  std::string uvc;
  std::string ucc;
  std::string thr;
  std::string ifmt;
  std::string surf;
  std::string tags;
  std::string rad;
  std::string idat;
  std::string odat;
  std::string mode;
  std::string vtx;
  std::string iter;
  bool extra_vtx = false;
};

struct query_curvature_options {
  mt_real rad;
};

void print_mesh_stats(const mt_meshdata &mesh);

// query curvature mode
void query_curvature_mode_read(const query_options &opts, mt_meshdata &mesh, mt_meshdata &surfmesh,
                               query_curvature_options &mode_opts);

void query_curvature_mode(mt_meshdata &mesh, mt_meshdata &surfmesh,
                          mt_vector<mt_real> &curv,
                          const query_curvature_options &mode_opts);

// query idxlist mode
void query_idxlist_mode_read(const query_options &opts,
                             mt_vector<mt_real> &coords,
                             mt_vector<mt_real> &thr, mt_vector<mt_idx_t> &nod,
                             mt_vector<mt_real> &xyz);

void query_idxlist_mode(const mt_vector<mt_real> &coords,
                        const mt_vector<mt_real> &thr,
                        const mt_vector<mt_idx_t> &nod,
                        const mt_vector<mt_real> &xyz,
                        mt_vector<mt_vector<mt_idx_t>> &vtxlist // out
);

// query tags mode

void query_tags_mode_read(const struct query_options &opts, mt_meshdata &mesh,
                          mt_vector<mt_idx_t> &vtx);

/**
 * @brief query tags of mesh
 * 
 * @param mesh[in] mesh to query tags of 
 * @param vtx[in]  vertices of nodes to query tags of. if empty, all 
 * @param myo_tags[out]  tags of myocardium
 * @param bath_tags[out] tags of bath
 * @param fib_tol[in]   tolerance whether to consider tags as myocardium or bath
 */

void query_tags_mode(mt_meshdata &mesh, const mt_vector<mt_idx_t> &vtx,
                     MT_MAP<mt_tag_t, size_t>& myo_tags, MT_MAP<mt_tag_t, size_t>& bath_tags,
                     mt_real fib_tol=0.1);

// query quality mode
struct query_quality_options {
  int mode = 0;
  bool comp_thr = false;
  float thr;
};
void query_quality_mode_read(const query_options &opts,
                             query_quality_options &mode_opts,
                             mt_meshdata &mesh);

void query_quality_mode(const mt_meshdata &mesh,
                        const query_quality_options &mode_opts,
                        mt_vector<mt_real> &elemqual);

/**
 * @brief get all edges of a mesh 
 * 
 * @param mesh[in]  meshes to calculate edges of
 * @param edge_set[out]  set of edges
 * @param edge_len[out]  length of edges
 */
void query_edge_mode(const mt_meshdata &mesh, MT_USET<mt_tuple<mt_idx_t>> &edge_set, std::vector<mt_real> &edge_len);

struct mt_bbox {
  vec3r min;
  vec3r max;
};


/**
 * @brief returns bound box of mesh.
 * 
 * @param mesh[in]  mesh to compute bbox for 
 * @param bbox[out] bbox of mesh
 */
void query_bbox_mode(const mt_meshdata &mesh, mt_bbox &bbox);


mt_real query_idx_mode(const mt_meshdata &mesh, const mt_vector<mt_idx_t> &nod, 
                       const vec3r &coords, mt_real thr, 
                       mt_vector<mt_idx_t> &vtxlist);


/**
 * @brief 
 * 
 * @param coords 
 * @param coord_is_lv 
 * @param thr 
 * @param eps 
 * @param xyz_cart 
 * @param UVCs 
 * @param is_LV 
 * @param vtxlist 
 */
void query_idxlist_uvc_mode(const mt_vector<mt_real> &coords, const mt_vector<bool> &coord_is_lv, const mt_vector<mt_real> &thr, const mt_vector<mt_real> &eps, const mt_vector<mt_real> &xyz_cart, const mt_vector<mt_real> &UVCs, const mt_mask &is_LV, mt_vector<mt_vector<mt_idx_t>> &vtxlist);


#endif