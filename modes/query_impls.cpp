#include <cstdlib>

#include "data_structs.h"
#include "mt_vector.h"
#include "query_mode.h"

void query_bbox_mode(const mt_meshdata &mesh, mt_bbox &bbox) {
  mt_real xmin = mesh.xyz[0], xmax = mesh.xyz[0], ymin = mesh.xyz[1], ymax = mesh.xyz[1], zmin = mesh.xyz[2],
          zmax = mesh.xyz[2];

  for (size_t i = 0; i < mesh.xyz.size() / 3; i++) {
    mt_real x = mesh.xyz[i * 3 + 0];
    mt_real y = mesh.xyz[i * 3 + 1];
    mt_real z = mesh.xyz[i * 3 + 2];

    if (xmin > x) xmin = x;
    if (ymin > y) ymin = y;
    if (zmin > z) zmin = z;

    if (xmax < x) xmax = x;
    if (ymax < y) ymax = y;
    if (zmax < z) zmax = z;
  }
  bbox.min.assign(xmin, ymin, zmin);
  bbox.max.assign(xmax, ymax, zmax);
}

void query_quality_mode_read(const query_options &opts, query_quality_options &mode_opts, mt_meshdata &mesh) {
  struct timeval t1, t2;
  std::cout << "Reading mesh: " << opts.msh.base << std::endl;
  gettimeofday(&t1, NULL);

  read_mesh_selected(mesh, opts.msh.format, opts.msh.base, CRP_READ_ELEM | CRP_READ_PTS);
  mesh.e2n_dsp.resize(mesh.e2n_cnt.size());
  bucket_sort_offset(mesh.e2n_cnt, mesh.e2n_dsp);

  gettimeofday(&t2, NULL);
  std::cout << "Done in " << (float)timediff_sec(t1, t2) << " sec" << std::endl;

  if (opts.mode.size()) mode_opts.mode = atoi(opts.mode.c_str());

  if (opts.thr.size() > 0) {
    mode_opts.thr = atof(opts.thr.c_str());
    mode_opts.comp_thr = true;
  }
}

void query_quality_mode(const mt_meshdata &mesh, const query_quality_options &mode_opts,
                        mt_vector<mt_real> &elemqual  // out
) {
  struct timeval t1, t2;

  if (mode_opts.mode == 1) {
    std::cout << "Checking self intersection .." << std::endl;
    gettimeofday(&t1, NULL);

    mt_vector<mt_tuple<mt_idx_t>> intersec_edges;
    mt_vector<mt_idx_t> intersec_eidx;

    check_self_intersection(mesh, intersec_edges, intersec_eidx);

    if (intersec_edges.size() > 0) {
      mt_meshdata intersec;

      for (size_t i = 0; i < intersec_edges.size(); i++) {
        const mt_tuple<mt_idx_t> &edge = intersec_edges[i];
        const mt_idx_t &eidx = intersec_eidx[i];

        mt_idx_t lncon[2] = {edge.v1, edge.v2};
        mt_idx_t curtag = intersec.e2n_cnt.size();

        mesh_add_elem(intersec, Line, lncon, curtag);
        mesh_add_elem(intersec, mesh.etype[eidx], mesh.e2n_con.data() + mesh.e2n_dsp[eidx], curtag);
      }

      intersec.xyz = mesh.xyz;

      mt_vector<mt_idx_t> nod;
      reindex_nodes(intersec, nod);

      const char *intersec_basename = "intersec.mesh";
      fprintf(stderr,
              "Self intersections detected. Outputting mesh intersections into "
              "mesh: %s.vtk \n",
              intersec_basename);
      write_mesh_selected(intersec, "vtk_bin", intersec_basename);
    }

    gettimeofday(&t2, NULL);
    std::cout << "Done in " << (float)timediff_sec(t1, t2) << " sec" << std::endl;
  }

  std::cout << "Computing mesh quality .." << std::endl;
  gettimeofday(&t1, NULL);
  mesh_quality(mesh, elemqual);

  gettimeofday(&t2, NULL);
  std::cout << "Done in " << (float)timediff_sec(t1, t2) << " sec" << std::endl;

  mt_real min = elemqual[0], max = 0;
  mt_real avrg = 0.0, stddev = 0.0;

  int above_thr = 0;

  // get basic min-max
  for (size_t i = 0; i < elemqual.size(); i++) {
    mt_real c = elemqual[i];
    if (min > c) min = c;
    if (max < c) max = c;
    if (mode_opts.comp_thr && (c > mode_opts.thr)) above_thr++;
    avrg += c;
  }
  avrg /= (double)elemqual.size();

  for (size_t i = 0; i < elemqual.size(); i++) {
    mt_real c = elemqual[i];
    stddev += (c - avrg) * (c - avrg);
  }
  stddev /= ((double)(elemqual.size()) - 1.0);
  stddev = std::sqrt(stddev);

  // build up histogramm
  size_t numint = 50;
  float delta = 1.0f / numint;
  std::vector<float> xval(numint + 1), yval(numint + 1, 0);

  asciiPlotter plotter(22, 90);

  for (size_t i = 0; i < numint + 1; i++) xval[i] = i * delta;
  for (size_t i = 0; i < elemqual.size(); i++) {
    size_t idx = elemqual[i] / delta;
    if (idx < numint + 1) yval[idx] += 1.0f;
  }
  for (size_t i = 0; i < numint + 1; i++) yval[i] /= (float)elemqual.size();

  plotter.set_xrange(0.0, 1.0);
  plotter.set_yrange(yval);
  plotter.add_graph(xval, yval, '*');
  std::cout << std::endl << "--------- element quality statistics ---------" << std::endl;
  std::cout << "Used method: " << QMETRIC_NAME << " with 0 == best, 1 == worst" << std::endl;
  std::cout << "Min: " << min << " Max: " << max << " Mean: " << avrg << " Stddev: " << stddev << std::endl;
  if (mode_opts.comp_thr)
    std::cout << "Number of elements with quality above " << mode_opts.thr << " : " << above_thr << std::endl;

  std::cout << std::endl << "Histogramm of element quality:" << std::endl << std::endl;
  plotter.print();
}

void query_tags_mode_read(const struct query_options &opts, mt_meshdata &mesh, mt_vector<mt_idx_t> &vtx) {
  struct timeval t1, t2;

  std::cout << "Reading mesh: " << opts.msh.base << std::endl;
  gettimeofday(&t1, NULL);
  read_mesh_selected(mesh, opts.msh.format, opts.msh.base, CRP_READ_ELEM | CRP_READ_LON);
  print_mesh_stats(mesh);

  if (opts.vtx.size()) read_vtx(vtx, opts.vtx);

  gettimeofday(&t2, NULL);
  std::cout << "Done in " << (float)timediff_sec(t1, t2) << " sec" << std::endl;
  if (vtx.size()) std::cout << "querying tags only for elems connected to vertices " << opts.vtx << std::endl;
}

void query_tags_mode(mt_meshdata &mesh, const mt_vector<mt_idx_t> &vtx, MT_MAP<mt_tag_t, size_t> &myo_tags,
                     MT_MAP<mt_tag_t, size_t> &bath_tags, mt_real fib_tol) {
  fib_tol = std::fabs(fib_tol);

  // compute and print all unique tags
  short numFib = mesh.lon.size() == mesh.e2n_cnt.size() * 6 ? 6 : 3;

  if (vtx.size()) {
    bucket_sort_offset(mesh.e2n_cnt, mesh.e2n_dsp);
    transpose_connectivity_par(mesh.e2n_cnt, mesh.e2n_dsp, mesh.e2n_con, mesh.n2e_cnt, mesh.n2e_dsp, mesh.n2e_con,
                               false);

    for (mt_idx_t nidx : vtx) {
      mt_idx_t start = mesh.n2e_dsp[nidx], stop = start + mesh.n2e_cnt[nidx];

      for (mt_idx_t j = start; j < stop; j++) {
        mt_idx_t eidx = mesh.n2e_con[j];

        vec3r l(mesh.lon.data() + eidx * numFib);
        mt_idx_t tag = mesh.etags[eidx];

        if (l.length2() > 0.1)
          myo_tags[tag] = myo_tags[tag] + 1;
        else
          bath_tags[tag] = bath_tags[tag] + 1;
      }
    }
  } else {
    for (size_t i = 0; i < mesh.etags.size(); i++) {
      vec3r l(mesh.lon.data() + i * numFib);
      mt_idx_t tag = mesh.etags[i];

      if (l.length2() > 0.1)
        myo_tags[tag] = myo_tags[tag] + 1;
      else
        bath_tags[tag] = bath_tags[tag] + 1;
    }
  }

  myo_tags.sort();
  bath_tags.sort();
}

/// read coordinates and thresholds
template <class S>
void read_coord_file(mt_vector<S> &coords, mt_vector<S> &thr, std::string file, const float def_thr = 0.0) {
  FILE *fd = fopen(file.c_str(), MT_FOPEN_READ);
  if (!fd) {
    treat_file_open_error(file);
    exit(1);
  }

  char line[512];
  fgets(line, sizeof(line), fd);

  int numcoord;
  sscanf(line, "%d", &numcoord);
  coords.resize(numcoord * 3);
  thr.resize(numcoord);

  float v[4];
  for (int i = 0; i < numcoord; i++) {
    fgets(line, sizeof(line), fd);
    const int nread = sscanf(line, "%f %f %f %f", v, v + 1, v + 2, v + 3);
    if (nread == 3) {
      coords[i * 3 + 0] = v[0];
      coords[i * 3 + 1] = v[1];
      coords[i * 3 + 2] = v[2];
      thr[i] = def_thr;
    } else if (nread == 4) {
      coords[i * 3 + 0] = v[0];
      coords[i * 3 + 1] = v[1];
      coords[i * 3 + 2] = v[2];
      thr[i] = v[3];
    } else {
      coords[i * 3 + 0] = 0.0;
      coords[i * 3 + 1] = 0.0;
      coords[i * 3 + 2] = 0.0;
      thr[i] = def_thr;
    }
  }
  fclose(fd);
}

mt_real query_idx_mode(const mt_meshdata &mesh, const mt_vector<mt_idx_t> &nod, 
                       const vec3r &coord, mt_real rad,
                       mt_vector<mt_idx_t> &vtxlist) {
  vtxlist.clear();
  mt_real mindist = -1;
  if (rad > 0) {
    // here we print a list of vertices that are below "thr" away from the given coordinate
    // printf("Searching for vertices in a %.2f radius to %.2f,%.2f,%.2f \n", rad, coords.x,coords.y,coords.z);

    rad *= rad;

#if 1
    // gettimeofday(&t1, NULL);
    for (const mt_idx_t &n : nod) {
      mt_real k1 = coord.x - mesh.xyz[n * 3 + 0];
      mt_real k2 = coord.y - mesh.xyz[n * 3 + 1];
      mt_real k3 = coord.z - mesh.xyz[n * 3 + 2];

      mt_real dist = (k1 * k1 + k2 * k2 + k3 * k3);

      if (dist < rad) {
        vtxlist.push_back(n);
        if (dist < mindist || mindist < 0) {
          mindist = dist;
        }
      }
    }
    // gettimeofday(&t2, NULL);
    // std::cout << "Done in " << (float)timediff_sec(t1, t2) << " sec" << std::endl;

#else

#endif
  } else {
    // here we print a single vertex, which is closest to the given coordinate
    // printf("Searching for vertex closest to %.2f,%.2f,%.2f \n", c1,c2,c3);

    mt_real k1 = coord.x - mesh.xyz[0];
    mt_real k2 = coord.y - mesh.xyz[1];
    mt_real k3 = coord.z - mesh.xyz[2];
    mt_idx_t minIdx = 0;
    mindist = (k1 * k1 + k2 * k2 + k3 * k3);

    for (const mt_idx_t &n : nod) {
      k1 = coord.x - mesh.xyz[n * 3 + 0];
      k2 = coord.y - mesh.xyz[n * 3 + 1];
      k3 = coord.z - mesh.xyz[n * 3 + 2];

      mt_real dist = (k1 * k1 + k2 * k2 + k3 * k3);

      if (mindist > dist) {
        mindist = dist;
        minIdx = n;
      }
    }
    vtxlist.push_back(minIdx);
  }
  return mindist;
}

void query_idxlist_mode_read(const query_options &opts, mt_vector<mt_real> &coords, mt_vector<mt_real> &thr,
                             mt_vector<mt_idx_t> &nod, mt_vector<mt_real> &xyz

) {
  struct timeval t1, t2;

  mt_meshdata mesh;

  // query index list
  // ------------------------------------------------------------
  std::cout << "Reading points of mesh: " << opts.msh.base << std::endl;
  short read_mask = CRP_READ_PTS;
  if (opts.tags.size() > 0) read_mask |= CRP_READ_ELEM;
  gettimeofday(&t1, NULL);
  read_mesh_selected(mesh, opts.msh.format, opts.msh.base, read_mask);
  gettimeofday(&t2, NULL);
  std::cout << "Done in " << (float)timediff_sec(t1, t2) << " sec" << std::endl;

  mt_meshdata surfmesh;

  if (opts.surf.size() > 0) {
    std::cout << "Restricting index search to surface: " << opts.surf + SURF_EXT << std::endl;
    readElements(surfmesh, opts.surf + SURF_EXT);
    nod = surfmesh.e2n_con;
    binary_sort(nod);
    unique_resize(nod);
    nod.shrink_to_fit();

    xyz.resize(3 * nod.size());
    for (size_t i = 0; i < nod.size(); i++) {
      xyz[3 * i + 0] = mesh.xyz[3 * nod[i] + 0];
      xyz[3 * i + 1] = mesh.xyz[3 * nod[i] + 1];
      xyz[3 * i + 2] = mesh.xyz[3 * nod[i] + 2];
    }
  } else if (opts.tags.size() > 0) {
    mt_vector<std::string> tags_str;
    split_string(opts.tags, ',', tags_str);

    MT_USET<mt_idx_t> selected_tags;
    for (size_t j = 0; j < tags_str.size(); j++) {
      int t = atoi(tags_str[j].c_str());
      selected_tags.insert(t);
    }

    bucket_sort_offset(mesh.e2n_cnt, mesh.e2n_dsp);
    mt_vector<mt_idx_t> m2s_map(mesh.xyz.size() / 3, -1);
    for (size_t i = 0; i < mesh.etags.size(); i++) {
      if (selected_tags.count(mesh.etags[i]) != 0) {
        const mt_idx_t dsp = mesh.e2n_dsp[i];
        const mt_cnt_t cnt = mesh.e2n_cnt[i];
        for (mt_cnt_t j = 0; j < cnt; j++) {
          const mt_idx_t nidx = mesh.e2n_con[dsp + j];
          if (m2s_map[nidx] == -1) {
            xyz.push_back(mesh.xyz[3 * nidx + 0]);
            xyz.push_back(mesh.xyz[3 * nidx + 1]);
            xyz.push_back(mesh.xyz[3 * nidx + 2]);
            m2s_map[nidx] = static_cast<mt_idx_t>(nod.size());
            nod.push_back(nidx);
          }
        }
      }
    }

    if (!(nod.size() > 0)) {
      std::cerr << "Error: No points in tag regions found!" << std::endl;
      exit(EXIT_FAILURE);
    }
  } else {
    nod.resize(mesh.xyz.size() / 3);
    for (size_t i = 0; i < nod.size(); i++) nod[i] = i;
    xyz.swap(mesh.xyz);
  }

  float def_thr = 0.0;
  if (opts.thr.size() > 0) def_thr = atof(opts.thr.c_str());

  std::cout << "Reading coord file " << opts.coord << std::endl;
  read_coord_file(coords, thr, opts.coord, def_thr);
}

void query_idxlist_mode(const mt_vector<mt_real> &coords, const mt_vector<mt_real> &thr, const mt_vector<mt_idx_t> &nod,
                        const mt_vector<mt_real> &xyz,
                        mt_vector<mt_vector<mt_idx_t>> &vtxlist  // out
) {
  struct timeval t1, t2;

  size_t numcoord = thr.size();

  vtxlist.resize(numcoord);

  std::cout << "Searching for vertices .." << std::endl;
  gettimeofday(&t1, NULL);

  kdtree tree(10);
  tree.build_vertex_tree(xyz);

  for (size_t i = 0; i < numcoord; i++) {
    if (thr[i] > 0.0) {
      mt_vector<mt_mixed_tuple<mt_real, int>> vertices;
      mt_real rad = thr[i];
      vec3r ctr(coords.data() + i * 3);
      tree.vertices_in_sphere(ctr, rad, vertices);

      // we sort the vertices by distance, to give more value to the output
      std::sort(vertices.begin(), vertices.end());

      for (auto &v : vertices) vtxlist[i].push_back(nod[v.v2]);
    } else {
      int cls_idx;
      vec3r cls_pos;
      mt_real dist;

      vec3f ref(coords.data() + i * 3);
      tree.closest_vertex(ref, cls_idx, cls_pos, dist);
      vtxlist[i].push_back(nod[cls_idx]);
    }
  }

  gettimeofday(&t2, NULL);
  std::cout << "Done in " << (float)timediff_sec(t1, t2) << " sec" << std::endl;
}

void query_idxlist_uvc_mode(const mt_vector<mt_real> &coords, const mt_vector<bool> &coord_is_lv,
                            const mt_vector<mt_real> &thr, const mt_vector<mt_real> &eps,
                            const mt_vector<mt_real> &xyz_cart, const mt_vector<mt_real> &UVCs, const mt_mask &is_LV,
                            mt_vector<mt_vector<mt_idx_t>> &vtxlist) {
  kdtree tree_cart(10);
  mt_vector<vec3r> ptsvec_cart;
  array_to_points(xyz_cart, ptsvec_cart);
  tree_cart.build_vertex_tree(ptsvec_cart);

  // split UVCs of mesh into two sets to set up seperate kdtrees
  mt_vector<vec3r> UVCs_vec;
  array_to_points(UVCs, UVCs_vec);

  mt_vector<vec3r> UVCs_lv, UVCs_rv;
  mt_vector<int> lv_UVC_idx, rv_UVC_idx;

  for (size_t i = 0; i < UVCs_vec.size(); i++) {
    if (is_LV.count(i)) {
      lv_UVC_idx.push_back(i);
      UVCs_lv.push_back(UVCs_vec[i]);
    } else {
      rv_UVC_idx.push_back(i);
      UVCs_rv.push_back(UVCs_vec[i]);
    }
  }

  // count how many lv / rv coord inputs
  mt_vector<int> lv_coord_idx, rv_coord_idx;
  for (size_t i = 0; i < coord_is_lv.size(); i++)
    if (coord_is_lv[i])
      lv_coord_idx.push_back(i);
    else
      rv_coord_idx.push_back(i);

  vtxlist.clear();
  vtxlist.resize(coord_is_lv.size());

  std::cout << "mesh UVCs consist of:" << std::endl;
  std::cout << UVCs_lv.size() << " LV coordinates" << std::endl;
  std::cout << UVCs_rv.size() << " RV coordinates" << std::endl;

  std::cout << "input consists of:" << std::endl;
  std::cout << lv_coord_idx.size() << " LV coordinates" << std::endl;
  std::cout << rv_coord_idx.size() << " RV coordinates" << std::endl;

  // left ventricle
  if (UVCs_lv.size()) {
    kdtree uvc_tree(10);
    uvc_tree.build_vertex_tree(UVCs_lv);

    std::cout << "Searching for vertices in LV .." << std::endl;

    for (int i : lv_coord_idx) {
      vec3f ctr(coords.data() + i * 3);
      vec3r dummy;
      mt_real dummy_len;
      int idx = 0;
      uvc_tree.closest_vertex(ctr, idx, dummy, dummy_len);  // Closest vertex in uvc coordinate
      mt_idx_t mesh_idx = lv_UVC_idx[idx];

      // Now locate all points in the given thresholds
      if (thr[i] > 0.0) {
        mt_vector<mt_mixed_tuple<mt_real, int>> vertices_cart;
        tree_cart.vertices_in_sphere(ptsvec_cart[mesh_idx], thr[i], vertices_cart);
        // Throw out anything that has a rho value outside of the epsilon-box
        for (auto &v : vertices_cart)
          if (is_LV.count(v.v2) && (std::abs(UVCs_vec[v.v2].x - UVCs_vec[mesh_idx].x) < eps[i]))
            vtxlist[i].push_back(v.v2);
      } else
        vtxlist[i].push_back(mesh_idx);
    }
  } else {
    for (int i : lv_coord_idx) vtxlist[i].push_back(-1);
  }

  // right ventricle
  if (UVCs_rv.size()) {
    kdtree uvc_tree(10);
    uvc_tree.build_vertex_tree(UVCs_rv);

    std::cout << "Searching for vertices in RV .." << std::endl;

    for (int i : rv_coord_idx) {
      vec3r ctr(coords.data() + i * 3);
      vec3r dummy;
      mt_real dummy_len;

      int idx = 0;
      uvc_tree.closest_vertex(ctr, idx, dummy, dummy_len);  // Closest vertex in uvc coordinate
      mt_idx_t mesh_idx = rv_UVC_idx[idx];

      // Now locate all points in the given thresholds
      if (thr[i] > 0.0) {
        mt_vector<mt_mixed_tuple<mt_real, int>> vertices_cart;
        tree_cart.vertices_in_sphere(ptsvec_cart[mesh_idx], thr[i], vertices_cart);

        // Throw out anything that has a rho value outside of the epsilon-box
        for (auto &v : vertices_cart)
          if (is_LV.count(v.v2) == 0 && (std::abs(UVCs_vec[v.v2].x - UVCs_vec[mesh_idx].x) < eps[i]))
            vtxlist[i].push_back(v.v2);
      } else
        vtxlist[i].push_back(mesh_idx);
    }
  } else {
    for (int i : rv_coord_idx) vtxlist[i].push_back(-1);
  }
}


void query_edge_mode(const mt_meshdata &mesh, MT_USET<mt_tuple<mt_idx_t>> &edge_set, std::vector<mt_real> &edge_len) {
  mt_vector<mt_tuple<mt_idx_t>> edges;

  for (size_t eidx = 0; eidx < mesh.e2n_cnt.size(); eidx++) {
    edges.resize(0);
    get_edges(mesh, eidx, edges);

    for (mt_tuple<mt_idx_t> &it : edges) edge_set.insert(it);
  }

  edge_len.resize(edge_set.size());
  size_t idx = 0;
  for (auto it = edge_set.begin(); it != edge_set.end(); ++it) {
    mt_point<mt_real> p0(mesh.xyz.data() + it->v1 * 3), p1(mesh.xyz.data() + it->v2 * 3);
    mt_point<mt_real> edge = p1 - p0;

    edge_len[idx++] = edge.length();
  }
}


void print_mesh_stats(const mt_meshdata &mesh) {
  printf("\n");
  printf("-------------- mesh statistics --------------\n");
  if (mesh.e2n_cnt.size()) printf("Number of elements:\t%zu\n", mesh.e2n_cnt.size());
  if (mesh.xyz.size()) printf("Number of nodes:\t%zu\n", mesh.xyz.size() / 3);
  printf("\n");
}

void query_curvature_mode_read(const query_options &opts, mt_meshdata &mesh, mt_meshdata &surfmesh,
                               query_curvature_options &mode_opts) {
  struct timeval t1, t2;

  std::cout << "Reading mesh: " << opts.msh.base << std::endl;
  gettimeofday(&t1, NULL);

  read_mesh_selected(mesh, opts.msh.format, opts.msh.base, CRP_READ_ELEM | CRP_READ_PTS);
  print_mesh_stats(mesh);

  gettimeofday(&t2, NULL);
  std::cout << "Done in " << (float)timediff_sec(t1, t2) << " sec" << std::endl;

  if (opts.surf.size() == 0) {
    if (mesh.etype[0] == Tri) {
      // we assume we have a surface mesh, thus we copy the connectivity data
      // (without the coords) from mesh into surfmesh
      surfmesh = mesh;
      surfmesh.xyz.resize(0);
      surfmesh.xyz.shrink_to_fit();
    } else {
      std::cerr << "Error. Volumetric meshes require a surface file input. Aborting." << std::endl;
      exit(1);
    }
  } else {
    std::cout << "Reading surface.. " << std::endl;
    gettimeofday(&t1, NULL);
    readElements(surfmesh, opts.surf + SURF_EXT);
    gettimeofday(&t2, NULL);
    std::cout << "Done in " << (float)timediff_sec(t1, t2) << " sec" << std::endl;
  }

  mode_opts.rad = atof(opts.rad.c_str());
}

void query_curvature_mode(mt_meshdata &mesh, mt_meshdata &surfmesh, mt_vector<mt_real> &curv,
                          const query_curvature_options &mode_opts

) {
  struct timeval t1, t2;

  compute_full_mesh_connectivity(surfmesh);

  std::cout << "Computing curvature .. " << std::endl;
  gettimeofday(&t1, NULL);

  mesh_curvature(surfmesh, mesh.xyz, mode_opts.rad, curv);

  gettimeofday(&t2, NULL);
  std::cout << "Done in " << (float)timediff_sec(t1, t2) << " sec" << std::endl;
}