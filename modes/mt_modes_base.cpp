/**
* @file mt_modes_base.cpp
* @brief Basic meshtool modes functions
* @author Aurel Neic
* @version 
* @date 2017-08-16
*/

#include "mt_modes_base.h"

void print_usage(const char* exe)
{
  fprintf(stderr, "Error: Wrong usage.\n");
  fprintf(stderr, "Use: %s [help|convert|collect|clean|extract|generate|"
                  "itk|insert|interpolate|split|smooth|transform|map|merge|query|resample|restore|reindex] \n", exe);
  fprintf(stderr, "For help use: %s help\n", exe);
  fprintf(stderr, "\n");
}

void print_general_help(const char* exe)
{
  fprintf(stderr, "\nHELP:\n\n");
  fprintf(stderr, "With %s, the user uses \"modes\" to specify the operation he wants to perform on a mesh.\n", exe);
  fprintf(stderr, "Currently the supported modes are:\n");
  fprintf(stderr,
                  "clean quality\t\t\tdeform mesh elements to reach a certain quality threshold value\n"
                  "clean topology\t\t\tclean the mesh from bad topology definitions.\n"
                  "convert\t\t\t\tconvert between different mesh formats\n"
                  "collect\t\t\t\tmerge a mesh with datasets\n"
                  "extract mesh\t\t\ta submesh is extracted from a given mesh based on given element tags\n"
                  "extract data\t\t\tdata defined on a mesh is extracted for a given submesh\n"
                  "extract vtkdata\t\t\textract all VTK datasets in a VTK file\n"
                  "extract surface\t\t\textract a sequence of surfaces defined by set operations on element tags\n"
                  "extract myocard\t\t\tthe myocardium is extracted from a given mesh The myocard is identified based on non-zero fibers.\n"
                  "extract volume\t\t\textract elements inside a given box volume\n"
                  "extract unreachable\t\tdecompose mesh into parts based on mesh connectivity\n"
                  "extract gradient\t\tcompute gradient and gradient magnitude of a scalar function on a mesh\n"
                  "extract overlap\t\t\textract the elements of mesh1 that overlap with mesh2\n"
                  "extract tags\t\t\textract the elements tags into an element data vector\n"
                  "extract fibers\t\t\textract the elements fibers/sheets into element data vectors\n"
                  "extract adjustment\t\textract nodal adjustment file from .dat and .vtx files\n"
                  "extract vtxhalo\t\t\textract connected halo vertex sets for a given block vertex set\n"
                  "extract isosurf\t\t\textract an isosurface\n"
                  "generate fibres\t\t\tgenerate default fibers for a given mesh file\n"
                  "generate distancefield\t\tgenerate a distancefield between two surfaces\n"
                  "generate mesh\t\t\tgenerate a tetrahedral mesh from a list of nested triangle surfaces\n"
                  "generate bboxmesh\t\tgenerate the bounding box mesh of a given mesh\n"
                  "generate split\t\t\tgenerate a split file for given split operations\n"
                  "generate surfsplit\t\tgenerate a split file from a given surface\n"
                  "insert submesh\t\t\ta submesh is inserted back into a mesh and written to an output mesh\n"
                  "insert meshdata\t\t\tthe fiber and tag data of a mesh is inserted into another mesh\n"
                  "insert data\t\t\tdata defined on a submesh is inserted back into a mesh\n"
                  "insert tagsurfs\t\t\tinsert element tags defined by closed surface regions\n"
                  "insert tags\t\t\tinsert an element data vector as new mesh element tags\n"
                  "insert imagetags\t\tinsert the region tags from an image segmentation\n"
                  "insert fibers\t\t\tinsert element fibers\n"
                  "interpolate clouddata\t\tinterpolate data from a pointcloud onto a mesh\n"
                  "interpolate elemdata\t\tinterpolate element data from one mesh onto another\n"
                  "interpolate elem2node\t\tinterpolate data from elements onto nodes\n"
                  "interpolate node2elem\t\tinterpolate data from nodes onto elements\n"
                  "interpolate nodedata\t\tinterpolate nodal data from one mesh onto another\n"
                  "interpolate shelldata\t\textrapolate data from a surface shell onto a volumetric mesh.\n"
                  "interpolate impstate\t\tinterpolate an ionic state file from one mesh onto another\n"
                  "itk smooth\t\t\tSmooth the voxel data\n"
                  "itk close\t\t\tApply closing (ie. dilate-erode) algorithm to itk data.\n"
                  "itk normalize\t\t\tNormalize voxel spacing\n"
                  "itk padding\t\t\tadd padding to voxel data\n"
                  "itk crop\t\t\tremove surrounding whitespace\n"
                  "itk flip\t\t\tflip the voxel data along given axes\n"
                  "itk dtype\t\t\tconvert datatype\n"
                  "itk resample\t\t\tresample voxel data\n"
                  "itk sample\t\t\tcreate an itk image stack from sampling surfaces\n"
                  "itk extract\t\t\textract slices of an itk image stack\n"
                  "itk mesh\t\t\tconvert itk image to hexahedral mesh\n"
                  "itk interpolate\t\t\tinterpolate clouddata to itk image\n"
                  "itk transform\t\t\ttransform an itk image stack\n"
                  "itk extrude\t\t\ttransform an itk image stack\n"
                  "itk convert\t\t\tconvert format of an image stack\n"
                  "map\t\t\t\tmap .vtx, .surf and .neubc files to the indexing of a submesh\n"
                  "merge surface\t\t\tmerge the geometry given by a closed surface mesh into a different mesh\n"
                  "merge meshes\t\t\tmerge two meshes, unifying co-located vertices\n"
                  "query bbox\t\t\tprint the bounding box of a given mesh\n"
                  "query curvature\t\t\tcompute the curvature of a surface\n"
                  "query edges\t\t\tprint several statistics related to the mesh edges\n"
                  "query elem_idx\t\t\tget the element index a given XYZ coordinate is closest to\n"
                  "query graph\t\t\tprint the nodal connectivity graph\n"
                  "query idx\t\t\tprint indices in a proximity to a given coordinate\n"
                  "query idxlist\t\t\tapply 'query idx' for all coordinates in a file\n"
                  "query idxlist_uvc\t\tapply 'query idx' for all coordinates in a file based on uvc coordinates\n"
                  "query idxlist_ucc\t\tapply 'query idx' for all coordinates in a file based on cardiac coordinates\n"
                  "query coords_xyz\t\tget the XYZ coordinates for all UVC coordinates provided in a file\n"
                  "query uccoords_xyz\t\tget the XYZ coordinates for all UCC coordinates provided in a file\n"
                  "query insidepoint\t\tget a point inside a given closed surface\n"
                  "query quality\t\t\tprint mesh quality statistics\n"
                  "query smoothness\t\tcompute the nodal smoothness\n"
                  "query curvature\t\t\tcompute the curvature of a surface.\n"
                  "query normals\t\t\toutput normal vectors for a given mesh and surface.\n"
                  "query tags\t\t\tprint the tags present in a given mesh\n"
                  "query values\t\t\tget min / max / avrg data value for a vertex set\n"
                  "query distance\t\t\tquery min/max/average distance of mesh to surface mesh\n"
                  "reindex\t\t\t\treindex a mesh to improve matrix bandwidth and cache efficiency\n"
                  "resample mesh\t\t\tresample a tetrahedral mesh to fit a given edge size range\n"
                  "resample purkinje\t\tresample purkinje cables as close as possible to a segment size\n"
                  "resample surfmesh\t\tresample a triangle mesh to fit a given edge size range\n"
                  "resample uniform\t\tuniformly refine a tri surf or tet volume\n"
                  "restore mapping\t\t\trestore nodal and element mapping for a submesh w.r.t. a reference mesh\n"
                  "smooth data\t\t\tsmooth data defined on a mesh\n"
                  "smooth mesh\t\t\tsmooth surfaces and volume of a mesh\n"
                  "smooth surface\t\t\tsmooth one or multiple surfaces of a mesh\n"
                  "split\t\t\t\tsplit the mesh connectivity based on a given split file\n"
                  "transform\t\t\ttransform the vertex coordinates of a mesh\n\n");
  fprintf(stderr, "For example, to extract a submesh from a given mesh, type\n\n");
  fprintf(stderr, "%s extract mesh <modeopts>\n\n", exe);
  fprintf(stderr, "Note, that <modeopts> denote the options of the \"extract mesh\" mode.\n");
  fprintf(stderr, "To view the options of any mode, just call that mode without any options.\n");
  fprintf(stderr, "\n");
}
