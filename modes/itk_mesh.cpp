#include "itk_mode.h"

bool itk_mesh_mode(itk_image& img, mt_meshdata& mesh, mt_vector<mt_real>& values, const bool has_slices,
                   const unsigned int comp, const mt_real scale, const mt_triple<MT_USET<mt_idx_t>>& slices) {
  struct timeval t1, t2;

  bool success = false;
  std::cout << "Converting to Hex-mesh ..." << std::endl;
  gettimeofday(&t1, NULL);

  if (has_slices)
    success = img.to_hex_mesh(mesh, values, slices, comp, scale);
  else
    success = img.to_hex_mesh(mesh, values, comp, scale);

  gettimeofday(&t2, NULL);
  std::cout << "Done in " << (float)timediff_sec(t1, t2) << " sec" << std::endl;
  return success;
}