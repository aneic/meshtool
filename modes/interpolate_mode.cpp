/**
* @file interpolate_mode.cpp
* @brief Interpolate data between meshes.
* @author Aurel Neic
* @version
* @date 2017-10-27
*/

#include "mt_modes_base.h"
#include "dense_mat.hpp"
#include "sparse_matrix.hpp"

#ifdef MT_ADDONS
#include "addons_utils.h"
#endif
#include "interpolate_mode.h"

enum itp_type {ITP_NODES, ITP_ELEM, ITP_E2N, ITP_N2E, ITP_CLOUD, ITP_IMP_STATE, ITP_SHELL};

struct interpolate_options {
  mt_filename imsh;
  mt_filename omsh;
  std::string pts;
  std::string idat;
  std::string odat;
  std::string thr;
  std::string pts_dynpt;
  std::string interp_mode;
  bool norm = false;

  enum itp_type type;
};

static const std::string pts_par          = "-pts=";
static const std::string dynpts_par       = "-dynpts=";

void print_interpolate_nodedata_help()
{
  fprintf(stderr, "interpolate nodedata: interpolate nodal data from one mesh onto another\n");
  fprintf(stderr, "parameters:\n");
  fprintf(stderr, "%s<path>\t (input) path to basename of the mesh we interpolate to\n", out_mesh_par.c_str());
  fprintf(stderr, "%s<path>\t (input) path to basename of the mesh we interpolate from\n", inp_mesh_par.c_str());
  fprintf(stderr, "%s<path>\t (input) path to input data.\n", idat_par.c_str());
  fprintf(stderr, "%s<path>\t (output) path to output data.\n", odat_par.c_str());
  fprintf(stderr, "%s<path>\t (optional) Dynpts describing point cloud movement.\n", dynpts_par.c_str());
  fprintf(stderr, "%s\t\t (optional flag) if set, normalize vector data.\n", norm_flag.c_str());
  fprintf(stderr, "\n");
}
void print_interpolate_elemdata_help()
{
  fprintf(stderr, "interpolate elemdata: interpolate element data from one mesh onto another\n");
  fprintf(stderr, "parameters:\n");
  fprintf(stderr, "%s<path>\t (input) path to basename of the mesh we interpolate to\n", out_mesh_par.c_str());
  fprintf(stderr, "%s<path>\t (input) path to basename of the mesh we interpolate from\n", inp_mesh_par.c_str());
  fprintf(stderr, "%s<path>\t (input) path to input data.\n", idat_par.c_str());
  fprintf(stderr, "%s<path>\t (output) path to output data.\n", odat_par.c_str());
  fprintf(stderr, "%s<path>\t (optional) Dynpts describing point cloud movement.\n", dynpts_par.c_str());
  fprintf(stderr, "%s\t\t (optional flag) if set, normalize vector data.\n", norm_flag.c_str());
  fprintf(stderr, "\n");
}
void print_interpolate_clouddata_help()
{
  fprintf(stderr, "interpolate clouddata: interpolate data from a pointcloud onto a mesh using radial basis function interpolation\n");
  fprintf(stderr, "parameters:\n");
  fprintf(stderr, "%s<path>\t (input) path to basename of the mesh we interpolate to\n", out_mesh_par.c_str());
  fprintf(stderr, "%s<path>\t (input) path to the coordinates of the point cloud\n", pts_par.c_str());
  fprintf(stderr, "%s<path>\t (input) path to input data.\n", idat_par.c_str());
  fprintf(stderr, "%s<path>\t (output) path to output data.\n", odat_par.c_str());
  fprintf(stderr, "%s<int>\t (optional) Choose between localized Shepard (=0), global Shepard (=1), and RBF interpolation (=2). Default is 2.\n", mode_par.c_str());
  fprintf(stderr, "%s<path>\t (optional) Dynpts describing point cloud movement.\n", dynpts_par.c_str());
  fprintf(stderr, "\n");
}
void print_interpolate_elem2node_help()
{
  fprintf(stderr, "interpolate elem2node: interpolate data from elements onto nodes\n");
  fprintf(stderr, "parameters:\n");
  fprintf(stderr, "%s<path>\t (input) path to basename of the mesh we interpolate to\n", out_mesh_par.c_str());
  fprintf(stderr, "%s<path>\t (input) path to input data.\n", idat_par.c_str());
  fprintf(stderr, "%s<path>\t (output) path to output data.\n", odat_par.c_str());
  fprintf(stderr, "%s\t\t (optional flag) if set, normalize vector data.\n", norm_flag.c_str());
  fprintf(stderr, "\n");
}
void print_interpolate_node2elem_help()
{
  fprintf(stderr, "interpolate node2elem: interpolate data from nodes onto elements\n");
  fprintf(stderr, "parameters:\n");
  fprintf(stderr, "%s<path>\t (input) path to basename of the mesh we interpolate to\n", out_mesh_par.c_str());
  fprintf(stderr, "%s<path>\t (input) path to input data.\n", idat_par.c_str());
  fprintf(stderr, "%s<path>\t (output) path to output data.\n", odat_par.c_str());
  fprintf(stderr, "%s<int>\t (optional) Choose between mean (for float data) (=0), highest frequency (for int data) (=1), interpolation. Default is 0.\n", mode_par.c_str());
  fprintf(stderr, "%s\t\t (optional flag) if set, normalize vector data.\n", norm_flag.c_str());
  fprintf(stderr, "\n");
  fprintf(stderr, "If no input data is provided, the mesh points are interpolated!\n");
  fprintf(stderr, "\n");
}

#ifdef MT_ADDONS
void print_interpolate_shell_help()
{
  fprintf(stderr, "interpolate shelldata: extrapolate data from a surface shell onto a volumetric mesh.\n");
  fprintf(stderr, "parameters:\n");
  fprintf(stderr, "%s<path>\t (input) path to basename of the surface mesh we interpolate from\n", inp_mesh_par.c_str());
  fprintf(stderr, "%s<path>\t (input) path to basename of the mesh we interpolate to\n", out_mesh_par.c_str());
  fprintf(stderr, "%s<path>\t (input) path to input data.\n", idat_par.c_str());
  fprintf(stderr, "%s<path>\t (output) path to output data.\n", odat_par.c_str());
  fprintf(stderr, "\n");
}
#endif


void print_interpolate_impstate_help()
{
  fprintf(stderr, "interpolate impstate: interpolate an ionic state file from one mesh onto another\n");
  fprintf(stderr, "parameters:\n");
  fprintf(stderr, "%s<path>\t (input) path to basename of the mesh we interpolate to\n", out_mesh_par.c_str());
  fprintf(stderr, "%s<path>\t (input) path to basename of the mesh we interpolate from\n", inp_mesh_par.c_str());
  fprintf(stderr, "%s<path>\t (input) path to input data.\n", idat_par.c_str());
  fprintf(stderr, "%s<path>\t (output) path to output data.\n", odat_par.c_str());
  fprintf(stderr, "\n");
}

int interpolate_parse_options(int argc, char** argv, struct interpolate_options & opts)
{
  if(argc < 3) {
    std::cerr << "Please choose one of the following interpolate modes: " << std::endl << std::endl;
    print_interpolate_nodedata_help();
    print_interpolate_elemdata_help();
    print_interpolate_clouddata_help();
    print_interpolate_elem2node_help();
    print_interpolate_node2elem_help();
#ifdef MT_ADDONS
    print_interpolate_shell_help();
#endif
    print_interpolate_impstate_help();
    return 1;
  }

  std::string interp_type = argv[2];

  std::string imsh_base;
  std::string omsh_base;

  for(int i=3; i<argc; i++) {
    std::string param = reformat_param(argv, i, argc);
    bool match = false;

    if(!match) match = parse_param(param, inp_mesh_par, imsh_base);
    if(!match) match = parse_param(param, out_mesh_par, omsh_base);
    if(!match) match = parse_param(param, idat_par, opts.idat, chk_fexists);
    if(!match) match = parse_param(param, odat_par, opts.odat);
    if(!match) match = parse_param(param, thr_par, opts.thr);
    if(!match) match = parse_param(param, pts_par, opts.pts);
    if(!match) match = parse_param(param, dynpts_par, opts.pts_dynpt);
    if(!match) match = parse_param(param, mode_par, opts.interp_mode);

    if(!match) match = parse_flag(param, norm_flag, opts.norm);

    if(!match) {
      std::cerr << "Error: Cannot parse parameter " << param << std::endl;
      return 2;
    }
  }

  if(interp_type.compare("nodedata") == 0)
  {
    opts.imsh.assign(imsh_base, "");
    opts.omsh.assign(omsh_base, "");

    opts.type = ITP_NODES;

    if(! (opts.imsh.isSet() && opts.omsh.isSet() &&
          opts.idat.size() && opts.odat.size()) ) {
      std::cerr << "Interpolate nodedata error: Insufficient parameters provided." << std::endl;
      print_interpolate_nodedata_help();
      return 3;
    }
  }
  else if(interp_type.compare("elemdata") == 0)
  {
    opts.imsh.assign(imsh_base, "");
    opts.omsh.assign(omsh_base, "");

    opts.type = ITP_ELEM;

    if(! (opts.imsh.isSet() && opts.omsh.isSet() &&
          opts.idat.size() && opts.odat.size()) ) {
      std::cerr << "Interpolate elemdata error: Insufficient parameters provided." << std::endl;
      print_interpolate_elemdata_help();
      return 3;
    }
  }
  else if(interp_type.compare("clouddata") == 0)
  {
    opts.omsh.assign(omsh_base, "");

    opts.type = ITP_CLOUD;

    if(! (opts.omsh.isSet() && opts.pts.size() &&
          opts.idat.size() && opts.odat.size()) ) {
      std::cerr << "Interpolate clouddata error: Insufficient parameters provided." << std::endl;
      print_interpolate_clouddata_help();
      return 3;
    }
  }
  else if(interp_type.compare("elem2node") == 0)
  {
    opts.omsh.assign(omsh_base, "");

    opts.type = ITP_E2N;

    if(! (opts.omsh.isSet() && opts.idat.size() && opts.odat.size()) ) {
      std::cerr << "Interpolate elem2node error: Insufficient parameters provided." << std::endl;
      print_interpolate_elem2node_help();
      return 3;
    }
  }
  else if(interp_type.compare("node2elem") == 0)
  {
    opts.omsh.assign(omsh_base, "");

    opts.type = ITP_N2E;

    if(! (opts.omsh.isSet() && opts.odat.size()) ) {
      std::cerr << "Interpolate node2elem error: Insufficient parameters provided." << std::endl;
      print_interpolate_node2elem_help();
      return 3;
    }
  }
#ifdef MT_ADDONS
  else if(interp_type.compare("shelldata") == 0)
  {
    opts.imsh.assign(imsh_base, "");
    opts.omsh.assign(omsh_base, "");

    opts.type = ITP_SHELL;

    if(! (opts.imsh.isSet() && opts.omsh.isSet() && opts.idat.size() && opts.odat.size()) ) {
      std::cerr << "Interpolate shelldata error: Insufficient parameters provided." << std::endl;
      print_interpolate_shell_help();
      return 3;
    }
  }
#endif
  else if(interp_type.compare("impstate") == 0)
  {
    opts.imsh.assign(imsh_base, "");
    opts.omsh.assign(omsh_base, "");

    opts.type = ITP_IMP_STATE;

    if(! (opts.imsh.isSet() && opts.omsh.isSet() &&
          opts.idat.size() && opts.odat.size()) ) {
      std::cerr << "Interpolate impstate error: Insufficient parameters provided." << std::endl;
      print_interpolate_impstate_help();
      return 3;
    }
  }
  else {
    print_usage(argv[0]);
    return 2;
  }

  return 0;
}


void interpolate_mode(int argc, char** argv)
{
  struct interpolate_options opts;
  int ret = interpolate_parse_options(argc, argv, opts);
  if(ret != 0) return;

  struct timeval t1, t2;
  struct mt_meshdata imesh, omesh;
  mt_vector<mt_real> ipts;

  // data_idx may be:
  // 0 : scalar dat file
  // 1 : scalar igb file
  // 2 : vec file
  // 3 : vec3 igb file
  // 4 : vec9 igb file
  short data_idx = -1;
  mt_vector<mt_real>            idat,     odat;
  mt_vector<mt_point<mt_real> > idat_vec, odat_vec;
  mt_vector<dmat<mt_real> >     idat_ten, odat_ten;
  igb_header igb, igb_out;

  if(opts.type != ITP_IMP_STATE) {
    setup_data_format(opts.idat, data_idx, igb);
    if((data_idx < 0) && (opts.type != ITP_N2E)) exit(EXIT_FAILURE);

    if(igb.fileptr)
      init_igb_header(igb, opts.odat, igb_out);
  }

  // first read mesh
  gettimeofday(&t1, NULL);
  std::cout << "Reading output mesh: " << opts.omsh.base << std::endl;
  read_mesh_selected(omesh, opts.omsh.format, opts.omsh.base, CRP_READ_ELEM | CRP_READ_PTS);
  compute_full_mesh_connectivity(omesh, opts.omsh.base);
  gettimeofday(&t2, NULL);
  std::cout << "Done in " << (float)timediff_sec(t1, t2) << " sec" << std::endl;

  switch(opts.type) {
    case ITP_NODES:
    {
      interpolate_nodes_data intplt_data;

      std::cout << "Reading input mesh: " << opts.imsh.base << std::endl;
      gettimeofday(&t1, NULL);
      read_mesh_selected(imesh, opts.imsh.format, opts.imsh.base, CRP_READ_ELEM | CRP_READ_PTS);
      compute_full_mesh_connectivity(imesh, opts.imsh.base);
      gettimeofday(&t2, NULL);
      std::cout << "Done in " << (float)timediff_sec(t1, t2) << " sec" << std::endl;

      std::cout << "Compute correspondance .." << std::endl;
      gettimeofday(&t1, NULL);
      intplt_data.update_correspondance(omesh, imesh);
      gettimeofday(&t2, NULL);
      std::cout << "Done in " << (float)timediff_sec(t1, t2) << " sec" << std::endl;

      intplt_data.ivtxtree.build_vertex_tree(imesh.xyz);

      mt_meshdata isurf;
      compute_surface(imesh, isurf, true); isurf.xyz = imesh.xyz;
      intplt_data.isurftree.build_tree(isurf);

      std::cout << "Compute interpolation .." << std::endl;
      gettimeofday(&t1, NULL);
      int dpn = 0;
      switch(data_idx) {
        case 0:
        case 1: dpn = 1; break;

        case 2:
        case 3: dpn = 3; break;

        case 4: dpn = 9; break;
        default: break;
      }

      igb_header dynpts;
      igb_header* dynpts_ptr = nullptr;
      if ((opts.pts_dynpt.size() > 0) && (file_exists(opts.pts_dynpt))) {
        // setup input igb header
        init_igb_header(opts.pts_dynpt, dynpts);
        read_igb_header(dynpts);
        dynpts_ptr = std::addressof(dynpts);
      }

      if(igb.fileptr) {
        igb_out.v_x = intplt_data.corr.size();
        write_igb_header(igb_out);

        interpolate_nodes_mode(intplt_data, imesh, omesh, igb, igb_out, dynpts_ptr, opts.norm, dpn, true);
      } else {
        read_vector_ascii(idat, opts.idat);
        interpolate_nodes_mode(intplt_data, imesh, omesh, idat, odat, opts.norm, dpn);
        write_vector_ascii(odat, opts.odat, dpn);
      }

      gettimeofday(&t2, NULL);
      std::cout << "Done in " << (float)timediff_sec(t1, t2) << " sec" << std::endl;
      break;
    }

    case ITP_ELEM:
    {
      std::cout << "Reading input mesh: " << opts.imsh.base << std::endl;
      gettimeofday(&t1, NULL);
      read_mesh_selected(imesh, opts.imsh.format, opts.imsh.base, CRP_READ_ELEM | CRP_READ_PTS);
      compute_full_mesh_connectivity(imesh, opts.imsh.base);
      gettimeofday(&t2, NULL);
      std::cout << "Done in " << (float)timediff_sec(t1, t2) << " sec" << std::endl;

      std::cout << "Setting up kdtree for input mesh vertices .." << std::endl;
      gettimeofday(&t1, NULL);

      kdtree vert_tree(10);
      vert_tree.build_vertex_tree(imesh.xyz);

      gettimeofday(&t2, NULL);
      std::cout << "Done in " << (float)timediff_sec(t1, t2) << " sec" << std::endl;

      std::cout << "Compute interpolation .." << std::endl;
      gettimeofday(&t1, NULL);
      switch(data_idx) {
        case 0:
          read_vector(idat, opts.idat, true);
          check_size(idat.size(), imesh.e2n_cnt.size(), "element interpolation");
          elementwise_interpolation(imesh, omesh, vert_tree, idat, odat);
          write_vector(odat, opts.odat, 1);
          break;

        case 2:
          read_vector_ascii(idat, opts.idat, true);
          check_size(idat.size(), imesh.e2n_cnt.size()*3, "element interpolation");
          array_to_points(idat, idat_vec);
          elementwise_interpolation(imesh, omesh, vert_tree, idat_vec, odat_vec);
          if(opts.norm) {
            for(vec3r & v : odat_vec) {
              mt_real len2 = v.length2();
              if(len2) v /= sqrt(len2);
            }
          }
          write_vector_ascii(odat_vec, opts.odat);
          break;

        case 1:
        case 3:
        case 4:
        {
          const bool have_dynpts = opts.pts_dynpt.size() > 0;
          igb_header * igb_pts_dynpt = have_dynpts ? new igb_header
            : nullptr;
          if(have_dynpts) {
            init_igb_header(opts.pts_dynpt, *igb_pts_dynpt);
            read_igb_header(*igb_pts_dynpt);
          }
          kdtree * act_tree = nullptr;
          check_size(size_t(igb.v_x), imesh.e2n_cnt.size(), "element interpolation");

          // set new spatial size and write output igb header
          igb_out.v_x = omesh.e2n_cnt.size();
          write_igb_header(igb_out);

          printf("Processing igb time-slices: %s to %s: \n", opts.idat.c_str(), opts.odat.c_str());

          for(int t=0; t<igb.v_t; t++) {
            printf("\rcurrent time-slice %d / %d .. ", t+1, int(igb.v_t));
            fflush(stdout);

            read_igb_slice(idat, igb);
            if(have_dynpts) {
              read_igb_slice(imesh.xyz, *igb_pts_dynpt);
              act_tree = new kdtree(10);
              act_tree->build_vertex_tree(imesh.xyz);
            } else {
              act_tree = &vert_tree;
            }
            if(data_idx == 1) {
              elementwise_interpolation(imesh, omesh, *act_tree, idat, odat);
            }
            else if(data_idx == 3) {
              array_to_points(idat, idat_vec);
              elementwise_interpolation(imesh, omesh, *act_tree, idat_vec, odat_vec);
              if(opts.norm) {
                for(vec3r & v : odat_vec) {
                  mt_real len2 = v.length2();
                  if(len2) v /= sqrt(len2);
                }
              }
              points_to_array(odat_vec, odat);
            }
            else if(data_idx == 4) {
              array_to_tensors(idat, idat_ten);
              elementwise_interpolation(imesh, omesh, *act_tree, idat_ten, odat_ten);
              tensors_to_array(odat_ten, odat);
            }

            write_igb_slice(odat, igb_out);
            if(have_dynpts)
              delete act_tree;
          }
          printf("\n");

          fclose(igb.fileptr);
          fclose(igb_out.fileptr);
          if(have_dynpts) {
            fclose(igb_pts_dynpt->fileptr);
            delete igb_pts_dynpt;
          }
          break;
        }
        default: {
          std::cout << "Data type not implementd!" << std::endl;
        } break;
      }

      gettimeofday(&t2, NULL);
      std::cout << "Done in " << (float)timediff_sec(t1, t2) << " sec" << std::endl;
      break;
    }

    case ITP_CLOUD:
    {
      interpolation_t interp_type = interpolation_t::rbf;
      if (opts.interp_mode.size() > 0) {
        const int interp_mode_int = atoi(opts.interp_mode.c_str());
        if (interp_mode_int == 0)
          interp_type = interpolation_t::shepard;
        else if (interp_mode_int == 1)
          interp_type = interpolation_t::glob_shepard;
        else
          std::cout << "Unknown interpolation mode, take default !" << std::endl;
      }

      std::cout << "Reading input points: " << opts.pts << std::endl;
      if (endswith(opts.pts, CARPBIN_PTS_EXT))
        readPointsBinary(ipts, opts.pts);
      else if (endswith(opts.pts, CARPTXT_PTS_EXT))
        readPoints(ipts, opts.pts);
      else {
        std::cout << "Unknown file extension in file '" << opts.pts << "', try to read text data ..." << std::endl;
        readPoints(ipts, opts.pts);
      }
      gettimeofday(&t2, NULL);
      std::cout << "Done in " << (float)timediff_sec(t1, t2) << " sec" << std::endl;

      std::cout << "Compute interpolation .." << std::endl;
      gettimeofday(&t1, NULL);
      switch(data_idx) {
        case 0:
        case 2:
        {
          if(data_idx == 0)
            read_vector(idat, opts.idat, true);
          else
            read_vector_ascii(idat, opts.idat, true);

          //Setup pointcloud interpolator
          const int dpn = (data_idx == 0) ? 1 : 3;
          const int rval = interpolate_clouddata_mode(interp_type, ipts, omesh,
                                                      idat, odat, dpn);
          if (rval == 0) {
            if(data_idx == 0)
              write_vector(odat, opts.odat, true);
            else
              write_vector_ascii(odat, opts.odat, 3);
          }
        } break;
        case 1:
        case 3:
        case 4: {
          const int dpn = (data_idx == 1) ? 1 : ((data_idx == 3) ? 3 : 9);
          igb_header dynpts;
          igb_header* dynpts_ptr = nullptr;
          if ((opts.pts_dynpt.size() > 0) && (file_exists(opts.pts_dynpt))) {
            // setup input igb header
            init_igb_header(opts.pts_dynpt, dynpts);
            read_igb_header(dynpts);
            dynpts_ptr = std::addressof(dynpts);
          }
          igb_out.v_x = omesh.xyz.size() / 3;
          write_igb_header(igb_out);
          const int rval = interpolate_clouddata_mode(interp_type, ipts, omesh,
                                                      igb, igb_out, dynpts_ptr, dpn);
          if (rval != 0)
            std::cout << "Error while processing igb data !" << std::endl;
          if (dynpts_ptr) {
            close_igb_fileptr(dynpts);
            dynpts_ptr = nullptr;
          }
          close_igb_fileptr(igb);
          close_igb_fileptr(igb_out);
        } break;
        default: {
          std::cout << "Data type not implementd!" << std::endl;
        } break;
      }

      gettimeofday(&t2, NULL);
      std::cout << "Done in " << (float)timediff_sec(t1, t2) << " sec" << std::endl;
      break;
    }

    case ITP_E2N:
    {
      std::cout << "Interpolating element data onto nodes .." << std::endl;
      gettimeofday(&t1, NULL);
      switch(data_idx) {
        case 0:
        case 2: {
          const int dpn = (data_idx == 0) ? 1 : 3;
          read_vector_ascii(idat, opts.idat, true);
          const int rval = interpolate_elem2node_mode(omesh, idat, odat, opts.norm, dpn);
          if (rval == 0)
            write_vector_ascii(odat, opts.odat, dpn);
        } break;
        case 1:
        case 3:
        case 4: {
          const int dpn = (data_idx == 1) ? 1 : ((data_idx == 3) ? 3 : 9);
          // set new spatial size and write output igb header
          igb_out.v_x = omesh.xyz.size() / 3;
          write_igb_header(igb_out);
          printf("Processing igb time-slices: %s to %s: \n", opts.idat.c_str(), opts.odat.c_str());
          const int rval = interpolate_elem2node_mode(omesh, igb, igb_out, opts.norm, dpn);
          if (rval != 0)
            std::cout << "Error while processing igb data !" << std::endl;
          close_igb_fileptr(igb);
          close_igb_fileptr(igb_out);
        } break;
        default: {
          std::cout << "Data type not implementd!" << std::endl;
        } break;
      }
      gettimeofday(&t2, NULL);
      std::cout << "Done in " << (float)timediff_sec(t1, t2) << " sec" << std::endl;
      break;
    }
    case ITP_N2E:
    {
      std::cout << "Interpolating node data onto elements .." << std::endl;

      int mode = 0;
      if(opts.interp_mode.size()) mode = atoi(opts.interp_mode.c_str());

      gettimeofday(&t1, NULL);
      switch(data_idx) {
        case 0:
        case 2: {
          const int dpn = (data_idx == 0) ? 1 : 3;
          read_vector_ascii(idat, opts.idat, true);
          if(data_idx != 0) {mode = 0;}
          const int rval = interpolate_node2elem_mode(omesh, idat, odat, opts.norm, mode, dpn);
          if (rval == 0)
            write_vector_ascii(odat, opts.odat, dpn);
        } break;
        case 1:
        case 3:
        case 4: {
          const int dpn = (data_idx == 1) ? 1 : ((data_idx == 3) ? 3 : 9);
          // set new spatial size and write output igb header
          igb_out.v_x = omesh.e2n_cnt.size();
          write_igb_header(igb_out);

          printf("Processing igb time-slices: %s to %s: \n", opts.idat.c_str(), opts.odat.c_str());
          if(data_idx != 1) {mode = 0;}
          const int rval = interpolate_node2elem_mode(omesh, igb, igb_out, opts.norm, mode, dpn);
          if (rval != 0)
            std::cout << "Error while processing igb data !" << std::endl;
          close_igb_fileptr(igb);
          close_igb_fileptr(igb_out);
        } break;
        default: {
          std::cout << "Data type not implemented, fall back to mesh points!" << std::endl;

          array_to_points(omesh.xyz, idat_vec);
          nodeData_to_elemData(omesh, idat_vec, odat_vec);
          points_to_array(odat_vec, odat);
          if (endswith(opts.odat, CARPTXT_PTS_EXT))
            writePoints(odat, opts.odat);
          else
            write_vector_ascii(odat, opts.odat, 3);
          break;

        } break;
      }
      gettimeofday(&t2, NULL);
      std::cout << "Done in " << (float)timediff_sec(t1, t2) << " sec" << std::endl;
      break;
    }

#ifdef MT_ADDONS
    case ITP_SHELL:
    {
      std::cout << "Reading input mesh: " << opts.imsh.base << std::endl;
      gettimeofday(&t1, NULL);
      read_mesh_selected(imesh, opts.imsh.format, opts.imsh.base);
      // compute_full_mesh_connectivity(imesh, opts.imsh.base);
      gettimeofday(&t2, NULL);
      std::cout << "Done in " << (float)timediff_sec(t1, t2) << " sec" << std::endl;

      std::cout << "Compute interpolation .." << std::endl;
      gettimeofday(&t1, NULL);
      switch(data_idx) {
        case 0:
        case 2: {
          int dpn = 0;
          if(data_idx == 0) {
            read_vector(idat, opts.idat, true);
            dpn = 1;
          } else {
            read_vector_ascii(idat, opts.idat, true);
            dpn = 3;
          }

          bool nodal = false;

          if( (idat.size() / dpn) == (imesh.xyz.size() / 3) ) {
            nodal = true;
          } else if ( (idat.size() / dpn) == imesh.e2n_cnt.size()) {
            nodal = false;
          } else {
            fprintf(stderr, "%s error: data doesnt match elements or nodes! Aborting!\n", __func__);
            exit(EXIT_FAILURE);
          }

          mt_vector<mt_idx_t> idx;
          find_extrapolation_index(omesh, imesh, nodal, idx);

          odat.resize(idx.size()*dpn);

          for(size_t i=0, k=0; i < idx.size(); i++) {
            for(int j=0; j<dpn; j++, k++) {
              odat[k] = idat[idx[i]*dpn+j];
            }
          }

          write_vector_ascii(odat, opts.odat, dpn);
          if(data_idx == 0)
            write_vector(idat, opts.idat, true);
          else
            write_vector_ascii(idat, opts.idat, 3);

          break;
        }

        case 1:
        case 3:
        case 4:
        {
          int dpn = 0;
          if     (data_idx == 1) dpn = 1;
          else if(data_idx == 3) dpn = 3;
          else                   dpn = 9;

          bool nodal = false;

          if( size_t(igb.v_x) == (imesh.xyz.size() / 3) ) {
            nodal = true;
            igb_out.v_x = omesh.xyz.size() / 3;
          } else if ( size_t(igb.v_x) == imesh.e2n_cnt.size()) {
            nodal = false;
            igb_out.v_x = omesh.e2n_cnt.size();
          } else {
            fprintf(stderr, "%s error: data doesnt match elements or nodes! Aborting!\n", __func__);
            exit(EXIT_FAILURE);
          }

          // set new spatial size and write output igb header
          write_igb_header(igb_out);

          mt_vector<mt_idx_t> idx;
          find_extrapolation_index(omesh, imesh, nodal, idx);
          odat.resize(idx.size()*dpn);

          printf("Processing igb time-slices: %s to %s: \n", opts.idat.c_str(), opts.odat.c_str());

          for(int t=0; t<igb.v_t; t++) {
            printf("\rcurrent time-slice %d / %d .. ", t+1, int(igb.v_t));
            fflush(stdout);

            read_igb_slice(idat, igb);

            for(size_t i=0, k=0; i < idx.size(); i++) {
              for(int j=0; j<dpn; j++, k++) {
                odat[k] = idat[idx[i]*dpn+j];
              }
            }

            write_igb_slice(odat, igb_out);
          }
          printf("\n");

          fclose(igb.fileptr);
          fclose(igb_out.fileptr);
          break;
        }
        default: {
          std::cout << "Data type not implementd!" << std::endl;
        } break;
      }

      break;
    }
#endif

    case ITP_IMP_STATE:
    {
      ep_state inp_state, outp_state;
      bool debug = false;

      std::cout << "Reading input state " << opts.idat << std::endl;
      gettimeofday(&t1, NULL);
      bool success = read_EP_state(inp_state, opts.idat);
      if(!success) exit(EXIT_FAILURE);
      gettimeofday(&t2, NULL);
      std::cout << "Done in " << (float)timediff_sec(t1, t2) << " sec" << std::endl;

      std::cout << "Reading input mesh: " << opts.imsh.base << std::endl;
      gettimeofday(&t1, NULL);
      read_mesh_selected(imesh, opts.imsh.format, opts.imsh.base, CRP_READ_ELEM | CRP_READ_PTS);
      compute_full_mesh_connectivity(imesh, opts.imsh.base);
      gettimeofday(&t2, NULL);
      std::cout << "Done in " << (float)timediff_sec(t1, t2) << " sec" << std::endl;

      if(inp_state.saved_nnodes != int(imesh.xyz.size() / 3)) {
        fprintf(stderr, "error: input state number of nodes (%d) and input mesh number of nodes (%d) do not match! Aborting!\n",
                inp_state.saved_nnodes, int(imesh.xyz.size() / 3));
        exit(EXIT_FAILURE);
      }

      std::cout << "Compute correspondance .." << std::endl;
      gettimeofday(&t1, NULL);
      mt_vector<mt_idx_t> corr;
      mt_vector<mt_real> corr_dist;
      compute_correspondance(omesh, imesh, corr, corr_dist);
      gettimeofday(&t2, NULL);
      std::cout << "Done in " << (float)timediff_sec(t1, t2) << " sec" << std::endl;

      std::cout << "Transferring state data .." << std::endl;
      gettimeofday(&t1, NULL);
      // state data that is node-independent can be copied over
      outp_state.format    = inp_state.format;
      outp_state.version   = inp_state.version;
      outp_state.save_date = inp_state.save_date;
      outp_state.time      = inp_state.time;
      outp_state.miif_id   = inp_state.miif_id;
      outp_state.imps      = inp_state.imps;
      outp_state.imp_sizes = inp_state.imp_sizes;
      outp_state.gdata_names = inp_state.gdata_names;

      outp_state.saved_nnodes = omesh.xyz.size() / 3;
      outp_state.gdata.resize(inp_state.gdata.size());

      // transfer global data, maybe we can interpolate here
      for(size_t g=0; g<outp_state.gdata.size(); g++) {
        outp_state.gdata[g].resize(outp_state.saved_nnodes);
        for(int i=0; i<outp_state.saved_nnodes; i++) {
          outp_state.gdata[g][i] = inp_state.gdata[g][corr[i]];
        }

        if(debug) {
          std::string outname = "debug_" + outp_state.gdata_names[g] + ".dat";
          write_vector_ascii(outp_state.gdata[g], outname);
        }
      }

      // the imp mask has to be based on the correspondance since we dont want
      // intermediate (interpolated) values
      outp_state.nodal_imp_mask.resize(outp_state.saved_nnodes);
      for(int i=0; i<outp_state.saved_nnodes; i++)
        outp_state.nodal_imp_mask[i] = inp_state.nodal_imp_mask[corr[i]];

      // compute imp offsets based on imp mask
      outp_state.nodal_imp_offset.resize(outp_state.saved_nnodes+1, 0);
      for(int i=1; i<=outp_state.saved_nnodes; i++)
        outp_state.nodal_imp_offset[i] = outp_state.nodal_imp_offset[i-1] +
                               outp_state.imp_sizes[(int)outp_state.nodal_imp_mask[i-1]];

      size_t data_size = outp_state.nodal_imp_offset[outp_state.saved_nnodes];
      outp_state.nodal_imp_data.resize(data_size);

      for(int widx = 0; widx < outp_state.saved_nnodes; widx++) {
        int ridx = corr[widx];
        int imp_size = outp_state.imp_sizes[(int)outp_state.nodal_imp_mask[widx]];
        memcpy(outp_state.nodal_imp_data.data() + outp_state.nodal_imp_offset[widx],
               inp_state .nodal_imp_data.data() + inp_state .nodal_imp_offset[ridx], imp_size);
      }
      gettimeofday(&t2, NULL);
      std::cout << "Done in " << (float)timediff_sec(t1, t2) << " sec" << std::endl;

      std::cout << "Writing output state " << opts.odat << std::endl;
      gettimeofday(&t1, NULL);
      write_EP_state(outp_state, opts.odat);
      gettimeofday(&t2, NULL);
      std::cout << "Done in " << (float)timediff_sec(t1, t2) << " sec" << std::endl;

      break;
    }

    default: break;
  }


}

