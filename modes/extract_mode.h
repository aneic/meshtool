#ifndef MT_EXTRACT_MODE_H_
#define MT_EXTRACT_MODE_H_

#include "mt_modes.h"

typedef mt_vector<MT_MAP<mt_triple<mt_idx_t>, tri_sele>> tri_surf_vec_t;
typedef mt_vector<MT_MAP<mt_quadruple<mt_idx_t>, quad_sele>> quad_surf_vec_t;

void tokenize_operstr(std::string& operstr,
                      const char token_indicator,
                      mt_vector<std::string>& file_names);

/**
 * @brief Extract the tag-sets for the different surface extraction operations.
 *
 * @param [in]  operstr  The string defining the operations.
 * @param [out] operflg  The encoded operation types.
 * @param [out] tagsA    The lhs tags associated with each operation.
 * @param [out] tagsB    The rhs tags associated with each operation.
 */
void extract_surf_fill_taglists(const std::string& operstr,
                                std::string& operflg,
                                mt_vector<mt_vector<std::string>>& surfA,
                                mt_vector<mt_vector<std::string>>& surfB);

void detokenize_surfaces(mt_vector<mt_vector<std::string>>& surf,
                         const char token_indicator,
                         const mt_vector<std::string>& file_names);

/**
 * @brief tag-based extraction of a sub-mesh from a mesh
 *
 * @param[in]  mesh      source mesh to extract the sub-mesh from
 * @param[in]  tags      element tags defining the sub-mesh
 * @param[out] nod       node map from mesh to sub-mesh
 * @param[out] eidx      element map from mesh to sub-mesh
 *
 * @return  0 on succes,
 *         -1 if no element is in the sub-mesh,
 *         -2 if all elements are in the sub-mesh
 */
int extract_mesh_mode(mt_meshdata& mesh, const std::set<mt_tag_t>& tags,
                      mt_vector<mt_idx_t>& nod,
                      mt_vector<mt_idx_t>& eidx);

/**
 * @brief extract data from mesh based on submesh. Either provide mesh and submesh files in file and submsh_dat_file or
 * provide idat and indices
 *
 * @param[in]  data_idx     data type index (See setup_data_format() function header for details)
 * @param[in]  idat         vector of input data
 * @param[out] odat         vetor or output data
 * @param[in]  indices      indices of nodes to keep for submesh from main mesh
 * @param[in]  igb_msh      mesh to read igb (i.e. time step) data for main mesh from if data_idx is 1 or 3
 * @param[out] igb_submsh   mesh to write extracted igb (i.e. time step) data to if data_idx is 1 or 3
 */

void extract_data_mode(const short data_idx, mt_vector<mt_real>& idat,
                       mt_vector<mt_real>& odata, const mt_vector<mt_idx_t>& indices,
                       igb_header & igb_msh, igb_header & igb_submsh);

/**
 * @brief extract a sequence of surfaces defined by set operations on element tags
 *
 * @param [in] edge_thr           edge threshold value
 * @param [in] ang_thr            angle threshold value
 * @param [in]     coords             vector of strings representing coordinates
 * @param [in]     mesh               input mesh data
 * @param [in]     oper            string indicating the operations
 * @param [in]     hybrid_output      boolean flag indicating whether hybrid output is enabled
 * @param [in]     rad                pointer to the radius value, if nullptr is provided, no radius is used
 * @param [in]     lower_rad          pointer to the lower radius value, if nullptr is provided, no lower radius is used
 * @param [out]    surf_mesh          vector of surface mesh data
 * @param [out]    nbc                vector of NBC data
 */

int extract_surface_mode(const float edge_thr, const float ang_thr,
                         const mt_vector<std::string>& coords,
                         const mt_meshdata& mesh, std::string oper,
                         const bool hybrid_output, const mt_real rad,
                         const mt_real lower_rad, mt_vector<mt_meshdata>& surf_mesh,
                         mt_vector<nbc_data>& nbc);

/**
 * @brief function calculating and writing mesh tags (basically calculating full mesh connectivity)
 * @param [in, out] mesh mesh to find the tags for
 * @param [out] tags vector storing all tags of mesh
 */
void extract_tags_mode(mt_meshdata& mesh, mt_vector<mt_tag_t>& tags);

/**
 * @brief write tags to tag file
 * @param [in] tags list of tags to write to file
 * @param [in] odat filename for tags file
 */
void extract_tags_mode_write(const mt_vector<mt_tag_t>& tags, std::string odat);

// data_idx may be:
// 0 : scalar dat file
// 1 : scalar igb file
// 2 : vec file
// 3 : vec3 igb file

/**
 * @brief compute gradient and gradient magnitude of a scalar function on a mesh.
 *
 * @param[in]  mesh           input mesh
 * @param[in]  idat           scalar input data
 * @param[out] mag            magnitude of gradient vectors
 * @param[out] grad           gradient vectors
 * @param[in]  nodal_input    whether `idat` is defined on the points (true) or on the elements (false)
 * @param[in]  nodal_output   whether `odat` is defined on the points (true) or on the elements (false)
 */
void extract_gradient_mode(const mt_meshdata& mesh,
                           const mt_vector<mt_real>& idat,
                           mt_vector<mt_real>& mag,
                           mt_vector<vec3r>& grad,
                           const bool nodal_input,
                           const bool nodal_output);

/**
 * @brief compute gradient and gradient magnitude of a scalar function on a mesh.
 *
 * @param[in]  mesh           input mesh
 * @param[in]  iigb           scalar igb input data
 * @param[out] oigb           magnitude of the gradient vectors
 * @param[out] oigb_vec       the gradient vectors
 * @param[in]  nodal_input    whether `idat` is defined on the points (true) or on the elements (false)
 * @param[in]  nodal_output   whether `odat` is defined on the points (true) or on the elements (false)
 */
void extract_gradient_mode(const mt_meshdata& mesh, igb_header& iigb,
                           igb_header* oigb, igb_header* oigb_vec,
                           const bool nodal_input, const bool nodal_output);

/**
 * @brief the myocardium is extracted from a given mesh. The myocard is identified based on non-zero fibers.
 *
 * @param[in]  mesh       source mesh to extract the myocard from
 * @param[out] nod        node map of myocard
 * @param[out] eidx       element map of myocard
 * @paraim[in] fib_thr    fiber threshold value, optional, default=0.0
 *
 * @return 0 on success, !0 otherwise
 */
int extract_myocard_mode(mt_meshdata& mesh, mt_vector<mt_idx_t>& nod,
                         mt_vector<mt_idx_t>& eidx, const mt_real fib_thr=0.0);

/**
 * @brief extract connected halo vertex sets for a given block vertex set.
 *
 * @param[in]  mesh             source mesh to extract the halo vertices from
 * @param[in] block_set         vector of vertex indices that are not contained in the halo
 * @param[out] halo             set of halo vertices
 * @param[out] halo_components  set of disconnected halo components
 */
void extract_vtxhalo_mode(const mt_meshdata& mesh,
                          const MT_USET<mt_idx_t>& block_set,
                          MT_USET<mt_idx_t>& halo,
                          mt_vector<mt_vector<mt_idx_t>>& halo_components);

/**
 * @brief extract elements inside a given box volume
 *
 * @param[in, out]  mesh      source mesh to extract the volume from
 * @param[in]       ref       reference point
 * @param[in]       ref_delta reference delta
 * @param[out]      nod       node indices of volume
 * @param[out]      eidx      element indices of volume
 */
void extract_volume_mode(mt_meshdata& mesh, const vec3r& ref,
                         const vec3r& ref_delta, mt_vector<mt_idx_t>& nod,
                         mt_vector<mt_idx_t>& eidx);

void extract_overlap_mode(const mt_meshdata& mesh1, const mt_meshdata& mesh2,
                          mt_real radius, mt_vector<bool>& keep);

/**
 * @brief extract fibers or sheets from mesh into out
 *
 * @param[in]  mesh      source mesh to extract the fibers from
 * @param[in]  mode      1 = only fibers, 2 = only sheets, 0 = both (if available)
 * @param[out] fibers    vector of extracted fibers
 * @param[out] sheers    extract sheets
 *
 * @return number of directions extracted
 */
int extract_fibers_mode(const mt_meshdata& mesh, const int mode,
                        mt_vector<mt_real>& fibers, mt_vector<mt_real>& sheets);

#endif
