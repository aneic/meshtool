/**
* @file resample_mode.h
* @brief Refine mesh elements
* @author Aurel Neic
* @version
* @date 2017-02-13
*/

#include "mt_modes_base.h"

#ifdef MT_ADDONS
#include "addons_utils.h"
#include "ng_interface.h"
#endif

/**
* @brief Enum encoding the resample mode.
*/
enum RES_MODE {RES_PURK, RES_MESH, RES_SURF, RES_UNI, RES_DAT};

static const std::string pkje_in_par   = "-ips=";
static const std::string pkje_out_par  = "-ops=";
static const std::string surf_corr_par = "-surf_corr=";
static const std::string postsmth_par  = "-postsmth=";
static const std::string uni_par       = "-uniform=";
static const std::string conv_par      = "-conv=";

/**
* @brief The resample mode options struct.
*/
struct resample_options{
  enum RES_MODE mode;
  std::string pkje_in;
  std::string pkje_out;
  std::string msh_base;
  std::string outmsh_base;
  std::string idat;
  std::string ifmt;
  std::string ofmt;
  std::string size;
  std::string min;
  std::string max;
  std::string avrg;
  std::string angl;
  std::string tags;
  std::string surf_corr;
  std::string fix;
  std::string postsmth;
  std::string unif;
  std::string conv;
  std::string iter;

  bool use_netgen = false;
};

#define CORR_DFLT 0.95

/**
* @brief The resample purkinje help message.
*/
void print_resample_purkinje_help()
{
  fprintf(stderr, "resample purkinje: resample purkinje cables as close as possible to a segment size\n");
  fprintf(stderr, "parameters:\n");
  fprintf(stderr, "%s<path>\t (input) Input Purkinje System file.\n", pkje_in_par.c_str());
  fprintf(stderr, "%s<float>\t (input) segment size to resample towards\n", size_par.c_str());
  fprintf(stderr, "%s<path>\t (output) Output Purkinje System file.\n", pkje_out_par.c_str());
  fprintf(stderr, "\n");
}
void print_resample_mesh_help()
{
  fprintf(stderr, "resample mesh: resample a tetrahedral mesh to fit a given edge size range\n");
  fprintf(stderr, "parameters:\n");
  fprintf(stderr, "%s<path>\t\t (input) path to basename of the input mesh\n", mesh_par.c_str());
  fprintf(stderr, "%s<float>\t\t (optional) min edge size\n", min_par.c_str());
  fprintf(stderr, "%s<float>\t\t (optional) max edge size\n", max_par.c_str());
  fprintf(stderr, "%s<float>\t\t (optional) average edge size. (min, max) are derived from it.\n", avrg_par.c_str());
  fprintf(stderr, "%s<float>\t\t (optional) sharp edge angle. used when smoothing. Default is 30.0.\n", angle_par.c_str());
  fprintf(stderr, "%s<path>\t\t (output) path to basename of the output mesh\n", outmesh_par.c_str());
  fprintf(stderr, "%s<float>\t (optional) surface correlation parameter (default %.2f).\n"
                  "\t\t\t    Edges connecting surface nodes with surface normals correlating greater\n"
                  "\t\t\t    than the specified amount can be collapsed.\n", surf_corr_par.c_str(), CORR_DFLT);
  fprintf(stderr, "%s<int>\t\t (optional) Apply conservative smoothing between resampling iterations.\n"
                  "\t\t\t    0 = no, 1 = yes. Default is 1.\n", postsmth_par.c_str());
  fprintf(stderr, "%s<int>\t\t (optional) Edge-refinement is applied uniformly on selected tags.\n"
                  "\t\t\t    0 = no, 1 = yes. Default is 0.\n", uni_par.c_str());
  fprintf(stderr, "%s<int>\t\t (optional) Fix boundary of mesh. 0 = no, 1 = yes. Default is 0.\n",
                  fix_par.c_str());
  fprintf(stderr, "%s<int>\t\t (optional) Iterate until fully converged. 0 = no, 1 = yes. Default is 0.\n",
                  conv_par.c_str());
  fprintf(stderr, "%s<tag lists>\t (optional) element tag lists specifying the regions\n"
                  "\t\t\t    to perform resampling on\n", tags_par.c_str());
  fprintf(stderr, "%s<path>\t\t (optional) format of the input mesh\n", inp_format_par.c_str());
  fprintf(stderr, "%s<path>\t\t (optional) format of the output mesh\n\n", out_format_par.c_str());
  fprintf(stderr, "The supported input formats are:\n%s\n", input_formats.c_str());
  fprintf(stderr, "The supported output formats are:\n%s\n", output_formats.c_str());
  fprintf(stderr, "\n");
  fprintf(stderr, "The tag lists have the following syntax: multiple lists are seperated by a \"/\",\n"
                  "while the tags in one list are seperated by a \",\" character. The surface smoothness\n"
                  "of the submesh formed by one tag list will be preserved.\n");
  fprintf(stderr, "\n");
}
void print_resample_surfmesh_help()
{
  fprintf(stderr, "resample surfmesh: resample a triangle mesh to fit a given edge size range\n");
  fprintf(stderr, "parameters:\n");
  fprintf(stderr, "%s<path>\t\t (input) path to basename of the input mesh\n", mesh_par.c_str());
  fprintf(stderr, "%s<float>\t\t (optional) min edge size\n", min_par.c_str());
  fprintf(stderr, "%s<float>\t\t (optional) max edge size\n", max_par.c_str());
  fprintf(stderr, "%s<float>\t\t (optional) average edge size. (min, max) are derived from it.\n", avrg_par.c_str());
  fprintf(stderr, "%s<float>\t\t (optional) sharp edge angle. used when smoothing. Default is 30.0.\n", angle_par.c_str());
  fprintf(stderr, "%s<path>\t\t (output) path to basename of the output mesh\n", outmesh_par.c_str());
  fprintf(stderr, "%s<float>\t (optional) surface correlation parameter (default %.2f).\n"
                  "\t\t\t    Edges connecting surface nodes with surface normals correlating greater\n"
                  "\t\t\t    than the specified amount can be collapsed.\n", surf_corr_par.c_str(), CORR_DFLT);
#ifdef MT_ADDONS
  fprintf(stderr, "%s\t\t\t (optional) If set, Netgen will be used for surface resampling.\n", netgen_par.c_str());
#endif
  fprintf(stderr, "%s<int>\t\t (optional) Fix boundary of non-closed surfaces. 0 = no, 1 = yes. Default is 1.\n",
                  fix_par.c_str());
  fprintf(stderr, "%s<int>\t\t (optional) Apply conservative post-resampling smoothing.\n"
                  "\t\t\t    0 = no, 1 = yes. Default is 1.\n", postsmth_par.c_str());
  fprintf(stderr, "%s<int>\t\t (optional) Edge-refinement is applied uniformly on selected tags.\n"
                  "\t\t\t    0 = no, 1 = yes. Default is 0.\n", uni_par.c_str());
  fprintf(stderr, "%s<format>\t\t (optional) format of the input mesh\n", inp_format_par.c_str());
  fprintf(stderr, "%s<format>\t\t (optional) format of the output mesh\n", out_format_par.c_str());
  fprintf(stderr, "%s<tag lists>\t (optional) element tag lists specifying the regions\n"
                  "\t\t\t    to perform resampling on\n\n", tags_par.c_str());
  fprintf(stderr, "The supported input formats are:\n%s\n", input_formats.c_str());
  fprintf(stderr, "The supported output formats are:\n%s\n", output_formats.c_str());
  fprintf(stderr, "\n");
  fprintf(stderr, "The tag lists have the following syntax: multiple lists are seperated by a \"/\",\n"
                  "while the tags in one list are seperated by a \",\" character. The surface smoothness\n"
                  "of the submesh formed by one tag list will be preserved.\n");
  fprintf(stderr, "\n");
}

void print_resample_uniform_help()
{
  fprintf(stderr, "resample uniform: uniformly refine a tri surf or tet volume\n");
  fprintf(stderr, "parameters:\n");
  fprintf(stderr, "%s<path>\t\t (input) path to basename of the input mesh\n", mesh_par.c_str());
  fprintf(stderr, "%s<int>\t\t (input) number of uniform refinement steps\n", iter_par.c_str());
  fprintf(stderr, "%s<path>\t\t (output) path to basename of the output mesh\n", outmesh_par.c_str());
  fprintf(stderr, "%s<format>\t\t (optional) format of the input mesh\n", inp_format_par.c_str());
  fprintf(stderr, "%s<format>\t\t (optional) format of the output mesh\n", out_format_par.c_str());
  fprintf(stderr, "\n");
}

#ifdef MT_ADDONS
void print_resample_databased_help()
{
  fprintf(stderr, "resample databased: resample a tetrahedral mesh to fit a given edge size range\n");
  fprintf(stderr, "parameters:\n");
  fprintf(stderr, "%s<path>\t\t (input) path to basename of the input mesh\n", mesh_par.c_str());
  fprintf(stderr, "%s<path>\t\t (input) path to element scalar field used from refining\n", idat_par.c_str());
  fprintf(stderr, "%s<float>\t\t (input) percentile we use to refine, in range [0, 100].\n", max_par.c_str());
  fprintf(stderr, "%s<path>\t\t (output) path to basename of the output mesh\n", outmesh_par.c_str());
  fprintf(stderr, "%s<path>\t\t (optional) format of the input mesh\n", inp_format_par.c_str());
  fprintf(stderr, "%s<path>\t\t (optional) format of the output mesh\n\n", out_format_par.c_str());
  fprintf(stderr, "The supported input formats are:\n%s\n", input_formats.c_str());
  fprintf(stderr, "The supported output formats are:\n%s\n", output_formats.c_str());
  fprintf(stderr, "\n");
}
#endif

/**
* @brief Refine mode options parsing
*/
int resample_parse_options(int argc, char** argv, struct resample_options & opts)
{
  if(argc < 3) {
    std::cerr << "Please choose one of the following resample modes: " << std::endl << std::endl;
    print_resample_purkinje_help();
    print_resample_mesh_help();
    print_resample_surfmesh_help();
    print_resample_uniform_help();
#ifdef MT_ADDONS
    print_resample_databased_help();
#endif
    return 1;
  }

  std::string resample_type = argv[2];

  // parse all resample parameters -----------------------------------------------------------------
  for(int i=3; i<argc; i++)
  {
    std::string param = reformat_param(argv, i, argc);
    bool match = false;

    if(!match) match = parse_param(param, pkje_in_par, opts.pkje_in);
    if(!match) match = parse_param(param, pkje_out_par, opts.pkje_out);
    if(!match) match = parse_param(param, mesh_par, opts.msh_base);
    if(!match) match = parse_param(param, outmesh_par, opts.outmsh_base);
    if(!match) match = parse_param(param, idat_par, opts.idat);
    if(!match) match = parse_param(param, inp_format_par, opts.ifmt);
    if(!match) match = parse_param(param, out_format_par, opts.ofmt);
    if(!match) match = parse_param(param, size_par, opts.size);
    if(!match) match = parse_param(param, min_par, opts.min);
    if(!match) match = parse_param(param, max_par, opts.max);
    if(!match) match = parse_param(param, avrg_par, opts.avrg);
    if(!match) match = parse_param(param, angle_par, opts.angl);
    if(!match) match = parse_param(param, tags_par, opts.tags);
    if(!match) match = parse_param(param, surf_corr_par, opts.surf_corr);
    if(!match) match = parse_param(param, fix_par, opts.fix);
    if(!match) match = parse_param(param, postsmth_par, opts.postsmth);
    if(!match) match = parse_param(param, uni_par, opts.unif);
    if(!match) match = parse_param(param, conv_par, opts.conv);
    if(!match) match = parse_param(param, iter_par, opts.iter);
    if(!match) match = parse_flag (param, netgen_par, opts.use_netgen);

    fixBasename(opts.msh_base);
    fixBasename(opts.outmsh_base);

    if(!match) {
      std::cerr << "Error: Cannot parse parameter " << param << std::endl;
      return 3;
    }
  }

  if(resample_type.compare("purkinje") == 0)
  {
    opts.mode = RES_PURK;
    // check if all relevant parameters have been set ---------------------------------------------------
    bool inok = opts.pkje_in.size() > 0, outok = opts.pkje_out.size() > 0,
         throk = opts.size.size() > 0;

    if( !(inok && outok && throk) )
    {
      std::cerr << "Error: Insufficient parameters provided." << std::endl;
      print_resample_purkinje_help();
      return 2;
    }
  }
  else if(resample_type.compare("mesh") == 0)
  {
    opts.mode = RES_MESH;

    bool inok = opts.msh_base.size() > 0, outok = opts.outmsh_base.size() > 0;

    if( !(inok && outok) )
    {
      std::cerr << "Error: Insufficient parameters provided." << std::endl;
      print_resample_mesh_help();
      return 2;
    }
  }
  else if(resample_type.compare("surfmesh") == 0)
  {
    opts.mode = RES_SURF;

    bool inok = opts.msh_base.size() > 0, outok = opts.outmsh_base.size() > 0;

    if( !(inok && outok) )
    {
      std::cerr << "Error: Insufficient parameters provided." << std::endl;
      print_resample_surfmesh_help();
      return 2;
    }
  }
  else if(resample_type.compare("uniform") == 0)
  {
    opts.mode = RES_UNI;

    bool inok = opts.msh_base.size() > 0, outok = opts.outmsh_base.size() > 0;

    if( !(inok && outok && opts.iter.size() > 0) )
    {
      std::cerr << "Error: Insufficient parameters provided." << std::endl;
      print_resample_uniform_help();
      return 2;
    }
  }
#ifdef MT_ADDONS
  else if(resample_type.compare("databased") == 0)
  {
    opts.mode = RES_DAT;

    bool inok = opts.msh_base.size() > 0, outok = opts.outmsh_base.size() > 0;

    if( !(inok && outok && opts.idat.size() && opts.max.size()) )
    {
      std::cerr << "Error: Insufficient parameters provided." << std::endl;
      print_resample_databased_help();
      return 2;
    }
  }
#endif
  else {
    print_usage(argv[0]);
    return 4;
  }

  return 0;
}

static void preprocess_surf_tags(const mt_meshdata & mesh,
                          const std::string & oper_string,
                          mt_vector<MT_USET<mt_tag_t>> & stags)
{
  // pre-process operations
  mt_vector<std::string> oper;
  if(oper_string.size() > 0)
    split_string(oper_string, '/' , oper);
  else {
    oper.resize(1);
    oper[0] = "*";
  }

  MT_USET<mt_tag_t> meshtags;
  meshtags.insert(mesh.etags.begin(), mesh.etags.end());

  stags.resize(oper.size());
  // extract tags of all operations
  for(int oidx = 0; oidx < (int)oper.size(); oidx++)
  {
    mt_vector<std::string> tags;
    // either read in from user input or add all tags
    if(oper[oidx].compare("*") != 0) {
      split_string(oper[oidx], ',' , tags);
      for(const std::string & s : tags) {
        mt_tag_t curtag = atoi(s.c_str());
        if(meshtags.count(curtag))
          stags[oidx].insert(curtag);
        else {
          fprintf(stderr, "Warning: Region tag %d does not exist in the mesh! Aborting!\n", (int) curtag);
          exit(EXIT_FAILURE);
        }
      }
    }
    else
      stags[oidx] = meshtags;

    stags[oidx].sort();
  }
}

void preprocess_min_max(const std::string & minstr, const std::string & maxstr,
                        std::string avrgstr, const float avrg_est,
                        float & min, float & max)
{
  min = -1.0;
  max = -1.0;

  // if neither of min, max, avrg are specified, we go for an average that is equal
  // to the current average
  if((minstr.size() + maxstr.size() + avrgstr.size()) == 0) {
    avrgstr = std::to_string(avrg_est);
  }

  if(avrgstr.size()) {
    float avrg_req = atof(avrgstr.c_str());

    if(avrg_est * 0.5f > avrg_req) {
      // the requested edge length is significantly smaller than the current one
      // thus, we will be mainly refining
      min = 0.5f * avrg_req, max = 1.8f * avrg_req;
    }
    else if(avrg_est * 1.5f < avrg_req) {
      // coarsening will play a significant role. Since it has lesser impact on the
      // average, we move it closer to it.
      min = 0.75f * avrg_req, max = 1.8f * avrg_req;
    }
    else {
      min = 0.65f * avrg_req, max = 1.7f * avrg_req;
    }

    printf("Selected average edge length %.2f. Setting min = %.2f, max = %.2f.\n",
           avrg_req, min, max);
    min *= min;
    max *= max;
  }
  else {
    if(minstr.size()) {
      min = atof(minstr.c_str());
      min *= min;
    }
    if(maxstr.size()) {
      max = atof(maxstr.c_str());
      max *= max;
    }
  }
}

void resample_purk_mode(const resample_options & opts)
{
  struct timeval t1, t2;
  struct mt_psdata ps;

  std::cout << "Reading purkinje file: " << opts.pkje_in << std::endl;
  gettimeofday(&t1, NULL);
  read_purkinje(ps, opts.pkje_in);
  gettimeofday(&t2, NULL);
  std::cout << "Done in " << (float)timediff_sec(t1, t2) << " sec" << std::endl;

  float size = atof(opts.size.c_str());
  std::cout << "Refining PS .. " << std::endl;
  gettimeofday(&t1, NULL);

  mt_vector<int> cables;
  resample_purkinje(ps, cables, size);

  gettimeofday(&t2, NULL);
  std::cout << "Done in " << (float)timediff_sec(t1, t2) << " sec" << std::endl;

  std::cout << "Writing purkinje file: " << opts.pkje_out << std::endl;
  gettimeofday(&t1, NULL);
  write_purkinje(ps, opts.pkje_out);
  gettimeofday(&t2, NULL);
  std::cout << "Done in " << (float)timediff_sec(t1, t2) << " sec" << std::endl;
}

void resample_mesh_mode(resample_options & opts)
{
  struct timeval t1, t2;

  bool fix_bnd   = opts.fix.size() ? atoi(opts.fix.c_str()) : 0;
  bool unif_splt = opts.unif.size() ? atoi(opts.unif.c_str()) : 0;
  bool do_conv   = opts.conv.size() ? atoi(opts.conv.c_str()) : 0;
  mt_real scorr  = opts.surf_corr.size() > 0 ? atof(opts.surf_corr.c_str()) : CORR_DFLT;

  if(fix_bnd && unif_splt) {
    fprintf(stderr, "Warning: Boundary fixing and uniform refinement exclude each other!\n"
        "Turning off uniform refinement!\n");
    unif_splt = false;
  }

  if(unif_splt && opts.tags.size()) {
    fprintf(stderr, "Warning: Uniform refinement currently does not support local refinement! Ignoring tags!\n");
    opts.tags = "";
  }

  mt_meshdata mesh;
  mt_filename msh(opts.msh_base, opts.ifmt);
  std::cout << "Reading mesh: " << msh.base << std::endl;
  gettimeofday(&t1, NULL);
  read_mesh_selected(mesh, msh.format, msh.base);
  compute_full_mesh_connectivity(mesh, msh.base);
  gettimeofday(&t2, NULL);
  std::cout << "Done in " << (float)timediff_sec(t1, t2) << " sec" << std::endl;

  float min, max;
  float avrg_est = avrg_edgelength_estimate(mesh);
  preprocess_min_max(opts.min, opts.max, opts.avrg, avrg_est, min, max);

  // make sure this is a tet mesh
  for(size_t i=0; i<mesh.etype.size(); i++)
    if(mesh.etype[i] != Tetra) {
      std::cerr << "Error: Mesh resampling implemented only for tetrahedral meshes!" << std::endl;
      exit(1);
    }

  // pre-process operations
  mt_vector<MT_USET<mt_tag_t>> stags;
  preprocess_surf_tags(mesh, opts.tags, stags);

  bool dosmooth = opts.postsmth.size() ? atoi(opts.postsmth.c_str()) : true;
  float smth_base = SMTH_BASE, edge_ang = 30.0f, iter_base = ITER_BASE;

  if(opts.angl.size()) edge_ang = atof(opts.angl.c_str());

  for(int oidx = 0; oidx < (int)stags.size(); oidx++)
  {
    // the set of tags that define the region we work on
    printf("Resampling op %d/%d: Processing tags: ", oidx+1, int(stags.size()));
    for(auto t : stags[oidx]) printf("%d ", int(t));
    printf("\n");

    resample_mesh(mesh, min, max, stags, stags[oidx], scorr, fix_bnd, do_conv, unif_splt, dosmooth, edge_ang);
  }

  // do some final smoothing
  if(dosmooth)
    volumetric_smooth_from_tags(mesh, stags, iter_base, smth_base, 1, edge_ang, RESAMPLE_QUAL_THR, false, true);

  // write final mesh
  mt_filename outmsh(opts.outmsh_base, opts.ofmt);
  std::cout << "Writing mesh: " << outmsh.base << std::endl;
  write_mesh_selected(mesh, outmsh.format, outmsh.base);
}

void resample_surf_mode(const resample_options & opts)
{
  struct timeval t1, t2;

  mt_meshdata mesh;
  mt_filename msh(opts.msh_base, opts.ifmt);
  std::cout << "Reading mesh: " << msh.base << std::endl;
  gettimeofday(&t1, NULL);
  read_mesh_selected(mesh, msh.format, msh.base);
  compute_full_mesh_connectivity(mesh);
  gettimeofday(&t2, NULL);
  std::cout << "Done in " << (float)timediff_sec(t1, t2) << " sec" << std::endl;

#ifdef MT_ADDONS
  if(opts.use_netgen) {
    if(opts.avrg.size() == 0) {
      fprintf(stderr, "Netgen meshing requires settings of -avrg. Aborting.\n");
      exit(EXIT_FAILURE);
    }

    mt_real avrg = atof(opts.avrg.c_str());

    std::cout << "Meshing with Netgen .. " << std::endl;
    gettimeofday(&t1, NULL);
    mt_meshdata outmesh;
    remesh_surface_with_netgen(mesh, outmesh, avrg);
    outmesh.lon.assign(outmesh.e2n_cnt.size() * 3, 0.0);
    compute_full_mesh_connectivity(outmesh, false);
    gettimeofday(&t2, NULL);
    std::cout << "Done in " << (float)timediff_sec(t1, t2) << " sec" << std::endl;

    std::cout << "Interpolating tags .. " << std::endl;
    gettimeofday(&t1, NULL);
    kdtree vtree(10);
    vtree.build_vertex_tree(mesh.xyz);
    elementwise_interpolation(mesh, outmesh, vtree, mesh.etags, outmesh.etags);
    gettimeofday(&t2, NULL);
    std::cout << "Done in " << (float)timediff_sec(t1, t2) << " sec" << std::endl;

    mt_filename outfile(opts.outmsh_base, opts.ofmt);
    std::cout << "Writing mesh: " << outfile.base << std::endl;
    write_mesh_selected(outmesh, outfile.format, outfile.base);
    exit(EXIT_SUCCESS);
  }
#endif

  float min, max;
  float avrg_est = avrg_edgelength_estimate(mesh);
  preprocess_min_max(opts.min, opts.max, opts.avrg, avrg_est, min, max);

  // make sure this is a tri mesh
  for(size_t i=0; i<mesh.etype.size(); i++)
    if(mesh.etype[i] != Tri) {
      std::cerr << "Error: Surface resampling implemented only for triangular meshes!" << std::endl;
      exit(1);
    }

  // pre-process operations
  mt_vector<MT_USET<mt_tag_t>> stags;
  preprocess_surf_tags(mesh, opts.tags, stags);

  bool dosmooth = opts.postsmth.size() ? atoi(opts.postsmth.c_str()) : true;
  bool fix_bnd = opts.fix.size() ? atoi(opts.fix.c_str()) : true;
  mt_real scorr = opts.surf_corr.size() > 0 ? atof(opts.surf_corr.c_str()) : CORR_DFLT;
  bool unif_splt = opts.unif.size() ? atoi(opts.unif.c_str()) : 0;

  float edge_ang = 30.0f;
  if(opts.angl.size()) edge_ang = atof(opts.angl.c_str());

  for(int oidx = 0; oidx < (int)stags.size(); oidx++)
  {
    // the set of tags that define the region we work on
    printf("Resampling op %d/%d: Processing tags: ", oidx+1, int(stags.size()));
    for(auto t : stags[oidx]) printf("%d ", int(t));
    printf("\n");

    resample_surface(mesh, min, max, stags[oidx], scorr, fix_bnd, unif_splt, dosmooth, edge_ang);
  }

  if(dosmooth) {
    // do some final smoothing
    mt_meshdata & surfmesh = mesh;
    mt_meshdata linemesh;
    // extract surface and line manifolds
    MT_USET<mt_idx_t> sm_vtx, ln_vtx;
    compute_line_interfaces(mesh, surfmesh, 0.25, true, linemesh);
    compute_full_mesh_connectivity(linemesh);

    sm_vtx.insert(mesh.e2n_con.begin(), mesh.e2n_con.end());
    ln_vtx.insert(linemesh.e2n_con.begin(), linemesh.e2n_con.end());
    // prepare data-struct for surface smoothing
    sm_vtx.sort(); ln_vtx.sort();
    for(auto n : ln_vtx) sm_vtx.erase(n);

    mt_vector<mt_idx_t> sm_nod;
    mt_mask isMnfld(mesh.xyz.size() / 3);

    sm_nod.assign(sm_vtx.begin(), sm_vtx.end());
    isMnfld.insert(linemesh.e2n_con.begin(), linemesh.e2n_con.end());

    // smooth
    smooth_nodes(mesh, linemesh, isMnfld, sm_nod, 100, 0.25);
  }

  // write final mesh
  mt_filename outmsh(opts.outmsh_base, opts.ofmt);
  std::cout << "Writing mesh: " << outmsh.base << std::endl;
  write_mesh_selected(mesh, outmsh.format, outmsh.base);
}

void resample_uniformly_mode(resample_options & opts)
{
  struct timeval t1, t2;

  mt_meshdata mesh;
  mt_filename msh(opts.msh_base, opts.ifmt);
  mt_filename outmsh(opts.outmsh_base, opts.ofmt);

  std::cout << "Reading mesh: " << msh.base << std::endl;
  gettimeofday(&t1, NULL);
  read_mesh_selected(mesh, msh.format, msh.base);
  compute_full_mesh_connectivity(mesh);
  gettimeofday(&t2, NULL);
  std::cout << "Done in " << (float)timediff_sec(t1, t2) << " sec" << std::endl;

  std::cout << "refining .." << std::endl;
  gettimeofday(&t1, NULL);
  int iter = atoi(opts.iter.c_str());
  for(int i=0; i<iter; i++) {
    printf("iter %d ..\n", i+1);
    refine_uniform(mesh);

    if(mesh.etype[0] == Tetra)
      correct_insideOut(mesh);
  }
  gettimeofday(&t2, NULL);
  std::cout << "Done in " << (float)timediff_sec(t1, t2) << " sec" << std::endl;

  write_mesh_selected(mesh, outmsh.format, outmsh.base);
}

#ifdef MT_ADDONS
void resample_databased_mode(resample_options & opts)
{
  struct timeval t1, t2;

  mt_meshdata mesh;
  mt_filename msh(opts.msh_base, opts.ifmt);
  mt_filename outmsh(opts.outmsh_base, opts.ofmt);

  std::cout << "Reading mesh: " << msh.base << std::endl;
  gettimeofday(&t1, NULL);
  read_mesh_selected(mesh, msh.format, msh.base);
  compute_full_mesh_connectivity(mesh);
  gettimeofday(&t2, NULL);
  std::cout << "Done in " << (float)timediff_sec(t1, t2) << " sec" << std::endl;

  std::cout << "reading field .." << std::endl;
  gettimeofday(&t1, NULL);
  mt_vector<mt_real> field;
  read_vector(field, opts.idat);

  bool data_nodal = true;
  if(field.size() == mesh.e2n_cnt.size()) {
    data_nodal = false;
  } else if (field.size() == (mesh.xyz.size()/3)) {
    data_nodal = true;
  } else {
    fprintf(stderr, "%s error: provided field does not match the numer of mesh elements! Aborting!\n",
            __func__);
    return;
  }

  gettimeofday(&t2, NULL);
  std::cout << "Done in " << (float)timediff_sec(t1, t2) << " sec" << std::endl;

  std::cout << "identifying elements to refine .." << std::endl;
  gettimeofday(&t1, NULL);

  float max = atof(opts.max.c_str()) / 100.0f;
  max = clamp<float>(max, 0, 1);

  mt_vector<mt_mixed_tuple<mt_real,mt_idx_t>> data_idx(field.size());
  for(size_t i=0; i<field.size(); i++)
    data_idx[i] = {field[i], mt_idx_t(i)};

  std::sort(data_idx.begin(), data_idx.end());

  MT_USET<mt_idx_t> sel;
  mt_idx_t start = data_idx.size() * max, end = data_idx.size();
  for(mt_idx_t i=start; i < end; i++)
    sel.insert(data_idx[i].v2);

  if(data_nodal == false) {
    MT_USET<mt_idx_t> sel2(sel);
    sel.clear();
    elemSet_to_nodeSet(mesh, sel2, sel);
  }

  printf("selected %d nodes..\n", (int)sel.size());

  gettimeofday(&t2, NULL);
  std::cout << "Done in " << (float)timediff_sec(t1, t2) << " sec" << std::endl;

  std::cout << "refining mesh .." << std::endl;
  gettimeofday(&t1, NULL);

  refine_uniform(mesh, sel);
  correct_insideOut(mesh);

  gettimeofday(&t2, NULL);
  std::cout << "Done in " << (float)timediff_sec(t1, t2) << " sec" << std::endl;

  write_mesh_selected(mesh, outmsh.format, outmsh.base);
}
#endif

/// resample mode function
int resample_mode(int argc, char** argv)
{
  struct resample_options opts;

  int ret = resample_parse_options(argc, argv, opts);
  if (ret != 0) return 1;

  switch(opts.mode)
  {
    case RES_PURK:
      resample_purk_mode(opts);
      break;

    case RES_MESH:
      resample_mesh_mode(opts);
      break;

    case RES_SURF:
      resample_surf_mode(opts);
      break;

    case RES_UNI:
      resample_uniformly_mode(opts);
      break;

#ifdef MT_ADDONS
    case RES_DAT:
      resample_databased_mode(opts);
      break;
#endif

    default: break;
  }
  return 0;
}
