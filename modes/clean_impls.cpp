#include "mt_modes_base.h"
#include <set>

#ifdef MT_ADDONS
#include "addons_utils.h"
#include "ng_interface.h"
#endif

#include "data_structs.h"

bool clean_topology_mode(mt_meshdata& mesh, const mt_real threshold) {

  std::cout << "Cleaning topology .. " << std::endl;

  struct timeval t1, t2;
  gettimeofday(&t1, NULL);

  size_t start_nodes = mesh.xyz.size() / 3;
  size_t start_elems = mesh.e2n_cnt.size();
  bool   have_flipped = false;

  if (threshold > 0.0)
    correct_duplicate_vertices(mesh, threshold, true);
  else
    correct_duplicate_vertices(mesh, true);

  correct_duplicate_elements(mesh);

  if (mesh_is_trisurf(mesh)) {
#ifndef MT_ADDONS
    mt_mapping<mt_idx_t> ele2edge;
    MT_MAP<mt_tuple<mt_idx_t>, mt_idx_t> edges;
    MT_USET<mt_tuple<mt_idx_t>> border_edges;

    compute_edges(mesh, ele2edge, edges);
    ele2edge.transpose();

    for (auto it = edges.begin(); it != edges.end(); ++it) {
      mt_idx_t edge_idx = it->second;
      if (ele2edge.bwd_cnt[edge_idx] == 1)
        border_edges.insert({it->first.v1, it->first.v2});
    }

    if (border_edges.size())
      correct_dangling_tri_elems(mesh, border_edges);

    have_flipped = apply_normal_orientation(mesh, true);
#else
        mt_real eps = avrg_edgelength_estimate(mesh, true) * 1e-3;
        have_flipped = clean_surface_topology(mesh, eps);
#endif
  }

  // Remove zero volume tets / tris
  mt_vector<bool> keep(mesh.e2n_cnt.size(), false);
  size_t empty = 0;

  for (size_t eidx = 0; eidx < mesh.e2n_cnt.size(); eidx++) {
    mt_idx_t estart = mesh.e2n_dsp[eidx];
    switch (mesh.etype[eidx]) {
    case Tetra:
    case Tri: {
      const mt_idx_t *con = mesh.e2n_con.data() + estart;
      mt_real tetvol = volume(mesh.etype[eidx], con, mesh.xyz.data());
      if (tetvol > 0.0) {
        keep[eidx] = true;
      } else
        empty++;
      break;
    }
    default:
      break;
    }
  }

  if (empty) {
    printf("Removing %zu zero-volume elements..\n", empty);
    mt_vector<mt_idx_t> elems, nod;
    restrict_meshdata(keep, mesh, elems, nod);
  }
  gettimeofday(&t2, NULL);
  std::cout << "Done in " << (float)timediff_sec(t1, t2) << " sec" << std::endl;

  return ((mesh.xyz.size() / 3 != start_nodes) || (mesh.e2n_cnt.size() != start_elems) || (have_flipped));
}
