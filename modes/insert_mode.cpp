/**
* @file insert_mode.h
* @brief Meshtool insert mode.
* @author Aurel Neic
* @version
* @date 2017-02-13
*/

#include "mt_modes_base.h"
#include "itk_utils.h"

#include "insert_mode.h"

#ifdef MT_ADDONS
#include "addons_utils.h"
#endif


static const std::string trsp_par="-trsp=";
static const std::string grow_par="-grow=";
static const std::string trsp_dist_par="-trsp_dist=";
static const std::string grad_par="-grad_thr=";
static const std::string con_par="-con_thr=";
static const std::string keep_ordering_par="-keep_ordering";
static const std::string keep_surf_par="-keep_surftags";


void print_insert_submesh_help()
{
  fprintf(stderr, "insert submesh: a submesh is inserted back into a mesh and written to an output mesh\n");
  fprintf(stderr, "parameters:\n");
  fprintf(stderr, "%s<path>\t (input) path to basename of the submesh to insert from\n", submesh_par.c_str());
  fprintf(stderr, "%s<path>\t (input) path to basename of the mesh to insert into\n", mesh_par.c_str());
  fprintf(stderr, "%s<format>\t (optional) mesh output format. may be: %s\n", out_format_par.c_str(), output_formats.c_str());
  fprintf(stderr, "%s<path>\t (output) path to basename of the output mesh\n", outmesh_par.c_str());
  fprintf(stderr, "\n");
  fprintf(stderr, "Note that the files defining the submesh must include a *.eidx and a *.nod file.\n"
                  "This files define how elements and nodes of the submesh map back into the original mesh.\n"
                  "The *.eidx and *.nod files are generated when using the \"extract mesh\" mode.\n");
  fprintf(stderr, "\n");
}
void print_insert_data_help()
{
  fprintf(stderr, "insert data: data defined on a submesh is inserted back into a mesh\n");
  fprintf(stderr, "parameters:\n");
  fprintf(stderr, "%s<path>\t\t (input) path to basename of the mesh\n", mesh_par.c_str());
  fprintf(stderr, "%s<path>\t\t (input) path to basename of the submesh\n", submesh_par.c_str());
  fprintf(stderr, "%s<path>\t (input) path to submesh data\n", submesh_data_par.c_str());
  fprintf(stderr, "%s<path>\t\t (output) path to output data\n", odat_par.c_str());
  fprintf(stderr, "%s<path>\t (optional) path to mesh data\n", mesh_data_par.c_str());
  fprintf(stderr, "%s<int>\t\t (optional) Data mode. 0 = nodal, 1 = element. Default is 0.\n", mode_par.c_str());
  fprintf(stderr, "%s<float>\t\t (optional) Default mesh data value (if no mesh data vector used). Default is 0.\n", val_par.c_str());
  fprintf(stderr, "\n");
  fprintf(stderr, "Note that the files defining the submesh must include a *.eidx and a *.nod file.\n"
                  "This files define how elements and nodes of the submesh map back into the original mesh.\n"
                  "The *.eidx and *.nod files are generated when using the \"extract mesh\" mode.\n");
  fprintf(stderr, "\n");
}
void print_insert_meshdata_help()
{
  fprintf(stderr, "insert meshdata: the fiber and tag data of a mesh is inserted into another mesh\n");
  fprintf(stderr, "                 based on vertex locations.\n");
  fprintf(stderr, "parameters:\n");
  fprintf(stderr, "%s<path>\t\t (input) path to basename of the mesh to insert from\n",
          inp_mesh_par.c_str());
  fprintf(stderr, "%s<path>\t\t (input) path to basename of the mesh to insert into\n",
          mesh_par.c_str());
  fprintf(stderr, "%s<path>\t\t (input) Operation index: 0 = only tags, 1 = only fibers, 2 = both\n",
          oper_par.c_str());
  fprintf(stderr, "%s<format>\t\t (optional) mesh input format. may be: %s\n",
          inp_format_par.c_str(), input_formats.c_str());
  fprintf(stderr, "%s<format>\t\t (optional) mesh output format. may be: %s\n",
          out_format_par.c_str(), output_formats.c_str());
  fprintf(stderr, "%s<vec-file>\t (optional) element-based transport gradient\n", trsp_par.c_str());
  fprintf(stderr, "%s<float>\t (optional) transport distance threshold\n", trsp_dist_par.c_str());
  fprintf(stderr, "%s<float>\t (optional) gradient correlation threshold. Must be in (0, 1). Default is %g.\n",
          grad_par.c_str(), GRAD_THR_DFLT);
  fprintf(stderr, "%s<float>\t (optional) Connectivity threshold. Required number of nodes an element of mesh1\n"
                   "\t\t\t needs to share with an element of mesh2 in order for the data to be inserted. Default is %d.\n",
          con_par.c_str(), CON_THR_DFLT);
  fprintf(stderr, "%s<path>\t\t (output) path to basename of the output mesh\n",
          outmesh_par.c_str());
  fprintf(stderr, "\n");
}

void print_insert_tags_help()
{
  fprintf(stderr, "insert tags: insert an element data vector as new mesh element tags.\n");
  fprintf(stderr, "parameters:\n");
  fprintf(stderr, "%s<path>\t (input) path to basename of the mesh.\n", mesh_par.c_str());
  fprintf(stderr, "%s<path>\t (input) .dat file the tags are inserted from.\n", idat_par.c_str());
  fprintf(stderr, "%s<path>\t (output) path to basename of the output mesh\n", outmesh_par.c_str());
  fprintf(stderr, "%s<format>\t (optional) mesh input format.\n", inp_format_par.c_str());
  fprintf(stderr, "%s<format>\t (optional) mesh output format.\n", out_format_par.c_str());
  fprintf(stderr, "The supported input formats are:\n%s\n", input_formats.c_str());
  fprintf(stderr, "The supported output formats are:\n%s\n", output_formats.c_str());
  fprintf(stderr, "\n");
}
void print_insert_imagetags_help()
{
  fprintf(stderr, "insert imagetags: insert the region tags from an image segmentation.\n");
  fprintf(stderr, "parameters:\n");
  fprintf(stderr, "%s<path>\t (input) path to basename of the mesh.\n", mesh_par.c_str());
  fprintf(stderr, "%s<path>\t (input) path to the image data.\n", idat_par.c_str());
  fprintf(stderr, "%s<tag1,tag2,..>\t(optional) image tag indices to consider.\n", tags_par.c_str());
  fprintf(stderr, "%s<path>\t (output) path to basename of the output mesh\n", outmesh_par.c_str());
  fprintf(stderr, "%s\t (optional) flag to skip relabeling on elements adjacent to the surface.\n", keep_surf_par.c_str());
  fprintf(stderr, "%s<format>\t (optional) mesh input format.\n", inp_format_par.c_str());
  fprintf(stderr, "%s<format>\t (optional) mesh output format.\n", out_format_par.c_str());
  fprintf(stderr, "The supported input formats are:\n%s\n", input_formats.c_str());
  fprintf(stderr, "The supported output formats are:\n%s\n", output_formats.c_str());
  fprintf(stderr, "\n");
}

void print_insert_tagsurfs_help()
{
  fprintf(stderr, "insert tagsurfs: insert element tags defined by closed surface regions.\n");
  fprintf(stderr, "parameters:\n");
  fprintf(stderr, "%s<path>\t\t(input) path to basename of the mesh.\n", mesh_par.c_str());
  fprintf(stderr, "%s<surf1,surf2,..>\t(input) list of surface meshes defining the regions to tag.\n",
                   surf_par.c_str());
  fprintf(stderr, "%s<path>\t\t(output) path to basename of the output mesh\n", outmesh_par.c_str());
  fprintf(stderr, "%s<tag1,tag2,..>\t(optional) tag indices for inserted regions. Should match order of surfaces.\n",
                   tags_par.c_str());
  fprintf(stderr, "%s<format>\t\t(optional) mesh input format.\n", inp_format_par.c_str());
  fprintf(stderr, "%s<format>\t\t(optional) mesh output format.\n", out_format_par.c_str());
  fprintf(stderr, "%s<mode>\t\t(optional) sampling mode {0 .. all nodes inside, 1 .. elem center inside, 2 .. one node inside, 3 .. triangle center close to surface}. Default is 1.\n", mode_par.c_str());
  fprintf(stderr, "%s<int>\t\t(optional) Layer of elements to grow regions when inserting. Default 0.\n", grow_par.c_str());
  fprintf(stderr, "%s\t\t(optional flag) if set, surfaces will be inserted in the provided order. Usually surfaces are reorderd inside-to-outside.\n", keep_ordering_par.c_str());
  fprintf(stderr, "The supported input formats are:\n%s\n", input_formats.c_str());
  fprintf(stderr, "The supported output formats are:\n%s\n", output_formats.c_str());
  fprintf(stderr, "\n");
}

void print_insert_fib_help()
{
  fprintf(stderr, "insert fibers: insert element fibers.\n");
  fprintf(stderr, "parameters:\n");
  fprintf(stderr, "%s<path>\t\t(input) path to basename of the mesh.\n", mesh_par.c_str());
  fprintf(stderr, "%s<fiber>,<sheets>\t(input) fibers or sheets, or list of fibers followed by sheets. Files are .vec files.\n",
                   idat_par.c_str());
  fprintf(stderr, "%s<int>\t (optional) insert mode. 0 == fibers and sheets, 1 == only fibers, 2 == only sheets. 0 is default.\n", mode_par.c_str());
  fprintf(stderr, "%s<path>\t\t(output) path to basename of the output mesh\n", outmesh_par.c_str());
  fprintf(stderr, "%s<format>\t\t(optional) mesh input format.\n", inp_format_par.c_str());
  fprintf(stderr, "%s<format>\t\t(optional) mesh output format.\n", out_format_par.c_str());
  fprintf(stderr, "The supported input formats are:\n%s\n", input_formats.c_str());
  fprintf(stderr, "The supported output formats are:\n%s\n", output_formats.c_str());
  fprintf(stderr, "\n");
}


int insert_parse_options(int argc, char** argv, struct insert_options & opts)
{
  if(argc < 3) {
    //print_usage(argc, argv);
    std::cerr << "Error: Please choose one of the following insert modes: " << std::endl << std::endl;
    print_insert_submesh_help();
    print_insert_meshdata_help();
    print_insert_data_help();
    print_insert_tagsurfs_help();
    print_insert_tags_help();
    print_insert_imagetags_help();
    print_insert_fib_help();
    return 1;
  }

  std::string insert_type = argv[2];

  for(int i=3; i<argc; i++){
    std::string param = reformat_param(argv, i, argc);
    bool match = false;

    if(!match) match = parse_param(param, mesh_par, opts.msh_base);
    if(!match) match = parse_param(param, submesh_par, opts.submsh_base);
    if(!match) match = parse_param(param, inp_mesh_par, opts.submsh_base);
    if(!match) match = parse_param(param, outmesh_par, opts.outmsh_base);
    if(!match) match = parse_param(param, oper_par, opts.oper);
    if(!match) match = parse_param(param, mesh_data_par, opts.msh_dat_file);
    if(!match) match = parse_param(param, idat_par, opts.idat);
    if(!match) match = parse_param(param, submesh_data_par, opts.submsh_dat_file);
    if(!match) match = parse_param(param, odat_par, opts.out_dat_file);
    if(!match) match = parse_param(param, out_format_par, opts.ofmt);
    if(!match) match = parse_param(param, inp_format_par, opts.ifmt);
    if(!match) match = parse_param(param, trsp_par, opts.trsp);
    if(!match) match = parse_param(param, trsp_dist_par, opts.trsp_dist);
    if(!match) match = parse_param(param, grad_par, opts.grad_thr);
    if(!match) match = parse_param(param, con_par, opts.con_thr);
    if(!match) match = parse_param(param, mode_par, opts.mode);
    if(!match) match = parse_param(param, surf_par, opts.surf);
    if(!match) match = parse_param(param, tags_par, opts.tags);
    if(!match) match = parse_param(param, val_par, opts.val);
    if(!match) match = parse_param(param, grow_par, opts.grow);
    if(!match) match = parse_flag (param, keep_ordering_par, opts.keep_ordering);
    if(!match) match = parse_flag (param, keep_surf_par, opts.keep_surface);

    if(!match) {
      std::cerr << "Error: Cannot parse parameter " << param << std::endl;
      return 2;
    }
  }

  if(insert_type.compare("submesh") == 0)
  {
    opts.type = OP_MESH;

    if( !(opts.msh_base.size() > 0 && opts.submsh_base.size() > 0 && opts.outmsh_base.size() > 0) )
    {
      std::cerr << "Submesh insert error: Insufficient parameters provided." << std::endl;
      print_insert_submesh_help();
      return 4;
    }
  }
  else if(insert_type.compare("meshdata") == 0)
  {
    opts.type = OP_SURF;

    if(! (opts.msh_base.size() > 0 &&
          opts.submsh_base.size() > 0 &&
          opts.outmsh_base.size() > 0 &&
          opts.oper.size() > 0) )
    {
      std::cerr << "Meshdata insert error: Insufficient parameters provided." << std::endl;
      print_insert_meshdata_help();
      return 4;
    }
  }
  else if(insert_type.compare("data") == 0)
  {
    opts.type = OP_DATA;

    if( !(opts.msh_base.size() > 0 && opts.submsh_base.size() > 0 &&
          opts.submsh_dat_file.size() > 0 && opts.out_dat_file.size() > 0) )
    {
      std::cerr << "Data insert error: Insufficient parameters provided." << std::endl;
      print_insert_data_help();
      return 4;
    }
  }
  else if(insert_type.compare("tags") == 0)
  {
    opts.type = OP_TAGS;

    if( !(opts.msh_base.size() > 0 && opts.idat.size() > 0 && opts.outmsh_base.size() > 0) )
    {
      std::cerr << "Tags insert error: Insufficient parameters provided." << std::endl;
      print_insert_tags_help();
      return 4;
    }
  }
  else if(insert_type.compare("imagetags") == 0)
  {
    opts.type = OP_IMGTAGS;

    if( !(opts.msh_base.size() > 0 && opts.idat.size() > 0 && opts.outmsh_base.size() > 0) )
    {
      std::cerr << "image tags insert error: Insufficient parameters provided." << std::endl;
      print_insert_imagetags_help();
      return 4;
    }
  }
  else if(insert_type.compare("tagsurfs") == 0)
  {
    opts.type = OP_TAGSURF;

    if( !(opts.msh_base.size() > 0 && opts.surf.size() > 0 && opts.outmsh_base.size() > 0) )
    {
      std::cerr << "insert tagsurfs error: Insufficient parameters provided." << std::endl;
      print_insert_tagsurfs_help();
      return 4;
    }
  }
  else if(insert_type.compare("fibers") == 0)
  {
    opts.type = OP_FIB;

    if( !(opts.msh_base.size() > 0 && opts.idat.size() > 0 && opts.outmsh_base.size() > 0) )
    {
      std::cerr << "insert fibers error: Insufficient parameters provided." << std::endl;
      print_insert_fib_help();
      return 4;
    }
  }
  else {
    print_usage(argv[0]);
    return 2;
  }

  return 0;
}


template<class T>
void insert_1dpn(const mt_vector<mt_idx_t> & nod, mt_vector<T> & in, mt_vector<T> & out)
{
  #ifdef OPENMP
  #pragma omp parallel for schedule(dynamic, 256)
  #endif
  for(size_t i=0; i<nod.size(); i++)
    out[nod[i]] = in[i];
}
template<class T>
void insert_3dpn(const mt_vector<mt_idx_t> & nod, mt_vector<T> & in, mt_vector<T> & out)
{
  #ifdef OPENMP
  #pragma omp parallel for schedule(dynamic, 128)
  #endif
  for(size_t i=0; i<nod.size(); i++) {
    out[nod[i]*3+0] = in[i*3+0];
    out[nod[i]*3+1] = in[i*3+1];
    out[nod[i]*3+2] = in[i*3+2];
  }
}



void insert_mode(int argc, char** argv)
{
  struct insert_options opts;
  int ret = insert_parse_options(argc, argv, opts);
  struct timeval t1, t2;

  if(ret != 0) return;

  struct mt_meshdata mesh;

  switch(opts.type) {
    case OP_MESH: {
      // insert mesh  ------------------------------------------------------------
      mt_filename mshfile   (opts.msh_base, opts.ifmt);
      mt_filename submshfile(opts.submsh_base, opts.ifmt);
      mt_filename outmshfile(opts.outmsh_base, opts.ofmt);

      std::cout << "Reading submesh: " << submshfile.base << std::endl;
      gettimeofday(&t1, NULL);

      mt_meshdata submsh;
      read_mesh_selected(submsh, submshfile.format, submshfile.base);
      mt_vector<mt_idx_t> eidx(submsh.etags.size()), nod(submsh.xyz.size() / 3);
      binary_read(eidx, submshfile.base + EIDX_EXT);
      binary_read(nod,  submshfile.base + NOD_EXT);

      gettimeofday(&t2, NULL);
      std::cout << "Done in " << (float)timediff_sec(t1, t2) << " sec" << std::endl;

      std::cout << "Reading mesh: " << mshfile.base << std::endl;
      gettimeofday(&t1, NULL);
      read_mesh_selected(mesh, mshfile.format, mshfile.base);
      gettimeofday(&t2, NULL);
      std::cout << "Done in " << (float)timediff_sec(t1, t2) << " sec" << std::endl;

      std::cout << "Inserting submesh ..." << std::endl;
      gettimeofday(&t1, NULL);
      insert_mesh_mode(mesh, submsh, eidx, nod); 
      gettimeofday(&t2, NULL);
      std::cout << "Done in " << (float)timediff_sec(t1, t2) << " sec" << std::endl;

      std::cout << "Writing mesh: " << outmshfile.base << std::endl;
      gettimeofday(&t1, NULL);
      write_mesh_selected(mesh, outmshfile.format, outmshfile.base);
      gettimeofday(&t2, NULL);
      std::cout << "Done in " << (float)timediff_sec(t1, t2) << " sec" << std::endl;
      break;
    }

    case OP_DATA: {
      bool nodal_data = true;
      if(opts.mode.size()) nodal_data = atoi(opts.mode.c_str()) != 1;

      // insert data  ------------------------------------------------------------
      mt_filename mshfile   (opts.msh_base, "");
      mt_filename submshfile(opts.submsh_base, "");

      std::string indices_file;
      if(nodal_data)
        indices_file = opts.submsh_base + NOD_EXT;
      else
        indices_file = opts.submsh_base + EIDX_EXT;

      size_t numidx = file_size(indices_file.c_str()) / sizeof(mt_idx_t);
      if(numidx == 0) {
        fprintf(stderr, "%s error: Cannot determine number of indices. Aborting!\n", __func__);
        exit(EXIT_FAILURE);
      }

      // read mapping
      mt_vector<mt_idx_t> indices(numidx);
      std::cout << "Reading " << indices_file << std::endl;
      binary_read(indices, indices_file);

      // deal with missing mesh data
      bool msh_data_exists = opts.msh_dat_file.size() > 0 && file_exists(opts.msh_dat_file);
      mt_idx_t num_mesh_idx = 0;
      if(msh_data_exists == false) {
        read_mesh_selected(mesh, mshfile.format, mshfile.base, CRP_READ_ELEM | CRP_READ_PTS);
        num_mesh_idx = nodal_data ? mesh.xyz.size() / 3 : mesh.e2n_cnt.size();
      }

      // read and insert data
      short data_idx = -1, dpn = 1;
      mt_vector<mt_real> idat, odat;
      igb_header igb_msh, igb_submsh;

      mt_real dflt_val = 0.0;
      if(opts.val.size()) dflt_val = atof(opts.val.c_str());

      setup_data_format(opts.submsh_dat_file, data_idx, igb_submsh);
      if(data_idx < 0) exit(EXIT_FAILURE);

      if(data_idx == 2 || data_idx == 3) dpn = 3;

      gettimeofday(&t1, NULL);
      std::cout << "Inserting data .. " << std::endl;
      switch(data_idx) {
        case 0:
        case 2:
          if(data_idx == 0) {
            std::cout << "Reading input data: " << opts.submsh_dat_file << std::endl;
            read_vector(idat, opts.submsh_dat_file, true);
            if(msh_data_exists) {
              std::cout << "Reading input data: " << opts.msh_dat_file << std::endl;
              read_vector(odat, opts.msh_dat_file, true);
            } else {
              odat.assign(num_mesh_idx*dpn, dflt_val);
            }

            insert_1dpn(indices, idat, odat);
            write_vector(odat, opts.out_dat_file, 1);
          } else {
            std::cout << "Reading input data: " << opts.submsh_dat_file << std::endl;
            read_vector_ascii(idat, opts.submsh_dat_file, true);
            if(msh_data_exists) {
              std::cout << "Reading input data: " << opts.msh_dat_file << std::endl;
              read_vector_ascii(odat, opts.msh_dat_file, true);
            } else {
              odat.assign(num_mesh_idx*dpn, dflt_val);
            }

            insert_3dpn(indices, idat, odat);
            write_vector_ascii(odat, opts.out_dat_file, 3);
          }

          std::cout << "Writing output data: " << opts.out_dat_file << std::endl;
          break;

        case 1:
        case 3:
        {
          igb_header igb_out;
          if(msh_data_exists) {
            init_igb_header(opts.msh_dat_file, igb_msh);
            read_igb_header(igb_msh);

            if(igb_msh.v_t != igb_submsh.v_t) {
              fprintf(stderr, "Number of time-slices in mesh and submesh do not match. Aborting!\n");
              exit(1);
            }
          }

          // set new spatial size and write output igb header
          init_igb_header(igb_submsh, opts.out_dat_file, igb_out);
          igb_out.v_x = num_mesh_idx;
          igb_out.v_t = igb_submsh.v_t;
          write_igb_header(igb_out);

          printf("Inserting igb time-slices %s into %s, writing to %s. \n", opts.submsh_dat_file.c_str(),
                 opts.msh_dat_file.c_str(), opts.out_dat_file.c_str());

          for(int t=0; t<igb_submsh.v_t; t++) {
            printf("\rcurrent time-slice %d / %d .. ", t+1, int(igb_submsh.v_t));
            fflush(stdout);

            read_igb_slice(idat, igb_submsh);

            if(msh_data_exists) {
              read_igb_slice(odat, igb_msh);
            } else {
              odat.assign(num_mesh_idx*dpn, dflt_val);
            }

            if(data_idx == 1) insert_1dpn(indices, idat, odat);
            else              insert_3dpn(indices, idat, odat);

            write_igb_slice(odat, igb_out);
          }
          printf("\n");

          if(msh_data_exists) fclose(igb_msh.fileptr);
          fclose(igb_submsh.fileptr);
          fclose(igb_out.fileptr);
          break;
        }
        default: break;
      }
      gettimeofday(&t2, NULL);
      std::cout << "Done in " << (float)timediff_sec(t1, t2) << " sec" << std::endl;
      break;
    }

    /*  The module maps element data defined on a manifold (line, surface) of the
     *  mesh onto mesh elements.
     *
     *  The main idea is to create a manifoldEle-to-meshEle map. With this map,
     *  one can select the manifold element sharing the most nodes with each
     *  mesh element.
     *
     */
    case OP_SURF:
    {
      struct insert_meshdata_options mode_opts;
      struct mt_meshdata manifold;
      insert_meshdata_mode_read(opts, mesh, manifold, mode_opts);

      insert_meshdata_mode(mesh, manifold, mode_opts);
      
      mt_filename outmshfile(opts.outmsh_base, opts.ofmt);

      std::cout << "Writing mesh: " << outmshfile.base << std::endl;
      gettimeofday(&t1, NULL);
      write_mesh_selected(mesh, outmshfile.format, outmshfile.base);
      gettimeofday(&t2, NULL);
      std::cout << "Done in " << (float)timediff_sec(t1, t2) << " sec" << std::endl;
      break;
    }

    case OP_TAGS: {
      mt_filename mshfile   (opts.msh_base, opts.ifmt);
      mt_filename outmshfile(opts.outmsh_base, opts.ofmt);

      std::cout << "Reading mesh: " << mshfile.base << std::endl;
      gettimeofday(&t1, NULL);
      read_mesh_selected(mesh, mshfile.format, mshfile.base);
      gettimeofday(&t2, NULL);
      std::cout << "Done in " << (float)timediff_sec(t1, t2) << " sec" << std::endl;

      mt_vector<mt_tag_t> newtags;
      readElementTags_general(newtags, opts.idat);

      insert_tags_mode(mesh, newtags);

      std::cout << "Writing mesh: " << outmshfile.base << std::endl;
      gettimeofday(&t1, NULL);
      write_mesh_selected(mesh, outmshfile.format, outmshfile.base);
      gettimeofday(&t2, NULL);
      std::cout << "Done in " << (float)timediff_sec(t1, t2) << " sec" << std::endl;
      break;
    }

    case OP_IMGTAGS: {

      mt_filename mshfile   (opts.msh_base, opts.ifmt);
      mt_filename outmshfile(opts.outmsh_base, opts.ofmt);

      std::cout << "Reading mesh: " << mshfile.base << std::endl;
      gettimeofday(&t1, NULL);
      read_mesh_selected(mesh, mshfile.format, mshfile.base);
      gettimeofday(&t2, NULL);
      std::cout << "Done in " << (float)timediff_sec(t1, t2) << " sec" << std::endl;

      std::cout << "Reading image: " << opts.idat << std::endl;
      gettimeofday(&t1, NULL);

      itk_image input_img;
      input_img.read_file(opts.idat.c_str());

      gettimeofday(&t2, NULL);
      std::cout << "Done in " << (float)timediff_sec(t1, t2) << " sec" << std::endl;

      mt_vector<std::string> tagslist;
      MT_USET<int>    tags;
      bool tags_provided = false;

      if(opts.tags.size()) {
        tags_provided = true;
        split_string(opts.tags, ',', tagslist);
        for(std::string & str : tagslist)
          tags.insert(atoi(str.c_str()));
      }

      MT_USET<mt_idx_t> surf_set;
      if(opts.keep_surface) {
        mt_meshdata surface;
        mt_vector<mt_idx_t> elems; range<mt_idx_t>(0, mesh.e2n_cnt.size(), elems);
        mt_vector<mt_idx_t> elem_orig;

        compute_surface(mesh, elems, surface, false, &elem_orig);
        surf_set.insert(elem_orig.begin(), elem_orig.end());
      }

      std::cout << "inserting image data .. " << std::endl;
      gettimeofday(&t1, NULL);
      insert_imgtags_mode(mesh, input_img, surf_set, tags);
      gettimeofday(&t2, NULL);
      std::cout << "Done in " << (float)timediff_sec(t1, t2) << " sec" << std::endl;

      std::cout << "Writing mesh: " << outmshfile.base << std::endl;
      gettimeofday(&t1, NULL);
      write_mesh_selected(mesh, outmshfile.format, outmshfile.base);
      gettimeofday(&t2, NULL);
      std::cout << "Done in " << (float)timediff_sec(t1, t2) << " sec" << std::endl;
      break;
    }

    case OP_TAGSURF: {
      mt_filename mshfile   (opts.msh_base, opts.ifmt);
      mt_filename outmshfile(opts.outmsh_base, opts.ofmt);

      mt_vector<kdtree*> surface_trees;
      mt_vector<mt_meshdata*> surface;
      mt_vector<std::string> tags_list;

      mt_vector<std::string> surf_list;
      split_string(opts.surf, ',', surf_list);
      if (opts.tags.size())
        split_string(opts.tags, ',', tags_list);

      insert_tagsurfs_mode_read(mshfile, mesh, surface_trees, surface, tags_list, surf_list);

      const int sampling_type = (opts.mode.size()) ? atoi(opts.mode.c_str()) : 1;
      const int grow          = (opts.grow.size()) ? atoi(opts.grow.c_str()) : 0;
      insert_tagsurfs_mode(mesh, surface_trees, surface, tags_list, sampling_type, grow);

      std::cout << "Writing output mesh " << outmshfile.base << std::endl;
      gettimeofday(&t1, NULL);

      write_mesh_selected(mesh, outmshfile.format, outmshfile.base);

      gettimeofday(&t2, NULL);
      std::cout << "Done in " << (float)timediff_sec(t1, t2) << " sec" << std::endl;

      for(size_t i=0; i<surface.size(); i++) {
        delete surface[i];
        delete surface_trees[i];
      }

      break;
    }

    case OP_FIB:
    {
      mt_filename mshfile(opts.msh_base, opts.ifmt);
      std::cout << "Reading mesh: " << mshfile.base << std::endl;
      gettimeofday(&t1, NULL);
      read_mesh_selected(mesh, mshfile.format, mshfile.base);
      gettimeofday(&t2, NULL);
      std::cout << "Done in " << (float)timediff_sec(t1, t2) << " sec" << std::endl;

      int mode = 0;
      if(opts.mode.size())
        mode = clamp(atoi(opts.mode.c_str()), 0, 2, true);

      mt_filename outmsh(opts.outmsh_base, opts.ofmt);

      mt_vector<mt_fib_t> fib(mesh.e2n_cnt.size() * 3, mt_real(0));
      int numfib = 3;

      mt_vector<std::string> input_files;
      split_string(opts.idat, ',', input_files);
      for(std::string & s : input_files) {
        size_t pos;
        if(!find_extension(s, VEC_EXT, pos))
          s += std::string(VEC_EXT);
      }

      if(mode == 0 && input_files.size() == size_t(1)) {
        fprintf(stderr, "insert fibers warning: Only one fiber vector provided! Using it as fiber directions!\n");
        mode = 1;
      }

      if(mode == 2 && mesh.lon.size() == mesh.e2n_cnt.size() * 3) {
        fprintf(stderr, "insert fibers warning: Input mesh has only one fiber direction! Extending mesh fibers to two directions!\n");
        numfib = 6;
        fib = mesh.lon;

        mesh.lon.resize(mesh.e2n_cnt.size() * numfib);
        for(size_t i=0; i<mesh.e2n_cnt.size(); i++) {
          mesh.lon[i*numfib+0] = fib[i*3+0];
          mesh.lon[i*numfib+1] = fib[i*3+1];
          mesh.lon[i*numfib+2] = fib[i*3+2];
        }
      }

      std::cout << "Inserting fiber data .." << std::endl;

      switch(mode) {
        case 0: {
          numfib = 6;
          std::string & fibfile = input_files[0];
          std::string & shefile = input_files[1];

          mesh.lon.resize(mesh.e2n_cnt.size() * numfib);

          read_vector_ascii(fib, fibfile);
          insert_fibers_mode(mesh, fib, 1);

          read_vector_ascii(fib, shefile);
          insert_fibers_mode(mesh, fib, 2);
          break;
        }

        case 1: {
          std::string & fibfile = input_files[0];
          read_vector_ascii(fib, fibfile);
          insert_fibers_mode(mesh, fib, 1);
          break;
        }

        case 2: {
          std::string & shefile = input_files[0];
          read_vector_ascii(fib, shefile);
          insert_fibers_mode(mesh, fib, 2);

          break;
        }

        default: break;
      }

      write_mesh_selected(mesh, outmsh.format, outmsh.base);
      break;
    }

    default: break;
  }
}
