#ifndef INTERPOLATE_MODE_H_
#define INTERPOLATE_MODE_H_

#include "mt_modes_base.h"
#ifdef MT_ADDONS
#include "addons_utils.h"
#endif

class interpolate_nodes_data
{
public:
  mt_manifold imnfld, omnfld;
  kdtree ivtxtree, isurftree;
  mt_vector<mt_idx_t> corr;
  mt_vector<mt_real> corr_dist;

  interpolate_nodes_data()
    : imnfld(), omnfld(), ivtxtree(10), isurftree(10), corr(), corr_dist()
  {}

  void update_correspondance(const mt_meshdata& mesh0, const mt_meshdata& mesh1)
  {
    compute_correspondance(mesh0, mesh1, corr, corr_dist);
  }
};


/**
 * @brief interpolate nodal data from one mesh onto another
 *
 * @param[in]  intplt_data   interpolation data
 * @param[in]  imesh         the mesh to interpolate from
 * @param[in]  omesh         the mesh to interpolate to
 * @param[in]  idat          input data
 * @param[out] odat          output data
 * @param[in]  norm          normalize vector data true
 * @param[in]  dpn           number of components per node (1,3, or 9) 
 * 
 * @return 0 on succes, !0 otherwise
 */
int interpolate_nodes_mode(
  const interpolate_nodes_data& intplt_data,
  const mt_meshdata& imesh,
  const mt_meshdata& omesh,
  const mt_vector<mt_real>& idata, 
  mt_vector<mt_real>& odata,
  const bool norm,
  int dpn=0);


/**
 * @brief interpolate nodal igb-data from one mesh onto another
 *
 * @param[in]  intplt_data   interpolation data
 * @param[in]  imesh         the mesh to interpolate from
 * @param[in]  omesh         the mesh to interpolate to
 * @param[in]  iigb          input igb-data
 * @param[out] oigb          output igb-data
 * @param[in]  dynpts        dynpoints applied to `imesh`
 * @param[in]  norm          normalize vector data true
 * @param[in]  dpn           number of components per node (1,3, or 9) 
 * 
 * @return 0 on succes, !0 otherwise
 */
int interpolate_nodes_mode(
  interpolate_nodes_data& intplt_data,
  mt_meshdata& imesh,
  const mt_meshdata& omesh,
  igb_header& iigb,
  igb_header& oigb,
  igb_header* dynpts,
  const bool norm, 
  int dpn=0,
  bool verbose = false);


/**
 * @brief interpolate data from elements onto nodes
 * 
 * @param[in]  imesh   input mesh data
 * @param[in]  idat    input data
 * @param[out] odat    output data
 * @param[in]  norm    normalize vector data if true
 * @param[in]  dpn     number of components per node (1,3, or 9) 
 *
 * @return 0 on succes, !0 otherwise
 */
int interpolate_elem2node_mode(
  const mt_meshdata& mesh,
  const mt_vector<mt_real>& idat, 
  mt_vector<mt_real>& odat,
  const bool norm,
  int dpn=0);


/**
 * @brief interpolate igb-data from elements onto nodes
 * 
 * @param[in]  imesh   input mesh data
 * @param[in]  idat    input igb-data
 * @param[out] odat    output igb-data
 * @param[in]  norm    normalize vector data if true
 * @param[in]  dpn     number of components per node (1,3, or 9) 
 *
 * @return 0 on succes, !0 otherwise
 */
int interpolate_elem2node_mode(
  const mt_meshdata& mesh,
  igb_header& iigb,
  igb_header& oigb,
  const bool norm,
  int dpn=0);


/**
 * @brief interpolate data from nodes onto elements
 * 
 * @param[in]  mesh   input mesh data
 * @param[in]  idat   input data
 * @param[out] odat   output data
 * @param[in]  norm   normalize vector data if true
 * @param[in]  dpn    number of components per node (1,3, or 9) 
 * 
 * @return 0 on succes, !0 otherwise
 */
int interpolate_node2elem_mode(
  const mt_meshdata& mesh,
  const mt_vector<mt_real>& idat,
  mt_vector<mt_real>& odat,
  const bool norm,
  const int mode,
  int dpn=0); 


/**
 * @brief interpolate igb-data from nodes onto elements
 * 
 * @param[in]  mesh   input mesh data
 * @param[in]  iigb   input igb-data
 * @param[out] oigb   output igb-data
 * @param[in]  norm   normalize vector data true
 * @param[in]  dpn    number of components per node (1,3, or 9) 
 * 
 * @return 0 on succes, !0 otherwise
 */
int interpolate_node2elem_mode(
  const mt_meshdata& mesh,
  igb_header& iigb, 
  igb_header& oigb,
  const bool norm,
  const int mode,
  int dpn=0);


enum class interpolation_t : int8_t
{
  rbf          = 0,
  shepard      = 1,
  glob_shepard = 2
};

/**
 * @brief interpolate data from a pointcloud onto a mesh
 * 
 * @param[in] interp_type  type of interpolation
 * @param[in] ipnts        input points
 * @param[out] omesh       output mesh
 * @param[in] idat         input data
 * @param[out] odat        output data
 * @param[in] dpn          number of components per node (1,3, or 9)
 */

int interpolate_clouddata_mode(
  const interpolation_t interp_type,
  const mt_vector<mt_real>& ipnts, 
  const mt_meshdata& omesh,
  const mt_vector<mt_real>& idat,
  mt_vector<mt_real>& odat,
  int dpn=0);


/**
 * @brief interpolate data from a pointcloud onto a mesh
 * 
 * @param[in] interp_type  type of interpolation
 * @param[in] ipnts        input points
 * @param[out] omesh       output mesh
 * @param[in] idat         input data
 * @param[out] odat        output data
 * @param[in] dpn          number of components per node (1,3, or 9)
 */

int interpolate_clouddata_mode(
  const interpolation_t interp_type,
  const mt_vector<vec3r>& ipnts, 
  const mt_meshdata& omesh,
  const mt_vector<mt_real>& idat,
  mt_vector<mt_real>& odat,
  int dpn=0);


/**
 * @brief interpolate data from a pointcloud onto a mesh including igb data
 * 
 * @param[in] interp_type  type of interpolation
 * @param[in] ipnts        input points
 * @param[out] omesh       output mesh
 * @param[in] iigb         input igb stream
 * @param[out] oigb        output igb stream
 * @param[in] dynpts       dynamic points
 * @param[in] dpn          number of components per node (1,3, or 9)
 */

int interpolate_clouddata_mode(
  const interpolation_t interp_type,
  mt_vector<mt_real>& ipnts,
  const mt_meshdata& omesh,
  igb_header& iigb,
  igb_header& oigb,
  igb_header* dynpts,
  int dpn=0);

#if 0
struct interpolate_clouddata_data
{
  const short data_idx; 
  bool shepard;
  bool shepard_global;
  bool have_dynpts;
};


/**
 * @brief interpolate data from a pointcloud onto a mesh using radial basis function interpolation
 * 
 * @param[in] data_idx data type index
 * @param[in] shepard use shepard interpolation
 * @param[in] shepard_global use global shepard interpolation
 * @param[in] have_dynpts whether dynamic points are present
 * @param[in] idat input data
 * @param[out] odat output data
 * @param[in] ipts input points
 * @param[out] omesh output mesh
 * @param[in] igb_input input igb stream
 * @param[in] igb_pts_dynpt input igb stream for dynamic points
 * @param[out] igb_output output igb stream
 */
void interpolate_clouddata_mode(
  const struct interpolate_clouddata_opts& opts,
  mt_vector<mt_real>& idat,
  mt_vector<mt_real>& odat,
  mt_vector<mt_real>& ipts,
  mt_meshdata& omesh,
  igb_input_stream* igb_input,
  igb_input_stream* igb_pts_dynpt,
  igb_output_stream* igb_output);
#endif

#endif
