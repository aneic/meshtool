#include "data_structs.h"
#include "query_mode.h"

void query_tags_mode_read(const struct query_options &opts, mt_meshdata &mesh, mt_vector<mt_idx_t> &vtx) {
  struct timeval t1, t2;

  std::cout << "Reading mesh: " << opts.msh.base << std::endl;
  gettimeofday(&t1, NULL);
  read_mesh_selected(mesh, opts.msh.format, opts.msh.base, CRP_READ_ELEM | CRP_READ_LON);
  print_mesh_stats(mesh);

  if (opts.vtx.size()) read_vtx(vtx, opts.vtx);

  gettimeofday(&t2, NULL);
  std::cout << "Done in " << (float)timediff_sec(t1, t2) << " sec" << std::endl;
  if (vtx.size()) std::cout << "querying tags only for elems connected to vertices " << opts.vtx << std::endl;
}

void query_tags_mode(mt_meshdata &mesh, const mt_vector<mt_idx_t> &vtx, MT_MAP<mt_tag_t, size_t> &myo_tags,
                     MT_MAP<mt_tag_t, size_t> &bath_tags, mt_real fib_tol) {
  fib_tol = std::fabs(fib_tol);

  // compute and print all unique tags
  short numFib = mesh.lon.size() == mesh.e2n_cnt.size() * 6 ? 6 : 3;

  if (vtx.size()) {
    transpose_connectivity_par(mesh.e2n_cnt, mesh.e2n_con, mesh.n2e_cnt, mesh.n2e_con, false);
    bucket_sort_offset(mesh.e2n_cnt, mesh.e2n_dsp);
    bucket_sort_offset(mesh.n2e_cnt, mesh.n2e_dsp);

    for (mt_idx_t nidx : vtx) {
      mt_idx_t start = mesh.n2e_dsp[nidx], stop = start + mesh.n2e_cnt[nidx];

      for (mt_idx_t j = start; j < stop; j++) {
        mt_idx_t eidx = mesh.n2e_con[j];

        vec3r l(mesh.lon.data() + eidx * numFib);
        mt_idx_t tag = mesh.etags[eidx];

        if (l.length2() > fib_tol)
          myo_tags[tag] = myo_tags[tag] + 1;
        else
          bath_tags[tag] = bath_tags[tag] + 1;
      }
    }
  } else {
    for (size_t i = 0; i < mesh.etags.size(); i++) {
      vec3r l(mesh.lon.data() + i * numFib);
      mt_idx_t tag = mesh.etags[i];

      if (l.length2() > 0.1)
        myo_tags[tag] = myo_tags[tag] + 1;
      else
        bath_tags[tag] = bath_tags[tag] + 1;
    }
  }

  myo_tags.sort();
  bath_tags.sort();
}