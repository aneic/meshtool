#include <data_structs.h>
#include <io_utils.h>

#include <cstddef>
#include <cstdio>
#include <cstring>
#include <igb_utils.hpp>
#include <iostream>
#include <shape_utils.hpp>

#include "data_structs.h"
#include "extract_mode.h"
#include "igb_utils.hpp"
#include "mesh_utils.h"
#include "mt_modes_base.h"
#include "mt_utils.h"
#include "mt_vector.h"
#include "string_utils.h"
#include "topology_utils.h"

int extract_mesh_mode(mt_meshdata& mesh, const std::set<mt_tag_t>& tags, mt_vector<mt_idx_t>& nod,
                      mt_vector<mt_idx_t>& eidx) {
  size_t count_keep = 0;

  // generate vector of elements to keep
  mt_vector<bool> keep(mesh.etags.size(), false);
  for (size_t i = 0; i < keep.size(); i++) {
    if (tags.count(mesh.etags[i])) {
      keep[i] = true;
      count_keep++;
    }
  }

  // if no element is selected, return -1
  if (count_keep == 0) return -1;
  // if all elements are selected, return -2
  if (count_keep == keep.size()) return -2;

  restrict_meshdata(keep, mesh, nod, eidx);
  return 0;
}

int extract_myocard_mode(mt_meshdata& mesh, mt_vector<mt_idx_t>& nod, 
                          mt_vector<mt_idx_t>& eidx, const mt_real fib_thr)
{
  if ((fib_thr < 0.0) || (fib_thr > 1.0))
    return -1;
  const mt_real thr = fib_thr*fib_thr;

  bool twoFib = (mesh.lon.size() == mesh.e2n_cnt.size() * 6);

  // generate vector of elements to keep
  mt_vector<bool> keep(mesh.etags.size(), false);
  if (twoFib) {
#ifdef OPENMP
#pragma omp parallel for schedule(guided, 100)
#endif
    for (size_t i = 0; i < keep.size(); i++) {
      mt_fib_t l1 = mesh.lon[i * 6 + 0];
      mt_fib_t l2 = mesh.lon[i * 6 + 1];
      mt_fib_t l3 = mesh.lon[i * 6 + 2];
      if ((l1*l1 + l2*l2 + l3*l3) > thr)
        keep[i] = true;
    }
  } else {
#ifdef OPENMP
#pragma omp parallel for schedule(guided, 100)
#endif
    for (size_t i = 0; i < keep.size(); i++) {
      mt_fib_t l1 = mesh.lon[i * 3 + 0];
      mt_fib_t l2 = mesh.lon[i * 3 + 1];
      mt_fib_t l3 = mesh.lon[i * 3 + 2];
      if ((l1*l1 + l2*l2 + l3*l3) > thr)
        keep[i] = true;
    }
  }
  restrict_meshdata(keep, mesh, nod, eidx);
  return 0;
}

void extract_overlap_mode(const mt_meshdata& mesh1, const mt_meshdata& mesh2, mt_real radius, mt_vector<bool>& keep)
{
  mt_vector<mt_real> m1_dat(mesh1.xyz.size() / 3, 0.0), m1_dat_ele;

  mt_real avrg_est = avrg_edgelength_estimate(mesh1, true);
  radius *= avrg_est;

  // we select mesh1 nodes in a radius around mesh2 nodes
  mt_vector<vec3r> pts;
  array_to_points(mesh1.xyz, pts);

  kdtree tree(10);
  tree.build_vertex_tree(pts);

  size_t npts_mesh2 = mesh2.xyz.size() / 3;
  MT_USET<mt_idx_t> sel_nod;

#ifdef OPENMP
#pragma omp parallel
#endif
  {
    MT_USET<mt_idx_t> loc_set;
    mt_vector<mt_mixed_tuple<mt_real, int>> found_vtx;

#ifdef OPENMP
#pragma omp for schedule(guided)
#endif
    for (size_t nidx = 0; nidx < npts_mesh2; nidx++) {
      vec3r ref(mesh2.xyz.data() + nidx * 3);
      tree.vertices_in_sphere(ref, radius, found_vtx);
      if (found_vtx.size()) {
        for (mt_mixed_tuple<mt_real, int>& t : found_vtx) loc_set.insert(t.v2);
      }
    }

#ifdef OPENMP
#pragma omp critical
#endif
    { sel_nod.insert(loc_set.begin(), loc_set.end()); }
  }

  for (mt_idx_t& idx : sel_nod) m1_dat[idx] = 100.0;
  nodeData_to_elemData(mesh1, m1_dat, m1_dat_ele);

  // now we mark all elements that have nodes inside mesh2
  // mt_vector<bool> keep(mesh1.e2n_cnt.size());
  keep.reserve(mesh1.e2n_cnt.size());
  for (size_t eidx = 0; eidx < mesh1.e2n_cnt.size(); eidx++) keep[eidx] = m1_dat_ele[eidx] > 0;
}


void tokenize_operstr(std::string & operstr, const char token_indicator, mt_vector<std::string> & file_names)
{
  int stringlen = operstr.length();

  file_names.resize(0);
  int file_names_idx = 0, pos = 0, start_pos;

  while (pos < stringlen) {

    if (operstr[pos] == '"') {
      // if we find a '"' char, the user is passing a protected filename. thus we search
      // the closing '"' and store the string between into file_names.
      start_pos = pos++;
      while ((pos < stringlen) && (operstr[pos] != '"'))
        pos++;

      // we found start and end pos characters, we do the tokenization
      if ((operstr[pos] == '"') && (pos-start_pos > 1)) {
        // extract filename and add to vector
        file_names.push_back(operstr.substr(start_pos+1, pos-start_pos-1));

        // create string token and replace in `operstr`
        std::string token = token_indicator + std::to_string(file_names_idx++);
        operstr.replace(start_pos, pos-start_pos+1, token);

        // update string length
        stringlen = operstr.size();

        // correct position
        pos = start_pos + token.length();
      }
      // we found start and end pos characters, but the string is empty
      else if ((operstr[pos] == '"') && (pos-start_pos == 1)) {
        fprintf(stderr, "%s error: Cannot tokenize string: %s \nEmpty string! Aborting!\n",
                __func__, operstr.c_str());
        exit(EXIT_FAILURE);
      }
      else {
        fprintf(stderr, "%s error: Cannot tokenize string: %s \nNon-matching \" characters! Aborting!\n",
                __func__, operstr.c_str());
        exit(EXIT_FAILURE);
      }

    }
    pos++;
  }
}

void extract_surf_fill_taglists(const std::string & operstr,
                                std::string & operflg,
                                mt_vector<mt_vector<std::string> > & surfA,
                                mt_vector<mt_vector<std::string> > & surfB)
{
   mt_vector<std::string> operlist;

   split_string(operstr, ';', operlist);
   int numoper = operlist.size();
   surfA.resize(numoper), surfB.resize(numoper);
   operflg.resize(numoper);

   for(int i=0; i<numoper; i++) {
     mt_vector<std::string> tlist;
     split_string(operlist[i], ':', tlist);
     if(tlist.size() == 2) {
       operflg[i] = ':';
       split_string(tlist[0], ',', surfA[i]);
       split_string(tlist[1], ',', surfB[i]);
     }
     else {
       split_string(operlist[i], '-', tlist);
       if(tlist.size() == 2) {
         operflg[i] = '-';
         split_string(tlist[0], ',', surfA[i]);
         split_string(tlist[1], ',', surfB[i]);
       }
       else {
         split_string(operlist[i], '+', tlist);
         if(tlist.size() == 2) {
           operflg[i] = '+';
           split_string(tlist[0], ',', surfA[i]);
           split_string(tlist[1], ',', surfB[i]);
         }
         else {
           operflg[i] = '0';
           split_string(tlist[0], ',', surfA[i]);
         }
       }
     }
   }
}


void detokenize_surfaces(mt_vector<mt_vector<std::string>> & surf,
                         const char token_indicator,
                         const mt_vector<std::string> & file_names)
{

  for (size_t i=0; i<surf.size(); i++)
    for (size_t j=0; j<surf[i].size(); j++)
      if ((surf[i][j].empty() == false) && (surf[i][j][0] == token_indicator)) {
        const int idx = std::stoi(surf[i][j]);
        surf[i][j] = file_names[idx];
      }
}


int extract_surface_mode(const float edge_thr, const float ang_thr,
                         const mt_vector<std::string>& coords,
                         const mt_meshdata& mesh, std::string oper,
                         const bool hybrid_output, const mt_real rad,
                         const mt_real lower_rad,
                         mt_vector<mt_meshdata>& surf_mesh,
                         mt_vector<nbc_data>& nbc)
{
  struct timeval t1, t2;
  mt_vector<mt_vector<std::string>> surfA, surfB;
  std::string operflg;

  // define operations
  if (oper.size() > 0) {
    const char token_indicator = ' ';
    mt_vector<std::string> file_names;
    tokenize_operstr(oper, token_indicator, file_names);

    extract_surf_fill_taglists(oper, operflg, surfA, surfB);

    detokenize_surfaces(surfA, token_indicator, file_names);
    detokenize_surfaces(surfB, token_indicator, file_names);
  } else {
    operflg = "0";
    std::string list;

    surfA.resize(1);
    mt_vector<mt_tag_t> alltags = mesh.etags;
    binary_sort(alltags);
    unique_resize(alltags);
    surfA[0].resize(alltags.size());

    for (size_t i = 0; i < alltags.size(); i++) surfA[0][i] = std::to_string(alltags[i]);
  }

  int numoper = operflg.size();
  // mt_vector<MT_MAP<mt_triple<mt_idx_t>, tri_sele>> tri_surf_a_vec, tri_surf_b_vec;
  // mt_vector<MT_MAP<mt_quadruple<mt_idx_t>, quad_sele>> quad_surf_a_vec, quad_surf_b_vec;

  // extract surface
  // --------------------------------------------------------------

  // check for sharp angle detection, we do this first to not waste the users
  // time when the input is actually bad

  // mt_vector< mt_vector<mt_idx_t> > surf_con(numoper);

  gettimeofday(&t1, NULL);
  surf_mesh.clear();
  surf_mesh.resize(numoper);
  nbc.clear();
  nbc.resize(numoper);

  for (int i = 0; i < numoper; i++) {
    std::cout << "Computing surface " << i + 1 << "/" << numoper << std::endl;

    MT_MAP<mt_triple<mt_idx_t>, tri_sele> tri_surf_a, tri_surf_b;
    MT_MAP<mt_quadruple<mt_idx_t>, quad_sele> quad_surf_a, quad_surf_b;

    unified_surf_from_mixed_list(mesh, surfA[i], tri_surf_a, quad_surf_a);
    if (operflg[i] != '0') unified_surf_from_mixed_list(mesh, surfB[i], tri_surf_b, quad_surf_b);

    switch (operflg[i]) {
      default:
        break;

      case '-':
        surface_difference(tri_surf_a, quad_surf_a, tri_surf_b, quad_surf_b);
        break;
      case '+':
        surface_union(tri_surf_a, quad_surf_a, tri_surf_b, quad_surf_b);
        break;
      case ':':
        surface_intersection(tri_surf_a, quad_surf_a, tri_surf_b, quad_surf_b);
        break;
    }

    mt_vector<mt_idx_t> elem_origin;

    // Later we want to do hybrid only, but for now we distinguish
    if (hybrid_output) {
      // we generate a surfmesh of quads and tris
      surfmap_to_surfmesh(tri_surf_a, quad_surf_a, surf_mesh[i], elem_origin);
    } else {
      // we convert all surface elements to triangles
      surfmap_to_vector(tri_surf_a, quad_surf_a, surf_mesh[i].e2n_con, elem_origin);
      surf_mesh[i].e2n_cnt.assign(surf_mesh[i].e2n_con.size() / 3, 3);
      surf_mesh[i].etype.assign(surf_mesh[i].e2n_con.size() / 3, Tri);
      surf_mesh[i].etags.assign(surf_mesh[i].e2n_con.size() / 3, mt_tag_t(0));
    }

    bucket_sort_offset(surf_mesh[i].e2n_cnt, surf_mesh[i].e2n_dsp);
    generate_nbc_data(mesh, surf_mesh[i], elem_origin, nbc[i]);
  }
  gettimeofday(&t2, NULL);
  std::cout << "Done in " << (float)timediff_sec(t1, t2) << " sec" << std::endl;

  int extr_error = 0;
  gettimeofday(&t1, NULL);
  for (int i = 0; i < numoper; i++) {
    if (surf_mesh[i].e2n_con.size() == 0) {
      std::cout << "Warning: surface extraction " << i << " yielded an empty surface! Skipping!" << std::endl;
      extr_error = 1;
      continue;
    }

    if (coords.size() > 0) {
      compute_full_mesh_connectivity(surf_mesh[i]);

      MT_USET<mt_idx_t> surf_nod;
      surf_nod.insert(surf_mesh[i].e2n_con.begin(), surf_mesh[i].e2n_con.end());

      if (edge_thr || ang_thr) {
        // we block connectiviy at sharp edges. therefore we have to traverse
        // along the surface elements and check for blocking edges (not blocking
        // nodes)
        mt_mapping<mt_idx_t> ele2edge;
        MT_MAP<mt_tuple<mt_idx_t>, mt_idx_t> edges;
        compute_edges(surf_mesh[i], ele2edge, edges);
        ele2edge.transpose();
        ele2edge.setup_dsp();

        MT_USET<mt_idx_t> sharp_edges;
        mt_vector<mt_real> elem_nrmls;

        if (edge_thr)
          identify_sharp_edges(surf_mesh[i], mesh.xyz, ele2edge, edges, edge_thr, sharp_edges);
        else if (ang_thr)
          compute_element_surface_normals(surf_mesh[i], mesh.xyz, elem_nrmls);

        mt_vector<bool> reached(surf_mesh[i].e2n_cnt.size(), false);
        vec3r ref_nrml;

        // we insert elements connected to input coords
        for (size_t cidx = 0; cidx < coords.size(); cidx++) {
          mt_vector<std::string> b;
          split_string(coords[cidx], ',', b);
          if (b.size() == 3) {
            vec3r p(atof(b[0].c_str()), atof(b[1].c_str()), atof(b[2].c_str()));

            mt_idx_t inp_idx;
            mt_real dist;
            linear_search_vtx(mesh.xyz, surf_nod, p, inp_idx, dist);

            // we insert the first element connected to the identified vertex
            // into reached
            mt_idx_t eidx = surf_mesh[i].n2e_con[surf_mesh[i].n2e_dsp[inp_idx]];
            reached[eidx] = true;

            if (ang_thr) ref_nrml.get(elem_nrmls.data() + eidx * 3);
          } else {
            fprintf(stderr, "Error: Failed to parse coord %s ! Aborting!\n", coords[cidx].c_str());
            exit(1);
          }
        }

        if (edge_thr)
          traverse_surfelem_connectivity(surf_mesh[i], ele2edge, sharp_edges, reached);
        else if (ang_thr)
          traverse_surfelem_connectivity(surf_mesh[i], ele2edge, elem_nrmls, ref_nrml, ang_thr, reached);

        remove_elems_from_surf(reached, surf_mesh[i].e2n_cnt, surf_mesh[i].e2n_con, nbc[i]);
      } else {
        // we do not have to care about sharp edges, therefore we can use the
        // cheaper traverse_nodal_connectivity functionality
        MT_USET<mt_idx_t> reached_set, remove_set;
        size_t numnodes = surf_mesh[i].n2n_cnt.size();
        MT_USET<mt_idx_t> reached;
        MT_USET<mt_idx_t> lower_reached;

        // we loop over coords and traverse
        for (size_t cidx = 0; cidx < coords.size(); cidx++) {
          mt_vector<std::string> b;
          split_string(coords[cidx], ',', b);
          if (b.size() == 3) {
            vec3r p(atof(b[0].c_str()), atof(b[1].c_str()), atof(b[2].c_str()));

            mt_idx_t inp_idx;
            mt_real dist;
            linear_search_vtx(mesh.xyz, surf_nod, p, inp_idx, dist);
            reached.insert(inp_idx), lower_reached.insert(inp_idx);

            if (rad > 0) {
              // we traverse only a given distance
              vec3r refpt(mesh.xyz.data() + inp_idx * 3);
              traverse_nodal_connectivity(surf_mesh[i].n2n_cnt, surf_mesh[i].n2n_dsp, surf_mesh[i].n2n_con, mesh.xyz,
                                          refpt, rad, reached);

              if (lower_rad > 0) {
                check_condition(lower_rad < rad, "Lower radius smaller than upper radius", __func__);

                traverse_nodal_connectivity(surf_mesh[i].n2n_cnt, surf_mesh[i].n2n_dsp, surf_mesh[i].n2n_con, mesh.xyz,
                                            refpt, lower_rad, lower_reached);

                for (mt_idx_t n : lower_reached) reached.erase(n);
              }
            } else {
              // we traverse whole connectivity
              traverse_nodal_connectivity(surf_mesh[i].n2n_cnt, surf_mesh[i].n2n_dsp, surf_mesh[i].n2n_con, reached);
            }

            // store reached nodes and reset reached boolean vec
            reached_set.insert(reached.begin(), reached.end());
            reached.clear();
            lower_reached.clear();
          } else {
            fprintf(stderr, "Error: Failed to parse coord %s ! Aborting!\n", coords[cidx].c_str());
            exit(1);
          }
        }

        for (mt_idx_t r = 0; r < mt_idx_t(numnodes); r++)
          if (reached_set.count(r) == 0) remove_set.insert(r);

        // remove nodes
        remove_nodes_from_surf(remove_set, surf_mesh[i].e2n_cnt, surf_mesh[i].e2n_con, nbc[i]);
      }
    }
  }
  return numoper;
}

void extract_tags_mode(mt_meshdata& mesh, mt_vector<mt_tag_t>& tags) {
  compute_full_mesh_connectivity(mesh, false);
  tags = mesh.etags;
}

void extract_tags_mode_write(const mt_vector<mt_tag_t>& tags, std::string odat) {
  if (endswith(odat, BIN_TAGS_EXT)) {
    mt_vector<int32_t> out_tags(tags);
    std::cout << "Writing binary tags: " << odat << std::endl;
    binary_write(out_tags.begin(), out_tags.end(), odat);
  } else {
    write_vector_ascii(tags, odat);
  }
}

void extract_volume_mode(mt_meshdata& mesh, const vec3r& ref, const vec3r& ref_delta, mt_vector<mt_idx_t>& nod,
                         mt_vector<mt_idx_t>& eidx) {
  // generate vector of elements to keep
  size_t nelem = mesh.e2n_cnt.size();
  mt_vector<bool> keep(nelem, false);
#ifdef OPENMP
#pragma omp parallel for schedule(guided, 100)
#endif
  for (size_t idx = 0; idx < nelem; idx++) {
    float x = 0, y = 0, z = 0;
    int esize = mesh.e2n_cnt[idx];

    // we use the center point of an element to check if
    // the element is inside the given box
    for (int i = 0; i < esize; i++) {
      int vidx = mesh.e2n_con[mesh.e2n_dsp[idx] + i];
      x += mesh.xyz[vidx * 3 + 0];
      y += mesh.xyz[vidx * 3 + 1];
      z += mesh.xyz[vidx * 3 + 2];
    }
    x /= esize;
    y /= esize;
    z /= esize;

    bool inside_x = ((x >= ref.x) && (x <= (ref.x + ref_delta.x)));
    bool inside_y = ((y >= ref.y) && (y <= (ref.y + ref_delta.y)));
    bool inside_z = ((z >= ref.z) && (z <= (ref.z + ref_delta.z)));

    keep[idx] = inside_x && inside_y && inside_z;
  }
  restrict_meshdata(keep, mesh, nod, eidx);
}

void extract_vtxhalo_mode(const mt_meshdata& mesh, const MT_USET<mt_idx_t> &block_set,
                          MT_USET<mt_idx_t> &halo, mt_vector<mt_vector<int64_t>> &halo_components) {

  const size_t num_nodes = mesh.n2n_cnt.size();
  const mt_idx_t max_nidx = static_cast<mt_idx_t>(num_nodes);

  halo.clear();
  // select halo vertices
  for (auto it=block_set.begin(); it!=block_set.end(); it++) {
    const mt_idx_t nidx = *it;
    if (nidx <= max_nidx) {
      const mt_cnt_t cnt = mesh.n2n_cnt[nidx];
      const mt_idx_t dsp = mesh.n2n_dsp[nidx];
      for (mt_cnt_t j=0; j<cnt; j++) {
        const mt_idx_t nbr_nidx = mesh.n2n_con[dsp+j];
        if (block_set.count(nbr_nidx) == 0)
          halo.insert(nbr_nidx);
      }
    }
  }


  if (halo.size() > 0)
  {
    mt_idx_t nidx(*halo.begin()), dom_idx(0);
    do {
      // remove current start vertex from halo set
      halo.erase(nidx);

      // selection and active set
      MT_USET<mt_idx_t> sel, act;
      // insert start vertex to selection and active set
      sel.insert(nidx);
      act.insert(nidx);

      while (act.size() > 0)
      {
        MT_USET<mt_idx_t> new_act;
        // iterate over all active vertices
        for (const mt_idx_t idx : act) {
          const mt_cnt_t cnt = mesh.n2n_cnt[idx];
          const mt_idx_t dsp = mesh.n2n_dsp[idx];
          // iterate over all neighbouring vertices
          for (mt_cnt_t j=0; j<cnt; j++) {
            const mt_idx_t nbr_idx = mesh.n2n_con[dsp+j];
            // if neighbour vertex is in halo set
            if (halo.count(nbr_idx) != 0) {
              // remove from halo set, add to selection and new active set
              halo.erase(nbr_idx);
              sel.insert(nbr_idx);
              new_act.insert(nbr_idx);
            }
          }
        }
        act.clear();
        act.swap(new_act);
      }

      mt_vector<int64_t>& sel_list = halo_components.push_back(mt_vector<int64_t>());
      sel.sort();
      sel_list.assign(sel.begin(), sel.end());

      nidx = -1;
      if (halo.size() > 0) {
        nidx = *halo.begin();
        dom_idx++;
      }
    } while (nidx != -1);
  }
}

void extract_gradient_mode(const mt_meshdata& mesh, const mt_vector<mt_real>& idat, mt_vector<mt_real>& mag,
                           mt_vector<vec3r>& grad, const bool nodal_input, const bool nodal_output) {
  compute_gradient(mesh, idat, nodal_input, nodal_output, grad, mag);
}

void extract_gradient_mode(const mt_meshdata& mesh, igb_header& iigb, igb_header* oigb, igb_header* oigb_vec,
                           const bool nodal_input, const bool nodal_output) {
  if ((oigb == nullptr) && (oigb_vec == nullptr)) return;
  const size_t num_time_slices = static_cast<size_t>(iigb.v_t);
  mt_vector<mt_real> idat, odat;
  mt_vector<vec3r> odat_vec;
  for (size_t t = 0; t < num_time_slices; t++) {
    read_igb_slice(idat, iigb);
    extract_gradient_mode(mesh, idat, odat, odat_vec, nodal_input, nodal_output);
    if (oigb) write_igb_slice(odat, *oigb);
    if (oigb_vec) {
      odat.resize(odat_vec.size() * 3);
      for (size_t i = 0; i < odat_vec.size(); i++) {
        odat[3 * i + 0] = odat_vec[i].x;
        odat[3 * i + 1] = odat_vec[i].y;
        odat[3 * i + 2] = odat_vec[i].z;
      }
      write_igb_slice(odat, *oigb_vec);
    }
  }
}

void extract_fibers(const mt_meshdata& mesh, const int numfib, mt_vector<mt_real>& fib, const int in_offset) {
  for (size_t i = 0; i < mesh.e2n_cnt.size(); i++) {
    fib[i * 3 + 0] = mesh.lon[i * numfib + 0 + in_offset];
    fib[i * 3 + 1] = mesh.lon[i * numfib + 1 + in_offset];
    fib[i * 3 + 2] = mesh.lon[i * numfib + 2 + in_offset];
  }
}

int extract_fibers_mode(const mt_meshdata& mesh, const int mode, mt_vector<mt_real>& fibers, mt_vector<mt_real>& sheets)
{
  const int numfib = mesh.lon.size() == mesh.e2n_cnt.size() * 6 ? 6 : 3;

  int num_extr = 0;
  if(mode >= 0 && mode <= 2) {
    if(mode == 1 || mode == 0) {
      fibers.resize(mesh.e2n_cnt.size() * 3);
      extract_fibers(mesh, numfib, fibers, 0);
      num_extr++;
    }
    if(mode == 2 || mode == 0) {
      assert(numfib == 6);
      sheets.resize(mesh.e2n_cnt.size() * 3);
      extract_fibers(mesh, numfib, sheets, 3);
      num_extr++;
    }
  }
  else {
    fprintf(stderr, "Error: unknown mode %d\n in %s\n", mode, __func__);
  }

  return num_extr;
}

template <class T>
void extract_1dpn(const mt_vector<mt_idx_t>& nod, const mt_vector<T>& in, mt_vector<T>& out) {
#ifdef OPENMP
#pragma omp parallel for schedule(dynamic)
#endif
  for (size_t i = 0; i < nod.size(); i++) {
    const size_t idx = static_cast<size_t>(nod[i]);
    out[i] = in[idx];
  }
}

template <class T>
void extract_3dpn(const mt_vector<mt_idx_t>& nod, const mt_vector<T>& in, mt_vector<T>& out) {
#ifdef OPENMP
#pragma omp parallel for schedule(dynamic)
#endif
  for (size_t i = 0; i < nod.size(); i++) {
    const size_t idx = static_cast<size_t>(nod[i]);
    out[i * 3 + 0] = in[idx * 3 + 0];
    out[i * 3 + 1] = in[idx * 3 + 1];
    out[i * 3 + 2] = in[idx * 3 + 2];
  }
}

template <class T>
void extract_4dpn(const mt_vector<mt_idx_t>& nod, const mt_vector<T>& in, mt_vector<T>& out) {
#ifdef OPENMP
#pragma omp parallel for schedule(dynamic, 128)
#endif
  for (size_t i = 0; i < nod.size(); i++) {
    out[i * 4 + 0] = in[nod[i] * 4 + 0];
    out[i * 4 + 1] = in[nod[i] * 4 + 1];
    out[i * 4 + 2] = in[nod[i] * 4 + 2];
    out[i * 4 + 3] = in[nod[i] * 4 + 3];
  }
}

// odat is written directly for data_idx 0 and 2
// for data_idx 1 and 3 (i.e. igb data), we are using the virtual igb_[input/output]_stream

void extract_data_mode(const short data_idx, mt_vector<mt_real>& idat, mt_vector<mt_real>& odat,
                       const mt_vector<mt_idx_t> & indices, igb_header & igb_msh, igb_header & igb_submsh)
{
  mt_timer dur;
  dur.start();

  std::cout << "Extracting data .." << std::endl;

  switch (data_idx) {
    case 0:
      odat.resize(indices.size());
      extract_1dpn(indices, idat, odat);
      break;
    case 2:
      odat.resize(3 * indices.size());
      extract_3dpn(indices, idat, odat);
      break;
    case 10:
      odat.resize(4 * indices.size());
      extract_4dpn(indices, idat, odat);
      break;

    case 1:
    case 3: {
      if (igb_msh.v_t != igb_submsh.v_t) {
        fprintf(stderr, "Number of time-slices in mesh and submesh do not match. Aborting!\n");
        exit(1);
      }

      int dpn = data_idx == 1 ? 1 : 3;
      const size_t num_time_slices = igb_submsh.v_t;

      for (size_t t = 0; t < num_time_slices; t++) {
        printf("\rcurrent time-slice %zd / %zu .. ", t + 1, num_time_slices);
        fflush(stdout);

        read_igb_slice(idat, igb_msh);
        odat.resize(indices.size() * dpn);

        if (idat.size() == (igb_msh.v_x * dpn)) {
          if(dpn == 1) extract_1dpn(indices, idat, odat);
          else         extract_3dpn(indices, idat, odat);

          write_igb_slice(odat, igb_submsh);
        } else {
          printf("\ndata dimension missmatch (%d != %d) at time-slice %zu\n",
                 (int)idat.size(), (int) igb_msh.v_x * dpn, t + 1);
          break;
        }
      }
      printf("\n");
      break;
    }

    default: break;
  }

  dur.stop();
  std::cout << "Done in " << dur.get_time_sec() << " sec" << std::endl;
}
