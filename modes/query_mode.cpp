/**
* @file query_mode.h
* @brief Meshtool query mode.
* @author Aurel Neic
* @version
* @date 2017-02-13
*/

#include "data_structs.h"
#include "mt_vector.h"
#include "shape_utils.hpp"
#include "kdtree.h"
#include "query_mode.h"


void print_query_idx_help()
{
  fprintf(stderr, "query idx: print indices in a proximity to a given coordinate\n");
  fprintf(stderr, "parameters:\n");
  fprintf(stderr, "%s<path>\t (input) path to basename of the mesh to query\n", mesh_par.c_str());
  fprintf(stderr, "%sx,y,z\t (input) triple of x,y,z coordinates\n", coord_par.c_str());
  fprintf(stderr, "%s<format>\t (optional) mesh input format. may be: %s\n", inp_format_par.c_str(), input_formats.c_str());
  fprintf(stderr, "%s<surface>\t (optional) index query is restricted to this surface\n", surf_par.c_str());
  fprintf(stderr, "%s<float>\t (optional input) threshold defining the proximity to the coordinate\n", thr_par.c_str());
  fprintf(stderr, "\n");
}
void print_query_idxlist_help()
{
  fprintf(stderr, "query idxlist: apply 'query idx' for all coordinates in a file\n");
  fprintf(stderr, "parameters:\n");
  fprintf(stderr, "%s<path>\t\t (input) path to basename of the mesh to query\n", mesh_par.c_str());
  fprintf(stderr, "%s<file>\t\t (input) the coordinates file\n", coord_par.c_str());
  fprintf(stderr, "%s<format>\t\t (optional) mesh input format. may be: %s\n", inp_format_par.c_str(), input_formats.c_str());
  fprintf(stderr, "%s<surface>\t\t (optional) index query is restricted to this surface\n", surf_par.c_str());
  fprintf(stderr, "%stag1,tag2,...\t (optional) list of region tags to apply the query to.\n", tags_par.c_str());
  fprintf(stderr, "%s<surface>\t\t (optional) index query is restricted to this surface\n", surf_par.c_str());
  fprintf(stderr, "%s<float>\t\t (optional) default threshold defining the proximity to the coordinate (default 0)\n", thr_par.c_str());
  fprintf(stderr, "\n");
  fprintf(stderr, "The format of the coordinates file is:\n");
  fprintf(stderr, "NUMBER_OF_COORDS\n");
  fprintf(stderr, "X Y Z THRESHOLD\n");
  fprintf(stderr, "X Y Z THRESHOLD\n");
  fprintf(stderr, "X Y Z THRESHOLD\n");
  fprintf(stderr, "...\n");
  fprintf(stderr, "\n");
}

void print_query_idxlist_uvc_help()
{
  fprintf(stderr, "query idxlist_uvc: apply 'query idx' for all coordinates in a file based on uvc coordinates\n");
  fprintf(stderr, "parameters:\n");
  fprintf(stderr, "%s<path>\t (input) path to basename of the mesh to perform query on\n", mesh_par.c_str());
  fprintf(stderr, "%s<format>\t (optional) mesh input format. may be: %s\n", inp_format_par.c_str(), input_formats.c_str());
  fprintf(stderr, "%s<file>\t (input) the uvc coordinates file holding the query points in uvc\n", coord_par.c_str());
  fprintf(stderr, "%s<file>\t (optional input) path to the ucv points file (default `basename`.{uvc_pts,hpts})\n", uvc_par.c_str());
  fprintf(stderr, "%s<int>\t (optional input) enable/disable vtx output for each cardiac coordinate (default 0)\n", vtx_par.c_str());
  fprintf(stderr, "%s\t\t (optional input) write extracellular vertex files\n", extra_flag.c_str());
  fprintf(stderr, "\n");
  fprintf(stderr, "The format of the coordinates file is:\n");
  fprintf(stderr, "NUMBER_OF_COORDS\n");
  fprintf(stderr, "Z RHO PHI VENT THRESHOLD EPSILON\n");
  fprintf(stderr, "Z RHO PHI VENT THRESHOLD EPSILON\n");
  fprintf(stderr, "Z RHO PHI VENT THRESHOLD EPSILON\n");
  fprintf(stderr, "...\n");
  fprintf(stderr, "VENT is -1 or 1, respectively for LV and RV\n");
  fprintf(stderr, "THRESHOLD is >= 0, in Euclidean space, for querying point clouds.\n");
  fprintf(stderr, "EPSILON is like THRESHOLD but for the RHO coordinate in UVC space.\n");
  fprintf(stderr, "\n");
}
void print_query_idxlist_ucc_help()
{
  fprintf(stderr, "query idxlist_ucc: apply 'query idx' for all coordinates in a file based on cardiac coordinates\n");
  fprintf(stderr, "parameters:\n");
  fprintf(stderr, "%s<path>\t (input) path to basename of the mesh to perform query on\n", mesh_par.c_str());
  fprintf(stderr, "%s<format>\t (optional) mesh input format. may be: %s\n", inp_format_par.c_str(), input_formats.c_str());
  fprintf(stderr, "%s<file>\t (input) path to the file holding the query points in heart coordinates\n", coord_par.c_str());
  fprintf(stderr, "%s<file>\t (optional input) path to the cardiac coordinates file (default `basename`.{ucc_pts,hpts})\n", ucc_par.c_str());
  fprintf(stderr, "%s<int>\t (optional input) enable/disable vtx output for each cardiac coordinate (default 0)\n", vtx_par.c_str());
  fprintf(stderr, "%s\t\t (optional input) write extracellular vertex files\n", extra_flag.c_str());
  fprintf(stderr, "\n");
  fprintf(stderr, "The format of the coordinates file is:\n");
  fprintf(stderr, "NUMBER_OF_COORDS\n");
  fprintf(stderr, "Z RHO PHI VENT THRESHOLD EPSILON\n");
  fprintf(stderr, "Z RHO PHI VENT THRESHOLD EPSILON\n");
  fprintf(stderr, "Z RHO PHI VENT THRESHOLD EPSILON\n");
  fprintf(stderr, "...\n");
  fprintf(stderr, "VENT is: -2 == LA, -1 == LV, 1 == RV, 2 == RA\n");
  fprintf(stderr, "THRESHOLD is >= 0, in Euclidean space, for querying point clouds.\n");
  fprintf(stderr, "EPSILON is like THRESHOLD but for the RHO coordinate in UVC space.\n");
  fprintf(stderr, "\n");
}

void print_query_vals_help()
{
  fprintf(stderr, "query values: get min / max / avrg data value for a vertex set\n");
  fprintf(stderr, "parameters:\n");
  fprintf(stderr, "%s<path>\t (input) path to the data\n", idat_par.c_str());
  fprintf(stderr, "%s<path>\t (input) path to the vertex set\n", vtx_par.c_str());
  fprintf(stderr, "%s<int>\t (input) operation type. 0 = avrg, 1 = min, 2 = max. Default is 0.\n", mode_par.c_str());
  fprintf(stderr, "\n");
}

void print_query_coords_xyz_help()
{
  fprintf(stderr, "query coords_xyz: get the XYZ coordinates for all UVC coordinates provided in a file\n");
  fprintf(stderr, "parameters:\n");
  fprintf(stderr, "%s<path>\t (input) path to basename of the mesh to perform query on\n", mesh_par.c_str());
  fprintf(stderr, "%s<format>\t (optional) mesh input format. may be: %s\n", inp_format_par.c_str(), input_formats.c_str());
  fprintf(stderr, "%s<file>\t (input) the uvc coordinates file holding the query points in uvc\n", coord_par.c_str());
  fprintf(stderr, "%s<file>\t (optional input) path to the ucv points file (default `basename`.{uvc_pts,hpts})\n", uvc_par.c_str());
  fprintf(stderr, "%s<int>\t (optional input) number of search iterations (default 10)\n", iter_par.c_str());
  fprintf(stderr, "\n");
  fprintf(stderr, "The format of the coordinates file is:\n");
  fprintf(stderr, "NUMBER_OF_COORDS\n");
  fprintf(stderr, "Z RHO PHI VENT\n");
  fprintf(stderr, "Z RHO PHI VENT\n");
  fprintf(stderr, "Z RHO PHI VENT\n");
  fprintf(stderr, "...\n");
  fprintf(stderr, "VENT is -1 or 1, respectively for LV and RV\n");
  fprintf(stderr, "\n");
}

void print_query_uccoords_xyz_help()
{
  fprintf(stderr, "query uccoords_xyz: get the XYZ coordinates for all UCC coordinates provided in a file\n");
  fprintf(stderr, "parameters:\n");
  fprintf(stderr, "%s<path>\t (input) path to basename of the mesh to perform query on\n", mesh_par.c_str());
  fprintf(stderr, "%s<format>\t (optional) mesh input format. may be: %s\n", inp_format_par.c_str(), input_formats.c_str());
  fprintf(stderr, "%s<file>\t (input) the ucc coordinates file holding the query points in ucc\n", coord_par.c_str());
  fprintf(stderr, "%s<file>\t (optional input) path to the ucv points file (default `basename`.{ucc_pts,hpts})\n", ucc_par.c_str());
  fprintf(stderr, "%s<int>\t (optional input) number of search iterations (default 10)\n", iter_par.c_str());
  fprintf(stderr, "\n");
  fprintf(stderr, "The format of the coordinates file is:\n");
  fprintf(stderr, "NUMBER_OF_COORDS\n");
  fprintf(stderr, "Z RHO PHI VENT\n");
  fprintf(stderr, "Z RHO PHI VENT\n");
  fprintf(stderr, "Z RHO PHI VENT\n");
  fprintf(stderr, "...\n");
  fprintf(stderr, "VENT is: -2 == LA, -1 == LV, 1 == RV, 2 == RA\n");
  fprintf(stderr, "\n");
}

void print_query_elem_idx_help()
{
  fprintf(stderr, "query elem_idx: get the element index a given XYZ coordinate is closest to\n");
  fprintf(stderr, "parameters:\n");
  fprintf(stderr, "%s<path>\t (input) path to basename of the mesh to perform query on\n", mesh_par.c_str());
  fprintf(stderr, "%s<file>\t (input) points file of the xyz coordinates\n", coord_par.c_str());
  fprintf(stderr, "%s<file>\t (optional input) path to the output file (default `elem_idx_out.dat`)\n", odat_par.c_str());
  fprintf(stderr, "\n");
}

void print_query_tags_help()
{
  fprintf(stderr, "query tags: print the tags present in a given mesh\n");
  fprintf(stderr, "parameters:\n");
  fprintf(stderr, "%s<path>\t (input) path to basename of the mesh to query\n", mesh_par.c_str());
  fprintf(stderr, "%s<vtx file>\t (optional) .vtx vertex file. query will only apply to connected elems. \n", vtx_par.c_str());
  fprintf(stderr, "%s<format>\t (optional) mesh input format. may be: %s\n", inp_format_par.c_str(), input_formats.c_str());
  fprintf(stderr, "\n");
}
void print_query_bbox_help()
{
  fprintf(stderr, "query bbox: print the bounding box of a given mesh\n");
  fprintf(stderr, "parameters:\n");
  fprintf(stderr, "%s<path>\t (input) path to basename of the mesh to query\n", mesh_par.c_str());
  fprintf(stderr, "%s<format>\t (optional) mesh input format. may be: %s\n", inp_format_par.c_str(), input_formats.c_str());
  fprintf(stderr, "\n");
}
void print_query_edges_help()
{
  fprintf(stderr, "query edges: print several statistics related to the mesh edges\n");
  fprintf(stderr, "parameters:\n");
  fprintf(stderr, "%s<path>\t (input) path to basename of the mesh to query\n", mesh_par.c_str());
  fprintf(stderr, "%s<format>\t (optional) mesh input format. may be: %s\n", inp_format_par.c_str(), input_formats.c_str());
  fprintf(stderr, "%stag1,tag2  (optional) List of region tags to compute the edge statistics on.\n", tags_par.c_str());
  fprintf(stderr, "%s<path>\t (optional) If set, the query data will be written to disk.\n", odat_par.c_str());
  fprintf(stderr, "\n");
}
void print_query_quality_help()
{
  fprintf(stderr, "query quality: print mesh quality statistics\n");
  fprintf(stderr, "parameters:\n");
  fprintf(stderr, "%s<path>\t (input) path to basename of the mesh to query\n", mesh_par.c_str());
  fprintf(stderr, "%s<format>\t (optional) mesh input format. may be: %s\n", inp_format_par.c_str(), input_formats.c_str());
  fprintf(stderr, "%s<float>\t (optional) output exact number of elements with quality above this threshold\n", thr_par.c_str());
  fprintf(stderr, "%s<int>\t (optional) 0 = query only quality, 1 = query also self-intersection. Default is 0.\n", mode_par.c_str());
  fprintf(stderr, "%s<path>\t (optional) If set, the query data will be written to disk.\n", odat_par.c_str());
  fprintf(stderr, "\n");
}
void print_query_resqual_help()
{
  fprintf(stderr, "query resqual: print mesh resolution quality statistics, based on a flow-field\n");
  fprintf(stderr, "parameters:\n");
  fprintf(stderr, "%s<path>\t (input) path to basename of the mesh to query\n", mesh_par.c_str());
  fprintf(stderr, "%s<path>\t (input) path to basename of the associated element vector data\n", idat_par.c_str());
  fprintf(stderr, "%s<format>\t (optional) mesh input format. may be: %s\n", inp_format_par.c_str(), input_formats.c_str());
  fprintf(stderr, "%s<path>\t (optional) If set, the query data will be written to disk.\n", odat_par.c_str());
  fprintf(stderr, "\n");
}
void print_query_distance_help()
{
  fprintf(stderr, "query distance: query min/max/average distance of mesh to surface mesh\n");
  fprintf(stderr, "parameters:\n");
  fprintf(stderr, "%s<path>\t (input) path to basename of the mesh\n", mesh_par.c_str());
  fprintf(stderr, "%s<path>\t (input) path to basename of the surface mesh\n", surf_par.c_str());
  fprintf(stderr, "%s<format>\t (optional) mesh input format. may be: %s\n", inp_format_par.c_str(), input_formats.c_str());
  fprintf(stderr, "\n");
}
void print_query_graph_help()
{
  fprintf(stderr, "query graph: print the nodal connectivity graph\n");
  fprintf(stderr, "parameters:\n");
  fprintf(stderr, "%s<path>\t (input) path to basename of the mesh to query\n", mesh_par.c_str());
  fprintf(stderr, "%s<format>\t (optional) mesh input format. may be: %s\n", inp_format_par.c_str(), input_formats.c_str());
  fprintf(stderr, "\n");
}
void print_query_smth_help()
{
  fprintf(stderr, "query smoothness: compute the nodal smoothness\n");
  fprintf(stderr, "parameters:\n");
  fprintf(stderr, "%s<path>\t (input) path to basename of the mesh to query\n", mesh_par.c_str());
  fprintf(stderr, "%s<surface>\t (input) surface to compute the smoothness for\n", surf_par.c_str());
  fprintf(stderr, "%s<format>\t (optional) mesh input format. may be: %s\n", inp_format_par.c_str(), input_formats.c_str());
  fprintf(stderr, "\n");
}
void print_query_curv_help()
{
  fprintf(stderr, "query curvature: compute the curvature of a surface.\n"
                  "In each node, a patch of adjustable size will be used to determine local curvature.\n\n");
  fprintf(stderr, "parameters:\n");
  fprintf(stderr, "%s<path>\t (input) path to basename of the mesh to query\n", mesh_par.c_str());
  fprintf(stderr, "%s<float>\t (input) patch radius in mesh units for curvature estimation\n", size_par.c_str());
  fprintf(stderr, "%s<path>\t (output, optional) path to data out\n", odat_par.c_str());
  fprintf(stderr, "%s<surface>\t (optional) surface to compute the curvature for\n", surf_par.c_str());
  fprintf(stderr, "%s<format>\t (optional) mesh input format. may be: %s\n", inp_format_par.c_str(), input_formats.c_str());
  fprintf(stderr, "\n");
}
void print_query_inside_help()
{
  fprintf(stderr, "query insidepoint: get a point inside a given closed surface.\n");
  fprintf(stderr, "parameters:\n");
  fprintf(stderr, "%s<path>\t (input) path to the mesh of a closed surface\n", mesh_par.c_str());
  fprintf(stderr, "%s<format>\t (optional) mesh input format. may be: %s\n", inp_format_par.c_str(), input_formats.c_str());
  fprintf(stderr, "\n");
}
void print_query_normals_help()
{
  fprintf(stderr, "query normals: output normal vectors for a given mesh and surface.\n");
  fprintf(stderr, "parameters:\n");
  fprintf(stderr, "%s<path>\t (input) path to basename of the mesh to query\n", mesh_par.c_str());
  fprintf(stderr, "%s<surface>\t (optional) surface to compute the normals for\n", surf_par.c_str());
  fprintf(stderr, "%s<format>\t (optional) mesh input format. may be: %s\n", inp_format_par.c_str(), input_formats.c_str());
  fprintf(stderr, "\n");
}
void print_query_nonmanifold_help()
{
  fprintf(stderr, "query nonmanifold: find nonmanifold elements and edges in a given surface.\n");
  fprintf(stderr, "parameters:\n");
  fprintf(stderr, "%s<path>\t (input) path to basename of the mesh to query\n", mesh_par.c_str());
  fprintf(stderr, "%s<path>\t (optional) basename of output file (default: `non_manifold`)\n", odat_par.c_str());
  fprintf(stderr, "%s<format>\t (optional) mesh input format. may be: %s\n", inp_format_par.c_str(), input_formats.c_str());
  fprintf(stderr, "\n");
}

int query_parse_options(int argc, char** argv, struct query_options & opts)
{
  if(argc < 3) {
    std::cerr << "Please choose one of the following query modes: " << std::endl << std::endl;
    print_query_bbox_help();
    print_query_edges_help();
    print_query_graph_help();
    print_query_idx_help();
    print_query_idxlist_help();
    print_query_idxlist_uvc_help();
    print_query_idxlist_ucc_help();
    print_query_coords_xyz_help();
    print_query_uccoords_xyz_help();
    print_query_elem_idx_help();
    print_query_quality_help();
    print_query_resqual_help();
    print_query_tags_help();
    print_query_smth_help();
    print_query_curv_help();
    print_query_inside_help();
    print_query_normals_help();
    print_query_vals_help();
    print_query_distance_help();
    print_query_nonmanifold_help();
    return 1;
  }

  std::string query_type = argv[2];
  std::string msh_base, ifmt;

  // parse all query parameters -----------------------------------------------------------------
  for(int i=3; i<argc; i++){
    std::string param = reformat_param(argv, i, argc);
    bool match = false;

    if(!match) match = parse_param(param, mesh_par, msh_base);
    if(!match) match = parse_param(param, coord_par, opts.coord);
    if(!match) match = parse_param(param, uvc_par, opts.uvc);
    if(!match) match = parse_param(param, ucc_par, opts.ucc);
    if(!match) match = parse_param(param, thr_par, opts.thr);
    if(!match) match = parse_param(param, surf_par, opts.surf);
    if(!match) match = parse_param(param, inp_format_par, ifmt);
    if(!match) match = parse_param(param, tags_par, opts.tags);
    if(!match) match = parse_param(param, size_par, opts.rad);
    if(!match) match = parse_param(param, idat_par, opts.idat);
    if(!match) match = parse_param(param, odat_par, opts.odat);
    if(!match) match = parse_param(param, mode_par, opts.mode);
    if(!match) match = parse_param(param, vtx_par, opts.vtx);
    if(!match) match = parse_param(param, iter_par, opts.iter);
    if(!match) match = parse_flag(param, extra_flag, opts.extra_vtx);

    if(!match) {
      std::cerr << "Error: Cannot parse parameter " << param << std::endl;
      return 2;
    }
  }
  fixBasename(opts.surf);
  opts.msh.assign(msh_base, ifmt);

  // check if all relevant parameters have been set ---------------------------------------------------
  if(query_type.compare("idx") == 0)
  {
    opts.type = QRY_IDX;

    if( !(opts.msh.isSet() && opts.coord.size() > 0) )
    {
      std::cerr << "Query index error: Insufficient parameters provided." << std::endl;
      print_query_idx_help();
      return 3;
    }
  }
  else if(query_type.compare("idxlist") == 0)
  {
    opts.type = QRY_IDXLIST;

    if( !(opts.msh.isSet() && opts.coord.size() > 0) )
    {
      std::cerr << "Query idxlist error: Insufficient parameters provided." << std::endl;
      print_query_idxlist_help();
      return 3;
    }
  }
  else if(query_type.compare("idxlist_uvc") == 0)
  {
    opts.type = QRY_IDXLIST_UVC;
    if( !(opts.msh.isSet() && opts.coord.size() > 0) )
    {
      std::cerr << "Query idxlist_uvc error: Insufficient parameters provided." << std::endl;
      print_query_idxlist_uvc_help();
      return 3;
    }
  }
  else if(query_type.compare("idxlist_ucc") == 0)
  {
    opts.type = QRY_IDXLIST_UCC;
    if( !(opts.msh.isSet() && opts.coord.size() > 0) )
    {
      std::cerr << "Query idxlist_ucc error: Insufficient parameters provided." << std::endl;
      print_query_idxlist_ucc_help();
      return 3;
    }
  }
  else if(query_type.compare("coords_xyz") == 0)
  {
    opts.type = QRY_UVCOORDS_XYZ;
    if( !(opts.msh.isSet() && opts.coord.size() > 0) )
    {
      std::cerr << "Query coords_xyz error: Insufficient parameters provided." << std::endl;
      print_query_coords_xyz_help();
      return 3;
    }
  }
  else if(query_type.compare("uccoords_xyz") == 0)
  {
    opts.type = QRY_UCCOORDS_XYZ;
    if( !(opts.msh.isSet() && opts.coord.size() > 0) )
    {
      std::cerr << "Query uccoords_xyz error: Insufficient parameters provided." << std::endl;
      print_query_uccoords_xyz_help();
      return 3;
    }
  }
  else if(query_type.compare("elem_idx") == 0)
  {
    opts.type = QRY_ELEM_IDX;
    if( !(opts.msh.isSet() && opts.coord.size() > 0) )
    {
      std::cerr << "Query elem_idx error: Insufficient parameters provided." << std::endl;
      print_query_elem_idx_help();
      return 3;
    }
  }
  else if(query_type.compare("values") == 0)
  {
    opts.type = QRY_VALS;

    if(!(opts.idat.size() && opts.vtx.size()))
    {
      std::cerr << "Query values error: Insufficient parameters provided." << std::endl;
      print_query_vals_help();
      return 3;
    }
  }
  else if(query_type.compare("tags") == 0)
  {
    opts.type = QRY_TAGS;

    if(!opts.msh.isSet())
    {
      std::cerr << "Query tags error: Insufficient parameters provided." << std::endl;
      print_query_tags_help();
      return 3;
    }
  }
  else if(query_type.compare("distance") == 0)
  {
    opts.type = QRY_DISTANCE;

    if(!(opts.msh.isSet() && opts.surf.size()))
    {
      std::cerr << "Query distance error: Insufficient parameters provided." << std::endl;
      print_query_distance_help();
      return 3;
    }
  }
  else if(query_type.compare("bbox") == 0)
  {
    opts.type = QRY_BBOX;

    if(!opts.msh.isSet())
    {
      std::cerr << "Query bbox error: Insufficient parameters provided." << std::endl;
      print_query_bbox_help();
      return 3;
    }
  }
  else if(query_type.compare("edges") == 0)
  {
    opts.type = QRY_EDGE;

    if(!opts.msh.isSet())
    {
      std::cerr << "Query edges error: Insufficient parameters provided." << std::endl;
      print_query_edges_help();
      return 3;
    }
  }
  else if(query_type.compare("quality") == 0)
  {
    opts.type = QRY_QUAL;

    if(!opts.msh.isSet())
    {
      std::cerr << "Query quality error: Insufficient parameters provided." << std::endl;
      print_query_quality_help();
      return 3;
    }
  }
  else if(query_type.compare("resqual") == 0)
  {
    opts.type = QRY_RESQUAL;

    if(!(opts.msh.isSet() || opts.idat.size()))
    {
      std::cerr << "Query resolution quality error: Insufficient parameters provided." << std::endl;
      print_query_resqual_help();
      return 3;
    }
  }
  else if(query_type.compare("graph") == 0)
  {
    opts.type = QRY_GRPH;

    if(!opts.msh.isSet())
    {
      std::cerr << "Query graph error: Insufficient parameters provided." << std::endl;
      print_query_graph_help();
      return 3;
    }
  }
  else if(query_type.compare("smoothness") == 0)
  {
    opts.type = QRY_SMTH;

    if( !(opts.msh.isSet() && opts.surf.size()) )
    {
      std::cerr << "Query smoothness error: Insufficient parameters provided." << std::endl;
      print_query_smth_help();
      return 3;
    }
  }
  else if(query_type.compare("curvature") == 0)
  {
    opts.type = QRY_CURV;

    if( !(opts.msh.isSet() && opts.rad.size() ) )
    {
      std::cerr << "Query curvature error: Insufficient parameters provided." << std::endl;
      print_query_curv_help();
      return 3;
    }
  }
  else if(query_type.compare("insidepoint") == 0)
  {
    opts.type = QRY_INSIDE;

    if( !opts.msh.isSet() )
    {
      std::cerr << "Query insidepoint error: Insufficient parameters provided." << std::endl;
      print_query_inside_help();
      return 3;
    }
  }
  else if(query_type.compare("normals") == 0)
  {
    opts.type = QRY_NRMLS;

    if(! opts.msh.isSet())
    {
      std::cerr << "Query normals error: Insufficient parameters provided." << std::endl;
      print_query_normals_help();
      return 3;
    }
  }
  else if(query_type.compare("nonmanifold") == 0)
  {
    opts.type = QRY_NON_MANIFOLD;

    if(! opts.msh.isSet())
    {
      std::cerr << "Query nonmanifold error: Insufficient parameters provided." << std::endl;
      print_query_nonmanifold_help();
      return 3;
    }
  }
  else {
    print_usage(argv[0]);
    return 4;
  }

  return 0;
}



/// read coordinates and thresholds for UVC with ordering of the points in the file (z, rho, phi) -> (rho, phi, z)
template<class S>
void read_coord_file_uvc(mt_vector<S> & uvc_coords,
                         mt_vector<bool> & is_lv,
                         mt_vector<S> & thr,
                         mt_vector<S> & eps,
                         std::string file)
{
  FILE* fd = fopen(file.c_str(), MT_FOPEN_READ);
  if(!fd) {
    treat_file_open_error(file);
    exit(1);
  }

  int numcoord;
  fscanf(fd, "%d", &numcoord);

  uvc_coords.resize(0); uvc_coords.reserve(3*numcoord);
  thr.resize(0); thr.reserve(numcoord);
  eps.resize(0); eps.reserve(numcoord);

  is_lv.assign(numcoord, false);

  float c[3];
  float v, t, e;

  for(int i=0; i<numcoord; i++)
  {
    fscanf(fd, "%f %f %f %f %f %f", c, c+1, c+2, &v, &t, &e);
    if(e <= 0.0f)
      e = std::numeric_limits<float>::max();

    uvc_coords.push_back(c[1]);
    uvc_coords.push_back(c[2]);
    uvc_coords.push_back(c[0]);
    thr.push_back(t);
    eps.push_back(e);

    if(v < 0.0)
      is_lv[i] = true;
  }

  fclose(fd);
}

// read heart coordinates and thresholds
// for the UVCs:
//   in file (z, rho, phi) -> in memory (rho, phi, z)
template<class S>
void read_heart_coord_file(mt_vector<S>& coords, mt_vector<int8_t>& ven,
                           mt_vector<S> & thr, mt_vector<S> & eps, const std::string& file)
{
  FILE* fd = fopen(file.c_str(), MT_FOPEN_READ);
  if (!fd) {
    treat_file_open_error(file);
    exit(1);
  }

  int numcoord;
  fscanf(fd, "%d", &numcoord);

  coords.resize(0); coords.resize(3*numcoord); coords.zero();
  ven.resize(0); ven.resize(numcoord); ven.zero();
  thr.resize(0); thr.resize(numcoord);
  eps.resize(0); eps.resize(numcoord);

  float c[3];
  float v, t, e;
  for(int i=0; i<numcoord; i++)
  {
    fscanf(fd, "%f %f %f %f %f %f", c, c+1, c+2, &v, &t, &e);

    if (!(e > 0.0f)) e = std::numeric_limits<float>::max();

    if ((-2.5 < v) && (v < -1.5))
      ven[i] = -2;
    else if ((-1.5 < v) && (v < -0.5))
      ven[i] = -1;
    else if ((0.5 < v) && (v < 1.5))
      ven[i] = 1;
    else if ((1.5 < v) && (v < 2.5))
      ven[i] = 2;

    if ((ven[i] == -1) || (ven[i] == 1)) {
      coords[3*i+0] = c[1];
      coords[3*i+1] = c[2];
      coords[3*i+2] = c[0];
      thr[i] = t;
      eps[i] = e;
    }
    else if ((ven[i] == -2) || (ven[i] == 2)) {
      coords[3*i+0] = c[0];
      coords[3*i+1] = c[1];
      coords[3*i+2] = c[2];
      thr[i] = t;
      eps[i] = e;
    }
  }
  fclose(fd);
}

// read coordinates and thresholds for UCC with re-ordering
// of the UVC points in the file (z, rho, phi) -> (rho, phi, z)
template<class S>
void read_coord_file_ucc(mt_vector<S> & ucc_coords,
                         mt_vector<int8_t> & ucc_chmbr,
                         mt_vector<S> & thr,
                         mt_vector<S> & eps,
                         std::string file)
{
  FILE* fd = fopen(file.c_str(), MT_FOPEN_READ);
  if(!fd) {
    treat_file_open_error(file);
    exit(1);
  }

  int numcoord;
  fscanf(fd, "%d", &numcoord);

  ucc_coords.resize(0);
  ucc_coords.reserve(3*numcoord);
  thr.resize(0); thr.reserve(numcoord);
  eps.resize(0); eps.reserve(numcoord);

  ucc_chmbr.resize(numcoord);
  ucc_chmbr.zero();

  float c[3];
  float v, t, e;

  for(int i=0; i<numcoord; i++)
  {
    fscanf(fd, "%f %f %f %f %f %f", c, c+1, c+2, &v, &t, &e);

    if ((-2.5 < v) && (v < -1.5)) {
      ucc_coords.push_back(c[0]);
      ucc_coords.push_back(c[1]);
      ucc_coords.push_back(c[2]);
      ucc_chmbr[i] = -2;
    }
    else if ((-1.5 < v) && (v < -0.5)) {
      ucc_coords.push_back(c[1]);
      ucc_coords.push_back(c[2]);
      ucc_coords.push_back(c[0]);
      ucc_chmbr[i] = -1;
    }
    else if ((0.5 < v) && (v < 1.5)) {
      ucc_coords.push_back(c[1]);
      ucc_coords.push_back(c[2]);
      ucc_coords.push_back(c[0]);
      ucc_chmbr[i] = 1;
    }
    else if ((1.5 < v) && (v < 2.5)) {
      ucc_coords.push_back(c[0]);
      ucc_coords.push_back(c[1]);
      ucc_coords.push_back(c[2]);
      ucc_chmbr[i] = 2;
    }
    else {
      ucc_coords.push_back(0.0);
      ucc_coords.push_back(0.0);
      ucc_coords.push_back(0.0);
    }

    if (e <= 0.0f)
      e = std::numeric_limits<float>::max();

    thr.push_back(t);
    eps.push_back(e);

  }

  fclose(fd);
}

/// write computed vertex indices
template<class T>
void write_vtxlist_file(mt_vector<mt_vector<T>> & vtxlist, std::string file)
{
  FILE* fd = fopen(file.c_str(), MT_FOPEN_WRITE);
  if(!fd) {
    treat_file_open_error(file);
    exit(1);
  }

  fprintf(fd, "%d\n", (int)vtxlist.size());

  for(size_t i=0; i<vtxlist.size(); i++)
  {
    T* it = vtxlist[i].begin();
    while(it != vtxlist[i].end()) {
      fprintf(fd, "%ld ", long(*it));
      it++;
    }
    fprintf(fd, "\n");
  }

  fclose(fd);
}



void get_value_bounds(const mt_vector<mt_real> & vals, const mt_vector<mt_idx_t> & idx,
                      mt_real & min, mt_real & max, mt_real & avrg)
{
  if(idx.size() == 0)
    return;

  avrg = 0.0;
  min = vals[idx[0]], max = vals[idx[0]];

  for(mt_idx_t i : idx) {
    mt_real v = vals[i];
    if(min > v) min = v;
    if(max < v) max = v;
    avrg += v;
  }

  avrg /= mt_real(idx.size());
}

void query_mode(int argc, char** argv)
{
  struct query_options opts;
  struct timeval t1, t2;

  {
    int ret = query_parse_options(argc, argv, opts);
    if(ret != 0) return;
  }

  mt_meshdata mesh;

  switch(opts.type) {
    case QRY_IDX: {
      // query index  ------------------------------------------------------------
      mt_vector<std::string> c;
      split_string(opts.coord, ',', c);

      if(c.size() != 3) {
        std::cerr << "Error: Number of provided coords != 3 ! Aborting!" << std::endl;
        break;
      }

      float c1 = atof(c[0].c_str());
      float c2 = atof(c[1].c_str());
      float c3 = atof(c[2].c_str());

      std::cout << "Reading points of mesh: " << opts.msh.base << std::endl;
      gettimeofday(&t1, NULL);
      read_mesh_selected(mesh, opts.msh.format, opts.msh.base, CRP_READ_PTS);
      gettimeofday(&t2, NULL);
      std::cout << "Done in " << (float)timediff_sec(t1, t2) << " sec" << std::endl;

      mt_meshdata surfmesh;
      mt_vector<mt_idx_t> nod;

      if(opts.surf.size() > 0) {
        std::cout << "Restricting index search to surface: " << opts.surf + SURF_EXT << std::endl;
        readElements(surfmesh, opts.surf + SURF_EXT);
        nod = (surfmesh).e2n_con;
        binary_sort(nod); unique_resize(nod);
        nod.shrink_to_fit();
      }
      else {
        nod.resize(mesh.xyz.size() / 3);
        for(size_t i=0; i<nod.size(); i++) nod[i] = i;
      }

      mt_real thr = 0;
      if(opts.thr.size() > 0) thr = atof(opts.thr.c_str());
      mt_vector<mt_idx_t> vtxlist;

      gettimeofday(&t1, NULL);
      mt_real mindist = query_idx_mode(mesh, nod, vec3r(c1, c2, c3), thr, vtxlist);
      gettimeofday(&t2, NULL);

      std::cout << "Done in " << (float)timediff_sec(t1, t2) << " sec" << std::endl;
      if(vtxlist.size()) {
        if(thr > 0) {
            std::sort(vtxlist.begin(), vtxlist.end());
            std::cout << "Vertex list:" << std::endl;
            for(size_t i=0; i<vtxlist.size() - 1; i++)
              std::cout << vtxlist[i] << ",";
            std::cout << vtxlist[vtxlist.size() - 1] << std::endl;
        } else {
            std::cout << "Vertex Idx: " << vtxlist[0] << " Distance: " << sqrt(mindist) << std::endl;
        }
      } else {
        std::cerr << "No vertices found!" << std::endl;
      }

      break;
    }

    case QRY_IDXLIST: {

      mt_vector<mt_real> coords;
      mt_vector<mt_real> thr;
      mt_vector<mt_idx_t> nod;
      mt_vector<mt_real> xyz;
      mt_vector<mt_vector<mt_idx_t>> vtxlist;

      query_idxlist_mode_read(opts, coords, thr, nod, xyz);
      query_idxlist_mode(coords, thr, nod, xyz, vtxlist);

      std::string outfile = opts.coord + ".out.txt";
      write_vtxlist_file(vtxlist, outfile);
      std::cout << "Wrote " << outfile << std::endl;
      break;
    }
    case QRY_IDXLIST_UVC: {
      // query index list  ------------------------------------------------------------
      mt_vector<mt_real> UVCs, xyz_cart;
      mt_mask is_LV;

      gettimeofday(&t1, NULL);
      if (opts.uvc.empty()) {
        opts.uvc = opts.msh.base + UVC_PTS_EXT;
        if (!file_exists(opts.uvc)) {
          opts.uvc = opts.msh.base + HRT_PTS_EXT;
          if (!file_exists(opts.uvc)) {
            std::cerr << "Error: Faild to open UVC file !" << std::endl;
            break;
          }
        }
      }
      readUVCPoints(UVCs, is_LV, opts.uvc);
      readPoints_general(xyz_cart, opts.msh.base);
      gettimeofday(&t2, NULL);
      std::cout << "Done in " << (float)timediff_sec(t1, t2) << " sec" << std::endl;

      if (UVCs.size() != xyz_cart.size()) {
        std::cerr << "Error: Number of points does not match the number of coordinates!" << std::endl;
        break;
      }

      bool output_vtx = false;
      if ((opts.vtx == "1") || (opts.vtx == "yes") || (opts.vtx == "true"))
        output_vtx = true;
      else if ((opts.vtx == "0") || (opts.vtx == "no") || (opts.vtx == "false"))
        output_vtx = false;
      else
        std::cerr << "Warning: unknown value for argument '-vtx', set to default!" << std::endl;

      std::cout << "Reading uvc coord file " << opts.coord << std::endl;
      mt_vector<mt_real> coords, thr, eps;
      mt_vector<bool> coord_is_lv;
      read_coord_file_uvc(coords, coord_is_lv, thr, eps, opts.coord);

      gettimeofday(&t1, NULL);
      mt_vector<mt_vector<mt_idx_t>> vtxlist;
      query_idxlist_uvc_mode(coords, coord_is_lv, thr, eps, xyz_cart, UVCs, is_LV, vtxlist);
      gettimeofday(&t2, NULL);
      std::cout << "Done in " << (float)timediff_sec(t1, t2) << " sec" << std::endl;

      std::string outbase = opts.coord;
      remove_extension(outbase);

      std::string outfile = outbase + ".out.txt";
      write_vtxlist_file(vtxlist, outfile);
      std::cout << "Wrote " << outfile << std::endl;

      if (output_vtx) {
        const short header = (opts.extra_vtx) ? WRITE_VTX_EXTRA : WRITE_VTX_INTRA;
        for (size_t i=0; i<vtxlist.size(); i++) {
          const std::string file_name = outbase + "_coord" + std::to_string(i) + VTX_EXT;
          write_vtx(vtxlist[i], file_name, header);
        }
      }

      break;
    }
    case QRY_IDXLIST_UCC: {
      // query index list  ------------------------------------------------------------
      mt_vector<mt_real> hcoords, xyz_cart;
      mt_vector<int8_t> ventricle;

      gettimeofday(&t1, NULL);
      if (opts.ucc.empty()) {
        opts.ucc = opts.msh.base + UCC_PTS_EXT;
        if (!file_exists(opts.ucc)) {
          opts.ucc = opts.msh.base + HRT_PTS_EXT;
          if (!file_exists(opts.ucc)) {
            std::cerr << "Error: Faild to open UCC file !" << std::endl;
            break;
          }
        }
      }
      std::cout << "Reading point files " << opts.ucc << " and " << opts.msh.base << std::endl;
      readCardiacCoordinates(hcoords, ventricle, opts.ucc);
      readPoints_general(xyz_cart, opts.msh.base);
      gettimeofday(&t2, NULL);
      std::cout << "Done in " << (float)timediff_sec(t1, t2) << " sec" << std::endl;
      if (hcoords.size() != xyz_cart.size()) {
        std::cerr << "Error: Number of points does not match the number of coordinates!" << std::endl;
        break;
      }

      bool output_vtx = false;
      if ((opts.vtx == "1") || (opts.vtx == "yes") || (opts.vtx == "true"))
        output_vtx = true;
      else if ((opts.vtx == "0") || (opts.vtx == "no") || (opts.vtx == "false"))
        output_vtx = false;
      else
        std::cerr << "Warning: unknown value for argument '-vtx', set to default!" << std::endl;

      mt_vector<vec3r> hcoords_vec;
      array_to_points(hcoords, hcoords_vec);
      hcoords.clear();

      gettimeofday(&t1, NULL);
      std::cout << "Reading UCC coord file " << opts.coord << std::endl;
      mt_vector<mt_real> coords, thr, eps;
      mt_vector<int8_t> ven;
      read_heart_coord_file(coords, ven, thr, eps, opts.coord);
      gettimeofday(&t2, NULL);
      std::cout << "Done in " << (float)timediff_sec(t1, t2) << " sec" << std::endl;

      gettimeofday(&t1, NULL);
      kdtree tree_cart(10);
      mt_vector<vec3r> ptsvec_cart;
      array_to_points(xyz_cart, ptsvec_cart);
      tree_cart.build_vertex_tree(ptsvec_cart);

      // split heart coordinates of mesh into four sets to set up seperate kdtrees
      mt_vector<vec3r> hcoords_lv, hcoords_rv, hcoords_la, hcoords_ra;
      mt_vector<int> lv_hcoord_idx, rv_hcoord_idx, la_hcoord_idx, ra_hcoord_idx;

      for(size_t i=0; i < hcoords_vec.size(); i++) {
        if (ventricle[i] == -2) {
          la_hcoord_idx.push_back(i);
          hcoords_la.push_back(hcoords_vec[i]);
        }
        else if (ventricle[i] == -1) {
          lv_hcoord_idx.push_back(i);
          hcoords_lv.push_back(hcoords_vec[i]);
        }
        else if (ventricle[i] == 1) {
          rv_hcoord_idx.push_back(i);
          hcoords_rv.push_back(hcoords_vec[i]);
        }
        else if (ventricle[i] == 2) {
          ra_hcoord_idx.push_back(i);
          hcoords_ra.push_back(hcoords_vec[i]);
        }
      }

      // count how many lv,rv,la,ra coord inputs
      mt_vector<int> lv_coord_idx, rv_coord_idx, la_coord_idx, ra_coord_idx;
      for (size_t i=0; i < ven.size(); i++)
      {
        if (ven[i] == -2) la_coord_idx.push_back(i);
        else if (ven[i] == -1) lv_coord_idx.push_back(i);
        else if (ven[i] == 1) rv_coord_idx.push_back(i);
        else if (ven[i] == 2) ra_coord_idx.push_back(i);
      }

      std::cout << std::endl
                << hcoords_vec.size() << " heart coordinates consist of:" << std::endl
                << "  " << hcoords_lv.size() << " LV coordinates" << std::endl
                << "  " << hcoords_rv.size() << " RV coordinates" << std::endl
                << "  " << hcoords_la.size() << " LA coordinates" << std::endl
                << "  " << hcoords_ra.size() << " RA coordinates" << std::endl
                << std::endl
                << ven.size() << " input consists of:" << std::endl
                << "  " << lv_coord_idx.size() << " LV coordinates" << std::endl
                << "  " << rv_coord_idx.size() << " RV coordinates" << std::endl
                << "  " << la_coord_idx.size() << " LA coordinates" << std::endl
                << "  " << ra_coord_idx.size() << " RA coordinates" << std::endl
                << std::endl;

      mt_vector<mt_vector<mt_idx_t>> vtxlist(ven.size());

      // left ventricle
      if ((hcoords_lv.size() > 0) && (lv_coord_idx.size() > 0))
      {
        std::cout << "Searching for vertices in LV .." << std::endl;

        kdtree kd_tree(10);
        kd_tree.build_vertex_tree(hcoords_lv);

        vec3r ctr, dummy;
        mt_real dummy_len = 0.0;
        int idx = 0;
        for (int i : lv_coord_idx)
        {
          ctr.assign(coords[3*i+0], coords[3*i+1], coords[3*i+2]);
          idx = 0;
          kd_tree.closest_vertex(ctr, idx, dummy, dummy_len);  // closest vertex in LV heart coordinates
          mt_idx_t mesh_idx = lv_hcoord_idx[idx];
          // Now locate all points in the given thresholds
          if (thr[i] > 0.0)
          {
            mt_vector<mt_mixed_tuple<mt_real, int> > vertices_cart;
            tree_cart.vertices_in_sphere(ptsvec_cart[mesh_idx], thr[i], vertices_cart);
            // Throw out anything that has a rho value outside of the epsilon-box
            for (auto & v : vertices_cart)
              if ((ventricle[v.v2] == -1) && (std::abs(hcoords_vec[v.v2].x-hcoords_vec[mesh_idx].x) < eps[i]))
                vtxlist[i].push_back(v.v2);
          }
          else
            vtxlist[i].push_back(mesh_idx);
        }
      }
      else {
        for(int i : lv_coord_idx)
          vtxlist[i].push_back(-1);
      }

      // right ventricle
      if ((hcoords_rv.size() > 0) && (rv_coord_idx.size() > 0))
      {
        std::cout << "Searching for vertices in RV .." << std::endl;

        kdtree kd_tree(10);
        kd_tree.build_vertex_tree(hcoords_rv);

        vec3r ctr, dummy;
        mt_real dummy_len = 0.0;
        int idx = 0;
        for(int i : rv_coord_idx)
        {
          ctr.assign(coords[3*i+0], coords[3*i+1], coords[3*i+2]);
          idx = 0;
          kd_tree.closest_vertex(ctr, idx, dummy, dummy_len);  // closest vertex in RV heart coordinates
          mt_idx_t mesh_idx = rv_hcoord_idx[idx];
          // Now locate all points in the given thresholds
          if (thr[i] > 0.0)
          {
            mt_vector<mt_mixed_tuple<mt_real,int> > vertices_cart;
            tree_cart.vertices_in_sphere(ptsvec_cart[mesh_idx], thr[i], vertices_cart);
            // Throw out anything that has a rho value outside of the epsilon-box
            for(auto & v : vertices_cart)
              if ((ventricle[v.v2] == 1) && (std::abs(hcoords_vec[v.v2].x-hcoords_vec[mesh_idx].x) < eps[i]))
                vtxlist[i].push_back(v.v2);
          }
          else
            vtxlist[i].push_back(mesh_idx);
        }
      }
      else {
        for (int i : rv_coord_idx)
          vtxlist[i].push_back(-1);
      }

      // left atria
      if ((hcoords_la.size() > 0) && (la_coord_idx.size() > 0))
      {
        std::cout << "Searching for vertices in LA .." << std::endl;

        kdtree kd_tree(10);
        kd_tree.build_vertex_tree(hcoords_la);

        vec3r ctr, dummy;
        mt_real dummy_len = 0.0;
        int idx = 0;
        for (int i : la_coord_idx)
        {
          ctr.assign(coords[3*i+0], coords[3*i+1], coords[3*i+2]);
          idx = 0;
          kd_tree.closest_vertex(ctr, idx, dummy, dummy_len);  // closest vertex in LA heart coordinates
          mt_idx_t mesh_idx = la_hcoord_idx[idx];
          // Now locate all points in the given thresholds
          if (thr[i] > 0.0)
          {
            mt_vector<mt_mixed_tuple<mt_real, int> > vertices_cart;
            tree_cart.vertices_in_sphere(ptsvec_cart[mesh_idx], thr[i], vertices_cart);

            // Throw out anything that has a rho value outside of the epsilon-box
            for (auto & v : vertices_cart)
              if ((ventricle[v.v2] == -2) && (std::abs(hcoords_vec[v.v2].z-hcoords_vec[mesh_idx].z) < eps[i]))
                vtxlist[i].push_back(v.v2);
          }
          else
            vtxlist[i].push_back(mesh_idx);
        }
      }
      else {
        for(int i : la_coord_idx)
          vtxlist[i].push_back(-1);
      }

      // right atria
      if ((hcoords_ra.size() > 0) && (ra_coord_idx.size() > 0))
      {
        std::cout << "Searching for vertices in RA .." << std::endl;

        kdtree kd_tree(10);
        kd_tree.build_vertex_tree(hcoords_ra);

        vec3r ctr, dummy;
        mt_real dummy_len = 0.0;
        int idx = 0;
        for (int i : ra_coord_idx)
        {
          ctr.assign(coords[3*i+0], coords[3*i+1], coords[3*i+2]);
          idx = 0;
          kd_tree.closest_vertex(ctr, idx, dummy, dummy_len);  // closest vertex in RA heart coordinates
          mt_idx_t mesh_idx = ra_hcoord_idx[idx];
          // Now locate all points in the given thresholds
          if (thr[i] > 0.0)
          {
            mt_vector<mt_mixed_tuple<mt_real, int> > vertices_cart;
            tree_cart.vertices_in_sphere(ptsvec_cart[mesh_idx], thr[i], vertices_cart);
            // Throw out anything that has a rho value outside of the epsilon-box
            for (auto & v : vertices_cart)
              if ((ventricle[v.v2] == 2) && (std::abs(hcoords_vec[v.v2].z-hcoords_vec[mesh_idx].z) < eps[i]))
                vtxlist[i].push_back(v.v2);
          }
          else
            vtxlist[i].push_back(mesh_idx);
        }
      }
      else {
        for(int i : ra_coord_idx)
          vtxlist[i].push_back(-1);
      }

      gettimeofday(&t2, NULL);
      std::cout << "Done in " << (float)timediff_sec(t1, t2) << " sec" << std::endl;

      std::string outbase = opts.coord;
      remove_extension(outbase);

      std::string outfile = outbase + ".out.txt";
      write_vtxlist_file(vtxlist, outfile);
      std::cout << "Wrote " << outfile << std::endl;

      if (output_vtx) {
        const short header = (opts.extra_vtx) ? WRITE_VTX_EXTRA : WRITE_VTX_INTRA;
        for (size_t i=0; i<vtxlist.size(); i++) {
          const std::string file_name = outbase + "_coord" + std::to_string(i) + VTX_EXT;
          write_vtx(vtxlist[i], file_name, header);
        }
      }

      break;
    }

    case QRY_UVCOORDS_XYZ: {
      // query coords-xyz  ------------------------------------------------------------
      std::cout << "Reading mesh: " << opts.msh.base << std::endl;
      gettimeofday(&t1, NULL);
      read_mesh_selected(mesh, opts.msh.format, opts.msh.base, CRP_READ_ELEM | CRP_READ_PTS);
      print_mesh_stats(mesh);
      gettimeofday(&t2, NULL);
      std::cout << "Done in " << (float)timediff_sec(t1, t2) << " sec" << std::endl;

      std::cout << "Setting up n2e / n2n graphs .. " << std::endl;
      gettimeofday(&t1, NULL);
      compute_full_mesh_connectivity(mesh);
      gettimeofday(&t2, NULL);
      std::cout << "Done in " << (float)timediff_sec(t1, t2) << " sec" << std::endl;

      mt_vector<mt_real> UVCs, xyz_cart;
      mt_mask is_LV;

      gettimeofday(&t1, NULL);
      if (opts.uvc.empty()) {
        opts.uvc = opts.msh.base + UVC_PTS_EXT;
        if (!file_exists(opts.uvc)) {
          opts.uvc = opts.msh.base + HRT_PTS_EXT;
          if (!file_exists(opts.uvc)) {
            std::cerr << "Error: Faild to open UVC file !" << std::endl;
            break;
          }
        }
      }
      readUVCPoints(UVCs, is_LV, opts.uvc);
      gettimeofday(&t2, NULL);
      std::cout << "Done in " << (float)timediff_sec(t1, t2) << " sec" << std::endl;
      if (UVCs.size() != mesh.xyz.size()) {
        std::cerr << "Error: Number of points does not match the number of coordinates!" << std::endl;
        break;
      }

      std::cout << "Reading uvc coord file " << opts.coord << std::endl;
      mt_vector<mt_real> coords, thr, eps;
      mt_vector<bool> coord_is_lv;
      read_coord_file_uvc(coords, coord_is_lv, thr, eps, opts.coord);

      // split UVCs of mesh into two sets to set up seperate kdtrees
      mt_vector<vec3r> UVCs_vec;
      array_to_points(UVCs, UVCs_vec);
      mt_vector<mt_idx_t> vtxlist;
      mt_vector<vec3r> ptslist;

      std::cout << "Mapping UVCs .." << std::endl;
      gettimeofday(&t1, NULL);

      size_t max_it = 10;
      if (!opts.iter.empty()) {
        const int iter = atoi(opts.iter.c_str());
        if (iter > 0)
          max_it = static_cast<size_t>(iter);
      }
      get_xyz_from_UVCs(mesh, UVCs_vec, is_LV, coords, coord_is_lv, vtxlist, ptslist, max_it);

      gettimeofday(&t2, NULL);
      std::cout << "Done in " << (float)timediff_sec(t1, t2) << " sec" << std::endl;

      std::string outfile;
      outfile = opts.coord + ".out.txt";

      // a little conversion to satisfy write_vtxlist_file
      mt_vector<mt_vector<mt_idx_t>> outlist(vtxlist.size());
      for(size_t i=0; i<vtxlist.size(); i++)
        outlist[i].push_back(vtxlist[i]);

      write_vtxlist_file(outlist, outfile);
      std::cout << "Wrote " << outfile << std::endl;

      outfile = opts.coord + ".out.pts";
      write_vector_ascii(ptslist, outfile, true);
      std::cout << "Wrote " << outfile << std::endl;

      break;
    }
    case QRY_UCCOORDS_XYZ: {
      // query coords-xyz  ------------------------------------------------------------
      std::cout << "Reading mesh: " << opts.msh.base << std::endl;
      gettimeofday(&t1, NULL);
      read_mesh_selected(mesh, opts.msh.format, opts.msh.base, CRP_READ_ELEM | CRP_READ_PTS);
      print_mesh_stats(mesh);
      gettimeofday(&t2, NULL);
      std::cout << "Done in " << (float)timediff_sec(t1, t2) << " sec" << std::endl;

      std::cout << "Setting up n2e / n2n graphs .. " << std::endl;
      gettimeofday(&t1, NULL);
      compute_full_mesh_connectivity(mesh);
      gettimeofday(&t2, NULL);
      std::cout << "Done in " << (float)timediff_sec(t1, t2) << " sec" << std::endl;

      mt_vector<mt_real> UCCs, xyz_cart;
      mt_vector<int8_t> UCCs_chmbr;

      gettimeofday(&t1, NULL);
      if (opts.ucc.empty()) {
        opts.ucc = opts.msh.base + UCC_PTS_EXT;
        if (!file_exists(opts.ucc)) {
          opts.ucc = opts.msh.base + HRT_PTS_EXT;
          if (!file_exists(opts.ucc)) {
            std::cerr << "Error: Faild to open UCC file !" << std::endl;
            break;
          }
        }
      }
      readCardiacCoordinates(UCCs, UCCs_chmbr, opts.ucc);
      gettimeofday(&t2, NULL);
      std::cout << "Done in " << (float)timediff_sec(t1, t2) << " sec" << std::endl;
      if (UCCs.size() != mesh.xyz.size()) {
        std::cerr << "Error: Number of points does not match the number of coordinates!" << std::endl;
        break;
      }

      std::cout << "Reading ucc coord file " << opts.coord << std::endl;
      mt_vector<mt_real> coords, thr, eps;
      mt_vector<int8_t> coord_chmbr;
      read_coord_file_ucc(coords, coord_chmbr, thr, eps, opts.coord);

      // split UVCs of mesh into two sets to set up seperate kdtrees
      mt_vector<vec3r> UCCs_vec;
      array_to_points(UCCs, UCCs_vec);
      mt_vector<mt_idx_t> vtxlist;
      mt_vector<vec3r> ptslist;

      std::cout << "Mapping UCCs .." << std::endl;
      gettimeofday(&t1, NULL);

      size_t max_it = 10;
      if (!opts.iter.empty()) {
        const int iter = atoi(opts.iter.c_str());
        if (iter > 0)
          max_it = static_cast<size_t>(iter);
      }
      get_xyz_from_UCCs(mesh, UCCs_vec, UCCs_chmbr, coords, coord_chmbr, vtxlist, ptslist, max_it);

      gettimeofday(&t2, NULL);
      std::cout << "Done in " << (float)timediff_sec(t1, t2) << " sec" << std::endl;

      std::string outfile;
      outfile = opts.coord + ".out.txt";

      // a little conversion to satisfy write_vtxlist_file
      mt_vector<mt_vector<mt_idx_t>> outlist(vtxlist.size());
      for(size_t i=0; i<vtxlist.size(); i++)
        outlist[i].push_back(vtxlist[i]);

      write_vtxlist_file(outlist, outfile);
      std::cout << "Wrote " << outfile << std::endl;

      outfile = opts.coord + ".out.pts";
      write_vector_ascii(ptslist, outfile, true);
      std::cout << "Wrote " << outfile << std::endl;
      break;
    }
    case QRY_ELEM_IDX: {
      // query elem-idx  ------------------------------------------------------------
      if (!file_exists(opts.coord)) {
        std::cerr << "Error: File " << opts.coord << " does not exist! Aborting!" << std::endl;
        break;
      }

      mt_vector<mt_real> pnts_xyz;
      readPoints(pnts_xyz, opts.coord);
      mt_vector<vec3r> pnts;
      array_to_points(pnts_xyz, pnts);

      const size_t num_xyz_pnts = pnts.size();
      std::cout << num_xyz_pnts << " points read!" << std::endl;

      std::cout << "Reading mesh: " << opts.msh.base << std::endl;
      gettimeofday(&t1, NULL);
      read_mesh_selected(mesh, opts.msh.format, opts.msh.base, CRP_READ_ELEM | CRP_READ_PTS);
      print_mesh_stats(mesh);
      gettimeofday(&t2, NULL);
      std::cout << "Done in " << (float)timediff_sec(t1, t2) << " sec" << std::endl;

      std::cout << "Setting up n2e / n2n graphs .. " << std::endl;
      gettimeofday(&t1, NULL);
      compute_full_mesh_connectivity(mesh);
      gettimeofday(&t2, NULL);
      std::cout << "Done in " << (float)timediff_sec(t1, t2) << " sec" << std::endl;

      kdtree tree(10);
      tree.build_vertex_tree(mesh.xyz);

      mt_vector<mt_mixed_triple<mt_idx_t, mt_idx_t, mt_real>> elem_dat(num_xyz_pnts);
      for (size_t i=0; i<num_xyz_pnts; i++) {
        const vec3r& pnt = pnts[i];

        int closest_idx;
        vec3r closest_pnt;
        mt_real dist;
        tree.closest_vertex(pnt, closest_idx, closest_pnt, dist);

        const auto rval  = find_elem_idx(pnt, closest_idx, mesh, 10);
        elem_dat[i].v1 = closest_idx;
        elem_dat[i].v2 = rval.v1;
        elem_dat[i].v3 = rval.v2;

        std::cout << "Point " << pnt.x << " " << pnt.y << " " << pnt.z
                  << ", Closest node " << elem_dat[i].v1
                  << ", Element index " << elem_dat[i].v2
                  << ", Element distance " << elem_dat[i].v3 << std::endl;
      }

      if (opts.odat.empty())
        opts.odat = "elem_idx_out.dat";

      FILE* fp = fopen(opts.odat.c_str(), "w");
      if (fp)
      {
        for (size_t i=0; i<num_xyz_pnts; i++)
        {
          // closest node index / element index / element distance
          const long unsigned nidx(elem_dat[i].v1);
          const long unsigned eidx(elem_dat[i].v2);
          const double dist(elem_dat[i].v3);
          fprintf(fp, "%lu  %lu  %lf\n", nidx, eidx, dist);
        }
        fclose(fp);

        std::cout << "Output written to " << opts.odat << "!" << std::endl;
      }
      else
         std::cerr << "Error: Faild to open file " << opts.odat << "!" << std::endl;

      break;
   }
   case QRY_TAGS: {
      // query tags  ------------------------------------------------------------

      mt_vector<mt_idx_t> vtx;
      query_tags_mode_read(opts, mesh, vtx);
      MT_MAP<mt_tag_t, size_t> myo_tags, bath_tags;
      query_tags_mode(mesh, vtx, myo_tags, bath_tags, 0.1);

      size_t pidx = 0;
      std::cout << "Myocardium tags: " << std::endl;
      for (const auto &t : myo_tags) {
        std::cout << t.first;
        if (pidx++ < myo_tags.size() - 1)
          std::cout << ",";
      }
      std::cout << std::endl;

      pidx = 0;
      std::cout << "Bath tags: " << std::endl;
      for (const auto &t : bath_tags) {
        std::cout << t.first;
        if (pidx++ < bath_tags.size() - 1)
          std::cout << ",";
      }
      std::cout << std::endl;

      std::cout << "\nHistograms: " << std::endl;
      std::cout << "Myocardium tags: " << std::endl;
      for (const auto &t : myo_tags)
        std::cout << t.first << ": " << t.second << " elems" << std::endl;
      std::cout << std::endl;

      pidx = 0;
      std::cout << "Bath tags: " << std::endl;
      for (const auto &t : bath_tags)
        std::cout << t.first << ": " << t.second << " elems" << std::endl;

      break;
    }

    case QRY_BBOX:
    {
      // query bbox  ------------------------------------------------------------
      std::cout << "Reading points of mesh: " << opts.msh.base << std::endl;
      gettimeofday(&t1, NULL);
      read_mesh_selected(mesh, opts.msh.format, opts.msh.base, CRP_READ_PTS);
      print_mesh_stats(mesh);
      gettimeofday(&t2, NULL);
      std::cout << "Done in " << (float)timediff_sec(t1, t2) << " sec" << std::endl;
      gettimeofday(&t1, NULL);
      std::cout << "Computing bounding box .. " << std::endl;
      mt_bbox bbox;
      query_bbox_mode(mesh, bbox);

      gettimeofday(&t2, NULL);
      std::cout << "Done in " << (float)timediff_sec(t1, t2) << " sec" << std::endl;


      mt_real dx = bbox.max.x - bbox.min.x, dy = bbox.max.y - bbox.min.y, dz = bbox.max.z - bbox.min.z;

      printf("Bbox:\n");
      printf("x: ( %g\t%g\t),\tdelta %g\n"
             "y: ( %g\t%g\t),\tdelta %g\n"
             "z: ( %g\t%g\t),\tdelta %g\n", bbox.min.x,bbox.max.x,dx,bbox.min.y,bbox.max.y,dy,bbox.min.z,bbox.max.z,dz);
      printf("bbox diagonal length: %g \n", sqrt(dx*dx + dy*dy + dz*dz));
      break;
    }

    case QRY_VALS:
    {
      // parse mode
      int mode = 0;
      if(opts.mode.size())
        mode = atoi(opts.mode.c_str());

      // read indices
      mt_vector<mt_idx_t> idx;
      read_vtx(idx, opts.vtx);

      mt_vector<mt_real>  vals, ret;
      // data_idx may be:
      // 0 : scalar dat file
      // 1 : scalar igb file
      // 2 : vec file
      // 3 : vec3 igb file
      // 4 : vec9 igb file
      short data_idx = -1;
      mt_vector<mt_real>            idat,     odat;
      mt_vector<mt_point<mt_real> > idat_vec, odat_vec;
      mt_vector<dmat<mt_real> >     idat_ten, odat_ten;
      igb_header igb;
      setup_data_format(opts.idat, data_idx, igb);

      switch(data_idx) {
        case 0: {
          mt_real min, max, avrg;
          read_vector(vals, opts.idat);
          get_value_bounds(vals, idx, min, max, avrg);

          switch(mode) {
            case 0: ret.push_back(avrg); break;
            case 1: ret.push_back(min); break;
            case 2: ret.push_back(max); break;
            default: break;
          }
          break;
        }

        case 1: {
          mt_real min, max, avrg;

          for(int t=0; t<igb.v_t; t++) {
            printf("\rcurrent time-slice %d / %d .. ", t+1, int(igb.v_t));
            fflush(stdout);

            read_igb_slice(vals, igb);
            get_value_bounds(vals, idx, min, max, avrg);
            switch(mode) {
              case 0: ret.push_back(avrg); break;
              case 1: ret.push_back(min); break;
              case 2: ret.push_back(max); break;
              default: break;
            }
          }
          printf("\n");
          fclose(igb.fileptr);
          break;
        }

        default:
          std::cout << "unsupported data type. aborting." << std::endl;
          return;
      }

      if(opts.odat.size()) {
        write_vector_ascii(ret, opts.odat);
      } else {
        for(mt_real v : ret) {
          printf("%g\n", v);
        }
      }
      break;
    }

    case QRY_EDGE:
    {
      std::cout << "Reading mesh: " << opts.msh.base << std::endl;
      gettimeofday(&t1, NULL);
      read_mesh_selected(mesh, opts.msh.format, opts.msh.base);

      if(opts.tags.size()) {

        std::cout << "Restricting mesh to tags: " << opts.tags << std::endl;

        mt_vector<std::string> tags_str;
        mt_vector<bool> keep(mesh.e2n_cnt.size(), false);
        MT_USET<mt_idx_t> selected_tags;

        split_string(opts.tags, ',', tags_str);

        for(size_t j=0; j<tags_str.size(); j++) {
          int t = atoi(tags_str[j].c_str());
          selected_tags.insert(t);
        }

        for(size_t i=0; i<mesh.etags.size(); i++)
          if(selected_tags.count(mesh.etags[i])) keep[i] = true;

        mt_vector<mt_idx_t> nod, eidx;
        restrict_meshdata(keep, mesh, nod, eidx);
      }
      else
        reindex_nodes(mesh);

      print_mesh_stats(mesh);
      gettimeofday(&t2, NULL);
      std::cout << "Done in " << (float)timediff_sec(t1, t2) << " sec" << std::endl;

      std::cout << "Setting up n2e / n2n graphs .. " << std::endl;
      gettimeofday(&t1, NULL);
      compute_full_mesh_connectivity(mesh);
      gettimeofday(&t2, NULL);
      std::cout << "Done in " << (float)timediff_sec(t1, t2) << " sec" << std::endl;

      // build up histogramms
      size_t num_his_int = 20, num_row = 15, num_col = 90;

      std::vector<size_t> edge_cnt(mesh.n2n_cnt.size());
      for(size_t i=0; i<edge_cnt.size(); i++)
        edge_cnt[i] = mesh.n2n_cnt[i];

      if(opts.odat.size()) {
        mt_vector<size_t> conv; conv.assign(edge_cnt.begin(), edge_cnt.end());
        std::string ofile = opts.odat + ".concnt" + DAT_EXT;
        write_vector_ascii(conv, ofile, 1);
      }

      MT_USET<mt_tuple<mt_idx_t>> edge_set;
      std::vector<mt_real> edge_len;
      query_edge_mode(mesh, edge_set, edge_len);


      if(opts.odat.size()) {
        mt_vector<mt_real> conv; conv.assign(edge_len.begin(), edge_len.end());
        std::string ofile = opts.odat + ".len" + DAT_EXT;
        write_vector_ascii(conv, ofile, 1);
      }

      mt_vector<mt_real> elemvolumes;
      std::cout << "Computing element sizes .." << std::endl;
      gettimeofday(&t1, NULL);
      //mesh_min_angles(mesh, elemvolumes);
      mesh_volumes(mesh, mesh.xyz, elemvolumes);
      gettimeofday(&t2, NULL);

      std::vector<mt_real> vols;
      vols.assign(elemvolumes.begin(), elemvolumes.end());

      if(opts.odat.size()) {
        std::string ofile = opts.odat + ".vol" + DAT_EXT;
        write_vector_ascii(elemvolumes, ofile, 1);
      }

      std::cout << "Done in " << (float)timediff_sec(t1, t2) << " sec" << std::endl;

      histogramm_plotter<size_t> ecnt_histo(num_his_int, num_row, num_col);
      histogramm_plotter<mt_real> elen_histo(num_his_int, num_row, num_col);
      histogramm_plotter<mt_real> vol_histo(num_his_int, num_row, num_col);

      ecnt_histo.plot(edge_cnt, "\nNumber of connections");
      elen_histo.plot(edge_len, "\nEdge lengths");
      vol_histo.plot(vols, "\nElement volumes");
      break;
    }

    case QRY_QUAL:
    {

      struct query_quality_options mode_opts;
      query_quality_mode_read(opts, mode_opts, mesh);

      print_mesh_stats(mesh);

      mt_vector<mt_real> elemqual; // pulling this out so we can write it to a file.
      query_quality_mode(mesh, mode_opts, elemqual);

      if (opts.odat.size()) {
        std::string ofile = opts.odat + ".qual" + DAT_EXT;
        write_vector_ascii(elemqual, ofile, 1);
      }

      break;
    }

    case QRY_RESQUAL:
    {
      std::cout << "Reading mesh: " << opts.msh.base << std::endl;
      gettimeofday(&t1, NULL);

      read_mesh_selected(mesh, opts.msh.format, opts.msh.base, CRP_READ_ELEM | CRP_READ_PTS);
      compute_full_mesh_connectivity(mesh, false);
      print_mesh_stats(mesh);

      gettimeofday(&t2, NULL);
      std::cout << "Done in " << (float)timediff_sec(t1, t2) << " sec" << std::endl;

      // data_idx may be:
      short data_idx = -1;
      mt_vector<mt_real>            idat,     odat;
      mt_vector<mt_point<mt_real>>  idat_vec, odat_vec;
      igb_header igb;

      bool do_output = opts.odat.size() > 0;

      setup_data_format(opts.idat, data_idx, igb);
      if(data_idx < 0) exit(EXIT_FAILURE);

      double min = FLT_MAX, max = -FLT_MAX, avrg = 0.0, p95 = 0.0, p99 = 0.0;

      auto update_minmax = [&odat,&min,&max,&avrg,&p95,&p99]()
      {
        mt_vector<mt_real> data(odat);
        std::sort(data.begin(), data.end());
        if(min > data.front()) min = data.front();
        if(max < data.back())  max = data.back();

        mt_real a = 0.0;
        for(mt_real & v : data)
          a += v;

        a /= mt_real(data.size());
        avrg = avrg ? (avrg + a) * 0.5 : a;

        double cp95 = data[data.size() * 0.95];
        double cp99 = data[data.size() * 0.99];
        if(p95 < cp95) p95 = cp95;
        if(p99 < cp99) p99 = cp99;
      };

      switch(data_idx) {
        case 2: {
          read_vector_ascii(idat, opts.idat, true);
          check_size(idat, mesh.e2n_cnt.size(), "resolution quality with .vec data");

          quality_metric_flowdiff(mesh, idat, odat, NULL);

          update_minmax();
          printf("min: %g, max: %g, avrg: %g, 95th pc: %g, 99th pc: %g\n", min, max, avrg, p95, p99);

          if(do_output) {
            write_vector(odat, opts.odat, true);
          }

          break;
        }

        case 3: {
          check_size(size_t(igb.v_x), mesh.e2n_cnt.size(), "resolution quality with .igb data");

          mt_vector<mt_real> max_avrg_data;
          mt_real            max_avrg = 0;

          mt_mapping<mt_idx_t> ele2face;
          MT_MAP<mt_triple<mt_idx_t>, mt_idx_t> face_map;

          printf("Processing igb time-slices: %s: \n", opts.idat.c_str());

          for(int t=0; t<igb.v_t; t++) {
            read_igb_slice(idat, igb);

            quality_metric_flowdiff(mesh, idat, ele2face, face_map, odat, NULL);
            update_minmax();

            if(max_avrg < avrg) {
              printf("t%d: min: %g, max: %g, avrg: %g, 95th pc: %g, 99th pc: %g\n", (int) t+1, min, max, avrg, p95, p99);
              max_avrg = avrg;
              max_avrg_data = odat;

              if(do_output)
                write_vector(max_avrg_data, opts.odat);
            }
          }
          printf("\n");

          printf("FINAL: min: %g, max: %g, avrg: %g, 95th pc: %g, 99th pc: %g\n", min, max, avrg, p95, p99);

          fclose(igb.fileptr);
          break;
        }

        default: {
          std::cout << "Data type not supported!" << std::endl;
        } break;
      }
      break;
    }

    case QRY_GRPH:
    {
      std::cout << "Reading mesh: " << opts.msh.base << std::endl;
      gettimeofday(&t1, NULL);

      read_mesh_selected(mesh, opts.msh.format, opts.msh.base, CRP_READ_ELEM );
      print_mesh_stats(mesh);
      compute_full_mesh_connectivity(mesh);

      gettimeofday(&t2, NULL);
      std::cout << "Done in " << (float)timediff_sec(t1, t2) << " sec" << std::endl;

      size_t num_his_int = 10, num_row = 15, num_col = 60;
      histogramm_plotter<mt_idx_t> bw_histo(num_his_int, num_row, num_col);

      std::vector<mt_idx_t> bwdth(mesh.n2n_cnt.size());
      for(size_t i=0, k=0; i<mesh.n2n_cnt.size(); i++)
      {
        mt_idx_t max=0, min=mesh.n2n_cnt.size();
        for(mt_idx_t j=0; j<mesh.n2n_cnt[i]; j++, k++)
        {
          mt_idx_t c = mesh.n2n_con[k];
          if(min > c) min = c;
          if(max < c) max = c;
        }
        bwdth[i] = max - min;
      }

      bw_histo.plot(bwdth, "\n ------- matrix bandwidth ------- ");

      std::cout << "\n\n ------- mesh connectivity graph pattern -------" <<
      std::endl << std::endl;

      matrixgraph_plotter plotter(30, 60);
      plotter.print(mesh.n2n_cnt, mesh.n2n_con, '*');
      std::cout << std::endl;

      // std::string outfile = opts.msh.base + ".grph.bin";
      // std::cout << "writing connectivity matrix " << outfile << std::endl;
      // write_graph(mesh.n2n_cnt, mesh.n2n_con, outfile.c_str());
      break;
    }

    case QRY_SMTH:
    {
      mt_meshdata surfmesh;

      std::cout << "Reading mesh: " << opts.msh.base << std::endl;
      gettimeofday(&t1, NULL);

      read_mesh_selected(mesh, opts.msh.format, opts.msh.base, CRP_READ_ELEM | CRP_READ_PTS);
      print_mesh_stats(mesh);
      compute_full_mesh_connectivity(mesh);

      gettimeofday(&t2, NULL);
      std::cout << "Done in " << (float)timediff_sec(t1, t2) << " sec" << std::endl;

      std::cout << "Reading surface.. " << std::endl;
      gettimeofday(&t1, NULL);

      readElements(surfmesh, opts.surf + SURF_EXT);
      compute_full_mesh_connectivity(surfmesh);

      gettimeofday(&t2, NULL);
      std::cout << "Done in " << (float)timediff_sec(t1, t2) << " sec" << std::endl;

      std::cout << "Computing smoothness .. " << std::endl;
      gettimeofday(&t1, NULL);

      size_t nnodes = mesh.n2n_cnt.size();
      mt_vector<double> smoothness(nnodes, 0.0);
      mt_vector<bool>   onManifold(nnodes, false);

      for(size_t i=0; i<surfmesh.e2n_con.size(); i++) onManifold[surfmesh.e2n_con[i]] = true;

      for(size_t i=0; i<nnodes; i++) {
        // smoothness[i] = distance_to_centroid(mesh, surfmesh, onManifold, i);

        if(onManifold[i])
          //smoothness[i] = nbhd_smoothness_functional(surfmesh, mesh.xyz, i, 0.9);
          smoothness[i] = minimal_surf_normal_correlation(surfmesh, mesh.xyz, i);
        else
          smoothness[i] = 0.0;
      }

      std::string outfile = opts.msh.base + ".smth" + DAT_EXT;
      write_vector_ascii(smoothness, outfile);

      gettimeofday(&t2, NULL);
      std::cout << "Done in " << (float)timediff_sec(t1, t2) << " sec" << std::endl;

      break;
    }
    case QRY_CURV:
    {

      mt_vector<mt_real> curv;

      mt_meshdata surfmesh;
      struct query_curvature_options mode_opts;

      query_curvature_mode_read(opts, mesh, surfmesh, mode_opts);


      gettimeofday(&t1, NULL);
      query_curvature_mode(mesh, surfmesh, curv, mode_opts);
      std::string outfile = opts.msh.base + ".curv" + DAT_EXT;
      if(opts.odat.size()) {
        outfile = opts.odat;
      }
      write_vector_ascii(curv, outfile);
      gettimeofday(&t2, NULL);
      std::cout << "Done in " << (float)timediff_sec(t1, t2) << " sec" << std::endl;

      break;
    }

    case QRY_INSIDE:
    {
      std::cout << "Reading mesh: " << opts.msh.base << std::endl;
      gettimeofday(&t1, NULL);

      read_mesh_selected(mesh, opts.msh.format, opts.msh.base, CRP_READ_ELEM | CRP_READ_PTS);
      compute_full_mesh_connectivity(mesh);
      print_mesh_stats(mesh);

      gettimeofday(&t2, NULL);
      std::cout << "Done in " << (float)timediff_sec(t1, t2) << " sec" << std::endl;

      kdtree tree(10); tree.build_tree(mesh);
      vec3r found_point;
      bool found = point_inside_surface(mesh, tree, found_point);

      if(found) {
        printf("Point inside closed surface:\n%.2f,%.2f,%.2f\n",
               found_point.x, found_point.y, found_point.z);
      } else {
        printf("Could not find a point inside the given surface\n");
      }
      break;
    }

    case QRY_NRMLS:
    {
      mt_meshdata surfmesh;

      std::cout << "Reading mesh: " << opts.msh.base << std::endl;
      gettimeofday(&t1, NULL);

      read_mesh_selected(mesh, opts.msh.format, opts.msh.base, CRP_READ_ELEM | CRP_READ_PTS);
      print_mesh_stats(mesh);
      compute_full_mesh_connectivity(mesh);

      gettimeofday(&t2, NULL);
      std::cout << "Done in " << (float)timediff_sec(t1, t2) << " sec" << std::endl;

      if(opts.surf.size() == 0) {
        if(mesh.etype[0] == Tri) {
          // we assume we have a surface mesh, thus we copy the connectivity data (without
          // the coords) from mesh into surfmesh
          surfmesh = mesh;
          surfmesh.xyz.resize(0); surfmesh.xyz.shrink_to_fit();
        }
        else {
          std::cerr << "Error. Volumetric meshes require a surface file input. Aborting." << std::endl;
          exit(1);
        }
      }
      else {
        std::cout << "Reading surface.. " << std::endl;
        gettimeofday(&t1, NULL);
        readElements(surfmesh, opts.surf + SURF_EXT);
        compute_full_mesh_connectivity(surfmesh);
        gettimeofday(&t2, NULL);
        std::cout << "Done in " << (float)timediff_sec(t1, t2) << " sec" << std::endl;
      }

      std::cout << "Computing nodal surface normals .. " << std::endl;
      mt_vector<mt_real> nrmls;
      compute_nodal_surface_normals(surfmesh, mesh.xyz, nrmls);

      std::string filename = opts.msh.base + ".normals" + VEC_EXT;
      write_vector_ascii(nrmls, filename, 3);
      break;
    }

    case QRY_NON_MANIFOLD:
    {
      std::cout << "Reading mesh: " << opts.msh.base << std::endl;
      gettimeofday(&t1, NULL);

      mt_meshdata surfmesh;
      read_mesh_selected(mesh, opts.msh.format, opts.msh.base, CRP_READ_ELEM | CRP_READ_PTS);
      print_mesh_stats(mesh);
      compute_full_mesh_connectivity(mesh);
      generate_type_reg(mesh);

      gettimeofday(&t2, NULL);
      std::cout << "Done in " << (float)timediff_sec(t1, t2) << " sec" << std::endl;

      if ((mesh.treg[(int)Tri] + mesh.treg[(int)Quad]) != mesh.etype.size()) {
        std::cerr << "Error. Surface mesh required. Aborting." << std::endl;
        exit(1);
      }


      std::cout << "Computing edge2elem map..." << std::endl;
      gettimeofday(&t1, NULL);

      MT_MAP<mt_tuple<mt_idx_t>, mt_vector<mt_idx_t>> edge2elem_map;
      compute_edge2elem(mesh, edge2elem_map);

      gettimeofday(&t2, NULL);
      std::cout << "Done in " << (float)timediff_sec(t1, t2) << " sec" << std::endl;


      std::cout << "Determine non-manifold elements..." << std::endl;
      gettimeofday(&t1, NULL);

      mt_vector<mt_idx_t> elem_idx, vtx;
      for (auto it=edge2elem_map.begin(); it!=edge2elem_map.end(); ++it) {
        if (it->second.size() > 2) {
          elem_idx.append(it->second);
          vtx.push_back(it->first.v1);
          vtx.push_back(it->first.v2);
        }
      }
      binary_sort(elem_idx);
      unique_resize(elem_idx);
      binary_sort(vtx);
      unique_resize(vtx);

      gettimeofday(&t2, NULL);
      std::cout << "Done in " << (float)timediff_sec(t1, t2) << " sec" << std::endl;


      if (elem_idx.size() > 0) {
        std::cout << "Found " << elem_idx.size() << " non-manifold elements" << std::endl;

        std::string out_base("non_manifold");
        if (opts.odat.size() > 0)
          out_base = opts.odat;

        const std::string eidx_out = out_base + EIDX_EXT;
        write_vector_ascii(elem_idx, eidx_out, 1);
        const std::string vtx_out = out_base + VTX_EXT;
        write_vtx(vtx, vtx_out, WRITE_VTX_EXTRA);
      }
      else
        std::cout << "No non-manifold elements found !" << std::endl;

      break;
    }

    case QRY_DISTANCE:
    {
      mt_meshdata surfmesh;

      std::cout << "Reading mesh: " << opts.msh.base << std::endl;
      gettimeofday(&t1, NULL);

      read_mesh_selected(mesh, opts.msh.format, opts.msh.base, CRP_READ_PTS);
      print_mesh_stats(mesh);

      gettimeofday(&t2, NULL);
      std::cout << "Done in " << (float)timediff_sec(t1, t2) << " sec" << std::endl;

      std::cout << "Reading surface.. " << std::endl;
      gettimeofday(&t1, NULL);

      mt_filename surffile(opts.surf, "");
      read_mesh_selected(surfmesh, surffile.format, surffile.base, CRP_READ_PTS);

      gettimeofday(&t2, NULL);
      std::cout << "Done in " << (float)timediff_sec(t1, t2) << " sec" << std::endl;

      kdtree tree(10);
      tree.build_vertex_tree(surfmesh.xyz);
      size_t numpts = mesh.xyz.size()/3;
      int idx;
      vec3r closest;
      mt_real len2;

      mt_real min = FLT_MAX, max = -FLT_MAX, avrg = 0.0;

      for(size_t i=0; i<numpts; i++) {
        vec3r p(mesh.xyz.data() + i*3);
        tree.closest_vertex(p, idx, closest, len2);
        len2 = sqrt(len2);

        if(min > len2) min = len2;
        if(max < len2) max = len2;
        avrg += len2;
      }
      avrg /= mt_real(numpts);

      printf("\n\nDistance stats:\nMin: %g\nMax: %g\nAvrg: %g\n\n", min, max, avrg);
      break;
    }

  }
}

