#ifndef MT_INSERT_MODE_H_
#define MT_INSERT_MODE_H_

#include "data_structs.h"
#include "io_utils.h"
#include "kdtree.h"
#include "mt_modes_base.h"

#define GRAD_THR_DFLT 0.99
#define CON_THR_DFLT 1

#ifdef MT_ADDONS
#include "addons_utils.h"
#endif

struct insert_options {
  enum op_type type;
  std::string msh_base;
  std::string submsh_base;
  std::string outmsh_base;
  std::string msh_dat_file;
  std::string submsh_dat_file;
  std::string out_dat_file;
  std::string idat;
  std::string ifmt;
  std::string ofmt;
  std::string trsp;
  std::string trsp_dist;
  std::string grad_thr;
  std::string con_thr;
  std::string oper;
  std::string mode;
  std::string surf;
  std::string tags;
  std::string val;
  std::string grow;

  bool keep_ordering = false;
  bool keep_surface  = false;
};

struct insert_meshdata_options {
  mt_idx_t mult_thr;
  short insert_op;
  bool trsp_defined = false;
  bool trsp_dist_defined = false;
  mt_real trsp_dist;
  mt_real grad_thr;
  mt_vector<mt_real> inp_grad;
};
/**
 * @brief insert an element data vector as new mesh element tags.
 *
 * @param[in,out]  mesh      source/target mesh
 * @param[in]      newtags   new element tags
 */
void insert_tags_mode(mt_meshdata& mesh, const mt_vector<mt_tag_t>& newtags);

/**
 * @brief read helper for insert tagsurfs function
 *
 * @param[in]   mshfile        source mesh file name
 * @param[out]  mesh           mesh to write to
 * @param[out]  surface_trees  vector of kdtrees for surface meshes
 * @param[out]  surface        vector of surface meshes
 * @param[in]   tags_list      list of tags
 * @param[in]   surf_list      list of surface mesh files
 */
void insert_tagsurfs_mode_read(
  const mt_filename& mshfile,
  mt_meshdata& mesh,
  mt_vector<kdtree*> & surface_trees,
  mt_vector<mt_meshdata*> & surface,
  mt_vector<std::string>& tags_list,
  mt_vector<std::string>& surf_list);

/**
 *  @brief insert an element data vector as new mesh element tags.
 *
 * @param[in,out]  mesh           source/target mesh
 * @param[in]      surface_trees  vector of kdtrees of surface meshes
 * @param[in]      surface        vector of surface meshes
 * @param[in]      tags_list      list of tags
 * @param[in]      sampling_type  type of sampling
 */

void insert_tagsurfs_mode(
  mt_meshdata& mesh,
  mt_vector<kdtree*> & surface_trees,
  mt_vector<mt_meshdata*> & surface,
  const mt_vector<std::string>& tags_list,
  const int sampling_type,
  const int grow);



/**
 * @brief helper read function for insert meshdata
 * 
 * @param[in] opts       options for insert mode
 * @param[out] mesh      mesh to write to
 * @param[out] manifold  manifold mesh to write to
 * @param[out] mode_opts options for insert meshdata
 
 */
void insert_meshdata_mode_read(
  const struct insert_options& opts,
  mt_meshdata& mesh,
  mt_meshdata& manifold,
  struct insert_meshdata_options& mode_opts
);

/**
 * the fiber and tag data of a mesh is inserted into another mesh based on vertex locations.
 * 
 * @param[in,out] mesh   mesh to insert data into
 * @param[in] manifold   mesh to insert data from
 * @param[in] mode_opts  options for insert meshdata
 */
void insert_meshdata_mode(
  mt_meshdata& mesh,
  mt_meshdata& manifold, 
  const struct insert_meshdata_options& mode_opts
);


/**
 * @brief the fiber and tag data of a mesh is inserted into another mesh based on vertex locations.
 * 
 * @param[in,out] mesh   mesh to insert data into
 * @param[in] submsh     mesh to insert data from
 * @param[in] eidx       element indices where to insert the tags and fibers
 * @param[in] nod        indices where to insert the coordinates
 */
void insert_mesh_mode(mt_meshdata& mesh, const mt_meshdata& submsh, const mt_vector<mt_idx_t>& eidx, const mt_vector<mt_idx_t>& nod);


/**
 * @brief insert the region tags from an image segmentation.
 * 
 * @param[in,out] mesh      mesh to insert data into
 * @param[in]     input_img image data
 * @param[in]     surf_set  set of surface indices
 * @param[in]     tags      image tag indices to consider
 */
void insert_imgtags_mode(mt_meshdata &mesh, itk_image &input_img, const MT_USET<mt_idx_t> &surf_set, const MT_USET<int> &tags);


/**
 * @brief insert element fibers into mesh
 * 
 * @param[in, out] mesh   mesh to insert fibers into 
 * @param[in] fib         fibers to insert 
 * @param[in] mode        mode of insertion (1=fibers, 2=sheets, for both make two calls) 
 */

void insert_fibers_mode(mt_meshdata &mesh, const mt_vector<mt_fib_t> &fib, const int mode);

#endif
