#include "data_structs.h"
#include "insert_mode.h"
#include "io_utils.h"
#include "kdtree.h"
#include "mt_vector.h"
#include "topology_utils.h"
// #include <bits/types/struct_timeval.h>
void insert_mesh_mode(mt_meshdata &mesh, const mt_meshdata &submsh, const mt_vector<mt_idx_t> &eidx,
                      const mt_vector<mt_idx_t> &nod) {
  insert_etags(mesh, submsh.etags, eidx);
  insert_fibers(mesh, submsh.lon, eidx);
  insert_points(mesh, submsh.xyz, nod);
}

void insert_meshdata_mode_read(const struct insert_options &opts, mt_meshdata &mesh, mt_meshdata &manifold,
                               struct insert_meshdata_options &mode_opts) {
  struct timeval t1, t2;

  mt_filename mshfile(opts.msh_base, opts.ifmt);
  mt_filename submshfile(opts.submsh_base, opts.ifmt);

  std::cout << "Reading mesh: " << mshfile.base << std::endl;
  gettimeofday(&t1, NULL);
  read_mesh_selected(mesh, mshfile.format, mshfile.base);
  reindex_nodes(mesh);
  compute_full_mesh_connectivity(mesh, mshfile.base);
  gettimeofday(&t2, NULL);
  std::cout << "Done in " << (float)timediff_sec(t1, t2) << " sec" << std::endl;

  std::cout << "Reading input mesh: " << submshfile.base << std::endl;
  gettimeofday(&t1, NULL);
  read_mesh_selected(manifold, submshfile.format, submshfile.base);
  reindex_nodes(manifold);
  compute_full_mesh_connectivity(manifold);
  gettimeofday(&t2, NULL);
  std::cout << "Done in " << (float)timediff_sec(t1, t2) << " sec" << std::endl;

  mode_opts.mult_thr = opts.con_thr.size() > 0 ? atoi(opts.con_thr.c_str()) : CON_THR_DFLT;

  mode_opts.insert_op = atoi(opts.oper.c_str());

  // read in transport distance
  mode_opts.trsp_dist = atof(opts.trsp_dist.c_str());
  // read gradient correlation threshold
  mode_opts.grad_thr = opts.grad_thr.size() > 0 ? atof(opts.grad_thr.c_str()) : GRAD_THR_DFLT;

  read_vector_ascii(mode_opts.inp_grad, opts.trsp);

  mode_opts.trsp_defined = opts.trsp.size() > 0;
  mode_opts.trsp_dist_defined = opts.trsp_dist.size() > 0;
}

void insert_meshdata_mode(mt_meshdata &mesh, mt_meshdata &manifold, const struct insert_meshdata_options &mode_opts) {
  struct timeval t1, t2;
  short numfib = mesh.e2n_cnt.size() * 6 == mesh.lon.size() ? 2 : 1;

  short manifold_numfib = manifold.e2n_cnt.size() * 6 == manifold.lon.size() ? 2 : 1;

  size_t manifold_nnodes = manifold.xyz.size() / 3;

  // consistency check
  if (numfib != manifold_numfib) {
    std::cout << "Warning: imsh and msh must have the same fiber/sheet setup!\n";
    std::cout << "         Code is likely to crash...\n";
  }

  mt_vector<mt_idx_t> corr;
  mt_vector<mt_real> corr_dist;
  std::cout << "Computing vertex correspondance .. " << std::endl;
  gettimeofday(&t1, NULL);
  compute_correspondance(manifold, mesh, corr, corr_dist);
  gettimeofday(&t2, NULL);
  std::cout << "Done in " << (float)timediff_sec(t1, t2) << " sec" << std::endl;

  std::cout << "Computing mapping between mesh and input-mesh.. " << std::endl;
  gettimeofday(&t1, NULL);

  MT_MAP<mt_idx_t, mt_idx_t> old2new, mesh2manifold;
  for (size_t i = 0; i < corr.size(); i++) mesh2manifold[corr[i]] = i;

  // select the initial elements we use in data insertion
  mt_meshdata res_mesh = mesh;
  mt_vector<mt_idx_t> res_eidx;
  {
    MT_USET<mt_idx_t> sel_elem, nodeSet;
    mt_vector<bool> keep(res_mesh.e2n_cnt.size(), false);

    nodeSet.insert(corr.begin(), corr.end());
    nodeSet_to_elemSet(mesh, nodeSet, sel_elem);

    // restrict mesh to selected elements
    for (const mt_idx_t &e : sel_elem) keep[e] = true;
    restrict_meshdata(keep, res_mesh, res_eidx);
  }

  // map restricted mesh into a nodal indexing consistent with the manifold
  size_t vtxidx = manifold_nnodes;
  for (size_t i = 0; i < res_mesh.e2n_con.size(); i++) {
    mt_idx_t c = res_mesh.e2n_con[i], nc = -1;
    auto mc = mesh2manifold.find(c);
    if (mc != mesh2manifold.end()) {
      // set new connectivity from manifold correspondance
      nc = mc->second;
      old2new[c] = nc;
    } else {
      // set new connectivity from extended indexing
      mc = old2new.find(c);
      if (mc != old2new.end())
        nc = mc->second;
      else {
        nc = vtxidx++;
        old2new[c] = nc;
      }
    }
    res_mesh.e2n_con[i] = nc;
  }
  compute_full_mesh_connectivity(res_mesh);

  mt_mapping<mt_idx_t> mesh2manifld;
  mt_vector<mt_idx_t> mesh_mult_cnt;
  /* We compute the mesh2manifld map by a matrix-matrix product.
   * The problem is that the manifold nodes dont span the whole
   * mesh node range. We must extend the manifold node range to
   * make the product work.
   */
  {
    mt_vector<mt_idx_t> manifld_mult_cnt;
    size_t manifold_old_nsize = manifold.n2e_cnt.size();
    mt_vector<mt_idx_t> aele(res_mesh.e2n_con.size(), 1);
    mt_vector<mt_idx_t> bele(manifold.n2e_con.size(), 1);

    manifold.n2e_cnt.resize(vtxidx, 0);
    mat_mult_mat_crs(res_mesh.e2n_cnt, res_mesh.e2n_con, aele, manifold.n2e_cnt, manifold.n2e_dsp, manifold.n2e_con,
                     bele, mesh2manifld.fwd_cnt, mesh2manifld.fwd_con, mesh_mult_cnt);
    manifold.n2e_cnt.resize(manifold_old_nsize);

    transpose_connectivity(mesh2manifld.fwd_cnt, mesh2manifld.fwd_con, mesh_mult_cnt, mesh2manifld.bwd_cnt,
                           mesh2manifld.bwd_con, manifld_mult_cnt);
    mesh2manifld.setup_dsp();
  }
  gettimeofday(&t2, NULL);
  std::cout << "Done in " << (float)timediff_sec(t1, t2) << " sec" << std::endl;

  // map the mesh data =====================================================
  std::cout << "Mapping element data .. " << std::endl;
  gettimeofday(&t1, NULL);

  for (size_t eidx = 0; eidx < res_mesh.e2n_cnt.size(); eidx++) {
    mt_idx_t maxidx = -1, maxcnt = 0;
    mt_idx_t start = mesh2manifld.fwd_dsp[eidx], stop = start + mesh2manifld.fwd_cnt[eidx];
    // sweep over connected elements and save element with maximum connected
    // nodes
    for (mt_idx_t i = start; i < stop; i++) {
      mt_idx_t midx = mesh2manifld.fwd_con[i];
      mt_idx_t cc = mesh_mult_cnt[i];
      if (cc >= mode_opts.mult_thr && cc > maxcnt) {
        maxcnt = mesh_mult_cnt[i];
        maxidx = midx;
      }
    }
    // set element data for saved element
    if (maxidx > -1) {
      // tags
      res_mesh.etags[eidx] = manifold.etags[maxidx];
      // fibers
      for (short i = 0; i < numfib * 3; i++)
        res_mesh.lon[eidx * 3 * numfib + i] = manifold.lon[maxidx * 3 * numfib + i];
    }
  }

  mesh2manifld.resize(0);

  // insert tags =====================================================
  if ((mode_opts.insert_op == 0 || mode_opts.insert_op == 2)) {
    if ((mode_opts.trsp_defined) && (mode_opts.trsp_dist_defined)) {
      // read in transport gradient

      assert(mode_opts.inp_grad.size() / 3 == mesh.e2n_cnt.size());
      mt_vector<mt_point<mt_real>> grad(mesh.e2n_cnt.size());
      for (size_t i = 0; i < grad.size(); i++) {
        grad[i].x = mode_opts.inp_grad[i * 3 + 0];
        grad[i].y = mode_opts.inp_grad[i * 3 + 1];
        grad[i].z = mode_opts.inp_grad[i * 3 + 2];
        if (grad[i].length2() == 0) std::cerr << "Error: Zero gradient at " << i << std::endl;
        grad[i].normalize();
      }

      // set up e2e map
      mt_vector<mt_cnt_t> e2e_cnt;
      mt_vector<mt_idx_t> e2e_dsp, e2e_con;
      multiply_connectivities(mesh.e2n_cnt, mesh.e2n_con, mesh.n2e_cnt, mesh.n2e_con, e2e_cnt, e2e_con);
      e2e_dsp.resize(e2e_cnt.size());
      bucket_sort_offset(e2e_cnt, e2e_dsp);

      // compute element centerpoints as average of node coords
      mt_vector<mt_point<mt_real>> vtx_nod, vtx_ele;
      array_to_points(mesh.xyz, vtx_nod);
      nodeData_to_elemData(mesh, vtx_nod, vtx_ele);

      // initialize element data transport
      mt_data_transport<mt_idx_t, mt_real> transport(e2e_cnt, e2e_dsp, e2e_con, vtx_ele);

      mt_vector<mt_mixed_tuple<mt_idx_t, mt_tag_t>> init;
      for (size_t i = 0; i < res_eidx.size(); i++) init.push_back({res_eidx[i], res_mesh.etags[i]});

      // apply data transport
      transport(grad, mode_opts.grad_thr, mode_opts.trsp_dist, mesh.etags, init);
    } else {
      insert_etags(mesh, res_mesh.etags, res_eidx);
    }
  }

  // insert fibers =========================================================
  if (mode_opts.insert_op == 1 || mode_opts.insert_op == 2) insert_fibers(mesh, res_mesh.lon, res_eidx);

  gettimeofday(&t2, NULL);
  std::cout << "Done in " << (float)timediff_sec(t1, t2) << " sec" << std::endl;
}

void insert_imgtags_mode(mt_meshdata &mesh, itk_image &input_img, const MT_USET<mt_idx_t> &surf_set,
                         const MT_USET<int> &tags) {
  if (tags.size()) {
    itk_access<float> acc(input_img);
    mt_idx_t *con = mesh.e2n_con.data();
    for (size_t eidx = 0; eidx < mesh.e2n_cnt.size(); eidx++) {
      mt_idx_t esize = mesh.e2n_cnt[eidx];

      if (surf_set.count(mt_idx_t(eidx)) == 0) {
        vec3r b = barycenter(esize, con, mesh.xyz.data());
        vec3i px = acc.pixel(b);

        if (valid_pixel(px, input_img)) {
          int val = 0;
          itk_comp_as<int>(input_img(px.x, px.y, px.z), input_img.comptypeid, val);

          if (tags.count(val)) mesh.etags[eidx] = val;
        }
      }
      con += esize;
    }
  } else {
    itk_image med_img;
    med_img.assign(input_img, "short");
    itk_access<short> acc(med_img);

    mt_idx_t *con = mesh.e2n_con.data();
    short rad = 2;
    mt_vector<short> buff(rad * rad * rad);

    for (size_t eidx = 0; eidx < mesh.e2n_cnt.size(); eidx++) {
      mt_idx_t esize = mesh.e2n_cnt[eidx];

      if (surf_set.count(mt_idx_t(eidx)) == 0) {
        vec3r b = barycenter(esize, con, mesh.xyz.data());
        vec3i px = acc.pixel(b);

        mt_tag_t tag = pix_median(acc, buff, rad, px.x, px.y, px.z);
        // mt_tag_t tag = acc(px.x, px.y, px.z);
        if (tag != 0)
          mesh.etags[eidx] = tag;
        else {
          for (short s : buff)
            if (s != 0) {
              mesh.etags[eidx] = s;
              break;
            }
        }
      }

      con += esize;
    }
  }
}

void insert_fibers_mode(mt_meshdata &mesh, const mt_vector<mt_fib_t> &fib, const int mode) {
  int numfib = mesh.lon.size() == mesh.e2n_cnt.size() * 6 ? 6 : 3;
  if (mode == 1) {
    for (size_t i = 0; i < mesh.e2n_cnt.size(); i++) {
      mesh.lon[i * numfib + 0] = fib[i * 3 + 0];
      mesh.lon[i * numfib + 1] = fib[i * 3 + 1];
      mesh.lon[i * numfib + 2] = fib[i * 3 + 2];
    }
  } else if (mode == 2 && numfib == 6) {
    for (size_t i = 0; i < mesh.e2n_cnt.size(); i++) {
      mesh.lon[i * numfib + 3] = fib[i * 3 + 0];
      mesh.lon[i * numfib + 4] = fib[i * 3 + 1];
      mesh.lon[i * numfib + 5] = fib[i * 3 + 2];
    }
  }
}

void insert_tags_mode(mt_meshdata &mesh, const mt_vector<mt_tag_t> &newtags) {
  check_same_size(mesh.etags, newtags, __func__);
  mesh.etags = newtags;
}

void insert_tagsurfs_mode_read(const mt_filename &mshfile, mt_meshdata &mesh, mt_vector<kdtree*> & surface_trees,
                               mt_vector<mt_meshdata *> & surface, mt_vector<std::string> &tags_list,
                               mt_vector<std::string> &surf_list) {
  struct timeval t1, t2;

  std::cout << "Reading mesh: " << mshfile.base << std::endl;
  gettimeofday(&t1, NULL);
  read_mesh_selected(mesh, mshfile.format, mshfile.base);
  gettimeofday(&t2, NULL);
  std::cout << "Done in " << (float)timediff_sec(t1, t2) << " sec" << std::endl;

  int num_tags  = tags_list.size();
  int num_surfs = surf_list.size();
  bool tags_provided = true;

  if (num_tags != num_surfs) {
    printf( "No tag list provided. Using the tag of the first element in each surface insted.\n");
    tags_provided = false;
    tags_list.resize(0);
  }

  surface_trees.resize(num_surfs);
  surface      .resize(num_surfs);

  for (int sidx = 0; sidx < num_surfs; sidx++) {
    mt_filename sfile(surf_list[sidx], mshfile.format);
    surface_trees[sidx] = new kdtree(10);
    surface[sidx] = new mt_meshdata();

    mt_meshdata &actsurf = *surface[sidx];

    std::cout << "Reading surface: " << sfile.base << std::endl;
    read_mesh_selected(actsurf, sfile.format, sfile.base);
  }
}

void insert_tagsurfs_mode(mt_meshdata &mesh, mt_vector<kdtree *> & surface_trees, mt_vector<mt_meshdata *> & surface,
                          const mt_vector<std::string> &tags_list, const int sampling_type, const int grow)
{
  size_t num_surfs = surface_trees.size();
  struct timeval t1, t2;

  mt_vector<mt_idx_t> tags(num_surfs);

  for (size_t sidx = 0; sidx < num_surfs; sidx++) {
    mt_meshdata & actsurf = *surface[sidx];

    correct_duplicate_vertices(actsurf);
    correct_duplicate_elements(actsurf);

    if (tags_list.size() > 0)
      tags[sidx] = atoi(tags_list[sidx].c_str());
    else
      tags[sidx] = actsurf.etags[0];

    std::cout << "Building surface kdtree .." << std::endl;
    surface_trees[sidx]->build_tree(actsurf);
  }

  if (surface_trees.size() > 1) {
    mt_vector<int> perm_new2old, perm_old2new;
    get_surface_outer_to_inner_perm(surface_trees, perm_new2old);
    invert_perm(perm_new2old, perm_old2new);

    std::cout << "Used outer-to-inner surface ordering: ";
    for (size_t i = 0; i < perm_new2old.size(); i++) std::cout << tags[perm_new2old[i]] << " ";
    std::cout << std::endl;

    // now permute the input data order
    mt_vector<mt_idx_t> tags_cpy(tags);
    mt_vector<kdtree *> trees_cpy(surface_trees);
    mt_vector<mt_meshdata *> surf_cpy(surface);

    for (size_t i = 0; i < perm_new2old.size(); i++) {
      mt_idx_t p = perm_new2old[i];
      tags[i] = tags_cpy[p];
      surface_trees[i] = trees_cpy[p];
      surface[i] = surf_cpy[p];
    }
  }

  std::cout << "Sampling elements " << std::endl;
  gettimeofday(&t1, NULL);

  mt_mask tag_found(mesh.e2n_cnt.size());

#ifdef MT_ADDONS
  if (tags_list.size()) {
    for (int sidx = num_surfs - 1; sidx >= 0; sidx--)
      sample_elem_tags(mesh, tag_found, *surface_trees[sidx], tags[sidx], sampling_type, grow);
  } else {
    for (int sidx = num_surfs - 1; sidx >= 0; sidx--)
      sample_tags_distbased(mesh, tag_found, *surface[sidx], *surface_trees[sidx]);
  }
#else
  for (int sidx = num_surfs - 1; sidx >= 0; sidx--)
    sample_elem_tags(mesh, tag_found, *surface_trees[sidx], tags[sidx], sampling_type, grow);
#endif

  gettimeofday(&t2, NULL);
  std::cout << "Done in " << (float)timediff_sec(t1, t2) << " sec" << std::endl;
}
