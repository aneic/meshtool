/**
* @file transform_mode.cpp
* @brief Geometric transformation of meshes.
* @author Aurel Neic
* @version 
* @date 2019-03-04
*/


#include "mt_modes_base.h"
#ifdef MT_ADDONS
#include "addons_utils.h"
#endif
#include "dense_mat.hpp"


struct transform_options {
  mt_filename inp_file;
  mt_filename out_file;

  mt_filename fit_file;
  std::string matrix_file;

  std::string scale;
  std::string translate;
  std::string rotx;
  std::string roty;
  std::string rotz;
  bool nofibrot = false;
  bool pntcloud = false;
};

static const std::string transl_par = "-trans=";
static const std::string rotx_par = "-rotx=";
static const std::string roty_par = "-roty=";
static const std::string rotz_par = "-rotz=";
static const std::string fit_par  = "-fit=";
static const std::string matrix_par  = "-matrix=";
static const std::string nofibrot_par = "-nofibrot";
static const std::string pntcloud_par = "-pntcloud";


void print_transform_help()
{
  fprintf(stderr, "transform: Transform the vertex coordinates of a mesh\n");
  fprintf(stderr, "parameters:\n");
  fprintf(stderr, "%s<path>\t (input) path to basename of the input mesh\n", inp_mesh_par.c_str());
  fprintf(stderr, "%s<path>\t (output) path to basename of the output mesh\n", out_mesh_par.c_str());
  fprintf(stderr, "%s<float>\t (optional) Vertex scaling\n", scale_par.c_str());
  fprintf(stderr, "%sx,y,z\t (optional) Vertex translation\n", transl_par.c_str());
  fprintf(stderr, "%s<float>\t (optional) Rotation (in degrees) along the x axis\n", rotx_par.c_str());
  fprintf(stderr, "%s<float>\t (optional) Rotation (in degrees) along the y axis\n", roty_par.c_str());
  fprintf(stderr, "%s<float>\t (optional) Rotation (in degrees) along the z axis\n", rotz_par.c_str());
#ifdef MT_ADDONS
  fprintf(stderr, "%s<path>\t (optional) Mesh to fit. If set, all other transforms are ignored.\n", fit_par.c_str());
  fprintf(stderr, "%s<path>\t (optional) Path to affine transform to apply. If set, all other transforms are ignored.\n", matrix_par.c_str());
#endif
  fprintf(stderr, "%s\t (optional) Set flag to disable fiber rotation\n", nofibrot_par.c_str());
  fprintf(stderr, "%s\t (optional) Treat input as point-cloud\n", pntcloud_par.c_str());
  fprintf(stderr, "%s<path>\t (optional) format of the input mesh\n", inp_format_par.c_str());
  fprintf(stderr, "%s<path>\t (optional) format of the output mesh\n\n", out_format_par.c_str());
  fprintf(stderr, "\nThe supported input formats are:\n%s\n", input_formats.c_str());
  fprintf(stderr, "\nThe supported output formats are:\n%s\n", output_formats.c_str());
  fprintf(stderr, "\n");
}

int transform_parse_options(int argc, char** argv, struct transform_options & opts)
{
  if(argc < 3) {
    print_transform_help();
    return 1;
  }

  std::string inp_msh_base;
  std::string fit_msh_base;
  std::string out_msh_base;
  std::string inp_format;
  std::string out_format;

  for(int i=2; i<argc; i++) {
    std::string param = reformat_param(argv, i, argc);
    bool match = false;

    if(!match) match = parse_param(param, inp_mesh_par, inp_msh_base);
    if(!match) match = parse_param(param, fit_par, fit_msh_base);
    if(!match) match = parse_param(param, matrix_par, opts.matrix_file);
    if(!match) match = parse_param(param, out_mesh_par, out_msh_base);
    if(!match) match = parse_param(param, inp_format_par, inp_format);
    if(!match) match = parse_param(param, out_format_par, out_format);
    if(!match) match = parse_param(param, scale_par, opts.scale);
    if(!match) match = parse_param(param, transl_par, opts.translate);
    if(!match) match = parse_param(param, rotx_par, opts.rotx);
    if(!match) match = parse_param(param, roty_par, opts.roty);
    if(!match) match = parse_param(param, rotz_par, opts.rotz);
    if(!match) match = parse_flag (param, nofibrot_par, opts.nofibrot);
    if(!match) match = parse_flag (param, pntcloud_par, opts.pntcloud);

    if(!match) {
      std::cerr << "Error: Cannot parse parameter " << param << std::endl;
      return 2;
    }
  }

  opts.inp_file.assign(inp_msh_base, inp_format);
  opts.fit_file.assign(fit_msh_base, inp_format);
  opts.out_file.assign(out_msh_base, out_format);

  if(! (opts.inp_file.isSet() && opts.out_file.isSet()) ) {
    std::cerr << "Mesh transform error: Insufficient parameters provided." << std::endl;
    print_transform_help();
    return 3;
  }

  return 0;
}


void transform_mode(int argc, char** argv)
{
  struct transform_options opts;
  int ret = transform_parse_options(argc, argv, opts);
  if(ret != 0) return;

  struct timeval t1, t2;
  struct mt_meshdata mesh;

  // first read mesh
  short read_mask = CRP_READ_PTS;
  if ((!opts.pntcloud) && ((!opts.nofibrot) || (opts.fit_file.isSet())))
    read_mask |= CRP_READ_ELEM | CRP_READ_LON;

  std::cout << "Reading mesh: " << opts.inp_file.base << std::endl;
  gettimeofday(&t1, NULL);
  read_mesh_selected(mesh, opts.inp_file.format, opts.inp_file.base, read_mask);
  gettimeofday(&t2, NULL);
  std::cout << "Done in " << (float)timediff_sec(t1, t2) << " sec" << std::endl;

  // transforming mesh
  bool have_scale     = opts.scale.size();
  bool have_translate = opts.translate.size();
  bool have_rotx      = opts.rotx.size();
  bool have_roty      = opts.roty.size();
  bool have_rotz      = opts.rotz.size();
#ifdef MT_ADDONS
  bool have_fit       = opts.fit_file.isSet();
  bool have_marix_file= opts.matrix_file.size();

  if(have_fit) {
    mt_meshdata fit_mesh, surf, fit_surf;

    std::cout << "Reading fitting reference mesh: " << opts.fit_file.base << std::endl;
    gettimeofday(&t1, NULL);
    read_mesh_selected(fit_mesh, opts.fit_file.format, opts.fit_file.base);
    gettimeofday(&t2, NULL);
    std::cout << "Done in " << (float)timediff_sec(t1, t2) << " sec" << std::endl;

    compute_surface(mesh, surf, false);
    compute_surface(fit_mesh, fit_surf, false);

    mt_vector<mt_idx_t> surf_nod(surf.e2n_con), fit_surf_nod(fit_surf.e2n_con);
    binary_sort(surf_nod); unique_resize(surf_nod);
    binary_sort(fit_surf_nod); unique_resize(fit_surf_nod);

    fit_coords(surf_nod, mesh.xyz, fit_surf_nod, fit_mesh.xyz);
  } else if(have_marix_file) {
    dmat<mt_real> trsf(4,4);

    if(endswith(opts.matrix_file, ".trfm") || endswith(opts.matrix_file, ".yaml")) {
      read_affine_transform(trsf, opts.matrix_file.c_str());
    } else {
      mt_vector<std::string> coords;
      split_string(opts.matrix_file, ',', coords);

      if(coords.size() != 16) {
        fprintf(stderr, "Error: could not parse affine transform from %s! Aborting!\n", opts.matrix_file.c_str());
        exit(EXIT_FAILURE);
      }

      for(short i=0,k=0; i<4; i++)
        for(short j=0; j<4; j++, k++)
          trsf[i][j] = atof(coords[k].c_str());
    }

    apply_affine_transform(trsf, mesh.xyz);

    // for fibers, we only want to apply the rotation, therefore we zero the translation
    // part of the affine transform matrix
    trsf[0][3] = trsf[1][3] = trsf[2][3] = 0.0;

    short num_fib = mesh.lon.size() == mesh.e2n_cnt.size() * 6 ? 6 : 3;
    mt_real f_in[4], f_out[4]; f_in[3] = f_out[3] = 0.0;

    for(size_t i=0; i<mesh.e2n_cnt.size(); i++) {
      mt_fib_t* lon = mesh.lon.data() + i*num_fib;

      f_in[0] = lon[0], f_in[1] = lon[1], f_in[2] = lon[2];
      trsf.mult(f_in, f_out);
      lon[0] = f_out[0], lon[1] = f_out[1], lon[2] = f_out[2];

      if(num_fib == 6) {
        lon += 3;
        f_in[0] = lon[0], f_in[1] = lon[1], f_in[2] = lon[2];
        trsf.mult(f_in, f_out);
        lon[0] = f_out[0], lon[1] = f_out[1], lon[2] = f_out[2];
      }
    }
  } else
#endif
  {
    if(have_scale || have_translate || have_rotx || have_roty || have_rotz) {
      // first we convert the points we want to work with to vec3r
      mt_vector<vec3r> pts;
      array_to_points(mesh.xyz, pts);

      if(have_rotx || have_roty || have_rotz) {
        mt_real rotx_angle = 0.0, roty_angle = 0.0, rotz_angle = 0.0;

        if(have_rotx) {
          rotx_angle = atof(opts.rotx.c_str());
          rotx_angle *= (MT_PI / 180.0);  // convert degrees angle to radiant angle
        }
        if(have_roty) {
          roty_angle = atof(opts.roty.c_str());
          roty_angle *= (MT_PI / 180.0);  // convert degrees angle to radiant angle
        }
        if(have_rotz) {
          rotz_angle = atof(opts.rotz.c_str());
          rotz_angle *= (MT_PI / 180.0);  // convert degrees angle to radiant angle
        }

        // rotations are applied to a centered object, thus we first compute the centerpoint
        // compute center point
        bbox box; generate_bbox(pts, box);
        vec3r ctr = (box.bounds[0] + box.bounds[1]) * mt_real(0.5);
        // now move object into center
        for(vec3r & p : pts) p -= ctr;

        rotate_points(pts, rotx_angle, roty_angle, rotz_angle);
        if ((!opts.pntcloud) && (!opts.nofibrot) && (mesh.lon.size() > 0)) {
          rotate_fibers(mesh.lon, mesh.e2n_cnt.size(), 
                        rotx_angle, roty_angle, rotz_angle);
        }

        // now move object back where it was
        for(vec3r & p : pts) p += ctr;
      }

      if(have_translate) {
        mt_vector<std::string> tr_comp; // translation components
        split_string(opts.translate, ',' , tr_comp);
        check_size(tr_comp, 3, "transform_mode");

        mt_real x = atof(tr_comp[0].c_str()), y = atof(tr_comp[1].c_str()),
                z = atof(tr_comp[2].c_str());

        vec3r t(x,y,z);
        for(vec3r & p : pts) p += t;
      }

      if(have_scale) {
        mt_real s = atof(opts.scale.c_str());
        for(vec3r & p : pts) p *= s;
      }

      points_to_array(pts, mesh.xyz);
    }
  }

  // now write mesh in other format
  std::cout << "Writing mesh: " << opts.out_file.base << std::endl;
  gettimeofday(&t1, NULL);
  if (opts.pntcloud) {
    if (opts.out_file.format == carp_bin_fmt) {
      const std::string filename(opts.out_file.base + CARPBIN_PTS_EXT);
      writePointsBinary(mesh.xyz, filename);
    }
    else {
      const std::string filename(opts.out_file.base + CARPTXT_PTS_EXT);
      writePoints(mesh.xyz, filename);
    }
  }
  else
    write_mesh_selected(mesh, opts.out_file.format, opts.out_file.base);
  gettimeofday(&t2, NULL);
  std::cout << "Done in " << (float)timediff_sec(t1, t2) << " sec" << std::endl;
}
