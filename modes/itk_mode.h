#include "mt_modes_base.h"

/**
 * @brief apply closing (i.e. dilation followed by erosion) to an image
 * 
 * @param[in,out] input_img     input image
 * @param[in]     iter          number of iterations
 * @param[in]     rad           discrete radius used for smoothing
 * @param[in]     use_ref       whether to use refinement
 * @param[in]     ref           refinement factor
 * @param[in]     use_taglist   whether to use taglist
 * @param[in]     regtag        region tag
 * @param[in]     taglist       list of tags
 */

void itk_close_mode(
    itk_image& input_img,
    const int& iter,
    const mt_triple<short>& rad,
    bool use_ref,
    const mt_triple<float> ref,
    bool use_taglist,
    mt_vector<std::string> regtag,
    mt_vector<std::string> taglist);


/**
 * @brief transform an itk image stack
 * 
 * @param[in] extrude_mode  extrude mode (0 = in, 1 = out, * = both)
 * @param[in] img           input image
 * @param[in] rad           extrude radius
 * @param[in] reg_tag       tag of region to extrude
 * @param[in] new_tag       tag of new region that was extruded
 * @param[in] bdry_tags     set of boundary tags
 * 
 */
void itk_extrude_mode(
    const int extrude_mode,
    itk_image& img,
    const short rad,
    const int reg_tag,
    int new_tag,
    MT_USET<int>& bdry_tags);

/**
 * @brief convert itk image to hexahedral mesh
 * 
 * @param[in]  img       input image
 * @param[out] mesh      output mesh
 * @param[out]  values   output mesh values
 * @param[in]  has_slices whether slices are used
 * @param[in]  comp      
 * @param[in]  scale     scale factor
 * @param[in]  slices    slices
 */
bool itk_mesh_mode(
    itk_image& img, 
    mt_meshdata& mesh, 
    mt_vector<mt_real>& values, 
    const bool has_slices, 
    const unsigned int comp, 
    const mt_real scale, 
    const mt_triple<MT_USET<mt_idx_t>>& slices);