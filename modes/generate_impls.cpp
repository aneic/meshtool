#include <cstdio>
#include <iostream>

#include "generate_mode.h"

#include "distancefield.hpp"
#include "io_utils.h"
#include "kdtree.h"

#ifdef MT_ADDONS
#include "addons_utils.h"
#include "bsp_tree.hpp"
#include "ng_interface.h"
#endif

void generate_distancefield_mode(mt_meshdata &mesh, mt_vector<mt_idx_t> &start_vtx, mt_vector<mt_idx_t> &end_vtx,
                                 mt_vector<mt_real> &rvec) {
  struct timeval t1, t2;

  mt_vector<mt_real> sol_start, sol_end, rel_dist;
  dfgen::initdata ekinit;

  ekinit.nod = start_vtx;
  binary_sort(ekinit.nod);
  unique_resize(ekinit.nod);
  ekinit.val.assign(ekinit.nod.size(), mt_real(0.0));

  // run eikonal from start
  std::cout << "Solving eikonal from start surface " << std::endl;
  gettimeofday(&t1, NULL);
  {
    dfgen slv(mesh, mesh.xyz, ekinit);
    slv(sol_start, 0.0);
  }
  gettimeofday(&t2, NULL);
  std::cout << "Done in " << (float)timediff_sec(t1, t2) << " sec" << std::endl;

  if (!end_vtx.empty()) {
    ekinit.nod = end_vtx;
    binary_sort(ekinit.nod);
    unique_resize(ekinit.nod);
    ekinit.val.assign(ekinit.nod.size(), mt_real(0.0));

    std::cout << "Solving eikonal from end surface " << std::endl;
    gettimeofday(&t1, NULL);
    {
      dfgen slv(mesh, mesh.xyz, ekinit);
      slv(sol_end, 0.0);
    }
    gettimeofday(&t2, NULL);
    std::cout << "Done in " << (float)timediff_sec(t1, t2) << " sec" << std::endl;

    rel_dist.resize(sol_start.size());
    for (size_t nidx = 0; nidx < rel_dist.size(); nidx++) {
      mt_real d = sol_start[nidx] + sol_end[nidx];
      if (d > 1e-6)
        rel_dist[nidx] = sol_start[nidx] / d;
      else
        rel_dist[nidx] = 0.0;
    }

    rvec.swap(rel_dist);
  } else {
    rvec.swap(sol_start);
  }
}

void generate_fibers_mode(mt_meshdata &mesh, const std::set<mt_tag_t> &tags, const int numfibers) {
  // creating fibers
  size_t numelem = mesh.e2n_cnt.size();
  mesh.lon.resize(numelem * numfibers * 3);

  mt_fib_t *lon_ptr = mesh.lon.data();
  mt_tag_t *tag_ptr = mesh.etags.data();

  for (size_t i = 0; i < numelem; i++) {
    if (tags.count(tag_ptr[i]) == 0) {
      // tag is not in set of bath tags
      lon_ptr[0] = 1.;
      lon_ptr[1] = 0.;
      lon_ptr[2] = 0.;
      if (numfibers == 2) {
        lon_ptr[3] = 0.;
        lon_ptr[4] = 1.;
        lon_ptr[5] = 0.;
      }
    } else {
      for (int j = 0; j < numfibers * 3; j++) lon_ptr[j] = 0.0;
    }

    lon_ptr += numfibers * 3;
  }
}

void parse_scaling_factors(std::string scale_str, size_t num_surfs, mt_vector<mt_real> &factors) {
  if (scale_str.size() == 0) {
    factors.assign(num_surfs, 1.0);
    return;
  }

  mt_vector<std::string> scale_list;
  split_string(scale_str, ',', scale_list);

  if (scale_list.size() == 1) {
    mt_real sz = atof(scale_list[0].c_str());
    factors.assign(num_surfs, sz);
  } else if (scale_list.size() == num_surfs) {
    factors.resize(num_surfs);

    size_t idx = 0;
    for (std::string &s : scale_list) factors[idx++] = atof(s.c_str());
  } else {
    fprintf(stderr,
            "%s error: The number of mesh size scaling factors must be either "
            "one,\n"
            "or equal the number of input surfaces. Aborting!\n",
            __func__);
    exit(EXIT_FAILURE);
  }
}

static void mesh_surf_with_tetgen(mt_meshdata &surfmesh, mt_meshdata &output_mesh, mt_vector<mt_idx_t> &tags,
                                  mt_vector<vec3r> &vol_pts, mt_vector<mt_real> &res, mt_vector<mt_real> &scale_factors,
                                  mt_vector<mt_real> &holes_xyz, int qual_lvl, bool preserve_boundary,
                                  short sizing_mode) {
  int numberofregions = tags.size();
  mt_vector<double> regionattributes(numberofregions * 5);

  for (int i = 0; i < numberofregions; i++) {
    regionattributes[i * 5 + 0] = vol_pts[i].x;
    regionattributes[i * 5 + 1] = vol_pts[i].y;
    regionattributes[i * 5 + 2] = vol_pts[i].z;
    regionattributes[i * 5 + 3] = tags[i];

    double edge_length = res[i] * scale_factors[i];
    double desired_volume = edge_length * edge_length * edge_length / 8.48528;
    regionattributes[i * 5 + 4] = desired_volume;
  }

  mt_vector<mt_real> sizes;
  if (sizing_mode == 1) {
    generate_surfmesh_sizing_field(surfmesh, sizes);

    mt_vector<mt_real> final_scaling_ele(surfmesh.e2n_cnt.size()), final_scaling_nod;
    for (size_t eidx = 0; eidx < surfmesh.e2n_cnt.size(); eidx++)
      final_scaling_ele[eidx] = scale_factors[surfmesh.etags[eidx]];

    elemData_to_nodeData(surfmesh, final_scaling_ele, final_scaling_nod);

    for (size_t i = 0; i < final_scaling_nod.size(); i++) sizes[i] *= final_scaling_nod[i] * 0.75;
  }

  mesh_with_tetgen(surfmesh, output_mesh, regionattributes, holes_xyz, qual_lvl, sizing_mode == 1 ? &sizes : nullptr,
                   preserve_boundary);
}

void parse_holes(const std::string &holes_str, mt_vector<bool> &is_hole) {
  mt_vector<std::string> holes;
  split_string(holes_str, ',', holes);

  if (holes.size() != is_hole.size()) {
    fprintf(stderr, "%s error: Number of hole flags provided does not match number of surfaces! Aborting!\n", __func__);
    return;
  }

  for (size_t h = 0; h < holes.size(); h++) is_hole[h] = (bool)atoi(holes[h].c_str());
}

void generate_mesh_mode_read(const struct generate_options &opts,
                             mt_vector<mt_meshdata> &surfmeshes,  // out
                             mt_vector<mt_idx_t> &tags,           // out
                             mt_vector<mt_real> &scale_factors,   // out
                             mt_vector<mt_real> &res,             // TODO: Why is this not written
                             mt_vector<bool> &is_hole,            // out
                             struct generate_mesh_options &gen_msh_opts) {
  mt_vector<std::string> surf_list, tags_list;
  split_string(opts.surf, ',', surf_list);
  if (opts.ins_tag.size()) split_string(opts.ins_tag, ',', tags_list);

  int num_tags = tags_list.size();
  int num_surfs = surf_list.size();

  if (num_tags != num_surfs) {
    printf("No tag list provided. Using the tag of the first element in each "
           "surface insted.\n");
    gen_msh_opts.tags_provided = false;
  } else {
    gen_msh_opts.tags_provided = true;
  }

  if (opts.boundary.size()) gen_msh_opts.boundary_layer = atoi(opts.boundary.c_str());
  if (gen_msh_opts.boundary_layer != 0 && num_surfs > 1) {
    fprintf(stderr, "Warning: boundary layer generation supports only one input surface! Turning boundary layers off!\n");
    gen_msh_opts.boundary_layer = 0;
  }

  gen_msh_opts.padding = opts.padding.size() ? atof(opts.padding.c_str()) : 0.0;
  gen_msh_opts.qual_lvl = opts.qual.size() ? atoi(opts.qual.c_str()) : 0;

  gen_msh_opts.boundary_step = opts.boundary_step.size() ? atof(opts.boundary_step.c_str()) : BND_STEP_DFLT;
  gen_msh_opts.boundary_inc = opts.boundary_inc.size() ? atof(opts.boundary_inc.c_str()) : BND_INC_DFLT;
  gen_msh_opts.sizing_mode = 1;
  if (opts.mode.size()) gen_msh_opts.sizing_mode = atoi(opts.mode.c_str());

  surfmeshes.resize(num_surfs);
  tags.resize(num_surfs);
  res.resize(num_surfs);
  is_hole.resize(num_surfs, false);

  parse_scaling_factors(opts.scale, num_surfs, scale_factors);
  if (opts.holes.size()) {
    parse_holes(opts.holes, is_hole);
  }

  if (opts.boundary_pres.size())
    gen_msh_opts.preserve_boundary = atoi(opts.boundary_pres.c_str()) == 1;

  for (int sidx = 0; sidx < num_surfs; sidx++) {
    mt_filename sfile(surf_list[sidx], opts.ifmt);

    std::cout << "Reading surface: " << sfile.base << std::endl;
    read_mesh_selected(surfmeshes[sidx], sfile.format, sfile.base);

    if (gen_msh_opts.tags_provided)
      tags[sidx] = atoi(tags_list[sidx].c_str());
    else
      tags[sidx] = surfmeshes[sidx].etags[0];

    // we set the tag of each surface to sidx, in this way we can identify
    // each final surface element with a source surface
    surfmeshes[sidx].etags.assign(surfmeshes[sidx].e2n_cnt.size(), sidx);

    correct_duplicate_vertices(surfmeshes[sidx]);
    correct_duplicate_elements(surfmeshes[sidx]);
    compute_full_mesh_connectivity(surfmeshes[sidx]);
  }
}

#ifdef MT_ADDONS
static void mesh_surf_with_netgen(mt_meshdata &surfmesh, mt_meshdata &output_mesh, mt_vector<mt_idx_t> &tags,
                                  mt_vector<mt_real> &res, mt_vector<kdtree *> &surface_trees,
                                  mt_vector<mt_real> &scale_factors, mt_vector<bool> &is_hole, int qual_lvl) {
  MT_MAP<int, int> bmap;
  get_direct_bounding_surface_map(surface_trees, bmap);

  mt_vector<mt_real> target_res(res);
  for (size_t i = 0; i < res.size(); i++) {
    target_res[i] *= scale_factors[i];
  }

  float max_avrg = *std::max_element(res.begin(), res.end());

  MESHING_QUALITY quality = MESHING_QUALITY(qual_lvl);

  mesh_with_netgen(surfmesh, output_mesh, bmap, target_res, is_hole, tags, max_avrg, quality);
}

static bool epstate_from_yaml(const char *filename, ep_state &EP) {
  Yaml::Node root;
  try {
    Yaml::Parse(root, filename);
  } catch (const Yaml::Exception &e) {
    std::cout << "parsing exception " << e.Type() << ": " << e.what() << std::endl;
    exit(EXIT_FAILURE);
  }

  Yaml::Node &global = root["globals"];

  for (Yaml::Iterator it = global.Begin(); it != global.End(); it++) {
    Yaml::Node &entry = (*it).second;

    if (entry["name"].IsNone() == false) {
      std::string name = entry["name"].As<std::string>("");
      EP.gdata_names.push_back(name);
    } else {
      fprintf(stderr, "%s error: malformed globals entry, name missing! Aborting!\n", __func__);
      return false;
    }

    if (entry["data"].IsNone() == false) {
      std::string datafile = entry["data"].As<std::string>("");
      mt_vector<double> &gdata = EP.gdata.push_back(mt_vector<double>{});
      read_vector(gdata, datafile);
    } else {
      fprintf(stderr, "%s error: malformed globals entry, data missing! Aborting!\n", __func__);
      return false;
    }
  }

  Yaml::Node &vars = root["variables"];
  mt_vector<mt_vector<double>> ep_vars;
  mt_vector<int> ep_var_idx;

  for (Yaml::Iterator it = vars.Begin(); it != vars.End(); it++) {
    Yaml::Node &entry = (*it).second;

    /*
        if(entry["idx"].IsNone() == false) {
          int idx = entry["idx"].As<int>(-1);
          if(idx > -1) {
            ep_var_idx.push_back(
          }
        } else {
          fprintf(stderr, "malformed variables entry, name missing! Aborting!\n");
          return;
        }
    */

    if (entry["data"].IsNone() == false) {
      std::string datafile = entry["data"].As<std::string>("");
      mt_vector<double> &var = ep_vars.push_back(mt_vector<double>{});
      read_vector(var, datafile);
    } else {
      fprintf(stderr, "%s error: malformed variables entry, data missing! Aborting!\n", __func__);
      return false;
    }
  }

  if (ep_vars.size()) {
    // for now we assume onle one IMP, with variables of type double
    int imp_size = ep_vars.size() * sizeof(double);

    ionic_model &imp = EP.imps.push_back(ionic_model{});
    imp.size = imp_size;
    imp.offset = 0;
    imp.nplug = 0;
    imp.name = root["imp name"].As<std::string>("");

    EP.imp_sizes.assign(size_t(1), imp_size);

    size_t num_nodes = ep_vars[0].size();
    EP.saved_nnodes = num_nodes;

    for (size_t i = 1; i < ep_vars.size(); i++) {
      if (ep_vars[i].size() != num_nodes) {
        fprintf(stderr, "%s error: inconsistent variable dataset sizes! Aborting!\n", __func__);
        return false;
      }
    }
    EP.nodal_imp_mask.assign(num_nodes, char(0));
    EP.nodal_imp_offset.resize(num_nodes);
    EP.nodal_imp_offset[0] = 0;
    for (size_t i = 1; i < num_nodes; i++) EP.nodal_imp_offset[i] = EP.nodal_imp_offset[i - 1] + imp_size;

    EP.nodal_imp_data.resize(num_nodes * imp_size);

    for (size_t i = 0; i < num_nodes; i++) {
      double *write_ptr = (double *)(EP.nodal_imp_data.data() + EP.nodal_imp_offset[i]);
      for (size_t j = 0; j < ep_vars.size(); j++) write_ptr[j] = ep_vars[j][i];
    }
  } else {
    fprintf(stderr, "%s error: No varaibles read! Aborting!\n", __func__);
    return false;
  }

  return true;
}
#endif


void generate_mesh_mode(mt_vector<mt_meshdata> &surfmeshes,  // in
                        mt_vector<mt_idx_t> &tags,
                        mt_vector<mt_real> &scale_factors,  // assert scale_factors->size == surfmeshes->size || 1
                        mt_vector<mt_real> &holes_xyz,      // assert holes_xyz->size == surfmeshes->size
                        mt_vector<mt_real> &res, mt_vector<bool> &is_hole,
                        generate_mesh_options &gen_msh_opts,
                        mt_meshdata &output_mesh  // out

) {
  mt_meshdata final_surfmesh;

  struct timeval t1, t2;

  int num_surfs = surfmeshes.size();
  mt_vector<kdtree *> surface_trees(num_surfs);
  mt_vector<vec3r> vol_pts(num_surfs);

  for (int sidx = 0; sidx < num_surfs; sidx++) {
    surface_trees[sidx] = new kdtree(10);

    if (sidx == 0) {
      final_surfmesh = surfmeshes[0];
      std::cout << "Building surface kdtree .." << std::endl;
      surface_trees[sidx]->build_tree(final_surfmesh);

      bool found = point_inside_surface(final_surfmesh, *surface_trees[sidx], vol_pts[sidx]);
      if (!found) {
        fprintf(stderr, "%s error: Cannot determine point in surface no %d! Aborting!\n", __func__, sidx);
        exit(EXIT_FAILURE);
      }

      if (gen_msh_opts.use_netgen == false && is_hole[sidx]) {
        vec3r &pt = vol_pts[sidx];
        holes_xyz.push_back(pt.x);
        holes_xyz.push_back(pt.y);
        holes_xyz.push_back(pt.z);
      }

      res[sidx] = avrg_edgelength_estimate(final_surfmesh, true);

    } else {
      mt_meshdata actsurf = surfmeshes[sidx];

      res[sidx] = avrg_edgelength_estimate(actsurf, true);

#ifdef MT_ADDONS
      if (gen_msh_opts.padding) {
        std::cout << "Enforcing padding .. " << std::endl;
        gettimeofday(&t1, NULL);

        MT_USET<mt_idx_t> modified;
        mt_vector<mt_real> nrml;
        compute_nodal_surface_normals(actsurf, actsurf.xyz, nrml);

        bsp::bsp_tree fitting_tree;
        fitting_tree.build(final_surfmesh.e2n_con, final_surfmesh.xyz);

        float abs_padding = avrg_edgelength_estimate(actsurf, false) * gen_msh_opts.padding;
        shrink_to_avoid(actsurf, nrml, modified, fitting_tree, abs_padding);

        // smooth modified region
        if (modified.size()) {
          short expand_lvl = 2;
          for (short i = 0; i < expand_lvl; i++) expand_nodeset(actsurf, modified);

          mt_vector<mt_idx_t> smth_nod;
          smth_nod.assign(modified.begin(), modified.end());

          mt_mask empty_mask;
          smooth_nodes(actsurf, actsurf, empty_mask, smth_nod, 50, 0.15, false);
        }

        gettimeofday(&t2, NULL);
        std::cout << "Done in " << (float)timediff_sec(t1, t2) << " sec" << std::endl;
      }
#endif

      std::cout << "Unifying surfaces .." << std::endl;
      mesh_union(final_surfmesh, actsurf, false);
      std::cout << "Building surface kdtree .." << std::endl;
      surface_trees[sidx]->build_tree(actsurf);

      bool found = point_inside_surface(actsurf, *surface_trees[sidx], vol_pts[sidx]);
      if (!found) {
        fprintf(stderr, "%s error: Cannot determine point in surface %d! Aborting!\n", __func__, sidx);
        exit(EXIT_FAILURE);
      }

      if (gen_msh_opts.use_netgen == false && is_hole[sidx]) {
        vec3r &pt = vol_pts[sidx];
        holes_xyz.push_back(pt.x);
        holes_xyz.push_back(pt.y);
        holes_xyz.push_back(pt.z);
      }
    }
  }

  compute_full_mesh_connectivity(final_surfmesh);
  gettimeofday(&t1, NULL);

  mt_vector<mt_tuple<mt_idx_t>> intersec_edges;
  mt_vector<mt_idx_t> intersec_eidx;
  check_self_intersection(final_surfmesh, intersec_edges, intersec_eidx);
  bool have_error = intersec_edges.size() > 0;

  if (have_error) {
    fprintf(stderr,
            "mesh generation error: meshing surface has self-intersections. "
            "Outputting debug surface \"dbg_surfmesh.vtk\". Aborting!\n");
    write_mesh_selected(final_surfmesh, "vtk_bin", "dbg_surfmesh");
    exit(EXIT_FAILURE);
  }

  gettimeofday(&t2, NULL);
  std::cout << "Done in " << (float)timediff_sec(t1, t2) << " sec" << std::endl;

  // compute permutation of surfaces so that we have an outer-to-inner surface
  // order
  if (surface_trees.size() > 1) {
    mt_vector<int> perm_new2old, perm_old2new;
    get_surface_outer_to_inner_perm(surface_trees, perm_new2old);
    invert_perm(perm_new2old, perm_old2new);

    std::cout << "Used outer-to-inner surface ordering: ";
    for (size_t i = 0; i < perm_new2old.size(); i++) std::cout << tags[perm_new2old[i]] << " ";
    std::cout << std::endl;

    // now permute the input data order
    mt_vector<mt_idx_t> tags_cpy(tags);
    mt_vector<vec3r> vol_pts_cpy(vol_pts);
    mt_vector<kdtree *> trees_cpy(surface_trees);
    mt_vector<mt_real> scale_factors_cpy(scale_factors);
    mt_vector<mt_real> res_cpy(res);
    mt_vector<bool> is_hole_cpy(is_hole);

    for (size_t i = 0; i < perm_new2old.size(); i++) {
      mt_idx_t p = perm_new2old[i];
      tags[i] = tags_cpy[p];
      surface_trees[i] = trees_cpy[p];
      vol_pts[i] = vol_pts_cpy[p];
      scale_factors[i] = scale_factors_cpy[p];
      res[i] = res_cpy[p];
      is_hole[i] = is_hole_cpy[p];
    }

    for (mt_tag_t &t : final_surfmesh.etags) t = perm_old2new[t];
  }

  // the first scale factor is also the global one
  mt_real avg_edge_length = avrg_edgelength_estimate(final_surfmesh, true);

  gen_msh_opts.boundary_step *= scale_factors[0];

  mt_meshdata boundary_mesh;
  bool only_boundary_layer = false;

  if (gen_msh_opts.boundary_layer != 0) {
    if (gen_msh_opts.boundary_layer < 0) {
      only_boundary_layer = true;
      gen_msh_opts.boundary_layer = -gen_msh_opts.boundary_layer;
    }

    mt_meshdata inner_surf;
    generate_boundary_layers(final_surfmesh, gen_msh_opts.boundary_layer, gen_msh_opts.boundary_step,
                             gen_msh_opts.boundary_inc, gen_msh_opts.sizing_mode, gen_msh_opts.boundary_symmetric,
                             inner_surf, boundary_mesh);
    final_surfmesh.xyz = inner_surf.xyz;
  }

  if (only_boundary_layer) {
    output_mesh = boundary_mesh;
  } else {
#ifdef MT_ADDONS
    if (gen_msh_opts.use_netgen) {
      std::cout << "\n\nMeshing with NETGEN\n\n" << std::endl;
      gettimeofday(&t1, NULL);
      mesh_surf_with_netgen(final_surfmesh, output_mesh, tags, res, surface_trees, scale_factors, is_hole, gen_msh_opts.qual_lvl);
      gettimeofday(&t2, NULL);
      std::cout << "Done in " << (float)timediff_sec(t1, t2) << " sec" << std::endl;
    } else
#endif
    {
      int numberofregions = tags.size();
      mt_vector<double> regionattributes(numberofregions * 5);

      for (int i = 0; i < numberofregions; i++) {
        regionattributes[i * 5 + 0] = vol_pts[i].x;
        regionattributes[i * 5 + 1] = vol_pts[i].y;
        regionattributes[i * 5 + 2] = vol_pts[i].z;
        regionattributes[i * 5 + 3] = tags[i];

        double edge_length = avg_edge_length * scale_factors[i];
        double desired_volume = edge_length * edge_length * edge_length / 8.48528;
        regionattributes[i * 5 + 4] = desired_volume;
      }

      std::cout << "\n\nMeshing with TETGEN\n\n" << std::endl;
      gettimeofday(&t1, NULL);

      mt_vector<mt_real> sizes;

      if (gen_msh_opts.sizing_mode == 1) {
        generate_surfmesh_sizing_field(final_surfmesh, sizes);

        mt_vector<mt_real> final_scaling_ele(final_surfmesh.e2n_cnt.size()), final_scaling_nod;
        for (size_t eidx = 0; eidx < final_surfmesh.e2n_cnt.size(); eidx++)
          final_scaling_ele[eidx] = scale_factors[final_surfmesh.etags[eidx]];

        elemData_to_nodeData(final_surfmesh, final_scaling_ele, final_scaling_nod);

        for (size_t i = 0; i < final_scaling_nod.size(); i++) sizes[i] *= final_scaling_nod[i];
      }

      // currently we can only mesh with preserved boundary since we have only one
      // triangle per face..
      mesh_with_tetgen(final_surfmesh, output_mesh, regionattributes, holes_xyz, gen_msh_opts.qual_lvl,
                       gen_msh_opts.sizing_mode == 1 ? &sizes : nullptr, gen_msh_opts.preserve_boundary);

      gettimeofday(&t2, NULL);
      std::cout << "Done in " << (float)timediff_sec(t1, t2) << " sec" << std::endl;

      if (gen_msh_opts.boundary_layer > 0) mesh_union(output_mesh, boundary_mesh, true);

      // for debug purposes we sometimes dont want to overwrite the elem tags
      bool recompute_tag = true;
      if (recompute_tag) {
        std::cout << "Sampling elements " << std::endl;
        gettimeofday(&t1, NULL);

        if (num_surfs > 1) {
          mt_mask tag_found(output_mesh.e2n_cnt.size());

          // tetgen can already keep track of the region index. unfortunately
          // there are some untagged elements remaining and we still need to do a
          // manual tagging pass. still, we can reduce the sampling time by
          // inserting all elements that already have a valid tag into the
          // tag_found mask.
          MT_USET<mt_idx_t> tags_set;
          tags_set.insert(tags.begin(), tags.end());

          size_t num_elems = output_mesh.e2n_cnt.size(), num_tagged = 0;
          for (size_t eidx = 0; eidx < num_elems; eidx++) {
            if (tags_set.count(output_mesh.etags[eidx])) {
              tag_found.insert(eidx);
              num_tagged++;
            } else {
              output_mesh.etags[eidx] = -1;
            }
          }

          printf("%d elems were not tagged correctly by Tetgen and need "
                 "additional tagging by surface sampling.\n",
                 int(num_elems - num_tagged));

          // for a nested surface input, we sample the output mesh elements w.r.t
          // the surfaces
          for (int sidx = num_surfs - 1; sidx >= 0; sidx--)
            sample_elem_tags(output_mesh, tag_found, *surface_trees[sidx], tags[sidx], 1);
        } else {
          // for one surf we just set the tags
          output_mesh.etags.assign(output_mesh.e2n_cnt.size(), tags[0]);
        }
        gettimeofday(&t2, NULL);
        std::cout << "Done in " << (float)timediff_sec(t1, t2) << " sec" << std::endl;
      }
    }
  }
}
