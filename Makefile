include my_switches.def
include switches.def

OBJ_UTILS = \
utils/data_structs.o \
utils/geometry_utils.o \
utils/io_utils.o \
utils/string_utils.o \
utils/mesh_quality.o \
utils/mesh_utils.o \
utils/mesh_refining.o \
utils/mesh_smoothing.o \
utils/mesh_generation.o \
utils/topology_utils.o \
utils/mmg_utils.o \
utils/gmsh_utils.o \
utils/stellar_utils.o \
utils/vtk_utils.o \
utils/itk_utils.o \
utils/vcflow_utils.o \
utils/netgen_utils.o \
utils/ensight_utils.o \
utils/mt_igb.o \
utils/kdtree.o \
utils/shape_utils.o \
utils/tetgen/tetgen.o \
utils/tetgen/predicates.o

OBJ_MODES = \
modes/clean_mode.o \
modes/clean_impls.o \
modes/convert_mode.o \
modes/collect_mode.o \
modes/extract_mode.o \
modes/extract_impls.o \
modes/generate_mode.o \
modes/generate_impls.o \
modes/insert_mode.o \
modes/insert_impls.o \
modes/interpolate_mode.o \
modes/interpolate_impls.o \
modes/itk_mode.o \
modes/itk_impls.o \
modes/map_mode.o \
modes/merge_mode.o \
modes/mt_modes_base.o \
modes/query_mode.o \
modes/query_impls.o \
modes/resample_mode.o \
modes/reindex_mode.o \
modes/restore_mode.o \
modes/smooth_mode.o \
modes/split_mode.o \
modes/transform_mode.o

DEPS = $(OBJ_UTILS:%.o=%.d) $(OBJ_MODES:%.o=%.d)

CHECKS = $(OBJ_UTILS:%.o=%_chk) $(OBJ_MODES:%.o=%_chk)
CHECKER = clang-check
CHECKER_FLAGS = $(CXXOPTS) -I/usr/lib/gcc/x86_64-redhat-linux/9/include

LIB_UTILS = utils/libmtutils.a
LIB_MODES = modes/libmtmodes.a
LIBS = -Lutils -Lmodes -lmtmodes -lmtutils
ifdef MT_OPENMP
	LIBS += $(OMP_LIB)
endif

ifdef MT_ADDONS
CXXOPTS += -DMT_ADDONS -Imeshtool_addons/src
LIBS += meshtool_addons/src/ng_interface.o -Lmeshtool_addons -laddons -Lmeshtool_addons/src/netgen -lngbase -lpthread
  ifndef WINDOWS_BUILD
  LIBS += -ldl
  endif
endif

ifdef MT_GZIP
LIBS += -lz
endif

# Guess the compiler for generating the template my_switches.def based on the OS
ifeq ($(shell uname), Darwin)
	MT_CC_ENV_GUESS = clang
else
	MT_CC_ENV_GUESS = gnu
endif

default: parbuild

all: parbuild stdl

parbuild:
	$(MAKE) $(MKFLG) meshtool_deps
	$(MAKE) $(MKSLFLG) meshtool

stdl:
	cd standalones; $(MAKE) $(MKFLG) all

doc: README.html Advanced.Workflows.html

my_switches.def:
	@echo "* generating "$@
	@echo "#MT_DEBUG = 1         # Compile in debug mode" > my_switches.def
	@echo "#MT_STATIC = 1        # Try to link statically" >> my_switches.def
	@echo "MT_OPENMP = 1        # Use OpenMP" >> my_switches.def
	@echo "#MT_ADDONS = 1        # Include addons" >> my_switches.def
	@echo "MT_SILENT = 1        # Dont show build command" >> my_switches.def
	@echo "MT_CC_ENV = $(MT_CC_ENV_GUESS)      # choose between intel, gnu, clang" >> my_switches.def
	@echo "#TUNE_NATIVE = 1         # tune for the native platform (not for deployment)" >> my_switches.def
	@echo "PAR_MAKE = 8         # choose the number of processes to use for parallel make" >> my_switches.def
	@echo "#MT_SILENT_PRG = 1    # use silent progress instead of a bar" >> my_switches.def
	@echo "#MT_GZIP = 1          # whether gzip is available" >> my_switches.def
	@echo "#WINDOWS_BUILD = 1    # tell build system we are on windows" >> my_switches.def

utils/%.o: utils/%.cpp
	@echo "* compiling:" $<
	$(CXX) -c -o $@ $(CXXOPTS) -fPIC $<

utils/tetgen/%.o: utils/tetgen/%.cxx
	@echo "* compiling:" $<
	$(CXX) -c -o $@ $(CXXOPTS) -fPIC $<

modes/%.o: modes/%.cpp
	@echo "* compiling:" $<
	$(CXX) -c -o $@ -Iutils $(CXXOPTS) -fPIC $<

%.html : %.md
	python md_to_html.py $< -o $@

$(LIB_UTILS): $(OBJ_UTILS)
	@echo "* creating archive:" $@
	ar cr $(LIB_UTILS) utils/*.o utils/tetgen/*.o
	ranlib $(LIB_UTILS)

$(LIB_MODES): $(OBJ_MODES)
	@echo "* creating archive:" $@
	ar cr $(LIB_MODES) modes/*.o
	ranlib $(LIB_MODES)

meshtool_deps: $(LIB_MODES) $(LIB_UTILS)

ifdef MT_ADDONS
ifndef WINDOWS_BUILD
LINK_OPTS = '-Wl,-rpath,$$ORIGIN/meshtool_addons/external/lib'
ifeq ($(shell uname), Darwin)
LINK_OPTS += -headerpad_max_install_names
endif
endif
endif

meshtool: main.cpp meshtool_deps
ifdef MT_ADDONS
	@echo "* building addons .."
	cd meshtool_addons && $(MAKE) $(MKFLG) libaddons.a
endif
	@echo "* linking:" $@
	$(CXX) $(CXXOPTS) $(LINK_OPTS) -Imodes -Iutils -o meshtool main.cpp $(LIBS)
ifdef MT_ADDONS
ifeq ($(shell uname), Darwin)
	install_name_tool -change libngbase.dylib @executable_path/meshtool_addons/external/lib/libngbase.dylib meshtool
endif
endif

%_chk: %.cpp
	$(CHECKER) -analyze $< -- $(CHECKER_FLAGS)

%_chk: %.cxx
	$(CHECKER) -analyze $< -- $(CHECKER_FLAGS)

check: $(CHECKS)
	find . -name \*.plist -exec rm \{\} \;

doxygen:
	doxygen doxydoc/mt_doc.doxygen

clean:
	rm -f meshtool
	rm -f utils/*.[od] utils/tetgen/*.[od] utils/*.a
	rm -f modes/*.[od] modes/*.a
	rm -rf doxydoc/html

cleanall: clean
	cd standalones && make clean

status:
	git status
ifdef MT_ADDONS
	cd meshtool_addons && git status
endif

update:
	git pull
ifdef MT_ADDONS
	cd meshtool_addons && git pull
endif

-include $(DEPS)
